﻿using Endor.Api.Web.Classes;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class EmbeddedAssemblyTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/assemblypart/embedded";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task TestEmbeddedAssemblyEndpoint()
        {
            var assemblyData = await EndpointTests.AssertGetStatusCode<AssemblyData>(client, $"{apiUrl}/{(int)EmbeddedAssemblyType.LayoutComponent}", HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);
            var assemblyData2 = await EndpointTests.AssertGetStatusCode<AssemblyData>(client, $"{apiUrl}/2", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public void TestEmbeddedAssemblySingleton()
        {
            EmbeddedAssemblySingleton layoutComponentSingleton = EmbeddedAssemblySingleton.GetInstance();
            Assert.IsNotNull(layoutComponentSingleton.GetEmbeddedAssembly(EmbeddedAssemblyType.LayoutComponent));
            Assert.IsNull(layoutComponentSingleton.GetEmbeddedAssembly((EmbeddedAssemblyType)4));
        }

        
    }
}
