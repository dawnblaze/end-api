﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    [TestClass]
    public class GLAccountEndpointTests
    {
        public const string apiUrl = "/api/glaccounts";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestGLAccountName;
        public TestContext TestContext { get; set; }
        
        [TestInitialize]
        public void Initialize()
        {
            this.NewTestGLAccountName = DateTime.UtcNow + " TEST GL ACCOUNT";
        }

        [TestMethod]
        public async Task TestGLAccountSingleEndpoints()
        {
            GLAccount acntNameTest = new GLAccount()
            {
                Name = this.NewTestGLAccountName,
                CanEdit = true,
                IsActive = true,
                BID = 1,
                GLAccountType = 10
            };

            //test: expect OK
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(acntNameTest), HttpStatusCode.OK);

            // GET api/glaccounts/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleGLAccount sga in simpleList)
            {
                if (sga.DisplayName == acntNameTest.Name)
                {
                    acntNameTest.ID = sga.ID;
                }
            }

            //POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{acntNameTest.ID}/action/setinactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{acntNameTest.ID}/action/setactive", HttpStatusCode.OK);

            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + acntNameTest.ID, JsonConvert.SerializeObject(acntNameTest), HttpStatusCode.OK);

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{acntNameTest.ID}", HttpStatusCode.NoContent);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/9241657", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestGLAccountCanDelete()
        {
            GLAccount acntNameTest = new GLAccount()
            {
                Name = this.NewTestGLAccountName,
                CanEdit = true,
                IsActive = true,
                BID = 1,
                GLAccountType = 10
            };

            //test: expect OK
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(acntNameTest), HttpStatusCode.OK);

            // GET api/glaccounts/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleGLAccount sga in simpleList)
            {
                if (sga.DisplayName == acntNameTest.Name)
                {
                    acntNameTest.ID = sga.ID;
                }
            }

            var statusResp = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{acntNameTest.ID}/Action/CanDelete", HttpStatusCode.OK);
            Assert.IsTrue(statusResp.Success);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{acntNameTest.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestFiltering()
        {

            GLAccount newAccount = new GLAccount()
            {
                Name = this.NewTestGLAccountName,
                CanEdit = true,
                IsActive = true,
                BID = 1,
                GLAccountType = 10 //asset
            };

            //test: expect OK
            var savedAccount = await EndpointTests.AssertPostStatusCode<GLAccount>(client, $"{apiUrl}", JsonConvert.SerializeObject(newAccount), HttpStatusCode.OK);

            //test that our account shows under no filters
            var all = await EndpointTests.AssertGetStatusCode<GLAccount[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(all.FirstOrDefault(x => x.Name == newAccount.Name));

            //test that our account DOES show under actives
            var active = await EndpointTests.AssertGetStatusCode<GLAccount[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(active.All(x => x.IsActive));
            Assert.IsNotNull(active.FirstOrDefault(x => x.Name == newAccount.Name));

            var activeSimple = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"{apiUrl}/simplelist?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(activeSimple.All(x => x.IsActive));
            Assert.IsNotNull(activeSimple.FirstOrDefault(x => x.DisplayName.Contains(newAccount.Name)));

            //test that our account DOES NOT show under inactives
            var inactive = await EndpointTests.AssertGetStatusCode<GLAccount[]>(client, $"{apiUrl}?IsActive=false", HttpStatusCode.OK);
            Assert.IsTrue(inactive.All(x => !x.IsActive));
            Assert.IsNull(inactive.FirstOrDefault(x => x.Name == newAccount.Name));

            var inactiveSimple = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"{apiUrl}/simplelist?IsActive=false", HttpStatusCode.OK);
            Assert.IsTrue(inactiveSimple.All(x => !x.IsActive));
            Assert.IsNull(inactiveSimple.FirstOrDefault(x => x.DisplayName.Contains(newAccount.Name)));

            var notCOGSSimple = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"{apiUrl}/simplelist?IsCOGS=false", HttpStatusCode.OK);
            Assert.IsNotNull(notCOGSSimple.FirstOrDefault(x => x.DisplayName.Contains(newAccount.Name)));

            var isCOGSSimple = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"{apiUrl}/simplelist?IsCOGS=true", HttpStatusCode.OK);
            Assert.IsNull(isCOGSSimple.FirstOrDefault(x => x.DisplayName.Contains(newAccount.Name)));

            //test that our account DOES show under assets
            var assets = await EndpointTests.AssertGetStatusCode<GLAccount[]>(client, $"{apiUrl}?IsAsset=true", HttpStatusCode.OK);
            Assert.IsTrue(assets.All(x => x.IsAsset.HasValue && x.IsAsset.Value));
            Assert.IsNotNull(assets.FirstOrDefault(x => x.Name == newAccount.Name));
            //test using the other IsAsset filter
            assets = await EndpointTests.AssertGetStatusCode<GLAccount[]>(client, $"{apiUrl}?GLAccountType=10", HttpStatusCode.OK);
            Assert.IsTrue(assets.All(x => x.GLAccountType == 10));

            var assetSimple = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"{apiUrl}/simplelist?GLAccountType=10", HttpStatusCode.OK);
            Assert.IsNotNull(assetSimple.FirstOrDefault(x => x.DisplayName.Contains(newAccount.Name)));

            assetSimple = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"{apiUrl}/simplelist?IsAsset=10", HttpStatusCode.OK);
            Assert.IsNotNull(assetSimple.FirstOrDefault(x => x.DisplayName.Contains(newAccount.Name)));

            //change our account to a liability
            savedAccount.GLAccountType = 20; //liability
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + savedAccount.ID, JsonConvert.SerializeObject(savedAccount), HttpStatusCode.OK);

            //and test that it shows up under IsLiability
            var liabilities = await EndpointTests.AssertGetStatusCode<GLAccount[]>(client, $"{apiUrl}?IsLiability=true", HttpStatusCode.OK);
            Assert.IsTrue(liabilities.All(x => x.IsLiability.HasValue && x.IsLiability.Value));

            //and GLACccountType 20
            liabilities = await EndpointTests.AssertGetStatusCode<GLAccount[]>(client, $"{apiUrl}?GLAccountType=20", HttpStatusCode.OK);
            Assert.IsTrue(liabilities.All(x => x.GLAccountType == 20));

            var liabilitySimple = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"{apiUrl}/simplelist?GLAccountType=10&GLAccountType=20", HttpStatusCode.OK);
            Assert.IsNotNull(liabilitySimple.FirstOrDefault(x => x.DisplayName.Contains(newAccount.Name)));

            //make sure our new account still doesn't show up under the ASSET filter
            assets = await EndpointTests.AssertGetStatusCode<GLAccount[]>(client, $"{apiUrl}?IsAsset=true", HttpStatusCode.OK);
            Assert.IsNull(assets.FirstOrDefault(x => x.Name == newAccount.Name));

            //test: lots of filters still work ANDded together
            var allFilters = await EndpointTests.AssertGetStatusCode<GLAccount[]>(client, $"{apiUrl}?IsActive=true&IsLiability=true&GLAccountType=20&NumberedName={newAccount.Name}", HttpStatusCode.OK);
            Assert.IsNotNull(allFilters.FirstOrDefault(x => x.Name == newAccount.Name));

            var assetLiabilityActiveSimple = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"{apiUrl}/simplelist?IsActive=True&GLAccountType=10&GLAccountType=20", HttpStatusCode.OK);
            Assert.IsNotNull(assetLiabilityActiveSimple.FirstOrDefault(x => x.DisplayName.Contains(newAccount.Name)));
        }

        [TestMethod]
        public async Task TestPostNewInactive()
        {

            GLAccount newAccount = new GLAccount()
            {
                Name = this.NewTestGLAccountName,
                CanEdit = true,
                IsActive = false,
                BID = 1,
                GLAccountType = 10 //asset
            };

            //test: expect OK
            var savedAccount = await EndpointTests.AssertPostStatusCode<GLAccount>(client, $"{apiUrl}", JsonConvert.SerializeObject(newAccount), HttpStatusCode.OK);

            var account = await EndpointTests.AssertGetStatusCode<GLAccount>(client, $"{apiUrl}/{savedAccount.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(account);
            Assert.AreEqual(false, account.IsActive);
        }

        [TestCleanup]
        public void Teardown()
        {
            //you can actually do test-specific things like this
            //switch (TestContext.TestName)
            //{
            //    case "TestGLAccountSingleEndpoints":
            //    case "TestFiltering":
            //        // do stuff
            //        break;
            //    default:
            //        break;
            //}
            this.DeleteTestAccountRecord();
        }

        private void DeleteTestAccountRecord()
        {
            string newTestGLAccountName = NewTestGLAccountName;
            Task.Run(async () =>
            {

                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                SimpleGLAccount[] result = JsonConvert.DeserializeObject<SimpleGLAccount[]>(responseString);
                if (result != null)
                {
                    SimpleGLAccount sga = result.FirstOrDefault(x => x.DisplayName.Contains(newTestGLAccountName));
                    if (sga != null)
                    {
                        await client.DeleteAsync($"{apiUrl}/{sga.ID}");
                    }
                }
            }).Wait();
        }
    }
}
