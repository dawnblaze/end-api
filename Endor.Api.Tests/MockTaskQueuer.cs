﻿using System.Threading.Tasks;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using System;
using System.Collections.Generic;
using Endor.EF;
using Microsoft.EntityFrameworkCore;
using Endor.GLEngine;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Endor.Models;

namespace Endor.Api.Tests
{
    internal class MockTaskQueuer : ITaskQueuer
    {
        public string Priority { get; set; }
        public string Schedule { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public int activityID = -99;

        ITenantDataCache cache;

        public MockTaskQueuer(ITenantDataCache cache)
        {
            this.cache = cache;
        }

        public Task<string> EmptyTemp(short bid)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public async Task<string> RecomputeGL(short bid, byte type, int Id, string name, string Subject, short? CompletedById, int? CompletedByContactId, byte glEntryType)
        {
            //comment this line out to recompute the GL
            //return Task.FromResult(Guid.NewGuid().ToString());
            //actually do real computations here
            var ctx = new ApiContext(new DbContextOptionsBuilder<ApiContext>().Options, this.cache, bid);
            Endor.GLEngine.GLEngine glEngine = new Endor.GLEngine.GLEngine(bid, ctx);
            var glEntries = glEngine.ComputeGLDiff((GLEngine.Models.EnumGLType)type, Id);

            if ((GLEngine.Models.EnumGLType)type == GLEngine.Models.EnumGLType.Payment)
            {
                var application = ctx.PaymentApplication.Where(x => x.ID == Id).First();
                var master = ctx.PaymentMaster.Where(x => x.ID == application.MasterID).Include(x => x.PaymentApplications).First();
                var applist = master.PaymentApplications;
                //var glEntries = ctx.GLData.Where(x => x.PaymentID.HasValue && applist.Select(q => q.ID).Contains(x.PaymentID.GetValueOrDefault()));
                if (glEntries.Count() == 0)
                {
                    if (((application.PaymentTransactionType == (byte)PaymentTransactionType.Payment_from_Refundable_Credit) ||
                        (application.PaymentTransactionType == (byte)PaymentTransactionType.Payment_from_Nonrefundable_Credit) ||
                        (application.PaymentTransactionType == (byte)PaymentTransactionType.Refund_to_Refundable_Credit) ||
                        (application.PaymentTransactionType == (byte)PaymentTransactionType.Refund_to_Nonrefundable_Credit)
                        )
                        && application.OrderID == null)
                    {
                        //the reason for this is that the GLEngine does not create any transaction for the negative credit amount against the master (by design).
                        //The transaction is handled completely by the applications to the order
                    }
                    else
                    {
                        throw new Exception("RecomputeGLError - No entries");
                    }
                }
                if (glEntries.Sum(g => g.Amount) != 0) throw new Exception("Sum of GLEntries must be 0");
                //these don't work because we're not processing all the applications
                //if (glEntries.Where(g => g.GLAccountID == Undeposited_Funds_ID).Sum(g => g.Amount) != (master.RefBalance)) throw new Exception("Sum of Deposits must be equal refundable balance");
                //if (glEntries.Where(g => g.GLAccountID == Credit_Adjustments_ID).Sum(g => g.Amount) !=  (master.NonRefBalance ?? 0m)) throw new Exception("Sum of credit adjustment must be equal non-refundable balance");
                //if (glEntries.Where(g => 
                //    (g.GLAccountID == Accounts_Receivable_ID) || (g.GLAccountID == Customer_Deposits_ID)).Sum(g => g.Amount) != 
                //    applist.Where(a => a.OrderID.HasValue).Sum(a => a.Amount)) throw new Exception("Sum of deposits and AR amounts must equal order balances");
            }

            if (glEntries.Any())
                await glEngine.InsertGLActivity(name, Subject, CompletedById, CompletedByContactId, DateTime.UtcNow, (GLEntryType)Enum.ToObject(typeof(GLEntryType), type), Id, glEntries, (a, b, c) => Task.FromResult(activityID++));

            return Guid.NewGuid().ToString();
        }

        public Task<string> EmptyTrash(short bid)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> IndexClasstype(short bid, int ctid)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> IndexModel(short bid, int ctid, int id)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> Test(short bid)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> CheckOrderStatus(short bid, int orderId, int status)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> OrderPricingCompute(short bid, int orderID)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> OrderPricingComputeTax(short bid, int orderID)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> IndexMultipleID(short bid, int ctid, ICollection<int> ids)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> DeleteExpiredDrafts(short bid)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> RepairUntrustedConstraint(short bid)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> RegenerateCBELForMachine(short bid, int machineId)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> CreateDBBackup(short bid, DbType dbType)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> ClearAllAssemblies(short bid)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> ClearAssembly(short bid, int ctid, int id)
        {
            return Task.FromResult(Guid.NewGuid().ToString());
        }

        public Task<string> GenerateDocumentReportByMenuID(byte aid, short bid, byte documentReportType, int MenuID, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null)
        {
            throw new NotImplementedException();
        }

        public Task<string> GenerateDocumentReportByTemplateName(byte aid, short bid, byte documentReportType, string templateName, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null)
        {
            throw new NotImplementedException();
        }

        public async Task<string> Reconciliate(short bid, byte? locationID, short? enteredById, DateTime accountingDT, bool createAdjustments, IEnumerable<(decimal CashIn, decimal CashOut, decimal CashDeposit, byte LocationID)> cashDrawer)
        {
            var ctx = new ApiContext(new DbContextOptionsBuilder<ApiContext>().Options, this.cache, bid);
            var glEngine = new Endor.GLEngine.GLEngine(bid, ctx);
            var lastKnownReconcilation = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var endDate = DateTime.UtcNow.AddDays(1);
            var enteredByID = ctx.EmployeeData.First(t => t.BID == bid).ID;
            var activityIDs = ctx.ActivityGlactivity.Where(x => x.ID < 0).Select(x => x.ID).ToList();
            var nextActivityID = activityIDs.Any() ? activityIDs.Max() + 1 : -99;

            var lastReconciliation = ctx.Reconciliation.FirstOrDefault(t => t.BID == bid);
            if (lastReconciliation != null)
            {
                lastKnownReconcilation = lastReconciliation.AccountingDT;
            }

            await glEngine.Reconcile(locationID, enteredByID, endDate, createAdjustments, null, (ctxRef, bidRef, classtype) => Task.FromResult(-99));

            return Guid.NewGuid().ToString();
        }

        public Task<string> CreateViewCustomField(short bid, int appliesToClassTypeID, string sourceDataTable, string viewBaseName, string viewSchema = "dbo", string IDFieldName = "ID")
        {
            throw new NotImplementedException();
        }
    }
}