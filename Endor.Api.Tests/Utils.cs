﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    public static class Utils
    {
        public static JArray EmptyJArray = new JArray();
        public static JObject EmptyJObject = new JObject();

        public static int GetHashIDForTestMethod([System.Runtime.CompilerServices.CallerMemberName]string memberName = "")
        {
            //ABS fouls up the hash code but entityClient regexes assume positive IDs
            return Math.Abs(memberName.GetHashCode());
        }

        internal static DomainEmail GetDomainEmail(short bid = 1)
        {
            return new DomainEmail()
            {
                BID = bid,
                IsActive = true,
                DomainName = "smtp",
                ProviderType = EmailProviderType.CustomSMTP
            };
        }

        internal static DomainEmail GetCorebridgeDomainEmail(short bid = 1, short? id = null)
        {
            var domainEmail = new DomainEmail()
            {
                BID = bid,
                IsActive = true,
                DomainName = "corebridge.net",
                ProviderType = EmailProviderType.CustomSMTP
            };

            if (id.HasValue)
                domainEmail.ID = id.Value;

            return domainEmail;
        }

        /// <summary>
        /// Get's an EmailAccountData object for testing
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="id">ID for the EmailAccount</param>
        /// <param name="domainEmailID">ID for the associated DomainEmail</param>
        /// <returns></returns>
        internal static EmailAccountData GetEmailAccount(short bid = 1, short? id = null, short? domainEmailID = null)
        {
            var emailAccountData = new EmailAccountData()
            {
                BID = bid,
                UserName = "test",
                DomainName = "corebridge.net",
                Credentials = "{password: \"test\"}",
                StatusType = EmailAccountStatus.Pending
            };

            if (id.HasValue)
                emailAccountData.ID = id.Value;
            if (domainEmailID.HasValue)
                emailAccountData.DomainEmailID = domainEmailID.Value;

            return emailAccountData;
        }

        /// <summary>
        /// You will need to set  
        /// CompanyID, PickupLocationID 
        /// TaxGroupID, ProductionLocationID
        /// </summary>
        /// <returns></returns>
        public static OrderData GetTestOrderData()
        {
            return new OrderData()
            {
                BID = TestConstants.BID,
                ClassTypeID = (int)ClassType.Order,
                ModifiedDT = DateTime.UtcNow,
                LocationID = 1,
                TransactionType = (byte)OrderTransactionType.Order,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                OrderStatusStartDT = DateTime.UtcNow,
                Number = 1000,
                FormattedNumber = "INV-1000",
                PriceTaxRate = 0.01m,
                PaymentPaid = 0m,
                TaxGroupID = 0,
                PriceTax = 0
            };
        }

        /// <summary>
        /// You will need to set  
        /// CompanyID, PickupLocationID 
        /// TaxGroupID, ProductionLocationID
        /// </summary>
        /// <returns></returns>
        public static CreditMemoData GetTestCreditMemoData()
        {
            return new CreditMemoData()
            {
                BID = TestConstants.BID,
                ClassTypeID = (int)ClassType.CreditMemo,
                ModifiedDT = DateTime.UtcNow,
                LocationID = 1,
                TransactionType = (byte)OrderTransactionType.Memo,
                OrderStatusID = OrderOrderStatus.CreditMemoUnposted,
                OrderStatusStartDT = DateTime.UtcNow,
                Number = 1000,
                FormattedNumber = "INV-1000",
                PriceTaxRate = 0.01m,
                PaymentPaid = 0m,
                TaxGroupID = 0,
                PriceProductTotal = 100m,
                PriceTax = 5m,
            };
        }

        public static EstimateData GetTestEstimateData()
        {
            return new EstimateData()
            {
                BID = TestConstants.BID,
                ClassTypeID = (int)ClassType.Estimate,
                ModifiedDT = DateTime.UtcNow,
                LocationID = 1,
                TransactionType = (byte)OrderTransactionType.Estimate,
                OrderStatusID = OrderOrderStatus.EstimatePending,
                OrderStatusStartDT = DateTime.UtcNow,
                Number = 1000,
                FormattedNumber = "INV-1000",
                PriceTaxRate = 0.01m,
                PaymentPaid = 0m,
                TaxGroupID = 0
            };
        }


        public static FlatListItem GetFlatListItem(FlatListType type = FlatListType.TaxExemptReasons)
        {
            return new FlatListItem()
            {
                ClassTypeID = (int)ClassType.FlatListItem,
                FlatListType = type,
                IsActive = true,
                IsAdHoc = false,
                IsSystem = true,
                ModifiedDT = DateTime.UtcNow,
                Name = "FlatListItemSerializationTest" + DateTime.Now.ToString("yyyyMMddHHmmssffff")
            };
        }

        internal static OrderDestinationData GetTestOrderDestinationData(string testID, short destNumber, short orderItemStatusID, int orderItemID, int orderID, decimal priceTax, OrderTransactionType transactionType)
        {
            return new OrderDestinationData()
            {
                BID = 1,
                ClassTypeID = (int)ClassType.OrderDestination,
                ComputedNet = null,
                Description = $"{nameof(OrderDestinationData.Description)}{testID}",
                DestinationNumber = destNumber,
                DestinationType = OrderDestinationType.OnSiteService,
                HasDocuments = false,
                IsForAllItems = true,
                IsTaxExempt = false,
                ItemStatusID = orderItemStatusID,
                Name = $"{nameof(OrderDestinationData.Name)}{testID}",
                OrderID = orderID,
                OrderStatusID = OrderOrderStatus.DestinationReady,
                PriceIsLocked = false,
                PriceNetOV = false,
                PriceTax = priceTax,
                PriceTaxableOV = false,
                TransactionType = (byte)transactionType
            };
        }

        public static OrderItemData GetTestOrderItemData(TransactionHeaderData createdOrder)
        {
            return new OrderItemData()
            {
                BID = TestConstants.BID,
                HasCustomImage = false,
                HasDocuments = false,
                HasProof = false,
                ID = 1,
                OrderID = createdOrder.ID,
                ItemNumber = 1,
                Quantity = 1,
                Name = "test",
                IsOutsourced = false,
                OrderStatusID = createdOrder.OrderStatusID,
                ItemStatusID = 18,
                ProductionLocationID = createdOrder.ProductionLocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = createdOrder.TaxGroupID,
                TaxGroupOV = false,
                IsTaxExempt = false,
                TransactionType = createdOrder.TransactionType
            };
        }

        public static OrderItemComponent GetTestOrderItemComponentData(OrderItemData createdOrderItem, GLAccount incomeAccount)
        {
            return new OrderItemComponent()
            {
                BID = TestConstants.BID,
                ID = -100,
                OrderID = createdOrderItem.OrderID,
                OrderItemID = createdOrderItem.ID,
                Name = "New Test Component",
                TotalQuantity = 1,
                PriceUnit = 10,
                CostUnit = 10 / 2,
                IncomeAccountID = incomeAccount.ID,
                IncomeAllocationType = AssemblyIncomeAllocationType.SingleIncomeAllocation
            };
        }

        public static OrderNote GetTestOrderNoteData(OrderData createOrder)
        {
            return new OrderNote()
            {
                BID = TestConstants.BID,
                ModifiedDT = DateTime.Now,
                OrderID = createOrder.ID,
                NoteType = OrderNoteType.Production,
                Note = "Test data for notes",
                IsActive = true
            };
        }

        public static OrderOrderLink GetOrderOrderLink(TransactionHeaderData createdOrder, TransactionHeaderData clonedOrder)
        {
            var result = new OrderOrderLink()
            {
                BID = createdOrder.BID,
                Description = $"{OrderOrderLinkType.ClonedTo.ToString()} {clonedOrder.FormattedNumber}",
                OrderID = createdOrder.ID,
                LinkedOrderID = clonedOrder.ID,
                LinkedFormattedNumber = clonedOrder.FormattedNumber,
            };

            return result;
        }

        internal static async Task<List<SimpleEmployeeData>> GetEmployees(HttpClient client)
        {
            return (await EndpointTests.AssertGetStatusCode<SimpleEmployeeData[]>(client, "api/employee/simplelist", HttpStatusCode.OK)).ToList();
        }

        public static OrderContactRole GetOrderContactRole()
        {
            var result = new OrderContactRole()
            {
                BID = TestConstants.BID,
                ContactName = "Test Contact 1",
                RoleType = OrderContactRoleType.Primary,
            };

            return result;
        }

        public static OrderEmployeeRole GetOrderEmployeeRole(EmployeeData employeeData = null)
        {
            var result = new OrderEmployeeRole()
            {
                EmployeeID = employeeData?.ID ?? 101,
                RoleID = 1,
            };

            return result;
        }

        public static async Task<int> GetCompanyID(HttpClient client)
        {
            return (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
        }

        public static async Task<int> GetContactID(HttpClient client, int? companyId = null)
        {
            if (companyId.HasValue)
                return (await EndpointTests.AssertGetStatusCode<SimpleContactData[]>(client, $"api/contact/simplelist?ParentID={companyId.Value}", HttpStatusCode.OK)).FirstOrDefault().ID;

            return (await EndpointTests.AssertGetStatusCode<SimpleContactData[]>(client, "api/contact/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
        }

        public static async Task<int> GetEmployeeID(HttpClient client)
        {
            return (await EndpointTests.AssertGetStatusCode<SimpleEmployeeData[]>(client, "api/employee/simplelist?IsActive=true", HttpStatusCode.OK)).FirstOrDefault().ID;
        }

        public static async Task<byte> GetLocationID(HttpClient client)
        {
            return (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
        }

        public static async Task<short> GetTaxGroupID(HttpClient client)
        {
            return (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
        }

        #region CustomFieldDefinitions
        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static CustomFieldDefinition GetCustomFieldDefinition()
        {
            return new CustomFieldDefinition()
            {
                ClassTypeID = (int)ClassType.CustomFieldDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                AppliesToClassTypeID = (int)ClassType.Contact,
                AppliesToID = null,
                Name = "TestCFD " + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                Label = "Test Custom Field Label",
                Description = "Test Custom Field Desc",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DisplayType = CustomFieldNumberDisplayType.General,
                DisplayFormatString = "",
                AllowMultipleValues = false,
                HasValidators = false
            };
        }
        #endregion

        #region CustomFieldLayoutDefinitions
        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static CustomFieldLayoutDefinition GetCustomFieldLayoutDefinition()
        {
            return new CustomFieldLayoutDefinition()
            {
                ClassTypeID = (int)ClassType.CustomFieldLayoutDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                AppliesToClassTypeID = (int)ClassType.Company,
                IsAllTab = false,
                IsSubTab = true,
                SortIndex = 1,
                Name = "Test Custom Field Layout Definition" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
            };
        }

        public static CustomFieldLayoutDefinition GetCustomFieldLayoutDefinitionTree()
        {
            return new CustomFieldLayoutDefinition()
            {
                ClassTypeID = (int)ClassType.CustomFieldLayoutDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                AppliesToClassTypeID = (int)ClassType.Company,
                IsAllTab = false,
                IsSubTab = true,
                SortIndex = 1,
                Name = "Test Custom Field Layout Definition-" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                Elements = new CustomFieldLayoutElement[] {
                    new CustomFieldLayoutElement()
                    {
                        Name = "Section",
                        SortIndex = 1,
                        ElementType = AssemblyElementType.Section,
                        Elements = new CustomFieldLayoutElement[]
                        {
                            new CustomFieldLayoutElement()
                            {
                                Name = "Test Custom Field Layout Definition Element 1-" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                                SortIndex = 1,
                                DataType = (short) DataType.Boolean
                            },
                            new CustomFieldLayoutElement()
                            {
                                Name = "Test Custom Field Layout Definition Element 2-" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                                SortIndex = 2,
                                DataType = (short) DataType.Boolean
                            }
                        }
                    },
                    new CustomFieldLayoutElement()
                    {
                        Name = "Section",
                        SortIndex = 2,
                        ElementType = AssemblyElementType.Section,
                    }
                },
            };
        }

        public static CustomFieldLayoutDefinition GetCustomFieldLayoutDefinitionTreeWithOneContainerAndOneElement()
        {
            return new CustomFieldLayoutDefinition()
            {
                ClassTypeID = (int)ClassType.CustomFieldLayoutDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                AppliesToClassTypeID = (int)ClassType.Company,
                IsAllTab = false,
                IsSubTab = true,
                SortIndex = 1,
                Name = "Test Custom Field Layout Definition-" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                Elements = new CustomFieldLayoutElement[] {
                    new CustomFieldLayoutElement()
                    {
                        Name = "Section",
                        ElementType = AssemblyElementType.Section,
                        SortIndex = 1,
                        Elements = new CustomFieldLayoutElement[]
                        {
                            new CustomFieldLayoutElement()
                            {
                                Name = "Test Custom Field Layout Definition Element 1-" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                                SortIndex = 1,
                                DataType = (short) DataType.Boolean
                            },
                        }
                    },
                },
            };
        }

        #endregion

        public static DashboardData GetTestDashboard(string name = "Dashboard.UnitTest")
        {
            return new DashboardData()
            {
                BID = 1,
                ClassTypeID = ClassType.Dashboard.ID(),
                IsActive = true,
                ModifiedDT = DateTime.UtcNow,
                Module = Module.Sales,
                Name = name,
                UserLinkID = 1
            };
        }

        public static DashboardWidgetData GetTestDashboardWidget(string name = "Dashboard.Widget.UnitTest", short widgetDefinitionID = 1, short dashboardID = 1)
        {
            return new DashboardWidgetData()
            {
                BID = 1,
                IsActive = true,
                Name = name,
                WidgetDefinitionID = widgetDefinitionID,
                DashboardID = dashboardID
            };
        }

        public static async Task<UserLink> GetAuthUserLink(ApiContext ctx)
        {
            UserLink testUserLink = await ctx.UserLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ID == TestConstants.AuthUserLinkID);

            if (testUserLink == null)
            {
                testUserLink = new UserLink()
                {
                    BID = TestConstants.BID,
                    ID = TestConstants.AuthUserLinkID,
                    AuthUserID = TestConstants.AuthUserID,
                    EmployeeID = ctx.EmployeeData.Where(e => e.BID == TestConstants.BID && e.IsActive && e.ID > 0).OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault(),
                    UserAccessType = UserAccessType.SupportManager
                };

                ctx.UserLink.Add(testUserLink);
                await ctx.SaveChangesAsync();
            }

            return testUserLink;
        }

        public static async Task<T> FirstOrAddAsync<T>(this Microsoft.EntityFrameworkCore.DbSet<T> dbSet, System.Linq.Expressions.Expression<Func<T, bool>> predicate, Func<T> ctor)
            where T : class
        {
            T entity = await dbSet.Where(predicate).FirstOrDefaultAsync();

            if (entity == null)
            {
                entity = ctor();

                await dbSet.AddAsync(entity);
            }

            return entity;
        }


        public static async Task TestJSONRelationshipProperties<T>(HttpClient client, string entityBaseUrl, T entityID, List<string> relationshipObjectNames)
        {
            string url = $"{entityBaseUrl}/{entityID}";
            string serializedBody = await client.GetStringAsync(url);
            int remainingRelationObjectCount = relationshipObjectNames.Count;
            foreach (var item in JObject.Parse(serializedBody))
            {
                if (item.Value is JArray || item.Value is JObject)
                {
                    if (relationshipObjectNames.Contains(item.Key))
                        remainingRelationObjectCount--;
                    else
                    {
                        if ((item.Value as JArray) != null)
                        {
                            CollectionAssert.AreEquivalent(EmptyJArray, (item.Value as JArray));
                        }
                        else
                        {
                            Assert.AreEqual(EmptyJObject, (item.Value as JObject));
                        }
                    }
                }
            }
            Assert.AreEqual(0, remainingRelationObjectCount);
        }

    }
}
