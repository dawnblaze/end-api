﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class PartCostingMethodEndpointTests
    {
        public const string apiUrl = "/api/partcostingmethod";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task TestGetAllPartCostingMethodsTest()
        {
            var partCostingMethods = await EndpointTests.AssertGetStatusCode<List<EnumMaterialCostingMethod>>(client, apiUrl, HttpStatusCode.OK);
            Assert.IsNotNull(partCostingMethods);
            Assert.AreEqual(Enum.GetValues(typeof(MaterialCostingMethod)).Length, partCostingMethods.Count);
        }
    }
}
