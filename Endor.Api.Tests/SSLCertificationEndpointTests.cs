﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using System.IO;
using Endor.DocumentStorage.Models;
using Endor.AzureStorage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Collections.Generic;

namespace Endor.Api.Tests
{
    [TestClass]
    public class SSLCertificationEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/Api/SSLCertificate";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string newSslCertName;
        public TestContext TestContext { get; set; }
        public MockTenantDataCache cache { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            this.newSslCertName = "TEST SSL CERT COMMON NAME";
            this.cache = new MockTenantDataCache(_config[SQLConfigName], _config[BlobStorageConfigName]);
        }

        [TestMethod]
        public async Task TestSSLCertificateCRUD()
        {
            short testBID = 1;
            var fileName = "F1FD2D7AB6D9FD06FDEF1AD5FB68D9158A7723C1-EndorDev-CentralUSwebspace";
            const string pwdKey = "metadata1";
            var cert = File.ReadAllBytes("..\\..\\..\\Cyriousapp9432.pfx");
            Assert.IsNotNull(cert);

            // need to upload cert to dm
            var sClient = new EntityStorageClient((await this.cache.Get(testBID)).StorageConnectionString, testBID);
            await sClient.AddFile(new MemoryStream(cert), StorageBin.Permanent, Bucket.Documents, new DMID { ctid = (int)ClassType.Business, id = testBID }, fileName, "application/x-pkcs12", 1, 1000, false, null, new Dictionary<string, string>() { { pwdKey, "Cyriousapp9432" } });

            var sslCertificate = new SSLCertificateData()
            {
                BID = testBID,
                ID = 1,
                ClassTypeID = ClassType.SSLCertificate.ID(),
                CanHaveMultipleDomains = false,
                CommonName = newSslCertName,
                FileName = fileName,
                Thumbprint = Guid.NewGuid().ToString(),
                ModifiedDT = DateTime.UtcNow,
                InstalledDT = DateTime.UtcNow,
                ValidFromDT = DateTime.UtcNow,
                ValidToDT = DateTime.UtcNow.AddMonths(3)
            };

            // create
            var postResponse = await EndpointTests.AssertPostStatusCode<SSLCertificateData>(client, $"{apiUrl}", JsonConvert.SerializeObject(sslCertificate), HttpStatusCode.OK);
            // retrieve
            var getResponse = await EndpointTests.AssertGetStatusCode<SSLCertificateData>(client, $"{apiUrl}/{postResponse.ID}", HttpStatusCode.OK);
            var getSimpleResponse = await EndpointTests.AssertGetStatusCode<SimpleListItem<short>[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            var getAllResponse = await EndpointTests.AssertGetStatusCode<SSLCertificateData[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            // update
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{getResponse.ID}", JsonConvert.SerializeObject(postResponse), HttpStatusCode.NotFound);
            // delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{getResponse.ID}", HttpStatusCode.NotFound);
        }

        [TestCleanup]
        public void TearDown()
        {
            var ctx = GetApiContext();
            var testSslCert = ctx.SSLCertificateData.Where(x => x.CommonName == newSslCertName).FirstOrDefault();
            if (testSslCert != null)
            {
                ctx.SSLCertificateData.Remove(testSslCert);
                ctx.SaveChanges();
            }
        }
    }
}
