﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Endor.Api.Tests
{
    [TestClass]
    public class OrderItemChildrenEndpointTests
    {
        public const string apiUrl = "/Api/OrderItem";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        private int testOrderId { get; set; }
        private short testTaxGroupID { get; set; }
        private byte testProductionLocationID { get; set; }

        private OrderItemData GetTestOrderItemData()
        {
            return new OrderItemData()
            {
                BID = 1,
                HasCustomImage = false,
                HasDocuments = false,
                HasProof = false,
                ID = 1,
                OrderID = testOrderId,
                ItemNumber = 1,
                Quantity = 1,
                Name = "test",
                IsOutsourced = false,
                OrderStatusID = OrderOrderStatus.OrderInvoiced,
                ItemStatusID = 18,
                ProductionLocationID = testProductionLocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = testTaxGroupID,
                TaxGroupOV = false,
                IsTaxExempt = false,
                CategoryID = 101
            };
        }

        [TestMethod]
        public async Task TestOrderItemContactRolesCRUDOperations()
        {
            #region Create Order and OrderItem

            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrderItem = Utils.GetTestOrderItemData(testOrder);

            #endregion

            #region CREATE OrderContactRole

            var testContactRole = new OrderContactRole()
            {
                BID = 1,
                ClassTypeID = (int)ClassType.OrderContactRole,
                OrderID = testOrder.ID,
                OrderItemID = createdOrderItem.ID,
                RoleType = OrderContactRoleType.Primary
            };
            var simpleContacts = await EndpointTests.AssertGetStatusCode<SimpleContactData[]>(client, "Api/Contact/SimpleList", HttpStatusCode.OK);
            testContactRole.ContactID = simpleContacts[0].ID;
            var createdContactRole = await EndpointTests.AssertPostStatusCode<OrderContactRole>(client, $"{apiUrl}/{createdOrderItem.ID}/ContactRole", JsonConvert.SerializeObject(testContactRole), HttpStatusCode.OK);

            #endregion

            #region RETRIEVE

            // without ContactRoles
            var retrievedOrderItem = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderItem);
            Assert.IsNull(retrievedOrderItem.ContactRoles);

            // with ContactRoles
            retrievedOrderItem = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}?ContactLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderItem);
            Assert.IsNotNull(retrievedOrderItem.ContactRoles);

            // get just the ContactRoles by OrderItem's ID
            var retrievedContactRoles = await EndpointTests.AssertGetStatusCode<OrderContactRole[]>(client, $"{apiUrl}/{createdOrderItem.ID}/ContactRole", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedContactRoles);
            Assert.AreEqual(true, (retrievedContactRoles.Count() > 0));

            // get just the ContactRoles by OrderItem's ID and ContactRole's ID
            var retrievedContactRole = await EndpointTests.AssertGetStatusCode<OrderContactRole>(client, $"{apiUrl}/{createdOrderItem.ID}/ContactRole/{createdContactRole.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedContactRole);
            
            // check NotFound responses
            int invalidID = -1;
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{invalidID}/ContactRole", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{invalidID}/ContactRole/{createdContactRole.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}/ContactRole/{invalidID}", HttpStatusCode.NotFound);

            #endregion

            #region UPDATE

            if (simpleContacts.Count() > 1)
                createdContactRole.ContactID = simpleContacts[1].ID;

            var testPut = await EndpointTests.AssertPutStatusCode<OrderContactRole>(client, $"{apiUrl}/{createdOrderItem.ID}/ContactRole/{createdContactRole.ID}", JsonConvert.SerializeObject(createdContactRole), HttpStatusCode.OK);
            Assert.IsNotNull(testPut);
            Assert.AreEqual(createdContactRole.ContactID, testPut.ContactID);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}/ContactRole/{createdContactRole.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/Api/Order/{testOrder.ID}", HttpStatusCode.NoContent);

            #endregion
        }

        [TestMethod]
        public async Task TestOrderItemEmployeeRolesCRUDOperations()
        {
            #region Create Order and OrderItem

            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;
            var createdOrderItem = Utils.GetTestOrderItemData(testOrder);

            #endregion

            #region CREATE OrderEmployeeRole

            var testEmployeeRole = new OrderEmployeeRole()
            {
                BID = 1,
                ClassTypeID = (int)ClassType.OrderEmployeeRole,
                OrderID = testOrder.ID,
                OrderItemID = createdOrderItem.ID,
                RoleID = 1
            };
            var simpleEmployees = await EndpointTests.AssertGetStatusCode<SimpleEmployeeData[]>(client, "Api/Employee/SimpleList", HttpStatusCode.OK);
            testEmployeeRole.EmployeeID = simpleEmployees[0].ID;
            var createdEmployeeRole = await EndpointTests.AssertPostStatusCode<OrderEmployeeRole>(client, $"{apiUrl}/{createdOrderItem.ID}/EmployeeRole", JsonConvert.SerializeObject(testEmployeeRole), HttpStatusCode.OK);

            #endregion

            #region RETRIEVE

            // without EmployeeRoles
            var retrievedOrderItem = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderItem);
            Assert.IsNull(retrievedOrderItem.EmployeeRoles);

            // with EmployeeRoles
            retrievedOrderItem = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}?EmployeeLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderItem);
            Assert.IsNotNull(retrievedOrderItem.EmployeeRoles);

            // get just the EmployeeRoles by OrderItem's ID
            var retrievedEmployeeRoles = await EndpointTests.AssertGetStatusCode<OrderEmployeeRole[]>(client, $"{apiUrl}/{createdOrderItem.ID}/EmployeeRole", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedEmployeeRoles);
            Assert.AreEqual(true, (retrievedEmployeeRoles.Count() > 0));

            // get just the EmployeeRoles by OrderItem's ID and EmployeeRole's ID
            var retrievedEmployeeRole = await EndpointTests.AssertGetStatusCode<OrderEmployeeRole>(client, 
                $"{apiUrl}/{createdOrderItem.ID}/EmployeeRole/{createdEmployeeRole.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedEmployeeRole);

            // check NotFound responses
            int invalidID = -1;
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{invalidID}/EmployeeRole", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{invalidID}/EmployeeRole/{createdEmployeeRole.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}/EmployeeRole/{invalidID}", HttpStatusCode.NotFound);

            #endregion

            #region UPDATE

            if (simpleEmployees.Count() > 1)
                createdEmployeeRole.EmployeeID = simpleEmployees[1].ID;

            var testPut = await EndpointTests.AssertPutStatusCode<OrderEmployeeRole>(client, $"{apiUrl}/{createdOrderItem.ID}/EmployeeRole/{createdEmployeeRole.ID}", JsonConvert.SerializeObject(createdEmployeeRole), HttpStatusCode.OK);
            Assert.IsNotNull(testPut);
            Assert.AreEqual(createdEmployeeRole.EmployeeID, testPut.EmployeeID);

            var updatedEmployeeRole = await EndpointTests.AssertGetStatusCode<OrderEmployeeRole>(client, $"{apiUrl}/{createdOrderItem.ID}/employeerole/{createdEmployeeRole.ID}", HttpStatusCode.OK);
            Assert.AreEqual(createdEmployeeRole.EmployeeID, updatedEmployeeRole.EmployeeID);
            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}/EmployeeRole/{createdEmployeeRole.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/Api/Order/{testOrder.ID}", HttpStatusCode.NoContent);

            #endregion
        }
    }
}