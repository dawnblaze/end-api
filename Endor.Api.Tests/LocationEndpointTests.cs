﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using Endor.Api.Web.Controllers;
using Endor.Api.Web.Services;
using AssertX = Xunit.Assert;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    [TestClass]
    public class LocationsEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/Location";
        private const string TestLocationName = "Create Location Test 123";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestInitialize]
        public async Task Initialize()
        {
            short testBID = 1;
            using (var ctx = GetApiContext(testBID))
            {
                var emailAccounts = await ctx.EmailAccountData.Where(x => x.BID == testBID && (x.ID < 0 || x.DomainEmailID < 0)).ToListAsync();
                if (emailAccounts.Count > 0)
                    ctx.RemoveRange(emailAccounts);

                var emailDomains = await ctx.DomainEmail.Where(d => d.BID == testBID && d.ID < 0).ToListAsync();
                if (emailDomains.Count > 0)
                    ctx.RemoveRange(emailDomains);

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task LocationDefaultEmailAccountTests()
        {
            using (var ctx = GetApiContext(1))
            {
                if (!ctx.DomainEmail.Where(d => d.BID == 1 && d.ID == -99).Any())
                {
                    //insert
                    var domainEmail = Utils.GetCorebridgeDomainEmail();
                    domainEmail.ID = -99;
                    ctx.DomainEmail.Add(domainEmail);
                    ctx.SaveChanges();
                }

                var newEmailAccount = Utils.GetEmailAccount(1, -99, -99);
                newEmailAccount.StatusType = EmailAccountStatus.Authorized;
                newEmailAccount.IsActive = true;
                newEmailAccount.EmployeeID = ctx.EmployeeData.First().ID;
                ctx.EmailAccountData.Add(newEmailAccount);
                ctx.SaveChanges();
                var location = ctx.LocationData.First();

                var resp = await EndpointTests.AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/0/action/setdefault/emailaccount/{newEmailAccount.ID}", null, HttpStatusCode.NotFound);
                Assert.IsNotNull(resp);
                Assert.IsFalse(resp.Success);
                resp = await EndpointTests.AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/{location.ID}/action/setdefault/emailaccount/-32768", null, HttpStatusCode.NotFound);
                Assert.IsNotNull(resp);
                Assert.IsFalse(resp.Success);
                resp = await EndpointTests.AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/{location.ID}/action/setdefault/emailaccount/{newEmailAccount.ID}", null, HttpStatusCode.OK);
                Assert.IsNotNull(resp);
                Assert.IsTrue(resp.Success);
                resp = await EndpointTests.AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/{location.ID}/action/cleardefault/emailaccount", null, HttpStatusCode.OK);
                Assert.IsNotNull(resp);
                Assert.IsTrue(resp.Success);
                resp = await EndpointTests.AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/0/action/cleardefault/emailaccount", null, HttpStatusCode.NotFound);
                Assert.IsNotNull(resp);
                Assert.IsFalse(resp.Success);

                newEmailAccount.StatusType = EmailAccountStatus.Inactive;
                ctx.EmailAccountData.Update(newEmailAccount);
                await ctx.SaveChangesAsync();

                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{location.ID}/action/setdefault/emailaccount/{newEmailAccount.ID}", null, HttpStatusCode.Forbidden);
            }
        }

        [TestMethod]
        public async Task TestLocationsEndpoints()
        {
            // GET api/location/{id}
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/1", HttpStatusCode.OK);

            // GET api/location/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            // confirm that a location of name "Create Location Test" doesn't already exist in the db
            var sld = simpleList.FirstOrDefault(s => s.DisplayName == TestLocationName);
            var locationController = GetAtomCRUDController<LocationController, LocationData, LocationService, byte>(1);
            if (sld != null)
            {
                // remove "Create Location Test" record
                AssertX.IsType<NoContentResult>(await locationController.Delete(sld.ID));
                // confirm location was deleted
                await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{sld.ID}", HttpStatusCode.NotFound);
            }

            var loc = new LocationData()
            {
                BID = 1,
                Name = TestLocationName,
                LegalName = "createLocationTest",
                IsActive = false,
                IsDefault = false
            };
            // POST api/location/
            var newLoc = await EndpointTests.AssertPostStatusCode<LocationData>(client, apiUrl, JsonConvert.SerializeObject(loc), HttpStatusCode.OK);

            // PUT api/location/{id}
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{newLoc.ID}", newLoc, HttpStatusCode.OK);

            // remove newly created location
            AssertX.IsType<NoContentResult>(await locationController.Delete(newLoc.ID));
            // confirm location was deleted
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newLoc.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task TestLocationNumber()
        {
            var ctx = GetApiContext();
            ctx.RemoveRange(ctx.UserLink.Where(l => l.ID == -99));
            ctx.SaveChanges();
            var newUserLink = new UserLink()
            {
                ID = -99,
                BID = TestConstants.BID,
                ClassTypeID = 0,
                EmployeeID = ctx.EmployeeData.First().ID,
                UserAccessType = UserAccessType.Employee,
                UserName = "TestUser"
            };
            ctx.UserLink.Add(newUserLink);
            ctx.SaveChanges();

            ctx.LocationData.RemoveRange(ctx.LocationData.Where(l => l.Name == TestLocationName));
            ctx.SaveChanges();
            var loc = new LocationData()
            {
                BID = 1,
                Name = TestLocationName,
                LegalName = "createLocationTest",
                IsActive = false,
                IsDefault = false,
                LocationNumber = "1416"
            };
            // POST api/location/
            var newLoc = await EndpointTests.AssertPostStatusCode<LocationData>(client, apiUrl, JsonConvert.SerializeObject(loc), HttpStatusCode.OK);
            client = EndpointTests.GetHttpClient(AuthUserID: -99);

            newLoc.LocationNumber = "146";
            // PUT api/location/{id}
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{newLoc.ID}", newLoc, HttpStatusCode.BadRequest);

            ctx.RemoveRange(ctx.UserLink.Where(l => l.ID == -99));
            ctx.SaveChanges();
            newUserLink = new UserLink()
            {
                ID = -99,
                BID = TestConstants.BID,
                ClassTypeID = 0,
                EmployeeID = ctx.EmployeeData.First().ID,
                UserAccessType = UserAccessType.SupportManager,
                UserName = "TestUser"
            };
            ctx.UserLink.Add(newUserLink);
            ctx.SaveChanges();
            client = EndpointTests.GetHttpClient(AuthUserID: -99);
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{newLoc.ID}", newLoc, HttpStatusCode.OK);

            var locationController = GetAtomCRUDController<LocationController, LocationData, LocationService, byte>(1);
            // remove newly created location
            AssertX.IsType<NoContentResult>(await locationController.Delete(newLoc.ID));
        }

        [TestMethod]
        public async Task TestLocationDeleteWithForceEndpoint()
        {
            var ctx = GetApiContext();

            var testUserLink = await Utils.GetAuthUserLink(ctx);
            if (testUserLink != null && testUserLink.UserAccessType < UserAccessType.SupportManager)
            {
                testUserLink.UserAccessType = UserAccessType.SupportManager;
                ctx.SaveChanges();
            }

            const string locTestName = "New Location - Unit Test";
            LocationData newLoc =
                await ctx.LocationData.Where(x => x.BID == TestConstants.BID && x.ID == 255 && x.Name == locTestName).FirstOrDefaultAsync();

            if (newLoc == null)
            {
                newLoc = new LocationData()
                {
                    ID = 255,
                    BID = TestConstants.BID,
                    Name = locTestName,
                    LegalName = "newLocationUnitTest",
                    IsActive = false,
                    IsDefault = false
                };

                await ctx.LocationData.AddAsync(newLoc);
            }

            TaxGroupLocationLink taxGroupLocationLink = await ctx.TaxGroupLocationLink.Where(x => x.BID == TestConstants.BID && x.LocationID == newLoc.ID).FirstOrDefaultAsync();
            if (taxGroupLocationLink == null)
            {
                taxGroupLocationLink = new TaxGroupLocationLink
                {
                    BID = TestConstants.BID,
                    GroupID = (await ctx.TaxGroup.FirstAsync(x => x.BID == TestConstants.BID)).ID,
                    LocationID = newLoc.ID
                };

                await ctx.TaxGroupLocationLink.AddAsync(taxGroupLocationLink);
            }

            DomainData domainData = await ctx.DomainData.Where(x => x.BID == TestConstants.BID && x.LocationID == newLoc.ID).FirstOrDefaultAsync();

            if (domainData == null)
            {
                domainData = new DomainData()
                {
                    ID = -99,
                    BID = TestConstants.BID,
                    IsDefault = true,
                    AccessType = DomainAccessType.Business,
                    LocationID = newLoc.ID,
                    Status = DomainStatus.Pending,
                    Domain = "testing.testing123.com"
                };

                await ctx.DomainData.AddAsync(domainData);
            }

            DomainEmailLocationLink domainEmailLocationLink = await ctx.DomainEmailLocationLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.LocationID == newLoc.ID);
            if (domainEmailLocationLink == null)
            {
                domainEmailLocationLink = new DomainEmailLocationLink
                {
                    BID = TestConstants.BID,
                    DomainID = (await ctx.DomainEmail.FirstAsync(x => x.BID == TestConstants.BID)).ID,
                    LocationID = newLoc.ID
                };
                await ctx.DomainEmailLocationLink.AddAsync(domainEmailLocationLink);
            }

            EmployeeTeamLocationLink employeeTeamLocationLink = await ctx.EmployeeTeamLocationLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.LocationID == newLoc.ID);
            if (employeeTeamLocationLink == null)
            {
                employeeTeamLocationLink = new EmployeeTeamLocationLink
                {
                    BID = TestConstants.BID,
                    TeamID = (await ctx.EmployeeTeam.FirstAsync(x => x.BID == TestConstants.BID)).ID,
                    LocationID = newLoc.ID
                };
                await ctx.EmployeeTeamLocationLink.AddAsync(employeeTeamLocationLink);
            }

            LocationGoal locationGoal = await ctx.LocationGoal.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.LocationID == newLoc.ID);
            if (locationGoal == null)
            {
                locationGoal = new LocationGoal()
                {
                    BID = TestConstants.BID,
                    ID = -99,
                    Year = 2018,
                    Actual = 10000.00m,
                    LocationID = newLoc.ID,
                    Month = 1,
                    IsYearlyTotal = true
                };
                await ctx.LocationGoal.AddAsync(locationGoal);
            }

            LocationLocator locationLocator = await ctx.LocationLocator.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ParentID == newLoc.ID);
            if (locationLocator == null)
            {
                locationLocator = new LocationLocator()
                {
                    BID = TestConstants.BID,
                    ID = -99,
                    ParentID = newLoc.ID,
                    LocatorType = 2,
                    Locator = "(801) 836-7117",
                    RawInput = "8018367117",
                    SortIndex = 1,
                    IsVerified = false,
                    IsValid = true,
                };
                await ctx.LocationLocator.AddAsync(locationLocator);
            }


            await ctx.SaveChangesAsync();

            var test = await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{newLoc.ID}?force=true", HttpStatusCode.OK);

            Assert.IsNull(await ctx.LocationLocator.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ParentID == newLoc.ID));
            Assert.IsNull(await ctx.LocationGoal.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.LocationID == newLoc.ID));
            Assert.IsNull(await ctx.EmployeeTeamLocationLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.LocationID == newLoc.ID));
            Assert.IsNull(await ctx.DomainEmailLocationLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.LocationID == newLoc.ID));
            Assert.IsNull(await ctx.DomainData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.LocationID == newLoc.ID));
            Assert.IsNull(await ctx.TaxGroupLocationLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.LocationID == newLoc.ID));
            Assert.IsNull(await ctx.LocationData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ID == newLoc.ID));
        }

        [TestCleanup]
        public void Teardown()
        {
            try
            {
                var ctx = GetApiContext();
                ctx.RemoveRange(ctx.LocationData.Where(x => x.Name == TestLocationName));
                ctx.SaveChanges();
            }
            catch { }
        }
    }
}
