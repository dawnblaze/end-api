﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Tests
{
    [TestClass]
    public class UserLinkEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/userLink";
        private const string ForceDeleteUserLinkUsername = "TestContactDeleteWithForceEndpoint";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        
        [TestMethod]
        public async Task TestUserLinkDeleteWithForceEndpoint()
        {
            var ctx = GetApiContext();
            var testUserLink = await Utils.GetAuthUserLink(ctx);
            if (testUserLink != null && testUserLink.UserAccessType < UserAccessType.SupportManager)
            {
                testUserLink.UserAccessType = UserAccessType.SupportManager;
                ctx.SaveChanges();
            }

            var existingEmployee = await ctx.EmployeeData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID);
            Assert.IsNotNull(existingEmployee);

            UserLink newUserLink =
                await ctx.UserLink.Where(x => x.BID == TestConstants.BID && x.EmployeeID == existingEmployee.ID && x.UserName == ForceDeleteUserLinkUsername).FirstOrDefaultAsync();

            if (newUserLink == null)
            {
                newUserLink = new UserLink()
                {
                    ID = (short)((await ctx.UserLink.ToArrayAsync()).Max(x => x.ID) + 1),
                    BID = TestConstants.BID,
                    ClassTypeID = 0,
                    EmployeeID = existingEmployee.ID,
                    UserAccessType = UserAccessType.SupportManager,
                    UserName = ForceDeleteUserLinkUsername
                };
                ctx.UserLink.Add(newUserLink);
            }
            await ctx.SaveChangesAsync();

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{newUserLink.ID}?force=true", HttpStatusCode.OK);

            Assert.IsNull(await ctx.UserLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ID == newUserLink.ID));
        }
        

        [TestCleanup]
        public void Teardown()
        {
            try
            {
                var ctx = GetApiContext();
                ctx.RemoveRange(ctx.UserLink.Where(x => x.BID == TestConstants.BID && x.UserName == ForceDeleteUserLinkUsername));
                ctx.SaveChanges();
            }
            catch { }
        }
    }
}
