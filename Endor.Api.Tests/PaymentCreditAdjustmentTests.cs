﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Request;
using PaymentApplication = Endor.Models.PaymentApplication;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Classes.Responses;

namespace Endor.Api.Tests
{
    [TestClass]
    [TestCategory("GL")]
    [TestCategory("Payment")]
    public class PaymentCreditAdjustmentTests : PaymentTestHelper
    {
        public const string apiUrl = "/api/payment/creditadjustment";
        public const string paymentApiUrl = "/api/payment";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }
        private PaymentMaster _paymentMaster;

        [TestInitialize]
        public async Task Initialize()
        {
            await Teardown();
            var ctx = GetApiContext();
            var testOrder = GetNewOrder();
            testOrder.PaymentPaid = (decimal)testOrder.PriceProductTotal;
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            var paymentMethod = GetNewPaymentMethod();
            var paymentMaster = GetNewPaymentMaster(
                amount: (decimal)createdOrder.PriceProductTotal, //Full payment
                orderID: createdOrder.ID
            );
            ctx.PaymentMethod.Add(paymentMethod);
            ctx.PaymentMaster.Add(paymentMaster);
            this._paymentMaster = paymentMaster;
            ctx.SaveChanges();
            await CleanUpTestData();
        }

        [TestCleanup]
        public async Task Teardown()
        {
            var ctx = GetApiContext();
            var paymentApplicationCondition = ctx.PaymentApplication.Where(t => t.BID == 1 && t.MasterID == -99);
            var paymentMethodCondition = ctx.PaymentMethod.Where(t => t.BID == 1 && t.ID == -99);
            var paymentMasterCondition = ctx.PaymentMaster.Where(t => t.BID == 1 && t.ID == -99);
            ctx.PaymentApplication.RemoveRange(paymentApplicationCondition);
            ctx.PaymentMethod.RemoveRange(paymentMethodCondition);
            ctx.PaymentMaster.RemoveRange(paymentMasterCondition);
            ctx.SaveChanges();
            // cleanup order
            var order = ctx.OrderData.FirstOrDefault(t => t.BID == 1 && t.InvoiceNumber == -999);
            if (order != null)
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{order.ID}", HttpStatusCode.NoContent);
            }
            await CleanUpTestData();
        }

        private OrderData GetNewOrder()
        {
            var ctx = GetApiContext();
            var locationID = ctx.LocationData.First().ID;
            var companyID = ctx.CompanyData.First().ID;
            var taxGroupID = ctx.TaxGroup.First().ID;
            var order = Utils.GetTestOrderData();
            order.CompanyID = companyID;
            order.PickupLocationID = locationID;
            order.TaxGroupID = taxGroupID;
            order.ProductionLocationID = locationID;
            order.PriceProductTotal = 20m;
            order.InvoiceNumber = -999;
            return order;
        }

        private PaymentMethod GetNewPaymentMethod()
        {
            var ctx = GetApiContext();
            return new PaymentMethod()
            {
                BID = 1,
                ID = -99,
                IsActive = true,
                Name = "TestPaymentMethodForApplication",
                PaymentMethodType = PaymentMethodType.CreditCard,
                DepositGLAccountID = ctx.GLAccount.First().ID,
            };
        }

        private PaymentMaster GetNewPaymentMaster(decimal amount, int? orderID = null)
        {
            var ctx = GetApiContext();
            var masterID = -99;
            var locationID = ctx.LocationData.First().ID;
            var companyID = ctx.CompanyData.First().ID;
            return new PaymentMaster()
            {
                BID = 1,
                ID = masterID,
                LocationID = locationID,
                ReceivedLocationID = locationID,
                Amount = amount,
                RefBalance = 0,
                NonRefBalance = amount,
                ChangeInNonRefCredit = amount,
                ChangeInRefCredit = null,
                PaymentTransactionType = (byte)PaymentTransactionType.Credit_Gift,
                CompanyID = companyID,
                PaymentApplications = new List<PaymentApplication>()
                {
                    new PaymentApplication()
                    {
                        BID = 1,
                        ID = -99,
                        PaymentType = PaymentMethodType.Visa,
                        PaymentTransactionType = (byte)PaymentTransactionType.Credit_Gift,
                        CompanyID = companyID,
                        ApplicationGroupID = -99,
                        LocationID = locationID,
                        Amount = amount,
                        NonRefBalance = amount,
                        OrderID = orderID,
                        PaymentMethodID = 1
                    }
                }
            };
        }

        private PaymentCreditAdjustmentRequest GetNewRequest()
        {
            var ctx = GetApiContext();

            return new PaymentCreditAdjustmentRequest()
            {
                PaymentTransactionType = PaymentTransactionType.Credit_Adjustment,
                PaymentMethodId = 251,
                MasterPaymentID = -99,
                Amount = -2.05m,
                Applications = new List<Web.Classes.Request.PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest()
                    {
                        OrderID = this._paymentMaster.PaymentApplications.First().OrderID,
                        Amount = -2.05m
                    }
                }
            };
        }
        private PaymentCreditAdjustmentRequest GetCreditAdjustmentRequest(int companyID, int masterID, decimal amount)
        {
            return new PaymentCreditAdjustmentRequest()
            {
                PaymentTransactionType = PaymentTransactionType.Credit_Adjustment,
                PaymentMethodId = 251,
                MasterPaymentID = masterID,
                CompanyID = companyID,
                Amount = -amount,
                Applications = new List<PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest()
                    {
                        OrderID = null,
                        Amount = -amount,
                    },
                },
            };
        }

        [TestMethod]
        public async Task CreditAdjustmentShouldUpdateMastersNonRefBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: false
            );
            var request = GetCreditAdjustmentRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                amount: 70
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            Assert.AreEqual(30, master.NonRefBalance);
            Assert.AreEqual(30, master.ChangeInNonRefCredit);
        }



        [TestMethod]
        public async Task CreditAdjustmentShouldUpdatePaymentApplications()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: false
            );
            var request = GetCreditAdjustmentRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                amount: 70
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            master = ctx.PaymentMaster.Include(t => t.PaymentApplications).First(t => t.ID == master.ID);
            var creditApplication = master.PaymentApplications.First(t => t.PaymentTransactionType == (byte)PaymentTransactionType.Credit_Gift);
            var lasApplication = master.PaymentApplications.Last(t => t.PaymentTransactionType == (byte)PaymentTransactionType.Credit_Adjustment);
            // assert that first credit gift has been adjusted
            Assert.AreEqual(true, creditApplication.HasAdjustments);
            Assert.AreEqual(30, creditApplication.NonRefBalance);
            // asset that a new application has been added
            Assert.AreEqual(-70, lasApplication.Amount);
        }






        [TestMethod]
        public async Task CreditAdjustmentShouldUpdateCompanysNonRefBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: false
            );
            var request = GetCreditAdjustmentRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                amount: 70
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            Assert.AreEqual(30, company.NonRefundableCredit);
        }

        [TestMethod]
        public async Task CreditAdjustmentPullsFromOneTransaction()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var master = await CreatePaymentForCompany(
                ID: -99,
                companyID: company.ID,
                amount: 100m,
                isRefundable: false
            );
            var master2 = await CreatePaymentForCompany(
                ID: -98,
                companyID: company.ID,
                amount: 100m,
                isRefundable: false
            );
            var request = GetCreditAdjustmentRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                amount: 70
            );
            request.MasterPaymentID = null;
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            master2 = ctx.PaymentMaster.First(t => t.ID == master2.ID);
            Assert.AreEqual(30, master.NonRefBalance);
            Assert.AreEqual(30, master.ChangeInNonRefCredit);
            Assert.AreEqual(100, master2.NonRefBalance);
            Assert.AreEqual(100, master2.ChangeInNonRefCredit);

        }

        [TestMethod]
        public async Task CreditAdjustmentPullsTooMuch()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var master = await CreatePaymentForCompany(
                ID: -99,
                companyID: company.ID,
                amount: 100m,
                isRefundable: false
            );
            var request = GetCreditAdjustmentRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                amount: 130
            );
            request.MasterPaymentID = null;
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            Assert.AreEqual(100, master.NonRefBalance);
            Assert.AreEqual(100, master.ChangeInNonRefCredit);
        }

        [TestMethod]
        public async Task CreditAdjustmentPullsFromOneTransactionThenAnother()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var master = await CreatePaymentForCompany(
                ID: -99,
                companyID: company.ID,
                amount: 100m,
                isRefundable: false
            );
            var master2 = await CreatePaymentForCompany(
                ID: -98,
                companyID: company.ID,
                amount: 100m,
                isRefundable: false
            );
            var request = GetCreditAdjustmentRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                amount: 70
            );
            request.MasterPaymentID = null;
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            master2 = ctx.PaymentMaster.First(t => t.ID == master2.ID);
            Assert.AreEqual(30, master.NonRefBalance);
            Assert.AreEqual(30, master.ChangeInNonRefCredit);
            Assert.AreEqual(100, master2.NonRefBalance);
            Assert.AreEqual(100, master2.ChangeInNonRefCredit);

            var request2 = GetCreditAdjustmentRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                amount: 70
            );
            request2.MasterPaymentID = null;
            var result2 = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request2), HttpStatusCode.OK);
            Assert.IsNotNull(result2);
            ctx.Entry(master).Reload();
            ctx.Entry(master2).Reload();
            Assert.AreEqual(0, master.NonRefBalance);
            Assert.AreEqual(0, master.ChangeInNonRefCredit);
            Assert.AreEqual(60, master2.NonRefBalance);
            Assert.AreEqual(60, master2.ChangeInNonRefCredit);

        }

        [TestMethod]
        public async Task CreditAdjustmentPullsFromTwoTransactions()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var master = await CreatePaymentForCompany(
                ID: -99,
                companyID: company.ID,
                amount: 100m,
                isRefundable: false
            );
            var master2 = await CreatePaymentForCompany(
                ID: -98,
                companyID: company.ID,
                amount: 100m,
                isRefundable: false
            );
            var request = GetCreditAdjustmentRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                amount: 130
            );
            request.MasterPaymentID = null;
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            master2 = ctx.PaymentMaster.First(t => t.ID == master2.ID);
            Assert.AreEqual(0, master.NonRefBalance);
            Assert.AreEqual(0, master.ChangeInNonRefCredit);
            Assert.AreEqual(70, master2.NonRefBalance);
            Assert.AreEqual(70, master2.ChangeInNonRefCredit);

        }

    }
}
