﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

namespace Endor.Api.Tests
{
    [TestClass]
    public class SystemAssemblyVariableTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/SystemAssemblyVariable";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task SystemAssemblyVariableEndPointsTests()
        {
            // GET ../api/systemAssemblyVariable
            var variables = await EndpointTests.AssertGetStatusCode<SystemAssemblyVariable[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(variables);

            Assert.IsTrue(variables.Count() == 5);

            // GET ../api/systemAssemblyVariable/simplelist
            var simpleVar = await EndpointTests.AssertGetStatusCode<SimpleSystemAssemblyVariable[]>(client, $"{apiUrl}/simplelist", HttpStatusCode.OK);
            Assert.IsNotNull(simpleVar);

            foreach (var variable in variables)
            {
                // GET ../api/systemAssemblyVariable/{id}
                var apiVariable = await EndpointTests.AssertGetStatusCode<SystemAssemblyVariable>(client, $"{apiUrl}/{variable.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(apiVariable);

                if (apiVariable.ID.Equals(1))
                    Assert.IsTrue(apiVariable.VariableName.Equals("Quantity"));

                if (apiVariable.ID.Equals(2))
                    Assert.IsTrue(apiVariable.VariableName.Equals("Tier"));

                if (apiVariable.ID.Equals(6))
                    Assert.IsTrue(apiVariable.VariableName.Equals("Height"));

                if (apiVariable.ID.Equals(7))
                    Assert.IsTrue(apiVariable.VariableName.Equals("Width"));

                if (apiVariable.ID.Equals(8))
                    Assert.IsTrue(apiVariable.VariableName.Equals("Area"));
            }
        }
    }
}
