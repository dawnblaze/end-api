﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class DomainEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/domain";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Initialize()
        {
        }

        [TestMethod]
        public async Task DomainCRUD()
        {
            #region CREATE
            DomainData newDomain = new DomainData()
            {
                Domain = "test.something.us",
                LocationID = 1,
                Status = DomainStatus.Pending,
                AccessType = DomainAccessType.Business,
            };

            var domain = await EndpointTests.AssertPostStatusCode<DomainData>(client, $"{apiUrl}", JsonConvert.SerializeObject(newDomain), HttpStatusCode.OK);
            #endregion

            #region RETRIEVE
            await EndpointTests.AssertGetStatusCode<DomainData>(client, $"{apiUrl}/{domain.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode<DomainData[]>(client, $"{apiUrl}/", HttpStatusCode.OK);
            #endregion

            #region UPDATE

            domain.Domain = "foo.bar";
            await EndpointTests.AssertPutStatusCode<DomainData>(client, $"{apiUrl}/{domain.ID}", JsonConvert.SerializeObject(domain), HttpStatusCode.OK);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{domain.ID}", HttpStatusCode.NoContent);

            #endregion
        }

        [TestMethod]
        public async Task DomainActionsTests()
        {
            var ctx = GetApiContext();

            //clean up from previous test
            var entries = ctx.DomainData.Where(x => x.ID < -95);
            ctx.RemoveRange(entries);
            ctx.SaveChanges();


            const string testDomainUrl = "testing.testing123.com";
            const string testDomain2Url = "testing2.testing123.com";

            DomainData testDomain = new DomainData()
            {
                ID = -99,
                BID = TestConstants.BID,
                IsDefault = true,
                AccessType = DomainAccessType.Business,
                LocationID = (await ctx.LocationData.FirstAsync(l => l.BID == TestConstants.BID)).ID,
                Status = DomainStatus.Pending,
                Domain = testDomainUrl,
            };

            DomainData testDomain2 = new DomainData()
            {
                ID = -98,
                BID = TestConstants.BID,
                IsDefault = false,
                AccessType = DomainAccessType.Business,
                LocationID = (await ctx.LocationData.FirstAsync(l => l.BID == TestConstants.BID)).ID,
                Status = DomainStatus.Pending,
                Domain = testDomain2Url,
            };

            await ctx.DomainData.AddAsync(testDomain);
            await ctx.DomainData.AddAsync(testDomain2);

            await ctx.SaveChangesAsync();

            DomainData checkDomain;

            try
            {
                EntityActionChangeResponse resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{testDomain.ID}/action/setinactive", HttpStatusCode.OK);

                Assert.IsTrue(resp.Success);
                checkDomain = await EndpointTests.AssertGetStatusCode<DomainData>(client, $"{apiUrl}/{testDomain.ID}", HttpStatusCode.OK);

                Assert.AreEqual(DomainStatus.Inactive, checkDomain.Status);

                resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{testDomain.ID}/action/setactive", HttpStatusCode.OK);

                Assert.IsTrue(resp.Success);
                checkDomain = await EndpointTests.AssertGetStatusCode<DomainData>(client, $"{apiUrl}/{testDomain.ID}", HttpStatusCode.OK);

                Assert.AreEqual(DomainStatus.Pending, checkDomain.Status);


                resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{testDomain2.ID}/action/setdefault", HttpStatusCode.OK);
                Assert.IsTrue(resp.Success);

                checkDomain = await EndpointTests.AssertGetStatusCode<DomainData>(client, $"{apiUrl}/{testDomain.ID}", HttpStatusCode.OK);
                Assert.IsFalse(checkDomain.IsDefault);

                checkDomain = await EndpointTests.AssertGetStatusCode<DomainData>(client, $"{apiUrl}/{testDomain2.ID}", HttpStatusCode.OK);
                Assert.IsTrue(checkDomain.IsDefault);


                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/testavailability?domain={testDomainUrl}", HttpStatusCode.BadRequest);
                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/testavailability?domain={testDomain2Url}", HttpStatusCode.BadRequest);
                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/testavailability?domain=test.test.com", HttpStatusCode.OK);
            }
            finally
            {
                ctx.DomainData.Remove(testDomain);
                ctx.DomainData.Remove(testDomain2);

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task DomainSslRefreshStatusTest()
        {
            var ctx = GetApiContext();
            var sslCertificateData = new SSLCertificateData()
            {
                BID = TestConstants.BID,
                ID = -99,
                ValidFromDT = DateTime.UtcNow.AddMonths(-3),
                ValidToDT = DateTime.UtcNow.AddMonths(3),
                CommonName = "Test SSL Common",
                Thumbprint = "thumbprint",
                FileName = "Test SSL File",
                CanHaveMultipleDomains = false,
                InstalledDT = DateTime.UtcNow
            };

            var domainData = new DomainData()
            {
                ID = -99,
                BID = TestConstants.BID,
                IsDefault = true,
                AccessType = DomainAccessType.Business,
                LocationID = (await ctx.LocationData.FirstAsync(l => l.BID == TestConstants.BID)).ID,
                Status = DomainStatus.Pending,
                Domain = "v3app-dev.corebridge.net",
                SSLCertificateID = sslCertificateData.ID,
            };

            try
            {
                await ctx.SSLCertificateData.AddAsync(sslCertificateData);
                await ctx.DomainData.AddAsync(domainData);
                await ctx.SaveChangesAsync();

                // test success
                GenericResponse<DomainData> resp = await EndpointTests.AssertPostStatusCode<GenericResponse<DomainData>>(client, $"{apiUrl}/{domainData.ID}/Action/RefreshSslStatus", HttpStatusCode.OK);
                Assert.IsTrue(resp.Success);

                // test fail
                domainData.Domain = "test.fail.co";
                ctx.DomainData.Update(domainData);
                await ctx.SaveChangesAsync();
                resp = await EndpointTests.AssertPostStatusCode<GenericResponse<DomainData>>(client, $"{apiUrl}/{domainData.ID}/Action/RefreshSslStatus", HttpStatusCode.BadRequest);
                Assert.IsFalse(resp.Success);
            }
            finally
            {
                ctx.DomainData.Remove(domainData);
                ctx.SSLCertificateData.Remove(sslCertificateData);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task DomainDnsStatusTests()
        {
            var ctx = GetApiContext();

            DateTime startTime = DateTime.UtcNow;

            const string testDomainUrl = "testing.testing123.com";
            const string testDomain2Url = "v3app-test.cyriousapp.com";

            DomainData testDomain = new DomainData()
            {
                ID = -99,
                BID = TestConstants.BID,
                IsDefault = false,
                AccessType = DomainAccessType.Business,
                LocationID = (await ctx.LocationData.FirstAsync(l => l.BID == TestConstants.BID)).ID,
                Status = DomainStatus.Pending,
                Domain = testDomainUrl,
            };

            DomainData testDomain2 = new DomainData()
            {
                ID = -98,
                BID = TestConstants.BID,
                IsDefault = false,
                AccessType = DomainAccessType.Business,
                LocationID = (await ctx.LocationData.FirstAsync(l => l.BID == TestConstants.BID)).ID,
                Status = DomainStatus.Pending,
                Domain = testDomain2Url,
            };

            await ctx.DomainData.AddAsync(testDomain);
            await ctx.DomainData.AddAsync(testDomain2);

            await ctx.SaveChangesAsync();

            DomainData checkDomain;

            try
            {
                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{testDomain.ID}/action/refreshdnsstatus", HttpStatusCode.BadRequest);

                checkDomain = await EndpointTests.AssertGetStatusCode<DomainData>(client, $"{apiUrl}/{testDomain.ID}", HttpStatusCode.OK);

                Assert.AreEqual(DomainStatus.Inactive, checkDomain.Status);
                Assert.IsTrue(checkDomain.DNSLastAttemptDT >= startTime);
                Assert.IsNull(checkDomain.DNSVerifiedDT);

                DomainRefreshDnsResponse resp = await EndpointTests.AssertPostStatusCode<DomainRefreshDnsResponse>(client, $"{apiUrl}/{testDomain2.ID}/action/refreshdnsstatus", HttpStatusCode.OK);

                checkDomain = await EndpointTests.AssertGetStatusCode<DomainData>(client, $"{apiUrl}/{testDomain2.ID}", HttpStatusCode.OK);

                Assert.AreEqual(DomainStatus.Online, checkDomain.Status);
                Assert.IsTrue(checkDomain.DNSLastAttemptDT >= startTime);
                Assert.IsTrue(checkDomain.DNSVerifiedDT >= startTime);
            }
            finally
            {
                ctx.DomainData.Remove(testDomain);
                ctx.DomainData.Remove(testDomain2);

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task DomainRefreshStatusTest()
        {
            var ctx = GetApiContext();
            var sslCertificateData = new SSLCertificateData()
            {
                BID = TestConstants.BID,
                ID = -99,
                ValidFromDT = DateTime.UtcNow.AddMonths(-3),
                ValidToDT = DateTime.UtcNow.AddMonths(3),
                CommonName = "Test SSL Common",
                Thumbprint = "thumbprint",
                FileName = "Test SSL File",
                CanHaveMultipleDomains = false,
                InstalledDT = DateTime.UtcNow
            };

            var domainData = new DomainData()
            {
                ID = -99,
                BID = TestConstants.BID,
                IsDefault = true,
                AccessType = DomainAccessType.Business,
                LocationID = (await ctx.LocationData.FirstAsync(l => l.BID == TestConstants.BID)).ID,
                Status = DomainStatus.Pending,
                Domain = "v3app-test7.cyriousapp.com",
                SSLCertificateID = sslCertificateData.ID,
            };

            try
            {
                await ctx.SSLCertificateData.AddAsync(sslCertificateData);
                await ctx.DomainData.AddAsync(domainData);
                await ctx.SaveChangesAsync();

                // test success
                GenericResponse<DomainData> resp = await EndpointTests.AssertPostStatusCode<GenericResponse<DomainData>>(client, $"{apiUrl}/{domainData.ID}/Action/RefreshStatus", HttpStatusCode.OK);
                Assert.IsTrue(resp.Success);

                // test fail
                domainData.Domain = "test.fail.co";
                ctx.DomainData.Update(domainData);
                await ctx.SaveChangesAsync();
                resp = await EndpointTests.AssertPostStatusCode<GenericResponse<DomainData>>(client, $"{apiUrl}/{domainData.ID}/Action/RefreshStatus", HttpStatusCode.BadRequest);
                Assert.IsFalse(resp.Success);
            }
            finally
            {
                ctx.DomainData.Remove(domainData);
                ctx.SSLCertificateData.Remove(sslCertificateData);
                await ctx.SaveChangesAsync();
            }
        }

    }
}
