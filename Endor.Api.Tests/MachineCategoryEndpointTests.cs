﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    [TestClass]
    public class MachineCategoryEndpointTests
    {
        public const string apiUrl = "/api/machinecategory";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestMachineDataName;
        private string NewTestMachineCategoryName;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            this.NewTestMachineDataName = DateTime.UtcNow + " TEST PART MACHINE DATA";
            this.NewTestMachineCategoryName = DateTime.UtcNow + " TEST PART MACHINE DATA";
        }

        [TestMethod]
        public async Task TestMachineCategorySingleEndpoints()
        {
            MachineCategory testObject = await GetPostAssertNewMachineCategory();

            // GET api/PaymentTerms/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleMachineCategory[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleMachineCategory sga in simpleList)
            {
                if (sga.DisplayName == testObject.Name)
                {
                    testObject.ID = sga.ID;
                }
            }

            testObject.Description = "test test";

            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + testObject.ID, JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            //test: expect OK
            var updatedMachineCategory = await EndpointTests.AssertGetStatusCode<MachineCategory>(client, $"{apiUrl}/" + testObject.ID, HttpStatusCode.OK);
            Assert.AreEqual(updatedMachineCategory.Description, testObject.Description);

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testObject.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestMachineCategoryFilters()
        {
            //setup: get new machine category
            MachineCategory testObject = await GetPostAssertNewMachineCategory();

            //test: make sure it exists when filtering simple list for isactive=true
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleMachineCategory[]>(client, $"{apiUrl}/SimpleList?isActive=true", HttpStatusCode.OK);
            Assert.IsNotNull(simpleList.FirstOrDefault(x => x.ID == testObject.ID));

            //test: expect only active categories
            var active = await EndpointTests.AssertGetStatusCode<MachineCategory[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(active.All(x => x.IsActive));
            Assert.IsTrue(active.Count() > 0);

            //test: set isactive false
            testObject.IsActive = false;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + testObject.ID, JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            //test: expect only inactive categories
            var inactive = await EndpointTests.AssertGetStatusCode<MachineCategory[]>(client, $"{apiUrl}?IsActive=false", HttpStatusCode.OK);
            Assert.IsTrue(inactive.All(x => !x.IsActive));
            Assert.IsTrue(inactive.Count() > 0);

            //setup: link child
            MachineData testChild = await GetPostAssertNewMachineData();
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{testObject.ID}/action/linkmachine/{testChild.ID}", "", HttpStatusCode.OK);

            //test: make sure there are no expanded machines
            var noneChild = await EndpointTests.AssertGetStatusCode<MachineCategory>(client, $"{apiUrl}/{testObject.ID}", HttpStatusCode.OK);
            Assert.IsTrue(noneChild.Machines == null || noneChild.Machines.Count() == 0);
            Assert.IsTrue(noneChild.SimpleMachines == null || noneChild.SimpleMachines.Count() == 0);

            //test: make sure there are expanded machines
            var fullChild = await EndpointTests.AssertGetStatusCode<MachineCategory>(client, $"{apiUrl}/{testObject.ID}?MachineParts=Full", HttpStatusCode.OK);
            Assert.IsTrue(fullChild.Machines != null && fullChild.Machines.Count() > 0);
            Assert.IsTrue(fullChild.SimpleMachines == null || fullChild.SimpleMachines.Count() == 0);

            //test: make sure there are  expanded simplemachines
            var simpleChild = await EndpointTests.AssertGetStatusCode<MachineCategory>(client, $"{apiUrl}/{testObject.ID}?MachineParts=Simple", HttpStatusCode.OK);
            Assert.IsTrue(simpleChild.Machines == null || simpleChild.Machines.Count() == 0);
            Assert.IsTrue(simpleChild.SimpleMachines != null && simpleChild.SimpleMachines.Count() > 0);
        }

        private async Task<MachineCategory> GetPostAssertNewMachineCategory()
        {
            MachineCategory testObject = new MachineCategory()
            {
                Name = this.NewTestMachineDataName,
                IsActive = true,
                BID = 1,
                Description = "blah blah"
            };

            //test: expect OK
            var newCategory = await EndpointTests.AssertPostStatusCode<MachineCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);
            return newCategory;
        }

        [TestMethod]
        public async Task TestMachineCategoryActionEndpoints()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleMachineData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            MachineData testMachineData = new MachineData()
            {
                Name = this.NewTestMachineDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                //InvoiceText = "blah blah"
                //SKU = "ASDF123"
            };
            var newMachineData = await EndpointTests.AssertPostStatusCode<MachineData>(client, $"/api/machinepart", JsonConvert.SerializeObject(testMachineData), HttpStatusCode.OK);

            MachineCategory testMachineCat = new MachineCategory()
            {
                Name = this.NewTestMachineCategoryName,
                IsActive = true,
                BID = 1,
                Description = "blah blah"
            };

            //test: expect OK
            var newMachineCategory = await EndpointTests.AssertPostStatusCode<MachineCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(testMachineCat), HttpStatusCode.OK);

            //POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newMachineCategory.ID}/action/setinactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newMachineCategory.ID}/action/setactive", HttpStatusCode.OK);

            //POST candelete
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newMachineCategory.ID}/action/candelete", HttpStatusCode.OK);

            //POST link machine
            EntityActionChangeResponse linkResponse;
            linkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newMachineCategory.ID}/action/linkmachine/{newMachineData.ID}", HttpStatusCode.OK);
            Assert.IsTrue(linkResponse.Success);
            //unable to link twice
            linkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newMachineCategory.ID}/action/linkmachine/{newMachineData.ID}", HttpStatusCode.BadRequest);
            Assert.IsFalse(linkResponse.Success);

            //POST unlink machine
            EntityActionChangeResponse unlinkResponse;
            unlinkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newMachineCategory.ID}/action/unlinkmachine/{newMachineData.ID}", HttpStatusCode.OK);
            Assert.IsTrue(unlinkResponse.Success);
            //unable to unlink twice
            unlinkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newMachineCategory.ID}/action/unlinkmachine/{newMachineData.ID}", HttpStatusCode.BadRequest);
            Assert.IsFalse(unlinkResponse.Success);
        }

        [TestMethod]
        public async Task TestMachineCategoryCanDelete()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleMachineData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            MachineData testMachineData = new MachineData()
            {
                Name = this.NewTestMachineDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                //InvoiceText = "blah blah"
                //SKU = "ASDF123"
            };
            var newMachineData = await EndpointTests.AssertPostStatusCode<MachineData>(client, $"/api/machinepart", JsonConvert.SerializeObject(testMachineData), HttpStatusCode.OK);

            MachineCategory testMachineCat = new MachineCategory()
            {
                Name = this.NewTestMachineCategoryName,
                IsActive = true,
                BID = 1,
                Description = "blah blah"
            };

            //test: expect OK
            var newMachineCategory = await EndpointTests.AssertPostStatusCode<MachineCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(testMachineCat), HttpStatusCode.OK);

            //POST candelete
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newMachineCategory.ID}/action/candelete", HttpStatusCode.OK);

        }

        [TestMethod]
        public async Task TestMachineCategoryLinkingEndpoints()
        {
            //setup
            MachineCategory testCategory = await GetPostAssertNewMachineCategory();
            MachineData testChild = await GetPostAssertNewMachineData();

            //test: link from category to child
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{testCategory.ID}/action/linkmachine/{testChild.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{testCategory.ID}/action/unlinkmachine/{testChild.ID}", HttpStatusCode.OK);

            //test: link from child to category
            await EndpointTests.AssertPostStatusCode(client, $"/api/machinepart/{testChild.ID}/action/linkmachinecategory/{testCategory.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"/api/machinepart/{testChild.ID}/action/unlinkmachinecategory/{testCategory.ID}", HttpStatusCode.OK);
        }

        private async Task<MachineData> GetPostAssertNewMachineData()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleMachineData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            MachineData testObject = new MachineData()
            {
                Name = this.NewTestMachineDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                //InvoiceText = "blah blah"
                SKU = "ASDF12345"
            };

            //test: expect OK
            var newObject = await EndpointTests.AssertPostStatusCode<MachineData>(client, $"api/machinepart", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);
            return newObject;
        }

        [TestCleanup]
        public void Teardown()
        {
            //you can actually do test-specific things like this
            //switch (TestContext.TestName)
            //{
            //    case "TestPaymentTermSingleEndpoints":
            //    case "TestFiltering":
            //        // do stuff
            //        break;
            //    default:
            //        break;
            //}
            this.DeleteTestRecord();
        }

        private void DeleteTestRecord()
        {
            string newTestMachineCategoryName = NewTestMachineCategoryName;
            string newTestMachineDataName = NewTestMachineDataName;
            Task.Run(async () =>
            {
                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                SimpleMachineCategory[] result = JsonConvert.DeserializeObject<SimpleMachineCategory[]>(responseString);
                if (result != null)
                {
                    SimpleMachineCategory sga = result.FirstOrDefault(x => x.DisplayName.Contains(newTestMachineCategoryName));
                    if (sga != null)
                    {
                        await client.DeleteAsync($"{apiUrl}/{sga.ID}?Force=true");
                    }
                }
                response = await client.GetAsync($"api/machinepart/SimpleList");
                responseString = await response.Content.ReadAsStringAsync();
                SimpleMachineData[] dataResult = JsonConvert.DeserializeObject<SimpleMachineData[]>(responseString);
                if (result != null)
                {
                    SimpleMachineData sga = dataResult.FirstOrDefault(x => x.DisplayName.Contains(newTestMachineDataName));
                    if (sga != null)
                    {
                        await client.DeleteAsync($"api/machinepart/{sga.ID}");
                    }
                }
            }).Wait();
        }
    }
}
