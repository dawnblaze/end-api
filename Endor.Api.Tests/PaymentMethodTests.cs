﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    [TestClass]
    public class PaymentMethodEndpointTests
    {
        public const string apiUrl = "/api/paymentmethod";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestPaymentMethod;
        public TestContext TestContext { get; set; }
        
        [TestInitialize]
        public void Initialize()
        {
            this.NewTestPaymentMethod = DateTime.UtcNow + " TEST PAYMENT METHOD";
        }

        [TestMethod]
        public async Task TestPaymentMethodSingleEndpoints()
        {
            PaymentMethod newModel = GetNewModel();

            //test: expect OK
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);

            // GET api/PaymentMethods/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimplePaymentMethod[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimplePaymentMethod spm in simpleList)
            {
                if (spm.DisplayName == newModel.Name)
                {
                    newModel.ID = spm.ID;
                }
            }

            //POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newModel.ID}/action/setinactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newModel.ID}/action/setactive", HttpStatusCode.OK);

            newModel.PaymentMethodType = PaymentMethodType.Check;
            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + newModel.ID, JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);

            //POST canDelete
            var canDeleteResp = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{newModel.ID}/action/candelete", HttpStatusCode.OK);
            Assert.IsTrue(canDeleteResp.Value.Value);

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{newModel.ID}", HttpStatusCode.NoContent);

            //test: expect OK per https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/460816420/Payment+Methods+API
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/9241657", HttpStatusCode.OK);
        }

        private PaymentMethod GetNewModel()
        {
            return new PaymentMethod()
            {
                BID = 1,
                Name = this.NewTestPaymentMethod,
                IsActive = true,
                DepositGLAccountID = 1100,
                PaymentMethodType = PaymentMethodType.Cash,
                IsCustom = true,
                IsIntegrated = false
            };
        }

        [TestMethod]
        public async Task TestFiltering()
        {

            PaymentMethod newModel = GetNewModel();

            //test: expect OK
            var savedAccount = await EndpointTests.AssertPostStatusCode<PaymentMethod>(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);

            //test that our account shows under no filters
            var all = await EndpointTests.AssertGetStatusCode<PaymentMethod[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(all.FirstOrDefault(x => x.Name == newModel.Name));

            //test that our account DOES show under actives
            var active = await EndpointTests.AssertGetStatusCode<PaymentMethod[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(active.All(x => x.IsActive));
            Assert.IsNotNull(active.FirstOrDefault(x => x.Name == newModel.Name));

            var activeSimple = await EndpointTests.AssertGetStatusCode<SimplePaymentMethod[]>(client, $"{apiUrl}/simplelist?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(activeSimple.All(x => x.IsActive));
            Assert.IsNotNull(activeSimple.FirstOrDefault(x => x.DisplayName.Contains(newModel.Name)));

            //test that our account DOES NOT show under inactives
            var inactive = await EndpointTests.AssertGetStatusCode<PaymentMethod[]>(client, $"{apiUrl}?IsActive=false", HttpStatusCode.OK);
            Assert.IsTrue(inactive.All(x => !x.IsActive));
            Assert.IsNull(inactive.FirstOrDefault(x => x.Name == newModel.Name));

            var inactiveSimple = await EndpointTests.AssertGetStatusCode<SimplePaymentMethod[]>(client, $"{apiUrl}/simplelist?IsActive=false", HttpStatusCode.OK);
            Assert.IsTrue(inactiveSimple.All(x => !x.IsActive));
            Assert.IsNull(inactiveSimple.FirstOrDefault(x => x.DisplayName.Contains(newModel.Name)));            

            var assetSimple = await EndpointTests.AssertGetStatusCode<SimplePaymentMethod[]>(client, $"{apiUrl}/simplelist?PaymentMethodType=10", HttpStatusCode.OK);
            Assert.IsNotNull(assetSimple.FirstOrDefault(x => x.DisplayName.Contains(newModel.Name)));
            
        }

        [TestMethod]
        public async Task TestPostNewInactive()
        {
            var newPaymentMethod = GetNewModel();
            newPaymentMethod.IsActive = false;
            //test: expect OK
            var savedPaymentMethod = await EndpointTests.AssertPostStatusCode<PaymentMethod>(client, $"{apiUrl}", JsonConvert.SerializeObject(newPaymentMethod), HttpStatusCode.OK);
            
            var account = await EndpointTests.AssertGetStatusCode<PaymentMethod>(client, $"{apiUrl}/{savedPaymentMethod.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(account);
            Assert.AreEqual(false, account.IsActive);
        }

        [TestCleanup]
        public void Teardown()
        {
            this.DeleteTestAccountRecord();
        }

        private void DeleteTestAccountRecord()
        {
            string newTestPaymentMethodName = NewTestPaymentMethod;
            Task.Run(async () =>
            {

                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                SimplePaymentMethod[] result = JsonConvert.DeserializeObject<SimplePaymentMethod[]>(responseString);
                if (result != null)
                {
                    SimplePaymentMethod sga = result.FirstOrDefault(x => x.DisplayName.Contains(newTestPaymentMethodName));
                    if (sga != null)
                    {
                        await client.DeleteAsync($"{apiUrl}/{sga.ID}");
                    }
                }
            }).Wait();
        }
    }
}
