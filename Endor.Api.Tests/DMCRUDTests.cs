using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Endor.Api.Web.Controllers;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using AssertX = Xunit.Assert;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using Newtonsoft.Json;
using Endor.DocumentStorage.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Endor.Tenant;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;

namespace Endor.Api.Tests
{
    [TestClass]
    public class DMCRUDTests: CommonControllerTestClass
    {
        [TestMethod]
        [DeploymentItem(TestTextFilePath)]
        [DeploymentItem(TestTextTwoFilePath)]
        public async Task CRUDEmployeeDocuments()
        {
            await CRUD(new DMController.RequestModel()
            {
                id = 100,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents
            });
        }

        [TestMethod]
        [DeploymentItem("Endor.Api.Tests\\test.txt")]
        [DeploymentItem("Endor.Api.Tests\\test_two.txt")]
        public async Task CRUDTemplateDocuments()
        {
            await CRUD(new DMController.RequestModel()
            {
                ctid = Convert.ToInt32(ClassType.Employee),
                classFolder = "template",
                Bucket = Web.BucketRequest.Documents
            });
        }

        [TestMethod]
        [DeploymentItem("Endor.Api.Tests\\test.txt")]
        [DeploymentItem("Endor.Api.Tests\\test_two.txt")]
        public async Task CRUDStaticData()
        {
            await CRUD(new DMController.RequestModel()
            {
                ctid = Convert.ToInt32(ClassType.Employee),
                classFolder = "static",
                Bucket = Web.BucketRequest.Data
            });
        }

        [TestMethod]
        [DeploymentItem("Endor.Api.Tests\\test.txt")]
        [DeploymentItem("Endor.Api.Tests\\test_two.txt")]
        public async Task CRUDAssociationData()
        {
            await CRUD(new DMController.RequestModel()
            {
                ctid = Convert.ToInt32(ClassType.Employee),
                classFolder = "association",
                Bucket = Web.BucketRequest.Data,
            });
        }

        [TestMethod]
        [DeploymentItem("Endor.Api.Tests\\test.txt")]
        [DeploymentItem("Endor.Api.Tests\\test_two.txt")]
        public async Task CRUDTempDocuments()
        {
            await CRUD(new DMController.RequestModel()
            {
                guid = Guid.NewGuid(),
                Bucket = Web.BucketRequest.Documents
            });
        }

        /// <summary>
        /// Deletes all documents for given model
        /// Uploads a file
        /// Reads that file back
        /// Updates the file with another file
        /// Deletes the file
        /// Asserts there are no files for this model
        /// </summary>
        /// <param name="baseModel"></param>
        /// <returns></returns>
        private async Task CRUD(DMController.RequestModel baseModel)
        {
            var testController = GetDMController();
            await CleanSlate(testController, baseModel);

            string fileAssetName = "test.txt";
            string fileToRenameAssetName = "iAmNotLongForThisWorld.txt";
            //C
            AddFileToForm(testController, "test.txt");
            var create = baseModel.Clone();
            create.PathAndFilename = fileAssetName;
            var okPost = AssertX.IsType<OkObjectResult>(await testController.Post(create, null, null));
            Assert.IsTrue(okPost.StatusCode == 200);
            AssertX.IsType<DMItem>(okPost.Value);
            testController.Request.Form = null;

            AddFileToForm(testController, "test.txt");
            create = baseModel.Clone();
            create.PathAndFilename = fileToRenameAssetName;
            okPost = AssertX.IsType<OkObjectResult>(await testController.Post(create, null, null));
            Assert.IsTrue(okPost.StatusCode == 200);
            AssertX.IsType<DMItem>(okPost.Value);
            testController.Request.Form = null;

            OkObjectResult okGet = null;

            //R
            okGet = AssertX.IsType<OkObjectResult>(await testController.GetDocuments(baseModel.Clone()));

            Assert.IsTrue(okGet.StatusCode == 200);
            var docs = AssertX.IsType<List<DMItem>>(okGet.Value);
            Assert.IsTrue(docs.Count == 2);
            Assert.IsTrue(docs.Exists(f => f.Name == fileAssetName));

            //U
            //upload/replace
            AddFileToForm(testController, "test_two.txt");
            var update = baseModel.Clone();
            update.PathAndFilename = fileAssetName;
            var okPut = AssertX.IsType<OkObjectResult>(await testController.Put(update, null, null, null, null, null));
            Assert.IsTrue(okPut.StatusCode == 200);
            AssertX.IsType<DMItem>(okPut.Value);
            AddFileToForm(testController, "test_two.txt");
            //rename
            update = baseModel.Clone();
            update.PathAndFilename = fileToRenameAssetName;
            string newFileName = "renamed.txt";
            testController.Request.Form = null;
            okPut = AssertX.IsType<OkObjectResult>(await testController.Put(update, null, newFileName, null, null, newFileName));
            Assert.IsTrue(okPut.StatusCode == 200);
            int numRenamed = AssertX.IsType<int>(okPut.Value);
            Assert.AreEqual(1, numRenamed);

            //D
            testController.Request.Form = null;
            var delete = baseModel.Clone();
            delete.PathAndFilename = fileAssetName;
            var okDelete = AssertX.IsType<OkObjectResult>(await testController.Delete(delete));
            Assert.IsTrue(okDelete.StatusCode == 200);
            Assert.AreEqual(1, AssertX.IsType<int>(okDelete.Value));
            
            delete = baseModel.Clone();
            delete.PathAndFilename = newFileName;
            okDelete = AssertX.IsType<OkObjectResult>(await testController.Delete(delete));
            Assert.IsTrue(okDelete.StatusCode == 200);
            Assert.AreEqual(1, AssertX.IsType<int>(okDelete.Value));

            //make sure we have no documents
            okGet = AssertX.IsType<OkObjectResult>(await testController.GetDocuments(baseModel.Clone()));

            Assert.IsTrue(okGet.StatusCode == 200);
            docs = AssertX.IsType<List<DMItem>>(okGet.Value);
            Assert.IsTrue(docs.Count == 0);
        }

        [TestMethod]
        [DeploymentItem(Test1x1PixelPngFilePath)]
        public async Task ContentTypeRecordedOnPostTest()
        {
            var testController = GetDMController();

            AddFileToForm(testController, Test1x1PixelPngFileName);
            DMController.RequestModel createRequest = new DMController.RequestModel()
            {
                Bucket = Web.BucketRequest.Documents,
                id = GetHashCode(),
                PathAndFilename = Test1x1PixelPngFileName,
                ctid = 2,
            };
                        
            var okPost = AssertX.IsType<OkObjectResult>(await testController.Post(createRequest, null, null));
            Assert.IsTrue(okPost.StatusCode == 200);
            var okResult = AssertX.IsType<DMItem>(okPost.Value);

            Assert.AreEqual(MediaTypes.image_png, okResult.MediaType);
            Assert.AreEqual(AssetTypes.Image, okResult.AssetType);
            Assert.AreEqual(MediaTypes.image_png.MimeType, okResult.MimeType);

            testController.Request.Form = null;

            var okGet = AssertX.IsType<OkObjectResult>(await testController.GetDocuments(createRequest));
            Assert.IsTrue(okGet.StatusCode == 200);
            var docs = AssertX.IsType<List<DMItem>>(okGet.Value);

            Assert.AreEqual(MediaTypes.image_png, docs[0].MediaType);
            Assert.AreEqual(AssetTypes.Image, docs[0].AssetType);
            Assert.AreEqual(MediaTypes.image_png.MimeType, docs[0].MimeType);


        }

        [TestCleanup]
        public async Task Cleanup()
        {
            await Task.Yield();
            //delete via entity storage client
        }
    }
}
