﻿using Endor.Api.Web.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Endor.Models;
using System.Threading.Tasks;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.EF;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Tests
{
    [TestClass]
    public class DashboardWidgetEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/dashboardwidget";

        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestInitialize]
        public void Initialize()
        {
        }

        [TestMethod]
        public async Task DashboardWidgetCRUDTest()
        {
            // POST A new employee 
            var ctx = GetApiContext();


            var postDashboardWidgetRequestModel = GetPOSTDashboardWidget(ctx);
            var postedDashboardWidget = await EndpointTests.AssertPostStatusCode<DashboardWidgetData>(client, apiUrl, postDashboardWidgetRequestModel, HttpStatusCode.OK);

            
            var allDashboardWidgets = await EndpointTests.AssertGetStatusCode<DashboardWidgetData[]>(client, $"{apiUrl}/?DashboardID={postDashboardWidgetRequestModel.DashboardID}", HttpStatusCode.OK);
            var allDashboardWidgets2 = await EndpointTests.AssertGetStatusCode<DashboardWidgetData>(client, $"{apiUrl}/{postedDashboardWidget.ID}", HttpStatusCode.OK);

            // Update
            postedDashboardWidget.Name = $"ChangedMyName{DateTime.UtcNow}";

            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{postedDashboardWidget.ID}", JsonConvert.SerializeObject(postedDashboardWidget), HttpStatusCode.OK);
            var updatedEmployee = await EndpointTests.AssertGetStatusCode<DashboardWidgetData>(client, $"{apiUrl}/{postedDashboardWidget.ID}", HttpStatusCode.OK);

            Assert.AreEqual(postedDashboardWidget.Name, updatedEmployee.Name);

            // Delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{postedDashboardWidget.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task DashboardWidgetQueryTest()
        {
            var startDT = DateTime.Now;
            var ctx = GetApiContext();

            // CurrentTimeWidget
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/query?widgetDefinitionID=2&employeeid=" + (await ctx.EmployeeData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.IsActive)).ID + "&startDT=" + startDT + "&forceRefresh=true", HttpStatusCode.OK);

            // Order Throughhput
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/query?widgetDefinitionID=3&rangetype=12&&employeeid=" + (await ctx.EmployeeData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.IsActive)).ID + "&startDT=" + startDT + "&forceRefresh=true", HttpStatusCode.OK);

            // LineItemStatusCount
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/query?widgetDefinitionID=5&statusidlist=21&statusidlist=23&statusidlist=23&employeeid=" + (await ctx.EmployeeData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.IsActive)).ID + "&startDT=" + startDT + "&forceRefresh=true", HttpStatusCode.OK);

            // LineItemStatusCount
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/query?widgetDefinitionID=6&&employeeid=" + (await ctx.EmployeeData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.IsActive)).ID + "&startDT=" + startDT + "&forceRefresh=true", HttpStatusCode.OK);

            // Current Estimate Status
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/query?widgetDefinitionID=7&rangetype=12&&employeeid=" + (await ctx.EmployeeData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.IsActive)).ID + "&startDT=" + startDT + "&forceRefresh=true", HttpStatusCode.OK);

        }

        [TestMethod]
        public async Task DashboardWidgetActionTest()
        {
            // POST A new employee 
            var ctx = GetApiContext();


            var postDashboardWidgetRequestModel = GetPOSTDashboardWidget(ctx);
            var postedDashboardWidget = await EndpointTests.AssertPostStatusCode<DashboardWidgetData>(client, apiUrl, postDashboardWidgetRequestModel, HttpStatusCode.OK);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{postedDashboardWidget.ID}/action/setinactive", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{postedDashboardWidget.ID}/action/setactive",  HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{postedDashboardWidget.ID}/action/candelete", HttpStatusCode.OK);

            var postedDashboardWidgetClone = await EndpointTests.AssertPostStatusCode<DashboardWidgetData>(client, $"{apiUrl}/{postedDashboardWidget.ID}/action/clone", postDashboardWidgetRequestModel, HttpStatusCode.OK);
            Assert.AreEqual(postedDashboardWidgetClone.Name, postedDashboardWidget.Name);
            Assert.AreNotEqual(postedDashboardWidgetClone.ID, postedDashboardWidget.ID);

            // Delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{postedDashboardWidget.ID}", HttpStatusCode.NoContent);
        }

        private DashboardWidgetData GetPOSTDashboardWidget(ApiContext ctx)
        {
            return new DashboardWidgetData()
            {
                BID = 1,
                IsActive = true,
                Name = "TestDashboardWidget",
                WidgetDefinitionID = ctx.DashboardWidgetDefinition.First().ID,
                DashboardID = ctx.DashboardData.First().ID
            };
        }
    }
}
