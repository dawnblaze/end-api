﻿using Endor.Api.Web.Classes;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class PaymentAPEEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/payment/ape/";
        public HttpClient client = EndpointTests.GetHttpClient();

        // NOTE: APE must run and PaymentAPEService.ApeBaseUrl is pointing to it

        [TestMethod]
        public async Task APERegistrationTest()
        {
            APERegistrationResponse registrationObj = await EndpointTests.AssertGetStatusCode<APERegistrationResponse>(client, $"{apiUrl}registration", HttpStatusCode.OK);
            Assert.IsNotNull(registrationObj, "Registration data is null");
            Assert.IsNotNull(registrationObj.RegistrationID, "Registration ID is null");
            Assert.IsNotNull(registrationObj.ApeURL, "APE URL is null");
            Assert.IsTrue(registrationObj.Expiration.CompareTo(DateTime.Now) > 0, "Expiration is less that current DT");
        }

        [TestMethod]
        public async Task DeleteBusinessKeyTest()
        {
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}businessKey", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task GetSingleOrderPaymentPageTest()
        {
            var ctx = GetApiContext();
            OrderData order = ctx.OrderData.LastOrDefault(x => x.BID == 1);
            // Ensure APE application is running
            APERegistrationResponse registrationObj = GetAPERegistration().Result;
            if (!string.IsNullOrEmpty(registrationObj.Provider.Name))
            {
                await EndpointTests.AssertGetResponseContent(client,
                $"{apiUrl}paymentpage?RegistrationID=" + registrationObj.RegistrationID + "&OrderID=" + order?.ID + "&Amount="
                + order?.PriceTotal + 1000, "", HttpStatusCode.OK);
            }
        }

        [TestMethod]
        public async Task GetMultipleOrderPaymentPageTest()
        {
            var ctx = GetApiContext();
            List<OrderData> orderList = ctx.OrderData.Take(2).ToList();
            // Ensure APE application is running
            APERegistrationResponse registrationObj = GetAPERegistration().Result;

            if (!string.IsNullOrEmpty(registrationObj.Provider.Name))
            {
                APIPaymentPageRequest paymentPageRequest = new APIPaymentPageRequest();
                paymentPageRequest.RegistrationID = registrationObj.RegistrationID;
                paymentPageRequest.Amount = (decimal)orderList.Sum(x => x.PriceTotal);
                paymentPageRequest.Orders = new APIPaymentPageRequestOrders[2];
                paymentPageRequest.Orders[0] = new APIPaymentPageRequestOrders();
                paymentPageRequest.Orders[0].OrderID = orderList[0].ID;
                paymentPageRequest.Orders[0].Amount = orderList[0].PriceNet;
                paymentPageRequest.Orders[1] = new APIPaymentPageRequestOrders();
                paymentPageRequest.Orders[1].OrderID = orderList[1].ID;
                paymentPageRequest.Orders[1].Amount = orderList[1].PriceNet;

                await EndpointTests.AssertPostStatusCode(client,
                    $"{apiUrl}paymentpage?", paymentPageRequest, HttpStatusCode.OK);
            }

        }

        private async Task<APERegistrationResponse> GetAPERegistration()
        {
            //APE Sandbox settings to use in public async Task<APERegistrationResponse> GetRegistrationAsync()
            //You need to either set this in your options.data in your database or assign this in the registration code.
            //username = "testing";
            //password = "testing123";
            //merchantID = "800000000147";
            //providerID = 1;

            return await EndpointTests.AssertGetStatusCode<APERegistrationResponse>(client, $"{apiUrl}registration",
                    HttpStatusCode.OK);
        }
    }
}
