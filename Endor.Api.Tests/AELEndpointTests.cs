﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;
using Endor.AEL;
using System.Net;
using System.Linq;
using Endor.Models;
using Endor.EF;
using Endor.Api.Web.Controllers;
using Endor.Api.Web.Classes;
using System.Security.Cryptography;
using Newtonsoft.Json;
using System.Text;
using System.Web;
using System;

namespace Endor.Api.Tests
{
    [TestClass]
    public class AELEndpointTests: CommonControllerTestClass
    {
        public const string apiUrl = "/api/ael";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task TestGetDataTypeInfoDictionary()
        {
            var instance = DataTypeDictionary.Instance;
            var etagDictionary = new Dictionary<EtagKey, string>();
            var result = await EndpointTests.AssertGetStatusCode<Dictionary<string, OperatorInfo>>(client, $"{apiUrl}/datatype", HttpStatusCode.OK, etagDictionary, EtagKey.AELDataTypeDictionary);
            Assert.AreEqual(instance.Keys.Count, result.Keys.Count);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/datatype", HttpStatusCode.NotModified, etagDictionary, EtagKey.AELDataTypeDictionary);
            etagDictionary[EtagKey.AELDataTypeDictionary] = "wrong key";
            result = await EndpointTests.AssertGetStatusCode<Dictionary<string, OperatorInfo>>(client, $"{apiUrl}/datatype", HttpStatusCode.OK, etagDictionary, EtagKey.AELDataTypeDictionary);
            Assert.AreEqual(instance.Keys.Count, result.Keys.Count);
        }

        [TestMethod]
        public async Task TestBusinessMemberDictionary()
        {
            var instance = BusinessMemberDictionary.MemberDictionaryByDataType(new DataProvider(1, GetApiContext(1)));
            var etagDictionary = new Dictionary<EtagKey, string>();
            var result = await EndpointTests.AssertGetStatusCode<Dictionary<string, OperatorInfo>>(client, $"{apiUrl}/member", HttpStatusCode.OK, etagDictionary, EtagKey.AELBusinessMemberDictionary);
            Assert.AreEqual(instance.Keys.Count, result.Keys.Count);
            var filteredResult = await EndpointTests.AssertGetStatusCode<Dictionary<DataType, object>>(client, $"{apiUrl}/member?dataType=BooleanList", HttpStatusCode.OK);
            var filteredInstance = instance.Where(entry=>entry.Key==DataType.BooleanList).ToDictionary(item=>item.Key, item=>item.Value);
            Assert.AreEqual(filteredResult.Keys.First(), filteredInstance.Keys.First());
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/member", HttpStatusCode.NotModified, etagDictionary, EtagKey.AELBusinessMemberDictionary);
            etagDictionary[EtagKey.AELBusinessMemberDictionary] = "wrong key";
            result = await EndpointTests.AssertGetStatusCode<Dictionary<string, OperatorInfo>>(client, $"{apiUrl}/member", HttpStatusCode.OK, etagDictionary, EtagKey.AELBusinessMemberDictionary);
            Assert.AreEqual(instance.Keys.Count, result.Keys.Count);
        }

        [TestMethod]
        public async Task TestGetOperatorDictionary()
        {
            var instance = OperatorDictionary.Instance;
            SHA1 sha = new SHA1CryptoServiceProvider();
            string json = JsonConvert.SerializeObject(instance, Formatting.Indented);
            var bytes = sha.ComputeHash(Encoding.ASCII.GetBytes(json));
            var HashToCheck =  HttpUtility.HtmlEncode(Convert.ToBase64String(bytes));

            var etagDictionary = new Dictionary<EtagKey, string>();
            var result = await EndpointTests.AssertGetStatusCode<Dictionary<string, OperatorInfo>>(client, $"{apiUrl}/operator", HttpStatusCode.OK, etagDictionary, EtagKey.AELOperatorDictionary);
            Assert.AreEqual(instance.Keys.Count, result.Keys.Count);
            Assert.AreEqual(HashToCheck, etagDictionary[EtagKey.AELOperatorDictionary]);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/operator", HttpStatusCode.NotModified, etagDictionary, EtagKey.AELOperatorDictionary);
            etagDictionary[EtagKey.AELOperatorDictionary] = "wrong key";
            result = await EndpointTests.AssertGetStatusCode<Dictionary<string, OperatorInfo>>(client, $"{apiUrl}/operator", HttpStatusCode.OK, etagDictionary, EtagKey.AELOperatorDictionary);
            Assert.AreEqual(instance.Keys.Count, result.Keys.Count);
        }
    }
}
