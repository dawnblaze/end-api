﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Endor.Api.Web.Classes;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Controllers;
using Endor.Api.Web.Classes.Responses;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.DocumentStorage.Models;
using Endor.Api.Web;

namespace Endor.Api.Tests
{
    [TestClass]
    public class UnitEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/Api/SSLCertificate";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }
        public MockTenantDataCache cache { get; set; }

        [TestInitialize]
        public void InitializeTests()
        {
            this.cache = new MockTenantDataCache(_config[SQLConfigName], _config[BlobStorageConfigName]);
        }

        [DataTestMethod]
        [DataRow("/Api/System/UnitInfo", 116, true)]
        [DataRow("/Api/System/UnitInfo?UnitType=11", 6, true)]
        [DataRow("/Api/System/UnitInfo?UnitType=Length", 6, true)]
        [DataRow("/Api/System/UnitInfo?UnitType=Length&UnitSystem=1", 3, true)]
        [DataRow("/Api/System/UnitInfo?UnitType=Length&UnitSystem=Metric", 3, true)]
        [DataRow("/Api/System/UnitInfo/11", 1, false)]
        [DataRow("/Api/System/UnitTypeInfo", 17, true)]
        [DataRow("/Api/System/UnitTypeInfo/Length", 6, true)]
        [DataRow("/Api/System/UnitTypeInfo/11", 6, true)]
        public async Task TestUnitInfoEndpoints(string apiUrl, int expectedCount, bool isList)
        {
            if (isList)
            {
                var response = await EndpointTests.AssertGetStatusCode<List<object>>(client, apiUrl, HttpStatusCode.OK);
                Assert.IsNotNull(response);
                Assert.AreEqual(response.Count, expectedCount);
            }
            else
            {
                var response = await EndpointTests.AssertGetStatusCode<object>(client, apiUrl, HttpStatusCode.OK);
                Assert.IsNotNull(response);
            }
        }
    }
}
