﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Clones;
using Endor.Pricing;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Endor.DocumentStorage.Models;
using AssertX = Xunit.Assert;
using Endor.EF;
using Endor.Tenant;
using Endor.AzureStorage;
using Endor.CBEL.Common;
using Endor.Api.Web.Classes.Request;

namespace Endor.Api.Tests
{
    [TestClass]
    public class CreditMemoTests : PaymentTestHelper
    {
        private const string applytoCreditApiUrl = "/api/payment/applycredit";
        public const string apiUrl = "/api/creditmemo";
        public System.Net.Http.HttpClient client;
        public TestContext TestContext { get; set; }
        private ApiContext ctx;
        private const int BID = 1;
        private const int _orderID = -99;
        private const int _companyID = -99;
        private const int _orderItemID = -99;
        private const int _taxGroupID = -99;
        private short taxCodeID = 0;
        private int glAccountID = 0;

        [TestInitialize]
        public async Task InitializePricingTests()
        {
            ctx = GetApiContext();
            client = EndpointTests.GetHttpClient(employeeID: ctx.EmployeeData.Where(x => x.BID==1 && x.ID>0).First().ID);
            await Cleanup();
            await InitializeCompanyAndGLAccountIDs();
        }

        /// <summary>
        /// Initializes the Company and GLAccount IDs
        /// </summary>
        /// <returns></returns>
        protected async Task InitializeCompanyAndGLAccountIDs()
        {
            // check for existing company
            var company = await ctx.CompanyData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _companyID);
            if (company == null)
            {
                company = new CompanyData()
                {
                    BID = BID,
                    ID = _companyID,
                    Name = "Test Pricing Company",
                    LocationID = 1,
                    IsAdHoc = true,
                    StatusID = 1
                };

                ctx.CompanyData.Add(company);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            // there should always be at least one GLAccount and TaxCode
            glAccountID = ctx.GLAccount.FirstOrDefault(t => t.BID == BID).ID;
            taxCodeID = ctx.TaxabilityCodes.FirstOrDefault(t => t.BID == BID).ID;

            var taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _taxGroupID);
            if (taxGroup == null)
            {
                taxGroup = new TaxGroup()
                {
                    BID = BID,
                    ID = _taxGroupID,
                    Name = "Test Pricing TaxGroup"
                };

                ctx.TaxGroup.Add(taxGroup);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            var taxItem = await ctx.TaxItem.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _taxGroupID);
            if (taxItem == null)
            {
                taxItem = new TaxItem()
                {
                    BID = BID,
                    ID = _taxGroupID,
                    Name = "Test Pricing taxItem",
                    TaxRate = 10,
                    InvoiceText = "test",
                    GLAccountID = glAccountID
                };

                ctx.TaxItem.Add(taxItem);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            var TaxGroupItemLink = await ctx.TaxGroupItemLink.FirstOrDefaultAsync(t => t.BID == BID && t.GroupID == _taxGroupID);
            if (TaxGroupItemLink == null)
            {
                TaxGroupItemLink = new TaxGroupItemLink()
                {
                    BID = BID,
                    GroupID = _taxGroupID,
                    ItemID = _taxGroupID,
                };

                ctx.TaxGroupItemLink.Add(TaxGroupItemLink);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }
        }

        [TestCleanup]
        public async Task Cleanup()
        {
            try
            {
                var toDeleteGl = ctx.GLData.Where(t => t.ID <= 0).ToList();
                var paymentsToDelete = toDeleteGl.Where(a => a.PaymentID.HasValue).Select(a => a.PaymentID);
                ctx.GLData.RemoveRange(toDeleteGl);
                ctx.ActivityGlactivity.RemoveRange(ctx.ActivityGlactivity.Where(t => t.ID <= 0));
                ctx.SaveChanges();
                var paymentsMasterToDelete = ctx.PaymentApplication.Where(a => paymentsToDelete.Contains(a.ID)).Select(a => a.MasterID).ToList();
                ctx.PaymentApplication.RemoveRange(ctx.PaymentApplication.Where(a => paymentsToDelete.Contains(a.AdjustedApplicationID)));
                ctx.SaveChanges();
                ctx.PaymentApplication.RemoveRange(ctx.PaymentApplication.Where(a => paymentsToDelete.Contains(a.ID)));
                ctx.SaveChanges();
                ctx.PaymentMaster.RemoveRange(ctx.PaymentMaster.Where(a => paymentsMasterToDelete.Contains(a.ID)));
                await ctx.SaveChangesAsync();

                var _assemblyTables = ctx.AssemblyTable.Where(t => t.BID == BID && (t.ID < 0 || t.AssemblyID < 0));
                if (_assemblyTables.Any())
                {
                    ctx.AssemblyTable.RemoveRange(_assemblyTables);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }
                var _assemblyVariableFormulas = ctx.AssemblyVariableFormula.Where(t => t.BID == BID && (t.ID < 0 || t.VariableID < 0));
                if (_assemblyVariableFormulas.Any())
                {
                    ctx.AssemblyVariableFormula.RemoveRange(_assemblyVariableFormulas);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }
                var _assemblyVariables = ctx.AssemblyVariable.Where(t => t.BID == BID && (t.ID < 0 || t.AssemblyID < 0 || t.LinkedMaterialID < 0));
                if (_assemblyVariables.Any())
                {
                    ctx.AssemblyVariable.RemoveRange(_assemblyVariables);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _orderItemComponents = ctx.OrderItemComponent.Where(t => t.BID == BID && (t.AssemblyID < 0 || t.ID < 0));
                if (_orderItemComponents.Any())
                {
                    ctx.OrderItemComponent.RemoveRange(_orderItemComponents);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
                }

                var _machineProfileTables = ctx.MachineProfileTable.Where(t => t.BID == BID && (t.ID < 0 || t.ProfileID < 0 || t.TableID < 0));
                if (_machineProfileTables.Count() > 0)
                {
                    ctx.MachineProfileTable.RemoveRange(_machineProfileTables);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _machineProfiles = ctx.MachineProfile.Where(t => t.BID == BID && (t.ID < 0 || t.MachineID < 0 || t.MachineTemplateID < 0));
                if (_machineProfiles.Count() > 0)
                {
                    ctx.MachineProfile.RemoveRange(_machineProfiles);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _assembly = await ctx.AssemblyData.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
                if (_assembly != null)
                {
                    ITenantDataCache cache = this.GetMockTenantDataCache();
                    EntityStorageClient client = new EntityStorageClient((await cache.Get(BID)).StorageConnectionString, BID);
                    var assemblyDoc = new DMID() { id = _assembly.ID, ctid = ClassType.Assembly.ID() };
                    string assemblyName = CBELAssemblyHelper.AssemblyName(BID, _assembly.ID, _assembly.ClassTypeID, 1);

                    await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"bin/{assemblyName}.dll");
                    await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"source/{assemblyName}.cs");
                    ctx.MachineData.RemoveRange(ctx.MachineData.Where(t => t.BID == BID && t.MachineTemplateID == _assembly.ID));
                    ctx.MachineProfile.RemoveRange(ctx.MachineProfile.Where(t => t.BID == BID && t.MachineTemplateID == _assembly.ID));
                    ctx.AssemblyData.Remove(_assembly);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _partMaterialCategoryLinks = ctx.MaterialCategoryLink.Where(t => t.BID == BID && t.PartID < 0);
                if (_partMaterialCategoryLinks.Any())
                {
                    ctx.MaterialCategoryLink.RemoveRange(_partMaterialCategoryLinks);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
                }

                var _material1 = await ctx.MaterialData.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
                if (_material1 != null)
                {
                    ctx.MaterialData.Remove(_material1);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _orderItem = await ctx.OrderItemData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _orderItemID);
                if (_orderItem != null)
                {
                    ctx.OrderItemData.Remove(_orderItem);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
                }

                ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
                ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
                ctx.OrderContactRole.RemoveRange(await ctx.OrderContactRole.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
                ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == BID && x.Parent.Order.ID == _orderID).ToArrayAsync());
                ctx.OrderCustomData.RemoveRange(await ctx.OrderCustomData.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());


                var _orderItemSurchages = ctx.OrderItemSurcharge.Where(t => t.BID == BID && t.SurchargeDefID < 0);
                var orderItemTaxAssessmentIDsToDelete = _orderItemSurchages.Select(x => x.ID).ToList();
                ctx.OrderItemTaxAssessment.RemoveRange(ctx.OrderItemTaxAssessment.Where(t => t.BID == BID && orderItemTaxAssessmentIDsToDelete.Contains(t.ItemSurchargeID.GetValueOrDefault())));
                ctx.OrderItemSurcharge.RemoveRange(_orderItemSurchages);
                await ctx.SaveChangesAsync();

                var _surcharge = await ctx.SurchargeDef.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
                if (_surcharge != null)
                {
                    ctx.SurchargeDef.RemoveRange(_surcharge);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _taxGroupTaxItem = await ctx.TaxGroupItemLink.FirstOrDefaultAsync(t => t.BID == BID && t.GroupID < 0);
                if (_taxGroupTaxItem != null)
                {
                    ctx.TaxGroupItemLink.Remove(_taxGroupTaxItem);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _taxItem = await ctx.TaxItem.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
                if (_taxItem != null)
                {
                    ctx.OrderItemTaxAssessment.RemoveRange(ctx.OrderItemTaxAssessment.Where(t => t.BID == BID && t.TaxItemID == _taxItem.ID));
                    ctx.TaxItem.Remove(_taxItem);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
                if (_taxGroup != null)
                {
                    ctx.TaxGroup.Remove(_taxGroup);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var companies = await ctx.CompanyData.Where(t => t.BID == BID && (t.ID == _companyID || t.DefaultPONumber == "TestIdentifier")).ToListAsync();
                foreach (var _company in companies)
                {
                    // remove payments
                    ctx.GLData.RemoveRange(ctx.GLData.Where(t => t.BID == BID && t.CompanyID == _company.ID));
                    ctx.ActivityGlactivity.RemoveRange(ctx.ActivityGlactivity.Where(t => t.BID == BID && t.CompanyID == _company.ID));
                    ctx.PaymentApplication.RemoveRange(ctx.PaymentApplication.Where(p => p.CompanyID == _company.ID));
                    ctx.PaymentMaster.RemoveRange(ctx.PaymentMaster.Where(p => p.CompanyID == _company.ID));

                    var _orders = await ctx.TransactionHeaderData.Where(t => t.BID == BID && t.CompanyID == _company.ID).ToListAsync();
                    foreach (var o in _orders)
                    {
                        await RemoveTransheaderRecursively(o.ID);
                    }

                    ctx.CompanyData.Remove(_company);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _labor = ctx.LaborData.Where(t => t.BID == BID && t.ID < 0);
                if (_labor.Count() > 0)
                {
                    ctx.LaborData.RemoveRange(_labor);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }


                var _machine = ctx.MachineData.Where(t => t.BID == BID && t.ID < 0);
                if (_machine.Count() > 0)
                {
                    ctx.MachineData.RemoveRange(_machine);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }


            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        private async Task<bool> RemoveTransheaderRecursively(int orderID)
        {
            ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(t => t.BID == BID && t.OrderID == orderID && t.IsOrderNote));
            var items = ctx.OrderItemData.Where(t => t.BID == BID && t.OrderID == orderID);
            var itemIDs = items.Select(x => x.ID);
            ctx.OrderItemData.RemoveRange(items);
            ctx.OrderItemComponent.RemoveRange(ctx.OrderItemComponent.Where(t => t.BID == BID && t.OrderID == orderID));
            ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(t => t.BID == BID && t.OrderID == orderID));
            ctx.OrderItemSurcharge.RemoveRange(ctx.OrderItemSurcharge.Where(t => t.BID == BID && itemIDs.Contains(t.OrderItemID)));
            ctx.OrderOrderLink.RemoveRange(ctx.OrderOrderLink.Where(x => x.BID == BID && x.Order.ID == orderID));
            ctx.OrderTagLink.RemoveRange(ctx.OrderTagLink.Where(x => x.BID == BID && x.Order.ID == orderID));
            ctx.OrderContactRole.RemoveRange(ctx.OrderContactRole.Where(x => x.BID == BID && x.Order.ID == orderID));
            ctx.OrderContactRoleLocator.RemoveRange(ctx.OrderContactRoleLocator.Where(x => x.BID == BID && x.Parent.Order.ID == orderID));
            ctx.OrderCustomData.RemoveRange(ctx.OrderCustomData.Where(x => x.BID == BID && x.Order.ID == orderID));
            ctx.OrderEmployeeRole.RemoveRange(ctx.OrderEmployeeRole.Where(x => x.BID == BID && x.Order.ID == orderID));
            ctx.OrderKeyDate.RemoveRange(ctx.OrderKeyDate.Where(x => x.BID == BID && x.Order.ID == orderID));
            ctx.GLData.RemoveRange(ctx.GLData.Where(t => t.BID == BID && t.OrderID == orderID));
            ctx.ActivityGlactivity.RemoveRange(ctx.ActivityGlactivity.Where(t => t.BID == BID && t.OrderID == orderID));
            
            ctx.TransactionHeaderData.RemoveRange(ctx.TransactionHeaderData.Where(t => t.ID == orderID));

            return await ctx.SaveChangesAsync() > 1;
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task TestCreditMemoCRUDOperations()
        {
            var ctx = GetApiContext();

            CompanyData company = new CompanyData()
            {
                ID = _companyID,
                Name = "Test Company",
                IsActive = true,
                BID = TestConstants.BID,
                StatusID = (byte)CompanyStatus.Lead,
                LocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID,
            };
            var existingCompanies = ctx.CompanyData.Where(c => c.ID == company.ID && c.BID == company.BID);
            if (existingCompanies.Count() > 0) company = existingCompanies.First();
            else
            {
                ctx.CompanyData.Add(company);
                ctx.SaveChanges();
            }
            #region CREATE
            var testOrder = Utils.GetTestCreditMemoData();
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.BadRequest);

            testOrder.CompanyID = company.ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<CreditMemoData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            #endregion

            #region RETRIEVE

            // Test Get By ID
            var retrievedOrder = await EndpointTests.AssertGetStatusCode<CreditMemoData>(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder);

            var getAll = await EndpointTests.AssertGetStatusCode<CreditMemoData[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(getAll);
            Assert.IsTrue(getAll.Count() > 0);
            Assert.IsTrue(getAll.All(t => t.TransactionType == (byte)OrderTransactionType.Memo));

            ctx.Entry<CompanyData>(company).Reload();
            Assert.IsTrue(company.StatusText.Contains("Client"));

            #endregion

            #region UPDATE

            var holdFormattedNumber = createdOrder.FormattedNumber;
            createdOrder.FormattedNumber = ""; //invalid length string;
            var updatedOrder = await EndpointTests.AssertPutStatusCode<CreditMemoData>(client, $"{apiUrl}/{createdOrder.ID}", JsonConvert.SerializeObject(createdOrder), HttpStatusCode.OK);
            Assert.IsTrue(holdFormattedNumber == updatedOrder.FormattedNumber);
            createdOrder.FormattedNumber = holdFormattedNumber;
            createdOrder.Description = "TEST DESCRIPTION";
            var testPut = await EndpointTests.AssertPutStatusCode<CreditMemoData>(client, $"{apiUrl}/{createdOrder.ID}", JsonConvert.SerializeObject(createdOrder), HttpStatusCode.OK);
            Assert.IsNotNull(testPut);
            Assert.AreEqual("TEST DESCRIPTION", testPut.Description);

            #endregion

            //#region DELETE

            //await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}?force=true", HttpStatusCode.OK);

            //try
            //{
            //    ctx.CompanyData.Remove(company);
            //    ctx.SaveChanges();
            //}
            //catch (Exception e)
            //{
            //    Assert.Fail(e.Message);
            //}

            //#endregion
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task TestCreditMemoCRUDOperationsWithTaxes()
        {
            var ctx = GetApiContext();

            CompanyData company = new CompanyData()
            {
                ID = _companyID,
                Name = "Test Company",
                IsActive = true,
                BID = TestConstants.BID,
                StatusID = (byte)CompanyStatus.Lead,
                LocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID,
            };
            var existingCompanies = ctx.CompanyData.Where(c => c.ID == company.ID && c.BID == company.BID);
            if (existingCompanies.Count() > 0) company = existingCompanies.First();
            else
            {
                ctx.CompanyData.Add(company);
                ctx.SaveChanges();
            }
            #region CREATE
            var testOrder = Utils.GetTestCreditMemoData();
            testOrder.CompanyID = company.ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;
            var testOrderItem = Utils.GetTestOrderItemData(testOrder);
            testOrderItem.ItemStatusID = 81;
            testOrder.Items = new List<OrderItemData>()
            {
                testOrderItem
            };
            var incomeAccounts = ctx.GLAccount.Where(account => account.GLAccountType == 40).ToList(); //Income
            var testOrderItemComponent = Utils.GetTestOrderItemComponentData(testOrderItem, incomeAccounts.First());
            testOrderItemComponent.TaxInfoList = new List<TaxAssessmentResult>()
            {
                new TaxAssessmentResult()
                {
                    TaxGroupID = ctx.TaxGroup.First(x => x.BID ==TestConstants.BID).ID,
                    TaxAmount = 1,
                    PriceTaxable = 10,
                    TaxRate = 0.1m,
                    InvoiceText = "TEst",
                    Items = new List<TaxAssessmentItemResult>()
                    {
                        new TaxAssessmentItemResult()
                        {
                            TaxItemID = ctx.TaxItem.First(x => x.BID ==TestConstants.BID).ID,
                            InvoiceText = "Test",
                            TaxRate=0.1m,
                            TaxAmount = 1,
                            GLAccountID = ctx.GLAccount.First(x => x.BID ==TestConstants.BID).ID
                        }
                    }
                }
            };
            var surcharge1 = new OrderItemSurcharge()
            {
                BID = TestConstants.BID,
                Name = "Test Surcharge",
                TaxCode = ctx.TaxabilityCodes.FirstOrDefault(x => x.BID == TestConstants.BID),
                IncomeAccountID = ctx.GLAccount.FirstOrDefault(x => x.BID == TestConstants.BID).ID,
                SurchargeDefID = ctx.SurchargeDef.FirstOrDefault(x => x.BID == TestConstants.BID).ID
            };
            surcharge1.TaxInfoList = new List<TaxAssessmentResult>()
            {
                new TaxAssessmentResult()
                {
                    TaxGroupID = ctx.TaxGroup.First().ID,
                    TaxAmount = 1,
                    PriceTaxable = 10,
                    TaxRate = 0.1m,
                    InvoiceText = "Test",
                    Items = new List<TaxAssessmentItemResult>()
                    {
                        new TaxAssessmentItemResult()
                        {
                            TaxItemID = ctx.TaxItem.First(x => x.BID ==TestConstants.BID).ID,
                            InvoiceText = "Test",
                            TaxRate=0.1m,
                            TaxAmount = 1,
                            GLAccountID = ctx.GLAccount.First(x => x.BID ==TestConstants.BID).ID
                        }
                    }
                }
            };
            testOrderItem.Components = new List<OrderItemComponent>()
            {
                testOrderItemComponent
            };
            testOrderItem.Surcharges = new List<OrderItemSurcharge>()
            {
                surcharge1
            };

            var createdOrder = await EndpointTests.AssertPostStatusCode<CreditMemoData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            var createdOrderItemId = createdOrder.Items.First().ID;
            Assert.AreEqual(ctx.OrderItemTaxAssessment.Where(x => x.BID == TestConstants.BID && x.OrderItemID == createdOrderItemId).Count(), 2);
            #endregion

            #region RETRIEVE

            // Test Get By ID
            var retrievedOrder = await EndpointTests.AssertGetStatusCode<CreditMemoData>(client, $"{apiUrl}/{createdOrder.ID}?ItemLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder);
            Assert.AreEqual(retrievedOrder.Items.First().Components.First().TaxInfoList.Count, 1);
            Assert.AreEqual(retrievedOrder.Items.First().Surcharges.First().TaxInfoList.Count, 1);

            #endregion

            #region UPDATE

            var holdFormattedNumber = createdOrder.FormattedNumber;
            createdOrder.FormattedNumber = ""; //invalid length string;
            var updatedOrder = await EndpointTests.AssertPutStatusCode<CreditMemoData>(client, $"{apiUrl}/{createdOrder.ID}", JsonConvert.SerializeObject(createdOrder), HttpStatusCode.OK);
            Assert.IsTrue(holdFormattedNumber == updatedOrder.FormattedNumber);
            Assert.AreEqual(ctx.OrderItemTaxAssessment.Where(x => x.BID == TestConstants.BID && x.OrderItemID == createdOrderItemId).Count(), 2);

            #endregion

            //#region DELETE

            //await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}?force=true", HttpStatusCode.OK);
            //Assert.AreEqual(ctx.OrderItemTaxAssessment.Where(x => x.BID == TestConstants.BID && x.OrderItemID == createdOrderItemId).Count(), 0);

            //try
            //{
            //    ctx.CompanyData.Remove(company);
            //    ctx.SaveChanges();
            //}
            //catch (Exception e)
            //{
            //    Assert.Fail(e.Message);
            //}

            //#endregion
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task RecomputeCreditMemoGLTest()
        {
            var priority = "low";
            CreditMemoData testOrder = Utils.GetTestCreditMemoData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;
            var createdOrder = await EndpointTests.AssertPostStatusCode<CreditMemoData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            // test INVALID priority
            priority = "invalid";
            await EndpointTests.AssertPostStatusCode(this.client, $"{apiUrl}/{createdOrder.ID}/action/recomputeGL?priority={priority}", HttpStatusCode.BadRequest);

            // test low priority
            priority = "low";
            await EndpointTests.AssertPostStatusCode(this.client, $"{apiUrl}/{createdOrder.ID}/action/recomputeGL?priority={priority}", HttpStatusCode.OK);
            // test medium priority
            priority = "medium";
            await EndpointTests.AssertPostStatusCode(this.client, $"{apiUrl}/{createdOrder.ID}/action/recomputeGL?priority={priority}", HttpStatusCode.OK);
            // test high priority
            priority = "high";
            await EndpointTests.AssertPostStatusCode(this.client, $"{apiUrl}/{createdOrder.ID}/action/recomputeGL?priority={priority}", HttpStatusCode.OK);
            // Test RecomputeGL with non existing order ID
            var failID = -12345;
            string orderStatusEndpoint = $"{apiUrl}/{failID}/action/recomputeGL?priority={priority}";
            await EndpointTests.AssertPostStatusCode(this.client, orderStatusEndpoint, HttpStatusCode.NotFound);
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task ChangeCreditMemoStatusTest()
        {
            var allowedOrderTypeStatusMap = new Dictionary<OrderTransactionType, OrderOrderStatus[]>()
            {
                {
                    OrderTransactionType.Memo,
                    new OrderOrderStatus[] {
                        OrderOrderStatus.CreditMemoUnposted,
                        OrderOrderStatus.CreditMemoPosted,
                    }
                }
            };

            OrderOrderStatus[] voidStatuses = new OrderOrderStatus[] {
                OrderOrderStatus.CreditMemoVoided
            };

            CreditMemoData testOrder = Utils.GetTestCreditMemoData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            // Regular Order status can't be initally void
            testOrder.TransactionType = (byte)OrderTransactionType.Memo;
            testOrder.OrderStatusID = OrderOrderStatus.CreditMemoVoided;
            await assertChangingOrderStatus(testOrder, OrderOrderStatus.CreditMemoUnposted, HttpStatusCode.BadRequest);

            // Orders can only change the status within its allowed OrderStatus
            foreach (KeyValuePair<OrderTransactionType, OrderOrderStatus[]> itemMap in allowedOrderTypeStatusMap)
            {
                // get other OrderTypes status
                List<OrderOrderStatus> otherInvalidStatuses = allowedOrderTypeStatusMap.Where(item => item.Key != itemMap.Key).SelectMany(x => x.Value).ToList();

                // run through all the non-void statuses
                foreach (OrderOrderStatus orderStatus in itemMap.Value.Where(item => !voidStatuses.Contains(item)))
                {
                    testOrder.TransactionType = (byte)itemMap.Key;
                    testOrder.OrderStatusID = orderStatus;

                    // test invalid order status change
                    foreach (OrderOrderStatus otherStatus in otherInvalidStatuses)
                    {
                        await assertChangingOrderStatus(testOrder, otherStatus, HttpStatusCode.BadRequest);
                    }

                    // test valid order status change
                    var otherValidStatuses = itemMap.Value.Where(item => item != orderStatus && !voidStatuses.Contains(item)).ToList();
                    foreach (OrderOrderStatus otherStatus in otherValidStatuses)
                    {
                        if(!(testOrder.OrderStatusID == OrderOrderStatus.CreditMemoPosted && otherStatus == OrderOrderStatus.CreditMemoUnposted))
                            await assertChangingOrderStatus(testOrder, otherStatus, HttpStatusCode.OK);
                    }
                }
            }
        }

        private async Task<CreditMemoData> assertChangingOrderStatus(CreditMemoData testOrder, OrderOrderStatus newOrderStatus, HttpStatusCode expectedStatusCode)
        {
            CreditMemoData order = await EndpointTests.AssertPostStatusCode<CreditMemoData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            string orderStatusEndpoint = $"{apiUrl}/{order.ID}/action/changestatus/{Convert.ToInt32(newOrderStatus)}";
            await EndpointTests.AssertPostStatusCode(this.client, orderStatusEndpoint, expectedStatusCode);
            return order;
        }

        private async Task<CreditMemoData> ChangeCreditMemoStatus(CreditMemoData creditMemo, OrderOrderStatus newStatus, HttpStatusCode expectedStatusCode)
        {
            string orderStatusEndpoint = $"{apiUrl}/{creditMemo.ID}/action/changestatus/{Convert.ToInt32(newStatus)}";
            await EndpointTests.AssertPostStatusCode(this.client, orderStatusEndpoint, expectedStatusCode);
            creditMemo.OrderStatusID = newStatus;
            return creditMemo;
        }

        private PaymentMaster AssertCreditGiftBalance(decimal? expected, int creditMemoID)
        {
            // get Payment related to CreditMemo
            var paymentMaster = ctx.PaymentMaster
                .AsNoTracking()
                .Include(t => t.PaymentApplications)
                .FirstOrDefault(
                    t => t.BID == BID &&
                    t.PaymentTransactionType == (byte)PaymentTransactionType.Credit_Gift &&
                    t.PaymentApplications.Any(ap => ap.OrderID == creditMemoID));

            // if we have null expectations, assert that no payment was created
            if (!expected.HasValue)
            {
                Assert.IsNull(paymentMaster, "Credit Gift record was found.");
                return paymentMaster;
            }

            // find the company's balance
            var creditMemo = ctx.CreditMemoData.AsNoTracking().First(t => t.BID == BID && t.ID == creditMemoID);
            var company = ctx.CompanyData.AsNoTracking().First(t => t.BID == BID && t.ID == creditMemo.CompanyID);

            Assert.IsNotNull(paymentMaster, "No Credit Gift payment found.");
            // Assert we have the expected balance
            Assert.AreEqual(expected, paymentMaster.NonRefBalance);
            Assert.AreEqual(expected, company.NonRefundableCredit);
            return paymentMaster;
        }

        private async Task AssertGLEntriesForCreditMemoIncome(decimal? expected, int creditMemoID)
        {
            var glentries = await ctx.GLData.Where(a => a.OrderID == creditMemoID && a.BID == BID && a.GLAccountID >= 4000 && a.GLAccountID <= 4999).ToListAsync();

            if (!expected.HasValue)
            {
                Assert.IsTrue(!glentries.Any(), "GL Entry record was found.");
                return;
            }

            Assert.IsNotNull(glentries);
            Assert.IsTrue(glentries.Any());
            Assert.IsTrue(Math.Abs(glentries.Sum(a => a.Amount)) == Math.Abs(expected.Value));
        }

        private async Task AssertGLEntriesForCreditMemoAR(decimal? expected, int creditMemoID)
        {
            var glentries = await ctx.GLData.Where(a => a.OrderID == creditMemoID && a.BID == BID && a.GLAccountID == 1200 && a.PaymentID==null).ToListAsync();

            if (!expected.HasValue)
            {
                Assert.IsTrue(!glentries.Any(), "GL Entry record was found.");
                return;
            }

            Assert.IsNotNull(glentries);
            Assert.IsTrue(glentries.Any());
            Assert.IsTrue(Math.Abs(glentries.Sum(a => a.Amount)) == Math.Abs(expected.Value));
        }

        private async Task<CreditMemoData> CreateCreditMemo()
        {
            var company = await CreateCompany();
            var creditMemo1 = Utils.GetTestCreditMemoData();
            var incomeAccounts = ctx.GLAccount.Where(account => account.GLAccountType == 40).ToList(); //Income
            var orderItem = Utils.GetTestOrderItemData(creditMemo1);
            var orderItemComponent = Utils.GetTestOrderItemComponentData(orderItem, incomeAccounts.First());
            orderItemComponent.PriceUnit = 100m;
            orderItemComponent.CostUnit = null;
            orderItem.CostTotal = 100m;
            orderItem.Components = new List<OrderItemComponent>
            {
                orderItemComponent
            };
            creditMemo1.Items = new List<OrderItemData>
            {
                orderItem
            };
            creditMemo1.CompanyID = company.ID;
            creditMemo1.PickupLocationID = await Utils.GetLocationID(client);
            creditMemo1.TaxGroupID = await Utils.GetTaxGroupID(client);
            creditMemo1.ProductionLocationID = creditMemo1.PickupLocationID;
            creditMemo1.PriceProductTotal = 100m;
            creditMemo1.PriceTax = 0m;

            return await EndpointTests.AssertPostStatusCode<CreditMemoData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(creditMemo1), HttpStatusCode.OK);
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task CreditAdjustmentAndGLEntriesTest()
        {
            var createdcreditMemo1 = await CreateCreditMemo();
            // test no credit adjustment when status is UNPOSTED
            AssertCreditGiftBalance(null, createdcreditMemo1.ID);
            await AssertGLEntriesForCreditMemoAR(null, createdcreditMemo1.ID);
            await AssertGLEntriesForCreditMemoIncome(null, createdcreditMemo1.ID);

            // changing credit memo to POSTED results in a credit adjustment
            await ChangeCreditMemoStatus(createdcreditMemo1, OrderOrderStatus.CreditMemoPosted, HttpStatusCode.OK);
            // Assert balance matched
            AssertCreditGiftBalance(createdcreditMemo1.PriceTotal, createdcreditMemo1.ID);
            await AssertGLEntriesForCreditMemoAR(-(createdcreditMemo1.PriceTotal), createdcreditMemo1.ID);
            await AssertGLEntriesForCreditMemoIncome(createdcreditMemo1.PriceTotal, createdcreditMemo1.ID);

            //// changing credit memo from POSTED to UNPOSTED results in reversing credit adjustment
            //await ChangeCreditMemoStatus(createdcreditMemo1, OrderOrderStatus.CreditMemoUnposted, HttpStatusCode.OK);
            //// Assert balance matched
            //AssertCreditGiftBalance(0, createdcreditMemo1.ID);

            // Asset New Credit Memo in Posted results in the proper Customer Credit adjustment.
            // Assert Voiding a Credit Memo in Posted results in the proper reversing Customer Credit adjustment.

            var createdcreditMemo2 = await CreateCreditMemo();

            await ChangeCreditMemoStatus(createdcreditMemo2, OrderOrderStatus.CreditMemoPosted, HttpStatusCode.OK);

            AssertCreditGiftBalance(createdcreditMemo2.PriceTotal, createdcreditMemo2.ID);
            await AssertGLEntriesForCreditMemoAR(-(createdcreditMemo2.PriceTotal), createdcreditMemo2.ID);
            await AssertGLEntriesForCreditMemoIncome(createdcreditMemo2.PriceTotal, createdcreditMemo2.ID);

            await ChangeCreditMemoStatus(createdcreditMemo2, OrderOrderStatus.CreditMemoVoided, HttpStatusCode.OK);

            AssertCreditGiftBalance(0, createdcreditMemo2.ID);
            await AssertGLEntriesForCreditMemoAR(0, createdcreditMemo2.ID);
            await AssertGLEntriesForCreditMemoIncome(0, createdcreditMemo2.ID);

            // Assert Voiding a Credit Memo in UnPosted results in no Customer Credit adjustment.
            var createdcreditMemo3 = await CreateCreditMemo();

            AssertCreditGiftBalance(null, createdcreditMemo3.ID);
            await AssertGLEntriesForCreditMemoAR(null, createdcreditMemo3.ID);
            await AssertGLEntriesForCreditMemoIncome(null, createdcreditMemo3.ID);

            await ChangeCreditMemoStatus(createdcreditMemo3, OrderOrderStatus.CreditMemoVoided, HttpStatusCode.OK);

            AssertCreditGiftBalance(null, createdcreditMemo3.ID);
            await AssertGLEntriesForCreditMemoAR(null, createdcreditMemo3.ID);
            await AssertGLEntriesForCreditMemoIncome(null, createdcreditMemo3.ID);

            // Assert Adjusting the Credit Memo up results in the proper Customer Credit adjustment.  The credit should only be adjusted by the amount of the net adjustment.
            // Assert Adjusting the Credit Memo down results in the proper Customer Credit adjustment. The credit should only be adjusted by the amount of the net adjustment.

            var createdcreditMemo4 = await CreateCreditMemo();

            await ChangeCreditMemoStatus(createdcreditMemo4, OrderOrderStatus.CreditMemoPosted, HttpStatusCode.OK);

            AssertCreditGiftBalance(createdcreditMemo4.PriceTotal, createdcreditMemo4.ID);
            await AssertGLEntriesForCreditMemoAR(-(createdcreditMemo4.PriceTotal), createdcreditMemo4.ID);
            await AssertGLEntriesForCreditMemoIncome(createdcreditMemo4.PriceTotal, createdcreditMemo4.ID);

            createdcreditMemo4.PriceProductTotal = createdcreditMemo4.PriceProductTotal - 5m;

            foreach (var item in createdcreditMemo4.Items)
            {
                item.CostTotal = createdcreditMemo4.PriceProductTotal.Value;
                foreach (var component in item.Components)
                    component.PriceUnit = createdcreditMemo4.PriceProductTotal.Value;
            }

            createdcreditMemo4 = await EndpointTests.AssertPutStatusCode<CreditMemoData>(client, $"{apiUrl}/{createdcreditMemo4.ID}", JsonConvert.SerializeObject(createdcreditMemo4), HttpStatusCode.OK);

            AssertCreditGiftBalance(createdcreditMemo4.PriceTotal, createdcreditMemo4.ID);
            await AssertGLEntriesForCreditMemoAR(-(createdcreditMemo4.PriceTotal), createdcreditMemo4.ID);
            await AssertGLEntriesForCreditMemoIncome(createdcreditMemo4.PriceTotal, createdcreditMemo4.ID);

            createdcreditMemo4.PriceProductTotal = createdcreditMemo4.PriceProductTotal + 5m;

            foreach (var item in createdcreditMemo4.Items)
            {
                item.CostTotal = createdcreditMemo4.PriceProductTotal.Value;
                foreach (var component in item.Components)
                    component.PriceUnit = createdcreditMemo4.PriceProductTotal.Value;
            }

            createdcreditMemo4 = await EndpointTests.AssertPutStatusCode<CreditMemoData>(client, $"{apiUrl}/{createdcreditMemo4.ID}", JsonConvert.SerializeObject(createdcreditMemo4), HttpStatusCode.OK);

            AssertCreditGiftBalance(createdcreditMemo4.PriceTotal, createdcreditMemo4.ID);
            await AssertGLEntriesForCreditMemoAR(-(createdcreditMemo4.PriceTotal), createdcreditMemo4.ID);
            await AssertGLEntriesForCreditMemoIncome(createdcreditMemo4.PriceTotal, createdcreditMemo4.ID);
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task PreventUnpostingPaidCreditMemo()
        {
            var company = await CreateCompany();
            var creditMemo1 = Utils.GetTestCreditMemoData();

            creditMemo1.CompanyID = company.ID;
            creditMemo1.PickupLocationID = await Utils.GetLocationID(client);
            creditMemo1.TaxGroupID = await Utils.GetTaxGroupID(client);
            creditMemo1.ProductionLocationID = creditMemo1.PickupLocationID;
            creditMemo1.PriceProductTotal = 100m;
            creditMemo1.PriceTax = 5m;
            creditMemo1.OrderStatusID = OrderOrderStatus.CreditMemoUnposted;
            var createdcreditMemo1 = await EndpointTests.AssertPostStatusCode<CreditMemoData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(creditMemo1), HttpStatusCode.OK);

            // test no credit adjustment when status is UNPOSTED
            AssertCreditGiftBalance(null, creditMemo1.ID);

            // changing credit memo to POSTED results in a credit adjustment
            await ChangeCreditMemoStatus(createdcreditMemo1, OrderOrderStatus.CreditMemoPosted, HttpStatusCode.OK);
            AssertCreditGiftBalance(createdcreditMemo1.PriceTotal, createdcreditMemo1.ID);

            // prevents changing paid credit memo to UNPOSTED 
            await ChangeCreditMemoStatus(createdcreditMemo1, OrderOrderStatus.CreditMemoUnposted, HttpStatusCode.BadRequest);
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task VoidingPaidCreditMemoTransfersCredit()
        {
            var createdcreditMemo1 = await CreateCreditMemo();

            // test no credit adjustment when status is UNPOSTED
            AssertCreditGiftBalance(null, createdcreditMemo1.ID);

            // changing credit memo to POSTED results in a credit adjustment
            await ChangeCreditMemoStatus(createdcreditMemo1, OrderOrderStatus.CreditMemoPosted, HttpStatusCode.OK);
            AssertCreditGiftBalance(createdcreditMemo1.PriceTotal, createdcreditMemo1.ID);

            // apply credit to another Order
            var hasAppliedCredit = await ApplyNonRefundableCreditToAnOrder(createdcreditMemo1.CompanyID, 50);
            Assert.IsTrue(hasAppliedCredit);

            // Void a credit memo
            createdcreditMemo1 = await VoidACreditMemo(createdcreditMemo1);
            AssertCreditGiftBalance(0m, createdcreditMemo1.ID);
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task VoidingPaidCreditMemoTransfersCreditTwice()
        {
            await VoidingPaidCreditMemoTransfersCredit();
            await VoidingPaidCreditMemoTransfersCredit();
        }

        private async Task<CreditMemoData> VoidACreditMemo(CreditMemoData creditMemo)
        {
            return await ChangeCreditMemoStatus(creditMemo, OrderOrderStatus.CreditMemoVoided, HttpStatusCode.OK);
        }

        private PaymentApplyCreditRequest NewApplyCreditRequest(int companyID, int? masterID, int orderID, bool isRefundable, decimal amount)
        {
            return new PaymentApplyCreditRequest()
            {
                PaymentTransactionType = isRefundable ? PaymentTransactionType.Payment_from_Refundable_Credit : PaymentTransactionType.Payment_from_Nonrefundable_Credit,
                MasterPaymentID = masterID,
                CompanyID = companyID,
                PaymentMethodId = isRefundable ? (int)PaymentMethodType.RefundableCredit : (int)PaymentMethodType.NonRefundableCredit,
                Applications = new List<PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest()
                    {
                        OrderID = null,
                        Amount = -amount,
                    },
                    new PaymentApplicationRequest()
                    {
                        OrderID = orderID,
                        Amount = amount,
                    },
                }
            };
        }

        private async Task<bool> ApplyNonRefundableCreditToAnOrder(int companyID, decimal amount)
        {
            var order = await CreateOrder(
                companyID: companyID,
                price: amount,
                amountPaid: amount
            );
            var request = NewApplyCreditRequest(companyID, null, order.ID, false, amount);
            await EndpointTests.AssertPostStatusCode(client, $"{applytoCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            return true;
        }

        [TestMethod]
        public async Task CreditMemoLineItemStatusChangeOnCreditMemoStatusChangeTest()
        {
            var ctx = GetApiContext();
            var testController = GetDMController();

            CreditMemoData testOrder = Utils.GetTestCreditMemoData();
            testOrder.TransactionType = (byte)OrderTransactionType.Memo;
            testOrder.OrderStatusID = OrderOrderStatus.CreditMemoUnposted;
            testOrder.ClassTypeID = (int)ClassType.CreditMemo;

            testOrder.CompanyID = ctx.CompanyContactLink.Where(x => x.BID == BID && x.IsActive == true).Select(x => x.CompanyID).FirstOrDefault();
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            //int contactId = await Utils.GetContactID(client, testOrder.CompanyID);
            int employeeId = await Utils.GetEmployeeID(client);
            var employeeRoleEnteredById = ctx.EmployeeRole.Where(er => er.Name == "Entered By").FirstOrDefault().ID;
            var employeeRoleSalesPersonId = ctx.EmployeeRole.Where(er => er.Name == "Salesperson").FirstOrDefault().ID;

            OrderItemData testOrderItem = new OrderItemData()
            {
                BID = testOrder.BID,
                HasCustomImage = false,
                HasDocuments = false,
                HasProof = false,
                ID = 1,
                OrderID = 0,
                ItemNumber = 1,
                Quantity = 1,
                Name = "test",
                IsOutsourced = false,
                OrderStatusID = testOrder.OrderStatusID,
                ItemStatusID = 81, //21=Pre-WIP
                ProductionLocationID = testOrder.ProductionLocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = testOrder.TaxGroupID,
                TaxGroupOV = false,
                IsTaxExempt = false,
                TransactionType = testOrder.TransactionType
            };

            testOrder.Items = new List<OrderItemData>();
            testOrder.Items.Add(testOrderItem);

            CreditMemoData createdOrder = await EndpointTests.AssertPostStatusCode<CreditMemoData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            string orderStatusEndpoint = $"{apiUrl}/{createdOrder.ID}/action/changestatus/{Convert.ToInt32(OrderOrderStatus.CreditMemoPosted)}";
            await EndpointTests.AssertPostStatusCode(this.client, orderStatusEndpoint, HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<CreditMemoData>(client, $"{apiUrl}/{createdOrder.ID}?ItemLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder);

            foreach (OrderItemData lineItemData in retrievedOrder.Items)
            {
                Assert.AreEqual(lineItemData.OrderStatusID, retrievedOrder.OrderStatusID);
            }
        }

    }
}
