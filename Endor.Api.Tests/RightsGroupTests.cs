﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

namespace Endor.Api.Tests
{
    [TestClass]
    public class RightsGroupTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/rightsgroupmenu";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task RightsGroupMenuTest()
        {
            // GET ../api/rightsgroupmenu/
            var rightsGroupMenuTree = await EndpointTests.AssertGetStatusCode<RightsGroupMenuTree[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(rightsGroupMenuTree);

            var first = rightsGroupMenuTree.FirstOrDefault();
            Assert.IsNotNull(first);

            // GET ../api/rightsgroupmenu/<menuid>/relatedmodules
            var rightsGroupRelatedModules = await EndpointTests.AssertGetStatusCode<int[]>(client, $"{apiUrl}/{first.ID}/relatedmodules", HttpStatusCode.OK);
            Assert.IsNotNull(rightsGroupRelatedModules);

        }

        private async Task<TimeCard> GetNewTimeCardModel()
        {
            var employee = await EndpointTests.AssertGetStatusCode<EmployeeData[]>(client, "/api/employee", HttpStatusCode.OK);

            return new TimeCard()
            {
                BID = 1,
                EmployeeID = employee.FirstOrDefault()?.ID ?? 0,
                StartDT = DateTime.Now,
                EndDT = DateTime.Now,
                TimeInMin = 0,
                PaidTimeInMin = 0,
                IsClosed = true,
                IsAdjusted = true,
                AdjustedByEmployeeID = employee.FirstOrDefault()?.ID ?? 0,
                AdjustedDT = DateTime.Now,
                MetaData = "Test"
            };
        }
    }
}
