﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Endor.Api.Web.Controllers;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using AssertX = Xunit.Assert;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    /*
    [TestClass]
    public class MessageHeaderEndpointTest : CommonControllerTestClass
    {
        public const string apiUrl = "/api/message";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        private MessageParticipantInfo GetParticipantInfo()
        {
            return new MessageParticipantInfo()
            {
                ID = -99,

                Channel = MessageChannelType.Email,
                IsMergeField = false,
                ModifiedDT = DateTime.UtcNow,

            };
        }
        private Message GetMessage()
        {
            return new Message()
            {
                ID = -99,
                ModifiedDT = DateTime.UtcNow,
                ReceivedDT = DateTime.UtcNow,
                Channels = MessageChannelType.None,
                Subject = "The Quick",
                HasBody = true,
                Body = "<b>brown fox</>",

            };
        }

        [TestInitialize]
        public void Initialize()
        {

        }

        [TestMethod]
        public async Task TestMessageHeaderDelete()
        {
            await TestMessageHeader(0);
        }

        [TestMethod]
        public async Task TestMessageHeaderUnDelete()
        {
            await TestMessageHeader(1);
        }

        [TestMethod]
        public async Task TestMessageHeaderMarkAsRead()
        {
            await TestMessageHeader(2);
        }
        [TestMethod]
        public async Task TestMessageHeaderMarkAsUnRead()
        {
            await TestMessageHeader(3);
        }

        [TestMethod]
        public async Task TestMessageHeaderMarkAllAsRead()
        {
            await TestMessageHeader(4);
        }

        [TestMethod]
        public async Task TestMessageHeaderMarkAsAllAsDeleted()
        {
            await TestMessageHeader(5);
        }

        [TestMethod]
        public async Task TestMessagePost()
        {
            await TestMessageHeader(6);
        }


        [TestMethod]
        public async Task TestMessageGetByHeaderId()
        {
            await TestMessageHeader(7);
        }

        [TestMethod]
        public async Task TestMessageGetByChannelId()
        {
            await TestMessageHeader(8);
        }


        private async Task TestMessageHeader(int testType)
        {
            var postedMessageID = -1;
            short employeeID = 0;
            if (testType != 8)
            {
                ApiContext ctx = GetApiContext(1);
                EmployeeData emp = ctx.EmployeeData.FirstOrDefault(e => e.IsActive);

                employeeID = emp.ID;

                var participantInfoFrom = GetParticipantInfo();
                participantInfoFrom.ParticipantRoleType = MessageParticipantRoleType.From;
                participantInfoFrom.EmployeeID = employeeID;

                var participantInfoTo = GetParticipantInfo();
                participantInfoTo.ParticipantRoleType = MessageParticipantRoleType.To;
                participantInfoTo.EmployeeID = employeeID;

                var message = GetMessage();
                message.EmployeeID = employeeID;
                message.Participants = new[] { participantInfoFrom, participantInfoTo };

                var postedMessageHeader = await EndpointTests.AssertPostStatusCode<Message>(client, apiUrl, message, HttpStatusCode.OK);
                postedMessageID = postedMessageHeader.ID;
            }


            switch (testType)
            {
                case 0:
                    await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{postedMessageID}",
                        HttpStatusCode.OK);
                    break;
                case 1:
                    await EndpointTests.AssertPostStatusCode(client,
                        $"{apiUrl}/{postedMessageID}/action/undelete",
                        HttpStatusCode.OK);
                    break;
                case 2:
                    await EndpointTests.AssertPostStatusCode(client,
                        $"{apiUrl}/{postedMessageID}/action/markasread",
                        HttpStatusCode.OK);
                    break;

                case 3:
                    await EndpointTests.AssertPostStatusCode(client,
                        $"{apiUrl}/{postedMessageID}/action/markasunread",
                        HttpStatusCode.OK);
                    break;

                case 4:
                    await EndpointTests.AssertPostStatusCode(client,
                        $"{apiUrl}/channel/{MessageChannelType.Email}/action/markallread?EmployeeID={employeeID}",
                        HttpStatusCode.OK);
                    break;
                case 5:
                    await EndpointTests.AssertPostStatusCode(client,
                        $"{apiUrl}/channel/{MessageChannelType.Email}/action/deleteall?EmployeeID={employeeID}",
                        HttpStatusCode.OK);
                    break;
                case 6:
                    break;
                case 7:
                    await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{postedMessageID}", HttpStatusCode.OK);
                    break;
                case 8:
                    await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/channel/{MessageChannelType.Email}", HttpStatusCode.OK);
                    break;
            }

            if (postedMessageID != -1)
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"/api/message/{postedMessageID}",
                    HttpStatusCode.OK);
            }
        }

        [TestMethod]
        //To Get this to pass - valid email credentials must be entered
        public async Task TestEmailSending()
        {
            var emailAccountApiUrl = "/api/emailaccount";
            var ctx = GetApiContext(1);
            
            ctx.SaveChanges();
            if (!ctx.DomainEmail.Where(d => d.ID == -99).Any())
            {
                //insert
                DomainEmail domainEmail = new DomainEmail()
                {
                    BID = 1,
                    ID = -99,
                    CreatedDate = DateTime.UtcNow,
                    IsActive = true,
                    DomainName = "corebridge.net",
                    SMTPAddress = "smtp.gmail.net",
                    ProviderType = EmailProviderType.GoogleGSuite,
                    SMTPConfigurationType = 0,
                    SMTPPort = 465,
                    SMTPSecurityType = EmailSecurityType.SSL,
                };
                ctx.DomainEmail.Add(domainEmail);
                ctx.SaveChanges();
            }

            var firstEmployee = ctx.EmployeeData.First();
            var originalDefaultEmailId = firstEmployee.DefaultEmailAccountID;
            #region CREATE
            var newEmailAccount = new EmailAccountData()
            {
                BID = 1,
                UserName = "justin",
                DomainName = "corebridge.net",
                Credentials = "sail2cor",
                StatusType = EmailAccountStatus.Pending
            };
            newEmailAccount.DomainEmailID = -99;
            var email = await EndpointTests.AssertPostStatusCode<EmailAccountData>(client, $"{emailAccountApiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);
            
            #endregion
            
            #region Test Authentication

            await EndpointTests.AssertPostStatusCode<EmailAccountData>(client, $"{emailAccountApiUrl}/{email.ID}/action/test", JsonConvert.SerializeObject(""), HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{emailAccountApiUrl}/{email.ID}/action/setdefault/employee/{firstEmployee.ID}", JsonConvert.SerializeObject(""), HttpStatusCode.OK);

            #endregion

            #region Test Email

            string emailURL = $"{apiUrl}/email";
            emailURL += "?From=" + firstEmployee.ID.ToString();
            emailURL += "&ToEmployeeID=" + firstEmployee.ID.ToString();
            emailURL += "&EmailAccountID=" + email.ID.ToString();
            var message = await EndpointTests.AssertPostStatusCode<MessageHeader>(client, emailURL, JsonConvert.SerializeObject("test email"), HttpStatusCode.OK);

            #endregion

            #region DELETE
            firstEmployee.DefaultEmailAccountID = originalDefaultEmailId;
            ctx.EmployeeData.Update(firstEmployee);
            ctx.SaveChanges();

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{message.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{emailAccountApiUrl}/{email.ID}", HttpStatusCode.NoContent);

            var accountsToDelete = ctx.EmailAccountData.Where(d => d.DomainEmailID == -99).ToList();
            var IDsToDelete = accountsToDelete.Select(a => a.ID);
            foreach (var emp in ctx.EmployeeData.Where(e => IDsToDelete.Contains(e.DefaultEmailAccountID.GetValueOrDefault())).ToList())
            {
                emp.DefaultEmailAccountID = null;
                ctx.EmployeeData.Update(emp);
            }
            ctx.SaveChanges();
            ctx.RemoveRange(accountsToDelete);
            ctx.SaveChanges();
            ctx.RemoveRange(ctx.DomainEmail.Where(d => d.ID == -99).ToList());
            ctx.SaveChanges();
            #endregion
        }

        [TestCleanup]
        public void Teardown()
        {

        }
    }*/
}
