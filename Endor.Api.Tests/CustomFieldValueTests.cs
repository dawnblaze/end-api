﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Endor.Api.Web.Classes;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Services;

namespace Endor.Api.Tests
{

    [TestClass]
    public class CustomFieldValueTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/cf/value";
        public const string contactApiUrl = "/Api/Contact";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        [TestMethod]
        public async Task TestCustomFieldValuesCRUDOperations()
        {
            var ctx = GetApiContext();
            var adHocContact = GetNewAdHocContact();
            var testCustomFieldDefinition = Utils.GetCustomFieldDefinition();
            var inactivetestCustomFieldDefinition = Utils.GetCustomFieldDefinition();

            try
            {
            #region CREATE
            inactivetestCustomFieldDefinition.IsActive = false;

            //test: expect OK
            adHocContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{contactApiUrl}", JsonConvert.SerializeObject(adHocContact), HttpStatusCode.OK);
            Assert.IsNotNull(adHocContact);

            var createdCustomFieldDefinition = await EndpointTests.AssertPostStatusCode<CustomFieldDefinition>(client, $"/api/cf/definition/", JsonConvert.SerializeObject(testCustomFieldDefinition), HttpStatusCode.OK);
            
            var inactiveCustomFieldDefinition = await EndpointTests.AssertPostStatusCode<CustomFieldDefinition>(client, $"/api/cf/definition/", JsonConvert.SerializeObject(inactivetestCustomFieldDefinition), HttpStatusCode.OK);

            var values = new CustomFieldValue[3];
            values[0] = GetNewCustomFieldValue(createdCustomFieldDefinition.ID, "abs");
            values[1] = GetNewCustomFieldValue(inactiveCustomFieldDefinition.ID, "cba");
            values[2] = GetNewCustomFieldValue(createdCustomFieldDefinition.ID, null);

            var createdCustomFieldValueDefinition = await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{ClassType.Contact.ID()}/{adHocContact.ID}", JsonConvert.SerializeObject(values), HttpStatusCode.OK);

            #endregion
            
            #region RETRIEVE

            // Test Get All
            var retrievedCustomFieldDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldValue[]>(client, $"{apiUrl}/{ClassType.Contact.ID()}/{adHocContact.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedCustomFieldDefinitions);
            Assert.IsNotNull(retrievedCustomFieldDefinitions.Count()==1);

            //test inactive
            var activeDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldValue[]>(client, $"{apiUrl}/{ClassType.Contact.ID()}/{adHocContact.ID}/?IsActive=false", HttpStatusCode.OK);
            Assert.IsNotNull(activeDefinitions);
            Assert.IsNotNull(activeDefinitions.Count() == 2);

            //Test Null
            var nullDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldValue[]>(client, $"{apiUrl}/{ClassType.Contact.ID()}/{adHocContact.ID}/?IncludeNull=false", HttpStatusCode.OK);
            Assert.IsNotNull(nullDefinitions);
            Assert.IsNotNull(nullDefinitions.Count() == 2);

            
            // Test Get By CFID
            var retrievedCustomFieldDefinition = await EndpointTests.AssertGetStatusCode<CustomFieldValue>(client, $"{apiUrl}/{ClassType.Contact.ID()}/{adHocContact.ID}/{createdCustomFieldDefinition.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedCustomFieldDefinition);
            //Assert.IsNotNull(retrievedCustomFieldDefinition.Count() == 2);

            #endregion

            #region UPDATE
            var updatedValues = GetNewCustomFieldValue(createdCustomFieldDefinition.ID, "jkl");

            var updatesCustomFieldValueDefinition = await EndpointTests.AssertPutStatusCode<CustomFieldValue[]>(client, $"{apiUrl}/{ClassType.Contact.ID()}/{adHocContact.ID}/{createdCustomFieldDefinition.ID}", JsonConvert.SerializeObject(updatedValues), HttpStatusCode.OK);
            Assert.IsNotNull(updatesCustomFieldValueDefinition.Count() == 2);

            #endregion

            #region DELETE

            var removedCustomFieldDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldValue[]>(client, $"{apiUrl}/{ClassType.Contact.ID()}/{adHocContact.ID}?IsActive=false", HttpStatusCode.OK);
            Assert.IsNotNull(removedCustomFieldDefinitions.Count() == 1);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{ClassType.Contact.ID()}/{adHocContact.ID}/{createdCustomFieldDefinition.ID}", HttpStatusCode.OK);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{ClassType.Contact.ID()}/{adHocContact.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/cf/definition/{createdCustomFieldDefinition.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/cf/definition/{inactiveCustomFieldDefinition.ID}", HttpStatusCode.NoContent);

            var ccd = await ctx.ContactCustomData.Where(c => c.BID == 1 && c.ID == adHocContact.ID).ToListAsync();
            ctx.ContactCustomData.RemoveRange(ccd);
            await ctx.SaveChangesAsync();

            var contact = await ctx.ContactData.FirstOrDefaultAsync(c => c.BID == 1 && c.ID == adHocContact.ID);
            ctx.ContactData.Remove(contact);
            await ctx.SaveChangesAsync();

            #endregion
            }
            finally
            {
                ctx.RemoveRange(ctx.CustomFieldDefinition.Where(x => x.BID == testCustomFieldDefinition.BID && (
                    x.ID == testCustomFieldDefinition.ID ||
                    x.ID == inactivetestCustomFieldDefinition.ID
                )).ToArray());
                ctx.RemoveRange(ctx.ContactData.Where(x => x.BID == adHocContact.BID && x.ID == adHocContact.ID).ToArray());
                ctx.SaveChanges();
            }
        }

        private CustomFieldValue GetNewCustomFieldValue(short ID, string val)
        {
            return new CustomFieldValue()
            {
                ID = ID,
                V = val
            };
        }

        private ContactData GetNewAdHocContact(CompanyData companyData = null)
        {
            return new ContactData()
            {
                BID = 1,
                First = "Test First",
                Last = $"Test Last {DateTime.UtcNow}",
                CompanyContactLinks = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = 1,
                        Company = companyData ?? new CompanyData(),
                        CompanyID = companyData?.ID ?? 0
                    }
                }
            };
        }

        [TestMethod]
        public async Task CompanyCustomFieldTest()
        {
            var ctx = GetApiContext();

            var business = ctx.BusinessData
                .Include(b => b.Locations)
                .Single(b => b.BID == 1);
            var defaultLocation = business.Locations.FirstOrDefault(l => l.IsDefault);

            CompanyData company = new CompanyData()
            {
                ID = -99,
                Name = "Test Company",
                IsActive = true,
                BID = business.ID,
                StatusID = 2,
                LocationID = defaultLocation.ID,
            };
            
            ctx.CompanyData.Add(company);

            CustomFieldDefinition cfd = new CustomFieldDefinition()
            {
                ID = -99,
                BID = 1,
                AllowMultipleValues = false,
                AppliesToClassTypeID = (int)ClassType.Company,
                DataType = DataType.String,
                DisplayType = CustomFieldNumberDisplayType.General,
                ElementType = AssemblyElementType.SingleLineText,
                IsActive = true,
                IsSystem = false,
                Name = "Temp CF",
                Label = "Temp CF",
                Description = "Temp CF",
            };

            ctx.CustomFieldDefinition.Add(cfd);

            await ctx.SaveChangesAsync();

            const string cfJson = "[{\"ID\": \"-99\", \"V\": \"TEST\"}]";

            try
            {
                company.CFValuesJSON = cfJson;
                company.SaveCFValues(ctx);

                var testCompany = await EndpointTests.AssertGetStatusCode<dynamic>(client, $"api/company/{company.ID}?CustomFields=Full", HttpStatusCode.OK);

                Assert.IsNotNull(testCompany.CFValues);

            }
            finally
            {
                ctx.CompanyData.Remove(company);
                ctx.CustomFieldDefinition.Remove(cfd);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task IHasCustomFieldExtensionTests()
        {
            var ctx = GetApiContext();

            var business = ctx.BusinessData
                .Include(b => b.Locations)
                .Single(b => b.BID == 1);
            var defaultLocation = business.Locations.FirstOrDefault(l => l.IsDefault);

            CompanyData company = new CompanyData()
            {
                ID = -99,
                Name = "Test Company",
                IsActive = true,
                BID = business.ID,
                StatusID = 2,
                LocationID = defaultLocation.ID,
            };

            ctx.CompanyData.Add(company);

            CustomFieldDefinition cfd = new CustomFieldDefinition()
            {
                ID = -99,
                BID = 1,
                AllowMultipleValues = false,
                AppliesToClassTypeID = (int)ClassType.Company,
                DataType = DataType.String,
                DisplayType = CustomFieldNumberDisplayType.General,
                ElementType = AssemblyElementType.SingleLineText,
                IsActive = true,
                IsSystem = false,
                Name = "Temp CF",
                Label = "Temp CF",
                Description = "Temp CF",
            };

            ctx.CustomFieldDefinition.Add(cfd);

            await ctx.SaveChangesAsync();

            const string cfJson = "[{\"ID\":\"-99\",\"V\":\"TEST\"}]";

            try
            {
                company.LoadCFValues(ctx);
                Assert.IsNull(company.CFValuesJSON);

                company.CFValuesJSON = cfJson;
                company.SaveCFValues(ctx);

                company.LoadCFValues(ctx);
                Assert.AreEqual(cfJson, company.CFValuesJSON);
            }
            finally
            {
                ctx.CompanyData.Remove(company);
                ctx.CustomFieldDefinition.Remove(cfd);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task CanGetProductAssemblyTypeCustomFieldTest()
        {
            var ctx = GetApiContext();

            var business = ctx.BusinessData
                .Include(b => b.Locations)
                .Single(b => b.BID == 1);
            
            AssemblyData assembly = new AssemblyData()
            {
                ID = -99,
                BID = business.ID,
                ClassTypeID = (int)ClassType.Assembly,
                AssemblyType = AssemblyType.Product,
                Name = "My Test Product Assembly",
                
            };

            ctx.AssemblyData.Add(assembly);


            CustomFieldOtherData customFieldOtherData = new CustomFieldOtherData()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.CustomFieldOther,
                AppliesToClassTypeID = (int)ClassType.Assembly,
                DataXML = "<root><data><name>Test</name><email>Test@test.com</email></data></root>"

            };

            ctx.CustomFieldOtherData.Add(customFieldOtherData);

            await ctx.SaveChangesAsync();

            try
            {
                
                var result = await EndpointTests.AssertGetStatusCode<List<CustomFieldValue>>(client, $"api/cf/value/{(int)ClassType.Assembly}/-99/", HttpStatusCode.OK);

                Assert.IsNotNull(result);
                Assert.IsTrue(result is List<CustomFieldValue>);

            }
            finally
            {
                ctx.AssemblyData.Remove(assembly);
                ctx.CustomFieldOtherData.Remove(customFieldOtherData);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task DoNotAllowSaveMachineTemplateAssemblyTypeCustomFieldValues()
        {
            
            var ctx = GetApiContext();

            var business = ctx.BusinessData
                .Include(b => b.Locations)
                .Single(b => b.BID == 1);

            AssemblyData assembly = new AssemblyData()
            {
                ID = -99,
                BID = business.ID,
                ClassTypeID = (int)ClassType.Assembly,
                AssemblyType = AssemblyType.MachineTemplate,
                Name = "My Test Product Assembly",

            };

            ctx.AssemblyData.Add(assembly);

            await ctx.SaveChangesAsync();

            var values = new CustomFieldValue[3];
            values[0] = GetNewCustomFieldValue(-99, "hello");
            values[1] = GetNewCustomFieldValue(-98, "world");
            values[2] = GetNewCustomFieldValue(-97, "test");

            try
            {

                var createdCustomFieldValueDefinition = await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{ClassType.Assembly.ID()}/-99", JsonConvert.SerializeObject(values), HttpStatusCode.BadRequest);


            }
            finally
            {
                ctx.AssemblyData.Remove(assembly);
                await ctx.SaveChangesAsync();
            }

            
        }
    }
}
