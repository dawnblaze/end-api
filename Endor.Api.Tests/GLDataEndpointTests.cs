﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.GLEngine;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Classes.Responses;

namespace Endor.Api.Tests
{
    [TestClass]
    public class GLDataEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/gldata";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }
        public static short BID = 1;

        [TestMethod]
        public async Task GetGLDataTest()
        {
            ApiContext ctx = GetApiContext();
            //Delete Entries
            DeleteTables(ctx);
            var GLActivity = GetSampleGLActivity(ctx);
            ctx.ActivityGlactivity.Add(GLActivity);
            ctx.SaveChanges();

            var GLData = GetSampleGLData(ctx, GLActivity);
            ctx.GLData.Add(GLData);
            ctx.SaveChanges();

            var glData = await EndpointTests.AssertGetStatusCode<GLData>(client, $"{apiUrl}/-99", HttpStatusCode.OK);
            Assert.IsTrue(glData.ID == -99);

            //Delete Entries
            DeleteTables(ctx);
        }

        [TestMethod]
        public async Task GetGLDataInvalid()
        {
            //One or more of the parameters are required. It is not possible to obtain all records.
            await EndpointTests.AssertGetStatusCode<object>(client, $"{apiUrl}/", HttpStatusCode.BadRequest);

            //The Take parameter should be specified, the maximum value is 5000
            await EndpointTests.AssertGetStatusCode<object>(client, $"{apiUrl}/?OrderId=-99", HttpStatusCode.BadRequest);

            //The maximum records allowed in one request is 5000.
            await EndpointTests.AssertGetStatusCode<object>(client, $"{apiUrl}/?Take=9000", HttpStatusCode.BadRequest);

            //Only Zero or One of the OrderID, CompanyID, or GLActivityID can be used.
            await EndpointTests.AssertGetStatusCode<object>(client, $"{apiUrl}/?OrderId=-99&GLActivityID=-99", HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public async Task GetGLDataValid()
        {
            ApiContext ctx = GetApiContext();
            //Delete Entries
            DeleteTables(ctx);
            var GLActivity = GetSampleGLActivity(ctx);
            ctx.ActivityGlactivity.Add(GLActivity);
            ctx.SaveChanges();

            ctx.GLData.Add(GetSampleGLData(ctx, GLActivity));
            ctx.GLData.Add(GetSampleGLData(ctx, GLActivity, -98));
            ctx.GLData.Add(GetSampleGLData(ctx, GLActivity, -97));
            ctx.SaveChanges();

            //Basic Filters
            var glDataBasic = await EndpointTests.AssertGetStatusCode<List<GLData>>(client, $"{apiUrl}?Take=3&Skip=0&GLAccountID={ctx.GLAccount.FirstOrDefault().ID}", HttpStatusCode.OK);
            Assert.IsTrue(glDataBasic.Count == 3);

            //Basic Filters
            var glDataTake2 = await EndpointTests.AssertGetStatusCode<List<GLData>>(client, $"{apiUrl}?Take=2&Skip=0&GLAccountID={ctx.GLAccount.FirstOrDefault().ID}", HttpStatusCode.OK);
            Assert.IsTrue(glDataTake2.Count == 2);

            //Basic Filters
            var glDataTake2SKip1 = await EndpointTests.AssertGetStatusCode<List<GLData>>(client, $"{apiUrl}?Take=2&Skip=1&GLAccountID={ctx.GLAccount.FirstOrDefault().ID}", HttpStatusCode.OK);
            Assert.IsTrue(glDataTake2SKip1.Count == 1);

            //Basic Filters 2
            var glDataBasic2 = await EndpointTests.AssertGetStatusCode<List<GLData>>(client, $"{apiUrl}?Take=3&Skip=0&GLAccountID={ctx.GLAccount.FirstOrDefault().ID}&CompanyID={ctx.CompanyData.FirstOrDefault().ID}", HttpStatusCode.OK);
            Assert.IsTrue(glDataBasic2.Count == 3);

            //Delete Entries
            DeleteTables(ctx);
        }

        [TestMethod]
        public async Task GetGLDataSummaryValid()
        {
            ApiContext ctx = GetApiContext();
            //Delete Entries
            DeleteTables(ctx);
            var GLActivity = GetSampleGLActivity(ctx);
            ctx.ActivityGlactivity.Add(GLActivity);
            ctx.SaveChanges();

            ctx.GLData.Add(GetSampleGLData(ctx, GLActivity));
            ctx.GLData.Add(GetSampleGLData(ctx, GLActivity, -98));
            ctx.GLData.Add(GetSampleGLData(ctx, GLActivity, -97));
            ctx.SaveChanges();

            //Basic Filters
            var glDataBasic = await EndpointTests.AssertGetStatusCode<List<GLDataSummaryResponse>>(client, $"{apiUrl}/summary?&GLAccountID={ctx.GLAccount.FirstOrDefault().ID}", HttpStatusCode.OK);
            Assert.IsTrue(glDataBasic.Count == 1);
            Assert.IsTrue(glDataBasic.First().Amount == 300);

            //Delete Entries
            DeleteTables(ctx);
        }

        private ActivityGlactivity GetSampleGLActivity(ApiContext ctx)
        {
            return new ActivityGlactivity()
            {
                ID = -99,
                BID = 1,
                IsActive = true,
                Name = "Test GLActivity",
                Subject = "Test",
                CompletedDT = new DateTime()
            };
        }

        private GLData GetSampleGLData(ApiContext ctx, ActivityGlactivity glActivity, int ID = -99)
        {
            var firstLocation = ctx.LocationData.FirstOrDefault();
            var GLAccount = ctx.GLAccount.FirstOrDefault();
            var Company = ctx.CompanyData.FirstOrDefault();
            return new GLData()
            {
                ID = ID,
                BID = 1,
                LocationID = firstLocation.ID,
                GLAccountID = GLAccount.ID,
                CompanyID = Company.ID,
                AccountingDT = DateTime.Now,
                Amount = 100,
                ActivityID = glActivity.ID
            };
        }

        public static void DeleteTables(ApiContext ctx)
        {
            ctx.GLData.RemoveRange(ctx.GLData.Where(o => o.ID < 0));
            ctx.ActivityGlactivity.RemoveRange(ctx.ActivityGlactivity.Where(o => o.ID < 0));
            ctx.SaveChanges();
        }
    }
}
