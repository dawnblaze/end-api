﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Endor.Api.Web.Controllers;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using AssertX = Xunit.Assert;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using Newtonsoft.Json;
using Endor.DocumentStorage.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Endor.Tenant;
using System.IO;
using System.Reflection;
using System.Collections.Generic;

namespace Endor.Api.Tests
{
    [TestClass]
    public class DMInterfaceTests: CommonControllerTestClass
    {
        [TestMethod]
        [Ignore]
        public async Task GetInvalidEmployeeBucket()
        {
            var testController = GetDMController();
            var result = await testController.GetDocuments(new DMController.RequestModel()
            {
                id = 100,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Data
            });
            AssertX.IsType<BadRequestObjectResult>(result);
        }

        [TestMethod]
        public async Task EmptyPost()
        {
            var testController = GetDMController();
            var result = await testController.Post(new DMController.RequestModel()
            {
                id = 100,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.txt"
            }, null, null);
            AssertX.IsType<BadRequestObjectResult>(result);
        }

        [TestMethod]
        [DeploymentItem(TestTextFilePath)]
        [Ignore]
        public async Task InvalidPostBucket()
        {
            var testController = GetDMController();
            AddFileToForm(testController, "test.txt");

            var reportPostResult = await testController.Post(new DMController.RequestModel()
            {
                id = 100,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Reports,
                PathAndFilename = "test.txt"
            }, null, null);
            AssertX.IsType<BadRequestObjectResult>(reportPostResult);

            var templateReportsPostResult = await testController.Post(new DMController.RequestModel()
            {
                classFolder = "template",
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Reports,
                PathAndFilename = "test.txt"
            }, null, null);
            AssertX.IsType<BadRequestObjectResult>(templateReportsPostResult);

            var staticDataPostResult = await testController.Post(new DMController.RequestModel()
            {
                classFolder = "static",
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.txt"
            }, null, null);
            AssertX.IsType<BadRequestObjectResult>(staticDataPostResult);
        }

        [TestMethod]
        [DeploymentItem(TestTextFilePath)]
        public async Task FilterOutDirectoryBlobsTest()
        {
            string foldername = "folder1";
            string folderPath = $"{foldername}/";
            DMController.RequestModel baseModel = new DMController.RequestModel()
            {
                id = 101,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents
            };

            var testController = GetDMController();
            await CleanSlate(testController, baseModel);
            
            string fileAssetName = $"{folderPath}{TestTextFileName}";
            //C
            AddFileToForm(testController, TestTextFileName);
            var create = baseModel.Clone();
            create.PathAndFilename = fileAssetName;
            var okPost = AssertX.IsType<OkObjectResult>(await testController.Post(create, null, null));
            Assert.AreEqual(200, okPost.StatusCode);
            AssertX.IsType<DMItem>(okPost.Value);

            testController.Request.Form = null;

            //R
            var okGet = AssertX.IsType<OkObjectResult>(await testController.GetDocuments(baseModel.Clone()));
            Assert.AreEqual(200, okPost.StatusCode);
            var docs = AssertX.IsType<List<DMItem>>(okGet.Value);
            // Only should return the folder not the folder's blob
            Assert.AreEqual(1, docs.Count);
            Assert.AreEqual(foldername, docs[0].Name);

            // folder should contain the 1 blob, not the folder's blob
            Assert.IsNotNull(docs[0].Contents);
            Assert.AreEqual(1, docs[0].Contents.Count);
            Assert.AreEqual(TestTextFileName, docs[0].Contents[0].Name);
            Assert.AreEqual(folderPath, docs[0].Contents[0].Path);
        }


        [TestMethod]
        public async Task RemoteUrlPost()
        {
            var testController = GetDMController();
            var result = await testController.Post(new DMController.RequestModel()
            {
                id = 100,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.svg"
            }, null, "https://corebridge.net/wp-content/uploads/2019/01/AdobeStock_202169599-e1551199093506.jpg");
            AssertX.IsType<OkObjectResult>(result);
            AssertX.IsType<DMItem>(((OkObjectResult)result).Value);
        }
    }
}
