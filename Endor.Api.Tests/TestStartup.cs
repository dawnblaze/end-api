﻿using Endor.Api.Web;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;
using Acheve.AspNetCore.TestHost.Security;
using Endor.Tenant;
using Endor.Tasks;
using Endor.RTM;
using System.IO;
using Newtonsoft.Json;
using Endor.Logging.Client;
using Endor.Pricing;
using Endor.EF;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Endor.DNSManagement;
using Microsoft.Extensions.Hosting;
using Endor.CBEL.Autocomplete;

namespace Endor.Api.Tests
{
    public class TestStartup : BaseStartup
    {
        protected const string SQLConfigName = "SQL";
        protected const string BlobStorageConfigName = "BlobStorage";
        protected bool PerformCompute = false;
        protected IConfigurationRoot _config;

        public TestStartup(IWebHostEnvironment env) : base(env)
        {
        }

        private static Dictionary<string, string> GetLocalSettings()
        {            
            string file = "..\\..\\..\\client-secrets.json";
            if (!File.Exists(file))
            {
                // If you don't have one, we'll make one here.
                File.WriteAllText(file, @"{
    ""SQL"": ""Data Source=.\\SQLEXPRESS;Initial Catalog=\""Dev.Endor.Business.DB1\"";User ID=cyrious;Password=watankahani"",
    ""BlobStorage"":  ""UseDevelopmentStorage=true""
}");
            }

            string text = File.ReadAllText(file);
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(text);
            return result;
        }

        public override IConfigurationBuilder GetConfigurationBuilder(IWebHostEnvironment env)
        {
            var configBuilder = base.GetConfigurationBuilder(env);
            configBuilder.AddInMemoryCollection(GetLocalSettings());

            return configBuilder;
        }

        public override void AddAuthentication(IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = TestServerAuthenticationDefaults.AuthenticationScheme;
            })
            .AddTestServerAuthentication();
        }
        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime appLifetime)
        {
            app.UseAuthentication();
            base.Configure(app, env, loggerFactory, appLifetime);
        }

        public override void ConfigureExternalEndorServices(IServiceCollection services)
        {
            var tdc = GetMockTenantDataCache();
            services.AddSingleton<ITenantDataCache>(tdc);
            services.AddSingleton<ITaskQueuer>(new MockTaskQueuer(tdc));//>(new HttpTaskQueuer(Configuration["Endor:TasksAPIURL"]));
            services.AddSingleton<RemoteLogger>((x) => new RemoteLogger(tdc)); //new RemoteLogger(tdc));
            services.AddTransient<IRTMPushClient>((x) => new MockRealtimeMessagingPushClient());
            //services.AddScoped<IPricingEngine, MockPricingEngine>();
            services.AddScoped<IPricingEngine>((x) =>
            {
                ApiContext context = x.GetService<ApiContext>();
                IHttpContextAccessor httpContext = x.GetService<IHttpContextAccessor>();
                ClaimsIdentity identity = httpContext.HttpContext.User.Identity as ClaimsIdentity;
                short BID;
                short.TryParse(identity.FindFirst(ClaimNameConstants.BID).Value, out BID);
                RemoteLogger logger = new RemoteLogger(tdc);
                return new PricingEngine(BID, context, tdc, logger);
            });
            services.AddSingleton<IDNSManager>(new DNSManager(Configuration["Endor:DNSManager:tenantid"],
                                                              Configuration["Endor:DNSManager:client_id"],
                                                              Configuration["Endor:DNSManager:client_secret"],
                                                              Configuration["Endor:DNSManager:subscription_id"],
                                                              Configuration["Endor:DNSManager:resourceGroup"],
                                                              Configuration["Endor:DNSManager:appName"],
                                                              Configuration["Endor:DNSManager:hostNameDNSTarget"],
                                                              Configuration["Endor:DNSManager:resourceLocation"]
                                                                ));
            services.AddSingleton<IMigrationHelper, MigrationHelper>();

            services.AddScoped<IAutocompleteEngine>((x) => {
                ApiContext context = x.GetService<ApiContext>();
                IHttpContextAccessor httpContext = x.GetService<IHttpContextAccessor>();
                ClaimsIdentity identity = httpContext.HttpContext.User.Identity as ClaimsIdentity;
                short BID;
                short.TryParse(identity.FindFirst(ClaimNameConstants.BID).Value, out BID);
                ITenantDataCache cache = x.GetService<ITenantDataCache>();
                RemoteLogger logger = x.GetService<RemoteLogger>();
                return new AutocompleteEngine(BID, context, cache, logger);
            });
        }

        private MockTenantDataCache GetMockTenantDataCache()
        {
            var tdc = new MockTenantDataCache(Configuration[SQLConfigName], Configuration[BlobStorageConfigName]);
            return tdc;
        }
    }
}
