﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    [TestClass]
    [TestCategory("GL")]
    [TestCategory("Payment")]
    public class PaymentApplicationTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/payment/application";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestDataName;
        public TestContext TestContext { get; set; }

        public const int masterPaymentId = -99;

        [TestInitialize]
        public void Initialize()
        {
            this.NewTestDataName = "TestPaymentApplication-" + DateTimeOffset.Now.ToUnixTimeSeconds();

            Teardown();
            var ctx = GetApiContext();
            var paymentMethod = new PaymentMethod()
            {
                BID = 1,
                ID = -99,
                IsActive = true,
                Name = "TestPaymentMethodForApplication",
                PaymentMethodType = PaymentMethodType.CreditCard,
                DepositGLAccountID = ctx.GLAccount.First().ID,
            };
            ctx.PaymentMethod.Add(paymentMethod);

            var paymentMaster = new PaymentMaster()
            {
                BID = 1,
                ID = masterPaymentId,
                LocationID = ctx.LocationData.First().ID,
                ReceivedLocationID = ctx.LocationData.First().ID,
                Amount = 5m,
                PaymentTransactionType = (byte)PaymentTransactionType.Payment,
                CompanyID = ctx.CompanyData.First().ID
            };
            ctx.PaymentMaster.Add(paymentMaster);

            ctx.SaveChanges();
        }

        [TestCleanup]
        public void Teardown()
        {
            var ctx = GetApiContext();
            var paymentApplicationCondition = ctx.PaymentApplication.Where(t => t.BID == 1 && t.MasterID == masterPaymentId);
            var paymentMethodCondition = ctx.PaymentMethod.Where(t => t.BID == 1 && t.ID == -99);
            var paymentMasterCondition = ctx.PaymentMaster.Where(t => t.BID == 1 && t.ID == masterPaymentId);
            ctx.PaymentApplication.RemoveRange(paymentApplicationCondition);
            ctx.PaymentMethod.RemoveRange(paymentMethodCondition);
            ctx.PaymentMaster.RemoveRange(paymentMasterCondition);
            ctx.SaveChanges();
        }

        private PaymentApplication GetNewModel(int ID = -99, byte? LocationID = null, int? CompanyID = null, int? OrderID = null, decimal? Amount = null)
        {
            var ctx = GetApiContext();
            var locationID = LocationID ?? ctx.LocationData.First().ID;
            var companyID = CompanyID ?? ctx.CompanyData.First().ID;
            var orderID = OrderID ?? ctx.OrderData.First().ID;
            var employeeID = ctx.EmployeeData.First().ID;
            var paymentMasterID = masterPaymentId;
            var paymentMethodID = ctx.PaymentMethod.First().ID;
            var amount = Amount ?? 1.05m;
            return new PaymentApplication()
            {
                BID = 1,
                ID = ID,
                LocationID = locationID,
                ReceivedLocationID = locationID,
                MasterID = paymentMasterID,
                Amount = amount,
                PaymentMethodID = paymentMethodID,
                PaymentTransactionType = (byte)PaymentTransactionType.Payment_from_Nonrefundable_Credit,
                CompanyID = companyID,
                ApplicationGroupID = ID,
                HasAdjustments = false,
                EnteredByEmployeeID = employeeID,
                OrderID = orderID,
            };
        }

        [TestMethod]
        public async Task TestLocationID()
        {
            var ctx = GetApiContext();
            var testObject99 = GetNewModel(-99);
            ctx.PaymentApplication.Add(testObject99);
            Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

            var result = await EndpointTests.AssertGetStatusCode<PaymentApplication>(client, $"{apiUrl}/{testObject99.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.ReceivedLocationID);
        }

        [TestMethod]
        public async Task TestGetAll()
        {
            var ctx = GetApiContext();
            var testObject99 = GetNewModel(-99);
            var testObject98 = GetNewModel(-98);
            var testObject97 = GetNewModel(-97);
            ctx.PaymentApplication.Add(testObject99);
            ctx.PaymentApplication.Add(testObject98);
            ctx.PaymentApplication.Add(testObject97);

            Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

            var results = await EndpointTests.AssertGetStatusCode<PaymentApplication[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsTrue(results.Any(t => t.ID == -99));
            Assert.IsTrue(results.Any(t => t.ID == -98));
            Assert.IsTrue(results.Any(t => t.ID == -97));
        }

        [TestMethod]
        public async Task TestGetAllLimit()
        {
            var ctx = GetApiContext();
            var i = 510;
            while (i > 0)
            {
                var model = GetNewModel(i * -1);
                ctx.PaymentApplication.Add(model);
                i--;
            }
            Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

            var results = await EndpointTests.AssertGetStatusCode<PaymentApplication[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.AreEqual(500, results.Count());
        }

        [TestMethod]
        public async Task TestGetAllByLocationID()
        {
            var ctx = GetApiContext();
            var locationID = ctx.LocationData.OrderByDescending(t => t.ID).First().ID;
            var testObject99 = GetNewModel(-99, locationID);
            var testObject98 = GetNewModel(-98);
            var testObject97 = GetNewModel(-97);
            ctx.PaymentApplication.Add(testObject99);
            ctx.PaymentApplication.Add(testObject98);
            ctx.PaymentApplication.Add(testObject97);

            Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

            var results = await EndpointTests.AssertGetStatusCode<PaymentApplication[]>(client, $"{apiUrl}?LocationID={locationID}", HttpStatusCode.OK);
            Assert.AreEqual(1, results.Count());
        }

        [TestMethod]
        public async Task TestGetAllByMultipleLocationID()
        {
            var ctx = GetApiContext();
            var firstLocationID = ctx.LocationData.First().ID;
            var lastLocationID = ctx.LocationData.OrderByDescending(t => t.ID).First().ID;
            var testObject99 = GetNewModel(-99, firstLocationID);
            var testObject98 = GetNewModel(-98, lastLocationID);
            var testObject97 = GetNewModel(-97, lastLocationID);
            ctx.PaymentApplication.Add(testObject99);
            ctx.PaymentApplication.Add(testObject98);
            ctx.PaymentApplication.Add(testObject97);

            Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

            var results = await EndpointTests.AssertGetStatusCode<PaymentApplication[]>(client, $"{apiUrl}?LocationID={firstLocationID}&LocationID={lastLocationID}", HttpStatusCode.OK);
            Assert.IsTrue(results.Any(t => t.ID == -99));
            Assert.IsTrue(results.Any(t => t.ID == -98));
            Assert.IsTrue(results.Any(t => t.ID == -97));
        }

        [TestMethod]
        public async Task TestGetAllByCompanyID()
        {
            var ctx = GetApiContext();
            var companyID = ctx.CompanyData.OrderByDescending(t => t.ID).First().ID;
            var testObject99 = GetNewModel(-99, CompanyID: companyID);
            var testObject98 = GetNewModel(-98);
            var testObject97 = GetNewModel(-97);
            ctx.PaymentApplication.Add(testObject99);
            ctx.PaymentApplication.Add(testObject98);
            ctx.PaymentApplication.Add(testObject97);

            Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

            var results = await EndpointTests.AssertGetStatusCode<PaymentApplication[]>(client, $"{apiUrl}?CompanyID={companyID}", HttpStatusCode.OK);
            Assert.AreEqual(1, results.Count());
        }

        [TestMethod]
        public async Task TestGetAllByMultipleCompanyID()
        {
            var ctx = GetApiContext();
            var firstCompanyID = ctx.CompanyData.First().ID;
            var lastCompanyID = ctx.CompanyData.OrderByDescending(t => t.ID).First().ID;
            var testObject99 = GetNewModel(-99, CompanyID: firstCompanyID);
            var testObject98 = GetNewModel(-98, CompanyID: lastCompanyID);
            var testObject97 = GetNewModel(-97, CompanyID: lastCompanyID);
            ctx.PaymentApplication.Add(testObject99);
            ctx.PaymentApplication.Add(testObject98);
            ctx.PaymentApplication.Add(testObject97);

            Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

            var results = await EndpointTests.AssertGetStatusCode<PaymentApplication[]>(client, $"{apiUrl}?CompanyID={firstCompanyID}&CompanyID={lastCompanyID}", HttpStatusCode.OK);
            Assert.IsTrue(results.Any(t => t.ID == -99));
            Assert.IsTrue(results.Any(t => t.ID == -98));
            Assert.IsTrue(results.Any(t => t.ID == -97));
        }

        [TestMethod]
        public async Task TestGetAllByAmount()
        {
            var ctx = GetApiContext();
            var amount = 4.05m;
            var testObject99 = GetNewModel(-99, Amount: amount);
            var testObject98 = GetNewModel(-98);
            var testObject97 = GetNewModel(-97);
            ctx.PaymentApplication.Add(testObject99);
            ctx.PaymentApplication.Add(testObject98);
            ctx.PaymentApplication.Add(testObject97);

            Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

            var results = await EndpointTests.AssertGetStatusCode<PaymentApplication[]>(client, $"{apiUrl}?Amount={amount}", HttpStatusCode.OK);
            Assert.AreEqual(1, results.Count());
        }

        [TestMethod]
        public async Task TestGetAllByOrderID()
        {
            var ctx = GetApiContext();
            var ordersWithPayments = ctx.PaymentApplication.Where(p => p.OrderID.HasValue).Select(p => p.OrderID).ToList();
            var orderID = ctx.OrderData.Where(o => !ordersWithPayments.Contains(o.ID)).OrderByDescending(t => t.ID).First().ID;
            var testObject99 = GetNewModel(-99, OrderID: orderID);
            var testObject98 = GetNewModel(-98);
            var testObject97 = GetNewModel(-97);
            ctx.PaymentApplication.Add(testObject99);
            ctx.PaymentApplication.Add(testObject98);
            ctx.PaymentApplication.Add(testObject97);

            Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

            var results = await EndpointTests.AssertGetStatusCode<PaymentApplication[]>(client, $"{apiUrl}?OrderID={orderID}", HttpStatusCode.OK);
            Assert.AreEqual(1, results.Count());
        }

        [TestMethod]
        public async Task TestGetByID()
        {
            var ctx = GetApiContext();
            var testObject99 = GetNewModel(-99);
            var testObject98 = GetNewModel(-98);
            var testObject97 = GetNewModel(-97);
            ctx.PaymentApplication.Add(testObject99);
            ctx.PaymentApplication.Add(testObject98);
            ctx.PaymentApplication.Add(testObject97);

            Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

            var result = await EndpointTests.AssertGetStatusCode<PaymentApplication>(client, $"{apiUrl}/{testObject99.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.AreEqual(testObject99.ID, result.ID);
        }


    }
}
