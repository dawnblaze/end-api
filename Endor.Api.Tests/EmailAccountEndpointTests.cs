﻿using Endor.Api.Web.Classes;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Endor.ExternalAuthenticator.Authenticator.Providers;
using Endor.ExternalAuthenticator.Models;
using Endor.Api.Web.Services;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Tests
{
    [TestClass]
    public class EmailAccountEndpointTests : CommonControllerTestClass
    {
        private readonly MockProvider _mockProvider = new MockProvider();
        public const string apiUrl = "/Api/EmailAccount";
        public const string teamApiUrl = "/Api/EmployeeTeam";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        public TestContext TestContext { get; set; }

        [TestInitialize]
        [TestCleanup]
        public async Task InitAndCleanup()
        {
            short testBID = 1;
            using (var ctx = GetApiContext(testBID))
            {
                var emailAccounts = await ctx.EmailAccountData.Where(x => x.BID == testBID && (x.ID < 0 || x.DomainEmailID < 0)).ToListAsync();
                if (emailAccounts.Count > 0)
                    ctx.RemoveRange(emailAccounts);

                var emailDomains = await ctx.DomainEmail.Where(d => d.BID == testBID && d.ID < 0).ToListAsync();
                if (emailDomains.Count > 0)
                    ctx.RemoveRange(emailDomains);

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task EmailAccountCRUD()
        {
            using (var ctx = GetApiContext(1))
            {
                if (!ctx.DomainEmail.Where(d => d.BID == 1 && d.ID == -99).Any())
                {
                    //insert
                    var domainEmail = Utils.GetCorebridgeDomainEmail();
                    domainEmail.ID = -99;
                    ctx.DomainEmail.Add(domainEmail);
                    ctx.SaveChanges();
                }
            }

            #region CREATE

            var newEmailAccount = Utils.GetEmailAccount();
            newEmailAccount.DomainEmailID = -99;
            var email = await EndpointTests.AssertPostStatusCode<EmailAccountData>(client, $"{apiUrl}", JsonConvert.SerializeObject(newEmailAccount), HttpStatusCode.OK);

            #endregion

            #region RETRIEVE

            await EndpointTests.AssertGetStatusCode<EmailAccountData>(client, $"{apiUrl}/{email.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode<EmailAccountData[]>(client, $"{apiUrl}/", HttpStatusCode.OK);

            // GET .../simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleEmailAccountData[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleEmailAccountData sga in simpleList)
            {
                if (sga.DisplayName == email.DisplayName)
                {
                    email.ID = sga.ID;
                }
            }

            #endregion

            #region UPDATE

            email.DomainName = "foo.bar";
            await EndpointTests.AssertPutStatusCode<EmailAccountData>(client, $"{apiUrl}/{email.ID}", JsonConvert.SerializeObject(email), HttpStatusCode.OK);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{email.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode<EmailAccountData>(client, $"{apiUrl}/{email.ID}", HttpStatusCode.NotFound);

            #endregion
        }

        /// <summary>
        /// validate and send email 
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TestValidateAndSendEmailUsingMock()
        {
            // mock email account setup
            var newEmailAccount = new EmailAccountData()
            {
                ID = -99,
                BID = 1,
                UserName = "mock",
                DomainName = "mock.com",
                Credentials = "{\r\n  AccessToken: \"access-token\",\r\n  RefreshToken: \"refresh-token\",\r\n ExpirationDT: \"2019-07-29T04:07:28.208Z\",\r\n}",
                StatusType = EmailAccountStatus.Pending
            };

            // deserialize credentials
            var credentials = JsonConvert.DeserializeObject<EmailAccountCredentials>(newEmailAccount.Credentials);


            // validate token
            var validationResult = _mockProvider.Validate(credentials.AccessToken);
            Assert.IsTrue(validationResult.IsValid);

            // send using refresh token
            var email = new Email()
            {
                Body = $"This is a test email sent by {newEmailAccount.EmailAddress}",
                Recipients = new string[] { newEmailAccount.EmailAddress },
                Sender = newEmailAccount.EmailAddress,
                Subject = $"Test Email {DateTime.UtcNow}"
            };

            var sendResult = await this._mockProvider.SendEmail(credentials.RefreshToken, email);
            Assert.AreEqual("mail success", sendResult);

        }

        [TestMethod]
        public async Task TestEmailExternalAuthenticator()
        {
            short testBID = 1;
            using (var ctx = GetApiContext(testBID))
            {
                if (!ctx.DomainEmail.Any(d => d.BID == testBID && d.DomainName == "gmailTest"))
                {
                    var domainEmail = new DomainEmail()
                    {
                        ID = -98,
                        BID = 1,
                        IsActive = true,
                        DomainName = "gmailTest",
                        ProviderType = EmailProviderType.GoogleGSuite
                    };
                    ctx.DomainEmail.Add(domainEmail);
                    ctx.SaveChanges();
                }
                if (!ctx.DomainEmail.Any(d => d.BID == testBID && d.DomainName == "office365Test"))
                {
                    var domainEmail = new DomainEmail()
                    {
                        ID = -97,
                        BID = 1,
                        IsActive = true,
                        DomainName = "office365Test",
                        ProviderType = EmailProviderType.Microsoft365
                    };
                    ctx.DomainEmail.Add(domainEmail);
                    ctx.SaveChanges();
                }
                var newEmailAccountGmail = new EmailAccountData()
                {
                    BID = 1,
                    ID = -99,
                    ClassTypeID = 1023,
                    ModifiedDT = DateTime.Now,
                    IsActive = true,
                    DomainEmailID = -98,
                    UserName = "test",
                    DomainName = "corebridge.net",
                    IsPrivate = true,
                    EmployeeID = ctx.EmployeeData.OrderByDescending(d => d.ID).FirstOrDefault(d => d.BID == testBID)?.ID,
                    Credentials = "{\r\n  AccessToken: \"test\",\r\n  ExpirationDT: \"2019-07-29T04:07:28.208Z\",\r\n  RefreshToken: \"test\",\r\n}",
                    CredentialsExpDT = DateTime.Now.AddDays(1),
                    LastEmailSuccessDT = DateTime.Now,
                    LastEmailFailureDT = null,
                    AliasUserNames = "test",
                    DisplayName = "Unit Test",
                    StatusType = EmailAccountStatus.Pending
                };
                var newEmailAccountOffice365 = new EmailAccountData()
                {
                    BID = 1,
                    ID = -98,
                    ClassTypeID = 1023,
                    ModifiedDT = DateTime.Now,
                    IsActive = true,
                    DomainEmailID = -98,
                    UserName = "test",
                    DomainName = "corebridge.net",
                    IsPrivate = true,
                    EmployeeID = ctx.EmployeeData.OrderByDescending(d => d.ID).FirstOrDefault(d => d.BID == testBID)?.ID,
                    Credentials = "{\r\n  AccessToken: \"test\",\r\n  ExpirationDT: \"2019-07-29T04:07:28.208Z\",\r\n  RefreshToken: \"test\",\r\n}",
                    CredentialsExpDT = DateTime.Now.AddDays(1),
                    LastEmailSuccessDT = DateTime.Now,
                    LastEmailFailureDT = null,
                    AliasUserNames = "test",
                    DisplayName = "Unit Test",
                    StatusType = EmailAccountStatus.Pending
                };
                ctx.EmailAccountData.Add(newEmailAccountGmail);
                ctx.EmailAccountData.Add(newEmailAccountOffice365);
                ctx.SaveChanges();
                var savedEmailAccountGmail = ctx.EmailAccountData.FirstOrDefault(x => x.BID == testBID && x.ID == -99);
                var savedEmailAccountOffice365 = ctx.EmailAccountData.FirstOrDefault(x => x.BID == testBID && x.ID == -98);

                // To test the endpoint if it works.
                // With the current test data provided it will only return StatusType of "Failed".
                await EndpointTests.AssertPutStatusCode<EmailAccountData>(client, $"{apiUrl}/{savedEmailAccountGmail?.ID}", JsonConvert.SerializeObject(savedEmailAccountGmail),
                        HttpStatusCode.OK);
                await EndpointTests.AssertPutStatusCode<EmailAccountData>(client, $"{apiUrl}/{savedEmailAccountOffice365?.ID}", JsonConvert.SerializeObject(savedEmailAccountOffice365),
                    HttpStatusCode.OK);
            }
        }

        [TestMethod]
        public async Task TestEmailAccountTeamLinks()
        {
            short testBID = 1;
            short domainEmailID = -99;
            short emailAccountID = -99;
            short teamID = -99;

            using (var ctx = GetApiContext(testBID))
            {
                #region Data Setup

                var domainEmail = Utils.GetCorebridgeDomainEmail(testBID, domainEmailID);
                var emailAccount = Utils.GetEmailAccount(testBID, emailAccountID, domainEmailID);

                ctx.DomainEmail.Add(domainEmail);
                ctx.EmailAccountData.Add(emailAccount);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var team = await ctx.EmployeeTeam.FirstOrDefaultAsync(x => x.BID == testBID && x.ID == teamID);
                if (team == null)
                {
                    team = new EmployeeTeam()
                    {
                        BID = testBID,
                        ID = teamID,
                        Name = "UNIT TEST TEAM",
                        IsActive = true
                    };
                    ctx.EmployeeTeam.Add(team);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }

                #endregion

                // test link endpoint
                var response = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{emailAccountID}/Action/LinkTeam/{teamID}", HttpStatusCode.OK);
                Assert.IsNotNull(response);
                Assert.IsTrue(response.Success);

                // test PUT doesn't remove links if they're not included
                var getSingleEmailAccount = await EndpointTests.AssertGetStatusCode<EmailAccountData>(client, $"{apiUrl}/{emailAccountID}?IncludeTeams=None", HttpStatusCode.OK);
                Assert.IsNotNull(getSingleEmailAccount);
                Assert.IsNull(getSingleEmailAccount.EmailAccountTeamLinks); // should not have been serialized
                Assert.IsNull(getSingleEmailAccount.Teams);
                var putSingleEmailAccount = await EndpointTests.AssertPutStatusCode<EmailAccountData>(client, $"{apiUrl}/{emailAccountID}", JsonConvert.SerializeObject(getSingleEmailAccount),HttpStatusCode.OK);

                // test for multiple EmailAccounts
                var getManyEmailAccounts = await EndpointTests.AssertGetStatusCode<List<EmailAccountData>>(client, $"{apiUrl}?IncludeTeams=Full", HttpStatusCode.OK);
                Assert.IsNotNull(getManyEmailAccounts);
                Assert.IsTrue(getManyEmailAccounts.Count > 0);
                var ourTestEmailAccount = getManyEmailAccounts.FirstOrDefault(x => x.ID == emailAccountID);
                Assert.IsNotNull(ourTestEmailAccount);
                Assert.AreEqual(emailAccount.UserName, ourTestEmailAccount.UserName);
                Assert.AreEqual(emailAccount.DomainName, ourTestEmailAccount.DomainName);
                Assert.IsNull(ourTestEmailAccount.EmailAccountTeamLinks); // should not have been serialized
                Assert.IsNotNull(ourTestEmailAccount.Teams);
                Assert.IsTrue(ourTestEmailAccount.Teams.Count > 0);
                Assert.AreEqual(teamID, ourTestEmailAccount.Teams.FirstOrDefault().ID);
                // test for single EmailAccount
                getSingleEmailAccount = await EndpointTests.AssertGetStatusCode<EmailAccountData>(client, $"{apiUrl}/{emailAccountID}?IncludeTeams=Full", HttpStatusCode.OK);
                Assert.IsNotNull(getSingleEmailAccount);
                Assert.IsNull(getSingleEmailAccount.EmailAccountTeamLinks); // should not have been serialized
                Assert.IsNotNull(getSingleEmailAccount.Teams);
                Assert.IsTrue(getSingleEmailAccount.Teams.Count > 0);
                Assert.AreEqual(teamID, getSingleEmailAccount.Teams.FirstOrDefault().ID);

                // test for single EmployeeTeam
                // we only test for single, because it appears that the multiple doesn't use any includes at all
                var getSingleTeam = await EndpointTests.AssertGetStatusCode<EmployeeTeam>(client, $"{teamApiUrl}/{teamID}?EmailAccountsLevel=None", HttpStatusCode.OK);
                Assert.IsNotNull(getSingleTeam);
                Assert.IsNull(getSingleTeam.EmailAccountTeamLinks); // should not have been serialized
                Assert.IsNull(getSingleTeam.EmailAccounts);
                var putSingleTeam = await EndpointTests.AssertPutStatusCode<EmployeeTeam>(client, $"{teamApiUrl}/{teamID}", JsonConvert.SerializeObject(getSingleTeam), HttpStatusCode.OK);

                getSingleTeam = await EndpointTests.AssertGetStatusCode<EmployeeTeam>(client, $"{teamApiUrl}/{teamID}?EmailAccountsLevel=Full", HttpStatusCode.OK);
                Assert.IsNotNull(getSingleTeam);
                Assert.IsNull(getSingleTeam.EmailAccountTeamLinks); // should not have been serialized
                Assert.IsNotNull(getSingleTeam.EmailAccounts);
                Assert.IsTrue(getSingleTeam.EmailAccounts.Count > 0);
                Assert.AreEqual(emailAccountID, getSingleTeam.EmailAccounts.FirstOrDefault().ID);

                // verify unlink
                response = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{emailAccountID}/Action/UnlinkTeam/{teamID}", HttpStatusCode.OK);
                Assert.IsNotNull(response);
                Assert.IsTrue(response.Success);
                getSingleEmailAccount = await EndpointTests.AssertGetStatusCode<EmailAccountData>(client, $"{apiUrl}/{emailAccountID}?IncludeTeams=Full", HttpStatusCode.OK);
                Assert.IsNotNull(getSingleEmailAccount);
                Assert.IsNull(getSingleEmailAccount.EmailAccountTeamLinks); // should not have been serialized
                Assert.IsNull(getSingleEmailAccount.Teams); // should now be empty

                var linkIsNull = await ctx.EmailAccountTeamLink.FirstOrDefaultAsync(x => x.BID == testBID && x.EmailAccountID == emailAccountID && x.TeamID == teamID);
                Assert.IsNull(linkIsNull);

                #region Cleanup

                ctx.Remove(team);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                #endregion
            }
        }
    }
}
