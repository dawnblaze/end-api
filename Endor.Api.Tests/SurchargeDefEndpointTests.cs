﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Tests
{
    [TestClass]
    public class SurchargeDefEndpointTests: CommonControllerTestClass
    {
        public const string apiUrl = "/api/surchargedef";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestSurchargeName;
        public TestContext TestContext { get; set; }
        
        [TestInitialize]
        public override void Init()
        {
            base.Init();
            this.NewTestSurchargeName = DateTime.UtcNow + " TEST SURCHARGE";
        }

        [TestMethod]
        public async Task TestSurchargeSingleEndpoints()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleLaborData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;

            var simpleTaxCodes = await EndpointTests.AssertGetStatusCode<SimpleTaxabilityCode[]>(client, $"/api/taxcode/SimpleList", HttpStatusCode.OK);
            short taxCodeID = simpleTaxCodes.FirstOrDefault().ID;

            SurchargeDef surchargeNameTest = new SurchargeDef()
            {
                Name = this.NewTestSurchargeName,
                IsActive = true,
                BID = 1,
                IncomeAccountID = glAccountID,
                TaxCodeID = taxCodeID,
            };

            //test: expect OK
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(surchargeNameTest), HttpStatusCode.OK);

            // GET api/surchargedef/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleSurchargeDef[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleSurchargeDef sga in simpleList)
            {
                if (sga.DisplayName == surchargeNameTest.Name)
                {
                    surchargeNameTest.ID = sga.ID;
                }
            }

            //POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{surchargeNameTest.ID}/action/setinactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{surchargeNameTest.ID}/action/setactive", HttpStatusCode.OK);

            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + surchargeNameTest.ID, JsonConvert.SerializeObject(surchargeNameTest), HttpStatusCode.OK);

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{surchargeNameTest.ID}", HttpStatusCode.NoContent);
            
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/9241657", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestMoveActionEndpoints()
        {
            #region Create SurchargeDef
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;

            var simpleTaxCodes = await EndpointTests.AssertGetStatusCode<SimpleTaxabilityCode[]>(client, $"/api/taxcode/SimpleList", HttpStatusCode.OK);
            short taxCodeID = simpleTaxCodes.FirstOrDefault().ID;

            for (var ctrl = 0; ctrl < 5; ctrl++)
            {
                SurchargeDef surchargeNameTest = new SurchargeDef()
                {
                    Name = this.NewTestSurchargeName + " TestMoveActionUnitTest " + ctrl,
                    IsActive = true,
                    BID = 1,
                    IncomeAccountID = glAccountID,
                    TaxCodeID = taxCodeID,
                    SortIndex = (short)ctrl
                };

                //test: expect OK
                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(surchargeNameTest), HttpStatusCode.OK);
            }
            #endregion

            short startSortIndex = 0;
            var SurchargeDefList = (await EndpointTests.AssertGetStatusCode<SurchargeDef[]>(client, $"{apiUrl}", HttpStatusCode.OK)).ToList();
            Assert.IsNotNull(SurchargeDefList);
            SurchargeDefList = SurchargeDefList.Where(x => x.Name.Contains("TestMoveActionUnitTest")).ToList();
            Assert.AreEqual(SurchargeDefList.Count(), 5);

            #region MoveBefore test

            //0,1,2,3,4
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{SurchargeDefList[1].ID}/action/movebefore/{SurchargeDefList[3].ID}", HttpStatusCode.OK);
            //0,2,1,3,4
            var adjustedSurchargeDefListMoveBefore = (await EndpointTests.AssertGetStatusCode<SurchargeDef[]>(client, $"{apiUrl}", HttpStatusCode.OK)).ToList();
            Assert.IsNotNull(SurchargeDefList);
            SurchargeDefList = SurchargeDefList.Where(x => x.Name.Contains("TestMoveActionUnitTest")).ToList();
            Assert.AreEqual(SurchargeDefList.Count(), 5);


            Assert.AreEqual<short?>((short)(startSortIndex + 0), adjustedSurchargeDefListMoveBefore.First(t => t.ID == SurchargeDefList[0].ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 1), adjustedSurchargeDefListMoveBefore.First(t => t.ID == SurchargeDefList[2].ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 2), adjustedSurchargeDefListMoveBefore.First(t => t.ID == SurchargeDefList[1].ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 3), adjustedSurchargeDefListMoveBefore.First(t => t.ID == SurchargeDefList[3].ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 4), adjustedSurchargeDefListMoveBefore.First(t => t.ID == SurchargeDefList[4].ID).SortIndex);
            #endregion

            #region MoveAfter test

            //0,2,1,3,4
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{SurchargeDefList[1].ID}/action/moveafter/{SurchargeDefList[0].ID}", HttpStatusCode.OK);
            //0,1,2,3,4
            var adjustedSurchargeDefListMoveAfter = (await EndpointTests.AssertGetStatusCode<SurchargeDef[]>(client, $"{apiUrl}", HttpStatusCode.OK)).ToList();
            Assert.IsNotNull(SurchargeDefList);
            SurchargeDefList = SurchargeDefList.Where(x => x.Name.Contains("TestMoveActionUnitTest")).ToList();
            Assert.AreEqual(SurchargeDefList.Count(), 5);


            Assert.AreEqual<short?>((short)(startSortIndex + 0), adjustedSurchargeDefListMoveAfter.First(t => t.ID == SurchargeDefList[0].ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 1), adjustedSurchargeDefListMoveAfter.First(t => t.ID == SurchargeDefList[1].ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 2), adjustedSurchargeDefListMoveAfter.First(t => t.ID == SurchargeDefList[2].ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 3), adjustedSurchargeDefListMoveAfter.First(t => t.ID == SurchargeDefList[3].ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 4), adjustedSurchargeDefListMoveAfter.First(t => t.ID == SurchargeDefList[4].ID).SortIndex);
            #endregion
        }

        [TestCleanup]
        public void Teardown()
        {
            this.DeleteTestSurchargeRecord();

        }

        private void DeleteTestSurchargeRecord()
        {
            string newTestSurchargeName = NewTestSurchargeName;
            Task.Run(async () =>
            {

                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                SimpleSurchargeDef[] result = JsonConvert.DeserializeObject<SimpleSurchargeDef[]>(responseString);
                if (result != null)
                {
                    foreach (var res in result)
                    {
                        if (res.DisplayName.Contains(newTestSurchargeName) || res.DisplayName.Contains("TestMoveActionUnitTest"))
                        {
                            await client.DeleteAsync($"{apiUrl}/{res.ID}");
                        }
                    }
                }
            }).Wait();
        }
    }
}
