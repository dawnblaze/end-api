﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

namespace Endor.Api.Tests
{
    [TestClass]
    public class TaxGroupEndpointTests
    {
        public const string apiUrl = "/api/taxgroup";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestTaxGroupName;
        public TestContext TestContext { get; set; }

        private TaxGroup GetNewModel()
        {
            return new TaxGroup()
            {
                Name = this.NewTestTaxGroupName,
                IsActive = true,
                BID = 1,
                TaxRate = 0
            };
        }

        [TestInitialize]
        public void Initialize()
        {
            this.NewTestTaxGroupName = DateTime.UtcNow + " TEST TAXGROUP DATA";
        }

        [TestMethod]
        public async Task GetTaxGroupTest()
        {
            var list = await EndpointTests.AssertGetStatusCode<List<TaxGroup>>(client, apiUrl, HttpStatusCode.OK);

            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod]
        public async Task TestTaxGroupCanDelete()
        {
            TaxGroup itemTest = GetNewModel();

            //test: expect OK
            var taxGroup = await EndpointTests.AssertPostStatusCode<TaxGroup>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(taxGroup);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{taxGroup.ID}/action/candelete", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{taxGroup.ID}", HttpStatusCode.NoContent);
        }

    }
}
