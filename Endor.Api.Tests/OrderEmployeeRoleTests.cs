﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    [TestClass]
    public class OrderEmployeeRoleTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/order";
        public TestContext TestContext { get; set; }

        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task TestOrderEmployeeRoleCRUDOperations()
        {
            OrderData testOrder = Utils.GetTestOrderData();
            
            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var contactID = await Utils.GetContactID(client);

            int employeeID = await Utils.GetEmployeeID(client);
            int otherEmployeeID = (await Utils.GetEmployees(client)).Where(t => t.ID != employeeID).FirstOrDefault().ID;
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            var orderEmployeeApiUrl = $"{ apiUrl }/{ createdOrder.ID}/employeerole";

            OrderEmployeeRole employeeRoleToCreate = Utils.GetOrderEmployeeRole();


            //for EmpoyeeRole
            var orderEmployeeRoleApiUrl = $"/api/order/{createdOrder.ID}/employeerole";

            // CREATE
            var createdEmployeeRole = await EndpointTests.AssertPostStatusCode<OrderEmployeeRole>(client, orderEmployeeRoleApiUrl, JsonConvert.SerializeObject(employeeRoleToCreate), HttpStatusCode.OK);

            //RETRIEVE
            var empRoleByRoleTypes = await EndpointTests.AssertGetStatusCode<OrderEmployeeRole[]>(client, $"{orderEmployeeApiUrl}?type={employeeRoleToCreate.RoleID}", HttpStatusCode.OK);
            Assert.IsNotNull(empRoleByRoleTypes);
            Assert.IsTrue(empRoleByRoleTypes.Length > 0);

            var empRole = await EndpointTests.AssertGetStatusCode<OrderEmployeeRole>(client, $"{orderEmployeeApiUrl}/{empRoleByRoleTypes.FirstOrDefault().ID}", HttpStatusCode.OK);
            Assert.IsNotNull(empRole);

            // UPDATE
            var updatedEmployeeRole = await EndpointTests.AssertPutStatusCode<OrderEmployeeRole>(client, $"{orderEmployeeRoleApiUrl}/{createdEmployeeRole.ID}", JsonConvert.SerializeObject(empRole), HttpStatusCode.OK);

            //for EmployeeOrder

            //CREATE
            OrderEmployeeRole oer = Utils.GetOrderEmployeeRole();
            createdEmployeeRole = await EndpointTests.AssertPostStatusCode<OrderEmployeeRole>(client, orderEmployeeRoleApiUrl, JsonConvert.SerializeObject(oer), HttpStatusCode.OK);

            //UPDATE
            empRole.EmployeeID = (short)otherEmployeeID;
            await EndpointTests.AssertPutStatusCode<OrderEmployeeRole>(client, $"{orderEmployeeApiUrl}/{empRole.ID}", JsonConvert.SerializeObject(empRole), HttpStatusCode.OK);

            var retrievOER = await EndpointTests.AssertGetStatusCode<OrderEmployeeRole>(client, $"{orderEmployeeApiUrl}/{empRole.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievOER);
            Assert.AreEqual(retrievOER.EmployeeID, empRole.EmployeeID);

            //DELETE
            await EndpointTests.AssertDeleteStatusCode<OrderEmployeeRole>(client, $"{orderEmployeeApiUrl}/{retrievOER.ID}", HttpStatusCode.NoContent);

        }
    }
}
