﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Request;
using PaymentApplication = Endor.Models.PaymentApplication;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Classes.Responses;

namespace Endor.Api.Tests
{
    [TestClass]
    [TestCategory("Payment")]
    public class PaymentApplyCreditNonRefundableTests : PaymentTestHelper
    {
        private const string applyCreditApiUrl = "/api/payment/applycredit";
        private System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        private PaymentApplyCreditRequest GetApplyCreditRequest(int companyID, int masterID, int orderID, bool isRefundable, decimal amount)
        {
            return new PaymentApplyCreditRequest()
            {
                PaymentTransactionType = isRefundable ? PaymentTransactionType.Payment_from_Refundable_Credit : PaymentTransactionType.Payment_from_Nonrefundable_Credit,
                MasterPaymentID = masterID,
                PaymentMethodId = 250,
                CompanyID = companyID,
                Applications = new List<PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest()
                    {
                        OrderID = orderID,
                        Amount = amount,
                    },
                    new PaymentApplicationRequest()
                    {
                        OrderID = null,
                        Amount = -amount,
                    },
                },
            };
        }


        [TestInitialize]
        public async Task Initialize()
        {
            await CleanUpTestData();
        }

        [TestCleanup]
        public async Task Teardown()
        {
            await CleanUpTestData();
        }

        [TestMethod]
        public async Task PaymentShouldUpdateCompanysBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID, 
                price: 100m
            );
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: false
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: order.ID,
                amount: 70,
                isRefundable: false
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            Assert.AreEqual(30, company.NonRefundableCredit);
        }

        [TestMethod]
        public async Task PaymentShouldUpdateOrdersBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID, 
                price: 100m
            );
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: false
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: order.ID,
                amount: 70,
                isRefundable: false
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            Assert.AreEqual(30, order.PaymentBalanceDue);
        }

        [TestMethod]
        public async Task PaymentShouldUpdateMastersBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID, 
                price: 100m
            );
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: false
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: order.ID,
                amount: 70,
                isRefundable: false
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            Assert.AreEqual(100, master.NonRefBalance);
            Assert.AreEqual(30, master.ChangeInNonRefCredit);
        }

        [TestMethod]
        public async Task PaymentShouldUpdateApplications()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID, 
                price: 100m
            );
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: false
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: order.ID,
                amount: 70,
                isRefundable: false
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            var applications = ctx.PaymentApplication.Where(t => t.MasterID == master.ID).ToList();
            var paymentApplication = applications.FirstOrDefault(t => t.HasAdjustments == true);
            var orderApplication = applications.FirstOrDefault(t => t.OrderID == order.ID);
            var companyApplication = applications.FirstOrDefault(t => t.OrderID == null && t.Amount == -70);
            // assert that these applications exist
            Assert.IsNotNull(paymentApplication);
            Assert.IsNotNull(orderApplication);
            Assert.IsNotNull(companyApplication);
            // assert application amount were inserted correctly
            Assert.AreEqual(-70m, companyApplication.Amount);
            Assert.AreEqual(70m, orderApplication.Amount);
            // assert applications are on the same group
            Assert.AreEqual(companyApplication.ApplicationGroupID, orderApplication.ApplicationGroupID);
            // assert original payment application adjustments
            Assert.AreEqual(30, paymentApplication.NonRefBalance);
        }



        [TestMethod]
        public async Task ValidateErrorReturnedIfAppliedCreditExceedsOrderBalanceDue()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID, 
                price: 100m
            );
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: true
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: order.ID,
                amount: 70,
                isRefundable: true
            );
            applyCreditRequest.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = 60
                },

                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = 60
                },

                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = -120 //Must be negative value.
                }
            };
            await EndpointTests.AssertPostStatusCode(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.BadRequest);
        }





        [TestMethod]
        public async Task PaymentToAnOrderFromSingleMasterShouldSucceed()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID, 
                price: 100m
            );
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: false
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: order.ID,
                amount: 70,
                isRefundable: false
            );
            // set the master to null
            applyCreditRequest.MasterPaymentID = null;
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            Assert.AreEqual(30, company.NonRefundableCredit);
            Assert.AreEqual(30, order.PaymentBalanceDue);
        }



        [TestMethod]
        public async Task PaymentToAnOrderFromMultipleMastersShouldSucceed()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID, 
                price: 200m
            );
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: false
            );
            var master2 = await CreatePaymentForCompany(
                ID: -98, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: false
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: order.ID,
                amount: 200,
                isRefundable: false
            );
            // set the master to null
            applyCreditRequest.MasterPaymentID = null;
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            Assert.AreEqual(0, company.NonRefundableCredit);
            Assert.AreEqual(0, order.PaymentBalanceDue);
        }

        [TestMethod]
        public async Task NonRefundableAndRefundablePaymentOnMultipleMastersWithoutMasterParamShouldSucceed()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID,
                price: 100m
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: 50m
            );
            var master = await CreatePaymentForCompany(
                ID: -99,
                companyID: company.ID,
                amount: 60m,
                isRefundable: false
            );
            var master2 = await CreatePaymentForCompany(
                ID: -98,
                companyID: company.ID,
                amount: 60m,
                isRefundable: true
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                orderID: order.ID,
                amount: 70,
                isRefundable: false
            );
            // set the master to null
            applyCreditRequest.MasterPaymentID = null;
            applyCreditRequest.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = 70,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = -70,
                }
            };
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            order2 = ctx.OrderData.First(t => t.ID == order2.ID);
            Assert.AreEqual(50, company.NonRefundableCredit);
            Assert.AreEqual(0, company.RefundableCredit);
            Assert.AreEqual(30, order.PaymentBalanceDue);
            Assert.AreEqual(50, order2.PaymentBalanceDue);
        }

        [TestMethod]
        public async Task PaymentToMultipleOrdersFromSingleMasterShouldSucceed()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID,
                price: 100m
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: 50m
            );
            var order3 = await CreateOrder(
                companyID: company.ID,
                price: 30m
            );
            var master = await CreatePaymentForCompany(
                ID: -99,
                companyID: company.ID,
                amount: 180m,
                isRefundable: false
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                orderID: order.ID,
                amount: 180,
                isRefundable: false
            );
            // set the master to null
            applyCreditRequest.MasterPaymentID = null;
            applyCreditRequest.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = 100,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order2.ID,
                    Amount = 50,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order3.ID,
                    Amount = 30,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = -180,
                }
            };
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            order2 = ctx.OrderData.First(t => t.ID == order2.ID);
            order3 = ctx.OrderData.First(t => t.ID == order3.ID);
            Assert.AreEqual(0, company.NonRefundableCredit);
            Assert.AreEqual(0, company.RefundableCredit);
            Assert.AreEqual(0, order.PaymentBalanceDue);
            Assert.AreEqual(0, order2.PaymentBalanceDue);
            Assert.AreEqual(0, order3.PaymentBalanceDue);
        }


        [TestMethod]
        public async Task PaymentToMultipleOrdersShouldNotExceedTheOrderPrice()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID,
                price: 100m
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: 50m
            );
            var order3 = await CreateOrder(
                companyID: company.ID,
                price: 30m
            );
            var master = await CreatePaymentForCompany(
                ID: -99,
                companyID: company.ID,
                amount: 180m,
                isRefundable: false
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                orderID: order.ID,
                amount: 180,
                isRefundable: false
            );
            // set the master to null
            applyCreditRequest.MasterPaymentID = null;
            applyCreditRequest.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = 101,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order2.ID,
                    Amount = 51,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order3.ID,
                    Amount = 31,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = -183,
                }
            };
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);
            // validate order balance should be the same
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            order2 = ctx.OrderData.First(t => t.ID == order2.ID);
            order3 = ctx.OrderData.First(t => t.ID == order3.ID);
            Assert.AreEqual(180, company.NonRefundableCredit);
            Assert.AreEqual(100, order.PaymentBalanceDue);
            Assert.AreEqual(50, order2.PaymentBalanceDue);
            Assert.AreEqual(30, order3.PaymentBalanceDue);
        }


        [TestMethod]
        public async Task PaymentToMultipleOrdersFromSingleMasterShouldNotExceedTheMastersBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID,
                price: 100m
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: 50m
            );
            var order3 = await CreateOrder(
                companyID: company.ID,
                price: 30m
            );
            var master = await CreatePaymentForCompany(
                ID: -99,
                companyID: company.ID,
                amount: 150m,
                isRefundable: false
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                orderID: order.ID,
                amount: 180,
                isRefundable: false
            );
            // set the master to null
            applyCreditRequest.MasterPaymentID = null;
            applyCreditRequest.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = 100,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order2.ID,
                    Amount = 50,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order3.ID,
                    Amount = 30,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = -180,
                }
            };
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);
            // validate order balance should be the same
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            order2 = ctx.OrderData.First(t => t.ID == order2.ID);
            order3 = ctx.OrderData.First(t => t.ID == order3.ID);
            Assert.AreEqual(150, company.NonRefundableCredit);
            Assert.AreEqual(100, order.PaymentBalanceDue);
            Assert.AreEqual(50, order2.PaymentBalanceDue);
            Assert.AreEqual(30, order3.PaymentBalanceDue);
        }

        [TestMethod]
        public async Task PaymentToMultipleOrdersFromMultipleMastersShouldNotExceedTheMastersBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID,
                price: 100m
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: 50m
            );
            var order3 = await CreateOrder(
                companyID: company.ID,
                price: 30m
            );
            var master = await CreatePaymentForCompany(
                ID: -99,
                companyID: company.ID,
                amount: 80m,
                isRefundable: false
            );
            var master2 = await CreatePaymentForCompany(
                ID: -98,
                companyID: company.ID,
                amount: 70m,
                isRefundable: false
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                orderID: order.ID,
                amount: 180,
                isRefundable: false
            );
            // set the master to null
            applyCreditRequest.MasterPaymentID = null;
            applyCreditRequest.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = 100,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order2.ID,
                    Amount = 50,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order3.ID,
                    Amount = 30,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = -180,
                }
            };
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);
            // validate order balance should be the same
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            order2 = ctx.OrderData.First(t => t.ID == order2.ID);
            order3 = ctx.OrderData.First(t => t.ID == order3.ID);
            Assert.AreEqual(150, company.NonRefundableCredit);
            Assert.AreEqual(100, order.PaymentBalanceDue);
            Assert.AreEqual(50, order2.PaymentBalanceDue);
            Assert.AreEqual(30, order3.PaymentBalanceDue);
        }


        [TestMethod]
        public async Task PaymentToMultipleOrdersFromMultipleMastersShouldSucceed()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID,
                price: 100m
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: 50m
            );
            var order3 = await CreateOrder(
                companyID: company.ID,
                price: 30m
            );
            var master = await CreatePaymentForCompany(
                ID: -99,
                companyID: company.ID,
                amount: 120m,
                isRefundable: false
            );
            var master2 = await CreatePaymentForCompany(
                ID: -98,
                companyID: company.ID,
                amount: 80m,
                isRefundable: false
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                orderID: order.ID,
                amount: 70,
                isRefundable: false
            );
            // set the master to null
            applyCreditRequest.MasterPaymentID = null;
            applyCreditRequest.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = 100,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order2.ID,
                    Amount = 50,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order3.ID,
                    Amount = 30,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = -180,
                }
            };
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            order2 = ctx.OrderData.First(t => t.ID == order2.ID);
            order3 = ctx.OrderData.First(t => t.ID == order3.ID);
            Assert.AreEqual(20, company.NonRefundableCredit);
            Assert.AreEqual(0, company.RefundableCredit);
            Assert.AreEqual(0, order.PaymentBalanceDue);
            Assert.AreEqual(0, order2.PaymentBalanceDue);
            Assert.AreEqual(0, order3.PaymentBalanceDue);
        }


        [TestMethod]
        public async Task PaymentToMultipleOrdersShouldBeOnTheSameApplicationGroup()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID,
                price: 40m
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: 40m
            );
            var order3 = await CreateOrder(
                companyID: company.ID,
                price: 40m
            );
            var master = await CreatePaymentForCompany(
                ID: -99,
                companyID: company.ID,
                amount: 120m,
                isRefundable: false
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                orderID: order.ID,
                amount: 120,
                isRefundable: false
            );
            // set the master to null
            applyCreditRequest.MasterPaymentID = null;
            applyCreditRequest.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = 40,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order2.ID,
                    Amount = 40,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order3.ID,
                    Amount = 40,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = -120,
                }
            };
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            var applications = ctx.PaymentApplication.Where(t => t.MasterID == master.ID).ToList();
            var paymentApplication = applications.FirstOrDefault(t => t.HasAdjustments == true);
            var order1Application = applications.FirstOrDefault(t => t.OrderID == order.ID);
            var order2Application = applications.FirstOrDefault(t => t.OrderID == order2.ID);
            var order3Application = applications.FirstOrDefault(t => t.OrderID == order3.ID);
            Assert.AreEqual(0, paymentApplication.NonRefBalance);
            Assert.AreEqual(order1Application.ApplicationGroupID, order2Application.ApplicationGroupID);
            Assert.AreEqual(order1Application.ApplicationGroupID, order3Application.ApplicationGroupID);
        }

        [TestMethod]
        public async Task ValidateApplicationCompanyMatchesMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID, 
                price: 100m
            );
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: false
            );
            company.NonRefundableCredit = 100m;
            ctx.CompanyData.Update(company);
            await ctx.SaveChangesAsync();
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: order.ID,
                amount: 70,
                isRefundable: false
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            var retreivedCompany = ctx.CompanyData.Where(c => c.BID == company.BID && c.ID == company.ID).FirstOrDefault();
            ctx.Entry(retreivedCompany).Reload();
            Assert.AreEqual(retreivedCompany.NonRefundableCredit, 30m); //100-70
        }

        [TestMethod]
        public async Task ValidateApplicationLocationIDMatchesMasterLocationID()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID, 
                price: 100m
            );
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: false
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: order.ID,
                amount: 70,
                isRefundable: false
            );
            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == master.ID);
            paymentMaster.LocationID = ctx.LocationData.Last().ID;
            ctx.PaymentMaster.Update(paymentMaster);
            ctx.Entry(paymentMaster).Property(t => t.ValidToDT).IsModified = false;
            ctx.SaveChanges();
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task ValidateMasterNonRefBalanceMustBeNull()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID, 
                price: 100m
            );
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: true
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: order.ID,
                amount: 70,
                isRefundable: true
            );
            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == master.ID);
            paymentMaster.NonRefBalance = 100;
            ctx.PaymentMaster.Update(paymentMaster);
            ctx.Entry(paymentMaster).Property(t => t.ValidToDT).IsModified = false;
            ctx.SaveChanges();
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task ValidateMasterChangeInNonRefCreditMustBeNull()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID, 
                price: 100m
            );
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: true
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: order.ID,
                amount: 70,
                isRefundable: true
            );
            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == master.ID);
            paymentMaster.ChangeInNonRefCredit = 100;
            ctx.PaymentMaster.Update(paymentMaster);
            ctx.Entry(paymentMaster).Property(t => t.ValidToDT).IsModified = false;
            ctx.SaveChanges();
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task ValidateSumOfApplicationRefBalanceEqualsMasterRefBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID, 
                price: 100m
            );
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: true
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: order.ID,
                amount: 70,
                isRefundable: true
            );
            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == master.ID);
            paymentMaster.RefBalance = 2000;
            ctx.PaymentMaster.Update(paymentMaster);
            ctx.Entry(paymentMaster).Property(t => t.ValidToDT).IsModified = false;
            ctx.SaveChanges();
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task ValidateSumOfApplicationAmountEqualsMasterRefBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID, 
                price: 100m
            );
            var master = await CreatePaymentForCompany(
                ID: -99, 
                companyID: company.ID,
                amount: 100m, 
                isRefundable: true
            );
            PaymentApplyCreditRequest applyCreditRequest = GetApplyCreditRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: order.ID,
                amount: 70,
                isRefundable: true
            );
            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == master.ID);
            paymentMaster.RefBalance = 2000;
            ctx.PaymentMaster.Update(paymentMaster);
            ctx.Entry(paymentMaster).Property(t => t.ValidToDT).IsModified = false;
            ctx.SaveChanges();
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);
        }
    }
}
