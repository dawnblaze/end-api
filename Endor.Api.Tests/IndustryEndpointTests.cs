﻿using Endor.Api.Web.Classes;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class IndustryEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/industry";
        private const string TestChildIndustryName = "UnitTestChildIndustry";
        private const string TestParentIndustryName = "UnitTestParentIndustry";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        public static IEnumerable<object[]> GetIndustryCollectionPropertyNames()
        {
            yield return new object[] { (short)1, (short)-99, (short)-100, -99, unchecked((byte)-99), new List<string>() { "ChildIndustries" } };
        }

        [DataTestMethod]
        [DynamicData(nameof(GetIndustryCollectionPropertyNames), DynamicDataSourceType.Method)]
        public async Task IndustryEndpointContract(short bid, short industryID, short childIndustryID, int companyID, byte locationID, List<string> relationshipObjectNames)
        {
            CleanupTestIndustries(bid, industryID, childIndustryID, companyID, locationID);
            SetupTestIndustries(bid, industryID, childIndustryID, companyID, locationID);
            await Utils.TestJSONRelationshipProperties(client, "api/industry", industryID, relationshipObjectNames);
            CleanupTestIndustries(bid, industryID, childIndustryID, companyID, locationID);
        }

        private void CleanupTestIndustries(short bid, short industryID, short childIndustryID, int companyID, byte locationID)
        {
            var ctx = GetApiContext(bid);
            var industry = ctx.CrmIndustry.FirstOrDefault(t => t.BID == bid && t.ID == industryID);
            if (industry != null)
                ctx.CrmIndustry.Remove(industry);

            var childIndustry = ctx.CrmIndustry.FirstOrDefault(t => t.BID == bid && t.ID == childIndustryID);
            if (childIndustry != null)
                ctx.CrmIndustry.Remove(childIndustry);

            var company = ctx.CompanyData.FirstOrDefault(t => t.BID == bid && t.ID == companyID);
            if (company != null)
                ctx.CompanyData.Remove(company);

            var location = ctx.LocationData.FirstOrDefault(t => t.BID == bid && t.ID == locationID);
            if (location != null)
                ctx.LocationData.Remove(location);

            ctx.SaveChanges();
        }

        private void SetupTestIndustries(short bid, short industryID, short childIndustryID, int companyID, byte locationID)
        {
            var ctx = GetApiContext(bid);
            CrmIndustry crmIndustry = new CrmIndustry()
            {
                BID = bid,
                ID = industryID,
                Name = $"testIndustry{industryID}",
            };
            CrmIndustry childIndustry = new CrmIndustry()
            {
                BID = bid,
                ID = childIndustryID,
                ParentID = industryID,
                Name = $"testIndustry{childIndustryID}",
            };
            CompanyData company = new CompanyData()
            {
                BID = bid,
                ID = companyID,
                IndustryID = crmIndustry.ID,
                Name = $"testCompany{companyID}",
                StatusID = (byte)CompanyStatus.Customer,
                LocationID = locationID,
            };

            LocationData location = new LocationData()
            {
                BID = bid,
                ID = locationID,
                Name = $"testLocation{locationID}",
            };

            ctx.CrmIndustry.Add(crmIndustry);
            ctx.CrmIndustry.Add(childIndustry);
            ctx.CompanyData.Add(company);
            ctx.LocationData.Add(location);

            ctx.SaveChanges();
        }

        [TestMethod]
        public async Task TestIndustry()
        {
            var industry = new CrmIndustry()
            {
                BID = 1,
                Name = TestChildIndustryName,
                IsActive = false,
                IsLocked = false
            };
            // POST api/industry/
            CrmIndustry newIndustry = await EndpointTests.AssertPostStatusCode<CrmIndustry>(client, apiUrl, JsonConvert.SerializeObject(industry), HttpStatusCode.OK);
            var parentIndustry = new CrmIndustry()
            {
                BID = 1,
                Name = TestParentIndustryName,
                IsActive = true,
                IsLocked = false
            };
            // POST api/industry/
            CrmIndustry newParentIndustry = await EndpointTests.AssertPostStatusCode<CrmIndustry>(client, apiUrl, JsonConvert.SerializeObject(industry), HttpStatusCode.OK);

            // GET api/industry/{id}
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newIndustry.ID}", HttpStatusCode.OK);

            // GET api/industry/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleCrmIndustry[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            Assert.IsNotNull(simpleList.Where(s => s.ID == newIndustry.ID).FirstOrDefault());

            // GET api/industry
            var industries = await EndpointTests.AssertGetStatusCode<List<CrmIndustry>>(client, $"{apiUrl}/", HttpStatusCode.OK);

            // PUT api/industry/{id}
            const string newIndustryName = "newIndustry";
            newIndustry.Name = newIndustryName;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{newIndustry.ID}", newIndustry, HttpStatusCode.OK);
            CrmIndustry updatedIndustry = await EndpointTests.AssertGetStatusCode<CrmIndustry>(client, $"{apiUrl}/{newIndustry.ID}", HttpStatusCode.OK);
            Assert.AreEqual(newIndustryName, updatedIndustry.Name);

            //POST api/origin/{id}/action/setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newIndustry.ID}/action/setactive", HttpStatusCode.OK);
            CrmIndustry nowActiveIndustry = await EndpointTests.AssertGetStatusCode<CrmIndustry>(client, $"{apiUrl}/{newIndustry.ID}", HttpStatusCode.OK);
            Assert.AreEqual(true, nowActiveIndustry.IsActive);

            //POST api/origin/{id}/action/setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newIndustry.ID}/action/setinactive", HttpStatusCode.OK);
            CrmIndustry nowInactiveIndustry = await EndpointTests.AssertGetStatusCode<CrmIndustry>(client, $"{apiUrl}/{newIndustry.ID}", HttpStatusCode.OK);
            Assert.AreEqual(false, nowInactiveIndustry.IsActive);

            //GET api/origin/{id}/action/candelete
            await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{newIndustry.ID}/action/candelete", HttpStatusCode.OK);

            //POST api/origin/{id}/action/setparent/{parentID}
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newIndustry.ID}/action/setparent/{newParentIndustry.ID}", HttpStatusCode.OK);
            CrmIndustry nowChildIndustry = await EndpointTests.AssertGetStatusCode<CrmIndustry>(client, $"{apiUrl}/{newIndustry.ID}", HttpStatusCode.OK);
            Assert.AreEqual(newParentIndustry.ID, nowChildIndustry.ParentID);

            //delete industry
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{newIndustry.ID}", HttpStatusCode.NoContent);
            // confirm industry was deleted
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newIndustry.ID}", HttpStatusCode.NotFound);
            //delete industry
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{newParentIndustry.ID}", HttpStatusCode.NoContent);
            // confirm industry was deleted
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newParentIndustry.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task TestIndustryCanDelete()
        {
            var industry = new CrmIndustry()
            {
                BID = 1,
                Name = TestChildIndustryName,
                IsActive = false,
                IsLocked = false
            };
            // POST api/industry/
            CrmIndustry newIndustry = await EndpointTests.AssertPostStatusCode<CrmIndustry>(client, apiUrl, JsonConvert.SerializeObject(industry), HttpStatusCode.OK);
            var parentIndustry = new CrmIndustry()
            {
                BID = 1,
                Name = TestParentIndustryName,
                IsActive = true,
                IsLocked = false
            };
            // POST api/industry/
            CrmIndustry newParentIndustry = await EndpointTests.AssertPostStatusCode<CrmIndustry>(client, apiUrl, JsonConvert.SerializeObject(industry), HttpStatusCode.OK);

            // GET api/industry/{id}
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newIndustry.ID}", HttpStatusCode.OK);

            // GET api/industry/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleCrmIndustry[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            Assert.IsNotNull(simpleList.Where(s => s.ID == newIndustry.ID).FirstOrDefault());

            // GET api/industry
            var industries = await EndpointTests.AssertGetStatusCode<List<CrmIndustry>>(client, $"{apiUrl}/", HttpStatusCode.OK);

            // PUT api/industry/{id}
            const string newIndustryName = "newIndustry";
            newIndustry.Name = newIndustryName;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{newIndustry.ID}", newIndustry, HttpStatusCode.OK);
            CrmIndustry updatedIndustry = await EndpointTests.AssertGetStatusCode<CrmIndustry>(client, $"{apiUrl}/{newIndustry.ID}", HttpStatusCode.OK);
            Assert.AreEqual(newIndustryName, updatedIndustry.Name);

            //GET api/origin/{id}/action/candelete
            await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{newIndustry.ID}/action/candelete", HttpStatusCode.OK);

            //delete industry
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{newIndustry.ID}", HttpStatusCode.NoContent);
            // confirm industry was deleted
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newIndustry.ID}", HttpStatusCode.NotFound);
            //delete industry
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{newParentIndustry.ID}", HttpStatusCode.NoContent);
            // confirm industry was deleted
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newParentIndustry.ID}", HttpStatusCode.NotFound);
        }

        [TestCleanup]
        public void Teardown()
        {
            try
            {
                var ctx = GetApiContext();
                ctx.RemoveRange(ctx.CrmIndustry.Where(x => x.Name == TestChildIndustryName));
                ctx.RemoveRange(ctx.CrmIndustry.Where(x => x.Name == TestParentIndustryName));
                ctx.SaveChanges();

            }
            catch { }
        }
    }
}
