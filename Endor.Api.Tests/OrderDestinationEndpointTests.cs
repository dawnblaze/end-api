﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class OrderDestinationEndpointTests
    {
        public const string apiUrl = "/api/orderdestination";
        public const string apiOrderItemUrl = "/api/orderitem";
        public const string apiOrderUrl = "/api/order";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        public string TestRunString { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            this.TestRunString = DateTime.UtcNow.ToString("u");
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task OrderDestinationCRUDTest()
        {
            // setup test data

            #region CREATE ORDER FIRST
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            #endregion

            #region CREATE

            var testOrderItem = Utils.GetTestOrderItemData(createdOrder);
            var createdOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"{apiOrderItemUrl}/", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);

            #endregion

            #region RETRIEVE

            // Test Get By ID
            var retrievedOrderItem = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiOrderItemUrl}/{createdOrderItem.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderItem);

            // Test Get Status ID
            OrderItemStatus orderItemStatus = (await EndpointTests.AssertGetStatusCode<OrderItemStatus>(client, $"api/orderitemstatus/{retrievedOrderItem.ItemStatusID}", HttpStatusCode.OK));

            #endregion

            //Start orderdestination

            var prePostResults = await EndpointTests.AssertGetStatusCode<OrderDestinationData[]>(client, apiUrl, HttpStatusCode.OK);
            OrderDestinationData postRequestModel = Utils.GetTestOrderDestinationData(TestRunString, 1, orderItemStatus.ID, retrievedOrderItem.ID, createdOrder.ID, 0m, (OrderTransactionType)createdOrder.TransactionType);
            var postResult = await EndpointTests.AssertPostStatusCode<OrderDestinationData>(client, apiUrl, JsonConvert.SerializeObject(postRequestModel), HttpStatusCode.OK);
            var postPostResults = await EndpointTests.AssertGetStatusCode<OrderDestinationData[]>(client, apiUrl, HttpStatusCode.OK);
            Assert.IsFalse(prePostResults.Any(t => t.ID == postResult.ID));
            Assert.IsTrue(postPostResults.Any(t => t.ID ==postResult.ID));
            var retrievedPostResult = await EndpointTests.AssertGetStatusCode<OrderDestinationData>(client, $"{apiUrl}/{postResult.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedPostResult);
            OrderDestinationData putRequestModel = JsonConvert.DeserializeObject<OrderDestinationData>(JsonConvert.SerializeObject(retrievedPostResult));
            putRequestModel.HasDocuments = true;
            var putResult = await EndpointTests.AssertPutStatusCode<OrderDestinationData>(client, $"{apiUrl}/{retrievedPostResult.ID}", JsonConvert.SerializeObject(putRequestModel), HttpStatusCode.OK);
            var retrievedPutResult = await EndpointTests.AssertGetStatusCode<OrderDestinationData>(client, $"{apiUrl}/{putResult.ID}", HttpStatusCode.OK);

            Assert.IsTrue(putResult.HasDocuments == true);
            Assert.IsTrue(retrievedPutResult.HasDocuments == true);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{retrievedPutResult.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{retrievedPutResult.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task TestOrderDestinationContactRoles()
        {
            #region Create Order and OrderItem and OrderDestination
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrder);

            var testOrderItem = Utils.GetTestOrderItemData(createdOrder);
            var createdOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"/api/OrderItem/", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrderItem);

            var retrievedOrderItemStatus = (await EndpointTests.AssertGetStatusCode<OrderItemStatus>(client, $"api/orderitemstatus/{createdOrderItem.ItemStatusID}", HttpStatusCode.OK));

            var testOrderDestination = Utils.GetTestOrderDestinationData(TestRunString, 1, retrievedOrderItemStatus.ID, createdOrderItem.ID, createdOrder.ID, 0m, (OrderTransactionType)createdOrder.TransactionType);
            var createdOrderDestination = await EndpointTests.AssertPostStatusCode<OrderDestinationData>(client, apiUrl, JsonConvert.SerializeObject(testOrderDestination), HttpStatusCode.OK);
            #endregion

            var contact = (await EndpointTests.AssertGetStatusCode<SimpleEmployeeData[]>(client, "/api/contact/simplelist", HttpStatusCode.OK)).FirstOrDefault();
            Assert.IsNotNull(contact);

            #region Create OrderDestinationContactRole
            //Temporary order contact role
            OrderContactRole orderContactRole = new OrderContactRole()
            {
                BID = 1,
                ID = 0,
                ContactID = contact.ID,
                RoleType = OrderContactRoleType.Billing,
                OrderID = createdOrder.ID,
                DestinationID = createdOrderDestination.ID,
            };

            var createdOrderDestinationContactRole = (await EndpointTests.AssertPostStatusCode<OrderContactRole>(client, $"{apiUrl}/{createdOrderDestination.ID}/contactrole", JsonConvert.SerializeObject(orderContactRole), HttpStatusCode.OK));
            Assert.IsNotNull(createdOrderDestinationContactRole);
            #endregion


            #region DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderDestination.ID}/contactrole/{createdOrderDestinationContactRole.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderDestination.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion

        }
    }
}
