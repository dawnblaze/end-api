﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class OrderContactRoleEndpointTests
    {
        public const string apiUrl = "/api/order";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        [TestMethod]
        public async Task TestOrderContactRoleCRUDOperations()
        {
            #region CREATE
            OrderData testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var contactID = await Utils.GetContactID(client);

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            OrderContactRole ocr = Utils.GetOrderContactRole();
            
            var orderContactApiUrl = $"{ apiUrl }/{ createdOrder.ID}/contactrole";
            var createdContactRole = await EndpointTests.AssertPostStatusCode<OrderContactRole>(client, orderContactApiUrl, JsonConvert.SerializeObject(ocr), HttpStatusCode.OK);

            #endregion

            #region RETRIEVE

            // Test Get By ID
            var retrievedOrderContact = await EndpointTests.AssertGetStatusCode<OrderContactRole>(client, $"{orderContactApiUrl}/{createdContactRole.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderContact);

            // Test Get all should return the unfiltered list of OrderContacts
            var orderContactRoles = await EndpointTests.AssertGetStatusCode<OrderContactRole[]>(client, $"{orderContactApiUrl}/", HttpStatusCode.OK);

            Assert.IsNotNull(orderContactRoles);
            Assert.AreEqual(1, orderContactRoles.Length);            
            
            var filteredOrderContactRoles = await EndpointTests.AssertGetStatusCode<OrderContactRole[]>(client, $"{orderContactApiUrl}?Type=Billing", HttpStatusCode.OK);

            Assert.IsNotNull(filteredOrderContactRoles);
            Assert.AreEqual(0, filteredOrderContactRoles.Length);

            #endregion

            #region UPDATE
            int invalidID = -1;
            await EndpointTests.AssertGetStatusCode(client, $"{orderContactApiUrl}/{invalidID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertPutStatusCode(client, $"{orderContactApiUrl}/{invalidID}", JsonConvert.SerializeObject(createdContactRole), HttpStatusCode.NotFound,HttpStatusCode.BadRequest);


            retrievedOrderContact = await EndpointTests.AssertGetStatusCode<OrderContactRole>(client, $"{orderContactApiUrl}/{createdContactRole.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderContact);

            // Sets the contact ID making the contact role not "ad-hoc"
            createdContactRole.ContactID = contactID;
            await EndpointTests.AssertPutStatusCode(client, $"{orderContactApiUrl}/{createdContactRole.ID}", JsonConvert.SerializeObject(createdContactRole), HttpStatusCode.OK);
            retrievedOrderContact = await EndpointTests.AssertGetStatusCode<OrderContactRole>(client, $"{orderContactApiUrl}/{createdContactRole.ID}", HttpStatusCode.OK);

            Assert.AreEqual(createdContactRole.ContactName, retrievedOrderContact.ContactName);
            Assert.AreEqual(createdContactRole.ContactID, retrievedOrderContact.ContactID);

            string holdName = createdContactRole.ContactName;
            createdContactRole.ContactName= ""; //invalid length string;
            await EndpointTests.AssertPutStatusCode(client, $"{orderContactApiUrl}/{createdContactRole.ID}", JsonConvert.SerializeObject(createdContactRole), HttpStatusCode.OK);

            retrievedOrderContact = await EndpointTests.AssertGetStatusCode<OrderContactRole>(client, $"{orderContactApiUrl}/{createdContactRole.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderContact);
            Assert.AreEqual(holdName, retrievedOrderContact.ContactName);

            createdContactRole.ContactID = null;

            await EndpointTests.AssertPutStatusCode(client, $"{orderContactApiUrl}/{createdContactRole.ID}", JsonConvert.SerializeObject(createdContactRole), HttpStatusCode.OK);
            retrievedOrderContact = await EndpointTests.AssertGetStatusCode<OrderContactRole>(client, $"{orderContactApiUrl}/{createdContactRole.ID}", HttpStatusCode.OK);
            Assert.AreEqual(createdContactRole.ContactName, retrievedOrderContact.ContactName);
            Assert.IsNull(retrievedOrderContact.ContactID);
            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{orderContactApiUrl}/{createdContactRole.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{orderContactApiUrl}/{createdContactRole.ID}", HttpStatusCode.NotFound);
            #endregion
        }

    }
}
