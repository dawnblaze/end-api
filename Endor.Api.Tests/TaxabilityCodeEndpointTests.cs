﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    [TestClass]
    public class TaxbilityCodeEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/taxcode";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        private TaxabilityCode GetNewModel()
        {
            return new TaxabilityCode()
            {
                Name = "Test tax code",
                IsActive = true,
                BID = 1,
                Description = "Test tax code",
                IsTaxExempt = false,
                HasExcludedTaxItems = false,
                TaxCode = "Goods",
            };
        }

        [TestMethod]
        public async Task TestTaxCodeSingleEndpoints()
        {
            TaxabilityCode newTaxCode = GetNewModel();

            //test: expect OK
            var postedResponse = await EndpointTests.AssertPostStatusCode<TaxabilityCode>(client, $"{apiUrl}", JsonConvert.SerializeObject(newTaxCode), HttpStatusCode.OK);
            newTaxCode.ID = postedResponse.ID;

            // GET api/TaxItems/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleTaxabilityCode[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleTaxabilityCode sga in simpleList)
            {
                if (sga.DisplayName == newTaxCode.Name)
                {
                    newTaxCode.ID = sga.ID;
                }
            }

            //POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newTaxCode.ID}/action/setinactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newTaxCode.ID}/action/setactive", HttpStatusCode.OK);

            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + newTaxCode.ID, JsonConvert.SerializeObject(newTaxCode), HttpStatusCode.OK);

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{newTaxCode.ID}", HttpStatusCode.NoContent);
            
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/32767", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestTaxCodeSetActiveSetInactive()
        {
            TaxabilityCode taxCodeTest = GetNewModel();

            //test: expect OK
            var taxCode = await EndpointTests.AssertPostStatusCode<TaxabilityCode>(client, $"{apiUrl}", JsonConvert.SerializeObject(taxCodeTest), HttpStatusCode.OK);
            try
            {
                Assert.IsNotNull(taxCode);

                // set taxability code inactive
                var resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{taxCode.ID}/action/setinactive", HttpStatusCode.OK);
                Assert.IsNotNull(resp);
                Assert.AreEqual(true, resp.Success);

                // set active
                resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{taxCode.ID}/action/setactive", HttpStatusCode.OK);
                Assert.IsNotNull(resp);
                Assert.AreEqual(true, resp.Success);
            }
            finally
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{taxCode.ID}", HttpStatusCode.NoContent);
            }
        }

        [TestMethod]
        public async Task TestTaxCodeCanDelete()
        {
            TaxabilityCode taxCodeTest = GetNewModel();

            //test: expect OK
            var taxCode = await EndpointTests.AssertPostStatusCode<TaxItem>(client, $"{apiUrl}", JsonConvert.SerializeObject(taxCodeTest), HttpStatusCode.OK);
            try
            {
                Assert.IsNotNull(taxCode);

                await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{taxCode.ID}/action/candelete", HttpStatusCode.OK);
            }
            finally
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{taxCode.ID}", HttpStatusCode.NoContent);
            }
        }

        [TestMethod]
        public async Task TestTaxCodeLink()
        {
            TaxabilityCode taxCodeTest = GetNewModel();

            var taxCode = await EndpointTests.AssertPostStatusCode<TaxItem>(client, $"{apiUrl}", JsonConvert.SerializeObject(taxCodeTest), HttpStatusCode.OK);
            try
            {
                Assert.IsNotNull(taxCode);
                var taxItems = await EndpointTests.AssertGetStatusCode<List<TaxItem>>(client, "/api/taxitem", HttpStatusCode.OK);
                short taxItemID = taxItems[0].ID;

                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{taxCode.ID}/action/linkexempttaxitem/{taxItemID}", HttpStatusCode.OK);

                var taxCodes = await EndpointTests.AssertGetStatusCode<List<TaxabilityCode>>(client, $"{apiUrl}", HttpStatusCode.OK);
                Assert.IsNotNull(taxCodes);
                var ourTaxCode = taxCodes.FirstOrDefault(tc => tc.ID == taxCode.ID);
                Assert.IsNotNull(ourTaxCode);
                Assert.IsNotNull(ourTaxCode.TaxItems);

                var singleTaxCode = await EndpointTests.AssertGetStatusCode<TaxabilityCode>(client, $"{apiUrl}/{taxCode.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(singleTaxCode);
                Assert.IsNotNull(singleTaxCode.TaxItems);

                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{taxCode.ID}/action/unlinkexempttaxitem/{taxItemID}", HttpStatusCode.OK);

                singleTaxCode = await EndpointTests.AssertGetStatusCode<TaxabilityCode>(client, $"{apiUrl}/{taxCode.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(singleTaxCode);
                Assert.AreEqual(0, singleTaxCode.TaxItems.Count);

                // recreate the link to test delete as removing links
                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{taxCode.ID}/action/linkexempttaxitem/{taxItemID}", HttpStatusCode.OK);
            }
            finally
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{taxCode.ID}", HttpStatusCode.NoContent);
            }
        }
    }
}
