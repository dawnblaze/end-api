﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Pricing;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Tests
{
    [TestClass]
    public class BusinessEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/business";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        [TestMethod]
        public async Task TestBusinessCreate()
        {
            var ctx = GetApiContext();

            //Please also make sure your END-Auth is running and up to date since this will also call an end-auth endpoint
            short testBID = 9999;
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testBID}?TestOnly=false", HttpStatusCode.OK);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}?TemplateBID=1&FirstName=Test&LastName=Owner&EmailAddress=test%40corebridge.net&LocationName=Test%20Valley&NewBusinessID={testBID}", HttpStatusCode.OK);
            
            Assert.IsTrue(await ctx.Set<BusinessData>().FirstOrDefaultAsync(d => d.BID == testBID) != null);
            Assert.IsTrue(await ctx.Set<LocationData>().FirstOrDefaultAsync(d => d.BID == testBID) != null);
            Assert.IsTrue(await ctx.Set<EmployeeData>().FirstOrDefaultAsync(d => d.BID == testBID) != null);

            // check for common US timezones for create business is created in BusinessTimeZoneLink
            int[] commonUSTimeZones = new int[] {
                35, // Eastern Time
                20, // Central Time
                10, // Mountain Time
                4, // Pacific Time
                3, // Alaska Time
                2, // Hawaii Time
                15, // Arizona
                40 // Indiana
            };
            Assert.AreEqual(commonUSTimeZones.Count(), await ctx.BusinessTimeZoneLink.Where(d => d.BID == testBID && commonUSTimeZones.Contains(d.TimeZoneID)).CountAsync());

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testBID}?TestOnly=false", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestBusinessDelete()
        {
            //Please also make sure your END-Auth is running and up to date since this will also call an end-auth endpoint
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/1?TestOnly=true", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestLinkTimeZoneToBusiness()
        {
            var ctx = GetApiContext();
            short bid = 1;
            var timezoneID = ctx.EnumTimeZone.FirstOrDefault().ID;
            var timezoneLink = ctx.BusinessTimeZoneLink.Where(x => x.BID == bid && x.TimeZoneID == timezoneID).FirstOrDefault();

            //try to link an invalid timezone, the expected endpoint response should be HttpStatusCode.NotFound as described in wiki)
            var invalidTimeZoneID = 9999;
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/link/timezone/{invalidTimeZoneID}", HttpStatusCode.NotFound);

            try
            {
                if (timezoneLink != null)
                {
                    //unlink
                    await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/unlink/timezone/{timezoneLink.TimeZoneID}", HttpStatusCode.OK);
                    var link = ctx.BusinessTimeZoneLink.Where(x => x.BID == bid && x.TimeZoneID == timezoneID).FirstOrDefault();
                    Assert.IsNull(link);

                    //link
                    await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/link/timezone/{timezoneID}", HttpStatusCode.OK);
                    link = ctx.BusinessTimeZoneLink.Where(x => x.BID == bid && x.TimeZoneID == timezoneID).FirstOrDefault();
                    Assert.IsNotNull(link);

                    //try to link timezone that is linked already, the expected endpoint response should be HttpStatusCode.OK/succeed as described in wiki)
                    var res = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/action/link/timezone/{timezoneID}", HttpStatusCode.OK);
                    Assert.IsTrue(res.Message.Contains("No Change"), "Message did not contain 'No Change'");
                }
                else
                {
                    //link
                    await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/link/timezone/{timezoneID}", HttpStatusCode.OK);
                    var link = ctx.BusinessTimeZoneLink.Where(x => x.BID == bid && x.TimeZoneID == timezoneID).FirstOrDefault();
                    Assert.IsNotNull(link);

                    //try to link timezone that is linked already, the expected endpoint response should be HttpStatusCode.OK/succeed as described in wiki)
                    var res = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/action/link/timezone/{timezoneID}", HttpStatusCode.OK);
                    Assert.IsTrue(res.Message.Contains("No Change"), "Message did not contain 'No Change'");

                }
            }
            finally
            {
                //cleanup
                if (timezoneLink != null)
                {
                    await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/link/timezone/{timezoneLink.TimeZoneID}", HttpStatusCode.OK);
                } else
                {
                    await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/unlink/timezone/{timezoneID}", HttpStatusCode.OK);
                }
            }
        }

        [TestMethod]
        public async Task TestUnlinkTimeZoneToBusiness()
        {
            var ctx = GetApiContext();
            short bid = 1;
            var timezoneID = ctx.EnumTimeZone.FirstOrDefault().ID;
            var timezoneLink = ctx.BusinessTimeZoneLink.Where(x => x.BID == bid && x.TimeZoneID == timezoneID).FirstOrDefault();

            //try to unlink an invalid timezone, the expected endpoint response should be HttpStatusCode.NotFound as described in wiki)
            var invalidTimeZoneID = 9999;
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/link/timezone/{invalidTimeZoneID}", HttpStatusCode.NotFound);

            try
            {
                if (timezoneLink != null)
                {
                    //unlink
                    await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/unlink/timezone/{timezoneLink.TimeZoneID}", HttpStatusCode.OK);
                    var link = ctx.BusinessTimeZoneLink.Where(x => x.BID == bid && x.TimeZoneID == timezoneID).FirstOrDefault();
                    Assert.IsNull(link);

                    //try to unlink timezone that is unlinked already, the expected endpoint response should be HttpStatusCode.OK/succeed as described in wiki)
                    var res = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/action/unlink/timezone/{timezoneID}", HttpStatusCode.OK);
                    Assert.IsTrue(res.Message.Contains("No Change"), "Message did not contain 'No Change'");
                } else
                {
                    //link
                    await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/link/timezone/{timezoneID}", HttpStatusCode.OK);
                    var link = ctx.BusinessTimeZoneLink.Where(x => x.BID == bid && x.TimeZoneID == timezoneID).FirstOrDefault();
                    Assert.IsNotNull(link);

                    //unlink
                    await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/unlink/timezone/{timezoneID}", HttpStatusCode.OK);
                    link = ctx.BusinessTimeZoneLink.Where(x => x.BID == bid && x.TimeZoneID == timezoneID).FirstOrDefault();
                    Assert.IsNull(link);

                    //try to unlink timezone that is unlinked already, the expected endpoint response should be HttpStatusCode.OK/succeed as described in wiki)
                    var res = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/action/unlink/timezone/{timezoneID}", HttpStatusCode.OK);
                    Assert.IsTrue(res.Message.Contains("No Change"), "Message did not contain 'No Change'");
                }
            }
            finally
            {
                //cleanup
                if (timezoneLink != null)
                {
                    await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/link/timezone/{timezoneLink.TimeZoneID}", HttpStatusCode.OK);
                }
                else
                {
                    await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/action/unlink/timezone/{timezoneID}", HttpStatusCode.OK);
                }
            }
        }

        [TestMethod]
        public async Task TestGetBusinessTimezoneSimplelist()
        {
            //Please also make sure your END-Auth is running and up to date since this will also call an end-auth endpoint
            await EndpointTests.AssertGetStatusCode<List<SimpleEnumTimeZone>>(client, $"{apiUrl}/timezone/simplelist", HttpStatusCode.OK);
        }
    }
}
