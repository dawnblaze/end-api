﻿using Endor.Api.Web.Classes;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class DashboardEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/Api/Dashboard";
        public const string widgetApiUrl = "/Api/DashboardWidget";
        public System.Net.Http.HttpClient client; 
        private string newDashboardName, newDashboardNameTwo;
        public TestContext TestContext { get; set; }

        #region Test Initialize/Cleanup

        [TestInitialize]
        public void Initialize()
        {
            var ctx = GetApiContext();
            DeleteTestRecords();
            ctx.RemoveRange(ctx.UserLink.Where(l => l.ID == -99));
            ctx.SaveChanges();
            var newUserLink = new UserLink()
            {
                ID = -99,
                BID = TestConstants.BID,
                ClassTypeID = 0,
                EmployeeID = ctx.EmployeeData.First().ID,
                UserAccessType = UserAccessType.SupportManager,
                UserName = "TestUser"
            };
            ctx.UserLink.Add(newUserLink);
            ctx.SaveChanges();
            client = EndpointTests.GetHttpClient(AuthUserID: -99);
            newDashboardName = GetTestDashboardName();
            newDashboardNameTwo = $"{GetTestDashboardName()}.TWO";
        }

        [TestCleanup]
        public void Teardown()
        {
            this.DeleteTestRecords();
        }

        private void DeleteTestRecords()
        {
            var ctx = GetApiContext();
            var widgets = ctx.DashboardWidgetData.Where(x => x.Name.Contains("UnitTest")).ToList();
            if (widgets != null && widgets.Count() > 0)
            {
                foreach (var widget in widgets)
                {
                    ctx.Remove(widget);
                    ctx.SaveChanges();
                }
            }
            var dashboards = ctx.DashboardData.Where(x => x.Name.Contains("UnitTest")).ToList();
            if (dashboards != null && dashboards.Count() > 0)
            {
                foreach (var dashboard in dashboards)
                {
                    ctx.Remove(dashboard);
                    ctx.SaveChanges();
                }
            }
            ctx.RemoveRange(ctx.UserLink.Where(l => l.ID == -99));
            ctx.SaveChanges();
        }

        #endregion

        [TestMethod]
        public async Task DashboardCRUD()
        {
            #region CREATE

            var newDashboard = Utils.GetTestDashboard(newDashboardName);
            newDashboard.UserLinkID = -99;
            var dashboard = await EndpointTests.AssertPostStatusCode<DashboardData>(client, $"{apiUrl}", JsonConvert.SerializeObject(newDashboard), HttpStatusCode.OK);
            Assert.IsNotNull(dashboard);

            var newWidget = Utils.GetTestDashboardWidget(dashboardID: dashboard.ID);
            var widget = await EndpointTests.AssertPostStatusCode<DashboardWidgetData>(client, $"{widgetApiUrl}", newWidget, HttpStatusCode.OK);
            Assert.IsNotNull(widget);

            #endregion

            #region RETRIEVE

            // returns all shared, system, user
            var getAll = await EndpointTests.AssertGetStatusCode<DashboardData[]>(client, $"{apiUrl}/", HttpStatusCode.OK);
            Assert.IsNotNull(getAll);
            Assert.IsTrue(getAll.Where(d => d.Name == newDashboardName).FirstOrDefault() != null);

            // returns all shared, system, user WITH widgets
            var getAllWidgets = await EndpointTests.AssertGetStatusCode<DashboardData[]>(client, $"{apiUrl}/?IncludeWidgets=full", HttpStatusCode.OK);
            Assert.IsNotNull(getAllWidgets);
            Assert.IsTrue(getAllWidgets.Where(d => d.Name == newDashboardName).FirstOrDefault() != null);
            Assert.IsNotNull(getAllWidgets.Where(d => d.Name == newDashboardName).FirstOrDefault().Widgets);

            // returns only the given module for shared, system, user
            var getAllModule = await EndpointTests.AssertGetStatusCode<DashboardData[]>(client, $"{apiUrl}/?Module=Sales", HttpStatusCode.OK);
            Assert.IsNotNull(getAllModule);
            Assert.IsTrue(getAllModule.Where(d => d.Name == newDashboardName).FirstOrDefault() != null);

            // should only return the newly created one
            var getFirstOrDefault = await EndpointTests.AssertGetStatusCode<DashboardData[]>(client, $"{apiUrl}/?FirstOrDefault=true", HttpStatusCode.OK);
            Assert.IsNotNull(getFirstOrDefault);
            Assert.AreEqual(1, getFirstOrDefault.Count());
            Assert.AreEqual(newDashboardName, getFirstOrDefault.FirstOrDefault().Name);

            // get single by id
            var getById = await EndpointTests.AssertGetStatusCode<DashboardData>(client, $"{apiUrl}/{dashboard.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(getById);

            // get simple list
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleListItem<short>[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            Assert.IsNotNull(simpleList);
            Assert.IsTrue(simpleList.Where(d => d.DisplayName == newDashboardName).FirstOrDefault() != null);

            #endregion

            #region UPDATE

            dashboard.Module = Module.Accounting;
            dashboard.UserLinkID = 2;
            var put = await EndpointTests.AssertPutStatusCode<DashboardData>(client, $"{apiUrl}/{dashboard.ID}", JsonConvert.SerializeObject(dashboard), HttpStatusCode.OK);
            Assert.IsNotNull(put);
            Assert.AreEqual(Module.Accounting, put.Module);

            // get simple list shouldn't return different userLinkId's dashboard
            simpleList = await EndpointTests.AssertGetStatusCode<SimpleListItem<short>[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            Assert.IsNotNull(simpleList);
            Assert.IsTrue(simpleList.Where(d => d.DisplayName == newDashboardName).FirstOrDefault() == null);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{widgetApiUrl}/{widget.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{widgetApiUrl}/{widget.ID}", HttpStatusCode.NotFound);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{dashboard.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{dashboard.ID}", HttpStatusCode.NotFound);

            #endregion
        }

        [TestMethod]
        public async Task DashboardActions()
        {
            #region CREATE
            var ctx = GetApiContext();

            var newDashboardOne = Utils.GetTestDashboard(newDashboardName);

            newDashboardOne.UserLinkID = -99;
            var dashboardOne = await EndpointTests.AssertPostStatusCode<DashboardData>(client, $"{apiUrl}", JsonConvert.SerializeObject(newDashboardOne), HttpStatusCode.OK);
            Assert.IsNotNull(dashboardOne);

            var newDashboardTwo = Utils.GetTestDashboard(newDashboardNameTwo);
            newDashboardTwo.UserLinkID = -99;
            var dashboardTwo = await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(newDashboardTwo), HttpStatusCode.Conflict);
            
            #endregion

            #region Test Set Active

            var setInactive = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{dashboardOne.ID}/Action/SetInactive", null, HttpStatusCode.OK);
            Assert.IsNotNull(setInactive);
            Assert.IsTrue(setInactive.Success);
            var getSetInactive = await EndpointTests.AssertGetStatusCode<DashboardData>(client, $"{apiUrl}/{dashboardOne.ID}", HttpStatusCode.OK);
            Assert.IsFalse(getSetInactive.IsActive);

            #endregion

            #region Test Set Inactive

            var setActive = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{dashboardOne.ID}/Action/SetActive", null, HttpStatusCode.OK);
            Assert.IsNotNull(setActive);
            Assert.IsTrue(setInactive.Success);
            var getSetActive = await EndpointTests.AssertGetStatusCode<DashboardData>(client, $"{apiUrl}/{dashboardOne.ID}", HttpStatusCode.OK);
            Assert.IsTrue(getSetActive.IsActive);

            #endregion

            #region Test CanDelete

            var canDeleteOne = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{dashboardOne.ID}/Action/CanDelete", HttpStatusCode.OK);
            Assert.IsNotNull(canDeleteOne);
            Assert.IsTrue(canDeleteOne.Value.HasValue);
            Assert.IsTrue(canDeleteOne.Value.Value);
            
            var getAll = await EndpointTests.AssertGetStatusCode<DashboardData[]>(client, $"{apiUrl}?IncludeAll=true", HttpStatusCode.OK);
            Assert.IsNotNull(getAll);
            var systemDash = getAll.FirstOrDefault();
            Assert.IsNotNull(systemDash);
            var cannotDelete = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{systemDash.ID}/Action/CanDelete", HttpStatusCode.OK);
            Assert.IsNotNull(cannotDelete);
            Assert.IsTrue(cannotDelete.Value.HasValue);
            Assert.IsTrue(cannotDelete.Value.Value);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{dashboardOne.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{dashboardOne.ID}", HttpStatusCode.NotFound);

            #endregion
        }

        private string GetTestDashboardName()
        {
            var tickString = DateTime.UtcNow.Ticks.ToString();
            return "UnitTest." + tickString.Substring(tickString.Length - 7, 4) + "." + tickString.Substring(tickString.Length - 3);
        }

    }
}
