﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Services;
using Endor.DocumentStorage.Models;
using Microsoft.AspNetCore.Mvc;

namespace Endor.Api.Tests
{
    [TestClass]
    public class ContactEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/Api/Contact";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string[] NewTestContactName = { "TEST", "SUBJECT" };
        private string NewTestCompanyName = "TEST COMPANY";
        private string currDateTime = "";

        private ContactData GetNewContact(CompanyData companyData = null)
        {
            return new ContactData()
            {
                BID = TestConstants.BID,
                First = NewTestContactName[0],
                Last = $"{NewTestContactName[1]} {currDateTime}"
            };
        }

        private ContactData GetNewAdHocContact(CompanyData companyData = null)
        {
            return new ContactData()
            {
                BID = TestConstants.BID,
                First = NewTestContactName[0],
                Last = $"{NewTestContactName[1]} {currDateTime}",
                CompanyContactLinks = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        Company = companyData ?? new CompanyData()
                        {
                            BID = TestConstants.BID,
                            ID = 0
                        },
                        CompanyID = companyData?.ID ?? 0
                    }
                }
            };
        }
        private ContactData GetNewAdHocContactWithInvalidCompanyID()
        {
            return new ContactData()
            {
                BID = TestConstants.BID,
                First = NewTestContactName[0],
                Last = $"{NewTestContactName[1]} {currDateTime}",
                CompanyContactLinks = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        Company = new CompanyData()
                        {
                            BID = TestConstants.BID,
                            ID = -123
                        },
                        CompanyID = -123
                    }
                }
            };
        }
        private async Task<CompanyData> GetNewCompanyData(int id)
        {
            var company = new CompanyData()
            {
                BID = TestConstants.BID,
                ID = id,
                Name = $"{NewTestCompanyName} {currDateTime}",
                IsActive = true,
                IsAdHoc = false,
                StatusID = (byte)CompanyStatus.Lead,
                StatusText = "TEST"
            };

            using (var ctx = GetApiContext())
            {
                var locationID = (await ctx.LocationData.FirstOrDefaultAsync()).ID;
                company.LocationID = locationID;

                ctx.CompanyData.Add(company);
                await ctx.SaveChangesAsync();
            }

            return company;
        }

        [TestInitialize]
        public void InitializeTest()
        {
            currDateTime = DateTime.UtcNow.ToString();
        }

        [TestCleanup]
        public async Task Teardown()
        {
            try
            {
                using (var ctx = GetApiContext())
                {
                    var adHocCompanyName = $"{NewTestCompanyName[0]} {NewTestCompanyName[1]}";
                    var companiesToRemove = ctx.CompanyData.Where(c => c.BID == TestConstants.BID && (c.Name.StartsWith(NewTestCompanyName) || c.Name.StartsWith(adHocCompanyName)));
                    if (companiesToRemove != null)
                    {
                        ctx.CompanyData.RemoveRange(companiesToRemove);
                        await ctx.SaveChangesAsync();
                    }

                    companiesToRemove = ctx.CompanyData.Where(c => c.BID == TestConstants.BID && c.ID < 0);
                    if (companiesToRemove != null)
                    {
                        ctx.CompanyData.RemoveRange(companiesToRemove);
                        await ctx.SaveChangesAsync();
                    }

                    var contactsToRemove = ctx.ContactData.Where(x => x.BID == TestConstants.BID && ((x.First == NewTestContactName[0] && x.Last == $"{NewTestContactName[1]} {currDateTime}") || x.ShortName.StartsWith($"{NewTestContactName[0]} {NewTestContactName[1]}")));
                    var idsList = contactsToRemove.Select(c => c.ID).ToList();
                    ctx.ContactCustomData.RemoveRange(ctx.ContactCustomData.Where(x => x.BID == TestConstants.BID && idsList.Contains(x.ID)));
                    ctx.ContactLocator.RemoveRange(ctx.ContactLocator.Where(x => x.BID == TestConstants.BID && idsList.Contains(x.ParentID)));
                    ctx.ContactData.RemoveRange(contactsToRemove);
                    await ctx.SaveChangesAsync();
                    ctx.CustomFieldDefinition.RemoveRange(ctx.CustomFieldDefinition.Where(x => x.BID == TestConstants.BID && x.ID == -99));
                    await ctx.SaveChangesAsync();
                }
            }
            catch { }
        }

        [TestMethod]
        public async Task TestUpdateContactCompany()
        {
            short testBID = 1;
            var ctx = GetApiContext(testBID);

            try
            {
                #region Remove if Existing Data

                var negativeLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (negativeLinks != null)
                {
                    ctx.RemoveRange(negativeLinks);
                    await ctx.SaveChangesAsync();
                }

                var negativeContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (negativeContacts != null)
                {
                    ctx.RemoveRange(negativeContacts);
                    await ctx.SaveChangesAsync();
                }

                var negativeCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (negativeCompanies != null)
                {
                    ctx.RemoveRange(negativeCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                #region Create Data 

                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var company1 = new CompanyData()
                {
                    BID = testBID,
                    ID = -99,
                    Name = "TEST COMPANY 1",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                ctx.CompanyData.AddRange(new[] { company1 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var contact1 = GetNewContact();
               

                #endregion

                var postContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, 
                                  $"{apiUrl}", JsonConvert.SerializeObject(contact1), HttpStatusCode.OK);
                Assert.IsNotNull(postContact);

                var getContact = await EndpointTests.AssertGetStatusCode<ContactData>(client, 
                                  $"{apiUrl}/{postContact.ID}", HttpStatusCode.OK);

                Assert.IsNotNull(getContact);
                Assert.IsNotNull(getContact.CompanyContactLinks);
                var getCompany = await EndpointTests.AssertGetStatusCode<CompanyData>(client, $"api/Company/{company1.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(getCompany);
                getContact.CompanyContactLinks = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        CompanyID = getCompany.ID,
                        ContactID = getContact.ID
                    }
                };
                var response = await EndpointTests.AssertPutStatusCode<ContactData>(client
                    , $"{apiUrl}/{getContact.ID}", JsonConvert.SerializeObject(getContact), HttpStatusCode.OK);
                Assert.IsNotNull(response);
            }
            finally
            {
                #region Cleanup Data

                var cleanupLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (cleanupLinks != null)
                {
                    ctx.RemoveRange(cleanupLinks);
                    await ctx.SaveChangesAsync();
                }

                var cleanupContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupContacts != null)
                {
                    ctx.RemoveRange(cleanupContacts);
                    await ctx.SaveChangesAsync();
                }

                var cleanupCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupCompanies != null)
                {
                    ctx.RemoveRange(cleanupCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion
            }
        }

        [TestMethod]
        public async Task TestContactCreateAsAdHoc()
        {
            var adHocContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}", JsonConvert.SerializeObject(GetNewAdHocContact()), HttpStatusCode.OK);
            Assert.IsNotNull(adHocContact);
            Assert.IsNotNull(adHocContact.CompanyContactLinks);
            var company = adHocContact.CompanyContactLinks.FirstOrDefault().Company;
            Assert.IsNotNull(company);
            Assert.IsTrue(company.IsAdHoc);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{adHocContact.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{adHocContact.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode(client, $"api/company/{company.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task TestContactCreateAsNonAdHoc()
        {
            var company = await GetNewCompanyData(-101);
            var nonAdHocContact = GetNewAdHocContact(company);

            var createdContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}", JsonConvert.SerializeObject(nonAdHocContact), HttpStatusCode.OK);
            Assert.IsNotNull(createdContact);
            Assert.IsNotNull(createdContact.CompanyContactLinks);
            var companyFromContact = createdContact.CompanyContactLinks.FirstOrDefault().Company;
            Assert.AreEqual(company.ID, companyFromContact.ID);
            Assert.IsNotNull(companyFromContact);
            Assert.IsFalse(companyFromContact.IsAdHoc);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdContact.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{createdContact.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task TestContactSetActiveInactivePrimaryBillingEndpoints()
        {
            short testBID = 1;
            var ctx = GetApiContext(testBID);

            try
            {
                #region Remove if Existing Data

                var negativeLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (negativeLinks != null)
                {
                    ctx.RemoveRange(negativeLinks);
                    await ctx.SaveChangesAsync();
                }

                var negativeContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (negativeContacts != null)
                {
                    ctx.RemoveRange(negativeContacts);
                    await ctx.SaveChangesAsync();
                }

                var negativeCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (negativeCompanies != null)
                {
                    ctx.RemoveRange(negativeCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                #region Create Data 

                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var company1 = new CompanyData()
                {
                    BID = testBID,
                    ID = -99,
                    Name = "TEST COMPANY 1",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                var company2 = new CompanyData()
                {
                    BID = testBID,
                    ID = -98,
                    Name = "TEST COMPANY 2",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                ctx.CompanyData.AddRange(new[] { company1, company2 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var contact1 = new ContactData()
                {
                    BID = testBID,
                    ID = -99,
                    First = "TEST",
                    Last = "SUBJECT1",
                };
                var contact2 = new ContactData()
                {
                    BID = testBID,
                    ID = -98,
                    First = "TEST",
                    Last = "SUBJECT2",
                };
                ctx.ContactData.AddRange(new[] { contact1, contact2 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var companyContactLink1 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company1.ID,
                    ContactID = contact1.ID,
                    Roles = (ContactRole.Primary | ContactRole.Billing)
                };
                var companyContactLink2 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company2.ID,
                    ContactID = contact1.ID,
                    Roles = ContactRole.Inactive
                };
                var companyContactLink3 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company2.ID,
                    ContactID = contact2.ID,
                    Roles = (ContactRole.Primary | ContactRole.Billing)
                };
                var companyContactLink4 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company1.ID,
                    ContactID = contact2.ID,
                    Roles = ContactRole.Inactive
                };
                ctx.CompanyContactLink.AddRange(new[] { companyContactLink1, companyContactLink2, companyContactLink3, companyContactLink4 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                #endregion

                Assert.AreEqual(ContactRole.Inactive, (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact1.ID && l.CompanyID == company2.ID)).Roles);
                Assert.AreEqual((ContactRole.Primary | ContactRole.Billing), (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact2.ID && l.CompanyID == company2.ID)).Roles);

                // set inactive should fail
                var statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client
                    , $"{apiUrl}/{contact2.ID}/Action/SetInactive/{company2.ID}", HttpStatusCode.BadRequest);
                Assert.IsNotNull(statusResp);
                Assert.IsFalse(statusResp.Success);
                Assert.AreEqual($"You can't set the primary or billing contact inactive without first setting another contact to those roles. ContactID={contact2.ID}"
                    , statusResp.ErrorMessage);

                // set primary: Con1, Co2
                statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client
                    , $"{apiUrl}/{contact1.ID}/Action/SetDefault/{company2.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(statusResp);
                Assert.IsTrue(statusResp.Success);
                Assert.AreEqual(ContactRole.Primary, (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact1.ID && l.CompanyID == company2.ID)).Roles);
                Assert.AreEqual(ContactRole.Billing, (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact2.ID && l.CompanyID == company2.ID)).Roles);
                // set billing: Con1, Co2
                statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client
                    , $"{apiUrl}/{contact1.ID}/Action/SetBilling/{company2.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(statusResp);
                Assert.IsTrue(statusResp.Success);
                Assert.AreEqual((ContactRole.Primary | ContactRole.Billing), (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact1.ID && l.CompanyID == company2.ID)).Roles);
                Assert.AreEqual(ContactRole.Observer, (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact2.ID && l.CompanyID == company2.ID)).Roles);

                // set primary: Con2, Co2
                statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client
                    , $"{apiUrl}/{contact2.ID}/Action/SetDefault/{company2.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(statusResp);
                Assert.IsTrue(statusResp.Success);
                // set billing: Con2, Co2
                statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client
                    , $"{apiUrl}/{contact2.ID}/Action/SetBilling/{company2.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(statusResp);
                Assert.IsTrue(statusResp.Success);
                Assert.AreEqual((ContactRole.Primary | ContactRole.Billing), (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact2.ID && l.CompanyID == company2.ID)).Roles);

                // set primary: Con2, Co1
                statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client
                    , $"{apiUrl}/{contact2.ID}/Action/SetDefault/{company1.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(statusResp);
                Assert.IsTrue(statusResp.Success);
                // set billing: Con2, Co1
                statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client
                    , $"{apiUrl}/{contact2.ID}/Action/SetBilling/{company1.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(statusResp);
                Assert.IsTrue(statusResp.Success);
                Assert.AreEqual((ContactRole.Primary | ContactRole.Billing), (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact2.ID && l.CompanyID == company2.ID)).Roles);

                // set inactive: Con1, Co2
                statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client
                    , $"{apiUrl}/{contact1.ID}/Action/SetInactive/{company2.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(statusResp);
                Assert.IsTrue(statusResp.Success);
                Assert.AreEqual(ContactRole.Inactive, (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact1.ID && l.CompanyID == company2.ID)).Roles);

                // set active, all linked companies: Con1
                statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client
                    , $"{apiUrl}/{contact1.ID}/Action/SetActive", HttpStatusCode.OK);
                Assert.IsNotNull(statusResp);
                Assert.IsTrue(statusResp.Success);
                Assert.AreEqual(ContactRole.Observer, (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact1.ID && l.CompanyID == company1.ID)).Roles);
                Assert.AreEqual(ContactRole.Observer, (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact1.ID && l.CompanyID == company2.ID)).Roles);

                // set inactive, all linked companies: Con1
                statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client
                    , $"{apiUrl}/{contact1.ID}/Action/SetInactive", HttpStatusCode.OK);
                Assert.IsNotNull(statusResp);
                Assert.IsTrue(statusResp.Success);
                Assert.AreEqual(ContactRole.Inactive, (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact1.ID && l.CompanyID == company1.ID)).Roles);
                Assert.AreEqual(ContactRole.Inactive, (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact1.ID && l.CompanyID == company2.ID)).Roles);
            }
            finally
            {
                #region Cleanup Data

                var cleanupLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (cleanupLinks != null)
                {
                    ctx.RemoveRange(cleanupLinks);
                    await ctx.SaveChangesAsync();
                }

                var cleanupContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupContacts != null)
                {
                    ctx.RemoveRange(cleanupContacts);
                    await ctx.SaveChangesAsync();
                }

                var cleanupCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupCompanies != null)
                {
                    ctx.RemoveRange(cleanupCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion
            }
        }

        [TestMethod]
        public async Task TestContactLinkUnlinkEndpoints()
        {
            short testBID = 1;
            var ctx = GetApiContext(testBID);

            try
            {
                #region Remove if Existing Data

                var negativeOrderContactRoles = await ctx.OrderContactRole.Where(x => x.ID < 0 || x.ContactID < 0 || x.OrderID < 0).ToListAsync();
                if (negativeOrderContactRoles != null)
                {
                    ctx.RemoveRange(negativeOrderContactRoles);
                    await ctx.SaveChangesAsync();
                }

                var negativeOrders = await ctx.TransactionHeaderData.Where(x => x.ID < 0).ToListAsync();
                if (negativeOrders != null)
                {
                    ctx.RemoveRange(negativeOrders);
                    await ctx.SaveChangesAsync();
                }

                var negativeLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (negativeLinks != null)
                {
                    ctx.RemoveRange(negativeLinks);
                    await ctx.SaveChangesAsync();
                }

                var negativeContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (negativeContacts != null)
                {
                    ctx.RemoveRange(negativeContacts);
                    await ctx.SaveChangesAsync();
                }

                var negativeCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (negativeCompanies != null)
                {
                    ctx.RemoveRange(negativeCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                #region Create Data 

                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync(t => t.BID == testBID);
                if (taxGroup == null)
                    Assert.Inconclusive("No tax group found. Unable to complete test.");

                var company1 = new CompanyData()
                {
                    BID = testBID,
                    ID = -99,
                    Name = "TEST COMPANY 1",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                var company2 = new CompanyData()
                {
                    BID = testBID,
                    ID = -98,
                    Name = "TEST COMPANY 2",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                ctx.CompanyData.AddRange(new[] { company1, company2 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var contact1 = new ContactData()
                {
                    BID = testBID,
                    ID = -99,
                    First = "TEST",
                    Last = "SUBJECT1",
                };
                var contact2 = new ContactData()
                {
                    BID = testBID,
                    ID = -98,
                    First = "TEST",
                    Last = "SUBJECT2",
                };
                ctx.ContactData.AddRange(new[] { contact1, contact2 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var companyContactLink1 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company1.ID,
                    ContactID = contact1.ID,
                    Roles = (ContactRole.Primary | ContactRole.Billing)
                };
                var companyContactLink2 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company2.ID,
                    ContactID = contact2.ID,
                    Roles = ContactRole.Observer
                };
                ctx.CompanyContactLink.AddRange(new[] { companyContactLink1, companyContactLink2 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var order = new OrderData()
                {
                    BID = testBID,
                    ID = -99,
                    CompanyID = company2.ID,
                    LocationID = location.ID,
                    ProductionLocationID = location.ID,
                    PickupLocationID = location.ID,
                    FormattedNumber = "",
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    TaxGroupID = taxGroup.ID
                };
                ctx.OrderData.AddRange(new[] { order });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var orderContactRole = new OrderContactRole()
                {
                    BID = testBID,
                    ID = -99,
                    OrderID = order.ID,
                    ContactID = contact2.ID,
                    RoleType = OrderContactRoleType.Primary
                };
                ctx.OrderContactRole.AddRange(new[] { orderContactRole });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                #endregion

                // Link
                var statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client
                    , $"{apiUrl}/{contact1.ID}/Action/Link/Company/{company2.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(statusResp);
                Assert.IsTrue(statusResp.Success);
                Assert.IsNotNull((await ctx.ContactData.AsNoTracking().Include(c => c.CompanyContactLinks)
                    .FirstOrDefaultAsync(c => c.BID == testBID && c.ID == contact1.ID
                        && c.CompanyContactLinks.Any(cc => c.BID == testBID && cc.ContactID == contact1.ID && cc.CompanyID == company2.ID))));
                Assert.AreEqual(ContactRole.Observer, (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact1.ID && l.CompanyID == company2.ID)).Roles);

                // Unlink
                statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client
                    , $"{apiUrl}/{contact1.ID}/Action/Unlink/Company/{company2.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(statusResp);
                Assert.IsTrue(statusResp.Success);
                Assert.IsNull((await ctx.ContactData.AsNoTracking().Include(c => c.CompanyContactLinks)
                    .FirstOrDefaultAsync(c => c.BID == testBID && c.ID == contact1.ID
                        && c.CompanyContactLinks.Any(ccl => c.BID == testBID && ccl.ContactID == contact1.ID && ccl.CompanyID == company2.ID))));

                // Unlink should fail for primary/billing
                statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client
                    , $"{apiUrl}/{contact1.ID}/Action/Unlink/Company/{company1.ID}", HttpStatusCode.BadRequest);
                Assert.IsNotNull(statusResp);
                Assert.IsFalse(statusResp.Success);
                Assert.AreEqual("Contact is set as either Billing or Primary for the Company and cannot be unlinked."
                    , statusResp.ErrorMessage);

                // Unlink should fail for associated order
                statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client
                    , $"{apiUrl}/{contact2.ID}/Action/Unlink/Company/{company2.ID}", HttpStatusCode.BadRequest);
                Assert.IsNotNull(statusResp);
                Assert.IsFalse(statusResp.Success);
                Assert.AreEqual("Contact is associated with one or more Orders or Estimates for the Company and cannot be unlinked."
                    , statusResp.ErrorMessage);
            }
            finally
            {
                #region Cleanup Data

                var cleanupOrderContactRoles = await ctx.OrderContactRole.Where(x => x.ID < 0 || x.ContactID < 0 || x.OrderID < 0).ToListAsync();
                if (cleanupOrderContactRoles != null)
                {
                    ctx.RemoveRange(cleanupOrderContactRoles);
                    await ctx.SaveChangesAsync();
                }

                var cleanupOrders = await ctx.TransactionHeaderData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupOrders != null)
                {
                    ctx.RemoveRange(cleanupOrders);
                    await ctx.SaveChangesAsync();
                }

                var cleanupLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (cleanupLinks != null)
                {
                    ctx.RemoveRange(cleanupLinks);
                    await ctx.SaveChangesAsync();
                }

                var cleanupContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupContacts != null)
                {
                    ctx.RemoveRange(cleanupContacts);
                    await ctx.SaveChangesAsync();
                }

                var cleanupCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupCompanies != null)
                {
                    ctx.RemoveRange(cleanupCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion
            }
        }

        [TestMethod]
        public async Task TestContactGetCompanyLinksEndpoint()
        {
            short testBID = 1;
            var ctx = GetApiContext(testBID);

            try
            {
                #region Remove if Existing Data

                var negativeLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (negativeLinks != null)
                {
                    ctx.RemoveRange(negativeLinks);
                    await ctx.SaveChangesAsync();
                }

                var negativeContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (negativeContacts != null)
                {
                    ctx.RemoveRange(negativeContacts);
                    await ctx.SaveChangesAsync();
                }

                var negativeCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (negativeCompanies != null)
                {
                    ctx.RemoveRange(negativeCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                #region Create Data 

                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var company1 = new CompanyData()
                {
                    BID = testBID,
                    ID = -99,
                    Name = "TEST COMPANY 1",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                var company2 = new CompanyData()
                {
                    BID = testBID,
                    ID = -98,
                    Name = "TEST COMPANY 2",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                ctx.CompanyData.AddRange(new[] { company1, company2 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var contact1 = new ContactData()
                {
                    BID = testBID,
                    ID = -99,
                    First = "TEST",
                    Last = "SUBJECT1",
                };
                var contact2 = new ContactData()
                {
                    BID = testBID,
                    ID = -98,
                    First = "TEST",
                    Last = "SUBJECT2",
                };
                var contact3 = new ContactData()
                {
                    BID = testBID,
                    ID = -97,
                    First = "TEST",
                    Last = "SUBJECT3",
                };
                var contact4 = new ContactData()
                {
                    BID = testBID,
                    ID = -96,
                    First = "TEST",
                    Last = "SUBJECT4",
                };
                ctx.ContactData.AddRange(new[] { contact1, contact2, contact3, contact4 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var companyContactLink1 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company1.ID,
                    ContactID = contact1.ID,
                    Roles = (ContactRole.Primary | ContactRole.Billing)
                };
                var companyContactLink2 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company1.ID,
                    ContactID = contact2.ID,
                    Roles = ContactRole.Inactive
                };
                var companyContactLink3 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company2.ID,
                    ContactID = contact3.ID,
                    Roles = ContactRole.Observer
                };
                var companyContactLink4 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company2.ID,
                    ContactID = contact4.ID,
                    Roles = (ContactRole.Primary | ContactRole.Billing)
                };
                ctx.CompanyContactLink.AddRange(new[] { companyContactLink1, companyContactLink2, companyContactLink3, companyContactLink4 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                #endregion

                var response = await EndpointTests.AssertGetStatusCode<ICollection<CompanyContactLink>>(client
                    , $"{apiUrl}/Company/{company1.ID}/ContactLinks", HttpStatusCode.OK);
                Assert.IsNotNull(response);
                Assert.AreEqual(2, response.Count);
                Assert.IsNotNull(response.FirstOrDefault().Contact);

                response = await EndpointTests.AssertGetStatusCode<ICollection<CompanyContactLink>>(client
                    , $"{apiUrl}/Company/{company2.ID}/ContactLinks", HttpStatusCode.OK);
                Assert.IsNotNull(response);
                Assert.AreEqual(2, response.Count);
                Assert.IsNotNull(response.FirstOrDefault().Contact);

                var badCompanyID = -123;
                await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/Company/{badCompanyID}/ContactLinks", HttpStatusCode.NotFound);
            }
            finally
            {
                #region Cleanup Data

                var cleanupLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (cleanupLinks != null)
                {
                    ctx.RemoveRange(cleanupLinks);
                    await ctx.SaveChangesAsync();
                }

                var cleanupContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupContacts != null)
                {
                    ctx.RemoveRange(cleanupContacts);
                    await ctx.SaveChangesAsync();
                }

                var cleanupCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupCompanies != null)
                {
                    ctx.RemoveRange(cleanupCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion
            }
        }

        [TestMethod]
        public async Task TestAdHocContactUpdatePositionSucceeds()
        {
            short testBID = 1;
            var ctx = GetApiContext(testBID);

            try
            {
                #region Remove if Existing Data

                var negativeLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (negativeLinks != null)
                {
                    ctx.RemoveRange(negativeLinks);
                    await ctx.SaveChangesAsync();
                }

                var negativeContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (negativeContacts != null)
                {
                    ctx.RemoveRange(negativeContacts);
                    await ctx.SaveChangesAsync();
                }

                var negativeCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (negativeCompanies != null)
                {
                    ctx.RemoveRange(negativeCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                #region Create Data 

                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var company1 = new CompanyData()
                {
                    BID = testBID,
                    ID = -99,
                    Name = "TEST SUBJECT1",
                    LocationID = location.ID,
                    IsAdHoc = true,
                    StatusID = 1
                };
                ctx.CompanyData.Add(company1);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var contact1 = new ContactData()
                {
                    BID = testBID,
                    ID = -99,
                    First = "TEST",
                    Last = "SUBJECT1",
                    LocationID = location.ID,
                };
                ctx.ContactData.Add(contact1);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var companyContactLink1 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company1.ID,
                    ContactID = contact1.ID,
                    Roles = (ContactRole.Primary | ContactRole.Billing)
                };
                ctx.CompanyContactLink.Add(companyContactLink1);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                #endregion

                var getContact = await EndpointTests.AssertGetStatusCode<ContactData>(client, $"{apiUrl}/{contact1.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(getContact);
                Assert.IsNotNull(getContact.CompanyContactLinks);
                Assert.IsNull(getContact.CompanyContactLinks.FirstOrDefault().Position);

                var position = "TEST POSITION";
                getContact.CompanyContactLinks.FirstOrDefault().Position = position;
                var putContact = await EndpointTests.AssertPutStatusCode<ContactData>(client, $"{apiUrl}/{contact1.ID}", JsonConvert.SerializeObject(getContact), HttpStatusCode.OK);
                Assert.IsNotNull(putContact);
                Assert.IsNotNull(putContact.CompanyContactLinks);
                Assert.IsNotNull(putContact.CompanyContactLinks.FirstOrDefault().Position);
                Assert.AreEqual(position, putContact.CompanyContactLinks.FirstOrDefault().Position);

                getContact = await EndpointTests.AssertGetStatusCode<ContactData>(client, $"{apiUrl}/{contact1.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(getContact);
                Assert.IsNotNull(getContact.CompanyContactLinks);
                Assert.IsNotNull(getContact.CompanyContactLinks.FirstOrDefault().Position);
                Assert.AreEqual(position, getContact.CompanyContactLinks.FirstOrDefault().Position);
            }
            finally
            {
                #region Cleanup Data

                var cleanupLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (cleanupLinks != null)
                {
                    ctx.RemoveRange(cleanupLinks);
                    await ctx.SaveChangesAsync();
                }

                var cleanupContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupContacts != null)
                {
                    ctx.RemoveRange(cleanupContacts);
                    await ctx.SaveChangesAsync();
                }

                var cleanupCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupCompanies != null)
                {
                    ctx.RemoveRange(cleanupCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion
            }
        }

        [TestMethod]
        public async Task TestNonAdHocContactUpdatePositionSucceeds()
        {
            short testBID = 1;
            var ctx = GetApiContext(testBID);

            try
            {
                #region Remove if Existing Data

                var negativeLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (negativeLinks != null)
                {
                    ctx.RemoveRange(negativeLinks);
                    await ctx.SaveChangesAsync();
                }

                var negativeContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (negativeContacts != null)
                {
                    ctx.RemoveRange(negativeContacts);
                    await ctx.SaveChangesAsync();
                }

                var negativeCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (negativeCompanies != null)
                {
                    ctx.RemoveRange(negativeCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                #region Create Data 

                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var company1 = new CompanyData()
                {
                    BID = testBID,
                    ID = -99,
                    Name = "TEST COMPANY 1",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                ctx.CompanyData.Add(company1);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var contact1 = new ContactData()
                {
                    BID = testBID,
                    ID = -99,
                    First = "TEST",
                    Last = "SUBJECT1",
                    LocationID = location.ID,
                };
                ctx.ContactData.Add(contact1);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var companyContactLink1 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company1.ID,
                    ContactID = contact1.ID,
                    Roles = (ContactRole.Primary | ContactRole.Billing)
                };
                ctx.CompanyContactLink.Add(companyContactLink1);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                #endregion

                var getContact = await EndpointTests.AssertGetStatusCode<ContactData>(client, $"{apiUrl}/{contact1.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(getContact);
                Assert.IsNotNull(getContact.CompanyContactLinks);
                Assert.IsNull(getContact.CompanyContactLinks.FirstOrDefault().Position);

                var position = "TEST POSITION";
                getContact.CompanyContactLinks.FirstOrDefault().Position = position;
                var putContact = await EndpointTests.AssertPutStatusCode<ContactData>(client, $"{apiUrl}/{contact1.ID}", JsonConvert.SerializeObject(getContact), HttpStatusCode.OK);
                Assert.IsNotNull(putContact);
                Assert.IsNotNull(putContact.CompanyContactLinks);
                Assert.IsNotNull(putContact.CompanyContactLinks.FirstOrDefault().Position);
                Assert.AreEqual(position, putContact.CompanyContactLinks.FirstOrDefault().Position);

                getContact = await EndpointTests.AssertGetStatusCode<ContactData>(client, $"{apiUrl}/{contact1.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(getContact);
                Assert.IsNotNull(getContact.CompanyContactLinks);
                Assert.IsNotNull(getContact.CompanyContactLinks.FirstOrDefault().Position);
                Assert.AreEqual(position, getContact.CompanyContactLinks.FirstOrDefault().Position);
            }
            finally
            {
                #region Cleanup Data

                var cleanupLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (cleanupLinks != null)
                {
                    ctx.RemoveRange(cleanupLinks);
                    await ctx.SaveChangesAsync();
                }

                var cleanupContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupContacts != null)
                {
                    ctx.RemoveRange(cleanupContacts);
                    await ctx.SaveChangesAsync();
                }

                var cleanupCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupCompanies != null)
                {
                    ctx.RemoveRange(cleanupCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion
            }
        }

        [TestMethod]
        public async Task TestCreateNewAdHocContactSucceeds()
        {
            var createdContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(GetNewAdHocContact()), HttpStatusCode.OK);
            Assert.IsNotNull(createdContact);
            Assert.IsNotNull(createdContact.CompanyContactLinks);
            Assert.IsTrue(createdContact.CompanyContactLinks.FirstOrDefault().Company.IsAdHoc == true);
            var companyID = createdContact.CompanyContactLinks.FirstOrDefault().CompanyID;
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdContact.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{createdContact.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode(client, $"Api/Company/{companyID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/Company/{companyID}/ContactLinks", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task TestCreateNewContactWithExistingCompany()
        {
            short testBID = TestConstants.BID;
            try
            {
                #region Remove if Existing Data

                var ctx = GetApiContext(testBID);

                var negativeCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (negativeCompanies != null)
                {
                    ctx.RemoveRange(negativeCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                #region Create Data

                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var company1 = new CompanyData()
                {
                    BID = testBID,
                    ID = -99,
                    Name = "TEST COMPANY 1",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                var company2 = new CompanyData()
                {
                    BID = testBID,
                    ID = -98,
                    Name = "TEST COMPANY 2",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                ctx.CompanyData.AddRange(new[] { company1, company2 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                #endregion

                var contactToCreate = GetNewAdHocContact(company1);
                contactToCreate.CompanyContactLinks.Add(new CompanyContactLink()
                {
                    CompanyID = company2.ID,
                    Company = company2
                });
                var createdContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}", JsonConvert.SerializeObject(contactToCreate), HttpStatusCode.OK);
                Assert.IsNotNull(createdContact);
                Assert.IsNotNull(createdContact.CompanyContactLinks);
                Assert.IsTrue(createdContact.CompanyContactLinks.Count == 2);
                Assert.IsTrue(createdContact.CompanyContactLinks.Any(l => l.CompanyID == company1.ID && l.ContactID == createdContact.ID));
                Assert.IsTrue(createdContact.CompanyContactLinks.Any(l => l.CompanyID == company2.ID && l.ContactID == createdContact.ID));
                Assert.IsNotNull(createdContact.CompanyContactLinks.FirstOrDefault().Company);

                await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdContact.ID}", HttpStatusCode.NoContent);
                await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{createdContact.ID}", HttpStatusCode.NotFound);
                var links = await EndpointTests.AssertGetStatusCode<ICollection<CompanyContactLink>>(client
                    , $"{apiUrl}/Company/{company1.ID}/ContactLinks", HttpStatusCode.OK);
                Assert.IsNotNull(links);
                Assert.IsFalse(links.Any(l => l.ContactID == createdContact.ID));
            }
            finally
            {
                #region Remove if Existing Data

                var ctx = GetApiContext(testBID);

                var cleanupCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupCompanies != null)
                {
                    ctx.RemoveRange(cleanupCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion
            }
        }

        [TestMethod]
        public async Task TestDeleteContactWithCompanyLinksEndpoints()
        {
            short testBID = 1;
            try
            {
                #region Remove if Existing Data

                var ctx = GetApiContext(testBID);

                var negativeOrderContactRoles = await ctx.OrderContactRole.Where(x => x.ID < 0 || x.ContactID < 0 || x.OrderID < 0).ToListAsync();
                if (negativeOrderContactRoles != null)
                {
                    ctx.RemoveRange(negativeOrderContactRoles);
                    await ctx.SaveChangesAsync();
                }

                var negativeOrders = await ctx.TransactionHeaderData.Where(x => x.ID < 0).ToListAsync();
                if (negativeOrders != null)
                {
                    ctx.RemoveRange(negativeOrders);
                    await ctx.SaveChangesAsync();
                }

                var negativeLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (negativeLinks != null)
                {
                    ctx.RemoveRange(negativeLinks);
                    await ctx.SaveChangesAsync();
                }

                var negativeContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (negativeContacts != null)
                {
                    ctx.RemoveRange(negativeContacts);
                    await ctx.SaveChangesAsync();
                }

                var negativeCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (negativeCompanies != null)
                {
                    ctx.RemoveRange(negativeCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                #region Create Data 

                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync(t => t.BID == testBID);
                if (taxGroup == null)
                    Assert.Inconclusive("No tax group found. Unable to complete test.");

                var company1 = new CompanyData()
                {
                    BID = testBID,
                    ID = -99,
                    Name = "TEST COMPANY 1",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                var company2 = new CompanyData()
                {
                    BID = testBID,
                    ID = -98,
                    Name = "TEST COMPANY 2",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                ctx.CompanyData.AddRange(new[] { company1, company2 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var contact1 = new ContactData()
                {
                    BID = testBID,
                    ID = -99,
                    First = "TEST",
                    Last = "SUBJECT1",
                };
                var contact2 = new ContactData()
                {
                    BID = testBID,
                    ID = -98,
                    First = "TEST",
                    Last = "SUBJECT2",
                };
                var contact3 = new ContactData()
                {
                    BID = testBID,
                    ID = -97,
                    First = "TEST",
                    Last = "SUBJECT3",
                };
                ctx.ContactData.AddRange(new[] { contact1, contact2, contact3 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var companyContactLink1 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company1.ID,
                    ContactID = contact1.ID,
                    Roles = ContactRole.Observer
                };
                var companyContactLink2 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company2.ID,
                    ContactID = contact2.ID,
                    Roles = (ContactRole.Primary | ContactRole.Billing)
                };
                var companyContactLink3 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company2.ID,
                    ContactID = contact3.ID,
                    Roles = ContactRole.Observer
                };
                ctx.CompanyContactLink.AddRange(new[] { companyContactLink1, companyContactLink2, companyContactLink3 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var order = new OrderData()
                {
                    BID = testBID,
                    ID = -99,
                    CompanyID = company2.ID,
                    LocationID = location.ID,
                    ProductionLocationID = location.ID,
                    PickupLocationID = location.ID,
                    FormattedNumber = "",
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    TaxGroupID = taxGroup.ID
                };
                ctx.OrderData.AddRange(new[] { order });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var orderContactRole = new OrderContactRole()
                {
                    BID = testBID,
                    ID = -99,
                    OrderID = order.ID,
                    ContactID = contact3.ID,
                    RoleType = OrderContactRoleType.Primary
                };
                ctx.OrderContactRole.AddRange(new[] { orderContactRole });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                #endregion

                // Delete should pass
                await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{contact1.ID}", HttpStatusCode.NoContent);
                await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{contact1.ID}", HttpStatusCode.NotFound);

                // Delete should fail for primary/billing
                var response = await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{contact2.ID}", HttpStatusCode.BadRequest);
                Assert.IsNotNull(response);
                var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(await response.Content.ReadAsStringAsync());
                Assert.AreEqual("INTERNAL ERROR: Contact is set as either Billing or Primary for the Company and cannot be Deleted."
                    , result["ErrorMessage"]);

                // Delete should fail for associated order
                response = await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{contact3.ID}", HttpStatusCode.BadRequest);
                Assert.IsNotNull(response);
                result = JsonConvert.DeserializeObject<Dictionary<string, string>>(await response.Content.ReadAsStringAsync());
                Assert.AreEqual("INTERNAL ERROR: Contact is associated with one or more Orders or Estimates for the Company and cannot be Deleted."
                    , result["ErrorMessage"]);
            }
            finally
            {
                #region Cleanup Data

                var ctx = GetApiContext(testBID);
                var cleanupOrderContactRoles = await ctx.OrderContactRole.Where(x => x.ID < 0 || x.ContactID < 0 || x.OrderID < 0).ToListAsync();
                if (cleanupOrderContactRoles != null)
                {
                    ctx.RemoveRange(cleanupOrderContactRoles);
                    await ctx.SaveChangesAsync();
                }

                var cleanupOrders = await ctx.TransactionHeaderData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupOrders != null)
                {
                    ctx.RemoveRange(cleanupOrders);
                    await ctx.SaveChangesAsync();
                }

                var cleanupLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (cleanupLinks != null)
                {
                    ctx.RemoveRange(cleanupLinks);
                    await ctx.SaveChangesAsync();
                }

                var cleanupContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupContacts != null)
                {
                    ctx.RemoveRange(cleanupContacts);
                    await ctx.SaveChangesAsync();
                }

                var cleanupCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupCompanies != null)
                {
                    ctx.RemoveRange(cleanupCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion
            }
        }

        [TestMethod]
        public async Task TestContactWithInvalidCompany()
        {
            var resp = await EndpointTests.AssertPostStatusCode<GenericResponse<ContactData>>(client, $"{apiUrl}/", JsonConvert.SerializeObject(GetNewAdHocContactWithInvalidCompanyID()), HttpStatusCode.NotFound);
            Assert.IsNotNull(resp);
            Assert.AreEqual("Company with ID `-123` not found.",
                resp.ErrorMessage);
        }

        [TestMethod]
        public async Task TestContactWithInvalidLocation()
        {
            var ctx = GetApiContext();

            var newContact = GetNewAdHocContact();
            var locationID = (await ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefaultAsync(x => x.BID == TestConstants.BID)).ID;
            locationID += 1;
            newContact.LocationID = locationID; 

            var response = await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(newContact), HttpStatusCode.BadRequest);
            Assert.IsNotNull(response);
            var errorContent = await response.Content.ReadAsStringAsync();
            Assert.IsNotNull(errorContent);
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(errorContent);
            Assert.AreEqual(result.Last().Value, "Invalid location.");
        }

        [TestMethod]
        public async Task TestContactDeleteWithForceEndpoint()
        {
            var newContact = GetNewAdHocContact();

            //test: expect OK
            newContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}", JsonConvert.SerializeObject(newContact), HttpStatusCode.OK);
            Assert.IsNotNull(newContact);

            var ctx = GetApiContext();

            var testUserLink = await Utils.GetAuthUserLink(ctx);
            if (testUserLink != null && testUserLink.UserAccessType < UserAccessType.SupportManager)
            {
                testUserLink.UserAccessType = UserAccessType.SupportManager;
                ctx.SaveChanges();
            }
            
            ContactCustomData newContactCusData =
                await ctx.ContactCustomData.Where(x => x.BID == TestConstants.BID && x.ID == newContact.ID).FirstOrDefaultAsync();

            if (newContactCusData == null)
            {
                newContactCusData = new ContactCustomData()
                {
                    ID = newContact.ID,
                    BID = TestConstants.BID,
                    AppliesToClassTypeID = 3000
                };

                await ctx.ContactCustomData.AddAsync(newContactCusData);
            }
            
            ContactLocator newContactLocator =
                await ctx.ContactLocator.Where(x => x.BID == TestConstants.BID && x.ParentID == newContact.ID).FirstOrDefaultAsync();

            if (newContactLocator == null)
            {
                newContactLocator = new ContactLocator()
                {
                    ID = -newContact.ID,
                    BID = TestConstants.BID,
                    ParentID = newContact.ID,
                    ModifiedDT = DateTime.UtcNow,
                    LocatorType = 2,
                    Locator = "(801) 836-7117",
                    RawInput = "8018367117",
                    SortIndex = 1,
                    IsVerified = false,
                    IsValid = true
                };

                await ctx.ContactLocator.AddAsync(newContactLocator);
            }

            ContactTagLink newContactTagLink =
                await ctx.ContactTagLink.Where(x => x.BID == TestConstants.BID && x.ContactID == newContact.ID).FirstOrDefaultAsync();

            if (newContactTagLink == null)
            {
                newContactTagLink = new ContactTagLink()
                {
                    BID = TestConstants.BID,
                    ContactID = newContact.ID,
                    TagID = (await ctx.ListTag.FirstAsync(x => x.BID == TestConstants.BID)).ID,
                };

                await ctx.ContactTagLink.AddAsync(newContactTagLink);
            }
            
            MessageParticipantInfo newMsgPartInfo =
                await ctx.MessageParticipantInfo.Where(x => x.BID == TestConstants.BID && x.ContactID == newContact.ID).FirstOrDefaultAsync();

            if (newMsgPartInfo == null)
            {
                newMsgPartInfo = new MessageParticipantInfo()
                {
                    ID = -newContact.ID,
                    BID = TestConstants.BID,
                    Channel = MessageChannelType.Email,
                    IsMergeField = false,
                    ModifiedDT = DateTime.UtcNow,
                    ContactID = newContact.ID,
                    ParticipantRoleType = MessageParticipantRoleType.From,
                    BodyID = (await ctx.MessageBody.FirstOrDefaultAsync(x => x.BID == TestConstants.BID)).ID
                };

                await ctx.MessageParticipantInfo.AddAsync(newMsgPartInfo);
            }

            MessageDeliveryRecord newMsgDelRec =
                await ctx.MessageDeliveryRecord.Where(x => x.BID == TestConstants.BID && x.ParticipantID == newMsgPartInfo.ID).FirstOrDefaultAsync();

            if (newMsgDelRec == null)
            {
                newMsgDelRec = new MessageDeliveryRecord()
                {
                    ID = -newContact.ID,
                    BID = TestConstants.BID,
                    ParticipantID = newMsgPartInfo.ID,
                    AttemptNumber = 123,
                    WasSuccessful = true,
                    FailureMessage = "Failed",
                    MetaData = "Meta"
                };

                await ctx.MessageDeliveryRecord.AddAsync(newMsgDelRec);
            }

            MessageHeader newMsgHead = 
                await ctx.MessageHeader.Where(x => x.BID == TestConstants.BID && x.ParticipantID == newMsgPartInfo.ID).FirstOrDefaultAsync();

            if (newMsgHead == null)
            {
                newMsgHead = new MessageHeader()
                {
                    ID = -newContact.ID,
                    BID = TestConstants.BID,
                    ModifiedDT = DateTime.UtcNow,
                    ReceivedDT = DateTime.UtcNow,
                    ParticipantID = newMsgPartInfo.ID,
                    IsRead = false,
                    IsDeleted = false,
                    IsExpired = false,
                    InSentFolder = false,
                    EmployeeID = (await ctx.EmployeeData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID)).ID,
                    BodyID = newMsgPartInfo.BodyID
                };

                await ctx.MessageHeader.AddAsync(newMsgHead);
            }

            OpportunityData newOppData =
                await ctx.OpportunityData.Where(x => x.BID == TestConstants.BID && x.ContactID == newContact.ID).FirstOrDefaultAsync();

            if (newOppData == null)
            {
                newOppData = new OpportunityData()
                {
                    ID = -newContact.ID,
                    BID = TestConstants.BID,
                    ContactID = newContact.ID,
                    CompanyID = newContact.CompanyContactLinks.FirstOrDefault().CompanyID,
                    Name = "ContactForceDeleteTest"
                };

                await ctx.OpportunityData.AddAsync(newOppData);
            }
            

            UserLink newUserLink =
                await ctx.UserLink.Where(x => x.BID == TestConstants.BID && x.ContactID == newContact.ID).FirstOrDefaultAsync();

            if (newUserLink == null)
            {
                newUserLink = new UserLink()
                {
                    ID = (short)(-newContact.ID),
                    BID = TestConstants.BID,
                    ContactID = newContact.ID,
                    UserName = "johndoe",
                    UserAccessType = UserAccessType.SupportManager
                };

                await ctx.UserLink.AddAsync(newUserLink);
            }

            await ctx.SaveChangesAsync();

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{newContact.ID}?force=true", HttpStatusCode.OK);

            Assert.IsNull(await ctx.UserLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ContactID == newContact.ID));
            Assert.IsNull(await ctx.OpportunityData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ContactID == newContact.ID));
            Assert.IsNull(await ctx.MessageHeader.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ParticipantID == newMsgPartInfo.ID));
            Assert.IsNull(await ctx.MessageDeliveryRecord.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ParticipantID == newMsgPartInfo.ID));
            Assert.IsNull(await ctx.MessageParticipantInfo.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ContactID == newContact.ID));
            Assert.IsNull(await ctx.ContactTagLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ContactID == newContact.ID));
            Assert.IsNull(await ctx.ContactLocator.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ParentID == newContact.ID));
            Assert.IsNull(await ctx.ContactCustomData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ID == newContact.ID));
            Assert.IsNull(await ctx.ContactData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ID == newContact.ID));
        }

        [TestMethod]
        public async Task TestCloneContactWithCopyFilesAndAdHocCompany()
        {
            var adHocContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}", JsonConvert.SerializeObject(GetNewAdHocContact()), HttpStatusCode.OK);
            Assert.IsNotNull(adHocContact);
            Assert.IsNotNull(adHocContact.CompanyContactLinks);
            Assert.IsTrue(adHocContact.CompanyContactLinks.FirstOrDefault(l => l.Company.IsAdHoc) != null);
            Assert.IsTrue(adHocContact.ContactLocators.Count() == 0 );

            var fooTextFileName = "non-addhoc.txt";
            var filePathFragment = $"{(int)ClassType.Contact}/{adHocContact.ID}/{fooTextFileName}";
            var payload = this.GetMultipartFormDataContent("foo", fooTextFileName);

            await AssertPostFileStatusCode(client, $"/api/dm/{Bucket.Documents}/{filePathFragment}", payload, HttpStatusCode.OK);

            // clone contact
            var cloned = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}/{adHocContact.ID}/clone", null, HttpStatusCode.OK);
            Assert.IsNotNull(cloned);
            Assert.IsNotNull(cloned.CompanyContactLinks);
            Assert.IsTrue(cloned.CompanyContactLinks.FirstOrDefault(l => l.Company.IsAdHoc) != null);
            Assert.AreNotEqual(cloned.CompanyContactLinks.FirstOrDefault().CompanyID, adHocContact.CompanyContactLinks.FirstOrDefault().CompanyID);

            // newContact file reference
            var dm = GetDocumentManager(adHocContact.BID, adHocContact.ID, ClassType.Contact, Web.BucketRequest.Documents);
            var createdDocuments = await dm.GetDocumentsAsync();

            // clone newContact file reference
            var cloneDm = GetDocumentManager(cloned.BID, cloned.ID, ClassType.Contact, Web.BucketRequest.Documents);
            var clonedDocuments = await cloneDm.GetDocumentsAsync();

            Assert.AreEqual(createdDocuments.Count, clonedDocuments.Count);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{cloned.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{adHocContact.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{cloned.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{adHocContact.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task TestCloneContactWithCopyFilesAndNonAdHocCompany()
        {
            var company = await GetNewCompanyData(-101);
            var nonAdHocContact = GetNewAdHocContact(company);

            nonAdHocContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}", JsonConvert.SerializeObject(nonAdHocContact), HttpStatusCode.OK);
            Assert.IsNotNull(nonAdHocContact);

            var fooTextFileName = "non-addhoc.txt";
            var filePathFragment = $"{(int)ClassType.Contact}/{nonAdHocContact.ID}/{fooTextFileName}";
            var payload = this.GetMultipartFormDataContent("foo", fooTextFileName);

            await AssertPostFileStatusCode(client, $"/api/dm/{Bucket.Documents}/{filePathFragment}", payload, HttpStatusCode.OK);

            // clone contact
            var cloned = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}/{nonAdHocContact.ID}/clone", null, HttpStatusCode.OK);
            Assert.IsNotNull(nonAdHocContact);

            // newContact file reference
            var dm = GetDocumentManager(nonAdHocContact.BID, nonAdHocContact.ID, ClassType.Contact, Web.BucketRequest.Documents);
            var createdDocuments = await dm.GetDocumentsAsync();

            // clone newContact file reference
            var cloneDm = GetDocumentManager(cloned.BID, cloned.ID, ClassType.Contact, Web.BucketRequest.Documents);
            var clonedDocuments = await cloneDm.GetDocumentsAsync();

            Assert.AreEqual(createdDocuments.Count, clonedDocuments.Count);
            Assert.AreEqual(createdDocuments[0].Name, clonedDocuments[0].Name);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{cloned.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{nonAdHocContact.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{cloned.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{nonAdHocContact.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task TestCloneNonAdHocContact()
        {
            try
            {
                var company = await GetNewCompanyData(-101);
                var nonAdHocContact = GetNewAdHocContact(company);

                using (var ctx = GetApiContext())
                {
                    //test: expect OK
                    nonAdHocContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}", JsonConvert.SerializeObject(nonAdHocContact), HttpStatusCode.OK);
                    Assert.IsNotNull(nonAdHocContact);

                    CustomFieldDefinition cfd = new CustomFieldDefinition()
                    {
                        ID = -99,
                        BID = 1,
                        AllowMultipleValues = false,
                        AppliesToClassTypeID = (int)ClassType.Contact,
                        DataType = DataType.String,
                        DisplayType = CustomFieldNumberDisplayType.General,
                        ElementType = AssemblyElementType.SingleLineText,
                        IsActive = true,
                        IsSystem = false,
                        Name = "Temp CF",
                        Label = "Temp CF",
                        Description = "Temp CF",
                    };
                    ctx.CustomFieldDefinition.Add(cfd);

                    await ctx.SaveChangesAsync();
                    var values = new CustomFieldValue[1];
                    values[0] = new CustomFieldValue() { ID = cfd.ID, V = "abs" };

                    await EndpointTests.AssertPutStatusCode(client, $"/api/cf/value/{ClassType.Contact.ID()}/{nonAdHocContact.ID}", JsonConvert.SerializeObject(values), HttpStatusCode.OK);

                    var lastName = nonAdHocContact.Last;
                    var cloned = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}/{nonAdHocContact.ID}/clone", null, HttpStatusCode.OK);
                    Assert.IsNotNull(cloned);
                    Assert.AreEqual($"{lastName} (Clone)", cloned.Last);
                    Assert.IsNotNull(cloned.CompanyContactLinks);
                    Assert.IsTrue(cloned.CompanyContactLinks.FirstOrDefault(l => l.Company.IsAdHoc) == null);
                    Assert.AreEqual(cloned.CompanyContactLinks.FirstOrDefault().CompanyID, nonAdHocContact.CompanyContactLinks.FirstOrDefault().CompanyID);

                    var clonedCustomField = await EndpointTests.AssertGetStatusCode<CustomFieldValue[]>(client, $"/api/cf/value/{ClassType.Contact.ID()}/{nonAdHocContact.ID}", HttpStatusCode.OK);
                    Assert.IsNotNull(clonedCustomField);
                    Assert.AreEqual(1, clonedCustomField.Count());
                    Assert.AreEqual(values[0].ID, clonedCustomField.First().ID);
                    Assert.AreEqual(values[0].V, clonedCustomField.First().V);

                    var cloned2 = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}/{nonAdHocContact.ID}/clone", null, HttpStatusCode.OK);
                    Assert.AreEqual($"{lastName} (Clone) (2)", cloned2.Last);
                    Assert.IsNotNull(cloned2.CompanyContactLinks);
                    Assert.IsTrue(cloned2.CompanyContactLinks.FirstOrDefault(l => l.Company.IsAdHoc) == null);
                    Assert.AreEqual(cloned2.CompanyContactLinks.FirstOrDefault().CompanyID, nonAdHocContact.CompanyContactLinks.FirstOrDefault().CompanyID);

                    var cloned3 = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}/{nonAdHocContact.ID}/clone", null, HttpStatusCode.OK);
                    Assert.AreEqual($"{lastName} (Clone) (3)", cloned3.Last);
                    Assert.IsNotNull(cloned3.CompanyContactLinks);
                    Assert.IsTrue(cloned3.CompanyContactLinks.FirstOrDefault(l => l.Company.IsAdHoc) == null);
                    Assert.AreEqual(cloned3.CompanyContactLinks.FirstOrDefault().CompanyID, nonAdHocContact.CompanyContactLinks.FirstOrDefault().CompanyID);

                    var cloneCloned = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}/{cloned.ID}/clone", null, HttpStatusCode.OK);
                    Assert.AreEqual($"{lastName} (Clone) (Clone)", cloneCloned.Last);
                    Assert.IsNotNull(cloneCloned.CompanyContactLinks);
                    Assert.IsTrue(cloneCloned.CompanyContactLinks.FirstOrDefault(l => l.Company.IsAdHoc) == null);
                    Assert.AreEqual(cloneCloned.CompanyContactLinks.FirstOrDefault().CompanyID, nonAdHocContact.CompanyContactLinks.FirstOrDefault().CompanyID);

                    var cloneCloned2 = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}/{cloned3.ID}/clone", null, HttpStatusCode.OK);
                    Assert.AreEqual($"{lastName} (Clone) (3) (Clone)", cloneCloned2.Last);
                    Assert.IsNotNull(cloneCloned2.CompanyContactLinks);
                    Assert.IsTrue(cloneCloned2.CompanyContactLinks.FirstOrDefault(l => l.Company.IsAdHoc) == null);
                    Assert.AreEqual(cloneCloned2.CompanyContactLinks.FirstOrDefault().CompanyID, nonAdHocContact.CompanyContactLinks.FirstOrDefault().CompanyID);

                    await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{cloned.ID}", HttpStatusCode.NoContent);
                    await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{cloned2.ID}", HttpStatusCode.NoContent);
                    await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{cloned3.ID}", HttpStatusCode.NoContent);
                    await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{cloneCloned.ID}", HttpStatusCode.NoContent);
                    await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{cloneCloned2.ID}", HttpStatusCode.NoContent);
                    await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{nonAdHocContact.ID}", HttpStatusCode.NoContent);
                    await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{cloned.ID}", HttpStatusCode.NotFound);
                    await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{cloned2.ID}", HttpStatusCode.NotFound);
                    await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{cloned3.ID}", HttpStatusCode.NotFound);
                    await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{cloneCloned.ID}", HttpStatusCode.NotFound);
                    await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{cloneCloned2.ID}", HttpStatusCode.NotFound);
                    await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{nonAdHocContact.ID}", HttpStatusCode.NotFound);
                }
            }
            finally
            {
                await Teardown();
            }

        }

        [TestMethod]
        public async Task TestCloneContactWithLocators()
        {
            var adHocContact = GetNewAdHocContact();
            adHocContact.ContactLocators = new List<ContactLocator>() {
                new ContactLocator()
                {
                    BID = TestConstants.BID,
                    LocatorType = 2,
                    Locator = "(801) 836-7117",
                    RawInput = "8018367117",
                    SortIndex = 1,
                    IsVerified = false,
                    IsValid = true
                }
            };

            adHocContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}", JsonConvert.SerializeObject(adHocContact), HttpStatusCode.OK);
            Assert.IsNotNull(adHocContact);
            Assert.AreEqual(1, adHocContact.ContactLocators.Count());

            var clonedContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}/{adHocContact.ID}/clone", JsonConvert.SerializeObject(""), HttpStatusCode.OK);
            Assert.IsNotNull(clonedContact);
            clonedContact = await EndpointTests.AssertGetStatusCode<ContactData>(client, $"{apiUrl}/{clonedContact.ID}", HttpStatusCode.OK);
            Assert.AreEqual(1, clonedContact.ContactLocators.Count());

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{clonedContact.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{adHocContact.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{clonedContact.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{adHocContact.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task TestCloneContactWithRegularCompany()
        {
            var company = await GetNewCompanyData(-101);
            var nonAdHocContact = GetNewAdHocContact(company);
            
            //test: expect OK
            nonAdHocContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}", JsonConvert.SerializeObject(nonAdHocContact), HttpStatusCode.OK);
            Assert.IsNotNull(nonAdHocContact);

            var clonedContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}/{nonAdHocContact.ID}/clone", JsonConvert.SerializeObject(""), HttpStatusCode.OK);
            Assert.IsNotNull(clonedContact);
            Assert.IsNotNull(clonedContact.CompanyContactLinks);
            Assert.AreEqual(1, clonedContact.CompanyContactLinks.Count);
            Assert.AreEqual(company.ID, clonedContact.CompanyContactLinks.FirstOrDefault().CompanyID);
            Assert.AreEqual(company.Name, clonedContact.CompanyContactLinks.FirstOrDefault().Company.Name);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{clonedContact.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{nonAdHocContact.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{clonedContact.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{nonAdHocContact.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task TestCloneContactWithAdHocCompany()
        {
            var adHocContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}", JsonConvert.SerializeObject(GetNewAdHocContact()), HttpStatusCode.OK);
            Assert.IsNotNull(adHocContact);
            Assert.IsNotNull(adHocContact.CompanyContactLinks);
            Assert.IsTrue(adHocContact.CompanyContactLinks.FirstOrDefault(l => l.Company.IsAdHoc) != null);

            var clonedContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}/{adHocContact.ID}/clone", JsonConvert.SerializeObject(""), HttpStatusCode.OK);
            Assert.IsNotNull(clonedContact);
            Assert.IsNotNull(clonedContact.CompanyContactLinks);
            Assert.IsTrue(clonedContact.CompanyContactLinks.FirstOrDefault(l => l.Company.IsAdHoc) != null);
            Assert.AreNotEqual(clonedContact.CompanyContactLinks.FirstOrDefault().CompanyID, adHocContact.CompanyContactLinks.FirstOrDefault().CompanyID);

            var createdCompany = await EndpointTests.AssertGetStatusCode<CompanyData>(client, $"/Api/Company/{clonedContact.CompanyContactLinks.FirstOrDefault().CompanyID}", HttpStatusCode.OK);
            Assert.IsNotNull(createdCompany);
            Assert.IsTrue(createdCompany.IsAdHoc);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{adHocContact.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{clonedContact.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{adHocContact.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode(client, $"Api/Company/{createdCompany.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task TestContactStatus()
        {
            var adHocContact = await EndpointTests.AssertPostStatusCode<ContactData>(client, $"{apiUrl}", JsonConvert.SerializeObject(GetNewAdHocContact()), HttpStatusCode.OK);
            Assert.IsNotNull(adHocContact);
            Assert.IsNotNull(adHocContact.CompanyContactLinks);
            var company = adHocContact.CompanyContactLinks.FirstOrDefault().Company;
            Assert.IsNotNull(company);

            var contactStatus = await EndpointTests.AssertGetStatusCode<ContactStatusResponse>(client, $"/api/contact/status/{adHocContact.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(contactStatus);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{adHocContact.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{adHocContact.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode(client, $"api/company/{company.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public void TestContactRoleBitwise()
        {
            var inactive = ContactRole.Inactive;
            Assert.AreEqual(ContactRole.Inactive, inactive);
            Assert.AreEqual(0, (int)inactive);

            var primaryOnly = ContactRole.Primary;
            Assert.AreEqual(ContactRole.Primary, primaryOnly);
            Assert.AreEqual(1, (int)primaryOnly);

            var billingOnly = ContactRole.Billing;
            Assert.AreEqual(ContactRole.Billing, billingOnly);
            Assert.AreEqual(2, (int)billingOnly);

            var billingAndPrimary = (ContactRole.Billing | ContactRole.Primary);
            Assert.AreEqual((ContactRole.Billing | ContactRole.Primary), billingAndPrimary);
            Assert.AreEqual(3, (int)billingAndPrimary);
            Assert.IsFalse(billingOnly == billingAndPrimary);
            Assert.IsTrue((billingOnly & billingAndPrimary) == ContactRole.Billing);
            Assert.IsTrue(ContactRole.Billing == (billingOnly & billingAndPrimary));
            Assert.IsTrue((primaryOnly & billingAndPrimary) == ContactRole.Primary);

            var ownerOnly = ContactRole.Owner;
            Assert.AreEqual(ContactRole.Owner, ownerOnly);
            Assert.AreEqual(4, (int)ownerOnly);

            var ownerAndPrimary = (ContactRole.Owner | ContactRole.Primary);
            Assert.AreEqual((ContactRole.Owner | ContactRole.Primary), ownerAndPrimary);
            Assert.AreEqual(5, (int)ownerAndPrimary);
            Assert.IsFalse(ownerOnly == ownerAndPrimary);
            Assert.IsTrue((primaryOnly & ownerAndPrimary) == ContactRole.Primary);
            Assert.IsTrue((ownerOnly & ownerAndPrimary) == ContactRole.Owner);

            var ownerAndBilling = (ContactRole.Owner | ContactRole.Billing);
            Assert.AreEqual((ContactRole.Owner | ContactRole.Billing), ownerAndBilling);
            Assert.AreEqual(6, (int)ownerAndBilling);
            Assert.IsFalse(ownerOnly == ownerAndBilling);
            Assert.IsTrue((billingOnly & ownerAndBilling) == ContactRole.Billing);
            Assert.IsTrue((ownerOnly & ownerAndBilling) == ContactRole.Owner);

            var ownerAndBillingAndPrimary = (ContactRole.Owner | ContactRole.Billing | ContactRole.Primary);
            Assert.AreEqual((ContactRole.Owner | ContactRole.Billing | ContactRole.Primary), ownerAndBillingAndPrimary);
            Assert.AreEqual(7, (int)ownerAndBillingAndPrimary);
            Assert.IsFalse(ownerOnly == ownerAndBillingAndPrimary);
            Assert.IsTrue((billingOnly & ownerAndBillingAndPrimary) == ContactRole.Billing);
            Assert.IsTrue((primaryOnly & ownerAndBillingAndPrimary) == ContactRole.Primary);
            Assert.IsTrue((billingAndPrimary & ownerAndBillingAndPrimary) == (ContactRole.Billing | ContactRole.Primary));
            Assert.IsTrue((ownerAndPrimary & ownerAndBillingAndPrimary) == (ContactRole.Owner | ContactRole.Primary));
            Assert.IsTrue((ownerAndBilling & ownerAndBillingAndPrimary) == (ContactRole.Owner | ContactRole.Billing));
            Assert.IsTrue((ownerOnly & ownerAndBillingAndPrimary) == ContactRole.Owner);
        }
    }
}
