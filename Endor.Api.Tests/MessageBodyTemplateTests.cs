﻿using Endor.Models;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Endor.Api.Web.Classes;
using System.Text.RegularExpressions;
using Endor.EF;

namespace Endor.Api.Tests
{
    /*
    [TestClass]
    public class MessageBodyTemplateTests : CommonControllerTestClass
    {
        #region Test Setup/Initialize/Cleanup

        public const string apiUrl = "/api/message/bodytemplate";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string newMsgBodyTemplate;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            newMsgBodyTemplate = DateTime.UtcNow + " MESSAGE BODY TEMPLATE";
        }

        [TestCleanup]
        public void Teardown()
        {
            DeleteTestRecord();
        }

        private void DeleteTestRecord()
        {
            Task.Run(async () =>
            {
                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                List<SimpleListItem<byte>> result = JsonConvert.DeserializeObject<List<SimpleListItem<byte>>>(responseString);
                if (result != null)
                {
                    SimpleListItem<byte> template = result.FirstOrDefault(x => x.DisplayName.Contains(newMsgBodyTemplate));
                    if (template != null)
                    {
                        await client.DeleteAsync($"{apiUrl}/{template.ID}");
                    }
                }
            }).Wait();
        }

        private MessageBodyTemplate GetTestTemplate()
        {
            ApiContext ctx = GetApiContext(1);
            EmployeeData emp = ctx.EmployeeData.FirstOrDefault(e => e.IsActive);
            ContactData contact = ctx.ContactData.FirstOrDefault(c => c.CompanyContactLinks.Count() > 0);
            CompanyContactLink ccl = ctx.CompanyContactLink.FirstOrDefault(cl => cl.ContactID == contact.ID );
            EmployeeTeam team = ctx.EmployeeTeam.FirstOrDefault(t => t.IsActive);
            
            return new MessageBodyTemplate()
            {
                BID = 1,
                IsActive = true,
                AppliesToClassTypeID = 5000,
                MessageTemplateType = 0,
                ChannelType = MessageChannelType.Email,
                LocationID = emp.LocationID,
                CompanyID = ccl.CompanyID,
                EmployeeID = emp.ID,
                Name = this.newMsgBodyTemplate,
                Description = $"{this.newMsgBodyTemplate} with MergeFields",
                Subject = "Test Mail {{Order.OrderNumber}}{{Order.ItemNumber}}",
                Body = "This is just a test {{Order.ordernumber}}{{Order.TransactionType}}",
                AttachedFileCount = 0,
                AttachedFileNames = "none",
                SizeInKB = 0,
                SortIndex = 1,
                Participants = new List<MessageParticipantInfo> 
                {
                    new MessageParticipantInfo 
                    {
                        ParticipantRoleType = MessageParticipantRoleType.To,
                        Channel = MessageChannelType.Email,
                        UserName = "TEST TO",
                        IsMergeField = false,
                        EmployeeID = emp.ID,
                        TeamID = team.ID,
                        ContactID = contact.ID
                    }
                }
            };
        }

        private MessageBodyTemplate GetNonValidTestTemplate()
        {
            //Non Valid Ids for EmployeeID, ContactID, TeamID
            return new MessageBodyTemplate()
            {
                IsActive = true,
                AppliesToClassTypeID = ClassType.MessageBodyTemplate.ID(),
                MessageTemplateType = 0,
                ChannelType = MessageChannelType.Email,
                LocationID = 0,
                CompanyID = -2147483648,
                EmployeeID = -32768,
                Name = this.newMsgBodyTemplate,
                Description = $"{this.newMsgBodyTemplate} with MergeFields",
                Subject = "Test Mail {{Order.OrderNumber}}{{Order.ItemNumber}}",
                Body = "This is just a test {{Order.ordernumber}}{{Order.TransactionType}}",
                AttachedFileCount = 0,
                AttachedFileNames = "none",
                SizeInKB = 0,
                SortIndex = 1,
                Participants = new List<MessageParticipantInfo>
                {
                    new MessageParticipantInfo
                    {
                        ParticipantRoleType = MessageParticipantRoleType.To,
                        Channel = MessageChannelType.Email,
                        UserName = "TEST TO",
                        IsMergeField = false,
                        EmployeeID = -32768,
                        TeamID = -2147483648,
                        ContactID = -2147483648
                    }
                }
            };
        }
        #endregion

        [TestMethod]
        public async Task TestMessageBodyTemplateSerialize()
        {
            //Case 1. Create (Test ParticipantJSON is inserted in the database)
            var newModel = GetTestTemplate();
            //Set IsMergeField=false
            //newModel.Participants[0].IsMergeField = false;
            var response = await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);
            Assert.IsNotNull(response);
            
            string responseContent = await response.Content.ReadAsStringAsync();

            MessageBodyTemplate resultObject = JsonConvert.DeserializeObject<MessageBodyTemplate>(responseContent);
            ApiContext ctx = GetApiContext(1);
            var dbModel = ctx.MessageBodyTemplate.FirstOrDefault(e => e.ID == resultObject.ID);
            Assert.IsNotNull(dbModel.ParticipantInfoJSON);
            Assert.IsNull(dbModel.Participants);

            //Case 2. GET (Test participants returned, but not ParticipantsInfoJSON)
            var messageTemplate = await EndpointTests.AssertGetStatusCode<MessageBodyTemplate>(client, $"{apiUrl}/{resultObject.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(messageTemplate.Participants.Count() > 0);
            Assert.IsNull(messageTemplate.ParticipantInfoJSON);
        }

        [TestMethod]
        public async Task TestMessageBodyTemplateValidation()
        {
            string invalidExpected = "Contact for ID `-2147483648` not found. Employee for ID `-32768` not found. Team for ID `-2147483648` not found. ";
            //Case 1. Create
            var newModel = GetNonValidTestTemplate();
            var response = await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.BadRequest);
            Assert.IsNotNull(response);
            var invalidMessage = await response.Content.ReadAsStringAsync();
            Assert.IsNotNull(invalidMessage);
            Assert.AreEqual(invalidExpected, invalidMessage);

            //Case 2. Modify (PUT) 
            newModel = GetTestTemplate();
            var newMsgBodyTemplate = await EndpointTests.AssertPostStatusCode<MessageBodyTemplate>(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);
            var getTemplate = await EndpointTests.AssertGetStatusCode<MessageBodyTemplate>(client, $"{apiUrl}/{newMsgBodyTemplate.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(getTemplate);
            getTemplate.EmployeeID = 99;

            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{getTemplate.ID}", JsonConvert.SerializeObject(getTemplate), HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public async Task TestMessageBodyTemplateCRUDEndpoints()
        {
            
            var newModel = GetTestTemplate();
            var newMsgBodyTemplate = await EndpointTests.AssertPostStatusCode<MessageBodyTemplate>(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);

            var msgBodyTemplate = await EndpointTests.AssertGetStatusCode<MessageBodyTemplate>(client, $"{apiUrl}/{newMsgBodyTemplate.ID}", HttpStatusCode.OK);
            Assert.IsTrue(msgBodyTemplate.Participants.Count > 0);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}?IsActive=false", HttpStatusCode.OK);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/SimpleList?IsActive=true", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/SimpleList?IsActive=false", HttpStatusCode.OK);

            var getTemplate = await EndpointTests.AssertGetStatusCode<MessageBodyTemplate>(client, $"{apiUrl}/{newMsgBodyTemplate.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(getTemplate);

            getTemplate.IsActive = false;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{getTemplate.ID}", JsonConvert.SerializeObject(getTemplate), HttpStatusCode.OK);

            getTemplate = await EndpointTests.AssertGetStatusCode<MessageBodyTemplate>(client, $"{apiUrl}/{getTemplate.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(getTemplate);
            Assert.IsFalse(getTemplate.IsActive);

            var cloneTemplate = await EndpointTests.AssertPostStatusCode<MessageBodyTemplate>(client, $"{apiUrl}/{getTemplate.ID}/Clone", null, HttpStatusCode.OK);
            Assert.IsNotNull(cloneTemplate);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{cloneTemplate.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{getTemplate.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestMessageBodyTemplateSetActiveInactive()
        {
            var adHocTemplate = GetTestTemplate();

            //test: expect OK
            adHocTemplate = await EndpointTests.AssertPostStatusCode<MessageBodyTemplate>(client, $"{apiUrl}", JsonConvert.SerializeObject(adHocTemplate), HttpStatusCode.OK);
            Assert.IsNotNull(adHocTemplate);

            // set inactive
            var statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{adHocTemplate.ID}/Action/SetInactive", HttpStatusCode.OK);
            Assert.IsNotNull(statusResp);
            Assert.IsTrue(statusResp.Success);

            var adHocTemplateTest = await EndpointTests.AssertGetStatusCode<MessageBodyTemplate>(client, $"{apiUrl}/{adHocTemplate.ID}", HttpStatusCode.OK);
            Assert.IsFalse(adHocTemplateTest.IsActive);

            // set active
            statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{adHocTemplate.ID}/Action/SetActive", HttpStatusCode.OK);
            Assert.IsNotNull(statusResp);
            Assert.IsTrue(statusResp.Success);

            // confirm adhoc is now active again
            adHocTemplateTest = await EndpointTests.AssertGetStatusCode<MessageBodyTemplate>(client, $"{apiUrl}/{adHocTemplate.ID}", HttpStatusCode.OK);
            Assert.IsTrue(adHocTemplateTest.IsActive);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{adHocTemplate.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestMessageBodyTemplateCanDelete()
        {
            var adHocTemplate = GetTestTemplate();

            adHocTemplate = await EndpointTests.AssertPostStatusCode<MessageBodyTemplate>(client, $"{apiUrl}", JsonConvert.SerializeObject(adHocTemplate), HttpStatusCode.OK);
            Assert.IsNotNull(adHocTemplate);

            var statusResp = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{adHocTemplate.ID}/Action/CanDelete", HttpStatusCode.OK);
            Assert.IsTrue(statusResp.Success);
            Assert.IsTrue(statusResp.Value.Value);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{adHocTemplate.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestMessageBodyTemplateMoveBeforeAfter()
        {
            var adHocTemplateTarget = GetTestTemplate();

            adHocTemplateTarget = await EndpointTests.AssertPostStatusCode<MessageBodyTemplate>(client, $"{apiUrl}", JsonConvert.SerializeObject(adHocTemplateTarget), HttpStatusCode.OK);
            Assert.IsNotNull(adHocTemplateTarget);

            var cloneTemplateTarget = await EndpointTests.AssertPostStatusCode<MessageBodyTemplate>(client, $"{apiUrl}/{adHocTemplateTarget.ID}/Clone", null, HttpStatusCode.OK);
            Assert.IsNotNull(cloneTemplateTarget);

            // Test Move Before
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{cloneTemplateTarget.ID}/Action/MoveBefore/{adHocTemplateTarget.ID}", HttpStatusCode.OK);
            
            adHocTemplateTarget = await EndpointTests.AssertGetStatusCode<MessageBodyTemplate>(client, $"{apiUrl}/{adHocTemplateTarget.ID}", HttpStatusCode.OK);
            cloneTemplateTarget = await EndpointTests.AssertGetStatusCode<MessageBodyTemplate>(client, $"{apiUrl}/{cloneTemplateTarget.ID}", HttpStatusCode.OK);
            Assert.AreEqual(cloneTemplateTarget.SortIndex, adHocTemplateTarget.SortIndex - 1); // EX. (1, 2-1) = true

            // Test Move After
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{cloneTemplateTarget.ID}/Action/MoveAfter/{adHocTemplateTarget.ID}", HttpStatusCode.OK);
            
            adHocTemplateTarget = await EndpointTests.AssertGetStatusCode<MessageBodyTemplate>(client, $"{apiUrl}/{adHocTemplateTarget.ID}", HttpStatusCode.OK);
            cloneTemplateTarget = await EndpointTests.AssertGetStatusCode<MessageBodyTemplate>(client, $"{apiUrl}/{cloneTemplateTarget.ID}", HttpStatusCode.OK);
            Assert.AreEqual(cloneTemplateTarget.SortIndex, adHocTemplateTarget.SortIndex + 1); // EX. (2, 1+1) = true

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{adHocTemplateTarget.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{cloneTemplateTarget.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestRetrieveMergeFieldList()
        {
            var adHocTemplate = GetTestTemplate();
            this.SetMergeFieldList(adHocTemplate);
            Assert.IsNotNull(adHocTemplate.MergeFieldList);

            adHocTemplate = await EndpointTests.AssertPostStatusCode<MessageBodyTemplate>(client, $"{apiUrl}", JsonConvert.SerializeObject(adHocTemplate), HttpStatusCode.OK);
            Assert.IsNotNull(adHocTemplate);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{adHocTemplate.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestDuplicateMergeFieldList()
        {
            var adHocTemplate = GetTestTemplate();
            this.SetMergeFieldList(adHocTemplate);

            var mergeFieldList = adHocTemplate.MergeFieldList.Split(",");
            Assert.IsTrue(mergeFieldList.Length == mergeFieldList.Distinct().Count()); // Check MergeFieldList has no duplicate

            adHocTemplate = await EndpointTests.AssertPostStatusCode<MessageBodyTemplate>(client, $"{apiUrl}", JsonConvert.SerializeObject(adHocTemplate), HttpStatusCode.OK);
            Assert.IsNotNull(adHocTemplate);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{adHocTemplate.ID}", HttpStatusCode.NoContent);
        }

        private void SetMergeFieldList(MessageBodyTemplate messageTemplate)
        {
            string input = messageTemplate.Subject + messageTemplate.Body + messageTemplate.AttachedFileNames;

            Dictionary<string, string> mf = new Dictionary<string, string>(StringComparer.InvariantCulture);
            var regex = new Regex("{{(.*?)}}");
            var matches = regex.Matches(input);
            foreach (Match match in matches)
            {
                var val = match.Groups[1].Value;
                if (!mf.ContainsKey(val.ToUpperInvariant()))
                    mf.Add(val.ToUpperInvariant(), val);
            }
            messageTemplate.MergeFieldList = String.Join(",", mf.Select(x => x.Value).ToArray());
        }
    }*/
}
