﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class PartConsumptionMethodEndpointTest
    {
        public const string apiUrl = "/api/partconsumptionmethod";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task TestGetAllPartConsumptionMethodsTest()
        {
            var partConsumptionMethods = await EndpointTests.AssertGetStatusCode<List<EnumMaterialConsumptionMethod>>(client, apiUrl, HttpStatusCode.OK);
            Assert.IsNotNull(partConsumptionMethods);
            Assert.AreEqual(Enum.GetValues(typeof(MaterialConsumptionMethod)).Length, partConsumptionMethods.Count);
        }
    }
}
