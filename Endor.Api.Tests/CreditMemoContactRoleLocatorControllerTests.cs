﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Endor.Api.Web.Controllers;
using Endor.Api.Web.Services;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Endor.Api.Tests
{
    [TestClass]
    public class CreditMemoContactRoleLocatorControllerTests : CommonControllerTestClass
    {
        private readonly HttpClient client = EndpointTests.GetHttpClient();
        private readonly string baseEndpoint = $"api/CreditMemoContactRoleLocator";

        [TestInitialize]
        public override void Init()
        {
            base.Init();
        }

        [TestMethod]
        public async Task TestCrudOperations()
        {
            int temporaryId = -9999;
            int createdLocatorId = -1;

            //Inserted temporary data
            CreditMemoData orderData;
            ContactData contactData;
            OrderContactRole orderContactRole;
            OrderContactRoleLocator orderContactRoleLocator;

            //Loaded from existing database records (not inserted)
            CompanyData companyData;
            LocationData locationData;
            TaxGroup taxGroup;

            try
            {
                //Delete any stray temporary data from previous test run
                await CleanupData(temporaryId, createdLocatorId);

                using (var ctx = GetApiContext())
                {

                    //Load existing data from database - entries required for test to pass
                    companyData = await ctx.CompanyData.FirstOrDefaultAsync();
                    locationData = await ctx.LocationData.FirstOrDefaultAsync();
                    taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync();

                    //Temporary order data
                    orderData = new CreditMemoData()
                    {
                        BID = companyData.BID,
                        ID = temporaryId,
                        CompanyID = companyData.ID,
                        PickupLocationID = locationData.ID,
                        ProductionLocationID = locationData.ID,
                        LocationID = locationData.ID,
                        TaxGroupID = taxGroup.ID,
                        ClassTypeID = (int)ClassType.Order,
                        ModifiedDT = DateTime.UtcNow,
                        TransactionType = (byte)OrderTransactionType.Order,
                        OrderStatusID = OrderOrderStatus.OrderPreWIP,
                        OrderStatusStartDT = DateTime.UtcNow,
                        Number = 1000,
                        FormattedNumber = "INV-1000",
                        PriceTaxRate = 0.01m,
                        PaymentPaid = 0m,
                    };

                    ctx.CreditMemoData.Add(orderData);
                    await ctx.SaveChangesAsync();

                    //Temporary contact data
                    contactData = new ContactData()
                    {
                        BID = 1,
                        ID = temporaryId,
                        First = "First Name",
                        Last = "Last Name"
                    };

                    ctx.ContactData.Add(contactData);
                    await ctx.SaveChangesAsync();

                    //Temporary order contact role
                    orderContactRole = new OrderContactRole()
                    {
                        BID = 1,
                        ID = temporaryId,
                        ContactID = contactData.ID,
                        RoleType = OrderContactRoleType.Billing,
                        OrderID = orderData.ID,
                    };

                    ctx.OrderContactRole.Add(orderContactRole);
                    await ctx.SaveChangesAsync();

                    //Temporary order contact role locator
                    orderContactRoleLocator = new OrderContactRoleLocator()
                    {
                        BID = 1,
                        ID = temporaryId,
                        ParentID = orderContactRole.ID,
                        LocatorType = (byte)LocatorType.Email,
                        RawInput = "mytest@emaildomain.com",
                        Locator = "mytest@emaildomain.com",
                        SortIndex = 0,
                        IsValid = true,
                        IsVerified = true,
                        HasImage = false,
                    };

                    //Test Create Endpoint
                    orderContactRoleLocator = await HttpAssert<OrderContactRoleLocator>(this.baseEndpoint, orderContactRoleLocator, HttpMethod.Post, HttpStatusCode.OK);

                    Assert.IsNotNull(orderContactRoleLocator);

                    createdLocatorId = orderContactRoleLocator.ID;

                    //Test Get Endpoint
                    orderContactRoleLocator = await HttpAssert<OrderContactRoleLocator>($"{this.baseEndpoint}/{createdLocatorId}", null, HttpMethod.Get, HttpStatusCode.OK);

                    Assert.IsNotNull(orderContactRoleLocator);

                    //Test Get Locators By ParentId Endpoint
                    OrderContactRoleLocator[] getByParentIdResult = await HttpAssert<OrderContactRoleLocator[]>($"{this.baseEndpoint}/parent/{orderContactRole.ID}", orderContactRoleLocator, HttpMethod.Get, HttpStatusCode.OK);


                    Assert.IsNotNull(getByParentIdResult);

                    Assert.IsTrue(getByParentIdResult.Length > 0);


                    //Modify locator
                    string newLocatorValue = "updatedTestEmail@emaildomain.com";

                    orderContactRoleLocator.Locator = newLocatorValue;
                    orderContactRoleLocator.RawInput = newLocatorValue;

                    //Test Put Endpoint
                    orderContactRoleLocator = await HttpAssert<OrderContactRoleLocator>($"{this.baseEndpoint}/{createdLocatorId}", orderContactRoleLocator, HttpMethod.Put, HttpStatusCode.OK);

                    Assert.IsNotNull(orderContactRoleLocator);
                    Assert.AreEqual(newLocatorValue, orderContactRoleLocator.Locator);

                    //Test Delete Endpoint
                    IActionResult deleteResult = await HttpAssert<NoContentResult>($"{this.baseEndpoint}/{createdLocatorId}", orderContactRoleLocator, HttpMethod.Delete, HttpStatusCode.NoContent);

                    Assert.IsInstanceOfType(deleteResult, typeof(NoContentResult));
                }
            }
            finally
            {
                using (var ctx = GetApiContext())
                {
                    await CleanupData(temporaryId, createdLocatorId);
                }
            }
        }

        private async Task<T> HttpAssert<T>(string endpoint, OrderContactRoleLocator locator, HttpMethod method, HttpStatusCode expectedResponseCode)
        {
            T result = default(T);
            string resultAsString = string.Empty;

            if (method == HttpMethod.Get)
            {
                resultAsString = JsonConvert.SerializeObject(await EndpointTests.AssertGetStatusCode<T>
                    (this.client, endpoint, expectedResponseCode));
            }
            else if (method == HttpMethod.Put)
            {
                resultAsString = JsonConvert.SerializeObject(await EndpointTests.AssertPutStatusCode<T>
                    (this.client, endpoint, JsonConvert.SerializeObject(locator), expectedResponseCode));
            }
            else if (method == HttpMethod.Post)
            {
                resultAsString = JsonConvert.SerializeObject(await EndpointTests.AssertPostStatusCode<T>
                (this.client, endpoint, JsonConvert.SerializeObject(locator), expectedResponseCode));
            }
            else if (method == HttpMethod.Delete)
            {
                resultAsString = JsonConvert.SerializeObject(await EndpointTests.AssertDeleteStatusCode<T>
                    (this.client, endpoint, expectedResponseCode));
            }

            result = JsonConvert.DeserializeObject<T>(resultAsString);

            return result;
        }

        private async Task CleanupData(int temporaryId, int createdLocatorId)
        {
            using (var ctx = GetApiContext())
            {
                Expression<Func<ContactData, bool>> contactDataLookupExpression = x => x.BID == 1 && x.ID == temporaryId;
                Expression<Func<OrderContactRole, bool>> orderContactRoleLookupExpression = x => x.BID == 1 && x.ID == temporaryId;
                Expression<Func<OrderContactRoleLocator, bool>> orderContactRoleLocatorLookupExpression = x => x.BID == 1 && x.ID == temporaryId;
                Expression<Func<TransactionHeaderData, bool>> orderLookupExpression = x => x.BID == 1 && x.ID == temporaryId;

                TransactionHeaderData orderData = await ctx.TransactionHeaderData.FirstOrDefaultAsync(orderLookupExpression);
                OrderContactRole orderContactRole = (OrderContactRole)await ctx.OrderContactRole.FirstOrDefaultAsync(orderContactRoleLookupExpression);
                ContactData contactData = (ContactData)await ctx.ContactData.FirstOrDefaultAsync(contactDataLookupExpression);
                OrderContactRoleLocator orderContactRoleLocator = (OrderContactRoleLocator)await ctx.OrderContactRoleLocator.FirstOrDefaultAsync(orderContactRoleLocatorLookupExpression);
                OrderContactRoleLocator createdOrderContactRoleLocator = await ctx.OrderContactRoleLocator.FirstOrDefaultAsync(x => x.BID == 1 && x.ID == createdLocatorId) as OrderContactRoleLocator;

                if (orderContactRoleLocator != null)
                {
                    ctx.OrderContactRoleLocator.Remove(orderContactRoleLocator);
                    await ctx.SaveChangesAsync();
                }

                if (createdOrderContactRoleLocator != null)
                {
                    ctx.OrderContactRoleLocator.Remove(createdOrderContactRoleLocator);
                    await ctx.SaveChangesAsync();
                }

                if (orderContactRole != null)
                {
                    ctx.OrderContactRole.Remove(orderContactRole);
                    await ctx.SaveChangesAsync();
                }

                if (contactData != null)
                {
                    ctx.ContactData.Remove(contactData);
                    await ctx.SaveChangesAsync();
                }

                if (orderData != null)
                {
                    ctx.TransactionHeaderData.Remove(orderData);
                    await ctx.SaveChangesAsync();
                }
            }
        }
    }
}
