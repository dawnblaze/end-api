﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Pricing;

namespace Endor.Api.Tests
{
    [TestClass]
    public class FlatListEndpointTests
    {
        public const string apiUrl = "/api/flatlist";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        [TestMethod]
        public async Task TestFlatListCRUDOperations()
        {
            int invalidID = -1;

            #region CREATE
            FlatListItem testItem = Utils.GetFlatListItem();
            FlatListItem createdItem = await EndpointTests.AssertPostStatusCode<FlatListItem>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testItem), HttpStatusCode.OK);

            // Test duplicate name validation
            await EndpointTests.AssertPostStatusCode<FlatListItem>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testItem), HttpStatusCode.BadRequest);

            // Test new item is sorted to the top
            FlatListItem sortTestItem = Utils.GetFlatListItem();
            FlatListItem createdSortItem = await EndpointTests.AssertPostStatusCode<FlatListItem>(client, $"{apiUrl}/", JsonConvert.SerializeObject(sortTestItem), HttpStatusCode.OK);
            Assert.AreEqual(createdItem.SortIndex + 1, createdSortItem.SortIndex);

            // Test insert at specified SortIndex
            sortTestItem = Utils.GetFlatListItem();
            sortTestItem.SortIndex = (short)(createdSortItem.SortIndex - 1);
            // replicate the sorting
            var newList = await EndpointTests.AssertGetStatusCode<List<FlatListItem>>(client, $"{apiUrl}?FlatListType=1", HttpStatusCode.OK);
            newList.Add(sortTestItem);
            newList = newList.OrderBy(i => i.Name).ToList();
            for (var i = 0; i < newList.Count; i++)
            {
                newList[i].SortIndex = (short)i;
            }

            createdSortItem = await EndpointTests.AssertPostStatusCode<FlatListItem>(client, $"{apiUrl}/", JsonConvert.SerializeObject(sortTestItem), HttpStatusCode.OK);
            var updatedItems = await EndpointTests.AssertGetStatusCode<List<FlatListItem>>(client, $"{apiUrl}?FlatListType=1", HttpStatusCode.OK);
            FlatListItem validItem;

            // verify indeces are sorted correctly
            foreach (var updatedItem in updatedItems)
            {
                validItem = newList.Where(i => i.Name == updatedItem.Name).FirstOrDefault();
                Assert.IsNotNull(validItem);
                Assert.AreEqual(validItem.SortIndex, updatedItem.SortIndex);
            }
            #endregion


            #region RETRIEVE
            // Test Get all should return badrequest because flatlisttype must be specified
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}", HttpStatusCode.BadRequest);
            
            // Test Get all where IsActive=true
            FlatListItem[] retrievedItems = await EndpointTests.AssertGetStatusCode<FlatListItem[]>(client, $"{apiUrl}?IsActive=true&FlatListType=1", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedItems);

            // Test Get all where IsActive=false
            retrievedItems = await EndpointTests.AssertGetStatusCode<FlatListItem[]>(client, $"{apiUrl}?IsActive=false&FlatListType=1", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedItems);

            // Test Get all where FlatListType=1
            retrievedItems = await EndpointTests.AssertGetStatusCode<FlatListItem[]>(client, $"{apiUrl}?FlatListType=1", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedItems);
            // All items should have ListType=1
            Assert.IsTrue(retrievedItems.All(t => t.FlatListType == FlatListType.TaxExemptReasons));

            // Test Get all where FlatListType=2
            retrievedItems = await EndpointTests.AssertGetStatusCode<FlatListItem[]>(client, $"{apiUrl}?FlatListType=2", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedItems);
            Assert.IsTrue(retrievedItems.All(t => t.FlatListType == FlatListType.EstimateCancelledReasons));

            // Test Get all where FlatListType=3
            retrievedItems = await EndpointTests.AssertGetStatusCode<FlatListItem[]>(client, $"{apiUrl}?FlatListType=3", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedItems);
            Assert.IsTrue(retrievedItems.All(t => t.FlatListType == FlatListType.OrderVoidedReasons));

            // Test Get By ID not found
            await EndpointTests.AssertGetStatusCode<FlatListItem>(client, $"{apiUrl}/{invalidID}", HttpStatusCode.NotFound);

            // Test Get By ID
            FlatListItem retrievedItem = await EndpointTests.AssertGetStatusCode<FlatListItem>(client, $"{apiUrl}/{createdItem.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedItem);
            
            #endregion


            #region UPDATE
            // Test updating not found item
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{invalidID}", HttpStatusCode.NotFound);

            // Test updating valid item
            createdItem.Name = "TEST NAME";
            var testPut = await EndpointTests.AssertPutStatusCode<FlatListItem>(client, $"{apiUrl}/{createdItem.ID}", JsonConvert.SerializeObject(createdItem), HttpStatusCode.OK);
            Assert.IsNotNull(testPut);
            Assert.AreEqual(createdItem.Name, testPut.Name);
            #endregion


            #region DELETE
            // Deleting an item marks it adhoc
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdItem.ID}", HttpStatusCode.NoContent);
            // It should not show up in a getall
            retrievedItems = await EndpointTests.AssertGetStatusCode<FlatListItem[]>(client, $"{apiUrl}?FlatListType=1", HttpStatusCode.OK);
            Assert.IsFalse(retrievedItems.Any(t => t.ID == createdItem.ID));
            // But it should show up in a get by ID
            retrievedItem = await EndpointTests.AssertGetStatusCode<FlatListItem>(client, $"{apiUrl}/{createdItem.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedItem);

            //cleanup test
            await EndpointTests.AssertPostStatusCodeEither(client, $"{apiUrl}/{testPut.ID}/action/forcedelete", JsonConvert.SerializeObject(testItem), HttpStatusCode.NoContent, HttpStatusCode.OK);
            foreach (var item in retrievedItems)
            {
                await EndpointTests.AssertPostStatusCodeEither(client, $"{apiUrl}/{item.ID}/action/forcedelete", JsonConvert.SerializeObject(testItem), HttpStatusCode.NoContent, HttpStatusCode.OK);
            }
            #endregion
        }

        [TestMethod]
        public async Task TestFlatListActionEndpoints()
        {
            // Pre-test cleanup
            string[] testNames = {
                "TestFlatListActionEndpointsAlice",
                "TestFlatListActionEndpointsBob",
                "TestFlatListActionEndpointsCharlie",
                "TestFlatListActionEndpointsDavid",
                "TestFlatListActionEndpointsEdward",
                "TestFlatListActionEndpointsFrank",
                "TestFlatListActionEndpointsGordon"
            };
            short startSortIndex = 0;
            var flatListItems= (await EndpointTests.AssertGetStatusCode<FlatListItem[]>(client, $"{apiUrl}?FlatListType=1", HttpStatusCode.OK)).ToList();
            foreach (var item in flatListItems)
            {
                if (testNames.Contains(item.Name))
                {
                    await client.PostAsync($"{apiUrl}/{item.ID}/action/forcedelete", null);
                }
            }

            flatListItems = (await EndpointTests.AssertGetStatusCode<FlatListItem[]>(client, $"{apiUrl}?FlatListType=1", HttpStatusCode.OK)).ToList();
            if (flatListItems.LastOrDefault() != null)
            {
                startSortIndex = flatListItems.LastOrDefault().SortIndex??0;
                startSortIndex += 1;
            }
            


            #region Create Flat List
            FlatListItem testItem = Utils.GetFlatListItem();
            testItem.IsActive = true;
            testItem.IsAdHoc = false;
            testItem.IsSystem = false;

            testItem.Name = "TestFlatListActionEndpointsAlice";
            testItem.SortIndex = startSortIndex;
            var alice = await EndpointTests.AssertPostStatusCode<FlatListItem>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testItem), HttpStatusCode.OK);

            testItem.Name = "TestFlatListActionEndpointsBob";
            testItem.SortIndex += 1;
            var bob = await EndpointTests.AssertPostStatusCode<FlatListItem>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testItem), HttpStatusCode.OK);

            testItem.Name = "TestFlatListActionEndpointsCharlie";
            testItem.SortIndex += 1;
            var charlie = await EndpointTests.AssertPostStatusCode<FlatListItem>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testItem), HttpStatusCode.OK);

            testItem.Name = "TestFlatListActionEndpointsDavid";
            testItem.SortIndex += 1;
            var david = await EndpointTests.AssertPostStatusCode<FlatListItem>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testItem), HttpStatusCode.OK);

            testItem.Name = "TestFlatListActionEndpointsEdward";
            testItem.SortIndex += 1;
            var edward = await EndpointTests.AssertPostStatusCode<FlatListItem>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testItem), HttpStatusCode.OK);

            testItem.Name = "TestFlatListActionEndpointsFrank";
            testItem.SortIndex += 1;
            var frank = await EndpointTests.AssertPostStatusCode<FlatListItem>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testItem), HttpStatusCode.OK);

            testItem.Name = "TestFlatListActionEndpointsGordon";
            testItem.SortIndex += 1;
            var gordon = await EndpointTests.AssertPostStatusCode<FlatListItem>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testItem), HttpStatusCode.OK);
            #endregion

            #region MoveBefore test
            flatListItems = (await EndpointTests.AssertGetStatusCode<FlatListItem[]>(client, $"{apiUrl}?FlatListType=1", HttpStatusCode.OK)).ToList();

            //Assign the database assigned sortindex back...
            alice.SortIndex = flatListItems.First(t => t.ID == alice.ID).SortIndex;
            bob.SortIndex = flatListItems.First(t => t.ID == bob.ID).SortIndex;
            charlie.SortIndex = flatListItems.First(t => t.ID == charlie.ID).SortIndex;
            david.SortIndex = flatListItems.First(t => t.ID == david.ID).SortIndex;
            edward.SortIndex = flatListItems.First(t => t.ID == edward.ID).SortIndex;
            frank.SortIndex = flatListItems.First(t => t.ID == frank.ID).SortIndex;
            gordon.SortIndex = flatListItems.First(t => t.ID == gordon.ID).SortIndex;

            //a,b,c,d,e,f,g
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{bob.ID}/action/movebefore/{frank.ID}", HttpStatusCode.OK);
            //a,c,d,e,b,f,g
            var adjustedFlatListItems = (await EndpointTests.AssertGetStatusCode<FlatListItem[]>(client, $"{apiUrl}?FlatListType=1", HttpStatusCode.OK)).ToList();

            Assert.AreEqual<short?>((short)(startSortIndex + 0), adjustedFlatListItems.First(t => t.ID == alice.ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 1), adjustedFlatListItems.First(t => t.ID == charlie.ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 2), adjustedFlatListItems.First(t => t.ID == david.ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 3), adjustedFlatListItems.First(t => t.ID == edward.ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 4), adjustedFlatListItems.First(t => t.ID == bob.ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 5), adjustedFlatListItems.First(t => t.ID == frank.ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 6), adjustedFlatListItems.First(t => t.ID == gordon.ID).SortIndex);

            #endregion

            #region MoveAfter test
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{charlie.ID}/action/moveafter/{frank.ID}", HttpStatusCode.OK);
            //a,d,e,b,f,c,g
            adjustedFlatListItems = (await EndpointTests.AssertGetStatusCode<FlatListItem[]>(client, $"{apiUrl}?FlatListType=1", HttpStatusCode.OK)).ToList();

            Assert.AreEqual<short?>((short)(startSortIndex + 0), adjustedFlatListItems.First(t => t.ID == alice.ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 1), adjustedFlatListItems.First(t => t.ID == david.ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 2), adjustedFlatListItems.First(t => t.ID == edward.ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 3), adjustedFlatListItems.First(t => t.ID == bob.ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 4), adjustedFlatListItems.First(t => t.ID == frank.ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 5), adjustedFlatListItems.First(t => t.ID == charlie.ID).SortIndex);
            Assert.AreEqual<short?>((short)(startSortIndex + 6), adjustedFlatListItems.First(t => t.ID == gordon.ID).SortIndex);

            #endregion

            #region SetInactive test
            var baseItem = await EndpointTests.AssertGetStatusCode<FlatListItem>(client, $"{apiUrl}/{alice.ID}", HttpStatusCode.OK);
            Assert.IsTrue(baseItem.IsActive);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{alice.ID}/action/setinactive", HttpStatusCode.OK);
            var inactiveItem = await EndpointTests.AssertGetStatusCode<FlatListItem>(client, $"{apiUrl}/{alice.ID}", HttpStatusCode.OK);
            Assert.IsFalse(inactiveItem.IsActive);

            #endregion

            #region SetActive test
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{alice.ID}/action/setactive", HttpStatusCode.OK);
            var activeItem = await EndpointTests.AssertGetStatusCode<FlatListItem>(client, $"{apiUrl}/{alice.ID}", HttpStatusCode.OK);
            Assert.IsTrue(activeItem.IsActive);
            #endregion

            #region CanDelete test
            BooleanResponse canDeleteResponse = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{bob.ID}/action/candelete", HttpStatusCode.OK);
            Assert.IsTrue(canDeleteResponse.Success);
            Assert.IsTrue(canDeleteResponse.Value.Value);

            var systemFlatListItem = Utils.GetFlatListItem();
            systemFlatListItem.Name = "TestFlatListActionEndpointsSystem";
            var createdSystemFLI = await EndpointTests.AssertPostStatusCode<FlatListItem>(client, $"{apiUrl}/action/forcecreate", systemFlatListItem, HttpStatusCode.OK);

            canDeleteResponse = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{createdSystemFLI.ID}/action/candelete", HttpStatusCode.OK);
            Assert.IsTrue(canDeleteResponse.Success);
            Assert.IsFalse(canDeleteResponse.Value.Value);

            canDeleteResponse = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{-bob.ID}/action/candelete", HttpStatusCode.BadRequest);
            Assert.IsFalse(canDeleteResponse.Success);
            #endregion

            #region Test Cleanup
            
            await EndpointTests.AssertPostStatusCodeEither(client, $"{apiUrl}/{alice.ID}/action/forcedelete", JsonConvert.SerializeObject(testItem), HttpStatusCode.NoContent, HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCodeEither(client, $"{apiUrl}/{bob.ID}/action/forcedelete", JsonConvert.SerializeObject(testItem), HttpStatusCode.NoContent, HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCodeEither(client, $"{apiUrl}/{charlie.ID}/action/forcedelete", JsonConvert.SerializeObject(testItem), HttpStatusCode.NoContent, HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCodeEither(client, $"{apiUrl}/{david.ID}/action/forcedelete", JsonConvert.SerializeObject(testItem), HttpStatusCode.NoContent, HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCodeEither(client, $"{apiUrl}/{edward.ID}/action/forcedelete", JsonConvert.SerializeObject(testItem), HttpStatusCode.NoContent, HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCodeEither(client, $"{apiUrl}/{frank.ID}/action/forcedelete", JsonConvert.SerializeObject(testItem), HttpStatusCode.NoContent, HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCodeEither(client, $"{apiUrl}/{gordon.ID}/action/forcedelete", JsonConvert.SerializeObject(testItem), HttpStatusCode.NoContent, HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCodeEither(client, $"{apiUrl}/{createdSystemFLI.ID}/action/forcedelete", JsonConvert.SerializeObject(testItem), HttpStatusCode.NoContent, HttpStatusCode.OK);

            #endregion
        }
    }
}
