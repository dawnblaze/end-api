﻿using Endor.EF;
using Endor.Pricing;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    class MockPricingEngine : IPricingEngine
    {
        public static bool MakeAllCallsFail = false;

        public async Task<OrderPriceResult> Compute(OrderPriceRequest request, bool ComputeTaxes = true)
        {
            await Task.Yield();
            CheckFailure();

            return new OrderPriceResult();
        }

        public async Task<ItemPriceResult> Compute(ItemPriceRequest request, bool ComputeTaxes = true)
        {
            await Task.Yield();
            CheckFailure();

            return new ItemPriceResult();
        }

        public DestinationPriceResult Compute(DestinationPriceRequest request, bool ComputeTaxes = true)
        {
            CheckFailure();

            return new DestinationPriceResult();
        }

        internal void CheckFailure()
        {
            if (MockPricingEngine.MakeAllCallsFail)
            {
                throw new Exception("Computation faild");
            }
        }
    }
}
