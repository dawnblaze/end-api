﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Api.Tests
{
    public static class TestConstants
    {
        public const short BID = 1;
        public const byte AID = 1;
        public const short AuthUserLinkID = 1;
        public const int AuthUserID = 1;
        public const string AuthUserName = "TestUser";
        public const short AuthEmployeeID = 1;
        public const short AccessType = 49; //SupportManage
    }
}
