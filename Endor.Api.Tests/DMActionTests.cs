﻿using Endor.Api.Web.Controllers;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AssertX = Xunit.Assert;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using Microsoft.AspNetCore.Mvc;
using Endor.DocumentStorage.Models;
using System.Net.Http;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Endor.AzureStorage;
using Endor.Tenant;

namespace Endor.Api.Tests
{
    [TestClass]
    public class DMActionTests : CommonControllerTestClass
    {
        private const string TestTextFileStringContent = "this is a test";

        [TestMethod]
        [DeploymentItem(TestTextFilePath)]
        [DeploymentItem(TestTextTwoFilePath)]
        public async Task DeleteMany()
        {
            var entity = new DMController.RequestModel()
            {
                id = 113,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents
            };

            var testController = GetDMController();
            await CleanSlate(testController, entity);
            //Create 1
            await UploadFile(entity, testController, TestTextFileName);

            //Create 2
            await UploadFile(entity, testController, TestTextTwoFileName);

            //delete what we just posted
            var postResult = await testController.DeleteManyRoute1(entity.Bucket, entity.ctid, entity.id, new string[] { TestTextFileName, TestTextTwoFileName });
            var okPost = AssertX.IsType<OkObjectResult>(postResult);
            Assert.IsTrue(okPost.StatusCode == 200);
            AssertX.IsType<int>(okPost.Value);
            Assert.IsTrue((int)okPost.Value == 2);

            //ensure we have no documents
            var getResult = await testController.GetDocuments(entity);
            var okGet = AssertX.IsType<OkObjectResult>(getResult);
            Assert.IsTrue(okGet.StatusCode == 200);
            AssertX.IsType<List<DMItem>>(okGet.Value);
            Assert.IsTrue((okGet.Value as List<DMItem>).Count == 0);
        }

        [TestMethod]
        [DeploymentItem(TestTextFilePath)]
        public async Task Duplicate()
        {
            var entity = new DMController.RequestModel()
            {
                id = 113,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents
            };

            var testController = GetDMController();
            await CleanSlate(testController, entity);
            //Create 1
            await UploadFile(entity, testController, TestTextFileName);

            //duplicate in same folder
            var dupePost = AssertX.IsType<OkObjectResult>(await testController.DuplicateManyRoute1(entity.Bucket, entity.ctid, entity.id, new string[] { TestTextFileName }));
            Assert.IsTrue(dupePost.StatusCode == 200);
            var dupes = AssertX.IsType<int>(dupePost.Value);
            Assert.AreEqual(1, dupes);

            var getResult = await testController.GetDocuments(entity);
            var okGet = AssertX.IsType<OkObjectResult>(getResult);
            Assert.IsTrue(okGet.StatusCode == 200);
            var files = AssertX.IsType<List<DMItem>>(okGet.Value);
            Assert.IsTrue(files.Count == 2); //two files
            var originalFile = files.FirstOrDefault(f => f.Name == TestTextFileName);
            Assert.IsNotNull(originalFile);
            string[] splits = TestTextFileName.Split('.');
            string expectedDuplicateFileName = splits[0] + " (2)." + splits[1];
            var newFile = files.FirstOrDefault(f => f.Name == expectedDuplicateFileName);

            //ModifiedByID is expected to be the current User.UserLinkID
            Assert.IsTrue(newFile.ModifiedByID == testController.User.UserLinkID().Value);
            
            Assert.IsNotNull(newFile);

        }

        [TestMethod]
        [DeploymentItem(TestTextFilePath)]
        [DeploymentItem(TestTextTwoFilePath)]
        public async Task MoveMultipleFiles()
        {
            var entity = new DMController.RequestModel()
            {
                id = 113,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents
            };
            var otherFolder = new DMController.RequestModel()
            {
                id = 113,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents,
                classFolder = "113/otherFolder"
            };

            var testController = GetDMController();
            await CleanSlate(testController, entity);
            await CleanSlate(testController, otherFolder);

            //Create 1
            await UploadFile(entity, testController, TestTextFileName);
            //Create 2
            await UploadFile(entity, testController, TestTextTwoFileName);

            //move to new folder
            var movePost = AssertX.IsType<OkObjectResult>(await testController.MoveManyRoute1(entity.Bucket, entity.ctid, entity.id, "otherFolder", null, null, new string[] { TestTextFileName, TestTextTwoFileName }));
            Assert.IsTrue(movePost.StatusCode == 200);
            Assert.IsTrue(AssertX.IsType<int>(movePost.Value) == 2);

            var getResult = await testController.GetDocuments(otherFolder);
            var okGet = AssertX.IsType<OkObjectResult>(getResult);
            Assert.IsTrue(okGet.StatusCode == 200);
            var filesAndFolders = AssertX.IsType<List<DMItem>>(okGet.Value);
            Assert.IsTrue(filesAndFolders.Count == 2); //files
            Assert.IsTrue(filesAndFolders[0].Name == TestTextFileName);
            Assert.IsTrue(filesAndFolders[1].Name == TestTextTwoFileName);

            //ModifiedByID is expected to be the current User.UserLinkID
            Assert.IsTrue(filesAndFolders[0].ModifiedByID == testController.User.UserLinkID().Value);
            Assert.IsTrue(filesAndFolders[1].ModifiedByID == testController.User.UserLinkID().Value);

        }


        [TestMethod]
        [DeploymentItem(TestTextFilePath)]
        [DeploymentItem(TestTextTwoFilePath)]
        public async Task CopyMultipleFiles()
        {
            var entity = new DMController.RequestModel()
            {
                id = 113,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents
            };
            var otherFolder = new DMController.RequestModel()
            {
                id = 113,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents,
                classFolder = "113/otherFolder"
            };

            var testController = GetDMController();
            await CleanSlate(testController, entity);
            await CleanSlate(testController, otherFolder);

            //Create 1
            await UploadFile(entity, testController, TestTextFileName);

            //Create 2
            await UploadFile(entity, testController, TestTextTwoFileName);

            //Copy to new folder
            var copyPost = AssertX.IsType<OkObjectResult>(await testController.CopyManyRoute1(entity.Bucket, entity.ctid, entity.id, "otherFolder", null, null, new string[] { TestTextFileName, TestTextTwoFileName }));
            Assert.IsTrue(copyPost.StatusCode == 200);
            AssertX.IsType<int>(copyPost.Value);
            Assert.IsTrue((int)copyPost.Value == 2);

            var getResult = await testController.GetDocuments(otherFolder);
            var okGet = AssertX.IsType<OkObjectResult>(getResult);
            Assert.IsTrue(okGet.StatusCode == 200);
            var filesAndFolders = AssertX.IsType<List<DMItem>>(okGet.Value);
            Assert.IsTrue(filesAndFolders.Count == 2); //files
            Assert.IsTrue(filesAndFolders[0].Name == TestTextFileName);
            Assert.IsTrue(filesAndFolders[1].Name == TestTextTwoFileName);

            //ModifiedByID is expected to be the current User.UserLinkID
            Assert.IsTrue(filesAndFolders[0].ModifiedByID == testController.User.UserLinkID().Value);
            Assert.IsTrue(filesAndFolders[1].ModifiedByID == testController.User.UserLinkID().Value);

        }

        [TestMethod]
        [DeploymentItem(TestTextFilePath)]
        public async Task ZipFile()
        {
            var entity = new DMController.RequestModel()
            {
                id = 114,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents
            };

            var testController = GetDMController();
            //Create 1
            await UploadFile(entity, testController, TestTextFileName);

            //zip just the one file
            var zip = AssertX.IsType<OkObjectResult>(await testController.ZipManyRoute1(entity.Bucket, entity.ctid, entity.id, entity.PathAndFilename, new string[] { TestTextFileName }));
            Assert.IsNotNull(zip);
            var zipString = AssertX.IsType<string>(zip.Value);

            //make sure we can access it anonymously
            Stream blob;
            using (HttpClient client = new HttpClient())
            {
                blob = await client.GetStreamAsync(new Uri(zipString));
            }
            Assert.IsNotNull(blob);

            //make sure the zip is fully formed and can be unzipped and has correct content
            using (var archive = new ZipArchive(blob, ZipArchiveMode.Read))
            {
                Assert.IsNotNull(archive.Entries);
                Assert.IsTrue(archive.Entries.Count == 1);
                Assert.AreEqual(archive.Entries[0].Name, TestTextFileName);
                using (StreamReader reader = new StreamReader(archive.Entries[0].Open()))
                {
                    string content = reader.ReadToEnd();
                    Assert.IsTrue(content == TestTextFileStringContent);
                }
            }
        }

        [TestMethod]
        [DeploymentItem(TestTextFilePath)]
        public async Task ZipFolder()
        {
            var entity = new DMController.RequestModel()
            {
                id = 114,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents
            };

            string FolderName = "Dan";
            string EmptySubfolderName = "Harmon";

            var testController = GetDMController();
            await CleanSlate(testController, entity);
            //Create 1
            await UploadFile(entity, testController, TestTextFileName, FolderName + '/' + TestTextFileName);
            await UploadFolder(entity, testController, $"{FolderName}/{EmptySubfolderName}");

            //zip just the one folder
            var zip = AssertX.IsType<OkObjectResult>(await testController.ZipManyRoute1(entity.Bucket, entity.ctid, entity.id, entity.PathAndFilename, new string[] { FolderName + "/_" }));
            Assert.IsNotNull(zip);
            var zipString = AssertX.IsType<string>(zip.Value);


            //make sure we can access it anonymously
            Stream blob;
            using (HttpClient client = new HttpClient())
            {
                blob = await client.GetStreamAsync(zipString);
            }
            Assert.IsNotNull(blob);

            //make sure the zip is fully formed and can be unzipped and has correct content
            using (var archive = new ZipArchive(blob, ZipArchiveMode.Read))
            {
                Assert.IsNotNull(archive.Entries);
                Assert.IsTrue(archive.Entries.Count == 3);//folder+file+emptyfolder
                var fileEntry = archive.Entries.FirstOrDefault(x => x.Name == TestTextFileName);
                Assert.IsNotNull(fileEntry);
                using (StreamReader reader = new StreamReader(fileEntry.Open()))
                {
                    string content = reader.ReadToEnd();
                    Assert.IsTrue(content == TestTextFileStringContent);
                }
                var folderEntry = archive.Entries.FirstOrDefault(x => x.FullName == FolderName + '/');
                Assert.IsNotNull(folderEntry);
                var emptyFolderEntry = archive.Entries.FirstOrDefault(x => x.FullName == $"{FolderName}/{EmptySubfolderName}/");
                Assert.IsNotNull(emptyFolderEntry);
            }

            string SubFolderName = "Marino";
            //Create 2
            await UploadFile(entity, testController, TestTextFileName, $"{FolderName}/{SubFolderName}/{TestTextFileName}");


            //zip just the subfolder
            zip = AssertX.IsType<OkObjectResult>(await testController.ZipManyRoute1(entity.Bucket, entity.ctid, entity.id, entity.PathAndFilename, new string[] { $"{FolderName}/{SubFolderName}/_" }));
            Assert.IsNotNull(zip);
            zipString = AssertX.IsType<string>(zip.Value);

            Assert.IsNotNull(zipString);

            using (HttpClient client = new HttpClient())
            {
                blob = await client.GetStreamAsync(zipString);
            }
            Assert.IsNotNull(blob);

            using (var archive = new ZipArchive(blob, ZipArchiveMode.Read))
            {
                Assert.IsNotNull(archive.Entries);
                Assert.IsTrue(archive.Entries.Count == 2);//folder+file
                var fileEntry = archive.Entries.FirstOrDefault(x => x.Name == TestTextFileName);
                Assert.IsNotNull(fileEntry);
                using (StreamReader reader = new StreamReader(fileEntry.Open()))
                {
                    string content = reader.ReadToEnd();
                    Assert.IsTrue(content == TestTextFileStringContent);
                }
                var folderEntry = archive.Entries.FirstOrDefault(x => x.FullName == SubFolderName + '/');
                Assert.IsNotNull(folderEntry);
            }
        }

        [TestMethod]
        [DeploymentItem(TestTextFilePath)]
        public async Task ZipRootFolder()
        {
            var entity = new DMController.RequestModel()
            {
                id = Utils.GetHashIDForTestMethod(),
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents
            };

            string FolderName = "folder";
            string FolderName2 = "folder2";
            string EmptySubfolderName = "emptysubfolder";
            string FolderSubfolderName = "subfolder";

            var testController = GetDMController();
            await CleanSlate(testController, entity);
            //Create 1st folder with an empty subfolder, and a subfolder with a file
            await UploadFile(entity, testController, TestTextFileName, $"{FolderName}/{FolderSubfolderName}/{TestTextFileName}");
            await UploadFolder(entity, testController, $"{FolderName}/{FolderSubfolderName}");
            await UploadFolder(entity, testController, $"{FolderName}/{EmptySubfolderName}");
            //Create 2nd folder that is empty
            await UploadFolder(entity, testController, FolderName2);

            //zip the root folder
            var zip = AssertX.IsType<OkObjectResult>(await testController.ZipManyRoute1(entity.Bucket, entity.ctid, entity.id, entity.PathAndFilename, new string[] { "/" + EntityStorageClient.SpecialFolderBlobName }));
            Assert.IsNotNull(zip);
            var zipString = AssertX.IsType<string>(zip.Value);

            //make sure we can access it anonymously
            Stream blob;
            using (HttpClient client = new HttpClient())
            {
                blob = await client.GetStreamAsync(zipString);
            }
            Assert.IsNotNull(blob);

            //make sure the zip is fully formed and can be unzipped and has correct content
            using (var archive = new ZipArchive(blob, ZipArchiveMode.Read))
            {
                Assert.IsNotNull(archive.Entries);
                //Entries: folder + folder_emptysubfolder + folder_subfolder + folder_subfolder_file + folder2
                Assert.IsTrue(archive.Entries.Count == 10);
                var fileEntry = archive.Entries.FirstOrDefault(x => x.Name == TestTextFileName);
                Assert.IsNotNull(fileEntry);
                using (StreamReader reader = new StreamReader(fileEntry.Open()))
                {
                    string content = reader.ReadToEnd();
                    Assert.IsTrue(content == TestTextFileStringContent);
                }
                var folderEntry = archive.Entries.FirstOrDefault(x => x.FullName == (FolderName + '/'));
                Assert.IsNotNull(folderEntry);
                var subfolderEntry = archive.Entries.FirstOrDefault(x => x.FullName == $"{FolderName}/{FolderSubfolderName}/");
                Assert.IsNotNull(subfolderEntry);
                var emptySubfolderEntry = archive.Entries.FirstOrDefault(x => x.FullName == $"{FolderName}/{EmptySubfolderName}/");
                Assert.IsNotNull(emptySubfolderEntry);
                var folder2Entry = archive.Entries.FirstOrDefault(x => x.FullName == (FolderName2 + '/'));
                Assert.IsNotNull(folder2Entry);
            }
        }

        private async Task UploadFolder(DMController.RequestModel entity, DMController testController, string folderName)
        {
            entity.PathAndFilename = folderName;

            var okPost = AssertX.IsType<OkObjectResult>(await testController.Post(entity, null, null));
            Assert.IsTrue(okPost.StatusCode == 200);
            Assert.IsTrue(AssertX.IsType<DMItem>(okPost.Value)?.IsFolder == true);

            //CreatedByID is expected to be the current User.UserLinkID
            DMItem folder = okPost.Value as DMItem;
            Assert.IsTrue(folder.CreatedByID == testController.User.UserLinkID().Value);

            entity.PathAndFilename = null;
        }

        [TestMethod]
        [DeploymentItem(TestTextFilePath)]
        public async Task ZipAll()
        {
            var entity = new DMController.RequestModel()
            {
                id = 114,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents
            };

            var testController = GetDMController();
            //Create 1
            await UploadFile(entity, testController, TestTextFileName);

            //zip just the one file
            var zip = AssertX.IsType<OkObjectResult>(await testController.ZipManyRoute1(entity.Bucket, entity.ctid, entity.id, entity.PathAndFilename, new string[] { TestTextFileName }));

            Assert.IsNotNull(zip);
            var zipString = AssertX.IsType<string>(zip.Value);

            //make sure we can access it anonymously
            Stream blob;
            using (HttpClient client = new HttpClient())
            {
                blob = await client.GetStreamAsync(zipString);
            }
            Assert.IsNotNull(blob);

            //make sure the zip is fully formed and can be unzipped and has correct content
            using (var archive = new ZipArchive(blob, ZipArchiveMode.Read))
            {
                Assert.IsNotNull(archive.Entries);
                Assert.IsTrue(archive.Entries.Count == 1);
                Assert.AreEqual(archive.Entries[0].Name, TestTextFileName);
                using (StreamReader reader = new StreamReader(archive.Entries[0].Open()))
                {
                    string content = reader.ReadToEnd();
                    Assert.IsTrue(content == TestTextFileStringContent);
                }
            }
        }
        
        private async Task<DMController.RequestModel> Initialize()
        {
            var classTemplateEntity = new DMController.RequestModel()
            {
                ctid = 5000,
                classFolder = "template"
            };
            var tempEntity = new DMController.RequestModel()
            {
                guid = Guid.NewGuid(),
            };

            var testController = GetDMController();
            //Create in template
            await UploadFile(classTemplateEntity, testController, TestTextFileName);

            //send a request with both temp and ctid
            var tempAsCTID = tempEntity.Clone();
            tempAsCTID.ctid = classTemplateEntity.ctid;
            //initialize using the temp+ctid
            var okPost = AssertX.IsType<OkObjectResult>(await testController.InitializeRoute1(tempAsCTID.ctid, tempAsCTID.id));
            Assert.AreEqual(200, okPost.StatusCode);
            Assert.AreEqual(1, AssertX.IsType<int>(okPost.Value));

            //now pull docs using _just_ the temp guid
            var okGet = AssertX.IsType<OkObjectResult>(await testController.GetDocuments(tempEntity));
            Assert.IsTrue(okGet.StatusCode == 200);
            var docs = AssertX.IsType<List<DMItem>>(okGet.Value);
            //we should have documents!
            Assert.IsTrue(docs.Count == 1);
            //and look, it's the one we put in the template
            Assert.IsTrue(docs[0].Name == TestTextFileName);

            return tempEntity;
        }

        [TestMethod]
        [DeploymentItem(TestTextFilePath)]
        [Ignore]
        public async Task Persist()
        {
            var tempEntity = await Initialize();
            var testController = GetDMController();

            const string tempFileName = "iAmJustInTemp.txt";
            var newFileRequest = tempEntity.Clone();
            newFileRequest.PathAndFilename = tempFileName;
            await UploadFile(tempEntity, testController, TestTextFileName, tempFileName);

            var entity = new DMController.RequestModel()
            {
                guid = tempEntity.guid,
                id = 133,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents
            };

            OkObjectResult okPost = AssertX.IsType<OkObjectResult>(await testController.PersistTemporary(entity.guid, entity.ctid, entity.id));
            Assert.IsTrue(okPost.StatusCode == 200);
            Assert.AreEqual(2, AssertX.IsType<int>(okPost.Value));

            OkObjectResult okGet = AssertX.IsType<OkObjectResult>(await testController.GetDocuments(entity));
            List<DMItem> docs = AssertX.IsType<List<DMItem>>(okGet.Value);
            Assert.IsNotNull(docs.FirstOrDefault(f => !f.IsFolder && f.Name == tempFileName));
        }

        [TestMethod]
        public async Task DeleteAllBucketsTest()
        {
            var entity = new DMController.RequestModel()
            {
                id = 115,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents
            };

            DMID target = new DMID() { id = entity.id, ctid = entity.ctid };

            var testController = GetDMController();

            EntityStorageClient entityStorageClient = new EntityStorageClient(_config[CommonControllerTestClass.BlobStorageConfigName], testController.User.BID().Value);
            string unique = Guid.NewGuid().ToString();
            await SetupMultiBucketStorageForTest(target, testController, entityStorageClient, unique);

            IActionResult result = await testController.DeleteAll(target.ctid.Value, target.id.Value);

            List<string> datalist = (await entityStorageClient.GetDocumentList(StorageBin.Permanent, Bucket.Data, target, true)).Select(t => t.Name).ToList();
            List<string> documentslist = (await entityStorageClient.GetDocumentList(StorageBin.Permanent, Bucket.Documents, target, true)).Select(t => t.Name).ToList();
            List<string> reportslist = (await entityStorageClient.GetDocumentList(StorageBin.Permanent, Bucket.Reports, target, true)).Select(t => t.Name).ToList();
            Assert.AreEqual(0, datalist.Count);
            Assert.AreEqual(0, documentslist.Count);
            Assert.AreEqual(0, reportslist.Count);
        }

        [TestMethod]
        public async Task CloneAllBucketsTest()
        {
            var entity = new DMController.RequestModel()
            {
                id = 116,
                ctid = Convert.ToInt32(ClassType.Employee),
                Bucket = Web.BucketRequest.Documents
            };

            int destid = 1117;

            DMID source = new DMID() { id = entity.id, ctid = entity.ctid };
            DMID dest = new DMID() { id = destid, ctid = source.ctid };
            var testController = GetDMController();

            EntityStorageClient entityStorageClient = new EntityStorageClient(_config[CommonControllerTestClass.BlobStorageConfigName], testController.User.BID().Value);
            string unique = Guid.NewGuid().ToString();
            await SetupMultiBucketStorageForTest(source, testController, entityStorageClient, unique);

            await DeleteStorageFiles(dest, entityStorageClient);

            await testController.Clone(source.ctid.Value, source.id.Value, destid);

            List<string> datalist = (await entityStorageClient.GetDocumentList(StorageBin.Permanent, Bucket.Data, dest, true)).Select(t => t.Name).ToList();
            List<string> documentslist = (await entityStorageClient.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dest, true)).Select(t => t.Name).ToList();
            List<string> reportslist = (await entityStorageClient.GetDocumentList(StorageBin.Permanent, Bucket.Reports, dest, true)).Select(t => t.Name).ToList();

            List<string> expectedFiles = new List<string>() { "file1.txt", "file2.txt", "folder1/file3.txt" };

            Assert.AreEqual(String.Join(";", expectedFiles), String.Join(";", datalist));
            Assert.AreEqual(String.Join(";", expectedFiles), String.Join(";", documentslist));
            Assert.AreEqual(String.Join(";", expectedFiles), String.Join(";", reportslist));
        }

        private static async Task SetupMultiBucketStorageForTest(DMID source, DMController testController, EntityStorageClient entityStorageClient, string uniqueFileName)
        {
            string file1Name = $"{uniqueFileName}-file1.txt";
            string file2Name = $"{uniqueFileName}-file2.txt";
            string file3Name = $"{uniqueFileName}-file3.txt";
            List<string> expectedFiles = new List<string>()
            {
                "file1.txt",
                "file2.txt",
                "folder1/file3.txt"
            };

            await DeleteStorageFiles(source, entityStorageClient);

            await File.WriteAllTextAsync(file1Name, "file1.txt");
            await File.WriteAllTextAsync(file2Name, "file2.txt");
            await File.WriteAllTextAsync(file3Name, "file3.txt");

            await entityStorageClient.AddFile(File.OpenRead(file1Name), StorageBin.Permanent, Bucket.Documents, source, "file1.txt", "text/plain", Convert.ToInt32(testController.User.FindFirst("EmployeeID").Value), (int)ClassType.Employee);
            await entityStorageClient.AddFile(File.OpenRead(file2Name), StorageBin.Permanent, Bucket.Documents, source, "file2.txt", "text/plain", Convert.ToInt32(testController.User.FindFirst("EmployeeID").Value), (int)ClassType.Employee);
            await entityStorageClient.AddFile(File.OpenRead(file3Name), StorageBin.Permanent, Bucket.Documents, source, "folder1/file3.txt", "text/plain", Convert.ToInt32(testController.User.FindFirst("EmployeeID").Value), (int)ClassType.Employee);

            await entityStorageClient.AddFile(File.OpenRead(file1Name), StorageBin.Permanent, Bucket.Reports, source, "file1.txt", "text/plain", Convert.ToInt32(testController.User.FindFirst("EmployeeID").Value), (int)ClassType.Employee);
            await entityStorageClient.AddFile(File.OpenRead(file2Name), StorageBin.Permanent, Bucket.Reports, source, "file2.txt", "text/plain", Convert.ToInt32(testController.User.FindFirst("EmployeeID").Value), (int)ClassType.Employee);
            await entityStorageClient.AddFile(File.OpenRead(file3Name), StorageBin.Permanent, Bucket.Reports, source, "folder1/file3.txt", "text/plain", Convert.ToInt32(testController.User.FindFirst("EmployeeID").Value), (int)ClassType.Employee);

            await entityStorageClient.AddFile(File.OpenRead(file1Name), StorageBin.Permanent, Bucket.Data, source, "file1.txt", "text/plain", Convert.ToInt32(testController.User.FindFirst("EmployeeID").Value), (int)ClassType.Employee);
            await entityStorageClient.AddFile(File.OpenRead(file2Name), StorageBin.Permanent, Bucket.Data, source, "file2.txt", "text/plain", Convert.ToInt32(testController.User.FindFirst("EmployeeID").Value), (int)ClassType.Employee);
            await entityStorageClient.AddFile(File.OpenRead(file3Name), StorageBin.Permanent, Bucket.Data, source, "folder1/file3.txt", "text/plain", Convert.ToInt32(testController.User.FindFirst("EmployeeID").Value), (int)ClassType.Employee);

            List<string> datalist = (await entityStorageClient.GetDocumentList(StorageBin.Permanent, Bucket.Data, source, true)).Select(t => t.Name).ToList();
            List<string> documentslist = (await entityStorageClient.GetDocumentList(StorageBin.Permanent, Bucket.Documents, source, true)).Select(t => t.Name).ToList();
            List<string> reportslist = (await entityStorageClient.GetDocumentList(StorageBin.Permanent, Bucket.Reports, source, true)).Select(t => t.Name).ToList();
            Assert.AreEqual(String.Join(";", expectedFiles), String.Join(";", datalist));
            Assert.AreEqual(String.Join(";", expectedFiles), String.Join(";", documentslist));
            Assert.AreEqual(String.Join(";", expectedFiles), String.Join(";", reportslist));
        }

        private static async Task DeleteStorageFiles(DMID source, EntityStorageClient entityStorageClient)
        {
            await entityStorageClient.DeleteFile(StorageBin.Permanent, Bucket.Data, source, "file1.txt");
            await entityStorageClient.DeleteFile(StorageBin.Permanent, Bucket.Data, source, "file2.txt");
            await entityStorageClient.DeleteFile(StorageBin.Permanent, Bucket.Data, source, "folder1/file3.txt");
            await entityStorageClient.DeleteFile(StorageBin.Permanent, Bucket.Documents, source, "file1.txt");
            await entityStorageClient.DeleteFile(StorageBin.Permanent, Bucket.Documents, source, "file2.txt");
            await entityStorageClient.DeleteFile(StorageBin.Permanent, Bucket.Documents, source, "folder1/file3.txt");
            await entityStorageClient.DeleteFile(StorageBin.Permanent, Bucket.Reports, source, "file1.txt");
            await entityStorageClient.DeleteFile(StorageBin.Permanent, Bucket.Reports, source, "file2.txt");
            await entityStorageClient.DeleteFile(StorageBin.Permanent, Bucket.Reports, source, "folder1/file3.txt");
        }
    }
}
