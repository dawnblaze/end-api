﻿using Endor.Api.Web.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Endor.Models;
using System.Threading.Tasks;
using System.Linq;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    [TestClass]
    public class EmployeeRoleEndpointTests
    {
        public const string apiUrl = "/api/employeerole";

        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public string EmployeeRoleTestStamp { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            this.EmployeeRoleTestStamp = $"{DateTime.UtcNow} EmployeeRoleTestStamp";
        }

        [TestMethod]
        public async Task EmployeeRoleGetSimpleEndpointTest()
        {
            await EndpointTests.AssertGetStatusCode<SimpleEmployeeRole[]>(client, $"{apiUrl}/simplelist", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task EmployeeRoleCRUDTest()
        {
            // POST A new employee Role
            var postRoleRequestModel = GetPOSTEmployeeRole();
            var postInactiveRoleRequestModel = GetPOSTEmployeeRole();
            postInactiveRoleRequestModel.IsActive = false;

            var postedRole = await EndpointTests.AssertPostStatusCode<EmployeeRole>(client, apiUrl, postRoleRequestModel, HttpStatusCode.OK);
            var postedInactiveRole = await EndpointTests.AssertPostStatusCode<EmployeeRole>(client, apiUrl, postInactiveRoleRequestModel, HttpStatusCode.OK);
            try
            {
                //IsActive=true should be default
                var defaultEmployeeRoles = await EndpointTests.AssertGetStatusCode<EmployeeRole[]>(client, apiUrl, HttpStatusCode.OK);

                var activeEmployeeRoles = await EndpointTests.AssertGetStatusCode<EmployeeRole[]>(client, $"{apiUrl}?isDefault=true", HttpStatusCode.OK);
                Assert.AreEqual(JsonConvert.SerializeObject(defaultEmployeeRoles), JsonConvert.SerializeObject(activeEmployeeRoles));
                Assert.IsNotNull(activeEmployeeRoles.All(t => t.IsActive));

                var inactiveEmployeeRoles = await EndpointTests.AssertGetStatusCode<EmployeeRole[]>(client, $"{apiUrl}?isactive=false", HttpStatusCode.OK);
                Assert.IsNotNull(inactiveEmployeeRoles.All(t => !t.IsActive));

                // GetSingle

                //simplelist filters
                var defaultSimpleEmployeeRoles = await EndpointTests.AssertGetStatusCode<SimpleEmployeeRole[]>(client, $"{apiUrl}/simplelist", HttpStatusCode.OK);
                var allSimpleEmployeeRoles = await EndpointTests.AssertGetStatusCode<SimpleEmployeeRole[]>(client, $"{apiUrl}/simplelist?type=all", HttpStatusCode.OK);
                Assert.AreEqual(JsonConvert.SerializeObject(defaultSimpleEmployeeRoles), JsonConvert.SerializeObject(allSimpleEmployeeRoles));

                var teamSimpleEmployeeRoles = await EndpointTests.AssertGetStatusCode<SimpleEmployeeRole[]>(client, $"{apiUrl}/simplelist?type=team", HttpStatusCode.OK);
                TestSimpleCollection(activeEmployeeRoles, teamSimpleEmployeeRoles, EmployeeRoleType.Team);
                var orderSimpleEmployeeRoles = await EndpointTests.AssertGetStatusCode<SimpleEmployeeRole[]>(client, $"{apiUrl}/simplelist?type=order", HttpStatusCode.OK);
                TestSimpleCollection(activeEmployeeRoles, orderSimpleEmployeeRoles, EmployeeRoleType.Order);
                var orderitemSimpleEmployeeRoles = await EndpointTests.AssertGetStatusCode<SimpleEmployeeRole[]>(client, $"{apiUrl}/simplelist?type=orderitem", HttpStatusCode.OK);
                TestSimpleCollection(activeEmployeeRoles, orderitemSimpleEmployeeRoles, EmployeeRoleType.OrderItem);
                var orderdestinationSimpleEmployeeRoles = await EndpointTests.AssertGetStatusCode<SimpleEmployeeRole[]>(client, $"{apiUrl}/simplelist?type=orderdestination", HttpStatusCode.OK);
                TestSimpleCollection(activeEmployeeRoles, orderdestinationSimpleEmployeeRoles, EmployeeRoleType.OrderDestination);
                var estimateSimpleEmployeeRoles = await EndpointTests.AssertGetStatusCode<SimpleEmployeeRole[]>(client, $"{apiUrl}/simplelist?type=estimate", HttpStatusCode.OK);
                TestSimpleCollection(activeEmployeeRoles, estimateSimpleEmployeeRoles, EmployeeRoleType.Estimate);
                var estimateitemSimpleEmployeeRoles = await EndpointTests.AssertGetStatusCode<SimpleEmployeeRole[]>(client, $"{apiUrl}/simplelist?type=estimateitem", HttpStatusCode.OK);
                TestSimpleCollection(activeEmployeeRoles, estimateitemSimpleEmployeeRoles, EmployeeRoleType.EstimateItem);
                var estimatedestinationSimpleEmployeeRoles = await EndpointTests.AssertGetStatusCode<SimpleEmployeeRole[]>(client, $"{apiUrl}/simplelist?type=estimatedestination", HttpStatusCode.OK);
                TestSimpleCollection(activeEmployeeRoles, estimatedestinationSimpleEmployeeRoles, EmployeeRoleType.EstimateDestination);
                var opportunitySimpleEmployeeRoles = await EndpointTests.AssertGetStatusCode<SimpleEmployeeRole[]>(client, $"{apiUrl}/simplelist?type=opportunity", HttpStatusCode.OK);
                TestSimpleCollection(activeEmployeeRoles, opportunitySimpleEmployeeRoles, EmployeeRoleType.Opportunity);
                var poSimpleEmployeeRoles = await EndpointTests.AssertGetStatusCode<SimpleEmployeeRole[]>(client, $"{apiUrl}/simplelist?type=po", HttpStatusCode.OK);
                TestSimpleCollection(activeEmployeeRoles, poSimpleEmployeeRoles, EmployeeRoleType.PO);

                // Update
                var activeSystemEmployee = activeEmployeeRoles.FirstOrDefault(t => t.IsSystem);

                await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{activeSystemEmployee.ID}", activeSystemEmployee, HttpStatusCode.BadRequest);

                var nonSystemEmployee = postedRole;
                nonSystemEmployee.Name = $"ChangedMyName{DateTime.UtcNow}";

                await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{nonSystemEmployee.ID}", JsonConvert.SerializeObject(nonSystemEmployee), HttpStatusCode.OK);
                var updatedNonSystemEmployee = await EndpointTests.AssertGetStatusCode<EmployeeRole>(client, $"{apiUrl}/{nonSystemEmployee.ID}", HttpStatusCode.OK);

                Assert.AreEqual(nonSystemEmployee.Name, updatedNonSystemEmployee.Name);
            } finally
            {
                // Delete
                await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{postedRole.ID}", HttpStatusCode.NoContent);
                await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{postedInactiveRole.ID}", HttpStatusCode.NoContent);
            }
        }

        private void TestSimpleCollection(EmployeeRole[] fullCollection, SimpleEmployeeRole[] simpleCollection, EmployeeRoleType targetType)
        {
            var employeeFilter = new EmployeeRoleFilters()
            { Type = targetType };
            Assert.IsNotNull(fullCollection);
            Assert.IsNotNull(simpleCollection);

            var fullItems = fullCollection.Where(t => simpleCollection.Select(m => m.ID).Contains(t.ID)).ToList();
            Assert.IsNotNull(fullItems);
            Assert.IsTrue(fullItems.AsQueryable().All(employeeFilter.GetTypePredicate(targetType)));

        }

        private EmployeeRole GetPOSTEmployeeRole()
        {
            return new EmployeeRole()
            {
                AllowMultiple = true,
                AllowOnTeam = true,
                BID = 1,
                ClassTypeID = ClassType.EmployeeRole.ID(),
                EstimateDestinationRestriction = RoleAccess.NotAllowed,
                EstimateItemRestriction = RoleAccess.NotAllowed,
                EstimateRestriction = RoleAccess.NotAllowed,
                ID = -1,
                IsActive = true,
                IsSystem = false,
                ModifiedDT = DateTime.UtcNow,
                Name = this.EmployeeRoleTestStamp,
                OpportunityRestriction = RoleAccess.NotAllowed,
                OrderDestinationRestriction = RoleAccess.NotAllowed,
                OrderItemRestriction = RoleAccess.NotAllowed,
                OrderRestriction = RoleAccess.NotAllowed,
                PORestriction = RoleAccess.NotAllowed,
            };
        }
    }
}
