﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.GLEngine;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Tests
{
    [TestClass]
    public class GLActivityEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/glactivity";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }
        public static short BID = 1;

        [TestMethod]
        public async Task GetGLActivityTest()
        {
            ApiContext ctx = GetApiContext();
            //Delete Entries
            DeleteTables(ctx);
            var GLActivity = GetSampleGLActivity(ctx);
            ctx.ActivityGlactivity.Add(GLActivity);
            ctx.SaveChanges();

            var GLData = GetSampleGLData(ctx, GLActivity);
            ctx.GLData.Add(GLData);
            ctx.SaveChanges();

            var activities = await EndpointTests.AssertGetStatusCode<ActivityGlactivity[]>(client, $"{apiUrl}/", HttpStatusCode.OK);
            Assert.IsTrue(activities.Count() > 0);

            var activity = await EndpointTests.AssertGetStatusCode<ActivityGlactivity>(client, $"{apiUrl}/-99", HttpStatusCode.OK);
            Assert.IsTrue(activity.ID == -99);
            Assert.IsTrue(activity.GL.Count() > 0);

            //Delete Entries
            DeleteTables(ctx);

        }

        [TestMethod]
        public async Task GetGLDataInfo()
        {
            ApiContext ctx = GetApiContext();
            short BID = 1;
            var glEngine = new Endor.GLEngine.GLEngine(BID, ctx);
            var locationArray = ctx.LocationData.Select(a => a.ID).ToArray();
            var LocationID = locationArray[0];
            var companyArray = ctx.CompanyData.Select(a => a.ID).ToArray();
            var CompanyID = companyArray[0];
            var taxItem = CreateTaxGroupInDb(ctx);
            var incomeAccounts = ctx.GLAccount.Where(account => account.GLAccountType == 40).ToList(); //Income
            var expenseAccounts = ctx.GLAccount.Where(account => account.GLAccountType == 50).ToList(); //Expense
            OrderData order = CreateOrderInDb(ctx, LocationID, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = CreateOrderItemInDb(ctx, order);
            OrderItemComponent orderItemComponentData = CreateComponentWithLaborDataInDb(ctx, orderItemData, incomeAccounts[0], expenseAccounts[0]);
            //Calculate current status in db
            var activity = await EndpointTests.AssertGetStatusCode<ActivityGlactivity>(client, $"{apiUrl}/order/-99?returntype=current", HttpStatusCode.OK);
            Assert.IsNotNull(activity);
            Assert.AreEqual(activity.GL.Count(), 0);
            //calculate difference
            var diffActivity = await EndpointTests.AssertGetStatusCode<ActivityGlactivity>(client, $"{apiUrl}/order/-99?returntype=difference", HttpStatusCode.OK);
            Assert.IsNotNull(diffActivity);
            Assert.IsTrue(diffActivity.GL.Count() > 0);
            //calculate final value based on present state
            var finalActivity = await EndpointTests.AssertGetStatusCode<ActivityGlactivity>(client, $"{apiUrl}/order/-99?returntype=final", HttpStatusCode.OK);
            Assert.IsNotNull(finalActivity);
            Assert.AreEqual(finalActivity.GL.Count(),diffActivity.GL.Count());
            DeleteTables(ctx);
        }

        private ActivityGlactivity GetSampleGLActivity(ApiContext ctx)
        {
            return new ActivityGlactivity()
            {
                ID = -99,
                BID = 1,
                IsActive = true,
                Name = "Test GLActivity",
                Subject = "Test",
                CompletedDT = new DateTime()               
            };
        }

        public static OrderItemComponent CreateComponentWithLaborDataInDb(ApiContext context, OrderItemData orderItem, GLAccount incomeAccount, GLAccount expenseAccount)
        {
            short nextID = -99;
            var IDs = context.LaborData.Where(x => x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = (short)(IDs.Max() + 1);
            }

            var _labor = new LaborData()
            {
                BID = BID,
                ID = nextID,
                Name = "Test.Labor",
                InvoiceText = "Test",
                ExpenseAccountID = expenseAccount.ID,
                IncomeAccountID = incomeAccount.ID
            };
            context.Add(_labor);

            var orderItemComponent = CreateOrderItemComponentInDb(context, orderItem, incomeAccount);
            orderItemComponent.ComponentID = _labor.ID;
            orderItemComponent.ComponentType = OrderItemComponentType.Labor;
            context.SaveChanges();
            return orderItemComponent;
        }

        public static OrderItemComponent CreateOrderItemComponentInDb(ApiContext context, OrderItemData orderItem, GLAccount incomeAccount)
        {
            int nextID = -99;
            var IDs = context.OrderItemComponent.Where(x => x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = IDs.Max() + 1;
            }

            var _orderItemComponent = new OrderItemComponent()
            {
                BID = BID,
                ID = nextID,
                OrderID = orderItem.OrderID,
                OrderItemID = orderItem.ID,
                Name = "New Component",
                TotalQuantity = 1,
                PriceUnit = 10,
                CostUnit = 10 / 2,
                IncomeAccountID = incomeAccount.ID,
                IncomeAllocationType = AssemblyIncomeAllocationType.SingleIncomeAllocation
            };
            context.Add(_orderItemComponent);
            context.SaveChanges();

            return _orderItemComponent;
        }


        private GLData GetSampleGLData(ApiContext ctx,ActivityGlactivity glActivity)
        {
            var firstLocation = ctx.LocationData.FirstOrDefault();
            var GLAccount = ctx.GLAccount.FirstOrDefault();
            return new GLData()
            {
                ID = -99,
                BID = 1,
                LocationID = firstLocation.ID,
                GLAccountID = GLAccount.ID,
                Amount = 0,
                ActivityID = glActivity.ID
            };
        }

        public static void DeleteTables(ApiContext ctx)
        {
            try
            {
                ctx.GLData.RemoveRange(ctx.GLData.Where(o => o.ID < 0));
                ctx.ActivityGlactivity.RemoveRange(ctx.ActivityGlactivity.Where(o => o.ID < 0));
                ctx.SaveChanges();

                DisableSystemVersioningIfEnabled(ctx, "Order.Data");
                DisableSystemVersioningIfEnabled(ctx, "Order.Item.Data");
                DisableSystemVersioningIfEnabled(ctx, "Order.Item.Component");
                DisableSystemVersioningIfEnabled(ctx, "Order.Item.Surcharge");
                DisableSystemVersioningIfEnabled(ctx, "Order.Destination.Data");
                DisableSystemVersioningIfEnabled(ctx, "Order.Tax.Item.Assessment");
                DisableSystemVersioningIfEnabled(ctx, "Accounting.Payment.Application");

                ctx.SaveChanges();
                ctx.OrderItemTaxAssessment.RemoveRange(ctx.OrderItemTaxAssessment.Where(o => o.ID < 0));
                ctx.SaveChanges();
                ctx.OrderItemComponent.RemoveRange(ctx.OrderItemComponent.Where(o => o.ID < 0));
                DeleteTestRowsFromHistoricTable(ctx, "Order.Item.Component");
                ctx.SaveChanges();
                ctx.OrderItemSurcharge.RemoveRange(ctx.OrderItemSurcharge.Where(o => o.SurchargeDefID < 0));
                DeleteTestRowsFromHistoricTable(ctx, "Order.Item.Surcharge", "SurchargeDefID < 0");
                ctx.SaveChanges();
                ctx.MachineData.RemoveRange(ctx.MachineData.Where(o => o.ID < 0));

                ctx.AssemblyVariable.RemoveRange(ctx.AssemblyVariable.Where(o => o.AssemblyID < 0));
                ctx.SaveChanges();

                ctx.LaborData.RemoveRange(ctx.LaborData.Where(o => o.ID < 0));

                ctx.SaveChanges();
                ctx.MaterialData.RemoveRange(ctx.MaterialData.Where(o => o.ID < 0));
                ctx.AssemblyVariable.RemoveRange(ctx.AssemblyVariable.Where(o => o.AssemblyID < 0));
                ctx.AssemblyData.RemoveRange(ctx.AssemblyData.Where(o => o.ID < 0));
                ctx.PaymentApplication.RemoveRange(ctx.PaymentApplication.Where(o => o.ID < 0));
                ctx.PaymentApplication.RemoveRange(ctx.PaymentApplication.Where(o => o.MasterID < 0));
                ctx.SaveChanges();
                ctx.PaymentMaster.RemoveRange(ctx.PaymentMaster.Where(o => o.ID < 0));
                ctx.OrderItemData.RemoveRange(ctx.OrderItemData.Where(o => o.ID < 0));
                ctx.SaveChanges();
                ctx.PaymentMethod.RemoveRange(ctx.PaymentMethod.Where(o => o.ID < 0));

                ctx.OrderEmployeeRole.RemoveRange(ctx.OrderEmployeeRole.Where(o => o.OrderID < 0));
                ctx.SaveChanges();

                ctx.OrderData.RemoveRange(ctx.OrderData.Where(o => o.ID < 0));
                ctx.SaveChanges();
                ctx.OrderDestinationData.RemoveRange(ctx.OrderDestinationData.Where(o => o.ID < 0));
                //ctx.PaymentData.RemoveRange(ctx.PaymentData.Where(o => o.ID < 0));
                ctx.TaxGroup.RemoveRange(ctx.TaxGroup.Where(o => o.ID < 0));
                ctx.TaxItem.RemoveRange(ctx.TaxItem.Where(o => o.ID < 0));
                ctx.OrderDestinationTagLink.RemoveRange(ctx.OrderDestinationTagLink.Where(o => o.OrderDestinationID < 0));
                ctx.SaveChanges();
            }
            catch { }
            finally
            {
                EnableSystemVersioning(ctx, "Order.Item.Data");
                EnableSystemVersioning(ctx, "Order.Item.Component");
                EnableSystemVersioning(ctx, "Order.Item.Surcharge");
                EnableSystemVersioning(ctx, "Order.Destination.Data");
                EnableSystemVersioning(ctx, "Order.Data");
                EnableSystemVersioning(ctx, "Order.Tax.Item.Assessment");
                EnableSystemVersioning(ctx, "Accounting.Payment.Application");
            }

        }

        /// <summary>
        /// Disables System Versioning on a table if it is already enabled
        /// </summary>
        /// <param name="migrationBuilder"></param>
        /// <param name="tableName">Name of the table without square brackets</param>
        public static void DisableSystemVersioningIfEnabled(ApiContext ctx, string tableName)
        {
            string s = $@"
                    IF EXISTS (SELECT temporal_type FROM sys.tables WHERE object_id = OBJECT_ID('dbo.[{tableName}]', 'u') AND temporal_type = 2)
                    BEGIN
                        ALTER TABLE dbo.[{tableName}] SET(SYSTEM_VERSIONING = OFF);
                    END
                ";
            ctx.Database.ExecuteSqlRawAsync(s);
        }

        /// <summary>
        /// Enables System Versioning on a table
        /// </summary>
        /// <param name="migrationBuilder"></param>
        /// <param name="tableName">Name of the table without square brackets</param>
        public static void EnableSystemVersioning(ApiContext ctx, string tableName)
        {
            if (string.IsNullOrWhiteSpace(tableName))
                throw new ArgumentException($"A value for parameter {nameof(tableName)} must be supplied!");

            string s = $@"ALTER TABLE [{tableName}] SET(SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.{tableName}]))";

            ctx.Database.ExecuteSqlRawAsync(s);
        }

        /// <summary>
        /// Delete Test Rows From Historic Table
        /// </summary>
        /// <param name="migrationBuilder"></param>
        /// <param name="tableName">Name of the table without square brackets</param>
        public static void DeleteTestRowsFromHistoricTable(ApiContext ctx, string tableName, string whereCondition = "ID < 0")
        {
            if (string.IsNullOrWhiteSpace(tableName))
                throw new ArgumentException($"A value for parameter {nameof(tableName)} must be supplied!");

            string s = $@"Delete [Historic.{tableName}] where {whereCondition}";

            ctx.Database.ExecuteSqlRawAsync(s);
        }

        public static OrderItemData CreateOrderItemInDb(ApiContext context, OrderData order)
        {
            int nextID = -99;
            var IDs = context.OrderItemData.Where(x => x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = IDs.Max() + 1;
            }

            var _orderItemData = new OrderItemData()
            {
                BID = BID,
                ID = nextID,
                OrderID = order.ID,
                ItemNumber = 1,
                Quantity = 1,
                Name = "Test Order Item",
                IsOutsourced = false,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                ItemStatusID = context.OrderItemStatus.FirstOrDefault(t => t.BID == BID).ID,
                TransactionType = (byte)OrderTransactionType.Order,
                ProductionLocationID = order.LocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = order.TaxGroupID,
                TaxGroupOV = false,
                HasProof = false,
                HasDocuments = false,
                HasCustomImage = false,
            };
            context.Add(_orderItemData);
            context.SaveChanges();

            return _orderItemData;
        }

        public static OrderData CreateOrderInDb(ApiContext context, byte locationId, int CompanyID, bool taxable, short taxGroupID)
        {
            int nextID = -99;
            var IDs = context.OrderData.Where(x => x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = IDs.Max() + 1;
            }

            var _orderData = new OrderData()
            {
                BID = BID,
                ID = nextID,
                ClassTypeID = (int)ClassType.Order,
                ModifiedDT = DateTime.UtcNow,
                LocationID = locationId,
                PickupLocationID = locationId,
                ProductionLocationID = locationId,
                TransactionType = (byte)OrderTransactionType.Order,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                OrderStatusStartDT = DateTime.UtcNow,
                Number = 1000,
                FormattedNumber = "INV-1000",
                PriceTaxRate = 0.01m,
                PaymentPaid = 0m,
                CompanyID = CompanyID,
                TaxGroupID = taxGroupID,
                PriceProductTotal = 10m,
                PriceTax = 0
            };
            context.Add(_orderData);
            context.SaveChanges();

            return _orderData;
        }

        public static TaxItem CreateTaxGroupInDb(ApiContext context)
        {
            short taxGroupNextID = -99;
            var _taxGroup = context.TaxGroup.FirstOrDefault(t => t.BID == BID && t.ID == taxGroupNextID);

            if (_taxGroup == null)
            {
                _taxGroup = new TaxGroup()
                {
                    BID = BID,
                    ID = taxGroupNextID,
                    Name = "Tax Group Name"
                };
                context.Add(_taxGroup);
            }

            var salesTaxPayable = context.GLAccount.Where(entry => entry.ID == Endor.GLEngine.GLEngine.Sales_Tax_Payable_ID).First();
            var _taxItem = context.TaxItem.FirstOrDefault(t => t.BID == BID && t.ID == taxGroupNextID);
            if (_taxItem == null)
            {
                _taxItem = new TaxItem
                {
                    BID = BID,
                    ID = taxGroupNextID,
                    IsActive = true,
                    AccountNumber = "9999",
                    AgencyName = "AgencyName",
                    InvoiceText = "InvoiceText",
                    LookupCode = "LookupCode",
                    GLAccount = salesTaxPayable,
                    GLAccountID = salesTaxPayable.ID,
                    ModifiedDT = DateTime.UtcNow,
                    Name = "tax item name",
                    TaxRate = 2.5m,
                    TaxableSalesCap = 0m,
                };

                context.Add(_taxItem);
                context.SaveChanges();
            }

            return _taxItem;
        }


    }
}
