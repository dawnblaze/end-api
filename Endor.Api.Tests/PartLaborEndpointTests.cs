﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Microsoft.EntityFrameworkCore;
using Endor.DocumentStorage.Models;
using Microsoft.AspNetCore.Mvc;

namespace Endor.Api.Tests
{
    [TestClass]
    public class PartLaborDataEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/laborpart";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestLaborDataName;
        private short testBID = 1;
        public TestContext TestContext { get; set; }

        private async Task<MachineData> GetNewMachine(string machineName, int machineTemplateID)
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;

            var simpleTaxCodes = await EndpointTests.AssertGetStatusCode<SimpleTaxabilityCode[]>(client, $"/api/taxcode/SimpleList", HttpStatusCode.OK);
            short taxCodeID = simpleTaxCodes.FirstOrDefault().ID;

            var simpleLocations = await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, $"/api/location/SimpleList", HttpStatusCode.OK);
            byte locationID = simpleLocations.FirstOrDefault().ID;

            return new MachineData()
            {
                Name = machineName,
                IncomeAccountID = glAccountID,
                ExpenseAccountID = glAccountID,
                TaxCodeID = taxCodeID,
                IsActive = true,
                BID = testBID,
                ActiveInstanceCount = 0,
                ActiveProfileCount = 0,
                Description = "Description",
                HasImage = false,
                SKU = "SKU",
                WorksheetData = "[]",
                MachineTemplateID = machineTemplateID,
                Instances = new List<MachineInstance>
                {
                    new MachineInstance
                    {
                        IsActive = true,
                        Name = machineName + " - Instance",
                        LocationID = locationID,
                        Manufacturer = "Manufacturer",
                        SerialNumber = "SerialNumber",
                        IPAddress = "IPAddress",
                        PurchaseDate = DateTime.Now,
                        HasServiceContract = true,
                        ContractNumber = "",
                        ContractStartDate = DateTime.Now,
                        ContractEndDate = DateTime.Now,
                        ContractInfo = "ContractInfo",
                        Model = "Model",
                        BID = testBID
                    }
                },
                Profiles = new List<MachineProfile>
                {
                    new MachineProfile
                    {
                        IsActive = true,
                        IsDefault = true,
                        Name = machineName + " - Profile",
                        MachineTemplateID = machineTemplateID,
                        AssemblyOVDataJSON = "{}",
                        BID = testBID
                    }
                }
            };
        }

        private async Task<LaborData> GetNewModel()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;

            var simpleTaxCodes = await EndpointTests.AssertGetStatusCode<SimpleTaxabilityCode[]>(client, $"/api/taxcode/SimpleList", HttpStatusCode.OK);
            short taxCodeID = simpleTaxCodes.FirstOrDefault().ID;

            return new LaborData()
            {
                Name = this.NewTestLaborDataName,
                IncomeAccountID = glAccountID,
                ExpenseAccountID = glAccountID,
                TaxCodeID = taxCodeID,
                IsActive = true,
                BID = testBID,
                Description = "Description",
                HasImage = false,
                SKU = "SKU",
                InvoiceText = this.NewTestLaborDataName
            };
        }

        [TestInitialize]
        public void Initialize()
        {
            this.NewTestLaborDataName = DateTime.UtcNow + " TEST PART LABOR DATA";
        }

        [TestMethod]
        public async Task TestLaborDataSingleEndpoints()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleLaborData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            LaborData testObject = new LaborData()
            {
                Name = this.NewTestLaborDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InvoiceText = "blah blah"
            };

            //test: expect OK
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            // GET api/PaymentTerms/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleLaborData[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleLaborData sga in simpleList)
            {
                if (sga.DisplayName == testObject.Name)
                {
                    testObject.ID = sga.ID;
                }
            }

            testObject.Description = "test test";

            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + testObject.ID, JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            //test: expect OK
            var updatedLaborData = await EndpointTests.AssertGetStatusCode<LaborData>(client, $"{apiUrl}/" + testObject.ID, HttpStatusCode.OK);
            Assert.AreEqual(updatedLaborData.Description, testObject.Description);

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testObject.ID}", HttpStatusCode.NoContent);

            //test: expect BAD REQUEST
            //await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/9241657", HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public async Task TestLaborDataFilters()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleLaborData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            LaborData testObject = new LaborData()
            {
                Name = this.NewTestLaborDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InvoiceText = "blah blah"
            };

            //test: expect OK
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            // GET api/PaymentTerms/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleLaborData[]>(client, $"{apiUrl}/SimpleList?isActive=true", HttpStatusCode.OK);
            foreach (SimpleLaborData sga in simpleList)
            {
                if (sga.DisplayName == testObject.Name)
                {
                    testObject.ID = sga.ID;
                }
            }

            var active = await EndpointTests.AssertGetStatusCode<LaborData[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(active.All(x => x.IsActive));

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testObject.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestLaborDataActionEndpoints()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleLaborData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            LaborData testProject = new LaborData()
            {
                Name = this.NewTestLaborDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InvoiceText = "blah blah"
            };

            //test: expect OK
            var newLaborData = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"{apiUrl}", JsonConvert.SerializeObject(testProject), HttpStatusCode.OK);

            //create laborcategory for test
            LaborCategory testLaborCat = new LaborCategory()
            {
                BID = 1,
                ModifiedDT = DateTime.UtcNow,
                //CreatedDate = DateTime.UtcNow, //removed to fix 'LaborCategory' does not contain a definition for 'CreatedDate' [Endor.Api.Tests]
                IsActive = true,
                Name = "LaborCategory 1",
                Description = "LaborCategory Description 1"
            };

            //test: expect OK
            var newLaborCat = await EndpointTests.AssertPostStatusCode<LaborCategory>(client, $"/api/laborcategory", JsonConvert.SerializeObject(testLaborCat), HttpStatusCode.OK);

            //POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newLaborData.ID}/action/setinactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newLaborData.ID}/action/setactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newLaborData.ID}/action/candelete", HttpStatusCode.OK);

            //POST linklaborcategory
            EntityActionChangeResponse linkReponse;
            linkReponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newLaborData.ID}/action/linklaborcategory/{newLaborCat.ID}", HttpStatusCode.OK);
            Assert.IsTrue(linkReponse.Success);
            //unable to link twice
            linkReponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newLaborData.ID}/action/linklaborcategory/{newLaborCat.ID}", HttpStatusCode.OK);
            Assert.IsFalse(linkReponse.Success);

            //POST linklaborcategory
            EntityActionChangeResponse unlinkReponse;
            unlinkReponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newLaborData.ID}/action/unlinklaborcategory/{newLaborCat.ID}", HttpStatusCode.OK);
            Assert.IsTrue(unlinkReponse.Success);
            //unable to unlink twice
            unlinkReponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newLaborData.ID}/action/unlinklaborcategory/{newLaborCat.ID}", HttpStatusCode.OK);
            Assert.IsFalse(unlinkReponse.Success);

        }

        [TestMethod]
        public async Task TestPartLaborCanDelete()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleLaborData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            LaborData testProject = new LaborData()
            {
                Name = this.NewTestLaborDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InvoiceText = "blah blah"
            };

            //test: expect OK
            var newLaborData = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"{apiUrl}", JsonConvert.SerializeObject(testProject), HttpStatusCode.OK);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newLaborData.ID}/action/candelete", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestLaborDataPostNewInactive()
        {

            //cleanup test labor data name...
            var simpleLaborData = await EndpointTests.AssertGetStatusCode<SimpleLaborData[]>(client, $"api/laborpart/simplelist", HttpStatusCode.OK);
            var laborData = simpleLaborData.FirstOrDefault(s => s.DisplayName == this.NewTestLaborDataName);

            if (laborData != null)
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"api/laborpart/{laborData.ID}", HttpStatusCode.NoContent);
            }

            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleLaborData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;

            LaborData testObject = new LaborData()
            {
                Name = this.NewTestLaborDataName,
                IsActive = false,
                BID = 1,
                InvoiceText = "blah",
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID
            };

            //test: expect OK
            var savedAccount = await EndpointTests.AssertPostStatusCode<PaymentTerm>(client, $"{apiUrl}", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);
            
            var account = await EndpointTests.AssertGetStatusCode<PaymentTerm>(client, $"{apiUrl}/{savedAccount.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(account);
            Assert.AreEqual(false, account.IsActive);
        }

        private async Task CleanupClonedLabor(string laborName = null)
        {
            var ctx = GetApiContext();

            // cleanup test data
            bool lamdaExp(LaborData t) => t.InvoiceText.Contains(laborName ?? "TestInvoiceText");

            var testLabors = ctx.LaborData.Include(t => t.LaborCategoryLinks).Where(lamdaExp).ToList();
            var testLinks = testLabors.SelectMany(t => t.LaborCategoryLinks).ToList();
            var categoryIds = testLinks.Select(t => t.CategoryID).ToList();
            var laborIds = testLabors.Select(t => t.ID).ToList();

            ctx.LaborCategoryLink.RemoveRange(testLinks);
            ctx.LaborCategory.RemoveRange(ctx.LaborCategory.Where(t => categoryIds.Contains(t.ID)));
            ctx.LaborData.RemoveRange(testLabors);

            ctx.CustomFieldDefinition.RemoveRange(ctx.CustomFieldDefinition.Where(t => t.AppliesToID != null && (t.AppliesToClassTypeID == (int)ClassType.Labor && laborIds.Contains((int)t.AppliesToID))));
            ctx.CustomFieldOtherData.RemoveRange(ctx.CustomFieldOtherData.Where(t => t.AppliesToClassTypeID == (int)ClassType.Labor && laborIds.Contains((int)t.ID)));


            await ctx.SaveChangesAsync();

            foreach(var labor in testLabors)
            {
                await EndpointTests.AssertPostStatusCode(client, $"/api/dm/delete/{(int)ClassType.Labor}/{labor.ID}", null, HttpStatusCode.OK);
            }
        }

        [TestMethod]
        public async Task TestLaborDataCloneWithCategories()
        {
            var ctx = GetApiContext();
            var laborName = "TestLabor-" + new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();

            await CleanupClonedLabor(laborName);

            

            // create Labor Category
            var testCategory = new LaborCategory()
            {
                BID = 1,
                Name = laborName + " Category",
                IsActive = true
            };
            var laborCategory = await EndpointTests.AssertPostStatusCode<LaborCategory>(client, $"/api/laborcategory", JsonConvert.SerializeObject(testCategory), HttpStatusCode.OK);

            // create Labor
            LaborData testObject = new LaborData()
            {
                Name = laborName,
                IsActive = false,
                BID = 1,
                InvoiceText = laborName,
                ExpenseAccountID = ctx.GLAccount.First().ID,
                IncomeAccountID = ctx.GLAccount.First().ID
            };

            //test: expect OK
            var created = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"{apiUrl}", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);
            // link labor and category
            await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"/api/laborcategory/{laborCategory.ID}/action/linklabor/{created.ID}", null, HttpStatusCode.OK);

            var cloned = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"{apiUrl}/{created.ID}/clone", null, HttpStatusCode.OK);
            Assert.AreEqual($"{laborName} (Clone)", cloned.Name);

            var cloned2 = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"{apiUrl}/{created.ID}/clone", null, HttpStatusCode.OK);
            Assert.AreEqual($"{laborName} (Clone) (2)", cloned2.Name);

            var cloned3 = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"{apiUrl}/{created.ID}/clone", null, HttpStatusCode.OK);
            Assert.AreEqual($"{laborName} (Clone) (3)", cloned3.Name);

            var cloneCloned = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"{apiUrl}/{cloned.ID}/clone", null, HttpStatusCode.OK);
            Assert.AreEqual($"{laborName} (Clone) (Clone)", cloneCloned.Name);

            var cloneCloned2 = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"{apiUrl}/{cloned3.ID}/clone", null, HttpStatusCode.OK);
            Assert.AreEqual($"{laborName} (Clone) (3) (Clone)", cloneCloned2.Name);
            

            var clonedLinks = ctx.LaborCategoryLink.FirstOrDefault(t => t.PartID == cloned.ID);
            Assert.IsNotNull(clonedLinks);


            // if the laborData.InvoiceText == laborData.Name, the clonedLaborData.InvoiceText should equal to the clonedLaborData.Name
            if (testObject.Name.Equals(testObject.InvoiceText))
            {
                Assert.AreEqual(cloned.InvoiceText, cloned.Name);
            }

            await CleanupClonedLabor(laborName);
        }

        [TestMethod]
        public async Task TestLaborDataCloneFiles()
        {
            var ctx = GetApiContext();
            await CleanupClonedLabor();

            var laborName = "TestLabor-" + new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            // create Labor
            LaborData testObject = new LaborData()
            {
                Name = laborName,
                IsActive = false,
                BID = 1,
                InvoiceText = "TestInvoiceText",
                ExpenseAccountID = ctx.GLAccount.First().ID,
                IncomeAccountID = ctx.GLAccount.First().ID
            };

            //test: expect OK
            var created = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"{apiUrl}", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);
            // upload test file
            var fooTextFileName = "foo.txt";
            var filePathFragment = $"{(int)ClassType.Labor}/{created.ID}/{fooTextFileName}";
            var someform = this.GetMultipartFormDataContent("foo", fooTextFileName);
            await AssertPostFileStatusCode(client, $"/api/dm/{Bucket.Documents}/{filePathFragment}", someform, HttpStatusCode.OK);

            var cloned = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"{apiUrl}/{created.ID}/clone", null, HttpStatusCode.OK);

            // initialize both DocumentManager
            var dm = GetDocumentManager(created.BID, created.ID, ClassType.Labor, Web.BucketRequest.Documents);
            var createdDocuments = await dm.GetDocumentsAsync();
            var cloneDm = GetDocumentManager(cloned.BID, cloned.ID, ClassType.Labor, Web.BucketRequest.Documents);
            var clonedDocuments = await cloneDm.GetDocumentsAsync();

            Assert.AreEqual(createdDocuments.Count, clonedDocuments.Count);
            Assert.AreEqual(createdDocuments[0].Name, clonedDocuments[0].Name);

            await CleanupClonedLabor();
        }

        [TestMethod]
        public async Task TestLaborDataCloneWithCustomFieldDefinition()
        {
            var ctx = GetApiContext();
            await CleanupClonedLabor();

            var laborName = "TestLabor-" + new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();


            // create Labor
            LaborData testObject = new LaborData()
            {
                Name = laborName,
                IsActive = false,
                BID = 1,
                InvoiceText = "TestInvoiceText",
                ExpenseAccountID = ctx.GLAccount.First().ID,
                IncomeAccountID = ctx.GLAccount.First().ID
            };
            var created = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"{apiUrl}", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            // create custom field definition
            var customFieldDefinition = new CustomFieldDefinition()
            {
                ClassTypeID = (int)ClassType.CustomFieldDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                AppliesToClassTypeID = (int)ClassType.Labor,
                AppliesToID = created.ID,
                Name = laborName,
                Label = "Test Custom Field Label",
                Description = "Test Custom Field Desc",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DisplayType = CustomFieldNumberDisplayType.General,
                DisplayFormatString = "",
                AllowMultipleValues = false,
                HasValidators = false
            };


            var createdCustomFieldDefinition = await EndpointTests.AssertPostStatusCode<CustomFieldDefinition>(client, "/api/cf/definition", JsonConvert.SerializeObject(customFieldDefinition), HttpStatusCode.OK);

            var values = new CustomFieldValue[1];
            values[0] = new CustomFieldValue()
            {
                ID = createdCustomFieldDefinition.ID,
                V = "TestLabor"
            };

            await EndpointTests.AssertPutStatusCode(client, $"/api/cf/value/{ClassType.Labor.ID()}/{createdCustomFieldDefinition.AppliesToID}", JsonConvert.SerializeObject(values), HttpStatusCode.OK);

            var cloned = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"{apiUrl}/{created.ID}/clone", null, HttpStatusCode.OK);
            Assert.AreEqual($"{laborName} (Clone)", cloned.Name);

            var retrievedCustomFieldDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldValue[]>(client, $"/api/cf/value/{ClassType.Labor.ID()}/{cloned.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedCustomFieldDefinitions);
            Assert.IsTrue(retrievedCustomFieldDefinitions.Count() == 1);


            await CleanupClonedLabor();
        }


        [TestCleanup]
        public void Teardown()
        {
            //you can actually do test-specific things like this
            //switch (TestContext.TestName)
            //{
            //    case "TestPaymentTermSingleEndpoints":
            //    case "TestFiltering":
            //        // do stuff
            //        break;
            //    default:
            //        break;
            //}
            this.DeleteTestRecord();
        }

        [TestMethod]
        public async Task LaborListByVariable()
        {
            var ctx = GetApiContext();
            MachineData testMachine = await GetNewMachine(DateTime.UtcNow + " TEST MACHINE DATA", -1);
            // Create 2 Machine Template
            AssemblyData machineTemplate1 = new AssemblyData()
            {
                BID = this.testBID,
                Name = DateTime.UtcNow + " TEST MACHINE TEMPLATE 1",
                AssemblyType = AssemblyType.MachineTemplate,
                MachineLayoutTypes = (MachineLayoutType)3,
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        BID = TestConstants.BID,
                        Name = "TestCheckboxVariable",
                        ElementType = AssemblyElementType.Checkbox,
                        DataType = DataType.Boolean,
                        Label = "label",
                        DefaultValue = null,
                        IsFormula = false,
                    }
                }
            };
            var newMachineTemplate1 = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, $"/api/assemblypart", JsonConvert.SerializeObject(machineTemplate1), HttpStatusCode.OK);
            Assert.IsNotNull(newMachineTemplate1);

            // test post
            testMachine.MachineTemplateID = newMachineTemplate1.ID;
            testMachine.Profiles.First().MachineTemplateID = newMachineTemplate1.ID;
            var machineData = await EndpointTests.AssertPostStatusCode<MachineData>(client, "/api/machinepart", JsonConvert.SerializeObject(testMachine), HttpStatusCode.OK);
            Assert.IsNotNull(machineData);

            LaborData testLabor = await GetNewModel();
            testLabor = await EndpointTests.AssertPostStatusCode<LaborData>(client, apiUrl, JsonConvert.SerializeObject(testLabor), HttpStatusCode.OK);
            Assert.IsNotNull(testLabor);

            AssemblyData model = new AssemblyData()
            {
                Name = "TEST_ASSEMBLY_DATA_" + DateTime.UtcNow,
                IsActive = true,
                BID = TestConstants.BID,
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        BID = TestConstants.BID,
                        Name = "TestVariable",
                        ElementType = AssemblyElementType.LinkedLabor,
                        IsConsumptionFormula = true,
                        DataType = DataType.LaborPart,
                        Label = "label",
                        DefaultValue = null,
                        ConsumptionDefaultValue = "1",
                        IsFormula = false,
                        ListValuesJSON = $"{{components: [{{\"ID\": {testLabor.ID}}}]}}"
                    }
                }
            };

            //CREATE
            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, "/api/assemblypart", JsonConvert.SerializeObject(model), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);
            var variableID = assemblyData.Variables.First().ID;
            var profileName = machineData.Profiles.First().Name;
            var simpleLaborCollection = await EndpointTests.AssertGetStatusCode<ICollection<SimpleLaborData>>(client, $"{apiUrl}/simplelist/byvariable/{variableID}?MachineID={machineData.ID}&ProfileName=\"{profileName}\"", HttpStatusCode.OK);
            Assert.IsNotNull(simpleLaborCollection);
            Assert.AreEqual(1, simpleLaborCollection.Count);
            Assert.AreEqual(testLabor.ID, simpleLaborCollection.First().ID);

            // test delete
            await EndpointTests.AssertGetStatusCode(client, $"/api/machinepart/{machineData.ID}/action/candelete", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/machinepart/{machineData.ID}", HttpStatusCode.NoContent);

            await EndpointTests.AssertDeleteStatusCode(client, $"/api/assemblypart/{assemblyData.ID}", HttpStatusCode.NoContent);
            Assert.IsFalse(ctx.AssemblyData.Any(x => x.ID == assemblyData.ID));
        }

        private void DeleteTestRecord()
        {
            string newTestLaborDataName = NewTestLaborDataName;
            Task.Run(async () =>
            {

                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                SimpleLaborData[] result = JsonConvert.DeserializeObject<SimpleLaborData[]>(responseString);
                if (result != null)
                {
                    SimpleLaborData sga = result.FirstOrDefault(x => x.DisplayName.Contains(newTestLaborDataName));
                    if (sga != null)
                    {
                        await client.DeleteAsync($"{apiUrl}/{sga.ID}");
                    }
                }
            }).Wait();
        }
    }
}
