﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class ListFilterEndpointTests
    {
        [TestMethod]
        public async Task TestListFilterEndpoints()
        {
            var client = EndpointTests.GetHttpClient();

            await EndpointTests.AssertGetStatusCode(client, $"/api/list/{ClassType.Employee}/filter/simplelist", HttpStatusCode.OK);
            await EndpointTests.AssertGetEitherStatusCode(client, $"/api/list/{ClassType.Employee}/filter/1", HttpStatusCode.OK, HttpStatusCode.NotFound);
            await EndpointTests.AssertPostStatusCode(client, $"/api/list/{ClassType.Employee}/filter/", JsonConvert.SerializeObject(""), HttpStatusCode.BadRequest);
            await EndpointTests.AssertPutStatusCode(client, $"/api/list/{ClassType.Employee}/filter/1", JsonConvert.SerializeObject(""), HttpStatusCode.BadRequest);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/list/{ClassType.Employee}/filter/1", HttpStatusCode.NoContent, HttpStatusCode.NotFound);

            await EndpointTests.AssertGetStatusCode(client, $"/api/list/{ClassType.Employee}/filter/1/subscription", HttpStatusCode.NotFound);
            await EndpointTests.AssertPostStatusCode(client, $"/api/list/{ClassType.Employee}/filter/1/subscription", JsonConvert.SerializeObject(""), HttpStatusCode.BadRequest);
            await EndpointTests.AssertPutStatusCode(client, $"/api/list/{ClassType.Employee}/filter/1/subscription", JsonConvert.SerializeObject(""), HttpStatusCode.BadRequest);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/list/{ClassType.Employee}/filter/1/subscription", HttpStatusCode.NoContent, HttpStatusCode.NotFound);

            await EndpointTests.AssertGetStatusCode(client, $"/api/list/{ClassType.Employee}/criteria", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"/api/list/{ClassType.Employee}/criteria", JsonConvert.SerializeObject(""), HttpStatusCode.NotFound);
            await EndpointTests.AssertPutStatusCode(client, $"/api/list/{ClassType.Employee}/criteria", JsonConvert.SerializeObject(""), HttpStatusCode.NotFound);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/list/{ClassType.Employee}/criteria", HttpStatusCode.NoContent, HttpStatusCode.NotFound);

            await EndpointTests.AssertPostStatusCode(client, $"/api/list/{ClassType.Employee}/columns", JsonConvert.SerializeObject(""), HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode(client, $"/api/list/{ClassType.Employee}/columns", HttpStatusCode.OK);
        }

        [TestMethod]
        [Ignore]
        public async Task TestListFilterDeleteEndpoint()
        {
            var client = EndpointTests.GetHttpClient();
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/list/{ClassType.Employee}/columns", HttpStatusCode.NoContent);
            
        }

        [TestMethod]
        [Ignore]
        public async Task TestListFilterPutEndpoint()
        {
            var client = EndpointTests.GetHttpClient();
        await EndpointTests.AssertPutStatusCode(client, $"/api/list/{ClassType.Employee}/columns", "", HttpStatusCode.OK);
     
        }
    }
}
