﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    [TestClass]
    public class OptionsEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/options";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        private class OptionTest
        {
            public int? ID { get; set; }
            public string Name { get; set; }
            public string value { get; set; }
            public short? bid { get; set; }
        }

        [TestMethod]
        public async Task TestOptionsSingleEndpoints()
        {
            //GET /api/option/HST?bid=2
            //test: provide the ID, expect OK and one option for that entity
            //await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/GLAccount.TaxName1?bid=1", HttpStatusCode.OK);

            //await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/2?associationid=1", HttpStatusCode.OK);
            var ctx = GetApiContext();
            OptionTest optNameTest = new OptionTest()
            {
                ID = -100,
                Name = "GLAccount.TaxName1",
                value = "NewOptionValueTest",
                bid = 1
            };

            OptionTest optIDTest = new OptionTest()
            {
                ID = -100,
                Name = null,
                value = "NewOptionValueTest",
                bid = 1
            };
            const string adhocOptionName = "optionIDReturnedCrazyAdhocTestOptionID";

            //PUT /api/option/1
            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{adhocOptionName}?bid={optIDTest.bid}", JsonConvert.SerializeObject(optIDTest.value), HttpStatusCode.OK);

            //PUT /api/option/HST
            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{optNameTest.Name}?bid={optNameTest.bid}", JsonConvert.SerializeObject(optNameTest.value), HttpStatusCode.OK);

            //PUT /api/option/-32768
            //test: expect BAD REQUEST because ID does not exist
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/-32768?bid={optNameTest.bid}", JsonConvert.SerializeObject(""), HttpStatusCode.NotFound);

            OptionTest optNewNameTest = new OptionTest()
            {
                ID = null,
                Name = "NewOptionNameTest",
                value = "NewOptionValueTest",
                bid = 1
            };

            //PUT /api/option/crazyNewOptionValue
            //test: expect OK, new adhoc option created
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{optNewNameTest.Name}?value={optNewNameTest.value}&bid={optNewNameTest.bid}", JsonConvert.SerializeObject(optNewNameTest), HttpStatusCode.OK);

            //DELETE /api/option/crazyNewOptionValue
            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{optNewNameTest.Name}?bid={optNameTest.bid}", HttpStatusCode.NoContent);

            //DELETE /api/option/1
            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/9241657?bid={optNameTest.bid}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestOptionsMultipleEndpoints()
        {
            //GET /api/option/?bid=2
            //test: expect OK and all options for that entity
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/?bid=1", HttpStatusCode.OK);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/?associationid=1", HttpStatusCode.OK);

            List<OptionData> opts = new List<OptionData>();
            opts.Add(new OptionData()
            {
                ID = 1,
                BID = 1,
                ClassTypeID = (int)ClassType.Option,
                CreatedDate = new DateTime(2017, 8, 20).ToUniversalTime(),
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                OptionID = 2,
                Value = "Test3",
                OptionLevel = 4
            });
            opts.Add(new OptionData()
            {
                ID = 2,
                BID = 1,
                ClassTypeID = (int)ClassType.Option,
                CreatedDate = new DateTime(2017, 8, 20).ToUniversalTime(),
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                OptionID = 1,
                Value = "0.20",
                OptionLevel = 4
            });
            opts.Add(new OptionData()
            {
                ID = 1,
                ClassTypeID = (int)ClassType.Option,
                CreatedDate = new DateTime(2017, 8, 20).ToUniversalTime(),
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                OptionID = 2,
                Value = "State",
                OptionLevel = 2,
                AssociationID = 4
            });

            //PUT /api/option w/ body of multiple options
            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/?bid=1", opts, HttpStatusCode.OK);

            List<Web.Classes.Options> optNameTest = new List<Web.Classes.Options>();
            optNameTest.Add(new Web.Classes.Options()
            {
                OptionID = null,
                OptionName = "NewOptionNameTest_1",
                Value = "NewOptionValueTest_1",
            });
            optNameTest.Add(new Web.Classes.Options()
            {
                OptionID = null,
                OptionName = "NewOptionNameTest_2",
                Value = "NewOptionValueTest_2",
            });

            //PUT /api/option w/ body of multiple options (create new)
            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/?bid=1", optNameTest, HttpStatusCode.OK);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{optNameTest[0].OptionName}?bid=1", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{optNameTest[1].OptionName}?bid=1", HttpStatusCode.NoContent);

        }

        [TestMethod]
        public async Task TestOptionsByCategoryEndpoints()
        {
            //GET /api/option/bycategory/1
            //test: expect OK and category options
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/category/1", HttpStatusCode.OK);

            //GET /api/option/category/domain
            //test: expect OK and category options
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/category/Domain", HttpStatusCode.OK);

            //DELETE /api/option/category/1
            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/category/2", HttpStatusCode.NoContent);

            //DELETE /api/option/category/Domain
            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/category/Domain", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestOptionsSectionEndpoints()
        {
            //GET /api/option/section/1
            //test: expect OK and section options
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/section/1", HttpStatusCode.OK);

            //GET /api/option/section/accounting
            //test: expect OK and section options
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/section/Accounting", HttpStatusCode.OK);
        }


        [TestMethod]
        public async Task TestGoogleOption()
        {
            var googleOption = await EndpointTests.AssertGetStatusCode<GetOptionValuesResponse>(client, $"{apiUrl}/Locator.Address.GooglePlaces.Enabled?bid=1", HttpStatusCode.OK);

            System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/google");
            bool isOkOrNoContent = response.StatusCode == HttpStatusCode.NoContent || response.StatusCode == HttpStatusCode.OK;
            Assert.IsTrue(isOkOrNoContent);
        }

    }
}
