﻿using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    public class PaymentTestHelper : CommonControllerTestClass
    {
        public System.Net.Http.HttpClient _client = EndpointTests.GetHttpClient();

        public async Task<CompanyData> CreateCompany()
        {
            var ctx = GetApiContext();
            ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var company = new CompanyData()
            {
                Name = "TestCompany",
                DefaultPONumber = "TestIdentifier",
                IsActive = true,
                BID = 1,
                RefundableCredit = 0,
                NonRefundableCredit = 0,
                LocationID = ctx.LocationData.First().ID,
            };

            return await EndpointTests.AssertPostStatusCode<CompanyData>(_client, $"/api/company/", JsonConvert.SerializeObject(company), HttpStatusCode.OK);
        }

        public async Task<PaymentMaster> CreatePaymentForCompany(int ID, int companyID, decimal amount, bool isRefundable = true)
        {
            var ctx = GetApiContext();
            ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var company = ctx.CompanyData.First(t => t.ID == companyID);
            var locationID = company.LocationID;
            var changeInCredit = amount;

            var master = new PaymentMaster()
            {
                BID = 1,
                ID = ID,
                LocationID = locationID,
                ReceivedLocationID = locationID,
                Amount = amount,
                RefBalance = isRefundable ? amount : 0,
                ChangeInRefCredit = isRefundable ? changeInCredit : (decimal?)null,
                NonRefBalance = isRefundable ? (decimal?)null : amount,
                ChangeInNonRefCredit = isRefundable ? (decimal?)null : changeInCredit,
                PaymentTransactionType = isRefundable ? (byte)PaymentTransactionType.Payment : (byte)PaymentTransactionType.Credit_Gift,
                CompanyID = companyID,
                DisplayNumber = "Test",
                PaymentApplications = new List<PaymentApplication>()
                {
                    new PaymentApplication()
                    {
                        BID = 1,
                        ID = ID,
                        PaymentType = PaymentMethodType.Visa,
                        PaymentTransactionType = isRefundable ? (byte)PaymentTransactionType.Payment : (byte)PaymentTransactionType.Credit_Gift,
                        CompanyID = companyID,
                        ApplicationGroupID = ID,
                        LocationID = locationID,
                        Amount = amount,
                        RefBalance = isRefundable ? amount : 0,
                        NonRefBalance = isRefundable ? (decimal?)null : amount,
                        OrderID = null,
                        PaymentMethodID = (int)PaymentMethodType.Visa,
                        DisplayNumber = "Test"
                    }
                }
            };
            ctx.PaymentMaster.Add(master);

            // update the company balance
            if (isRefundable)
            {
                company.RefundableCredit += amount;
            }
            else
            {
                company.NonRefundableCredit += amount;
            }
            ctx.CompanyData.Update(company);

            await ctx.SaveChangesAsync();

            return master;
        }


        public async Task<PaymentMaster> CreatePaymentForOrder(int ID, int? orderID, decimal amount, bool isRefundable = true, int companyID=0)
        {
            var ctx = GetApiContext();
            ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            OrderData order = null;
            if (orderID!=null) order = ctx.OrderData.First(t => t.ID == orderID);
            CompanyData company = null;
            if (companyID!=0) company = ctx.CompanyData.First(t => t.ID == companyID);
            else company = ctx.CompanyData.First(t => t.ID == order.CompanyID);
            var locationID = company.LocationID;
            var changeInCredit = amount;

            var master = new PaymentMaster()
            {
                BID = 1,
                ID = ID,
                LocationID = locationID,
                ReceivedLocationID = locationID,
                Amount = amount,
                RefBalance = isRefundable ? amount : 0,
                ChangeInRefCredit = isRefundable ? (orderID == null) ? changeInCredit : 0m : (decimal?)null,
                NonRefBalance = isRefundable ? (decimal?)null : amount,
                ChangeInNonRefCredit = isRefundable ? (decimal?)null : (orderID == null) ? changeInCredit : 0m,
                PaymentTransactionType = isRefundable ? (byte)PaymentTransactionType.Payment : (byte)PaymentTransactionType.Credit_Gift,
                CompanyID = company.ID,
                DisplayNumber = "Test",
                PaymentApplications = new List<PaymentApplication>()
                {
                    new PaymentApplication()
                    {
                        BID = 1,
                        ID = ID,
                        PaymentType = isRefundable ? PaymentMethodType.Cash : PaymentMethodType.NonRefundableCredit,
                        PaymentMethodID = isRefundable ? (int)PaymentMethodType.Cash : (int)PaymentMethodType.NonRefundableCredit,
                        PaymentTransactionType = isRefundable ? (byte)PaymentTransactionType.Payment : (byte)PaymentTransactionType.Credit_Gift,
                        CompanyID = company.ID,
                        ApplicationGroupID = ID,
                        LocationID = locationID,
                        Amount = amount,
                        RefBalance = isRefundable ? amount : 0,
                        NonRefBalance = isRefundable ? (decimal?)null : amount,
                        OrderID = orderID,
                        DisplayNumber = "Test"
                    }
                }
            };
            ctx.PaymentMaster.Add(master);

            // update the company balance
            if (isRefundable && (orderID==null))
            {
                company.RefundableCredit += amount;
            }
            else
            {
                company.NonRefundableCredit += amount;
            }
            ctx.CompanyData.Update(company);

            await ctx.SaveChangesAsync();

            return master;
        }

        //public async Task<PaymentMaster> PayOrders(int? orderID, decimal amount, bool isRefundable = true)
        //{
        //}

        /// <summary>
        /// Creates a payment for multiple orders distributing amount evenly across all orders
        /// </summary>
        public async Task<PaymentMaster> CreatePaymentForMultipleOrders(int ID, int[] orderIDs, int companyID, decimal amount, bool isRefundable = true)
        {
            var ctx = GetApiContext();
            ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var company = ctx.CompanyData.First(t => t.ID == companyID);
            var locationID = company.LocationID;
            var changeInCredit = amount;

            var master = new PaymentMaster()
            {
                BID = 1,
                ID = ID,
                LocationID = locationID,
                ReceivedLocationID = locationID,
                Amount = amount,
                RefBalance = isRefundable ? amount : 0,
                ChangeInRefCredit = isRefundable ? changeInCredit : (decimal?)null,
                NonRefBalance = isRefundable ? (decimal?)null : amount,
                ChangeInNonRefCredit = isRefundable ? (decimal?)null : changeInCredit,
                PaymentTransactionType = isRefundable ? (byte)PaymentTransactionType.Payment : (byte)PaymentTransactionType.Credit_Gift,
                CompanyID = company.ID,
                DisplayNumber = "Test",
                PaymentApplications = new List<PaymentApplication>() { }
            };
            foreach(var orderID in orderIDs)
            {
                var appAmount = amount / orderIDs.Count();
                var itemIndex = Array.FindIndex(orderIDs, t => t == orderID);
                master.PaymentApplications.Add(
                    new PaymentApplication()
                    {
                        BID = 1,
                        ID = ID - (itemIndex + 1),
                        PaymentType = isRefundable ? PaymentMethodType.Cash : PaymentMethodType.NonRefundableCredit,
                        PaymentMethodID = isRefundable ? (int)PaymentMethodType.Cash : (int)PaymentMethodType.NonRefundableCredit,
                        PaymentTransactionType = isRefundable ? (byte)PaymentTransactionType.Payment : (byte)PaymentTransactionType.Credit_Gift,
                        CompanyID = company.ID,
                        ApplicationGroupID = ID,
                        LocationID = locationID,
                        Amount = appAmount,
                        RefBalance = isRefundable ? appAmount : 0,
                        NonRefBalance = isRefundable ? (decimal?)null : appAmount,
                        OrderID = orderID,
                        DisplayNumber = "Test"
                    });
            }

            ctx.PaymentMaster.Add(master);

            // update the company balance
            if (isRefundable)
            {
                company.RefundableCredit += amount;
            }
            else
            {
                company.NonRefundableCredit += amount;
            }
            ctx.CompanyData.Update(company);

            await ctx.SaveChangesAsync();

            return master;
        }

        public async Task<OrderData> CreateOrder(int companyID, decimal price, decimal amountPaid = 0)
        {
            var ctx = GetApiContext();
            ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var company = ctx.CompanyData.First(t => t.ID == companyID);
            var locationID = company.LocationID;
            var taxGroupID = ctx.TaxGroup.First().ID;
            var order = Utils.GetTestOrderData();
            var incomeAccounts = ctx.GLAccount.Where(account => account.GLAccountType == 40).ToList(); //Income
            var orderItem = Utils.GetTestOrderItemData(order);
            var orderItemComponent = Utils.GetTestOrderItemComponentData(orderItem, incomeAccounts.First());
            orderItemComponent.PriceUnit = 100m;
            orderItemComponent.CostUnit = null;
            orderItem.CostTotal = 100m;
            orderItem.Components = new List<OrderItemComponent>
            {
                orderItemComponent
            };
            order.Items = new List<OrderItemData>
            {
                orderItem
            };
            order.CompanyID = companyID;
            order.PickupLocationID = locationID;
            order.TaxGroupID = taxGroupID;
            order.ProductionLocationID = locationID;
            order.PriceProductTotal = price;
            order.InvoiceNumber = -99;
            order.PaymentPaid = amountPaid;
            _client = EndpointTests.GetHttpClient(employeeID: ctx.EmployeeData.Where(x => x.BID == 1 && x.ID > 0).First().ID);
                           
            return await EndpointTests.AssertPostStatusCode<OrderData>(_client, $"/api/order/", JsonConvert.SerializeObject(order), HttpStatusCode.OK);
        }


        public async Task CleanUpTestData()
        {
            var ctx = GetApiContext();
            ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var testCompanies = ctx.CompanyData.Where(t => t.DefaultPONumber == "TestIdentifier").ToList();
            var testCompanyIDs = testCompanies.Select(t => t.ID).ToList();
            var paymentMasterCondition = ctx.PaymentMaster.Where(t => t.BID == 1 && (t.ID == -99 || t.DisplayNumber == "Test" || testCompanyIDs.Contains(t.CompanyID)));
            var paymentMasters = paymentMasterCondition.ToList();

            foreach (var master in paymentMasters)
            {
                var applicationCondition = ctx.PaymentApplication.Where(t => t.MasterID == master.ID);
                ctx.PaymentApplication.RemoveRange(applicationCondition);
                ctx.PaymentMaster.Remove(master);
            }

            await ctx.SaveChangesAsync();

            // cleanup order
            var order = ctx.OrderData.FirstOrDefault(t => t.BID == 1 && t.InvoiceNumber == -99);
            if (order != null)
            {
                await EndpointTests.AssertDeleteStatusCode(_client, $"/api/order/{order.ID}", HttpStatusCode.NoContent);
            }

            // cleanup company
            foreach (var company in testCompanies)
            {
                await RemoveOrdersFromCompany(company.ID);
                ctx.CompanyData.Remove(company);
            }
            await ctx.SaveChangesAsync();

            var paymentMethodCondition = ctx.PaymentMethod.Where(t => t.BID == 1 && t.ID == -99).ToList();
            ctx.PaymentMethod.RemoveRange(paymentMethodCondition);
            await ctx.SaveChangesAsync();
        }

        private async Task RemoveOrdersFromCompany(int companyID)
        {
            var ctx = GetApiContext();
            var orders = ctx.TransactionHeaderData.Where(t => t.CompanyID == companyID).ToList();
            foreach(var order in orders)
            {
                ctx.OrderKeyDate.RemoveRange(ctx.OrderKeyDate.Where(x => x.OrderID == order.ID));
                ctx.Remove(order);
            }
            await ctx.SaveChangesAsync();
        }
    }
}
