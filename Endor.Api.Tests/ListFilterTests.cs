﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AssertX = Xunit.Assert;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using Microsoft.AspNetCore.Mvc;

namespace Endor.Api.Tests
{
    [TestClass]
    public class ListFilterTests : CommonControllerTestClass
    {
        [TestMethod]
        [Ignore]
        public async Task Columns()
        {
            var testController = GetColumnsController();
            AssertX.IsType<OkObjectResult>(await testController.Read((int)ClassType.Employee));
            AssertX.IsType<BadRequestObjectResult>(await testController.Update((int)ClassType.Employee, new UserListColumn()
            {
                TargetClassTypeID = (int)ClassType.Business, //mismatched CTID
                Field = "someField",
                IsFrozen = false,
                IsExpandable = false,
                IsVisible = true,
                SortIndex = 50
            }));
            AssertX.IsType<OkObjectResult>(await testController.Update((int)ClassType.Employee, new UserListColumn()
            {
                TargetClassTypeID = (int)ClassType.Employee,
                Field = "someField",
                IsFrozen = false,
                IsExpandable = false,
                IsVisible = true,
                SortIndex = 50
            }));
            var created = AssertX.IsType<OkObjectResult>(await testController.Delete((int)ClassType.Employee));
        }

        [TestMethod]
        public async Task Criteria()
        {
            var testController = GetCriteriaController();
            AssertX.IsType<OkObjectResult>(await testController.Read((int)ClassType.Employee));
        }

        [TestMethod]
        [Ignore]
        public async Task Filters()
        {
            var testController = GetFilterController();
            AssertX.IsType<UnauthorizedResult>(await testController.Create((int)ClassType.Employee, new SystemListFilter()
            {
                Name = "test filter",
                TargetClassTypeID = (int)ClassType.Employee,
                IsActive = true,
                Hint = "",
                SortIndex = 50
            }));
            AssertX.IsType<BadRequestObjectResult>(await testController.Create((int)ClassType.Employee, new SystemListFilter()
            {
                Name = "test filter",
                TargetClassTypeID = (int)ClassType.Business, //mismatched CTID
                IsActive = true,
                Hint = "",
                SortIndex = 50
            }));
            var created = AssertX.IsType<OkObjectResult>(await testController.Create((int)ClassType.Employee, new SystemListFilter()
            {
                Name = "test filter",
                TargetClassTypeID = (int)ClassType.Employee,
                IsActive = true,
                Hint = "",
                SortIndex = 50
            }));
        }
    }
}
