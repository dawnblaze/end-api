﻿using Endor.Api.Web.Classes;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    /*
    [TestClass]
    public class DomainEmailEndpointTests
    {
        public const string apiUrl = "/api/domain/email";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewDomainEmailDomain, SecondDomainEmailDomain;
        public TestContext TestContext { get; set; }


        [TestInitialize]
        public void Initialize()
        {
            NewDomainEmailDomain = GetTestDomainName();
            SecondDomainEmailDomain = GetTestDomainName();
        }

        private string GetTestDomainName()
        {
            var tickString = DateTime.UtcNow.Ticks.ToString();
            return "unitest." + tickString.Substring(tickString.Length - 7, 4) + "." + tickString.Substring(tickString.Length - 3);
        }

        [TestMethod]
        public async Task DomainEmailCRUD()
        {
            #region CREATE
            var newDomainEmail = Utils.GetDomainEmail();
            newDomainEmail.DomainName = NewDomainEmailDomain;
            var domainEmail = await EndpointTests.AssertPostStatusCode<DomainEmail>(client, $"{apiUrl}", JsonConvert.SerializeObject(newDomainEmail), HttpStatusCode.OK);
            #endregion
            #region RETRIEVE

            await EndpointTests.AssertGetStatusCode<DomainEmail>(client, $"{apiUrl}/{domainEmail.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode<DomainEmail[]>(client, $"{apiUrl}/", HttpStatusCode.OK);

            // GET .../simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleDomainEmail[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleDomainEmail sga in simpleList)
            {
                if (sga.DisplayName == domainEmail.DomainName)
                {
                    domainEmail.ID = sga.ID;
                }
            }
            #endregion

            #region UPDATE

            domainEmail.DomainName = "foo.bar";
            await EndpointTests.AssertPutStatusCode<DomainEmail>(client, $"{apiUrl}/{domainEmail.ID}", JsonConvert.SerializeObject(domainEmail), HttpStatusCode.OK);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{domainEmail.ID}", HttpStatusCode.NoContent);

            #endregion
        }

        [TestMethod]
        public async Task GetNewlyAddedSMTPConfiguration()
        {
            var result = await EndpointTests.AssertGetStatusCode<SystemEmailSMTPConfigurationType[]>(client, "api/domain/email/smtpconfigurationtype", HttpStatusCode.OK);
            Assert.IsTrue(result.Length > 1);
            var found = result.Where(x => x.ID == 4 || x.ID == 5).ToList();
            Assert.AreEqual(2, found.Count);
        }

        [TestMethod]
        public async Task DomainEmailActions()
        {
            #region CREATE
            var newDomainEmail = Utils.GetDomainEmail();
            newDomainEmail.DomainName = NewDomainEmailDomain;
            var domainEmail = await EndpointTests.AssertPostStatusCode<DomainEmail>(client, $"{apiUrl}", JsonConvert.SerializeObject(newDomainEmail), HttpStatusCode.OK);

            var secondDomainEmail = Utils.GetDomainEmail();
            secondDomainEmail.DomainName = SecondDomainEmailDomain;
            var otherDomainEmail = await EndpointTests.AssertPostStatusCode<DomainEmail>(client, $"{apiUrl}", JsonConvert.SerializeObject(secondDomainEmail), HttpStatusCode.OK);
            #endregion

            #region SETACTIVE

            await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{domainEmail.ID}/action/setactive", HttpStatusCode.OK);
            #endregion

            #region SETINACTIVE

            await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{domainEmail.ID}/action/setinactive", HttpStatusCode.OK);

            #endregion

            #region CANDELETE

            await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{domainEmail.ID}/action/candelete", HttpStatusCode.OK);

            #endregion

            var validLocations = await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "api/location/simplelist", HttpStatusCode.OK);
            Assert.IsTrue(validLocations.Length > 1);
            var validLocation = validLocations[0];
            Assert.IsNotNull(validLocation);
            byte locationID = validLocation.ID;

            #region LINK

            await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{domainEmail.ID}/action/linklocation/{locationID}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{otherDomainEmail.ID}/action/linklocation/{validLocations[1].ID}", HttpStatusCode.OK);

            #endregion

            #region LOCATIONFILTER

            var domainsInLocation = await EndpointTests.AssertGetStatusCode<DomainEmail[]>(client, $"{apiUrl}/?LocationID={locationID}", HttpStatusCode.OK);
            Assert.IsNotNull(domainsInLocation.FirstOrDefault(x => x.ID == domainEmail.ID));
            Assert.IsNull(domainsInLocation.FirstOrDefault(x => x.ID == otherDomainEmail.ID));

            #endregion

            #region UNLINK

            await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{domainEmail.ID}/action/unlinklocation/{locationID}", HttpStatusCode.OK);

            #endregion

            #region TEST

            await EndpointTests.AssertGetStatusCode<SystemEmailSMTPConfigurationType[]>(client, $"{apiUrl}/smtpconfigurationtype", HttpStatusCode.OK);

            #endregion
        }
        [TestCleanup]
        public void Teardown()
        {
            //you can actually do test-specific things like this
            //switch (TestContext.TestName)
            //{
            //    case "TestPaymentTermSingleEndpoints":
            //    case "TestFiltering":
            //        // do stuff
            //        break;
            //    default:
            //        break;
            //}
            this.DeleteTestRecord();
        }

        private void DeleteTestRecord()
        {
            string testOne = NewDomainEmailDomain;
            string testTwo= SecondDomainEmailDomain;
            //Task.Run(async () =>
            //{
            //    System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
            //    string responseString = await response.Content.ReadAsStringAsync();
            //    SimpleMaterialCategory[] result = JsonConvert.DeserializeObject<DomainEmail[]>(responseString);
            //    if (result != null)
            //    {
            //        SimpleMaterialCategory sga = result.FirstOrDefault(x => x.DisplayName.Contains(testOne));
            //        if (sga != null)
            //        {
            //            await client.DeleteAsync($"{apiUrl}/{sga.ID}?Force=true");
            //        }
            //    }
            //    response = await client.GetAsync($"api/materialpart/SimpleList");
            //    responseString = await response.Content.ReadAsStringAsync();
            //    SimpleMaterialData[] dataResult = JsonConvert.DeserializeObject<SimpleMaterialData[]>(responseString);
            //    if (result != null)
            //    {
            //        SimpleMaterialData sga = dataResult.FirstOrDefault(x => x.DisplayName.Contains(newTestMaterialDataName));
            //        if (sga != null)
            //        {
            //            await client.DeleteAsync($"api/materialpart/{sga.ID}");
            //        }
            //    }
            //}).Wait();
        }
    }*/
}
