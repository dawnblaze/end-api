﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

namespace Endor.Api.Tests
{
    [TestClass]
    public class LaborCategoryEndpointTests
    {
        public const string apiUrl = "/api/laborcategory";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestLaborCategoryName;
        public TestContext TestContext { get; set; }

        private LaborCategory GetNewModel()
        {
            return new LaborCategory()
            { 
                Name = this.NewTestLaborCategoryName,
                IsActive = true,
                BID = 1
            };
        }

        [TestInitialize]
        public void Initialize()
        {
            this.NewTestLaborCategoryName = DateTime.UtcNow + " TEST LABORCATEGORY DATA";
        }

        [TestMethod]
        public async Task GetLaborCategoryTest()
        {
            await EndpointTests.AssertGetStatusCode<List<LaborCategory>>(client, apiUrl, HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestLaborCategoryCanDelete()
        {
            LaborCategory itemTest = GetNewModel();

            //test: expect OK
            var laborCategory = await EndpointTests.AssertPostStatusCode<LaborCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(laborCategory);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{laborCategory.ID}/action/candelete", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{laborCategory.ID}", HttpStatusCode.NoContent);
        }

    }
}
