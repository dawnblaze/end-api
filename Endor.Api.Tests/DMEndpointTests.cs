﻿using Acheve.AspNetCore.TestHost.Security;
using Endor.Api.Web;
using Endor.Tenant;
using Endor.DocumentStorage.Models;
using Endor.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Net.Http.Headers;

namespace Endor.Api.Tests
{
    [TestClass]
    public class DocumentManagementEndpointTests
    {
        private TestServer _testServer;
        
        [TestMethod]
        public async Task TestReadPersitentDocManEndpoints()
        {
            HttpClient client = StartServerAndGetClient();
            
            //test GET bucket+ctd+id
            await AssertGetStatusCode(client, $"/api/dm/{Bucket.Documents}/{(int)ClassType.Employee}/1", HttpStatusCode.OK);
            await AssertGetStatusCode(client, $"/api/dm/{Bucket.Reports}/{(int)ClassType.Employee}/1", HttpStatusCode.OK);
            await AssertGetEitherStatusCode(client, $"/api/dm/{Bucket.Data}/{(int)ClassType.Employee}/1", HttpStatusCode.NotFound, HttpStatusCode.BadRequest);
            await AssertGetEitherStatusCode(client, $"/api/dm/{Bucket.Documents}/{(int)ClassType.Employee}", HttpStatusCode.NotFound, HttpStatusCode.BadRequest);
            await AssertGetStatusCode(client, $"/api/dm/{Bucket.Documents}/{(int)ClassType.Employee}/1?designation=foo", HttpStatusCode.OK);

            ////test invalid CTID
            await AssertEmptyGetResponse(client, $"/api/dm/{Bucket.Documents}/9813214/1");
        }

        [TestMethod]
        public async Task TestWritePersistentDocManEndpoints()
        {
            HttpClient client = StartServerAndGetClient();
            string fooTextFileName = "foo.txt";
            string filePathFragment = $"{(int)ClassType.Employee}/1/someClassFolder/{fooTextFileName}";
            string pathFragment = $"{(int)ClassType.Employee}/1/someClassFolder";
            string noneInstancePath = $"{(int)ClassType.Employee}/someClassFolder/{fooTextFileName}";
            var someform = this.GetMultipartFormDataContent("foo",fooTextFileName);

            string invalidFileName = "_TemplateFiles.json";
            string invalidFilePathFragment = $"{(int)ClassType.Employee}/someClassFolder/{invalidFileName}";

            //test can post/put/delete to docs bucket
            await AssertPostFileStatusCode(client, $"/api/dm/{Bucket.Documents}/{filePathFragment}", someform, HttpStatusCode.OK);
            await AssertGetStatusCode(client, $"/api/dm/{Bucket.Documents}/{pathFragment}", HttpStatusCode.OK);
            await AssertPutFormStatusCode(client, $"/api/dm/{Bucket.Documents}/{filePathFragment}", "replaced string foo", HttpStatusCode.OK);
            await AssertDeleteStatusCode(client, $"/api/dm/{Bucket.Documents}/{filePathFragment}", HttpStatusCode.OK);

            //test can post/put/delete to report bucket --EDIT > post was used in the api to edit files, replace put with post
            await AssertPostFormStatusCode(client, $"/api/dm/{Bucket.Reports}/{noneInstancePath}", "initial foo string", HttpStatusCode.OK);
            await AssertGetStatusCode(client, $"/api/dm/{Bucket.Reports}/{pathFragment}", HttpStatusCode.OK);
            await AssertPutFormStatusCode(client, $"/api/dm/{Bucket.Reports}/{filePathFragment}", "replaced string foo", HttpStatusCode.OK);
            await AssertDeleteStatusCode(client, $"/api/dm/{Bucket.Reports}/{filePathFragment}", HttpStatusCode.OK);

            //test can post/put/delete to data bucket
            await AssertPostFileStatusCode(client, $"/api/dm/{Bucket.Data}/{filePathFragment}", someform, HttpStatusCode.OK);
            await AssertGetStatusCode(client, $"/api/dm/{Bucket.Data}/{pathFragment}", HttpStatusCode.OK);
            await AssertPutFormStatusCode(client, $"/api/dm/{Bucket.Data}/{filePathFragment}", "replaced string foo", HttpStatusCode.OK);
            await AssertDeleteStatusCode(client, $"/api/dm/{Bucket.Data}/{filePathFragment}", HttpStatusCode.OK);

            // test post/put invalid filename
            await AssertPostFileStatusCode(client, $"/api/dm/{Bucket.Data}/{invalidFilePathFragment}", someform, HttpStatusCode.BadRequest);
            await AssertPutFormStatusCode(client, $"/api/dm/{Bucket.Data}/{invalidFilePathFragment}", "replaced string foo", HttpStatusCode.BadRequest);
            await AssertDeleteStatusCode(client, $"/api/dm/{Bucket.Data}/{invalidFilePathFragment}", HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public async Task TestTempDocManEndpoints()
        {
            HttpClient client = StartServerAndGetClient();
            string fooTextFileName = "foo.txt";
            string guid = Guid.NewGuid().ToString();
            string filePathFragment = $"{guid}/{fooTextFileName}";
            string pathFragment = $"{guid}";

            //test can post to docs bucket
            await AssertPostFormStatusCode(client,   $"/api/dm/{Bucket.Documents}/temp/{filePathFragment}", "initial foo string", HttpStatusCode.OK);
            //test can post to report bucket
            await AssertPostFormStatusCode(client, $"/api/dm/{Bucket.Reports}/temp/{filePathFragment}", "report foo string", HttpStatusCode.OK);

            //I'm commenting out the test below because of this comment note found in DMController.CRUD.cs

            //===START DMController.CRUD.cs comment note==//
            //commented out, not sure why this condition exists
            //as per spec https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/845578283/MessageBody+Object
            //MessageBody needs access to temp Data bucket
            // if (requestModel.IsTemporary && requestModel.Bucket == BucketRequest.Data)
            // {
            //     return BadRequest("Invalid Bucket");
            // }

            //===END DMController.CRUD.cs comment note==//

            //test data bucket is not postable
            //await AssertPostFormStatusCode(client, $"/api/dm/{Bucket.Data}/temp/{filePathFragment}", "report foo string", HttpStatusCode.BadRequest, HttpStatusCode.NotFound);

            await AssertGetStatusCode(client, $"/api/dm/{Bucket.Documents}/temp/{guid}", HttpStatusCode.OK);
            await AssertPutFormStatusCode(client, $"/api/dm/{Bucket.Documents}/temp/{filePathFragment}", "replaced foo string", HttpStatusCode.OK);
            await AssertDeleteStatusCode(client, $"/api/dm/{Bucket.Documents}/temp/{filePathFragment}", HttpStatusCode.OK);
            //test if blob deleted;
            await AssertEmptyGetResponse(client, $"/api/dm/{Bucket.Documents}/temp/{pathFragment}");
        }

        [TestMethod]
        public async Task TestTemplateDocManEndpoints()
        {
            HttpClient client = StartServerAndGetClient();
            string fooTextFileName = "foo.txt";

            //test can post/put to docs bucket
            await AssertGetStatusCode(client, $"/api/dm/{Bucket.Documents}/{(int)ClassType.Employee}/shared/template/", HttpStatusCode.OK);
            await AssertPostFormStatusCode(client, $"/api/dm/{Bucket.Documents}/{(int)ClassType.Employee}/shared/template/{fooTextFileName}", "initial foo string", HttpStatusCode.OK);
            await AssertPutFormStatusCode(client, $"/api/dm/{Bucket.Documents}/{(int)ClassType.Employee}/shared/template/{fooTextFileName}", "replaced foo string", HttpStatusCode.OK);

            //test cannot post/put to report bucket
            await AssertGetStatusCode(client, $"/api/dm/{Bucket.Reports}/{(int)ClassType.Employee}/shared/template/", HttpStatusCode.BadRequest);
            await AssertPostFormStatusCode(client, $"/api/dm/{Bucket.Reports}/{(int)ClassType.Employee}/template/{fooTextFileName}", "report foo string", HttpStatusCode.BadRequest, HttpStatusCode.NotFound);
            await AssertPutFormStatusCode(client, $"/api/dm/{Bucket.Reports}/{(int)ClassType.Employee}/template/{fooTextFileName}", "report replaced string", HttpStatusCode.BadRequest, HttpStatusCode.NotFound);

            //test can post/put to data bucket
            await AssertGetStatusCode(client, $"/api/dm/{Bucket.Data}/{(int)ClassType.Employee}/shared/template/", HttpStatusCode.OK);
            await AssertPostFormStatusCode(client, $"/api/dm/{Bucket.Data}/{(int)ClassType.Employee}/shared/template/{fooTextFileName}", "data foo string", HttpStatusCode.OK);
            await AssertPutFormStatusCode(client, $"/api/dm/{Bucket.Data}/{(int)ClassType.Employee}/shared/template/{fooTextFileName}", "data replaced string", HttpStatusCode.OK);

            await AssertDeleteStatusCode(client, $"/api/dm/{Bucket.Data}/{(int)ClassType.Employee}/shared/template/{fooTextFileName}", HttpStatusCode.OK);
            await AssertDeleteStatusCode(client, $"/api/dm/{Bucket.Documents}/{(int)ClassType.Employee}/shared/template/{fooTextFileName}", HttpStatusCode.OK);
            //this is legal on DELETE but not POST/PUT
            await AssertDeleteStatusCode(client, $"/api/dm/{Bucket.Reports}/{(int)ClassType.Employee}/shared/template/{fooTextFileName}", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestStaticDocManEndpoints()
        {
            HttpClient client = StartServerAndGetClient();
            string fooTextFileName = "foo.txt";

            //test can post to report bucket
            await AssertPostFormStatusCode(client, $"/api/dm/{Bucket.Reports}/{(int)ClassType.Employee}/shared/static/{fooTextFileName}", "report foo string", HttpStatusCode.OK);
            await AssertPutFormStatusCode(client, $"/api/dm/{Bucket.Reports}/{(int)ClassType.Employee}/shared/static/{fooTextFileName}", "report replaced string", HttpStatusCode.OK);
            
            //test can post to data bucket
            await AssertPostFormStatusCode(client, $"/api/dm/{Bucket.Data}/{(int)ClassType.Employee}/shared/static/{fooTextFileName}", "data foo string", HttpStatusCode.OK);
            await AssertPutFormStatusCode(client, $"/api/dm/{Bucket.Data}/{(int)ClassType.Employee}/shared/static/{fooTextFileName}", "data replaced string", HttpStatusCode.OK);
            
            //test cannot post to docs bucket
            await AssertPostFormStatusCode(client, $"/api/dm/{Bucket.Documents}/{(int)ClassType.Employee}/shared/static/{fooTextFileName}", "initial foo string", HttpStatusCode.BadRequest, HttpStatusCode.NotFound);
            await AssertPutFormStatusCode(client, $"/api/dm/{Bucket.Documents}/{(int)ClassType.Employee}/shared/static/{fooTextFileName}", "will not work", HttpStatusCode.BadRequest, HttpStatusCode.NotFound);
            await AssertDeleteStatusCode(client, $"/api/dm/{Bucket.Reports}/{(int)ClassType.Employee}/shared/template/{fooTextFileName}", HttpStatusCode.OK);
            await AssertDeleteStatusCode(client, $"/api/dm/{Bucket.Data}/{(int)ClassType.Employee}/shared/template/{fooTextFileName}", HttpStatusCode.OK);
            
            //this is legal on DELETE but not POST/PUT
            await AssertDeleteStatusCode(client, $"/api/dm/{Bucket.Documents}/{(int)ClassType.Employee}/shared/template/{fooTextFileName}", HttpStatusCode.OK);
        }

        [TestMethod]
        [Ignore]
        public async Task TestZipAllDocManEndpoints()
        {
            HttpClient client = StartServerAndGetClient();

            await AssertGetStatusCode(client, $"api/dm/zip/all", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestActionDocManEndpoints()
        {
            HttpClient client = StartServerAndGetClient();
            Bucket bucket = Bucket.Documents;
            string guid = Guid.NewGuid().ToString();
            string invalidGuid = "test-invalid-guid";
            string rawFileArray = "[\"foo.txt\"]";
            int ctid = (int)ClassType.Employee;
            int id = 1;
            //create test cases
            await AssertPostFormStatusCode(client, $"api/dm/{bucket}/{ctid}/{id}/foo.txt", "hello world", HttpStatusCode.OK);
            await AssertPostFormStatusCode(client, $"api/dm/{bucket}/{ctid}/shared/newfolder/foo.txt", "hello world", HttpStatusCode.OK);
            await AssertPostFormStatusCode(client, $"api/dm/{bucket}/temp/{guid}/foo.txt", "hello world", HttpStatusCode.OK);

            //TEST COPY
            await AssertPostStatusCode(client, $"api/dm/copy/{bucket}/{ctid}/shared/newfolder?destination=newfoldercopy", "", HttpStatusCode.OK);//copy 

            //TEST MOVE
            await AssertRequestWithContentType(HttpMethod.Post, "application/json", client, $"api/dm/move/{bucket}/{ctid}/shared/newfoldercopy?destination=newfoldermove", rawFileArray, HttpStatusCode.OK);

            //TEST HttpDelete /api/DM/{bucket}/{ctid}/shared/{classFolder}
            await AssertDeleteStatusCode(client, $"api/dm/{bucket}/{ctid}/shared/newfoldermove/foo.txt", HttpStatusCode.OK);//delete 

            //-----------------TEST INITIALIZE----------------------------------------------------
            //[HttpPost("initialize/all/{ctid:int}/{id:int}")]
            //[HttpPost("initialize/all/temp/{guid:guid}/as/{ctid:int}")]
            await AssertPostStatusCode(client, $"api/dm/copyfromtemplate/{ctid}/{id}", "", HttpStatusCode.OK);
            await AssertPostStatusCode(client, $"api/dm/copyfromtemplate/temp/{guid}/as/{ctid}", "", HttpStatusCode.OK);
            await AssertPostStatusCode(client, $"api/dm/copyfromtemplate/temp/{invalidGuid}/as/{ctid}", "", HttpStatusCode.NotFound);

            //-----------------TEST PERSIST----------------------------------------------------
            //[HttpPost("persist/all/temp/{guid:guid}/as/{ctid:int}/{id:int}")]
            await AssertPostStatusCode(client, $"api/dm/persist/all/temp/{guid}/as/{ctid}/{id}", "", HttpStatusCode.OK);
            await AssertPostStatusCode(client, $"api/dm/persist/all/temp/{invalidGuid}/as/{ctid}/{id}", "", HttpStatusCode.NotFound);

            //-----------------TEST DUPLICATE----------------------------------------------------
            //[HttpPost("duplicate/" + bucket_ctid_id_route)]
            //[HttpPost("duplicate/" + bucket_ctid_folder_route)]
            //[HttpPost("duplicate/" + bucket_temp_guid_route)]
            await AssertRequestWithContentType(HttpMethod.Post, "application/json", client, $"api/dm/duplicate/{bucket}/{ctid}/{id}/", rawFileArray, HttpStatusCode.OK);
            await AssertRequestWithContentType(HttpMethod.Post, "application/json", client, $"api/dm/duplicate/{bucket}/{ctid}/shared/newfolder/", rawFileArray, HttpStatusCode.OK);
            await AssertRequestWithContentType(HttpMethod.Post, "application/json", client, $"api/dm/duplicate/{bucket}/temp/{guid}/", rawFileArray, HttpStatusCode.OK);
            //duplicate non-existing file
            await AssertRequestWithContentType(HttpMethod.Post, "application/json", client, $"api/dm/duplicate/{bucket}/{ctid}/shared/th!s-f0ld3r-d03s-n0t-3xist/", rawFileArray, HttpStatusCode.NotFound, HttpStatusCode.BadRequest);

            //-----------------TEST ZIP----------------------------------------------------
            //[HttpPost("zip/" + bucket_ctid_id_pathAndFilename_route)]
            //[HttpPost("zip/" + bucket_ctid_folder_pathAndFilename_route)]
            //[HttpPost("zip/" + bucket_temp_guid_pathAndFilename_route)]
            await AssertRequestWithContentType(HttpMethod.Post, "application/json", client, $"api/dm/zip/{bucket}/{ctid}/{id}/", rawFileArray, HttpStatusCode.OK);
            await AssertRequestWithContentType(HttpMethod.Post, "application/json", client, $"api/dm/zip/{bucket}/{ctid}/shared/newfolder/", rawFileArray, HttpStatusCode.OK);
            await AssertRequestWithContentType(HttpMethod.Post, "application/json", client, $"api/dm/zip/{bucket}/temp/{guid}/", rawFileArray, HttpStatusCode.OK);
            //zip non-existing file
            await AssertRequestWithContentType(HttpMethod.Post, "application/json", client, $"api/dm/zip/{bucket}/{ctid}/shared/th!s-f0ld3r-d03s-n0t-3xist/", rawFileArray, HttpStatusCode.NotFound, HttpStatusCode.OK);
            
            //-----------------TEST DELETE----------------------------------------------------
            //[HttpPost("delete/" + bucket_ctid_id_route)]
            //[HttpPost("delete/" + bucket_ctid_folder_route)]
            //[HttpPost("delete/" + bucket_temp_guid_route)]
            await AssertRequestWithContentType(HttpMethod.Post, "application/json", client, $"api/dm/delete/{bucket}/{ctid}/{id}/", rawFileArray, HttpStatusCode.OK);
            await AssertRequestWithContentType(HttpMethod.Post, "application/json", client, $"api/dm/delete/{bucket}/{ctid}/shared/newfolder/", rawFileArray, HttpStatusCode.OK);
            await AssertRequestWithContentType(HttpMethod.Post, "application/json", client, $"api/dm/delete/{bucket}/temp/{guid}/", rawFileArray, HttpStatusCode.OK);
            //delete non-existing file
            await AssertRequestWithContentType(HttpMethod.Post, "application/json", client, $"api/dm/delete/{bucket}/{ctid}/shared/th!s-f0ld3r-d03s-n0t-3xist/", rawFileArray, HttpStatusCode.OK);

            //-----------------TEST DELETE ALL----------------------------------------------------
            //[HttpPost("delete/{ctid:int}/{id:int}")]
            await AssertPostStatusCode(client, $"api/dm/delete/{ctid}/{id}", "", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestTokenDocManEndpoints()
        {
            string checkStr = "foo string";
            HttpClient client = StartServerAndGetClient();
            string fooTextFileName = "foo-token.txt";
            string filePathFragment = $"{Bucket.Documents}/{(int)ClassType.Employee}/1/{fooTextFileName}";

            await AssertPostFormStatusCode(client, $"/api/dm/{filePathFragment}", checkStr, HttpStatusCode.OK);
            await AssertDeleteStatusCode(client, $"/api/dm/token/{filePathFragment}", HttpStatusCode.OK);

            //Test Direct
            var tokenResponse = await AssertPostStatusCode(client, $"/api/dm/token/{filePathFragment}", "", HttpStatusCode.OK);
            string token = (await tokenResponse.Content.ReadAsStringAsync()).Replace("\"","");
            Assert.IsNotNull(token);

            using (WebClient webClient = new WebClient())
            {
                string s = webClient.DownloadString($"{token}");
                Assert.AreEqual(s, checkStr);
            }

            //Test InDirect
            var ExpirationDate = DateTime.UtcNow.AddMilliseconds(100000);
            var tokenResponse2 = await AssertPostStatusCode(client, $"/api/dm/token/{filePathFragment}?IsIndirect=true&MaxViews=1&ExpirationDate={ExpirationDate.ToString()}", "", HttpStatusCode.OK);
            string token2 = (await tokenResponse2.Content.ReadAsStringAsync()).Replace("\"", "");
            using (WebClient webClient = new WebClient())
            {
                Assert.IsTrue(token2.Contains("/api/dm/token/"));
                HttpResponseMessage response = await client.GetAsync(token2);
                response.EnsureSuccessStatusCode();
                var properURL = await response.Content.ReadAsStringAsync();
                string s = webClient.DownloadString($"{properURL}");
                Assert.AreEqual(s, checkStr);
                await AssertGetStatusCode(client, $"{token2}", HttpStatusCode.BadRequest);
                //wait 1 second
                System.Threading.Thread.Sleep(1000);
                //check Expiration
                response = await client.GetAsync(token2);
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            }
            await AssertDeleteStatusCode(client, $"/api/dm/token/{filePathFragment}", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestAdminDocManEndpoints()
        {
            HttpClient client = StartServerAndGetClient();

            await AssertPostStatusCode(client, $"/api/dm/calcusage", "", HttpStatusCode.InternalServerError);
        }

        private HttpClient StartServerAndGetClient()
        {
            string contentRoot = Environment.CurrentDirectory;
            contentRoot = contentRoot.Substring(0, contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\') - 1) - 1) - 1)) + "\\Endor.Api.Web\\";
            var builder = new WebHostBuilder()
              .UseContentRoot(contentRoot)
              .UseEnvironment("Development")
              .UseStartup<TestStartup>();

            this._testServer = new TestServer(builder);

            HttpClient client = _testServer.CreateClient().WithDefaultIdentity(new[] {
                new Claim(ClaimNameConstants.BID, "1"),
                new Claim(ClaimNameConstants.UserID, "1"),
                new Claim(ClaimNameConstants.EmployeeID, "1"),
                new Claim(ClaimNameConstants.UserLinkID, "1")
            });
            return client;
        }

        private async Task AssertGetResponseContent(HttpClient client, string path, string content)
        {
            HttpResponseMessage response = await client.GetAsync(path);
            // Fail the test if non-success result
            response.EnsureSuccessStatusCode();

            // Get the response as a string
            string responseString = await response.Content.ReadAsStringAsync();

            Assert.IsTrue(responseString.Contains(content));
        }

        private async Task AssertGetStatusCode(HttpClient client, string url, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await client.GetAsync(url);
            Assert.AreEqual(statusCode, response.StatusCode);
        }

        private async Task AssertEmptyDeleteResponse(HttpClient client, string url) {
            HttpResponseMessage response = await client.DeleteAsync(url);
            AssertEmpty(await response.Content.ReadAsStringAsync());
        }

        private async Task AssertEmptyGetResponse(HttpClient client, string url) {
            HttpResponseMessage response = await client.GetAsync(url);
            AssertEmpty(await response.Content.ReadAsStringAsync());
        }

        private void AssertEmpty(string contentResponse) {
            bool isEmptyArray = contentResponse == "[]";
            bool isEmptyContent = String.IsNullOrWhiteSpace(contentResponse);
            Assert.IsTrue(isEmptyArray || isEmptyContent, $"Exptected Empty Array or Empty String. Got `{contentResponse}`");
        }

        private async Task AssertGetEitherStatusCode(HttpClient client, string url, HttpStatusCode statusCodeOne, HttpStatusCode statusCodeTwo)
        {
            HttpResponseMessage response = await client.GetAsync(url);
            Assert.IsTrue(response.StatusCode == statusCodeOne || response.StatusCode == statusCodeTwo);
        }

        private async Task<HttpResponseMessage> AssertPostFileStatusCode(HttpClient client, string url, HttpContent fileContent, params HttpStatusCode[] statusCodes) {
            HttpResponseMessage response = await client.PostAsync(url, fileContent );
            AssertAtLeastOneMatch(response, statusCodes);
            return response;
        }

        private async Task<HttpResponseMessage> AssertPostStatusCode(HttpClient client, string url, object content, HttpStatusCode statusCode)
        {
            return await AssertPostStatusCode(client, url, JsonConvert.SerializeObject(content), statusCode);
        }
        private async Task<HttpResponseMessage> AssertPostStatusCode(HttpClient client, string url, string content, params HttpStatusCode[] statusCodes)
        {
            HttpResponseMessage response = await client.PostAsync(url, new StringContent(content));
            AssertAtLeastOneMatch(response, statusCodes);
            return response;
        }
        public static async Task<HttpResponseMessage> AssertPostFormStatusCode(HttpClient client, string url, string content, params HttpStatusCode[] statusCodes)
        {
            var form = new MultipartFormDataContent();
            form.Add(new StringContent(content), "file", "file");
            HttpResponseMessage response = await client.PostAsync(url, form);
            AssertAtLeastOneMatch(response, statusCodes);
            return response;
        }

        private async Task<HttpResponseMessage> AssertPutStatusCode(HttpClient client, string url, string content, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await client.PutAsync(url, new StringContent(content));
            Assert.AreEqual(statusCode, response.StatusCode);
            return response;
        }

        private async Task<T> AssertPutStatusCode<T>(HttpClient client, string url, string content, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await AssertPutStatusCode(client, url, content, statusCode);

            string responseString = await response.Content.ReadAsStringAsync();
            Assert.IsTrue(!string.IsNullOrWhiteSpace(responseString));

            var result = JsonConvert.DeserializeObject<T>(responseString);
            Assert.IsNotNull(result);

            return result;
        }

        private async Task<HttpResponseMessage> AssertPutFormStatusCode(HttpClient client, string url, string content, params HttpStatusCode[] statusCodes)
        {
            var form = new MultipartFormDataContent();
            form.Add(new StringContent(content), "file", "file");
            HttpResponseMessage response = await client.PostAsync(url, form);
            AssertAtLeastOneMatch(response, statusCodes);
            return response;
        }

        private static void AssertAtLeastOneMatch(HttpResponseMessage response, HttpStatusCode[] statusCodes)
        {
            Assert.IsNotNull(response);
            if (statusCodes == null)
                throw new ArgumentNullException("statusCodes");
            else if (statusCodes.Length == 0)
                throw new InvalidOperationException("Must pass at least one expected status code");
            else if (statusCodes.Length == 1)
                Assert.AreEqual(statusCodes[0], response.StatusCode);
            else
            {
                if (!statusCodes.Any(x => response.StatusCode == x))
                {
                    string expected = String.Join(" or ", statusCodes);
                    Assert.Fail($"Expected {expected}, instead got {response.StatusCode}");
                }
            }
        }

        private async Task AssertPutStatusCode(HttpClient client, string url, object content, HttpStatusCode statusCode)
        {
            await AssertPutStatusCode(client, url, JsonConvert.SerializeObject(content), statusCode);
        }


        private async Task<HttpResponseMessage> AssertDeleteStatusCode(HttpClient client, string url, params HttpStatusCode[] statusCodes)
        {
            HttpResponseMessage response = await client.DeleteAsync(url);
            AssertAtLeastOneMatch(response, statusCodes);
            return response;
        }

        private MultipartFormDataContent GetMultipartFormDataContent(string fieldName, string fileName) 
        {
            MemoryStream stream = new MemoryStream();
            TextWriter tw = new StreamWriter(stream, Encoding.ASCII);
            tw.WriteLine("Hello World!");
            MultipartFormDataContent form = new MultipartFormDataContent();
            form.Add( new StreamContent(stream), fieldName, fileName);
            return form;
        }

        private async Task AssertRequestWithContentType(HttpMethod method , string contentType, HttpClient client, string url, string rawContent, params HttpStatusCode[] statusCodes) {

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));//ACCEPT header
            //add more request type here if you need any
            HttpResponseMessage response = null;
            if (method == HttpMethod.Post) {
                response = await client.PostAsync(url, new StringContent(rawContent,Encoding.UTF8,contentType));
            }
            AssertAtLeastOneMatch(response, statusCodes);
        }

        private byte[] ReadToEnd(System.IO.Stream stream) {
            long originalPosition = 0;

            if (stream.CanSeek) {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0) {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length) {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1) {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead) {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally {
                if (stream.CanSeek) {
                    stream.Position = originalPosition;
                }
            }
        }
    }
}
