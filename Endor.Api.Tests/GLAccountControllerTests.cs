﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using AssertX = Xunit.Assert;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using Microsoft.AspNetCore.Mvc;
using Endor.Models;
using Endor.Api.Web.Controllers;
using Endor.Api.Web.Services;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Tests
{
    [TestClass]
    [Ignore]
    public class GLAccountControllerTests: CommonControllerTestClass
    {
        [TestMethod]
        public async Task TestClone()
        {
            AssertHasTestBusiness();

            var GLAccountController = GetAtomCRUDController<GLAccountController, GLAccount, GLAccountService, int>();

            int? GLAccountID = await GetFirstDefaultGLAccount(GLAccountController);

            if (!GLAccountID.HasValue)
            {
                await GLAccountController.Create(new GLAccount()
                {
                    BID = TestConstants.BID,
                    ModifiedDT = DateTime.UtcNow,
                    IsActive = true,
                    CanEdit = true,
                    Name = "GLAccountTestName01",
                    GLAccountType = 10,
                });
                GLAccountID = await GetFirstDefaultGLAccount(GLAccountController);
                Assert.IsFalse(GLAccountID == null);
            }

            var result = await GLAccountController.Clone(GLAccountID.Value);
            var okPost = AssertX.IsType<OkObjectResult>(result);
            var clonedGLAccount = AssertX.IsType<GLAccount>(okPost.Value);
            Assert.IsNotNull(clonedGLAccount);
        }

        private async Task<int?> GetFirstDefaultGLAccount(GLAccountController GLAccountController)
        {
            var result = await GLAccountController.Read();
            if (result is OkObjectResult)
            {
                var GLAccounts = AssertX.IsType<List<GLAccount>>((result as OkObjectResult).Value);

                return GLAccounts.FirstOrDefault(x => x.IsActive)?.ID;
            }
            else
            {
                return null;
            }
        }
    }
}
