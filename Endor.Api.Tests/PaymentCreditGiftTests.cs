﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Request;
using Endor.Api.Web.Classes.Responses;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    [TestCategory("GL")]
    [TestCategory("Payment")]
    public class PaymentCreditGiftTests : PaymentTestHelper
    {

        private const string apiUrl = "/api/payment/creditgift";
        public TestContext TestContext { get; set; }


        [TestInitialize]
        public async Task Initialize()
        {
            await CleanUpTestData();
        }

        [TestCleanup]
        public async Task Teardown()
        {
            await CleanUpTestData();
        }

        private PaymentRequest GetSamplePaymentRequest()
        {
            List<PaymentApplicationRequest> paymentApplications = new List<PaymentApplicationRequest>();
            paymentApplications.Add(new PaymentApplicationRequest
            {
                OrderID = null,
                Amount = 5m
            });

            var ctx = GetApiContext();
            return new PaymentRequest
            {
                LocationID = ctx.LocationData.First().ID,
                CompanyID = ctx.CompanyData.First().ID,
                Amount = 5m,
                PaymentTransactionType = (int)PaymentTransactionType.Payment,
                ReceivedLocationID = ctx.LocationData.First().ID,
                Applications = paymentApplications,
                PaymentMethodID = 1
            };
        }
        private CreditGiftRequest GetCreditGiftRequest(int companyID, decimal amount)
        {
            var ctx = GetApiContext();
            var company = ctx.CompanyData.First(t => t.ID == companyID);
            return new CreditGiftRequest()
            {
                LocationID = company.LocationID,
                CompanyID = company.ID,
                Amount = amount,
                PaymentTransactionType = PaymentTransactionType.Credit_Gift,
                ReceivedLocationID = company.LocationID,
                PaymentMethodId = 251,
                Applications = new List<PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest
                    {
                        OrderID = null,
                        Amount = amount
                    }
                }
            };
        }

        [TestMethod]
        public async Task CreditGiftShouldCreateAMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var request = GetCreditGiftRequest(
                companyID: company.ID, 
                amount: 70
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(_client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            var master = ctx.PaymentMaster.FirstOrDefault(t => t.CompanyID == company.ID && t.PaymentTransactionType == (byte)PaymentTransactionType.Credit_Gift);
            Assert.IsNotNull(master);
            Assert.AreEqual(request.Amount, master.NonRefBalance);
            Assert.AreEqual(request.Amount, master.ChangeInNonRefCredit);
        }

        [TestMethod]
        public async Task CreditGiftShouldCreateAnApplication()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var request = GetCreditGiftRequest(
                companyID: company.ID, 
                amount: 70
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(_client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            var application = ctx.PaymentApplication.FirstOrDefault(t => t.CompanyID == company.ID && t.PaymentTransactionType == (byte)PaymentTransactionType.Credit_Gift);
            Assert.IsNotNull(application);
            Assert.AreEqual(request.Amount, application.NonRefBalance);
            Assert.AreEqual(request.Amount, application.Amount);
        }




        [TestMethod]
        public async Task CreditGiftShouldUpdateCompanysNonRefBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var request = GetCreditGiftRequest(
                companyID: company.ID, 
                amount: 70
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(_client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            ctx.Entry(company).Reload();
            Assert.AreEqual(request.Amount, company.NonRefundableCredit);
        }
    }
}
