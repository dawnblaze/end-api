﻿using Acheve.AspNetCore.TestHost.Security;
using Endor.Api.Web.Classes;
using Endor.Tenant;
using Endor.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Net.Http.Headers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    /// <summary>
    /// Used for testing API Endpoints
    /// </summary>
    public static class EndpointTests
    {
        /// <summary>
        /// Gets an HttpClient for use with the other EndpointTests methods
        /// </summary>
        /// <param name="AuthUserID"></param>
        /// <param name="rights">Array of SecurityRights that the User will have (optional)</param>
        /// <returns></returns>
        public static HttpClient GetHttpClient(short AuthUserID = TestConstants.AuthUserLinkID, SecurityRight[] rights = null, short employeeID = TestConstants.AuthEmployeeID)
        {
            string contentRoot = Environment.CurrentDirectory;
            contentRoot = contentRoot.Substring(0, contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\') - 1) - 1) - 1)) + "\\Endor.Api.Web\\";
            var builder = new WebHostBuilder()
              .UseContentRoot(contentRoot)
              .UseEnvironment("Development")
              .UseStartup<TestStartup>();

            var testServer = new TestServer(builder);

            rights = rights ?? new SecurityRight[]
            {
            };

            byte[] bytes = new byte[128];
            for (int i = 0; i < 128; i++)
            {
                bytes[i] = 0x0;
            }

            foreach (var right in rights)
            {
                int bytePos = (int)right / 8;
                int bitPos = (int)right % 8;
                byte byteAtBytePos = bytes[bytePos];
                int resultByte = byteAtBytePos | ((byte)(1 << bitPos));

                bytes[bytePos] = Convert.ToByte(resultByte);
            }


            return testServer.CreateClient().WithDefaultIdentity(new[] {
                new Claim(ClaimNameConstants.BID,        TestConstants.BID.ToString()),
                new Claim(ClaimNameConstants.AID,        TestConstants.AID.ToString()),
                new Claim(ClaimNameConstants.UserID,     TestConstants.AuthUserID.ToString()),
                new Claim(ClaimNameConstants.UserName,     TestConstants.AuthUserName.ToString()),
                new Claim(ClaimNameConstants.EmployeeID, employeeID.ToString()),
                new Claim(ClaimNameConstants.UserLinkID, AuthUserID.ToString()),
                new Claim(ClaimNameConstants.AccessType, TestConstants.AccessType.ToString()),
                new Claim(ClaimNameConstants.Rights, Convert.ToBase64String(bytes))
            });

        }

        /// <summary>
        /// Tests against GET response's content
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="path">Path</param>
        /// <param name="content">Content to test against</param>
        /// <returns></returns>
        public static async Task<string> AssertGetResponseContent(HttpClient client, string path, string content)
        {
            HttpResponseMessage response = await client.GetAsync(path);
            // Fail the test if non-success result
            response.EnsureSuccessStatusCode();

            // Get the response as a string
            string responseString = await response.Content.ReadAsStringAsync();

            Assert.IsTrue(responseString.Contains(content), $"GET { path} content did not contain string {content}");

            return responseString;
        }

        public static async Task<string> AssertGetResponseContent(HttpClient client, string url, string content, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await client.GetAsync(url);

            Assert.AreEqual<HttpStatusCode>(statusCode, response.StatusCode, $"GET {url} did not match status code {statusCode}");

            // Get the response as a string
            string responseString = await response.Content.ReadAsStringAsync();

            Assert.IsTrue(responseString.Contains(content), $"GET { url} content did not contain string {content}");

            return responseString;
        }

        public static async Task<string> AssertGetStatusContent(HttpClient client, string url, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await client.GetAsync(url);

            Assert.AreEqual(statusCode, response.StatusCode, $"GET {url} did not match status code {statusCode}");

            return await response.Content.ReadAsStringAsync(); ;
        }

        /// <summary>
        /// Tests against GET response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task AssertGetStatusCode(HttpClient client, string url, HttpStatusCode statusCode, Dictionary<EtagKey, string> etagDictionary = null, EtagKey key = EtagKey.AELBusinessMemberDictionary)
        {
            if ((etagDictionary != null) && (etagDictionary.ContainsKey(key)))
            {
                client.DefaultRequestHeaders.Remove("If-None-Match");
                client.DefaultRequestHeaders.TryAddWithoutValidation("If-None-Match",
                        etagDictionary[key]);
            }
            HttpResponseMessage response = await client.GetAsync(url);
            Assert.AreEqual(statusCode, response.StatusCode, $"GET {url} did not match status code {statusCode}");
        }

        /// <summary>
        /// Tests against GET response's Status Code and returns an object
        /// </summary>
        /// <typeparam name="T">Class to test against</typeparam>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<T> AssertGetStatusCode<T>(HttpClient client, string url, HttpStatusCode statusCode, Dictionary<EtagKey, string> etagDictionary = null, EtagKey key = EtagKey.AELBusinessMemberDictionary)
        {
            if ((etagDictionary != null) && (etagDictionary.ContainsKey(key)))
            {
                client.DefaultRequestHeaders.Remove("If-None-Match");
                client.DefaultRequestHeaders.TryAddWithoutValidation("If-None-Match",
                        etagDictionary[key]);
            }
            HttpResponseMessage response = await client.GetAsync(url);
            Assert.AreEqual(statusCode, response.StatusCode, $"GET {url} did not match expected status code");

            // skip checking when bad request is expected
            if (statusCode.Equals(HttpStatusCode.NotFound) || statusCode.Equals(HttpStatusCode.BadRequest))
            {
                return default(T);
            }

            string responseString = await response.Content.ReadAsStringAsync();

            Assert.IsTrue(!string.IsNullOrWhiteSpace(responseString), $"GET {url} had null content");

            if (etagDictionary != null)
            {
                IEnumerable<string> eTags;
                Assert.IsTrue(response.Headers.TryGetValues(HeaderNames.ETag, out eTags));
                etagDictionary[key] = eTags.First();
            }

            if (typeof(T).Name == nameof(String))
                return (T)Convert.ChangeType(responseString, typeof(T));

            var result = JsonConvert.DeserializeObject<T>(responseString);
            Assert.IsNotNull(result, $"GET {url} content did deserialize into a {typeof(T).Name}");

            return result;
        }



        /// <summary>
        /// Tests against either of GET response's Status Codes.
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="statusCodeOne">HTTPStatusCode 1 to test for</param>
        /// <param name="statusCodeTwo">HTTPStatusCode 2 to test for</param>
        /// <returns></returns>
        public static async Task AssertGetEitherStatusCode(HttpClient client, string url, HttpStatusCode statusCodeOne, HttpStatusCode statusCodeTwo)
        {
            HttpResponseMessage response = await client.GetAsync(url);
            Assert.IsTrue(response.StatusCode == statusCodeOne || response.StatusCode == statusCodeTwo, $"GET {url} did not match status code {statusCodeOne} or {statusCodeTwo}");
        }

        /// <summary>
        /// Tests against POST response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content Object to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task AssertPostStatusCode(HttpClient client, string url, object content, HttpStatusCode statusCode)
        {
            await AssertPostStatusCode(client, url, JsonConvert.SerializeObject(content), statusCode);
        }

        /// <summary>
        /// Tests against POST response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content JSON to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> AssertPostStatusCode(HttpClient client, string url, string content, HttpStatusCode statusCode, bool isRaw = false)
        {
            StringContent strContent = null;

            if (!string.IsNullOrWhiteSpace(content))
                strContent = new StringContent(content, Encoding.UTF8, !isRaw ? "application/json" : "text/plain");

            HttpResponseMessage response = await client.PostAsync(url, strContent);
            var strResponseTask = response.Content.ReadAsStringAsync();
            strResponseTask.Wait();
            string strResponse = strResponseTask.Result;

            Assert.AreEqual(statusCode, response.StatusCode, $"POST {url} did not match status code {statusCode}. {strResponse}");
            return response;
        }

        public static async Task<HttpResponseMessage> AssertPostStatusCodeEither(HttpClient client, string url, string content, HttpStatusCode statusCode, HttpStatusCode altStatusCode)
        {
            StringContent strContent = null;

            if (!string.IsNullOrWhiteSpace(content))
                strContent = new StringContent(content, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(url, strContent);
            Assert.IsTrue(statusCode == response.StatusCode || altStatusCode == response.StatusCode, $"POST {url} did not match status code {statusCode} or {altStatusCode}");
            return response;
        }
        public static async Task<HttpResponseMessage> AssertPostStatusCode(HttpClient client, string url, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await client.PostAsync(url, null);
            Assert.AreEqual(statusCode, response.StatusCode, $"POST {url} did not match status code {statusCode}");
            return response;
        }

        /// <summary>
        /// Tests against POST response's Status Code and returns an object
        /// </summary>
        /// <typeparam name="T">Class to test against</typeparam>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content JSON to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<T> AssertPostStatusCode<T>(HttpClient client, string url, string content, HttpStatusCode statusCode, bool isRaw = false)
        {
            HttpResponseMessage response = await AssertPostStatusCode(client, url, content, statusCode, isRaw);

            string responseString = await response.Content.ReadAsStringAsync();

            // skip deserializing response if we expect a bad request
            if (string.IsNullOrWhiteSpace(responseString) && statusCode == HttpStatusCode.BadRequest)
            {
                return default(T);
            }

            Assert.IsTrue(!string.IsNullOrWhiteSpace(responseString), $"POST {url} content was null");

            var result = !isRaw ? (T)JsonConvert.DeserializeObject<T>(responseString) : (T)(object)responseString;
            Assert.IsNotNull(result, $"POST {url} content did not deserialize into {typeof(T).Name}");

            return result;
        }

        public static async Task<T> AssertPostStatusCode<T>(HttpClient client, string url, object content, HttpStatusCode statusCode)
        {
            return await AssertPostStatusCode<T>(client, url, JsonConvert.SerializeObject(content), statusCode);
        }

        public static async Task<T> AssertPostStatusCode<T>(HttpClient client, string url, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await AssertPostStatusCode(client, url, statusCode);

            string responseString = await response.Content.ReadAsStringAsync();
            Assert.IsTrue(!string.IsNullOrWhiteSpace(responseString), $"POST {url} content was null");

            var result = JsonConvert.DeserializeObject<T>(responseString);
            Assert.IsNotNull(result, $"POST {url} content did not deserialize into {typeof(T).Name}");

            return result;
        }

        /// <summary>
        /// Tests against PUT response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content JSON to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> AssertPutStatusCode(HttpClient client, string url, string content, HttpStatusCode statusCode, HttpStatusCode? statusCodeAlternative = null)
        {
            StringContent strContent = null;

            if (!string.IsNullOrWhiteSpace(content))
                strContent = new StringContent(content, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PutAsync(url, strContent);
            Assert.IsTrue(response.StatusCode == statusCode || (statusCodeAlternative.HasValue && response.StatusCode == statusCodeAlternative.Value),
                $"PUT {url} expected {statusCode}{(statusCodeAlternative == null ? "" : " or " + statusCodeAlternative)} instead, got {response.StatusCode}");

            return response;
        }

        public static async Task<T> AssertPutStatusCode<T>(HttpClient client, string url, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await client.PutAsync(url, null);
            T result = await HandleResponseMessage<T>(response);
            return result;
        }

        /// <summary>
        /// Tests against PUT response's Status Code and returns an object
        /// </summary>
        /// <typeparam name="T">Class to test against</typeparam>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content JSON to POST</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<T> AssertPutStatusCode<T>(HttpClient client, string url, string content, HttpStatusCode statusCode)
        {
            HttpResponseMessage response = await AssertPutStatusCode(client, url, content, statusCode);
            T result = await HandleResponseMessage<T>(response);

            return result;
        }

        /// <summary>
        /// put object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="client"></param>
        /// <param name="url"></param>
        /// <param name="content"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public static async Task<T> AssertPutStatusCode<T>(HttpClient client, string url, object content, HttpStatusCode statusCode)
        {
            return await AssertPutStatusCode<T>(client, url, JsonConvert.SerializeObject(content), statusCode);
        }

        /// <summary>
        /// Deserializes response from endpoint into type of T. Optionally allows for NoContentResponse e.g. from DELETE endpoints
        /// </summary>
        /// <typeparam name="T">Type of object to be used for deserialization</typeparam>
        /// <param name="response">HttpResponseMessage received from API</param>
        /// <param name="allowNoContentResponse">Set this to 'true' when expecting NoContentResponse or empty response body. Defaults to false</param>
        /// <returns></returns>
        private static async Task<T> HandleResponseMessage<T>(HttpResponseMessage response, bool allowNoContentResponse = false)
        {
            T result;
            string verb = response.RequestMessage.Method.Method;
            string responseString = await response.Content.ReadAsStringAsync();

            //Special handling of Delete endpoints that return NoContentResult
            if (!allowNoContentResponse)
            {
                Assert.IsTrue(!string.IsNullOrWhiteSpace(responseString), $"{verb} {response.RequestMessage.RequestUri.AbsoluteUri} content was null");
                result = JsonConvert.DeserializeObject<T>(responseString);
                Assert.IsNotNull(result, $"{verb} {response.RequestMessage.RequestUri.AbsoluteUri} content did not deserialize into {typeof(T).Name}");
            }
            else
            {//Create instance of NoContentResult or expected return type of T
                result = Activator.CreateInstance<T>();
            }

            return result;
        }

        /// Tests against PUT response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="content">Content Object to PUT</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task AssertPutStatusCode(HttpClient client, string url, object content, HttpStatusCode statusCode)
        {
            await AssertPutStatusCode(client, url, JsonConvert.SerializeObject(content), statusCode);
        }
        public static async Task AssertPutStatusCode(HttpClient client, string url, HttpStatusCode statusCode)
        {
            await AssertPutStatusCode(client, url, null, statusCode);
        }

        /// <summary>
        /// Tests against DELETE response's Status Code
        /// </summary>
        /// <param name="client">HttpClient</param>
        /// <param name="url">API Url</param>
        /// <param name="statusCode">HttpStatusCode to test for</param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> AssertDeleteStatusCode(HttpClient client, string url, params HttpStatusCode[] statusCodes)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync(url);
                //Assert.AreEqual(statusCode, response.StatusCode);
                AssertAtLeastOneMatch(response, statusCodes);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void AssertAtLeastOneMatch(HttpResponseMessage response, HttpStatusCode[] statusCodes)
        {
            Assert.IsNotNull(response);
            if (statusCodes == null)
                throw new ArgumentNullException("statusCodes");
            else if (statusCodes.Length == 0)
                throw new InvalidOperationException("Must pass at least one expected status code");
            else if (statusCodes.Length == 1)
                Assert.AreEqual(statusCodes[0], response.StatusCode);
            else
            {
                if (!statusCodes.Any(x => response.StatusCode == x))
                {
                    string expected = String.Join(" or ", statusCodes);
                    Assert.Fail($"Expected {expected}, instead got {response.StatusCode}");
                }
            }
        }

        public static async Task<T> AssertDeleteStatusCode<T>(HttpClient client, string url, HttpStatusCode statusCode)
        {
            var response = await AssertDeleteStatusCode(client, url, statusCode);
            return await HandleResponseMessage<T>(response, allowNoContentResponse: true);
        }

        public static async Task<T> AssertDeleteStatusCodeWithContent<T>(HttpClient client, string url, HttpStatusCode statusCode)
        {
            var response = await AssertDeleteStatusCode(client, url, statusCode);
            return await HandleResponseMessage<T>(response);
        }
    }
}
