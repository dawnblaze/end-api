﻿using Endor.Api.Web.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class SecurityTests : CommonControllerTestClass
    {
        public const string apiUrl = "api/security";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task RightDefinitionsTest()
        {
            var rigths = await EndpointTests.AssertGetStatusCode<string>(client, $"{apiUrl}/rightdefinitions", HttpStatusCode.OK);
            Assert.IsNotNull(rigths);

            foreach (var enumItem in Enum.GetValues(typeof(Security.SecurityRight)))
                Assert.IsTrue(rigths.Contains(enumItem.ToString()));
        }

        [TestMethod]
        public async Task UserRightsResultFailureWrongFormatDefaultsToJson()
        {
            var right = await EndpointTests.AssertGetStatusCode<SecurityRightsResponses.JSONRights>(client, $"{apiUrl}/rights?format=XML", HttpStatusCode.OK);

            Assert.IsNotNull(right);
            Assert.IsTrue(right.BID == TestConstants.BID);
            Assert.IsTrue(right.UserLinkID == TestConstants.AuthUserID);
            Assert.IsTrue(right.Rights.Any());
        }

        [TestMethod]
        public async Task UserRightsJsonResultSucceed()
        {
            var right = await EndpointTests.AssertGetStatusCode<SecurityRightsResponses.JSONRights>(client, $"{apiUrl}/rights?format=JSON", HttpStatusCode.OK);
            
            Assert.IsNotNull(right);
            Assert.IsTrue(right.BID == TestConstants.BID);
            Assert.IsTrue(right.UserLinkID == TestConstants.AuthUserID);
            Assert.IsTrue(right.Rights.Any());
        }

        [TestMethod]
        public async Task UserRightsStringResultSucceed()
        {
            var right = await EndpointTests.AssertGetStatusCode<string>(client, $"{apiUrl}/rights?format=Base64", HttpStatusCode.OK);

            Assert.IsNotNull(right);
        }

        [TestMethod]
        public async Task UserRightsWithSourceJsonResultSucceed()
        {
            var right = await EndpointTests.AssertGetStatusCode<SecurityRightsProceduresResults.RightsForSource[]>(client, $"{apiUrl}/rights/withsource", HttpStatusCode.OK);

            Assert.IsNotNull(right);
            Assert.IsTrue(right.Any());
        }
    }
}
