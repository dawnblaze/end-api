﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.EF;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Tests
{
    [TestClass]
    public class OrderItemStatusTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/orderitemstatus";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        protected string testOrderItemStatusName, testOrderItemStatusName2, testOrderItemStatusName3;

        [TestInitialize]
        public override void Init()
        {
            testOrderItemStatusName = "test." + DateTime.UtcNow.Ticks.ToString();
            testOrderItemStatusName2 = "test." + DateTime.UtcNow.Ticks.ToString();
            testOrderItemStatusName3 = "test." + DateTime.UtcNow.Ticks.ToString();
            base.Init();
        }

        [TestMethod]
        public async Task TestCRUD()
        {
            OrderItemStatus testObject = GetOrderItemStatus();
            testObject.IsDefault = false;
            testObject.IsActive = false;
            OrderItemStatus saved = await EndpointTests.AssertPostStatusCode<OrderItemStatus>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);






            var read = await EndpointTests.AssertGetStatusCode<OrderItemStatus>(client, $"{apiUrl}/{saved.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(read);

            var readAll = await EndpointTests.AssertGetStatusCode<List<OrderItemStatus>>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(readAll);

            saved.Description = "saved";

            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{saved.ID}", JsonConvert.SerializeObject(saved), HttpStatusCode.OK);
            read = await EndpointTests.AssertGetStatusCode<OrderItemStatus>(client, $"{apiUrl}/{saved.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(read);
            Assert.AreEqual(saved.Description, read.Description);

            // check for delete for non-default orderitemstatus
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{saved.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{saved.ID}", HttpStatusCode.NotFound);

            // check delete default orderitemstatus
            var defaultOrderItem = readAll.FirstOrDefault(ois => ois.IsDefault);
            if (defaultOrderItem != null)
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{defaultOrderItem.ID}", HttpStatusCode.Forbidden, HttpStatusCode.BadRequest);
                await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{defaultOrderItem.ID}", HttpStatusCode.OK);
            }
        }

        private async Task<OrderItemStatus> AssertPostNewOrderItemStatus(string name = null, OrderOrderStatus status = OrderOrderStatus.OrderWIP)
        {
            var testOrderItemStatus = GetOrderItemStatus(name, status);

            var saved = await EndpointTests.AssertPostStatusCode<OrderItemStatus>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrderItemStatus), HttpStatusCode.OK);
            return saved;
        }

        private OrderItemStatus GetOrderItemStatus(string name = null, OrderOrderStatus status = OrderOrderStatus.OrderWIP)
        {
            return new OrderItemStatus()
            {
                BID = 1,
                Name = name ?? this.testOrderItemStatusName,
                IsActive = true,
                IsSystem = false,
                IsDefault = true,
                TransactionType = (byte)OrderTransactionType.Order,
                StatusIndex = 50,
                OrderStatusID = status
            };
        }

        [TestMethod]
        public async Task TestFilters()
        {
            OrderItemStatus saved = await AssertPostNewOrderItemStatus();
            Assert.IsTrue(saved.IsActive);
            //test get all isactive
            var readAll = await EndpointTests.AssertGetStatusCode<List<OrderItemStatus>>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(readAll.All(x => x.IsActive));

            //test get all orderstatusid
            int wip = Convert.ToInt32(OrderOrderStatus.OrderWIP);
            var allWip = await EndpointTests.AssertGetStatusCode<List<OrderItemStatus>>(client, $"{apiUrl}?OrderStatusID={wip}", HttpStatusCode.OK);
            Assert.IsTrue(allWip.All(x => x.OrderStatusID == OrderOrderStatus.OrderWIP));

            //test get all transactiontype
            var allOrders = await EndpointTests.AssertGetStatusCode<List<OrderItemStatus>>(client, $"{apiUrl}?TransactionType={Convert.ToInt32(OrderTransactionType.Order)}", HttpStatusCode.OK);
            Assert.IsTrue(allOrders.All(x => x.TransactionType == (byte)OrderTransactionType.Order));

            //test get simple isactive
            var readAllSimple = await EndpointTests.AssertGetStatusCode<List<SimpleOrderItemStatus>>(client, $"{apiUrl}/simplelist?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(readAllSimple.All(x => x.IsActive));

            //test get simple orderstatusid
            readAllSimple = await EndpointTests.AssertGetStatusCode<List<SimpleOrderItemStatus>>(client, $"{apiUrl}/simplelist?OrderStatusID={wip}", HttpStatusCode.OK);
            Assert.IsTrue(readAllSimple.All(x => allWip.FirstOrDefault(y => y.ID == x.ID) != null));

            //test get simple transactiontype
            readAllSimple = await EndpointTests.AssertGetStatusCode<List<SimpleOrderItemStatus>>(client, $"{apiUrl}/simplelist?TransactionType={Convert.ToInt32(OrderTransactionType.Order)}", HttpStatusCode.OK);
            Assert.IsTrue(readAllSimple.All(x => allOrders.FirstOrDefault(y => y.ID == x.ID) != null));
        }

        [TestMethod]
        public async Task TestEndpointSetInactiveDefault()
        {
            OrderItemStatus saved = await AssertPostNewOrderItemStatus();
            Assert.IsTrue(saved.IsActive);
            Assert.IsTrue(saved.IsDefault);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{saved.ID}/action/setinactive", HttpStatusCode.Forbidden);
            //cleanup
            var ctx = GetApiContext();
            ctx.OrderItemStatus.Remove(ctx.OrderItemStatus.Where(ois => ois.ID==saved.ID && ois.BID == 1).First());
            ctx.SaveChanges();
        }

        [TestMethod]
        public async Task TestSetActiveDefault()
        {
            OrderItemStatus saved = await AssertPostNewOrderItemStatus();
            Assert.IsTrue(saved.IsActive);

            var actualDefault = GetApiContext().OrderItemStatus.FirstOrDefault(x => x.BID == 1 && x.TransactionType == saved.TransactionType && x.IsDefault);

            //test set inactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{saved.ID}/action/setinactive", HttpStatusCode.Forbidden);
            var read = await EndpointTests.AssertGetStatusCode<OrderItemStatus>(client, $"{apiUrl}/{saved.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(read);
            Assert.IsTrue(read.IsActive);

            //test set active
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{saved.ID}/action/setactive", HttpStatusCode.OK);
            read = await EndpointTests.AssertGetStatusCode<OrderItemStatus>(client, $"{apiUrl}/{saved.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(read);
            Assert.IsTrue(read.IsActive);

            //test set default
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{saved.ID}/action/setdefault", HttpStatusCode.OK);
            read = await EndpointTests.AssertGetStatusCode<OrderItemStatus>(client, $"{apiUrl}/{saved.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(read);
            Assert.IsTrue(read.IsDefault);

            //set the default back so we can delete our test
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{actualDefault.ID}/action/setdefault", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestSetInactiveAsDefault()
        {
            OrderItemStatus testObject = GetOrderItemStatus();
            testObject.IsDefault = false;
            testObject.IsActive = false;
            OrderItemStatus saved = await EndpointTests.AssertPostStatusCode<OrderItemStatus>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);
            Assert.IsFalse(saved.IsDefault);
            Assert.IsFalse(saved.IsActive);

            var actualDefault = GetApiContext().OrderItemStatus.FirstOrDefault(x => x.BID == 1 && x.TransactionType == saved.TransactionType && x.IsDefault);

            //test set default
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{saved.ID}/action/setdefault", HttpStatusCode.OK);
            var read = await EndpointTests.AssertGetStatusCode<OrderItemStatus>(client, $"{apiUrl}/{saved.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(read);
            Assert.IsTrue(read.IsDefault);
            Assert.IsTrue(read.IsActive);

            //set the default back so we can delete our test
            if (actualDefault != null)
            {
                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{actualDefault.ID}/action/setdefault", HttpStatusCode.OK);
            }
        }

        [TestMethod]
        public async Task TestCanDeleteMove()
        {
            OrderItemStatus saved1 = await AssertPostNewOrderItemStatus();
            Assert.IsTrue(saved1.IsActive);

            //candelete
            var canDelete = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{saved1.ID}/action/candelete", HttpStatusCode.OK);
            Assert.IsTrue(canDelete.Success && canDelete.Value.HasValue && canDelete.Value.Value);

            OrderItemStatus saved2 = await AssertPostNewOrderItemStatus(testOrderItemStatusName2);
            Assert.IsTrue(saved2.IsActive);

            //test move before
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{saved1.ID}/action/movebefore/{saved2.ID}", HttpStatusCode.OK);
            var read1 = await EndpointTests.AssertGetStatusCode<OrderItemStatus>(client, $"{apiUrl}/{saved1.ID}", HttpStatusCode.OK);
            var read2 = await EndpointTests.AssertGetStatusCode<OrderItemStatus>(client, $"{apiUrl}/{saved2.ID}", HttpStatusCode.OK);
            Assert.IsTrue(read1.StatusIndex < read2.StatusIndex);

            //test move after
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{saved1.ID}/action/moveafter/{saved2.ID}", HttpStatusCode.OK);
            read1 = await EndpointTests.AssertGetStatusCode<OrderItemStatus>(client, $"{apiUrl}/{saved1.ID}", HttpStatusCode.OK);
            read2 = await EndpointTests.AssertGetStatusCode<OrderItemStatus>(client, $"{apiUrl}/{saved2.ID}", HttpStatusCode.OK);
            Assert.IsTrue(read1.StatusIndex > read2.StatusIndex);

            //test move invalid
            OrderItemStatus poItemStatus = await AssertPostNewOrderItemStatus(testOrderItemStatusName3, OrderOrderStatus.POApproved);
            Assert.IsTrue(poItemStatus.IsActive);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{saved1.ID}/action/moveafter/{poItemStatus.ID}", HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public async Task TestSubStatuses()
        {
            OrderItemStatus saved = await AssertPostNewOrderItemStatus();

            using (ApiContext ctx = GetApiContext(1))
            {
                OrderItemSubStatus itemSubStatus = new OrderItemSubStatus()
                {
                    BID = 1,
                    ID = 999,
                    Name = "banana",
                    Description = "des"
                };

                var existingOrderItemSubStatusTest = await ctx.OrderItemSubStatus.Where(oiss => oiss.ID == itemSubStatus.ID).FirstOrDefaultAsync();
                if (existingOrderItemSubStatusTest != null)
                {
                    ctx.OrderItemSubStatus.Remove(existingOrderItemSubStatusTest);
                    await ctx.SaveChangesAsync();//remove existing test records
                }

                ctx.OrderItemSubStatus.Add(itemSubStatus);
                Assert.IsTrue((await ctx.SaveChangesAsync()) > 0);//test insert

                //test link-substatus
                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{saved.ID}/action/linksubstatus/{itemSubStatus.ID}", HttpStatusCode.OK);
                var mytestLink = await ctx.OrderItemStatusSubStatusLink
                                    .Include(link => link.OrderItemStatus)
                                    .Include(link => link.OrderItemSubStatus)
                                    .Where(link => link.StatusID == saved.ID && link.SubStatusID == itemSubStatus.ID)
                                    .FirstOrDefaultAsync();

                Assert.IsNotNull(mytestLink.OrderItemStatus);//test navigation prop
                Assert.IsNotNull(mytestLink.OrderItemSubStatus);//test navigation prop

                //test link-substatus
                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{saved.ID}/action/unlinksubstatus/{itemSubStatus.ID}", HttpStatusCode.OK);
                mytestLink = await ctx.OrderItemStatusSubStatusLink
                                    .Include(link => link.OrderItemStatus)
                                    .Include(link => link.OrderItemSubStatus)
                                    .Where(link => link.StatusID == saved.ID && link.SubStatusID == itemSubStatus.ID)
                                    .FirstOrDefaultAsync();
                Assert.IsNull(mytestLink);
            }
        }


        [TestCleanup]
        public void Teardown()
        {
            try
            {
                Task.Run(async () =>
                {
                    var readAll = await EndpointTests.AssertGetStatusCode<List<OrderItemStatus>>(client, $"{apiUrl}", HttpStatusCode.OK);

                    var created = readAll.FirstOrDefault(x => x.Name == testOrderItemStatusName);
                    if (created != null)
                    {
                        await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{created.ID}", HttpStatusCode.NoContent);
                    }
                    created = readAll.FirstOrDefault(x => x.Name == testOrderItemStatusName2);
                    if (created != null)
                    {
                        await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{created.ID}", HttpStatusCode.NoContent);
                    }
                    created = readAll.FirstOrDefault(x => x.Name == testOrderItemStatusName3);
                    if (created != null)
                    {
                        await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{created.ID}", HttpStatusCode.NoContent);
                    }
                }).Wait();
            }
            catch { }
        }
    }
}
