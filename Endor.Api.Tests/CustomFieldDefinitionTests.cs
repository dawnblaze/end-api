﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Tests
{
    [TestClass]
    public class CustomFieldDefinitionTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/cf/definition";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public List<short> IDsToCleanup = new List<short>();

        [TestCleanup]
        public void Cleanup()
        {
            CleanupCustomFieldDefinitions(GetApiContext(), 1, IDsToCleanup.ToArray());
        }

        public static void CleanupCustomFieldDefinitions(EF.ApiContext ctx, short bid, short[] IDs)
        {
            ctx.RemoveRange(ctx.CustomFieldDefinition.Where(x => IDs.Contains(x.ID) && x.BID == bid).ToArray());
            ctx.SaveChanges();
        }

        [TestMethod]
        public async Task TestCustomFieldDefinitionCRUDOperations()
        {
            const int contactClassID = (int)Endor.Models.ClassType.Contact;
            CustomFieldDefinition testCustomFieldDefinition = Utils.GetCustomFieldDefinition();
            testCustomFieldDefinition.AppliesToClassTypeID = contactClassID;
            testCustomFieldDefinition.AppliesToID = 2;

            #region CREATE
            var createdCustomFieldDefinition = await EndpointTests.AssertPostStatusCode<CustomFieldDefinition>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testCustomFieldDefinition), HttpStatusCode.OK);
            IDsToCleanup.Add(createdCustomFieldDefinition.ID);
            #endregion

            #region RETRIEVE

            // Test Get All
            var retrievedCustomFieldDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldDefinition[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedCustomFieldDefinitions);

            //test applies to class Type
            var contactDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldDefinition[]>(client, $"{apiUrl}/?AppliesToClassTypeID={contactClassID}", HttpStatusCode.OK);
            if (contactDefinitions.Length > 0)
            {
                Assert.IsTrue(contactDefinitions.Any(t => t.ID == createdCustomFieldDefinition.ID));
            }

            //Test Applies to Int
            int companyClassID = (int)Endor.Models.ClassType.Company;
            var companyDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldDefinition[]>(client, $"{apiUrl}/?AppliesToClassTypeID={companyClassID}&AppliesToID={createdCustomFieldDefinition.AppliesToID}", HttpStatusCode.OK);
            if (companyDefinitions.Length > 0)
            {
                Assert.IsFalse(companyDefinitions.Any(t => t.ID == createdCustomFieldDefinition.ID));
            }

            // Test Get By ID
            var retrievedCustomFieldDefinition = await EndpointTests.AssertGetStatusCode<CustomFieldDefinition>(client, $"{apiUrl}/{createdCustomFieldDefinition.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedCustomFieldDefinition);

            #endregion

            #region UPDATE

            int invalidID = -1;
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{invalidID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{invalidID}", JsonConvert.SerializeObject(createdCustomFieldDefinition), HttpStatusCode.NotFound, HttpStatusCode.BadRequest);

            //test failing if one of the following fields does not match the original
            //AppliesToClassTypeId
            var testCustomFieldDefinition2 = Utils.GetCustomFieldDefinition();
            testCustomFieldDefinition2.AppliesToClassTypeID = contactClassID;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdCustomFieldDefinition.ID}", JsonConvert.SerializeObject(testCustomFieldDefinition2), HttpStatusCode.BadRequest, HttpStatusCode.BadRequest);
            //AppliesToId
            testCustomFieldDefinition2.AppliesToClassTypeID = companyClassID;
            testCustomFieldDefinition2.AppliesToID = 3;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdCustomFieldDefinition.ID}", JsonConvert.SerializeObject(testCustomFieldDefinition2), HttpStatusCode.BadRequest, HttpStatusCode.BadRequest);
            //DataType
            testCustomFieldDefinition2.AppliesToID = 2;
            testCustomFieldDefinition2.DataType = DataType.String;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdCustomFieldDefinition.ID}", JsonConvert.SerializeObject(testCustomFieldDefinition2), HttpStatusCode.BadRequest, HttpStatusCode.BadRequest);

            createdCustomFieldDefinition.Description = "TEST DESCRIPTION";
            var testPut = await EndpointTests.AssertPutStatusCode<CustomFieldDefinition>(client, $"{apiUrl}/{createdCustomFieldDefinition.ID}", JsonConvert.SerializeObject(createdCustomFieldDefinition), HttpStatusCode.OK);
            Assert.IsNotNull(testPut);
            Assert.AreEqual("TEST DESCRIPTION", testPut.Description);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdCustomFieldDefinition.ID}", HttpStatusCode.NoContent);

            #endregion
        }


        [TestMethod]
        public async Task TestCustomFieldDefinitionActions()
        {
            var testCustomFieldDefinition = Utils.GetCustomFieldDefinition();
            var createdCustomFieldDefinition = await EndpointTests.AssertPostStatusCode<CustomFieldDefinition>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testCustomFieldDefinition), HttpStatusCode.OK);
            IDsToCleanup.Add(createdCustomFieldDefinition.ID);

            //active
            await EndpointTests.AssertPostStatusCode<CustomFieldDefinition>(client, $"{apiUrl}/{createdCustomFieldDefinition.ID}/action/setactive", JsonConvert.SerializeObject(createdCustomFieldDefinition), HttpStatusCode.OK);
            var cfd1 = await EndpointTests.AssertGetStatusCode<CompanyData>(client, $"{apiUrl}/{createdCustomFieldDefinition.ID}", HttpStatusCode.OK);
            Assert.IsTrue(cfd1.IsActive == true);

            //test in-active
            await EndpointTests.AssertPostStatusCode<CustomFieldDefinition>(client, $"{apiUrl}/{createdCustomFieldDefinition.ID}/action/setinactive", JsonConvert.SerializeObject(createdCustomFieldDefinition), HttpStatusCode.OK);
            var cfd2 = await EndpointTests.AssertGetStatusCode<CompanyData>(client, $"{apiUrl}/{createdCustomFieldDefinition.ID}", HttpStatusCode.OK);
            Assert.IsTrue(cfd2.IsActive == false);
        }
    }
}
