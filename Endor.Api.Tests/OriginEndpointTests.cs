﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

namespace Endor.Api.Tests
{
    [TestClass]
    public class OriginEndpointTests
    {
        public const string apiUrl = "/api/origin";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestOriginName;
        public TestContext TestContext { get; set; }

        private CrmOrigin GetNewModel()
        {
            return new CrmOrigin()
            { 
                Name = this.NewTestOriginName,
                IsActive = true,
                BID = 1
            };
        }

        [TestInitialize]
        public void Initialize()
        {
            this.NewTestOriginName = DateTime.UtcNow + " TEST ORIGIN DATA";
        }

        [TestMethod]
        public async Task GetOriginTest()
        {
            var list = await EndpointTests.AssertGetStatusCode<List<CrmOrigin>>(client, apiUrl, HttpStatusCode.OK);

            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod]
        public async Task TestOriginCanDelete()
        {
            CrmOrigin itemTest = GetNewModel();

            //test: expect OK
            var crmOrigin = await EndpointTests.AssertPostStatusCode<CrmOrigin>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(crmOrigin);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{crmOrigin.ID}/action/candelete", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{crmOrigin.ID}", HttpStatusCode.NoContent);
        }

    }
}
