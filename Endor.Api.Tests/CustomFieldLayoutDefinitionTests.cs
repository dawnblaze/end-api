﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Endor.EF;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Tests
{
    [TestClass]
    public class CustomFieldLayoutDefinitionTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/cf/layout";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task TestCustomFieldLayoutDefinitionCRUDOperations()
        {

            #region CREATE

            var testCustomFieldDefinition = Utils.GetCustomFieldLayoutDefinition();
            var createdCustomFieldLayoutDefinition = await EndpointTests.AssertPostStatusCode<CustomFieldLayoutDefinition>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testCustomFieldDefinition), HttpStatusCode.OK);
            
            #endregion

            #region RETRIEVE

            // Test Get All
            var retrievedCustomFieldDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldLayoutDefinition[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedCustomFieldDefinitions);

            //test applies to class Type
            int contactClassID = (int)Endor.Models.ClassType.Contact;
            var contactLayoutDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldLayoutDefinition[]>(client, $"{apiUrl}/?AppliesToClassTypeID={contactClassID}", HttpStatusCode.OK);
            Assert.IsFalse(contactLayoutDefinitions.Any(t => t.ID == createdCustomFieldLayoutDefinition.ID));

            //Test Applies to Int
            int companyClassID = (int)Endor.Models.ClassType.Company;
            var companyDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldDefinition[]>(client, $"{apiUrl}/?AppliesToClassTypeID={companyClassID}&AppliesToID={createdCustomFieldLayoutDefinition.AppliesToClassTypeID}", HttpStatusCode.OK);
            Assert.IsTrue(companyDefinitions.Any(t => t.ID == createdCustomFieldLayoutDefinition.ID));

            // Test Get By ID
            var retrievedCustomFieldLayoutDefinition = await EndpointTests.AssertGetStatusCode<CustomFieldDefinition>(client, $"{apiUrl}/{createdCustomFieldLayoutDefinition.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedCustomFieldLayoutDefinition);

            #endregion

            #region UPDATE

            int invalidID = -1;
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{invalidID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{invalidID}", JsonConvert.SerializeObject(createdCustomFieldLayoutDefinition), HttpStatusCode.NotFound, HttpStatusCode.BadRequest);

            createdCustomFieldLayoutDefinition.SortIndex = 2;
            var testPut = await EndpointTests.AssertPutStatusCode<CustomFieldLayoutDefinition>(client, $"{apiUrl}/{createdCustomFieldLayoutDefinition.ID}", JsonConvert.SerializeObject(createdCustomFieldLayoutDefinition), HttpStatusCode.OK);
            Assert.IsNotNull(testPut);
            Assert.AreEqual(createdCustomFieldLayoutDefinition.SortIndex, testPut.SortIndex);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdCustomFieldLayoutDefinition.ID}", HttpStatusCode.NoContent);
            
            #endregion
        }


        [TestMethod]
        public async Task TestCustomFieldLayoutDefinitionActions()
        {
            var ctx = GetApiContext();
            var testCustomFieldDefinition = Utils.GetCustomFieldDefinition();
            try
            {
            #region CREATE
            var createdCustomFieldLayoutDefinition = await EndpointTests.AssertPostStatusCode<CustomFieldLayoutDefinition>(client, $"{apiUrl}/", testCustomFieldDefinition, HttpStatusCode.OK);

            #endregion

            #region Test Get Active Only
            var retrievedActiveLayoutDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldLayoutDefinition[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            int baseActiveCount = retrievedActiveLayoutDefinitions.Count();
            var retrievedInActiveDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldLayoutDefinition[]>(client, $"{apiUrl}?IsActive=false", HttpStatusCode.OK);
            int baseInActiveCount = retrievedInActiveDefinitions.Count();
            Assert.IsNotNull(retrievedActiveLayoutDefinitions);
            #endregion

            #region Test Actions here
            await EndpointTests.AssertPostStatusCode<CustomFieldLayoutDefinition>(client, $"{apiUrl}/{createdCustomFieldLayoutDefinition.ID}/action/setinactive", JsonConvert.SerializeObject(createdCustomFieldLayoutDefinition), HttpStatusCode.OK);
            retrievedActiveLayoutDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldLayoutDefinition[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(retrievedActiveLayoutDefinitions.Count() == baseActiveCount - 1);
            await EndpointTests.AssertPostStatusCode<CustomFieldLayoutDefinition>(client, $"{apiUrl}/{createdCustomFieldLayoutDefinition.ID}/action/setactive", JsonConvert.SerializeObject(createdCustomFieldLayoutDefinition), HttpStatusCode.OK);
            retrievedActiveLayoutDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldLayoutDefinition[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(retrievedActiveLayoutDefinitions.Count() == baseActiveCount);
            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdCustomFieldLayoutDefinition.ID}", HttpStatusCode.NoContent);

            #endregion
            }
            finally
            {
                ctx.RemoveRange(ctx.CustomFieldDefinition.Where(x => 
                    x.BID == testCustomFieldDefinition.BID && 
                    x.ID == testCustomFieldDefinition.ID
                ).ToArray());
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public async Task TestCustomFieldLayoutDefinitionSavingRelatedEntites()
        {
            #region CREATE from data tree

            var testCustomFieldDefinition = Utils.GetCustomFieldLayoutDefinitionTree();
            var createdCustomFieldLayoutDefinition = await EndpointTests.AssertPostStatusCode<CustomFieldLayoutDefinition>(client, $"{apiUrl}/", testCustomFieldDefinition, HttpStatusCode.OK);
            Assert.IsTrue(createdCustomFieldLayoutDefinition.Elements.Count() > 0);
            Assert.IsTrue(createdCustomFieldLayoutDefinition.Elements.ElementAt(0).Elements.Count() > 0);

            #endregion

            #region Update from data tree
            createdCustomFieldLayoutDefinition.Elements = new HashSet<CustomFieldLayoutElement>();
            var updatedCustomFieldLayoutDefinition = await EndpointTests.AssertPutStatusCode<CustomFieldLayoutDefinition>(client, $"{apiUrl}/{createdCustomFieldLayoutDefinition.ID}", createdCustomFieldLayoutDefinition, HttpStatusCode.OK);
            Assert.AreEqual(0, updatedCustomFieldLayoutDefinition.Elements.Count());
            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdCustomFieldLayoutDefinition.ID}", HttpStatusCode.NoContent);
            ApiContext ctx = GetApiContext(1);

            var containers = await ctx.CustomFieldLayoutElement.Where(e => e.LayoutID == createdCustomFieldLayoutDefinition.ID).ToListAsync();
            Assert.IsNotNull(containers);
            Assert.AreEqual(0, containers.Count());
            #endregion
        }

        [TestMethod]
        public async Task TestCustomFieldLayoutDefinitionSavingRelatedEntitesWithOneContainerAndOneElement()
        {
            #region CREATE from data tree

            var newCustomFieldLayoutDef = Utils.GetCustomFieldLayoutDefinitionTreeWithOneContainerAndOneElement();
            var createdCustomFieldLayoutDefinition = await EndpointTests.AssertPostStatusCode<CustomFieldLayoutDefinition>(client, $"{apiUrl}/", newCustomFieldLayoutDef, HttpStatusCode.OK);
            Assert.IsTrue(createdCustomFieldLayoutDefinition.Elements.Count() > 0);
            Assert.IsTrue(createdCustomFieldLayoutDefinition.Elements.ElementAt(0).Elements.Count() > 0);

            #endregion

            #region Update from data tree
            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdCustomFieldLayoutDefinition.ID}", HttpStatusCode.NoContent);

            #endregion
        }

    }
}
