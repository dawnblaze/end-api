﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Endor.AEL;
using Newtonsoft.Json;
using System.Net;
using System.Linq;
using Endor.Models;
using Endor.EF;

namespace Endor.Api.Tests
{
    [TestClass]
    public class SystemLanguageEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/Api/System/translate";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        [TestMethod]
        public async Task TestGetSystemLanguages()
        {
            var result = await EndpointTests.AssertGetStatusCode<List<SystemLanguageType>>(client, $"{apiUrl}/languages", HttpStatusCode.OK);
            Assert.IsTrue(result.Count >= 1);
            Assert.IsTrue(result.Where(x => x.Name == "English").Any());
        }

        [TestMethod]
        public async Task TestGetLanguageDictionary()
        {
            byte languageID = 0;
            string languageCode = "en";
            string KEY = "TEST_TEST_TEST";
            var ctx = GetApiContext();
            AddValueToDictionary(ctx, languageID, KEY);


            var result = await EndpointTests.AssertGetStatusCode<Dictionary<string, string>>(client, $"{apiUrl}/dict/?lang={languageCode}", HttpStatusCode.OK);
            Assert.IsTrue(result.Count >= 1);
            Assert.IsTrue(result.ContainsKey(KEY));
            var result2 = await EndpointTests.AssertGetStatusCode<Dictionary<string, string>>(client, $"{apiUrl}/dict?lang={languageCode}&unverified=true", HttpStatusCode.OK);
            Assert.IsFalse(result2.ContainsKey(KEY));
            var result3 = await EndpointTests.AssertGetStatusCode<Dictionary<string, string>>(client, $"{apiUrl}/dict?lang={languageCode}&unverified=false", HttpStatusCode.OK);
            Assert.IsTrue(result3.ContainsKey(KEY));
            //delete test sample from db
            var existingValue = ctx.SystemTranslationDefinitions.Where(x => x.LanguageTypeId == languageID && x.SourceText == KEY);
            ctx.SystemTranslationDefinitions.RemoveRange(existingValue);
            ctx.SaveChanges();
        }

        [TestMethod]
        public async Task TestGetLanguageValue()
        {
            byte languageID = 0;
            string languageCode = "en";
            string KEY = "TEST_TEST_TEST";
            var ctx = GetApiContext();
            AddValueToDictionary(ctx, languageID, KEY);

            //Test for Google. Comment out normally.
            //var result = await EndpointTests.AssertGetStatusCode<Dictionary<string, string>>(client, $"{apiUrl}/single?lang=es&text=Search", HttpStatusCode.OK);
            var result = await EndpointTests.AssertGetStatusCode<Dictionary<string, string>>(client, $"{apiUrl}/single/{languageCode}?text={KEY}", HttpStatusCode.OK);
            Assert.IsTrue(result.Count >= 1);
            Assert.IsTrue(result.ContainsKey(KEY));
            // delete test sample from db
            var existingValue = ctx.SystemTranslationDefinitions.Where(x => x.LanguageTypeId == languageID && x.SourceText == KEY);
            ctx.SystemTranslationDefinitions.RemoveRange(existingValue);
            ctx.SaveChanges();
        }

        [TestMethod]
        public async Task TestPostLanguageDictionary()
        {
            byte languageID = 0;
            string languageCode = "en";
            string KEY = "TEST_TEST_TEST";
            string KEY2 = "TEST_TEST_TEST_TEST";
            string Value = "Test Value";
            string Value2 = "{\"\":\"Test Value\",\"ALT\":\"TEST Value2\"}";
            //add test sample to db
            var ctx = GetApiContext();
            var existingValue = ctx.SystemTranslationDefinitions.Where(x => x.LanguageTypeId == languageID && x.SourceText == KEY);
            var found = existingValue.Any();
            if (found)
                ctx.SystemTranslationDefinitions.RemoveRange(existingValue);
            existingValue = ctx.SystemTranslationDefinitions.Where(x => x.LanguageTypeId == languageID && x.SourceText == KEY2);
            found = existingValue.Any();
            if (found)
                ctx.SystemTranslationDefinitions.RemoveRange(existingValue);
            ctx.SaveChanges();
            var dict = new Dictionary<string, string>();
            dict.Add(KEY, Value);
            dict.Add(KEY2, Value2);
            //Action
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/upload?lang={languageCode}", dict, HttpStatusCode.OK);
            //clean up
            existingValue = ctx.SystemTranslationDefinitions.Where(x => x.LanguageTypeId == languageID && x.SourceText == KEY);
            Assert.IsTrue(existingValue.Any());
            ctx.SystemTranslationDefinitions.RemoveRange(existingValue);
            existingValue = ctx.SystemTranslationDefinitions.Where(x => x.LanguageTypeId == languageID && x.SourceText == KEY2);
            Assert.IsTrue(existingValue.Any());
            ctx.SystemTranslationDefinitions.RemoveRange(existingValue);
            ctx.SaveChanges();

        }

        [TestMethod]
        public async Task TestDeleteLanguageDictionary()
        {
            byte languageID = 0;
            string languageCode = "en";
            string KEY = "TEST_TEST_TEST";
            var ctx = GetApiContext();
            AddValueToDictionary(ctx, languageID, KEY);
            var result = await EndpointTests.AssertDeleteStatusCode<Dictionary<string, string>>(client, $"{apiUrl}/delete?lang={languageCode}&text={KEY}", HttpStatusCode.OK);
            //delete test sample from db
            var newValue = ctx.SystemTranslationDefinitions.Where(x => x.LanguageTypeId == languageID && x.SourceText == KEY);
            Assert.IsFalse(newValue.Any());
        }

        private void AddValueToDictionary(ApiContext ctx, byte languageID, string KEY)
        {
            string Value = "Test Value";
            //add test sample to db
            var existingValue = ctx.SystemTranslationDefinitions.Where(x => x.LanguageTypeId == languageID && x.SourceText == KEY);
            var found = existingValue.Any();
            var entry = existingValue.FirstOrDefault();
            if (!found)
            {
                entry = new SystemTranslationDefinition
                {
                    LanguageTypeId = languageID,
                    SourceText = KEY,
                    TranslatedText = Value,
                    IsVerified = true
                };
                ctx.SystemTranslationDefinitions.Add(entry);
                ctx.SaveChanges();
            }
        }

    }
}
