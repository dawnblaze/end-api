﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Pricing;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web;
using System.Net.Http.Headers;
using Acheve.AspNetCore.TestHost.Security;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Endor.Tenant;
using Microsoft.Extensions.DependencyInjection;
using Endor.EF;

namespace Endor.Api.Tests
{
    [TestClass]
    public class AuthorizationTest : CommonControllerTestClass
    {

        public static string cText =
        "-----BEGIN CERTIFICATE-----\n" +
        "MIIBljCCAUACCQCIDMpqK7WfWDANBgkqhkiG9w0BAQsFADBSMQswCQYDVQQGEwJV\n" +
        "UzETMBEGA1UECAwKU29tZS1TdGF0ZTESMBAGA1UECgwJTHV4b3R0aWNhMRowGAYD\n" +
        "VQQLDBFMdXhvdHRpY2EgZXllY2FyZTAeFw0xODA1MjMxNTE1MjdaFw0yODA1MjAx\n" +
        "NTE1MjdaMFIxCzAJBgNVBAYTAlVTMRMwEQYDVQQIDApTb21lLVN0YXRlMRIwEAYD\n" +
        "VQQKDAlMdXhvdHRpY2ExGjAYBgNVBAsMEUx1eG90dGljYSBleWVjYXJlMFwwDQYJ\n" +
        "KoZIhvcNAQEBBQADSwAwSAJBAKuMYcirPj81WBtMituJJenF0CG/HYLcAUOtWKl1\n" +
        "HchC0dM8VRRBI/HV+nZcweXzpjhX8ySa9s7kJneP0cuJiU8CAwEAATANBgkqhkiG\n" +
        "9w0BAQsFAANBAKEM8wQwlqKgkfqnNFcbsZM0RUxS+eWR9LvycGuMN7aL9M6GOmfp\n" +
        "QmF4MH4uvkaiZenqCkhDkyi4Cy81tz453tQ=\n" +
        "-----END CERTIFICATE-----";
        public const string securityUrl = "/security";
        public const string employeeApiUrl = "/api/employee";
        public const string locationApiUrl = "/api/location";
        public const string employeeTeamApiUrl = "/api/employeeteam";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public const short userIDEmployeeUser = -100;
        public const short userIDEmployeeAdmin = -99;
        ApiContext ctx;

        [TestInitialize]
        public async Task TestStart()
        {
            ctx = GetApiContext();
            await TestCleanup();
            var testUserLink = new UserLink()
            {
                BID = TestConstants.BID,
                ID = userIDEmployeeUser,
                AuthUserID = userIDEmployeeUser,
                EmployeeID = ctx.EmployeeData.Where(e => e.BID == TestConstants.BID && e.IsActive && e.ID > 0).OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault(),
                UserAccessType = UserAccessType.Employee
            };
            var testUserLink2 = new UserLink()
            {
                BID = TestConstants.BID,
                ID = userIDEmployeeAdmin,
                AuthUserID = userIDEmployeeAdmin,
                EmployeeID = ctx.EmployeeData.Where(e => e.BID == TestConstants.BID && e.IsActive && e.ID > 0).OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault(),
                UserAccessType = UserAccessType.EmployeeAdministrator
            };

            ctx.UserLink.Add(testUserLink);
            ctx.UserLink.Add(testUserLink2);
            await ctx.SaveChangesAsync();
        }

        [TestCleanup]
        public async Task TestCleanup()
        {
            ctx.UserLink.RemoveRange(ctx.UserLink.Where(x => x.ID<0));
            await ctx.SaveChangesAsync();
        }

        [TestMethod]
        public async Task TestJWTExpiry()
        {
            var request = new Request<AuthorizeStartup>();
            byte[] bytes = new byte[128];
            var token = request.Jwt.GenerateToken(Convert.ToBase64String(bytes),0);
            //var resp = await request.AddAuth(Token).Get(employeeApiUrl);
            await EndpointTests.AssertGetStatusCode(request.AddAuth(token).client, employeeApiUrl, HttpStatusCode.Unauthorized);           
        }

        [TestMethod]
        public async Task TestLocationAuthorization()
        {
            HttpResponseMessage getRightsResponse = await client.GetAsync($"{securityUrl}?bid={TestConstants.BID}&userid={userIDEmployeeUser}");
            string responseString = await getRightsResponse.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<BusinessRightsAndIDs>(responseString);

            var employeeUserRequest = new Request<AuthorizeStartup>();
            var emmployeeUser = employeeUserRequest.Jwt.GenerateToken(result.rights);

            getRightsResponse = await client.GetAsync($"{securityUrl}?bid={TestConstants.BID}&userid={userIDEmployeeAdmin}");
            responseString = await getRightsResponse.Content.ReadAsStringAsync();
            result = JsonConvert.DeserializeObject<BusinessRightsAndIDs>(responseString);
            var employeeAdminRequest = new Request<AuthorizeStartup>();
            var employeeAdmin = employeeAdminRequest.Jwt.GenerateToken(result.rights);

            var loc = new LocationData()
            {
                BID = TestConstants.BID,
                Name = "Test"+Guid.NewGuid().ToString(),
                LegalName = "createLocationTest",
                IsActive = false,
                IsDefault = false
            };

            // EmployeeAdmin User Can Create New Location
            var newLoc = await EndpointTests.AssertPostStatusCode<LocationData>(employeeAdminRequest.AddAuth(employeeAdmin).client, locationApiUrl, JsonConvert.SerializeObject(loc), HttpStatusCode.OK);

            // EmployeeAdmin User Can Edit Location
            await EndpointTests.AssertPutStatusCode(employeeAdminRequest.client, $"{locationApiUrl}/{newLoc.ID}", newLoc, HttpStatusCode.OK);

            // EmployeeAdmin Fails when Deleting a Location
            var test = await EndpointTests.AssertDeleteStatusCode(employeeAdminRequest.AddAuth(employeeAdmin).client, $"{locationApiUrl}/{newLoc.ID}?force=true", HttpStatusCode.Forbidden);

            // Employee User Can't retrieve Location Details
            await EndpointTests.AssertGetStatusCode(employeeUserRequest.AddAuth(emmployeeUser).client, $"{locationApiUrl}/{newLoc.ID}", HttpStatusCode.Forbidden);

            // Employee User Fails when Creating a new Location
            await EndpointTests.AssertPostStatusCode(employeeUserRequest.AddAuth(emmployeeUser).client, locationApiUrl, JsonConvert.SerializeObject(loc), HttpStatusCode.Forbidden);

            //Employee User Fails when Editing a Location
            await EndpointTests.AssertPutStatusCode(employeeUserRequest.AddAuth(emmployeeUser).client, $"{locationApiUrl}/{newLoc.ID}", newLoc, HttpStatusCode.Forbidden);

            //cleanup
            ctx.LocationData.RemoveRange(ctx.LocationData.Where(x => x.BID == TestConstants.BID && x.ID == newLoc.ID));
            await ctx.SaveChangesAsync();

        }

        [TestMethod]
        public async Task TestEmployeeAuthorization()
        {
            HttpResponseMessage getRightsResponse = await client.GetAsync($"{securityUrl}?bid={TestConstants.BID}&userid={userIDEmployeeUser}");
            string responseString = await getRightsResponse.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<BusinessRightsAndIDs>(responseString);

            var employeeUserRequest = new Request<AuthorizeStartup>();
            var emmployeeUser = employeeUserRequest.Jwt.GenerateToken(result.rights);

            getRightsResponse = await client.GetAsync($"{securityUrl}?bid={TestConstants.BID}&userid={userIDEmployeeAdmin}");
            responseString = await getRightsResponse.Content.ReadAsStringAsync();
            result = JsonConvert.DeserializeObject<BusinessRightsAndIDs>(responseString);
            var employeeAdminRequest = new Request<AuthorizeStartup>();
            var employeeAdmin = employeeAdminRequest.Jwt.GenerateToken(result.rights);

            var simpleLocations = await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, $"/api/location/SimpleList", HttpStatusCode.OK);
            byte locationID = simpleLocations.FirstOrDefault().ID;

            var emp = new EmployeeData()
            {
                BID = 1,
                IsActive = true,
                First = "John",
                Last = "Doe",
                DefaultModule = Module.Accounting,
                LocationID = locationID
            };

            // EmployeeAdmin User Can Create New Employee
            var newEmp = await EndpointTests.AssertPostStatusCode<EmployeeData>(employeeAdminRequest.AddAuth(employeeAdmin).client, employeeApiUrl, JsonConvert.SerializeObject(emp), HttpStatusCode.OK);

            // EmployeeAdmin User Can Edit Employee
            await EndpointTests.AssertPutStatusCode(employeeAdminRequest.client, $"{employeeApiUrl}/{newEmp.ID}", newEmp, HttpStatusCode.OK);

            // Employee User Can't retrieve Employee Details
            await EndpointTests.AssertGetStatusCode(employeeUserRequest.AddAuth(emmployeeUser).client, $"{employeeApiUrl}/{newEmp.ID}", HttpStatusCode.Forbidden);

            // Employee User Fails when Creating a new Employee
            await EndpointTests.AssertPostStatusCode(employeeUserRequest.AddAuth(emmployeeUser).client, employeeApiUrl, JsonConvert.SerializeObject(emp), HttpStatusCode.Forbidden);

            // Employee User Fails when Editing a Employee
            await EndpointTests.AssertPutStatusCode(employeeUserRequest.AddAuth(emmployeeUser).client, $"{employeeApiUrl}/{newEmp.ID}", newEmp, HttpStatusCode.Forbidden);

            // EmployeeAdmin Fails when Deleting a Employee
            var test = await EndpointTests.AssertDeleteStatusCode(employeeAdminRequest.AddAuth(employeeAdmin).client, $"{employeeApiUrl}/{newEmp.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestEmployeeTeamAuthorization()
        {
            HttpResponseMessage getRightsResponse = await client.GetAsync($"{securityUrl}?bid={TestConstants.BID}&userid={userIDEmployeeUser}");
            string responseString = await getRightsResponse.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<BusinessRightsAndIDs>(responseString);

            var employeeUserRequest = new Request<AuthorizeStartup>();
            var emmployeeUser = employeeUserRequest.Jwt.GenerateToken(result.rights);

            getRightsResponse = await client.GetAsync($"{securityUrl}?bid={TestConstants.BID}&userid={userIDEmployeeAdmin}");
            responseString = await getRightsResponse.Content.ReadAsStringAsync();
            result = JsonConvert.DeserializeObject<BusinessRightsAndIDs>(responseString);
            var employeeAdminRequest = new Request<AuthorizeStartup>();
            var employeeAdmin = employeeAdminRequest.Jwt.GenerateToken(result.rights);

            var emp = new EmployeeTeam()
            {
                BID = TestConstants.BID,
                Name = "UNIT TEST TEAM",
                IsActive = true
            };

            // EmployeeAdmin User Can Create New Employee Team
            var newEmp = await EndpointTests.AssertPostStatusCode<EmployeeTeam>(employeeAdminRequest.AddAuth(employeeAdmin).client, employeeTeamApiUrl, JsonConvert.SerializeObject(emp), HttpStatusCode.OK);

            // EmployeeAdmin User Can Edit Employee Team
            await EndpointTests.AssertPutStatusCode(employeeAdminRequest.client, $"{employeeTeamApiUrl}/{newEmp.ID}", newEmp, HttpStatusCode.OK);

            // Employee User Can't retrieve Employee Team Details
            await EndpointTests.AssertGetStatusCode(employeeUserRequest.AddAuth(emmployeeUser).client, $"{employeeTeamApiUrl}/{newEmp.ID}", HttpStatusCode.Forbidden);

            // Employee User Fails when Creating a new Employee Team
            await EndpointTests.AssertPostStatusCode(employeeUserRequest.AddAuth(emmployeeUser).client, employeeTeamApiUrl, JsonConvert.SerializeObject(emp), HttpStatusCode.Forbidden);

            // Employee User Fails when Editing a Employee Team
            await EndpointTests.AssertPutStatusCode(employeeUserRequest.AddAuth(emmployeeUser).client, $"{employeeTeamApiUrl}/{newEmp.ID}", newEmp, HttpStatusCode.Forbidden);

            // EmployeeAdmin Succeeds when Deleting a Employee Team
            await EndpointTests.AssertDeleteStatusCode(employeeAdminRequest.AddAuth(employeeAdmin).client, $"{employeeTeamApiUrl}/{newEmp.ID}", HttpStatusCode.NoContent);

        }
    }

    public class BusinessRightsAndIDs
    {
        public string accessType;
        public string rights;
        public short? employeeID;
        public int? contactID;
        public short? userlinkID;
    }
    public class Request<TStartup> : IDisposable where TStartup : class
    {
        public readonly HttpClient client;
        private readonly TestServer server;

        public Request()
        {
            string contentRoot = Environment.CurrentDirectory;
            contentRoot = contentRoot.Substring(0, contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\', contentRoot.LastIndexOf('\\') - 1) - 1) - 1)) + "\\Endor.Api.Web\\";
            var builder = new WebHostBuilder()
              .UseContentRoot(contentRoot)
              .UseEnvironment("Development")
              .UseStartup<TStartup>(); 
            this.server = new TestServer(builder);
            this.client = server.CreateClient();
        }

        public JwtAuthentication Jwt => new JwtAuthentication();

        public Request<TStartup> AddAuth(string token)
        {
            this.client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            return this;
        }

        public void Dispose()
        {
            client.Dispose();
            server.Dispose();
        }
    }

    public class JwtAuthentication
    {
        public JwtAuthentication()
        {
        }

        public string GenerateToken(string rightsStr, int hoursTillExpiry = 2)//(User user)
        {
            var claims = new List<Claim>()
            {
                new Claim(ClaimNameConstants.BID,        TestConstants.BID.ToString()),
                new Claim(ClaimNameConstants.AID,        TestConstants.AID.ToString()),
                new Claim(ClaimNameConstants.Rights, rightsStr)
            };
            return GetJwtToken(claims, hoursTillExpiry);
        }

        private string GetJwtToken(IEnumerable<Claim> claims,int hoursTillExpiry)
        {
            
            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AuthorizationTest.cText));
            var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha512);

            var tokeOptions = new JwtSecurityToken(
                issuer: "Test",//validIssuer,
                audience: "Test", //validAudience,
                claims: claims,
                notBefore: DateTime.UtcNow.AddHours(-1),
                expires: DateTime.UtcNow.AddHours(-1+ hoursTillExpiry),
                signingCredentials: signinCredentials
            );

            return new JwtSecurityTokenHandler().WriteToken(tokeOptions);
        }
    }

    public class AuthorizeStartup : TestStartup
    {

        public AuthorizeStartup(IWebHostEnvironment env) : base(env)
        {
        }
        public override void AddAuthentication(IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "Bearer";
            }).AddJwtBearer(options =>
            {
                //options.Authority = "Test";
                options.Audience = "Test";
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = false,
                    //ValidIssuer = Configuration["JwtToken:Issuer"],
                    //ValidAudience = Configuration["JwtToken:Issuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AuthorizationTest.cText))
                };
            })
            .AddTestServerAuthentication();
        }
    }
}
