﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Tests
{
    [TestClass]
    public class CompanyEndpointTests: CommonControllerTestClass
    {
        public const string apiUrl = "/api/company";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestCompanyName;
        public TestContext TestContext { get; set; }
        
        [TestInitialize]
        public override void Init()
        {
            base.Init();
            this.NewTestCompanyName = DateTime.UtcNow + " TEST COMPANY";
        }

        
        [TestMethod]
        public async Task TestCreateCompanyWithContactLinksWithExistingContactsInDb()
        {
            short testBID = TestConstants.BID;
            var ctx = GetApiContext(testBID);
            try
            {
                #region Remove if Existing Data

                var negativeLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (negativeLinks != null)
                {
                    ctx.RemoveRange(negativeLinks);
                    await ctx.SaveChangesAsync();
                }

                var negativeContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (negativeContacts != null)
                {
                    ctx.RemoveRange(negativeContacts);
                    await ctx.SaveChangesAsync();
                }

                var negativeCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (negativeCompanies != null)
                {
                    ctx.RemoveRange(negativeCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                #region Create Data

                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var contactA = new ContactData()
                {
                    ID = -91,
                    First = "CompanyA First",
                    Last = "LastTest1A CompanyA",
                    BID = TestConstants.BID
                };
                var contactB = new ContactData()
                {
                    ID = -92,
                    First = "CompanyA First",
                    Last = "LastTest2A CompanyA",
                    BID = TestConstants.BID
                };
                var contactC = new ContactData()
                {
                    ID = -81,
                    First = "CompanyB First",
                    Last = "LastTest1B",
                    BID = TestConstants.BID
                };
                var contactD = new ContactData()
                {
                    ID = -82,
                    First = "CompanyB First",
                    Last = "LastTest2B",
                    BID = TestConstants.BID
                };

                ctx.ContactData.AddRange(new[] { contactA, contactB, contactC, contactD });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                #endregion

                var newCompany = new CompanyData()
                {
                    Name = "TEST Company A",
                    BID = TestConstants.BID,
                    IsActive = true,
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1,
                    StatusText = "TEST",
                    CompanyContactLinks = new List<CompanyContactLink>()
                    {
                        new CompanyContactLink()
                        {
                            BID = TestConstants.BID,
                            ContactID = -82,
                            Roles = ContactRole.Primary
                        },
                        new CompanyContactLink()
                        {
                            BID = TestConstants.BID,
                            ContactID = -81,
                            Roles = ContactRole.Billing
                        }
                    }
                };

                var companyAResponse = await EndpointTests.AssertPostStatusCode<CompanyData>(client, $"{apiUrl}", JsonConvert.SerializeObject(newCompany), HttpStatusCode.OK);
                Assert.IsNotNull(companyAResponse);

                var testCtx = GetApiContext(testBID);
                var companyContactLinks = await testCtx.CompanyContactLink.Where(x => x.CompanyID == companyAResponse.ID).ToListAsync();
                Assert.AreEqual(2, companyContactLinks.Count);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);
                else
                    Assert.Fail(ex.Message);
            }
            finally
            {
                #region Cleanup Data
                await this.CleanUpCompanyContactAndContactLinks(testBID);
                #endregion
            }
        }


        [TestMethod]
        public async Task TestDeleteCompanyDeletesContactWithoutOtherCompaniesAssociated()
        {
            short testBID = TestConstants.BID;
            var ctx = GetApiContext(testBID);
            try
            {
                #region Remove if Existing Data

                var negativeLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (negativeLinks != null)
                {
                    ctx.RemoveRange(negativeLinks);
                    await ctx.SaveChangesAsync();
                }

                var negativeContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (negativeContacts != null)
                {
                    ctx.RemoveRange(negativeContacts);
                    await ctx.SaveChangesAsync();
                }

                var negativeCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (negativeCompanies != null)
                {
                    ctx.RemoveRange(negativeCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                #region Create Data

                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var contactA = new ContactData()
                {
                    ID = -91,
                    First = "CompanyA",
                    Last = "LastTest1A CompanyA",
                    BID = TestConstants.BID
                };
                var contactB = new ContactData()
                {
                    ID = -92,
                    First = "CompanyA",
                    Last = "LastTest2A CompanyA",
                    BID = TestConstants.BID
                };
                var contactC = new ContactData()
                {
                    ID = -81,
                    First = "CompanyB",
                    Last = "LastTest1B",
                    BID = TestConstants.BID
                };
                var contactD = new ContactData()
                {
                    ID = -82,
                    First = "CompanyB",
                    Last = "LastTest2B",
                    BID = TestConstants.BID
                };

                ctx.ContactData.AddRange(new[] { contactA, contactB, contactC, contactD });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var companyA = new CompanyData()
                {
                    ID = -90,
                    Name = "Company A",
                    BID = TestConstants.BID,
                    IsActive = true,
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };

                ctx.CompanyData.Add(companyA);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var contactLinksForCompanyA = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        ContactID = -91,
                        CompanyID = companyA.ID
                    },
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        ContactID = -92,
                        CompanyID = companyA.ID
                    }
                };
                ctx.CompanyContactLink.AddRange(contactLinksForCompanyA);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                #endregion

                var companyAResponse = await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{companyA.ID}", HttpStatusCode.NoContent);
                Assert.IsNotNull(companyAResponse);

                var company = await EndpointTests.AssertGetStatusCode<CompanyData>(client, $"{apiUrl}/{companyA.ID}", HttpStatusCode.NotFound);
                Assert.IsNull(company);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);
                else
                    Assert.Fail(ex.Message);
            }
            finally
            {
                #region Cleanup Data
                await this.CleanUpCompanyContactAndContactLinks(testBID);
                #endregion
            }
        }

        [TestMethod]
        public async Task TestDeleteCompanyDeletesContactsWithOhterCompaniesAssociated()
        {
            short testBID = TestConstants.BID;
            var ctx = GetApiContext(testBID);

            try
            {
                #region Remove if Existing Data

                var negativeLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (negativeLinks != null)
                {
                    ctx.RemoveRange(negativeLinks);
                    await ctx.SaveChangesAsync();
                }

                var negativeContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (negativeContacts != null)
                {
                    ctx.RemoveRange(negativeContacts);
                    await ctx.SaveChangesAsync();
                }

                var negativeCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (negativeCompanies != null)
                {
                    ctx.RemoveRange(negativeCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                #region Create Data

                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var contactA = new ContactData()
                {
                    ID = -91,
                    First = "CompanyA",
                    Last = "LastTest1A CompanyA",
                    BID = TestConstants.BID
                };
                var contactB = new ContactData()
                {
                    ID = -92,
                    First = "CompanyA",
                    Last = "LastTest2A CompanyA",
                    BID = TestConstants.BID
                };

                ctx.ContactData.AddRange(new[] { contactA, contactB });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var companyA = new CompanyData()
                {
                    ID = -90,
                    Name = "Company A",
                    BID = TestConstants.BID,
                    IsActive = true,
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                ctx.CompanyData.Add(companyA);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var companyB = new CompanyData()
                {
                    ID = -80,
                    Name = "Company B",
                    BID = TestConstants.BID,
                    IsActive = true,
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                ctx.CompanyData.Add(companyB);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var contactLinksForCompanyA = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        ContactID = contactA.ID,
                        CompanyID = companyA.ID
                    },
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        ContactID = contactA.ID,
                        CompanyID = companyB.ID
                    },
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        ContactID = contactB.ID,
                        CompanyID = companyA.ID
                    }
                };
                ctx.CompanyContactLink.AddRange(contactLinksForCompanyA);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                #endregion

                var companyAResponse = await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{companyA.ID}", HttpStatusCode.NoContent);
                Assert.IsNotNull(companyAResponse);

                var company = await EndpointTests.AssertGetStatusCode<CompanyData>(client, $"{apiUrl}/{companyA.ID}", HttpStatusCode.NotFound);
                Assert.IsNull(company);

                var contactCompanyLinks = await EndpointTests.AssertGetStatusCode<CompanyContactLink[]>(client, $"{apiUrl}/Contact/{contactA.ID}/CompanyLinks", HttpStatusCode.OK);
                Assert.IsNotNull(contactCompanyLinks);
                Assert.AreEqual(1, contactCompanyLinks.ToList().Count);

                await EndpointTests.AssertGetStatusCode<CompanyContactLink[]>(client, $"Api/Contact/{contactB.ID}", HttpStatusCode.NotFound);
                await EndpointTests.AssertGetStatusCode<CompanyContactLink[]>(client, $"{apiUrl}/Contact/{contactB.ID}/CompanyLinks", HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);
                else
                    Assert.Fail(ex.Message);
            }
            finally
            {
                #region Cleanup Data
                await this.CleanUpCompanyContactAndContactLinks(testBID);
                #endregion
            }
        }

        [TestMethod]
        public async Task TestPutCompanyChangesToContactsPersistsCorrectly()
        {
            short testBID = TestConstants.BID;
            var ctx = GetApiContext(testBID);
            try
            {
                #region Remove if Existing Data

                var negativeLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (negativeLinks != null)
                {
                    ctx.RemoveRange(negativeLinks);
                    await ctx.SaveChangesAsync();
                }

                var negativeContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (negativeContacts != null)
                {
                    ctx.RemoveRange(negativeContacts);
                    await ctx.SaveChangesAsync();
                }

                var negativeCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (negativeCompanies != null)
                {
                    ctx.RemoveRange(negativeCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                #region Create Data
                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var contactA = new ContactData()
                {
                    ID = -91,
                    First = "CompanyA",
                    Last = "LastTest1A CompanyA",
                    BID = TestConstants.BID
                };
                var contactB = new ContactData()
                {
                    ID = -92,
                    First = "CompanyA",
                    Last = "LastTest2A CompanyA",
                    BID = TestConstants.BID
                };
                var contactC = new ContactData()
                {
                    ID = -81,
                    First = "CompanyB",
                    Last = "LastTest1B",
                    BID = TestConstants.BID
                };
               

                ctx.ContactData.AddRange(new[] { contactA, contactB, contactC });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var companyA = new CompanyData()
                {
                    ID = -90,
                    Name = "Test Company A",
                    BID = TestConstants.BID,
                    IsActive = true,
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                ctx.CompanyData.Add(companyA);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);


                var contactLinksForCompanyA = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        ContactID = contactA.ID,
                        CompanyID = companyA.ID,
                        Roles = (ContactRole.Primary | ContactRole.Billing)
                    },
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        ContactID = contactB.ID,
                        CompanyID = companyA.ID,
                        Roles = (ContactRole.Inactive)
                    }
                };
                ctx.CompanyContactLink.AddRange(contactLinksForCompanyA);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                #endregion

                // make a get request company
                var companyAGetResponse = await EndpointTests.AssertGetStatusCode<CompanyData>(client, $"{apiUrl}/{companyA.ID}", HttpStatusCode.OK);
                Assert.IsNotNull(companyAGetResponse);
                Assert.AreEqual(2, companyAGetResponse.CompanyContactLinks.Count);

                // update role
                companyAGetResponse.CompanyContactLinks = companyAGetResponse.CompanyContactLinks.Select(x =>
                {
                    if (x.ContactID == contactA.ID)
                    {
                        x.Roles = ContactRole.Primary;
                    }
                    return x;
                }).ToList();

                // add a role
                companyAGetResponse.CompanyContactLinks.Add(new CompanyContactLink()
                {
                    BID = TestConstants.BID,
                    ContactID = contactC.ID,
                    CompanyID = companyA.ID,
                    Roles = (ContactRole.Billing)
                });

                // make a PUT request to update company
                var companyAPutResponse = await EndpointTests.AssertPutStatusCode<CompanyData>(client, $"{apiUrl}/{companyAGetResponse.ID}", JsonConvert.SerializeObject(companyAGetResponse), HttpStatusCode.OK);
                Assert.IsNotNull(companyAPutResponse);
                Assert.AreEqual(3, companyAPutResponse.CompanyContactLinks.Count);

                // modify company
                companyAPutResponse.CompanyContactLinks = companyAPutResponse.CompanyContactLinks.Where(x => x.ContactID != contactA.ID).ToList();
                companyAPutResponse.CompanyContactLinks = companyAPutResponse.CompanyContactLinks.Select(x => { 
                    if(x.ContactID == contactB.ID)
                    {
                        x.Roles = ContactRole.Primary;
                    }
                    return x; 
                }).ToList();

                // make another PUT request to update company
                var companyAPutResponse2 = await EndpointTests.AssertPutStatusCode<CompanyData>(client, $"{apiUrl}/{companyAGetResponse.ID}", JsonConvert.SerializeObject(companyAPutResponse), HttpStatusCode.OK);
                Assert.IsNotNull(companyAPutResponse2);
                Assert.AreEqual(2, companyAPutResponse2.CompanyContactLinks.Count);
                Assert.AreEqual(ContactRole.Primary, companyAPutResponse2.CompanyContactLinks.FirstOrDefault(x => x.ContactID == contactB.ID).Roles);
                Assert.AreEqual(ContactRole.Billing, companyAPutResponse2.CompanyContactLinks.FirstOrDefault(x => x.ContactID == contactC.ID).Roles);

                // check contact links of contact a
                await EndpointTests.AssertGetStatusCode<CompanyContactLink[]>(client, $"{apiUrl}/Contact/{contactA.ID}/CompanyLinks", HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);
                else
                    Assert.Fail(ex.Message);
            }
            finally
            {
                #region Cleanup Data
                await this.CleanUpCompanyContactAndContactLinks(testBID);
                #endregion
            }
        }

        private async Task CleanUpCompanyContactAndContactLinks(short testBID)
        {
            var cleanupCtx = GetApiContext(testBID);
            var cleanupLinks = await cleanupCtx.CompanyContactLink.Where(x => x.ContactID <= 0 || x.CompanyID <= 0).ToListAsync();
            if (cleanupLinks != null)
            {
                cleanupCtx.RemoveRange(cleanupLinks);
                await cleanupCtx.SaveChangesAsync();
            }
            var cleanupContacts = await cleanupCtx.ContactData.Where(x => x.ID <= 0 || x.First.Contains("TEST")).ToListAsync();
            if (cleanupContacts != null)
            {
                cleanupCtx.RemoveRange(cleanupContacts);
                await cleanupCtx.SaveChangesAsync();
            }
            var cleanupCompanies = await cleanupCtx.CompanyData.Where(x => x.ID <= 0 || x.Name.Contains("TEST")).ToListAsync();
            if (cleanupCompanies != null)
            {
                cleanupCtx.RemoveRange(cleanupCompanies);
                await cleanupCtx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task TestCompanyWithInvalidLocationID()
        {
            var ctx = GetApiContext();

            // where LocationID can't be found
            var locationID = (await ctx.LocationData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID)).ID;
            locationID += 2; // change this depending on your db location's available IDs

            CompanyData acntNameTest = new CompanyData()
            {
                Name = this.NewTestCompanyName,
                IsActive = true,
                BID = TestConstants.BID,
                LocationID = locationID 
            };

            var response = await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(acntNameTest), HttpStatusCode.BadRequest);
            Assert.IsNotNull(response);
            var errorContent = await response.Content.ReadAsStringAsync();
            Assert.IsNotNull(errorContent);
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(errorContent);
            Assert.AreEqual(result.Last().Value, "Invalid LocationID.");
        }

        [TestMethod]
        public async Task TestCompanySingleEndpoints()
        {
            short testBID = TestConstants.BID;
            var ctx = GetApiContext(testBID);

            var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
            if (location == null)
                Assert.Inconclusive("No location data found. Unable to complete test.");

            CompanyData acntNameTest = new CompanyData()
            {
                ID = -90,
                Name = "TEST Company A",
                BID = TestConstants.BID,
                IsActive = true,
                LocationID = location.ID,
                IsAdHoc = false,
                StatusID = 1
            };

            //test: expect OK
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(acntNameTest), HttpStatusCode.OK);

            // GET api/Companys/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleCompanyData sga in simpleList)
            {
                if (sga.DisplayName == acntNameTest.Name)
                {
                    acntNameTest.ID = sga.ID;
                }
            }

            //POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{acntNameTest.ID}/action/setinactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{acntNameTest.ID}/action/setactive", HttpStatusCode.OK);

            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + acntNameTest.ID, JsonConvert.SerializeObject(acntNameTest), HttpStatusCode.OK);

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{acntNameTest.ID}", HttpStatusCode.NoContent);
            
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/9241657", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestCloneCompanyWithContactLinks()
        {
            short testBID = TestConstants.BID;
            var ctx = GetApiContext(testBID);
            try
            {
                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                #region Create

                var contactA = new ContactData
                {
                    ID = -99,
                    First = "Test First A",
                    Last = "Test Last A",
                    BID = TestConstants.BID
                };
                var contactB = new ContactData
                {
                    ID = -98,
                    First = "Test First B",
                    Last = "Test Last B",
                    BID = TestConstants.BID
                };
                ctx.ContactData.AddRange(new[] { contactA, contactB });
                await ctx.SaveChangesAsync();

                var companyToCreate = new CompanyData
                {
                    ID = -90,
                    Name = "Test Company A",
                    BID = TestConstants.BID,
                    IsActive = true,
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1,
                    CompanyContactLinks = new List<CompanyContactLink>()
                    {
                        new CompanyContactLink
                        {
                             BID = testBID,
                             CompanyID = -90,
                             ContactID = contactA.ID
                        },
                        new CompanyContactLink
                        {
                             BID = testBID,
                             CompanyID = -90,
                             ContactID = contactB.ID
                        }
                    }
                };
                #endregion

                var createdCompany = await EndpointTests.AssertPostStatusCode<CompanyData>(client, $"{apiUrl}", JsonConvert.SerializeObject(companyToCreate), HttpStatusCode.OK);
                Assert.IsNotNull(createdCompany);
                Assert.IsNotNull(createdCompany.CompanyContactLinks);
                Assert.AreEqual(companyToCreate.Name, createdCompany.Name);
                Assert.AreEqual(companyToCreate.CompanyContactLinks.Count, createdCompany.CompanyContactLinks.Count);

                var clonedCompany = await EndpointTests.AssertPostStatusCode<CompanyData>(client, $"{apiUrl}/{createdCompany.ID}/clone", JsonConvert.SerializeObject(""), HttpStatusCode.OK);
                Assert.IsNotNull(clonedCompany);
                Assert.IsNotNull(clonedCompany.CompanyContactLinks);
                Assert.AreEqual($"{createdCompany.Name} (Clone)", clonedCompany.Name);
                Assert.AreEqual(createdCompany.CompanyContactLinks.Count, clonedCompany.CompanyContactLinks.Count);

                await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdCompany.ID}", HttpStatusCode.NoContent);
                await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{clonedCompany.ID}", HttpStatusCode.NoContent);
                await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{createdCompany.ID}", HttpStatusCode.NotFound);
                await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{clonedCompany.ID}", HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);
                else
                    Assert.Fail(ex.Message);
            }
            finally
            {
                await CleanUpCompanyContactAndContactLinks(testBID);
            }
           
        }

        [TestMethod]
        public async Task CompanyPOSTFailsWithNonNullNonZeroCredit()
        {
            CompanyData acntNameTest = new CompanyData()
            {
                Name = this.NewTestCompanyName,
                IsActive = true,
                BID = 1,
                RefundableCredit = 10,
            };

            //test: expect BadRequest
            var response = await EndpointTests.AssertPostStatusCodeEither(client, $"{apiUrl}", JsonConvert.SerializeObject(acntNameTest), HttpStatusCode.BadRequest, HttpStatusCode.BadRequest);
            string responseContent = await response.Content.ReadAsStringAsync();
            Assert.IsTrue(responseContent.Contains("Company must have an empty or 0 value for credit when created", StringComparison.OrdinalIgnoreCase));
        }

        [TestMethod]
        public async Task CompanyPUTFailsWithChangesToCredit()
        {
            CompanyData acntNameTest = new CompanyData()
            {
                Name = this.NewTestCompanyName,
                IsActive = true,
                BID = 1,
            };
            var response = await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(acntNameTest), HttpStatusCode.OK);

            string responseContent = await response.Content.ReadAsStringAsync();
            CompanyData resultCompany = JsonConvert.DeserializeObject<CompanyData>(responseContent);
            Assert.IsNotNull(resultCompany);
            resultCompany.RefundableCredit = 1;
            //test: expect OK
            var putResponse = await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{resultCompany.ID}", JsonConvert.SerializeObject(resultCompany), HttpStatusCode.BadRequest, HttpStatusCode.BadRequest);
            string putResponseContent = await putResponse.Content.ReadAsStringAsync();
            Assert.IsTrue(putResponseContent.Contains("Non refundable and refundable credit must match the value already present in the company", StringComparison.OrdinalIgnoreCase));

        }

        [TestMethod]
        public async Task TestFilterCriteria()
        {
            var criteria = await EndpointTests.AssertGetStatusCode<List<SystemListFilterCriteria>>(client, $"/api/list/{Convert.ToInt32(ClassType.Company)}/criteria", HttpStatusCode.OK);
            Assert.IsNotNull(criteria);
            Assert.IsTrue(criteria.Count > 0);
            CompanyData     company = new CompanyData();
            Assert.IsNotNull(criteria.FirstOrDefault(x => x.Field == nameof(company.Name)));
            Assert.IsNotNull(criteria.FirstOrDefault(x => x.Field == nameof(company.IsActive) || x.Field == ('-' + nameof(company.IsActive))));
            Assert.IsNotNull(criteria.FirstOrDefault(x => x.Field == nameof(company.LocationID)));
            Assert.IsNotNull(criteria.FirstOrDefault(x => x.Field == "PrimaryContactID"));
            Assert.IsNotNull(criteria.FirstOrDefault(x => x.Field == "SalesPersonID"));
        }

        [TestMethod]
        public async Task TestFilterLists()
        {
            var ctx = GetApiContext(); //auto migrate

            var all = await EndpointTests.AssertGetStatusCode<List<SystemListFilter>>(client, $"/api/list/{Convert.ToInt32(ClassType.Company)}/filter", HttpStatusCode.OK);
            Assert.IsNotNull(all);
            Assert.IsTrue(all.Count > 0);
        }

        [TestMethod]
        public async Task TestFiltering()
        {

            CompanyData newAccount = new CompanyData()
            {
                Name = this.NewTestCompanyName,
                IsActive = true,
                IsAdHoc = true,
                BID = 1
            };

            //test: expect OK
            var savedAccount = await EndpointTests.AssertPostStatusCode<CompanyData>(client, $"{apiUrl}", JsonConvert.SerializeObject(newAccount), HttpStatusCode.OK);

            //test that our account shows under no filters
            var all = await EndpointTests.AssertGetStatusCode<CompanyData[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(all.FirstOrDefault(x => x.Name == newAccount.Name));

            //test that our account DOES show under actives
            //var active = await EndpointTests.AssertGetStatusCode<CompanyData[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            //Assert.IsTrue(active.All(x => x.IsActive));
            //Assert.IsNotNull(active.FirstOrDefault(x => x.Name == newAccount.Name));

            var activeSimple = await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, $"{apiUrl}/simplelist?IsAdHoc=true", HttpStatusCode.OK);
            Assert.IsTrue(activeSimple.All(x => x.IsAdHoc));
            Assert.IsNotNull(activeSimple.FirstOrDefault(x => x.DisplayName.Contains(newAccount.Name)));

            //test that our account DOES NOT show under inactives
            //var inactive = await EndpointTests.AssertGetStatusCode<CompanyData[]>(client, $"{apiUrl}?IsAdHoc=false", HttpStatusCode.OK);
            //Assert.IsTrue(inactive.All(x => !x.IsAdHoc));
            //Assert.IsNull(inactive.FirstOrDefault(x => x.Name == newAccount.Name));

            var inactiveSimple = await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, $"{apiUrl}/simplelist?IsAdHoc=false", HttpStatusCode.OK);
            Assert.IsTrue(inactiveSimple.All(x => !x.IsAdHoc));
            Assert.IsNull(inactiveSimple.FirstOrDefault(x => x.DisplayName.Contains(newAccount.Name)));
        }

        [TestMethod]
        public async Task TestCompanyGetContactLinksEndpoint()
        {
            short testBID = 1;
            var ctx = GetApiContext(testBID);

            try
            {
                #region Remove if Existing Data

                var negativeLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (negativeLinks != null)
                {
                    ctx.RemoveRange(negativeLinks);
                    await ctx.SaveChangesAsync();
                }

                var negativeContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (negativeContacts != null)
                {
                    ctx.RemoveRange(negativeContacts);
                    await ctx.SaveChangesAsync();
                }

                var negativeCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (negativeCompanies != null)
                {
                    ctx.RemoveRange(negativeCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                #region Create Data 

                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var company1 = new CompanyData()
                {
                    BID = testBID,
                    ID = -99,
                    Name = "TEST COMPANY 1",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                var company2 = new CompanyData()
                {
                    BID = testBID,
                    ID = -98,
                    Name = "TEST COMPANY 2",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                ctx.CompanyData.AddRange(new[] { company1, company2 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var contact1 = new ContactData()
                {
                    BID = testBID,
                    ID = -99,
                    First = "TEST",
                    Last = "SUBJECT1",
                };
                var contact2 = new ContactData()
                {
                    BID = testBID,
                    ID = -98,
                    First = "TEST",
                    Last = "SUBJECT2",
                };
                var contact3 = new ContactData()
                {
                    BID = testBID,
                    ID = -97,
                    First = "TEST",
                    Last = "SUBJECT3",
                };
                var contact4 = new ContactData()
                {
                    BID = testBID,
                    ID = -96,
                    First = "TEST",
                    Last = "SUBJECT4",
                };
                ctx.ContactData.AddRange(new[] { contact1, contact2, contact3, contact4 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var companyContactLink1 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company1.ID,
                    ContactID = contact1.ID,
                    Roles = (ContactRole.Primary | ContactRole.Billing)
                };
                var companyContactLink2 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company1.ID,
                    ContactID = contact2.ID,
                    Roles = ContactRole.Inactive
                };
                var companyContactLink3 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company2.ID,
                    ContactID = contact2.ID,
                    Roles = ContactRole.Observer
                };
                var companyContactLink4 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company2.ID,
                    ContactID = contact4.ID,
                    Roles = (ContactRole.Primary | ContactRole.Billing)
                };
                ctx.CompanyContactLink.AddRange(new[] { companyContactLink1, companyContactLink2, companyContactLink3, companyContactLink4 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                #endregion

                var response = await EndpointTests.AssertGetStatusCode<ICollection<CompanyContactLink>>(client
                    , $"{apiUrl}/Contact/{contact1.ID}/CompanyLinks", HttpStatusCode.OK);
                Assert.IsNotNull(response);
                Assert.AreEqual(1, response.Count);
                Assert.IsNotNull(response.FirstOrDefault().Company);

                response = await EndpointTests.AssertGetStatusCode<ICollection<CompanyContactLink>>(client
                    , $"{apiUrl}/Contact/{contact2.ID}/CompanyLinks", HttpStatusCode.OK);
                Assert.IsNotNull(response);
                Assert.AreEqual(2, response.Count);
                Assert.IsNotNull(response.FirstOrDefault().Company);
            }
            finally
            {
                #region Cleanup Data

                var cleanupLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (cleanupLinks != null)
                {
                    ctx.RemoveRange(cleanupLinks);
                    await ctx.SaveChangesAsync();
                }

                var cleanupContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupContacts != null)
                {
                    ctx.RemoveRange(cleanupContacts);
                    await ctx.SaveChangesAsync();
                }

                var cleanupCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupCompanies != null)
                {
                    ctx.RemoveRange(cleanupCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion
            }
        }

        [TestMethod]
        public async Task TestCompanyDeleteWithForceEndpoint()
        {
            var ctx = GetApiContext();

            var testUserLink = await Utils.GetAuthUserLink(ctx);
            if (testUserLink != null && testUserLink.UserAccessType < UserAccessType.SupportManager)
            {
                testUserLink.UserAccessType = UserAccessType.SupportManager;
                ctx.SaveChanges();
            }

            const string companyDataName = "New Company - Unit Test";
            CompanyData companyData = await ctx.CompanyData
                .FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ID == -99);

            if (companyData == null)
            {
                companyData = new CompanyData()
                {
                    ID = -99,
                    BID = TestConstants.BID,
                    Name = companyDataName,
                    IsActive = true,
                    StatusID = (await ctx.EnumCrmCompanyStatus.FirstAsync()).ID,
                    LocationID = (await ctx.LocationData.FirstAsync(x => x.BID == TestConstants.BID)).ID
                };

                await ctx.CompanyData.AddAsync(companyData);
            }

            string[] contactDataName = new[] { "TEST", "CONTACT" };
            ContactData contactData = await ctx.ContactData
                .FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ID == -91);
            if (contactData == null)
            {
                contactData = new ContactData()
                {
                    ID = -91,
                    BID = TestConstants.BID,
                    First = contactDataName[0],
                    Last = contactDataName[1]
                };
                await ctx.ContactData.AddAsync(contactData);
            }

            CompanyCustomData companyCustomData =
                await ctx.CompanyCustomData.Where(x => x.BID == TestConstants.BID && x.ID == companyData.ID).FirstOrDefaultAsync();
            if (companyCustomData == null)
            {
                companyCustomData = new CompanyCustomData
                {
                    ID = companyData.ID,
                    BID = TestConstants.BID,
                    AppliesToClassTypeID = 2000,
                };

                await ctx.CompanyCustomData.AddAsync(companyCustomData);
            }

            CompanyLocator companyLocator =
                await ctx.CompanyLocator.Where(x => x.BID == TestConstants.BID && x.ParentID == companyData.ID).FirstOrDefaultAsync();
            if (companyLocator == null)
            {
                companyLocator = new CompanyLocator
                {
                    ID = -99,
                    BID = TestConstants.BID,
                    ParentID = companyData.ID,
                    LocatorType = 2,
                    Locator = "(801) 836-7117",
                    RawInput = "8018367117",
                    SortIndex = 1,
                    IsVerified = false,
                    IsValid = true,
                    ModifiedDT = DateTime.Now
                };

                await ctx.CompanyLocator.AddAsync(companyLocator);
            }

            CompanyTagLink companyTagLink =
                await ctx.CompanyTagLink.Where(x => x.BID == TestConstants.BID && x.CompanyID == companyData.ID).FirstOrDefaultAsync();
            if (companyTagLink == null)
            {
                companyTagLink = new CompanyTagLink
                {
                    BID = TestConstants.BID,
                    CompanyID = companyData.ID,
                    TagID = (await ctx.ListTag.FirstAsync(x => x.BID == TestConstants.BID)).ID
                };

                await ctx.CompanyTagLink.AddAsync(companyTagLink);
            }

            List<CompanyContactLink> companyContactLinks = await ctx.CompanyContactLink
                .Where(x => x.BID == TestConstants.BID && x.ContactID == contactData.ID && x.CompanyID == companyData.ID).ToListAsync();
            if (companyContactLinks == null || companyContactLinks.Count() == 0)
            {
                companyContactLinks = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        ContactID = contactData.ID,
                        CompanyID = companyData.ID,
                        Roles = (ContactRole.Primary | ContactRole.Billing)
                    }
                };
                await ctx.CompanyContactLink.AddRangeAsync(companyContactLinks);
            }

            await ctx.SaveChangesAsync();

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{companyData.ID}?force=true", HttpStatusCode.OK);

            Assert.IsNull(await ctx.CompanyData.Where(x => x.BID == TestConstants.BID && x.ID == companyData.ID).FirstOrDefaultAsync());
            Assert.IsNull(await ctx.ContactData.Where(x => x.BID == TestConstants.BID && x.ID == contactData.ID).FirstOrDefaultAsync());
            Assert.IsNull(await ctx.CompanyCustomData.Where(x => x.BID == TestConstants.BID && x.ID == companyData.ID).FirstOrDefaultAsync());
            Assert.IsNull(await ctx.CompanyLocator.Where(x => x.BID == TestConstants.BID && x.ParentID == companyData.ID).FirstOrDefaultAsync());
            Assert.IsNull(await ctx.CompanyTagLink.Where(x => x.BID == TestConstants.BID && x.CompanyID == companyData.ID).FirstOrDefaultAsync());
            Assert.IsNull(await ctx.CompanyContactLink.Where(x => x.BID == TestConstants.BID && x.CompanyID == companyData.ID && x.ContactID == contactData.ID).FirstOrDefaultAsync());
        }

        [TestCleanup]
        public void Teardown()
        {
            //you can actually do test-specific things like this
            //switch (TestContext.TestName)
            //{
            //    case "TestCompanySingleEndpoints":
            //    case "TestFiltering":
            //        // do stuff
            //        break;
            //    default:
            //        break;
            //}
            this.DeleteTestCompanyRecord();

            var ctx = GetApiContext();

            if (TestContext.TestName == "TestCompanyDeleteWithForceEndpoint")
            {
                const string companyDataName = "New Company - Unit Test";
                CompanyData companyData = ctx.CompanyData.Where(x => x.BID == TestConstants.BID && x.ID == -99 && x.Name == companyDataName).FirstOrDefault();
                if (companyData != null)
                {
                    var companyCustomData = ctx.CompanyCustomData.Where(x => x.BID == TestConstants.BID && x.ID == companyData.ID).FirstOrDefault();
                    if (companyCustomData != null)
                        ctx.RemoveRange(companyCustomData);

                    var companyLocator = ctx.CompanyLocator.Where(x => x.BID == TestConstants.BID && x.ParentID == companyData.ID).FirstOrDefault();
                    if (companyLocator != null)
                        ctx.RemoveRange(companyLocator);

                    var companyTagLink = ctx.CompanyTagLink.Where(x => x.BID == TestConstants.BID && x.CompanyID == companyData.ID).FirstOrDefault();
                    if (companyTagLink != null)
                        ctx.RemoveRange(companyTagLink);

                    ctx.RemoveRange(companyData);
                    ctx.SaveChanges();
                }
            }

        }

        private void DeleteTestCompanyRecord()
        {
            string newTestCompanyName = NewTestCompanyName;
            Task.Run(async () =>
            {

                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                SimpleCompanyData[] result = JsonConvert.DeserializeObject<SimpleCompanyData[]>(responseString);
                if (result != null)
                {
                    SimpleCompanyData sga = result.FirstOrDefault(x => x.DisplayName.Contains(newTestCompanyName));
                    if (sga != null)
                    {
                        await client.DeleteAsync($"{apiUrl}/{sga.ID}");
                    }
                }
            }).Wait();
        }

        [TestMethod]
        public async Task SimpleListDoesntReturnAdHoc()
        {
            var adHocSimple = await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, $"{apiUrl}/simplelist?IsAdHoc=true", HttpStatusCode.OK);
            Assert.IsTrue(adHocSimple.All(x => x.IsAdHoc));
            var adHocSimple2 = await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, $"{apiUrl}/simplelist", HttpStatusCode.OK);
            Assert.IsTrue(adHocSimple2.All(x => !x.IsAdHoc));

        }

        [TestMethod]
        public void TestCheckPrimaryBillingExistsLogic()
        {
            var company1 = new CompanyData()
            {
                BID = 1,
                ID = 1,
                Name = "Test",
                CompanyContactLinks = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = 1,
                        CompanyID = 1,
                        ContactID = 1,
                        Roles = (ContactRole.Primary | ContactRole.Billing)
                    }
                }
            };
            var company2 = new CompanyData()
            {
                BID = 1,
                ID = 1,
                Name = "Test",
                CompanyContactLinks = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = 1,
                        CompanyID = 1,
                        ContactID = 1,
                        Roles = ContactRole.Primary
                    },
                    new CompanyContactLink()
                    {
                        BID = 1,
                        CompanyID = 1,
                        ContactID = 1,
                        Roles = ContactRole.Billing
                    }
                }
            };
            var company3 = new CompanyData()
            {
                BID = 1,
                ID = 1,
                Name = "Test",
                CompanyContactLinks = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = 1,
                        CompanyID = 1,
                        ContactID = 1,
                        Roles = ContactRole.Inactive
                    },
                    new CompanyContactLink()
                    {
                        BID = 1,
                        CompanyID = 1,
                        ContactID = 1,
                        Roles = ContactRole.Billing
                    }
                }
            };

            Assert.IsTrue(verifyLinks(company1));
            Assert.IsTrue(verifyLinks(company2));
            Assert.IsFalse(verifyLinks(company3));
        }

        private bool verifyLinks(CompanyData company)
        {
            if (company.CompanyContactLinks == null ||
                 company.CompanyContactLinks.Count == 0 ||
                !company.CompanyContactLinks.Any(x => (x.Roles & ContactRole.Billing) == ContactRole.Billing) ||
                !company.CompanyContactLinks.Any(x => (x.Roles & ContactRole.Primary) == ContactRole.Primary))
            {
                return false;
            }
            else
            {
                return true;
            }

        }
    }
}
