﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class SystemColorEndpointTests
    {
        public const string apiUrl = "/Api/System/Color";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        [TestMethod]
        public async Task TestGetSystemColors()
        {
            var colors = await EndpointTests.AssertGetStatusCode<SystemColor[]>(client, apiUrl, HttpStatusCode.OK);
            Assert.IsNotNull(colors);
            Assert.IsTrue(colors.Count() > 0);
        }
    }
}
