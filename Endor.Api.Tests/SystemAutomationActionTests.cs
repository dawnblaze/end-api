﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class SystemAutomationActionTests
    {
        public const string apiUrl = "/api/system";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        [TestMethod]
        public async Task SystemAutomationBasicTests()
        {

            var automations = await EndpointTests.AssertGetStatusCode<SystemAutomationActionDefinition[]>(client, $"{apiUrl}/automation/actions", HttpStatusCode.OK);
            Assert.IsTrue(automations.All(x => x.AppliesToAutomations));
            Assert.IsTrue(automations.All(x => x.AppliesToAllDataTypes));
            var alerts = await EndpointTests.AssertGetStatusCode<SystemAutomationActionDefinition[]>(client, $"{apiUrl}/alert/actions", HttpStatusCode.OK);
            Assert.IsTrue(alerts.All(x => x.AppliesToAlerts));
            Assert.IsTrue(alerts.All(x => x.AppliesToAllDataTypes));

            var autoTriggerDefs = await EndpointTests.AssertGetStatusCode<SystemAutomationTriggerDefinition[]>(client, $"{apiUrl}/automation/triggers", HttpStatusCode.OK);
            Assert.IsTrue(autoTriggerDefs.All(x => x.AppliesToAutomations));
            Assert.IsTrue(autoTriggerDefs.Count() > 0);

            var alertTriggerDefs = await EndpointTests.AssertGetStatusCode<SystemAutomationTriggerDefinition[]>(client, $"{apiUrl}/alert/triggers", HttpStatusCode.OK);
            Assert.IsTrue(alertTriggerDefs.All(x => x.AppliesToAlerts));
            Assert.IsTrue(alertTriggerDefs.Count() > 0);

            var alertLineItemTriggerDefs = await EndpointTests.AssertGetStatusCode<SystemAutomationTriggerDefinition[]>(client, $"{apiUrl}/alert/triggers?DataType={DataType.LineItem}", HttpStatusCode.OK);
            Assert.IsTrue(alertLineItemTriggerDefs.All(x => x.AppliesToAlerts));
            Assert.IsTrue(alertLineItemTriggerDefs.All(x => x.DataType == DataType.LineItem));
            Assert.IsTrue(alertLineItemTriggerDefs.Count() > 0);
        }
    }
}
