﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using Endor.Api.Web.Classes.Responses;

namespace Endor.Api.Tests
{
    [TestClass]
    public class TimeCardDetailEndpointTest : CommonControllerTestClass
    {
        public const string apiUrl = "/api/timecarddetail";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task TimeCardDetailCRUDTest()
        {
            // POST ../api/timecarddetail/
            var newTimeCardDetailModel = await GetNewTimeCardDetailModel();
            var newlyCreatedTimeCardDetail = await EndpointTests.AssertPostStatusCode<TimeCardDetail>(client, apiUrl, newTimeCardDetailModel, HttpStatusCode.OK);
            Assert.IsNotNull(newlyCreatedTimeCardDetail);

            // GET ../api/timecarddetail/
            var timeCardDetail = await EndpointTests.AssertGetStatusCode<TimeCardDetail[]>(client, $"{apiUrl}?EmployeeID={newlyCreatedTimeCardDetail.ID}&TimeCardID={newlyCreatedTimeCardDetail.EmployeeID}", HttpStatusCode.OK);
            Assert.IsNotNull(timeCardDetail);

            // PUT ../api/timecarddetail/{detailid}
            newlyCreatedTimeCardDetail.MetaData = "Test Update";
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{newlyCreatedTimeCardDetail.ID}", JsonConvert.SerializeObject(newlyCreatedTimeCardDetail), HttpStatusCode.OK);

            // GET ../api/timecarddetail/{detailid}
            var updatedTimeCard = await EndpointTests.AssertGetStatusCode<TimeCardDetail>(client, $"{apiUrl}/{newlyCreatedTimeCardDetail.ID}", HttpStatusCode.OK);
            Assert.AreEqual(newlyCreatedTimeCardDetail.MetaData, updatedTimeCard.MetaData);

            // DELETE ../api/timecarddetail/{detailid}
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{newlyCreatedTimeCardDetail.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TimeCardDetailStatusesTest()
        {
            // GET ../api/timecarddetail/statuses
            var timeCardStatuses = await EndpointTests.AssertGetStatusCode<TimeCardDetailStatus[]>(client, $"{apiUrl}/statuses", HttpStatusCode.OK);
            Assert.IsNotNull(timeCardStatuses);

            //PUT /api/options/Timeclock.BreakTracking.Enabled
            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"/api/options/Timeclock.BreakTracking.Enabled?bid=1", "false", HttpStatusCode.OK);
            await EndpointTests.AssertPutStatusCode(client, $"/api/options/Timeclock.ActivityTracking.UseItemStatus?bid=1", "false", HttpStatusCode.OK);
            await EndpointTests.AssertPutStatusCode(client, $"/api/options/Timeclock.ActivityTracking.UseCustomList?bid=1", "false", HttpStatusCode.OK);

            timeCardStatuses = await EndpointTests.AssertGetStatusCode<TimeCardDetailStatus[]>(client, $"{apiUrl}/statuses", HttpStatusCode.OK);
            Assert.IsNotNull(timeCardStatuses);
            Assert.AreEqual(0, timeCardStatuses.Length);

            await EndpointTests.AssertPutStatusCode(client, $"/api/options/Timeclock.BreakTracking.Enabled?bid=1", "true", HttpStatusCode.OK);
            await EndpointTests.AssertPutStatusCode(client, $"/api/options/Timeclock.ActivityTracking.UseItemStatus?bid=1", "true", HttpStatusCode.OK);
            await EndpointTests.AssertPutStatusCode(client, $"/api/options/Timeclock.ActivityTracking.UseCustomList?bid=1", "true", HttpStatusCode.OK);

            timeCardStatuses = await EndpointTests.AssertGetStatusCode<TimeCardDetailStatus[]>(client, $"{apiUrl}/statuses", HttpStatusCode.OK);
            Assert.IsNotNull(timeCardStatuses);
            Assert.IsTrue(timeCardStatuses.Length > 0);
        }

        [TestMethod]
        public async Task LastFiveTimeCardDetailStatusesTest()
        {
            var allTimeCards = await EndpointTests.AssertGetStatusCode<TimeCardDetail[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(allTimeCards, "failed to pull time cards");
            Assert.IsTrue(allTimeCards.Length > 0, "No time card data to test against, please add some time cards tied to an employee on this data set");
            short mostFrequentEmployeeID = allTimeCards.Select(x => x.EmployeeID).GroupBy(a => a).Select(b => new
            {
                EmployeeID = b.Key,
                Frequency = b.Count()
            }).OrderByDescending(c => c.Frequency).First().EmployeeID;

            //turn on all timeclock options
            await EndpointTests.AssertPutStatusCode(client, $"/api/options/Timeclock.BreakTracking.Enabled?bid=1", "true", HttpStatusCode.OK);
            await EndpointTests.AssertPutStatusCode(client, $"/api/options/Timeclock.ActivityTracking.UseItemStatus?bid=1", "true", HttpStatusCode.OK);
            await EndpointTests.AssertPutStatusCode(client, $"/api/options/Timeclock.ActivityTracking.UseCustomList?bid=1", "true", HttpStatusCode.OK);

            var timeCardStatuses = await EndpointTests.AssertGetStatusCode<TimeCardDetailStatus[]>(client, $"/api/employee/{mostFrequentEmployeeID}/recenttimecards", HttpStatusCode.OK);
            Assert.IsNotNull(timeCardStatuses);
        }

        private async Task<TimeCardDetail> GetNewTimeCardDetailModel()
        {
            var newTimeCardModel = await GetNewTimeCardModel();
            var newlyCreatedTimeCard = await EndpointTests.AssertPostStatusCode<TimeCard>(client, "api/timecard", newTimeCardModel, HttpStatusCode.OK);

            return new TimeCardDetail()
            {
                BID = 1,
                EmployeeID = newlyCreatedTimeCard?.EmployeeID ?? 0,
                StartDT = DateTime.Now,
                AdjustedByEmployeeID = newlyCreatedTimeCard?.EmployeeID ?? 0,
                MetaData = "Test",
                TimeCardID = newlyCreatedTimeCard?.ID ?? 0,
            };
        }

        private async Task<TimeCard> GetNewTimeCardModel()
        {
            var employee = await EndpointTests.AssertGetStatusCode<EmployeeData[]>(client, "/api/employee", HttpStatusCode.OK);

            return new TimeCard()
            {
                BID = 1,
                EmployeeID = employee.FirstOrDefault()?.ID ?? 0,
                StartDT = DateTime.Now,
                EndDT = DateTime.Now,
                TimeInMin = 0,
                PaidTimeInMin = 0,
                IsClosed = true,
                IsAdjusted = true,
                AdjustedByEmployeeID = employee.FirstOrDefault()?.ID ?? 0,
                AdjustedDT = DateTime.Now,
                MetaData = "Test"
            };
        }
    }
}

