﻿using Endor.Api.Web.Classes;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class AlertDefinitionEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/alert/definition";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        private string TestAlertDefinitionName;

        [TestInitialize]
        public void Initialize()
        {
            this.TestAlertDefinitionName = $"TestAlertDefinition{DateTime.UtcNow}";
        }

        [TestMethod]
        public async Task AlertDefinitionCRUDTest()
        {
            // CREATE
            var newAlertDefinitionModel = await GetNewAlertDefinitionModel(TestAlertDefinitionName, client);
            var createdAlertDef = await EndpointTests.AssertPostStatusCode<AlertDefinition>(client, apiUrl, newAlertDefinitionModel, HttpStatusCode.OK);

            // READ
            var allAlerts = await EndpointTests.AssertGetStatusCode<AlertDefinition[]>(client, apiUrl, HttpStatusCode.OK);
            Assert.IsNotNull(allAlerts.FirstOrDefault(x => x.ID == createdAlertDef.ID));

            var simpleAlerts = await EndpointTests.AssertGetStatusCode<SimpleAlertDefinition[]>(client, $"{apiUrl}/simplelist", HttpStatusCode.OK);
            Assert.IsNotNull(simpleAlerts.FirstOrDefault(x => x.ID == createdAlertDef.ID));

            var myAlert = await EndpointTests.AssertGetStatusCode<AlertDefinition>(client, $"{apiUrl}/{createdAlertDef.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(myAlert);

            // UPDATE
            createdAlertDef.Description = $"description{DateTime.UtcNow}";

            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdAlertDef.ID}", JsonConvert.SerializeObject(createdAlertDef), HttpStatusCode.OK);
            var updatedAlert = await EndpointTests.AssertGetStatusCode<AlertDefinition>(client, $"{apiUrl}/{createdAlertDef.ID}", HttpStatusCode.OK);

            Assert.AreEqual(createdAlertDef.Description, updatedAlert.Description);

            await EndpointTests.AssertPostStatusCode<AlertDefinitionAction>(client, $"{apiUrl}/{createdAlertDef.ID}/clone", null, HttpStatusCode.OK);

            // DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{createdAlertDef.ID}", HttpStatusCode.NoContent);

        }

        [TestMethod]
        public async Task AlertDefinitionAPIActionTest()
        {
            // CREATE/READ
            var newAlertDefinitionModel = await GetNewAlertDefinitionModel(TestAlertDefinitionName, client);
            var createdAlertDef = await EndpointTests.AssertPostStatusCode<AlertDefinition>(client, apiUrl, newAlertDefinitionModel, HttpStatusCode.OK);
            var myAlert = await EndpointTests.AssertGetStatusCode<AlertDefinition>(client, $"{apiUrl}/{createdAlertDef.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(myAlert);

            // set active/inactive
            await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdAlertDef.ID}/action/setinactive", null, HttpStatusCode.OK);
            myAlert = await EndpointTests.AssertGetStatusCode<AlertDefinition>(client, $"{apiUrl}/{createdAlertDef.ID}", HttpStatusCode.OK);
            Assert.AreEqual(false, myAlert.IsActive);

            await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdAlertDef.ID}/action/setactive", null, HttpStatusCode.OK);
            myAlert = await EndpointTests.AssertGetStatusCode<AlertDefinition>(client, $"{apiUrl}/{createdAlertDef.ID}", HttpStatusCode.OK);
            Assert.AreEqual(true, myAlert.IsActive);

            var canDelete = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{createdAlertDef.ID}/action/candelete", HttpStatusCode.OK);
            Assert.AreEqual(true, canDelete.Value);

            await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdAlertDef.ID}/action/postresults", null, HttpStatusCode.OK);
            myAlert = await EndpointTests.AssertGetStatusCode<AlertDefinition>(client, $"{apiUrl}/{createdAlertDef.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(myAlert.LastRunDT);
            Assert.IsNotNull(myAlert.CummRunCount);
            Assert.IsTrue(myAlert.CummRunCount > 0);


            await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdAlertDef.ID}/action/resettimer", null, HttpStatusCode.OK);
            myAlert = await EndpointTests.AssertGetStatusCode<AlertDefinition>(client, $"{apiUrl}/{createdAlertDef.ID}", HttpStatusCode.OK);
            Assert.IsNull(myAlert.LastRunDT);

            await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdAlertDef.ID}/action/postresults", null, HttpStatusCode.OK);
            myAlert = await EndpointTests.AssertGetStatusCode<AlertDefinition>(client, $"{apiUrl}/{createdAlertDef.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(myAlert.LastRunDT);
            Assert.IsNotNull(myAlert.CummRunCount);
            Assert.IsTrue(myAlert.CummRunCount > 0);

            await EndpointTests.AssertPostStatusCode<EntitiesActionChangeResponse>(client, $"{apiUrl}/all/action/resettimer", null, HttpStatusCode.OK);
            myAlert = await EndpointTests.AssertGetStatusCode<AlertDefinition>(client, $"{apiUrl}/{createdAlertDef.ID}", HttpStatusCode.OK);
            Assert.IsNull(myAlert.LastRunDT);

            // DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{createdAlertDef.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task AlertDefinitionFiltersTest()
        {
            // CREATE/READ
            var newAlertDefinitionModel = await GetNewAlertDefinitionModel(TestAlertDefinitionName, client);
            var createdAlertDef = await EndpointTests.AssertPostStatusCode<AlertDefinition>(client, apiUrl, newAlertDefinitionModel, HttpStatusCode.OK);
            var myAlert = await EndpointTests.AssertGetStatusCode<AlertDefinition>(client, $"{apiUrl}/{createdAlertDef.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(myAlert);

            var myAlerts = await EndpointTests.AssertGetStatusCode<AlertDefinition[]>(client, $"{apiUrl}/?EmployeeID={newAlertDefinitionModel.EmployeeID}", HttpStatusCode.OK);
            Assert.IsNotNull(myAlerts);
            Assert.IsNotNull(myAlerts.FirstOrDefault(x => x.ID == createdAlertDef.ID));

            myAlerts = await EndpointTests.AssertGetStatusCode<AlertDefinition[]>(client, $"{apiUrl}/?EmployeeID={-newAlertDefinitionModel.EmployeeID}", HttpStatusCode.OK);
            Assert.IsNotNull(myAlerts);
            Assert.IsNull(myAlerts.FirstOrDefault(x => x.ID == createdAlertDef.ID));

            var simpleAlerts = await EndpointTests.AssertGetStatusCode<SimpleAlertDefinition[]>(client, $"{apiUrl}/simplelist?EmployeeID={newAlertDefinitionModel.EmployeeID}", HttpStatusCode.OK);
            Assert.IsNotNull(simpleAlerts.FirstOrDefault(x => x.ID == createdAlertDef.ID));
            simpleAlerts = await EndpointTests.AssertGetStatusCode<SimpleAlertDefinition[]>(client, $"{apiUrl}/simplelist?EmployeeID={-newAlertDefinitionModel.EmployeeID}", HttpStatusCode.OK);
            Assert.IsNull(simpleAlerts.FirstOrDefault(x => x.ID == createdAlertDef.ID));
            simpleAlerts = await EndpointTests.AssertGetStatusCode<SimpleAlertDefinition[]>(client, $"{apiUrl}/simplelist?IsActive=false", HttpStatusCode.OK);
            Assert.IsNull(simpleAlerts.FirstOrDefault(x => x.ID == createdAlertDef.ID));
            simpleAlerts = await EndpointTests.AssertGetStatusCode<SimpleAlertDefinition[]>(client, $"{apiUrl}/simplelist?DataType={newAlertDefinitionModel.DataType}", HttpStatusCode.OK);
            Assert.IsNotNull(simpleAlerts.FirstOrDefault(x => x.ID == createdAlertDef.ID));
            simpleAlerts = await EndpointTests.AssertGetStatusCode<SimpleAlertDefinition[]>(client, $"{apiUrl}/simplelist?DataType={-(int)newAlertDefinitionModel.DataType}", HttpStatusCode.OK);
            Assert.IsNull(simpleAlerts.FirstOrDefault(x => x.ID == createdAlertDef.ID));

            // DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{createdAlertDef.ID}", HttpStatusCode.NoContent);
        }

        public static async Task<AlertDefinition> GetNewAlertDefinitionModel(string TestAlertDefinitionName, HttpClient client)
        {
            var employees = await EndpointTests.AssertGetStatusCode<EmployeeData[]>(client, "/api/employee", HttpStatusCode.OK);
            var triggers = await EndpointTests.AssertGetStatusCode<SystemAutomationTriggerDefinition[]>(client, "/api/system/alert/triggers", HttpStatusCode.OK);
            return new AlertDefinition()
            {
                Name = TestAlertDefinitionName,
                EmployeeID = employees.FirstOrDefault()?.ID ?? 0,
                HasImage = false,
                IsActive = true,
                IsSystem = false,
                TriggerID = triggers.FirstOrDefault()?.ID ?? 0,
                DataType = DataType.Order
            };
        }



        [TestCleanup]
        public void Teardown()
        {
            this.DeleteTestRecord();
        }

        private void DeleteTestRecord()
        {
            string testAlertDefName = TestAlertDefinitionName;
            Task.Run(async () =>
            {
                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}");
                string responseString = await response.Content.ReadAsStringAsync();
                AlertDefinition[] result = JsonConvert.DeserializeObject<AlertDefinition[]>(responseString);
                if (result != null)
                {
                    result.Where(x => x.Name.Contains(testAlertDefName)).ToList().ForEach(async (ad) =>
                    {
                        if (ad != null)
                        {
                            await client.DeleteAsync($"{apiUrl}/{ad.ID}");
                        }
                    });
                }
            }).Wait();
        }
    }
}
