﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Pricing;
using Microsoft.EntityFrameworkCore;
using Endor.CBEL.Elements;

namespace Endor.Api.Tests
{
    [TestClass]
    public class OrderItemEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/orderitem";
        public const string apiOrderUrl = "/api/order";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public async Task Initialize()
        {
            var ctx = GetApiContext();
            var PartAssembliesToRemove = ctx.AssemblyData.Where(x => x.Name == "TestAssembly" && x.AssemblyType == AssemblyType.Product && x.BID == 1 ).ToList();
            var AssemblyIDs = PartAssembliesToRemove.Select(y => y.ID);
            ctx.RemoveRange(ctx.AssemblyTable.Where(x => x.BID == 1 && AssemblyIDs.Contains(x.AssemblyID)));
            ctx.RemoveRange(ctx.AssemblyVariable.Where(x => x.BID == 1 && AssemblyIDs.Contains(x.AssemblyID)));
            await ctx.SaveChangesAsync();
            ctx.RemoveRange(PartAssembliesToRemove);
            await ctx.SaveChangesAsync();

        }


        [TestMethod]
        [TestCategory("GL")]
        public async Task TestOrderItemCRUDOperations()
        {
            #region CREATE ORDER FIRST
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            #endregion

            #region CREATE

            var testOrderItem = Utils.GetTestOrderItemData(createdOrder);
            var createdOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);

            #endregion

            #region RETRIEVE

            // Test Get By ID
            var retrievedOrderItem = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderItem);

            #endregion

            #region UPDATE

            int invalidID = -1;
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{invalidID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{invalidID}", JsonConvert.SerializeObject(createdOrderItem), HttpStatusCode.NotFound, HttpStatusCode.BadRequest);

            createdOrderItem.Description = "TEST DESCRIPTION";
            var testPut = await EndpointTests.AssertPutStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}", JsonConvert.SerializeObject(createdOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(testPut);
            Assert.AreEqual("TEST DESCRIPTION", testPut.Description);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.NoContent);

            #endregion
        }

        [TestMethod]
        [TestCategory("compute")]
        [TestCategory("GL")]
        public async Task TestOrderItemComputeWithTierTableVariables()
        {
            //test invalid request
            object invalidRequest = JsonConvert.DeserializeObject(@"{ 'EngineType': 'Free' }"); // force invalid request by setting OrderpriceRequest.Result property to invalid type
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(invalidRequest), HttpStatusCode.BadRequest);
            var ctx = GetApiContext();
            var pricingTier = ctx.FlatListItem.Where(c => c.BID == 1 && c.FlatListType == FlatListType.PricingTiers).Last();
            var dbCompany = ctx.CompanyData.Include(c => c.PricingTier).Where(c => c.BID == 1).First();
            var originalPricingTier = dbCompany.PricingTierID;
            dbCompany.PricingTierID = pricingTier.ID;
            ctx.SaveChanges();

            var tmpID = new Guid();
            AssemblyData assembly = new AssemblyData()
            {
                Name = "TestAssembly",
                IsActive = true,
                
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Price",
                        IsFormula = true,
                        DefaultValue = "=PriceTable",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Height",
                        TempID = tmpID
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Width",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Area",
                        IsFormula = true,
                        DefaultValue = "=Height * Width",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Boolean,
                        ElementType = AssemblyElementType.Checkbox,
                        Name = "IsDoubleSided",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "TotalArea",
                        IsFormula = true,
                        DefaultValue = "=( Quantity * Area ) * IF(IsDoubleSided = TRUE, 2, 1)",
                    },
                },
                Tables = new List<AssemblyTable>()
                {
                    new AssemblyTable()
                    {
                        BID = TestConstants.BID,
                        Label = "TierTable",
                        TableType = AssemblyTableType.Custom,
                        CellDataJSON = "[[15,20],[12,17]]",
                        VariableName = "TierTable",
                        CellDataType = DataType.Number,
                        ColumnCount = 1,
                        ColumnDataType = DataType.Number,
                        ColumnIsSorted = false,
                        ColumnLabel = "Quantity",
                        ColumnMatchType = AssemblyTableMatchType.ExactMatch,
                        ColumnUnitID = 0,
                        ColumnValuesJSON = "[{\"index\":0,\"value\":18,\"name\":\"Default\"}, {\"index\":2,\"value\":36,\"name\":\"Default\"}]",
                        ColumnVariableTempID = tmpID.ToString(),
                        ColumnVariableID = 0,
                        Description = "",
                        ModifiedDT = DateTime.Parse("2019-04-24T16:07:46.7515121"),
                        RowCount = 3,
                        RowDataType = DataType.Number,
                        RowDefaultMarkupJSON = "",
                        RowIsSorted = false,
                        RowLabel = "Row",
                        RowMatchType = 0,
                        RowUnitID = 0,
                        RowValuesJSON = "[{\"index\":0,\"value\":\"TestTier\"},{\"index\":1,\"value\":\"" + pricingTier.Name+ "\"}]",
                        IsTierTable = true,
                        ClassTypeID = 12047
                    }
                },
                PricingType = AssemblyPricingType.MarketBasedPricing,
                PriceFormulaType = PriceFormulaType.PriceTable,
                HasTierTable = true,

            };

            assembly = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, "/api/assemblypart", assembly, HttpStatusCode.OK);

            try
            {
                //test valid request
                ItemPriceRequest testRequest = new ItemPriceRequest()
                {
                    EngineType = PricingEngineType.Supplied,
                    Quantity = 5,
                    CompanyID = dbCompany.ID,
                    Components = new List<ComponentPriceRequest>
                           {
                               new ComponentPriceRequest
                               {
                                   ComponentType = OrderItemComponentType.Assembly,
                                   ComponentID = assembly.ID,
                                   TotalQuantity = 5,
                                   PriceUnit = 20m,
                                   PriceUnitOV = true,
                                   Variables = new Dictionary<string, VariableValue>()
                                   {
                                       { "Quantity", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 2m } },
                                       { "Height", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 36m } },
                                       { "Width", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 12m } },
                                       { "IsDoubleSided", new VariableValue() { DataType = DataType.Boolean, ValueOV = true, ValueAsBool = true } },
                                   }
                               }
                           },
                };

                ItemPriceResult result = await EndpointTests.AssertPostStatusCode<ItemPriceResult>(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);

                Assert.IsNotNull(result);

                Assert.AreEqual(100m, result.PricePreTax);
                Assert.AreEqual(100m, result.PriceComponent);
                Assert.AreEqual(0m, result.CostLabor);
                Assert.AreEqual(0m, result.CostMachine);
                Assert.AreEqual(0m, result.CostMaterial);
                Assert.AreEqual(0m, result.CostTotal);

                Assert.IsNull(result.TaxAmount);
                Assert.IsNull(result.PriceTotal);
                Assert.AreEqual(1, result.Components.Count);
                Assert.IsNotNull(result.Components[0].Variables);

                VariableValue varValue;

                varValue = result.Components[0].Variables.GetValueOrDefault("Height");
                Assert.IsNotNull(varValue);
                Assert.AreEqual(36m, varValue.ValueAsDecimal);

                varValue = result.Components[0].Variables.GetValueOrDefault("Width");
                Assert.IsNotNull(varValue);
                Assert.AreEqual(12m, varValue.ValueAsDecimal);
            }
            finally
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"/api/assemblypart/{assembly.ID}", HttpStatusCode.NoContent);
                dbCompany.PricingTierID = originalPricingTier;
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        [TestCategory("compute")]
        [TestCategory("GL")]
        public async Task TestOrderItemComputeWithTierTableNameDoesntMatchVariables()
        {
            //test invalid request
            object invalidRequest = JsonConvert.DeserializeObject(@"{ 'EngineType': 'Free' }"); // force invalid request by setting OrderpriceRequest.Result property to invalid type
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(invalidRequest), HttpStatusCode.BadRequest);
            var ctx = GetApiContext();
            var pricingTier = ctx.FlatListItem.Where(c => c.BID == 1 && c.FlatListType == FlatListType.PricingTiers).Last();
            var dbCompany = ctx.CompanyData.Include(c => c.PricingTier).Where(c => c.BID == 1).First();
            var originalPricingTier = dbCompany.PricingTierID;
            dbCompany.PricingTierID = pricingTier.ID;
            ctx.SaveChanges();

            var tmpID = new Guid();
            AssemblyData assembly = new AssemblyData()
            {
                Name = "TestAssembly",
                IsActive = true,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Price",
                        IsFormula = true,
                        DefaultValue = "=PriceTable",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Height",
                        TempID = tmpID
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Width",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Area",
                        IsFormula = true,
                        DefaultValue = "=Height * Width",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Boolean,
                        ElementType = AssemblyElementType.Checkbox,
                        Name = "IsDoubleSided",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "TotalArea",
                        IsFormula = true,
                        DefaultValue = "=( Quantity * Area ) * IF(IsDoubleSided = TRUE, 2, 1)",
                    },
                },
                Tables = new List<AssemblyTable>()
                {
                    new AssemblyTable()
                    {
                        BID = TestConstants.BID,
                        Label = "TierTable",
                        TableType = AssemblyTableType.Custom,
                        CellDataJSON = "[[15,20],[12,17]]",
                        VariableName = "TierTable",
                        CellDataType = DataType.Number,
                        ColumnCount = 1,
                        ColumnDataType = DataType.Number,
                        ColumnIsSorted = false,
                        ColumnLabel = "Quantity",
                        ColumnMatchType = AssemblyTableMatchType.ExactMatch,
                        ColumnUnitID = 0,
                        ColumnValuesJSON = "[{\"index\":0,\"value\":18,\"name\":\"Default\"}, {\"index\":2,\"value\":36,\"name\":\"Default\"}]",
                        ColumnVariableTempID = tmpID.ToString(),
                        ColumnVariableID = 0,
                        Description = "",
                        ModifiedDT = DateTime.Parse("2019-04-24T16:07:46.7515121"),
                        RowCount = 3,
                        RowDataType = DataType.Number,
                        RowDefaultMarkupJSON = "",
                        RowIsSorted = false,
                        RowLabel = "Row",
                        RowMatchType = 0,
                        RowUnitID = 0,
                        RowValuesJSON = "[{\"index\":0,\"value\":\"TestTier\"},{\"index\":1,\"value\":\"AnotherBogusName\"}]",
                        IsTierTable = true,
                        ClassTypeID = 12047
                    }
                },
                PricingType = AssemblyPricingType.MarketBasedPricing,
                PriceFormulaType = PriceFormulaType.PriceTable,
                HasTierTable = true,

            };

            assembly = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, "/api/assemblypart", assembly, HttpStatusCode.OK);

            try
            {
                //test valid request
                ItemPriceRequest testRequest = new ItemPriceRequest()
                {
                    EngineType = PricingEngineType.Supplied,
                    Quantity = 5,
                    CompanyID = dbCompany.ID,
                    Components = new List<ComponentPriceRequest>
                           {
                               new ComponentPriceRequest
                               {
                                   ComponentType = OrderItemComponentType.Assembly,
                                   ComponentID = assembly.ID,
                                   TotalQuantity  = 5,
                                   PriceUnit = 20m,
                                   PriceUnitOV = true,
                                   Variables = new Dictionary<string, VariableValue>()
                                   {
                                       { "Quantity", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 2m } },
                                       { "Height", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 36m } },
                                       { "Width", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 12m } },
                                       { "IsDoubleSided", new VariableValue() { DataType = DataType.Boolean, ValueOV = true, ValueAsBool = true } },
                                   }
                               }
                           },
                };

                ItemPriceResult result = await EndpointTests.AssertPostStatusCode<ItemPriceResult>(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);

                Assert.IsNotNull(result);

                Assert.AreEqual(100m, result.PricePreTax);
                Assert.AreEqual(100m, result.PriceComponent);
                Assert.AreEqual(0m, result.CostLabor);
                Assert.AreEqual(0m, result.CostMachine);
                Assert.AreEqual(0m, result.CostMaterial);
                Assert.AreEqual(0m, result.CostTotal);

                Assert.IsNull(result.TaxAmount);
                Assert.IsNull(result.PriceTotal);
                Assert.AreEqual(1, result.Components.Count);
                Assert.IsNotNull(result.Components[0].Variables);

                VariableValue varValue;

                varValue = result.Components[0].Variables.GetValueOrDefault("Height");
                Assert.IsNotNull(varValue);
                Assert.AreEqual(36m, varValue.ValueAsDecimal);

                varValue = result.Components[0].Variables.GetValueOrDefault("Width");
                Assert.IsNotNull(varValue);
                Assert.AreEqual(12m, varValue.ValueAsDecimal);
            }
            finally
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"/api/assemblypart/{assembly.ID}", HttpStatusCode.NoContent);
                dbCompany.PricingTierID = originalPricingTier;
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        [TestCategory("compute")]
        [TestCategory("GL")]
        public async Task TestOrderItemComputeWithVariables()
        {
            //test invalid request
            object invalidRequest = JsonConvert.DeserializeObject(@"{ 'EngineType': 'Free' }"); // force invalid request by setting OrderpriceRequest.Result property to invalid type
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(invalidRequest), HttpStatusCode.BadRequest);

            AssemblyData assembly = new AssemblyData()
            {
                Name = "TestAssembly",
                IsActive = true,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Price",
                        DefaultValue = "100",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Height",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Width",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Area",
                        IsFormula = true,
                        DefaultValue = "=Height * Width",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Boolean,
                        ElementType = AssemblyElementType.Checkbox,
                        Name = "IsDoubleSided",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "TotalArea",
                        IsFormula = true,
                        DefaultValue = "=( Quantity * Area ) * IF(IsDoubleSided = TRUE, 2, 1)",
                    },
                }
            };

            assembly = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, "/api/assemblypart", assembly, HttpStatusCode.OK);

            try
            {
                //test valid request
                ItemPriceRequest testRequest = new ItemPriceRequest()
                {
                    EngineType = PricingEngineType.Supplied,
                    Quantity = 5,
                    Components = new List<ComponentPriceRequest>
                           {
                               new ComponentPriceRequest
                               {
                                   ComponentType = OrderItemComponentType.Assembly,
                                   ComponentID = assembly.ID,
                                   TotalQuantity = 5,
                                   PriceUnit = 20m,
                                   PriceUnitOV = true,
                                   Variables = new Dictionary<string, VariableValue>()
                                   {
                                       { "Quantity", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 2m } },
                                       { "Height", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 36m } },
                                       { "Width", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 12m } },
                                       { "IsDoubleSided", new VariableValue() { DataType = DataType.Boolean, ValueOV = true, ValueAsBool = true } },
                                   }
                               }
                           },
                };

                ItemPriceResult result = await EndpointTests.AssertPostStatusCode<ItemPriceResult>(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);

                Assert.IsNotNull(result);

                Assert.AreEqual(100m, result.PricePreTax);
                Assert.AreEqual(100m, result.PriceComponent);
                Assert.AreEqual(0m, result.CostLabor);
                Assert.AreEqual(0m, result.CostMachine);
                Assert.AreEqual(0m, result.CostMaterial);
                Assert.AreEqual(0m, result.CostTotal);

                Assert.IsNull(result.TaxAmount);
                Assert.IsNull(result.PriceTotal);
                Assert.AreEqual(1, result.Components.Count);
                Assert.IsNotNull(result.Components[0].Variables);

                VariableValue varValue;

                varValue = result.Components[0].Variables.GetValueOrDefault("Height");
                Assert.IsNotNull(varValue);
                Assert.AreEqual(36m, varValue.ValueAsDecimal);

                varValue = result.Components[0].Variables.GetValueOrDefault("Width");
                Assert.IsNotNull(varValue);
                Assert.AreEqual(12m, varValue.ValueAsDecimal);
            }
            finally
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"/api/assemblypart/{assembly.ID}", HttpStatusCode.NoContent);
            }
        }

        [TestMethod]
        [TestCategory("compute")]
        [TestCategory("GL")]
        public async Task TestOrderItemComputeWithErrorFunction()
        {
            AssemblyData assembly = new AssemblyData()
            {
                Name = "TestAssembly",
                IsActive = true,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.SingleLineText,
                        Name = "ErrorVariableNumber",
                        IsFormula = true,
                        DefaultValue = "=Error(\"Number Error\")",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Boolean,
                        ElementType = AssemblyElementType.SingleLineText,
                        Name = "ErrorVariableBoolean",
                        IsFormula = true,
                        DefaultValue = "=Error(\"Boolean Error\")",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.String,
                        ElementType = AssemblyElementType.SingleLineText,
                        Name = "ErrorVariableString",
                        IsFormula = true,
                        DefaultValue = "=Error(\"String Error\")",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Price",
                        DefaultValue = "100",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Height",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Width",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Area",
                        IsFormula = true,
                        DefaultValue = "=Height * Width",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Boolean,
                        ElementType = AssemblyElementType.Checkbox,
                        Name = "IsDoubleSided",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "TotalArea",
                        IsFormula = true,
                        DefaultValue = "=( Quantity * Area ) * IF(IsDoubleSided = TRUE, 2, 1)",
                    },
                }
            };

            assembly = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, "/api/assemblypart", assembly, HttpStatusCode.OK);

            try
            {
                //test valid request
                ItemPriceRequest testRequest = new ItemPriceRequest()
                {
                    EngineType = PricingEngineType.Supplied,
                    Quantity = 5,
                    Components = new List<ComponentPriceRequest>
                           {
                               new ComponentPriceRequest
                               {
                                   ComponentType = OrderItemComponentType.Assembly,
                                   ComponentID = assembly.ID,
                                   TotalQuantity = 5,
                                   PriceUnit = 20m,
                                   PriceUnitOV = true,
                                   Variables = new Dictionary<string, VariableValue>()
                                   {
                                       { "Quantity", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 2m } },
                                       { "Height", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 36m } },
                                       { "Width", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 12m } },
                                       { "IsDoubleSided", new VariableValue() { DataType = DataType.Boolean, ValueOV = true, ValueAsBool = true } },
                                   }
                               }
                           },
                };

                ItemPriceResult result = await EndpointTests.AssertPostStatusCode<ItemPriceResult>(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);

                Assert.IsNotNull(result);
                Assert.IsFalse(result.Success);

                Assert.AreEqual(100m, result.PricePreTax);
                Assert.AreEqual(100m, result.PriceComponent);
                Assert.AreEqual(0m, result.CostLabor);
                Assert.AreEqual(0m, result.CostMachine);
                Assert.AreEqual(0m, result.CostMaterial);
                Assert.AreEqual(0m, result.CostTotal);

                Assert.IsNull(result.TaxAmount);
                Assert.IsNull(result.PriceTotal);
                Assert.AreEqual(1, result.Components.Count);
                Assert.IsNotNull(result.Components[0].Variables);

                VariableValue varValue;

                varValue = result.Components[0].Variables.GetValueOrDefault("Height");
                Assert.IsNotNull(varValue);
                Assert.AreEqual(36m, varValue.ValueAsDecimal);

                varValue = result.Components[0].Variables.GetValueOrDefault("Width");
                Assert.IsNotNull(varValue);
                Assert.AreEqual(12m, varValue.ValueAsDecimal);

                var numberVariableError = result.Components[0].Variables.GetValueOrDefault("ErrorVariableNumber");
                Assert.IsNull(numberVariableError.Value);

                var booleanVariableError = result.Components[0].Variables.GetValueOrDefault("ErrorVariableBoolean");
                Assert.IsNull(booleanVariableError.Value);

                var stringVariableError = result.Components[0].Variables.GetValueOrDefault("ErrorVariableString");
                Assert.IsNull(stringVariableError.Value);

                Assert.AreEqual(3, result.Components[0].Errors.Count);
                Assert.IsTrue(result.Components[0].Errors.Select(x => x.Message).Contains("Boolean Error"));
                Assert.IsTrue(result.Components[0].Errors.Select(x => x.Message).Contains("String Error"));
                Assert.IsTrue(result.Components[0].Errors.Select(x => x.Message).Contains("Number Error"));

            }
            finally
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"/api/assemblypart/{assembly.ID}", HttpStatusCode.NoContent);
            }
        }

        [TestMethod]
        [TestCategory("compute")]
        [TestCategory("GL")]
        public async Task TestOrderItemComputeWithNullFunction()
        {
            AssemblyData assembly = new AssemblyData()
            {
                Name = "TestAssembly",
                IsActive = true,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.SingleLineText,
                        Name = "ErrorVariableNumber",
                        IsFormula = true,
                        DefaultValue = "=Null()",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Boolean,
                        ElementType = AssemblyElementType.SingleLineText,
                        Name = "ErrorVariableBoolean",
                        IsFormula = true,
                        DefaultValue = "=Null()",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.String,
                        ElementType = AssemblyElementType.SingleLineText,
                        Name = "ErrorVariableString",
                        IsFormula = true,
                        DefaultValue = "=Null()",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Price",
                        DefaultValue = "100",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Height",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Width",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "Area",
                        IsFormula = true,
                        DefaultValue = "=Height * Width",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Boolean,
                        ElementType = AssemblyElementType.Checkbox,
                        Name = "IsDoubleSided",
                    },
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "TotalArea",
                        IsFormula = true,
                        DefaultValue = "=( Quantity * Area ) * IF(IsDoubleSided = TRUE, 2, 1)",
                    },
                }
            };

            assembly = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, "/api/assemblypart", assembly, HttpStatusCode.OK);

            try
            {
                //test valid request
                ItemPriceRequest testRequest = new ItemPriceRequest()
                {
                    EngineType = PricingEngineType.Supplied,
                    Quantity = 5,
                    Components = new List<ComponentPriceRequest>
                           {
                               new ComponentPriceRequest
                               {
                                   ComponentType = OrderItemComponentType.Assembly,
                                   ComponentID = assembly.ID,
                                   TotalQuantity = 5,
                                   PriceUnit = 20m,
                                   PriceUnitOV = true,
                                   Variables = new Dictionary<string, VariableValue>()
                                   {
                                       { "Quantity", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 2m } },
                                       { "Height", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 36m } },
                                       { "Width", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 12m } },
                                       { "IsDoubleSided", new VariableValue() { DataType = DataType.Boolean, ValueOV = true, ValueAsBool = true } },
                                   }
                               }
                           },
                };

                ItemPriceResult result = await EndpointTests.AssertPostStatusCode<ItemPriceResult>(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);

                Assert.IsNotNull(result);
                Assert.IsTrue(result.Success);

                Assert.AreEqual(100m, result.PricePreTax);
                Assert.AreEqual(100m, result.PriceComponent);
                Assert.AreEqual(0m, result.CostLabor);
                Assert.AreEqual(0m, result.CostMachine);
                Assert.AreEqual(0m, result.CostMaterial);
                Assert.AreEqual(0m, result.CostTotal);

                Assert.IsNull(result.TaxAmount);
                Assert.IsNull(result.PriceTotal);
                Assert.AreEqual(1, result.Components.Count);
                Assert.IsNotNull(result.Components[0].Variables);

                VariableValue varValue;

                varValue = result.Components[0].Variables.GetValueOrDefault("Height");
                Assert.IsNotNull(varValue);
                Assert.AreEqual(36m, varValue.ValueAsDecimal);

                varValue = result.Components[0].Variables.GetValueOrDefault("Width");
                Assert.IsNotNull(varValue);
                Assert.AreEqual(12m, varValue.ValueAsDecimal);

                var numberVariableError = result.Components[0].Variables.GetValueOrDefault("ErrorVariableNumber");
                Assert.IsNull(numberVariableError.Value);

                var booleanVariableError = result.Components[0].Variables.GetValueOrDefault("ErrorVariableBoolean");
                Assert.IsNull(booleanVariableError.Value);

                var stringVariableError = result.Components[0].Variables.GetValueOrDefault("ErrorVariableString");
                Assert.IsNull(stringVariableError.Value);

                Assert.IsNull(result.Components[0].Errors);
                
            }
            finally
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"/api/assemblypart/{assembly.ID}", HttpStatusCode.NoContent);
            }
        }

        [TestMethod]
        [TestCategory("compute")]
        [TestCategory("GL")]
        public async Task TestOrderItemComputeWithParts()
        {
            //test invalid request
            object invalidRequest = JsonConvert.DeserializeObject(@"{ 'EngineType': 'Free' }"); // force invalid request by setting OrderpriceRequest.Result property to invalid type
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(invalidRequest), HttpStatusCode.BadRequest);

            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleLaborData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            LaborData labor = new LaborData()
            {
                Name = "Pricing Test Labor",
                Description = "Pricing Test Labor",
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InvoiceText = "Pricing Test Labor",
                EstimatingCostFixed = 100m,      
            };

            MachineData machine = new MachineData()
            {
                Name = "Pricing Test Labor",
                Description = "Pricing Test Labor",
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID
            };

            MaterialData material = new MaterialData()
            {
                Name = "Pricing Test Labor",
                Description = "Pricing Test Labor",
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InvoiceText = "Pricing Test Material",
                EstimatingCost = 120m,
            };


            int? laborId = null;
            int? materialId = null;
            int? machineId = null;
            try
            {
                labor = await EndpointTests.AssertPostStatusCode<LaborData>(client, "/api/laborpart", JsonConvert.SerializeObject(labor), HttpStatusCode.OK);
                laborId = labor.ID;

                machine = await EndpointTests.AssertPostStatusCode<MachineData>(client, "/api/machinepart", JsonConvert.SerializeObject(machine), HttpStatusCode.OK);
                machineId = machine.ID;

                material = await EndpointTests.AssertPostStatusCode<MaterialData>(client, "/api/materialpart", JsonConvert.SerializeObject(material), HttpStatusCode.OK);
                materialId = material.ID;

                //test valid request
                ItemPriceRequest testRequest = new ItemPriceRequest()
                {
                    EngineType = PricingEngineType.Supplied,
                    Quantity = 1,
                    Components = new List<ComponentPriceRequest>
                           {
                               new ComponentPriceRequest
                               {
                                   ComponentType = OrderItemComponentType.Labor,
                                   TotalQuantity = 1,
                                   PriceUnit = 20m,
                                   PriceUnitOV = true,
                                   ComponentID = laborId.GetValueOrDefault(-1),
                               },
                               new ComponentPriceRequest
                               {
                                   ComponentType = OrderItemComponentType.Machine,
                                   TotalQuantity = 1,
                                   PriceUnit = 30m,
                                   PriceUnitOV = true,
                                   ComponentID = machineId.GetValueOrDefault(-1),
                               },
                               new ComponentPriceRequest
                               {
                                   ComponentType = OrderItemComponentType.Material,
                                   TotalQuantity = 1,
                                   PriceUnit = 40m,
                                   PriceUnitOV = true,
                                   ComponentID = materialId.GetValueOrDefault(-1),
                               },
                           },
                };

                ItemPriceResult result = await EndpointTests.AssertPostStatusCode<ItemPriceResult>(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);

                Assert.IsNotNull(result);

                Assert.AreEqual(90m, result.PricePreTax);
                Assert.AreEqual(100m, result.CostLabor);
                Assert.AreEqual(0m, result.CostMachine);
                Assert.AreEqual(120m, result.CostMaterial);
                Assert.AreEqual(220m, result.CostTotal);

                Assert.IsNull(result.TaxAmount);
                Assert.IsNull(result.PriceTotal);
                Assert.AreEqual(3, result.Components.Count);


                Assert.AreEqual(20m, result.Components[0].PricePreTax);
                Assert.AreEqual(100m, result.Components[0].CostLabor);
                Assert.AreEqual(0m, result.Components[0].CostMachine);
                Assert.AreEqual(0m, result.Components[0].CostMaterial);
                Assert.AreEqual(100m, result.Components[0].CostNet);

                Assert.AreEqual(30m, result.Components[1].PricePreTax);
                Assert.AreEqual(0m, result.Components[1].CostLabor);
                Assert.AreEqual(0m, result.Components[1].CostMachine);
                Assert.AreEqual(0m, result.Components[1].CostMaterial);
                Assert.AreEqual(0m, result.Components[1].CostNet);

                Assert.AreEqual(40m, result.Components[2].PricePreTax);
                Assert.AreEqual(0m, result.Components[2].CostLabor);
                Assert.AreEqual(0m, result.Components[2].CostMachine);
                Assert.AreEqual(120m, result.Components[2].CostMaterial);
                Assert.AreEqual(120m, result.Components[2].CostNet);
            }
            finally
            {
                if (laborId.HasValue)
                {
                    try
                    {
                        await client.DeleteAsync($"/api/laborpart/{laborId.Value}");
                    }
                    catch { }
                }

                if (machineId.HasValue)
                {
                    try
                    {
                        await client.DeleteAsync($"/api/machinepart/{machineId.Value}");
                    }
                    catch { }
                }

                if (materialId.HasValue)
                {
                    try
                    {
                        await client.DeleteAsync($"/api/materialpart/{materialId.Value}");
                    }
                    catch { }
                }
            }
        }

        [TestMethod]
        [TestCategory("compute")]
        [TestCategory("GL")]
        public async Task TestLinkedAssemblyComputeIsVended()
        {
            AssemblyData assembly = new AssemblyData()
            {
                Name = "TestAssembly",
                IsActive = true,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        DataType = DataType.String,
                        ElementType = AssemblyElementType.SingleLineText,
                        Name = "IsVendedVar",
                        IsFormula = true,
                        DefaultValue = "=IF(IsVended = true, \"Vended\", \"Manufactured\") ",
                    },

                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "ParentQuantityVar",
                        IsFormula = true,
                        DefaultValue = "=ParentQuantity",
                    },

                    new AssemblyVariable()
                    {
                        DataType = DataType.String,
                        ElementType = AssemblyElementType.SingleLineText,
                        Name = "CalculatedCost",
                        IsFormula = true,
                        DefaultValue = "=text(cost, \"G\")",
                    },
                }
            };

            assembly = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, "/api/assemblypart", assembly, HttpStatusCode.OK);
            try
            {
                //test valid request
                ItemPriceRequest testRequest = new ItemPriceRequest()
                {
                    EngineType = PricingEngineType.Supplied,
                    Quantity = 50,
                    IsOutsourced = true,
                    Components = new List<ComponentPriceRequest>
                           {
                               new ComponentPriceRequest
                               {
                                   ComponentType = OrderItemComponentType.Assembly,
                                   ComponentID = assembly.ID,
                                   TotalQuantity = 5,
                                   PriceUnit = 20m,
                                   PriceUnitOV = true,
                               }
                           },
                };

                ItemPriceResult result = await EndpointTests.AssertPostStatusCode<ItemPriceResult>(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);
                Assert.IsNotNull(result);
                var component = result.Components.FirstOrDefault();
                Assert.IsNotNull(component);
                var val = component.Variables.FirstOrDefault(i => i.Key == "IsVendedVar").Value;
                Assert.IsNotNull(val);
                Assert.AreEqual("Vended", val.Value);

            }
            finally
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"/api/assemblypart/{assembly.ID}", HttpStatusCode.NoContent);
            }
        }

        [TestMethod]
        [TestCategory("compute")]
        [TestCategory("GL")]
        public async Task TestLinkedAssemblyComputeParentQuantity()
        {
            AssemblyData assembly = new AssemblyData()
            {
                Name = "TestAssembly",
                IsActive = true,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        DataType = DataType.Number,
                        ElementType = AssemblyElementType.Number,
                        Name = "ParentQuantityVar",
                        IsFormula = true,
                        DefaultValue = "=ParentQuantity",
                    },
                }
            };

            assembly = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, "/api/assemblypart", assembly, HttpStatusCode.OK);
            try
            {
                //test valid request
                ItemPriceRequest testRequest = new ItemPriceRequest()
                {
                    EngineType = PricingEngineType.Supplied,
                    Quantity = 50,
                    IsOutsourced = true,
                    Components = new List<ComponentPriceRequest>
                           {
                               new ComponentPriceRequest
                               {
                                   ComponentType = OrderItemComponentType.Assembly,
                                   ComponentID = assembly.ID,
                                   TotalQuantity = 5,
                                   PriceUnit = 20m,
                                   PriceUnitOV = true,
                               }
                           },
                };

                ItemPriceResult result = await EndpointTests.AssertPostStatusCode<ItemPriceResult>(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);
                Assert.IsNotNull(result);
                var component = result.Components.FirstOrDefault();
                Assert.IsNotNull(component);
                var val = component.Variables.FirstOrDefault(i => i.Key == "ParentQuantityVar").Value;
                Assert.IsNotNull(val);
                Assert.AreEqual(testRequest.Quantity.ToString(),val.Value);
            }
            finally
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"/api/assemblypart/{assembly.ID}", HttpStatusCode.NoContent);
            }
        }

        [TestMethod]
        [TestCategory("compute")]
        [TestCategory("GL")]
        public async Task TestLinkedAssemblyComputeCost()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleLaborData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            MaterialData material = new MaterialData()
            {
                Name = "Pricing Test Material",
                Description = "Pricing Test Material",
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InvoiceText = "Pricing Test Material",
                EstimatingCost = 120m,
            };
            
            int? materialId = null;
            try
            {
                material = await EndpointTests.AssertPostStatusCode<MaterialData>(client, "/api/materialpart", JsonConvert.SerializeObject(material), HttpStatusCode.OK);
                materialId = material.ID;

                //test valid request
                ItemPriceRequest testRequest = new ItemPriceRequest()
                {
                    EngineType = PricingEngineType.Supplied,
                    Quantity = 1,
                    Components = new List<ComponentPriceRequest>
                        {
                            new ComponentPriceRequest
                            {
                                ComponentType = OrderItemComponentType.Material,
                                TotalQuantity = 2,
                                ComponentID = materialId.GetValueOrDefault(-1),
                            },
                        },
                };

                ItemPriceResult result = await EndpointTests.AssertPostStatusCode<ItemPriceResult>(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);

                Assert.IsNotNull(result);
                Assert.AreEqual(material.EstimatingCost * 2, result.CostMaterial);
            }
            finally
            {
                if (materialId.HasValue)
                {
                    try
                    {
                        await client.DeleteAsync($"/api/materialpart/{materialId.Value}");
                    }
                    catch { }
                }
            }
        }

        [TestMethod]
        public async Task TestOrderItemCanDeleteTest()
        {
            // Setup data 
            // Gather data needed
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var postedOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order", testOrder, HttpStatusCode.OK);
            // Create an OrderItem
            var testOrderItem = Utils.GetTestOrderItemData(postedOrder);

            var postedOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"{apiUrl}", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(postedOrderItem);

            // Test Endpoint
            var statusResp = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{postedOrderItem.ID}/Action/CanDelete", HttpStatusCode.OK);

            // Assert Results
            Assert.IsTrue(statusResp.Success);
            Assert.IsTrue(statusResp.Value.Value);

            // Test Endpoint
            statusResp = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{postedOrderItem.ID}/Action/CanDelete", HttpStatusCode.OK);

            // Assert Results
            Assert.IsTrue(statusResp.Success);
            Assert.IsTrue(statusResp.Value.Value);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{postedOrderItem.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{postedOrder.ID}", HttpStatusCode.NoContent);
        }

        private async Task<OrderItemData> CreateNumberedItem(OrderData order, short number)
        {
            OrderItemData testItem = new OrderItemData
            {
                OrderID = order.ID,
                ItemNumber = number,
                Quantity = 1,
                Name = $"Original Item {number}",
                Description = $"Original Item {number}",
                OrderStatusID = order.OrderStatusID,
                ProductionLocationID = order.ProductionLocationID,
                ItemStatusID = 23,
                TaxGroupID = order.TaxGroupID,
                TransactionType = order.TransactionType
            };

            return await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"{apiUrl}/", testItem, HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestOrderItemMoveBefore()
        {
            // Create temp order
            var testOrder = new
            {
                TransactionType = OrderTransactionType.Order,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                Description = "Test Order",
                CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID
            };

            OrderData createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiOrderUrl}/", testOrder, HttpStatusCode.OK);
            int orderID = createdOrder.ID;
            try
            {
                // Create 5 line items
                int item1ID = (await CreateNumberedItem(createdOrder, 1)).ID;
                int item2ID = (await CreateNumberedItem(createdOrder, 2)).ID;
                int item3ID = (await CreateNumberedItem(createdOrder, 3)).ID;
                int item4ID = (await CreateNumberedItem(createdOrder, 4)).ID;
                int item5ID = (await CreateNumberedItem(createdOrder, 5)).ID;

                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{item1ID}/action/movebefore/-99", HttpStatusCode.NotFound);

                OrderItemData item1;
                OrderItemData item2;
                OrderItemData item3;
                OrderItemData item4;
                OrderItemData item5;

                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{item1ID}/action/movebefore/{item5ID}", HttpStatusCode.OK);

                item1 = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{item1ID}", HttpStatusCode.OK);
                item2 = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{item2ID}", HttpStatusCode.OK);
                item3 = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{item3ID}", HttpStatusCode.OK);
                item4 = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{item4ID}", HttpStatusCode.OK);
                item5 = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{item5ID}", HttpStatusCode.OK);

                Assert.AreEqual(1, item2.ItemNumber);
                Assert.AreEqual(2, item3.ItemNumber);
                Assert.AreEqual(3, item4.ItemNumber);
                Assert.AreEqual(4, item1.ItemNumber);
                Assert.AreEqual(5, item5.ItemNumber);

                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{item5ID}/action/movebefore/{item2ID}", HttpStatusCode.OK);

                item1 = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{item1ID}", HttpStatusCode.OK);
                item2 = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{item2ID}", HttpStatusCode.OK);
                item3 = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{item3ID}", HttpStatusCode.OK);
                item4 = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{item4ID}", HttpStatusCode.OK);
                item5 = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{item5ID}", HttpStatusCode.OK);

                Assert.AreEqual(1, item5.ItemNumber);
                Assert.AreEqual(2, item2.ItemNumber);
                Assert.AreEqual(3, item3.ItemNumber);
                Assert.AreEqual(4, item4.ItemNumber);
                Assert.AreEqual(5, item1.ItemNumber);
            }

            finally
            {
                await client.DeleteAsync($"{apiOrderUrl}/{orderID}");
            }
        }

        [TestMethod]
        public async Task TestOrderItemMoveAfter()
        {
            // Create temp order
            var testOrder = new
            {
                TransactionType = OrderTransactionType.Order,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                Description = "Test Order",
                CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID
            };

            OrderData createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiOrderUrl}/", testOrder, HttpStatusCode.OK);
            int orderID = createdOrder.ID;
        }

        [TestMethod]
        public async Task TestOrderItemEmployeeRoles()
        {
            #region Create Order and OrderItem

            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrder);

            var testOrderItem = Utils.GetTestOrderItemData(createdOrder);
            var createdOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrderItem);

            #endregion

            var employee = (await EndpointTests.AssertGetStatusCode<SimpleEmployeeData[]>(client, "/api/employee/simplelist", HttpStatusCode.OK)).FirstOrDefault();
            var employeeRole = (await EndpointTests.AssertGetStatusCode<SimpleEmployeeRole[]>(client, "/api/employeerole/simplelist", HttpStatusCode.OK)).FirstOrDefault();
            Assert.IsNotNull(employee);
            Assert.IsNotNull(employeeRole);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}/Action/LinkEmployee/{employeeRole.ID}/{employee.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}/Action/LinkEmployee/{employeeRole.ID}/{employee.ID}", HttpStatusCode.OK);

            #region DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.NoContent);

            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion

        }

        [TestMethod]
        public async Task TestOrderItemContactRoles()
        {
            #region Create Order and OrderItem

            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrder);

            var testOrderItem = Utils.GetTestOrderItemData(createdOrder);
            var createdOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrderItem);

            #endregion

            var contact = (await EndpointTests.AssertGetStatusCode<SimpleEmployeeData[]>(client, "/api/contact/simplelist", HttpStatusCode.OK)).FirstOrDefault();
            Assert.IsNotNull(contact);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}/Action/LinkContact/{OrderContactRoleType.Primary}/{contact.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}/Action/LinkContact/{OrderContactRoleType.Primary}/{contact.ID}", HttpStatusCode.OK);

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.NoContent);

            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion

        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task TestOrderItemChangeStatus()
        {
            #region Create Order and OrderItem
            var ctx = this.GetApiContext();
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrder);

            var testOrderItem = Utils.GetTestOrderItemData(createdOrder);
            var createdOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrderItem);

            #endregion

            short statusID = 16; // should fail (happens before)
            string failStr = "The new Line Item Status can not be for an Order Status that is before the current one.";
            string statusName = "Invoicing";

            // test fail
            var resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdOrderItem.ID}/Action/ChangeItemStatus/{statusID}", HttpStatusCode.BadRequest);
            Assert.IsNotNull(resp);
            Assert.IsFalse(resp.Success);
            Assert.AreEqual(failStr, resp.Message);

            // test passing ID
            statusID = 23; // Built, should pass
            resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdOrderItem.ID}/Action/ChangeItemStatus/{statusID}", HttpStatusCode.OK);
            Assert.IsNotNull(resp);
            Assert.IsTrue(resp.Success);

            // test passing Name
            statusName = (await ctx.OrderItemStatus.FirstOrDefaultAsync(x => x.BID == 1 && x.ID == statusID))?.Name ?? "Built";
            resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdOrderItem.ID}/Action/ChangeItemStatus/{statusName}", HttpStatusCode.OK);
            Assert.IsNotNull(resp);
            Assert.IsTrue(resp.Success);

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.NotFound);

            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        [TestMethod]
        public async Task TestOrderItemKeyDate()
        {
            #region Create Order and OrderItem

            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrder);

            var testOrderItem = Utils.GetTestOrderItemData(createdOrder);
            var createdOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrderItem);

            #endregion

            // base put
            var resp = await EndpointTests.AssertPutStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdOrderItem.ID}/Action/Date/{OrderKeyDateType.Approved}", HttpStatusCode.OK);
            Assert.IsNotNull(resp);
            Assert.IsTrue(resp.Success);

            var orderItemGet = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}?DateLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(orderItemGet);
            Assert.IsNotNull(orderItemGet.Dates);
            Assert.AreEqual(1, orderItemGet.Dates.Count);
            Assert.IsFalse(orderItemGet.Dates.First().IsFirmDate);
            var defaultDate = orderItemGet.Dates.First().KeyDT.ToString("yyyy-MM-dd");

            // put with IsFirmDate
            resp = await EndpointTests.AssertPutStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdOrderItem.ID}/Action/Date/{OrderKeyDateType.Approved}?IsFirmDate=true", HttpStatusCode.OK);
            Assert.IsNotNull(resp);
            Assert.IsTrue(resp.Success);

            orderItemGet = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}?DateLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(orderItemGet);
            Assert.IsNotNull(orderItemGet.Dates);
            Assert.AreEqual(1, orderItemGet.Dates.Count);
            Assert.IsTrue(orderItemGet.Dates.First().IsFirmDate);
            Assert.AreEqual(defaultDate, orderItemGet.Dates.First().KeyDT.ToString("yyyy-MM-dd"));

            // put with specific DateTime
            var specificDate = new DateTime(2020, 1, 31).ToString("yyyy-MM-dd HH:mm:ss");
            resp = await EndpointTests.AssertPutStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdOrderItem.ID}/Action/Date/{OrderKeyDateType.Approved}/{specificDate}/", HttpStatusCode.OK);
            Assert.IsNotNull(resp);
            Assert.IsTrue(resp.Success);

            orderItemGet = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}?DateLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(orderItemGet);
            Assert.IsNotNull(orderItemGet.Dates);
            Assert.AreEqual(1, orderItemGet.Dates.Count);
            Assert.AreEqual(specificDate, orderItemGet.Dates.First().KeyDT.ToString("yyyy-MM-dd HH:mm:ss"));

            // put with specific DateTime and IsFirmDate
            resp = await EndpointTests.AssertPutStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdOrderItem.ID}/Action/Date/{OrderKeyDateType.Approved}/{specificDate}/?IsFirmDate=false", HttpStatusCode.OK);
            Assert.IsNotNull(resp);
            Assert.IsTrue(resp.Success);

            orderItemGet = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}?DateLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(orderItemGet);
            Assert.IsNotNull(orderItemGet.Dates);
            Assert.AreEqual(1, orderItemGet.Dates.Count);
            Assert.IsFalse(orderItemGet.Dates.First().IsFirmDate);
            Assert.AreEqual(specificDate, orderItemGet.Dates.First().KeyDT.ToString("yyyy-MM-dd HH:mm:ss"));

            // delete
            resp = await EndpointTests.AssertDeleteStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdOrderItem.ID}/Action/Date/{OrderKeyDateType.Approved}", HttpStatusCode.OK);
            orderItemGet = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}?DateLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(orderItemGet);
            Assert.IsNotNull(orderItemGet.Dates);
            Assert.AreEqual(0, orderItemGet.Dates.Count);

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.NoContent);

            #endregion
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task TestOrderItemComponentsCrud()
        {            
            #region Create Order and OrderItem

            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrder);

            var testOrderItem = Utils.GetTestOrderItemData(createdOrder);

            var component1 = new OrderItemComponent()
            {
                BID = createdOrder.BID,
                OrderID = createdOrder.ID,
                Name = "1",
            };

            var component2 = new OrderItemComponent()
            {
                BID = createdOrder.BID,
                OrderID = createdOrder.ID,
                Name = "2",
            };


            testOrderItem.Components = new List<OrderItemComponent> {component1, component2};
            OrderItemData createdOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrderItem);
            Assert.IsNotNull(createdOrderItem.Components);
            component1.ID = createdOrderItem.Components.FirstOrDefault(x => x.Name == component1.Name).ID;
            component2.ID = createdOrderItem.Components.FirstOrDefault(x => x.Name == component2.Name).ID;

            #endregion Create Order and OrderItem

            #region Update

            testOrderItem.Components.Remove(component1);
            OrderItemData updateResponse = null;
            updateResponse = await EndpointTests.AssertPutStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(updateResponse);
            Assert.IsNotNull(updateResponse.Components);
            Assert.AreEqual(1, updateResponse.Components.Count);

            var component3 = new OrderItemComponent()
            {
                BID = createdOrder.BID,
                OrderID = createdOrder.ID,
                Name = "3",
            };
            testOrderItem.Components.Add(component3);

            updateResponse = await EndpointTests.AssertPutStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(updateResponse);
            Assert.IsNotNull(updateResponse.Components);
            Assert.AreEqual(2, updateResponse.Components.Count);
            #endregion Update

            #region Clone

            //EntityActionChangeResponse cloneResponse = null;
            //cloneResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdOrderItem.ID}/clone", null, HttpStatusCode.OK);
            //Assert.IsNotNull(cloneResponse);

            #endregion Clone

            #region Delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.NoContent);

            //await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{cloneResponse.Id}", HttpStatusCode.NoContent);
            //await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{cloneResponse.Id}", HttpStatusCode.NotFound);

            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion Delete
        }

        [TestMethod]
        public async Task TestOrderItemSurchargesCrud()
        {
            #region Create Order and OrderItem

            var ctx = GetApiContext();
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrder);

            var testOrderItem = Utils.GetTestOrderItemData(createdOrder);

            var surcharge = new SurchargeDef()
            {
                BID = TestConstants.BID,
                ID = -99,
                Name = "Test.SurchargeDef",
                IsActive = false,
                ModifiedDT = DateTime.UtcNow,
                ClassTypeID = ClassType.SurchargeDef.ID(),
                TaxCode = ctx.TaxabilityCodes.FirstOrDefault(x => x.BID== TestConstants.BID),
                IncomeAccountID = ctx.GLAccount.FirstOrDefault(x => x.BID == TestConstants.BID).ID
            };

            if (!ctx.SurchargeDef.Where(s => s.ID == -99 && s.BID== TestConstants.BID).Any())
            {
                await ctx.SurchargeDef.AddAsync(surcharge);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
            }
            else
            {
                surcharge = ctx.SurchargeDef.Where(s => s.ID == -99).First();
            }
            var surcharge1 = new OrderItemSurcharge()
            {
                BID = createdOrder.BID,
                Name = "1",
                TaxCode = ctx.TaxabilityCodes.FirstOrDefault(),
                IncomeAccountID = ctx.GLAccount.FirstOrDefault().ID,
                SurchargeDefID = ctx.SurchargeDef.FirstOrDefault().ID
            };

            var surcharge2 = new OrderItemSurcharge()
            {
                BID = createdOrder.BID,
                Name = "2",
                TaxCode = ctx.TaxabilityCodes.FirstOrDefault(),
                IncomeAccountID = ctx.GLAccount.FirstOrDefault().ID,
                SurchargeDefID = ctx.SurchargeDef.FirstOrDefault().ID
            };

            testOrderItem.Surcharges = new List<OrderItemSurcharge> { surcharge1, surcharge2 };
            OrderItemData createdOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrderItem);
            Assert.IsNotNull(createdOrderItem.Surcharges);
            surcharge1.ID = createdOrderItem.Surcharges.FirstOrDefault(x => x.Name == surcharge1.Name).ID;
            surcharge2.ID = createdOrderItem.Surcharges.FirstOrDefault(x => x.Name == surcharge2.Name).ID;

            #endregion Create Order and OrderItem

            #region Update

            testOrderItem.Surcharges.Remove(surcharge1);
            OrderItemData updateResponse = null;
            updateResponse = await EndpointTests.AssertPutStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(updateResponse);
            Assert.IsNotNull(updateResponse.Surcharges);
            Assert.AreEqual(1, updateResponse.Surcharges.Count);

            var surcharge3 = new OrderItemSurcharge()
            {
                BID = createdOrder.BID,
                Name = "3",
                TaxCode = ctx.TaxabilityCodes.FirstOrDefault(),
                IncomeAccountID = ctx.GLAccount.FirstOrDefault().ID,
                OrderItemID = createdOrderItem.ID,
                SurchargeDefID = ctx.SurchargeDef.FirstOrDefault().ID
            };
            testOrderItem.Surcharges.Add(surcharge3);

            updateResponse = await EndpointTests.AssertPutStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(updateResponse);
            Assert.IsNotNull(updateResponse.Surcharges);
            Assert.AreEqual(2, updateResponse.Surcharges.Count);
            #endregion Update

            #region Test Includes
            var getResponse = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(getResponse);
            Assert.IsNull(getResponse.Surcharges);
            getResponse = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}/?Surcharges=Full", HttpStatusCode.OK);
            Assert.IsNotNull(getResponse);
            Assert.IsNotNull(getResponse.Surcharges);

            #endregion

            #region Clone

            //EntityActionChangeResponse cloneResponse = null;
            //cloneResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdOrderItem.ID}/clone", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            //Assert.IsNotNull(cloneResponse);

            #endregion Clone
            #region Delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.NoContent);

            //await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{cloneResponse.Id}", HttpStatusCode.NoContent);

            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.NoContent);
            #region DELETE

            ctx.SurchargeDef.Remove(surcharge);
            try
            {
                await ctx.SaveChangesAsync();
            }
            catch (Exception ) {}

            #endregion
            #endregion Delete
        }


        [TestMethod]
        public async Task TestOrderItemOrderNotesCRUDOperations()
        {
            #region CREATE Order and OrderItem

            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrder);

            var testOrderItem = Utils.GetTestOrderItemData(createdOrder);
            OrderItemData createdOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            #endregion

            OrderNote orderNote = new OrderNote()
            {
                ModifiedDT = DateTime.Now,
                NoteType = OrderNoteType.Sales,
                Note = "This is a note",
                OrderID = createdOrder.ID,
                OrderItemID = createdOrderItem.ID,
            };
            string itemUrlPrefix = $"{apiUrl}/{createdOrderItem.ID}";
            string createNoteEndpoint = $"{itemUrlPrefix}/action/notes/{orderNote.NoteType}";

            //Create Note
            var createdNote = await EndpointTests.AssertPostStatusCode<OrderNote>(this.client, createNoteEndpoint, JsonConvert.SerializeObject(orderNote), HttpStatusCode.OK);
            Assert.IsNotNull(createdNote);

            //Get Note by ID
            var savedNote = await EndpointTests.AssertGetStatusCode<OrderNote>(this.client, $"{itemUrlPrefix}/notes/{createdNote.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(savedNote);
            Assert.AreEqual(savedNote.ID, createdNote.ID);
            Assert.AreEqual(savedNote.Note, createdNote.Note);

            //Update Note
            string updatedNoteText = "Test Note Updated!";
            createdNote.Note = updatedNoteText;
            var updatedNote = await EndpointTests.AssertPutStatusCode<OrderNote>(this.client, $"{itemUrlPrefix}/action/notes/{createdNote.ID}", JsonConvert.SerializeObject(createdNote), HttpStatusCode.OK);
            Assert.AreEqual(updatedNoteText, updatedNote.Note);

            // Get all notes
            OrderNote[] orderNotes = await EndpointTests.AssertGetStatusCode<OrderNote[]>(this.client, $"{itemUrlPrefix}/notes?orderNoteType={orderNote.NoteType}", HttpStatusCode.OK);
            updatedNote = orderNotes.FirstOrDefault(n => n.ID == createdNote.ID && n.Note == updatedNoteText);
            Assert.IsNotNull(updatedNote);

            // Set OrderNote IsActive=false
            await EndpointTests.AssertPostStatusCode<OrderNote>(this.client, $"{itemUrlPrefix}/action/notes/{createdNote.ID}/setinactive", JsonConvert.SerializeObject(orderNote), HttpStatusCode.OK);
            savedNote = await EndpointTests.AssertGetStatusCode<OrderNote>(this.client, $"{itemUrlPrefix}/notes/{createdNote.ID}", HttpStatusCode.OK);
            Assert.IsFalse(savedNote.IsActive);

            // Set OrderNote IsActive=true
            await EndpointTests.AssertPostStatusCode<OrderNote>(this.client, $"{itemUrlPrefix}/action/notes/{createdNote.ID}/setactive", JsonConvert.SerializeObject(orderNote), HttpStatusCode.OK);
            savedNote = await EndpointTests.AssertGetStatusCode<OrderNote>(this.client, $"{itemUrlPrefix}/notes/{createdNote.ID}", HttpStatusCode.OK);
            Assert.IsTrue(savedNote.IsActive);

            //Delete Order Note
            EntityActionChangeResponse entityActionChangeResponse = await EndpointTests.AssertDeleteStatusCode<EntityActionChangeResponse>(this.client, $"{itemUrlPrefix}/action/notes/{createdNote.ID}", HttpStatusCode.OK);

            #region Cleanup

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/Api/Order/{createdOrder.ID}", HttpStatusCode.NoContent);

            #endregion
        }



        [TestMethod]
        public async Task TestOrderItemTaxAssessmentCRUD()
        {
            #region Create Order and OrderItem

            var ctx = GetApiContext();
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrder);

            int glAccountID = ctx.GLAccount.First().ID;
            short taxItemID = ctx.TaxItem.First().ID;
            var testOrderItem = Utils.GetTestOrderItemData(createdOrder);

            var taxAssessmentResult1 = new Models.TaxAssessmentResult()
            {
                Items = new List<Models.TaxAssessmentItemResult>()
                {
                    new Models.TaxAssessmentItemResult()
                    {
                        InvoiceText = "Test",
                        GLAccountID = glAccountID,
                        TaxAmount = 100,
                        TaxItemID = taxItemID,
                        TaxRate = 1,
                    }
                }
            };

            var taxAssessmentResult2 = new Models.TaxAssessmentResult()
            {
                Items = new List<Models.TaxAssessmentItemResult>()
                {
                    new Models.TaxAssessmentItemResult()
                    {
                        InvoiceText = "Test2.1",
                        GLAccountID = glAccountID,
                        TaxAmount = 100,
                        TaxItemID = taxItemID,
                        TaxRate = 1,
                    },

                    new Models.TaxAssessmentItemResult()
                    {
                        InvoiceText = "Test2.2",
                        GLAccountID = glAccountID,
                        TaxAmount = 100,
                        TaxItemID = taxItemID,
                        TaxRate = 1,
                    }
                }
            };

            var component1 = new OrderItemComponent()
            {
                BID = createdOrder.BID,
                OrderID = createdOrder.ID,
                Name = "1",
                TaxInfoList = new List<Models.TaxAssessmentResult>()
                {
                    taxAssessmentResult1,    
                    taxAssessmentResult2
                }
            };

            var component2 = new OrderItemComponent()
            {
                BID = createdOrder.BID,
                OrderID = createdOrder.ID,
                Name = "2",
                TaxInfoList = new List<Models.TaxAssessmentResult>()
                {
                    taxAssessmentResult1,
                }
            };

            // setup for surcharge
            var surcharge = new SurchargeDef()
            {
                BID = TestConstants.BID,
                ID = -99,
                Name = "Test.SurchargeDef",
                IsActive = false,
                ModifiedDT = DateTime.UtcNow,
                ClassTypeID = ClassType.SurchargeDef.ID(),
                TaxCode = ctx.TaxabilityCodes.FirstOrDefault(x => x.BID == TestConstants.BID),
                IncomeAccountID = ctx.GLAccount.FirstOrDefault(x => x.BID == TestConstants.BID).ID
            };

            if (!ctx.SurchargeDef.Where(s => s.ID == -99 && s.BID == TestConstants.BID).Any())
            {
                await ctx.SurchargeDef.AddAsync(surcharge);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
            }
            else
            {
                surcharge = ctx.SurchargeDef.Where(s => s.ID == -99).First();
            }

            // setup for order item surcharge
            var orderSurcharge1 = new OrderItemSurcharge()
            {
                BID = TestConstants.BID,
                Name ="Test_OrderItemSurcharge-1-" + GetType().Name,
                TaxCode = ctx.TaxabilityCodes.FirstOrDefault(x => x.BID == TestConstants.BID),
                IncomeAccountID = ctx.GLAccount.FirstOrDefault(x => x.BID == TestConstants.BID).ID,
                SurchargeDefID = surcharge.ID,
                TaxInfoList = new List<Models.TaxAssessmentResult>()
                {
                    taxAssessmentResult2,
                    taxAssessmentResult1,
                }
            };

            testOrderItem.Components = new List<OrderItemComponent> {component1, component2};
            testOrderItem.Surcharges = new List<OrderItemSurcharge> { orderSurcharge1 };
            OrderItemData createdOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrderItem);
            Assert.IsNotNull(createdOrderItem.Components);
            component1.ID = createdOrderItem.Components.FirstOrDefault(x => x.Name == component1.Name).ID;
            component2.ID = createdOrderItem.Components.FirstOrDefault(x => x.Name == component2.Name).ID;
            Assert.IsNotNull(createdOrderItem.Surcharges);
            orderSurcharge1.ID = createdOrderItem.Surcharges.FirstOrDefault(x => x.Name == orderSurcharge1.Name).ID;

            #endregion Create Order and OrderItem

            #region Get
            var retrievedResponse = await EndpointTests.AssertGetStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}?Components=Full&Surcharges=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedResponse);
            Assert.IsNotNull(retrievedResponse.Components);
            Assert.IsNotNull(retrievedResponse.Surcharges);
            Assert.AreEqual(2, retrievedResponse.Components.Count);
            Assert.AreEqual(1, retrievedResponse.Components.First().TaxInfoList.Count);
            Assert.AreEqual(1, retrievedResponse.Surcharges.Count);
            Assert.AreEqual(1, retrievedResponse.Surcharges.First().TaxInfoList.Count);

            #endregion

            #region Update

            testOrderItem.Components.First().TaxInfoList.Remove(taxAssessmentResult1);
            testOrderItem.Surcharges.First().TaxInfoList.Remove(taxAssessmentResult2);
            OrderItemData updateResponse = null;
            updateResponse = await EndpointTests.AssertPutStatusCode<OrderItemData>(client, $"{apiUrl}/{createdOrderItem.ID}", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);
            Assert.IsNotNull(updateResponse);
            Assert.IsNotNull(updateResponse.Components);
            Assert.IsNotNull(updateResponse.Surcharges);
            Assert.AreEqual(2, updateResponse.Components.Count);
            Assert.AreEqual(1, updateResponse.Components.First().TaxInfoList.Count);
            Assert.AreEqual(1, updateResponse.Surcharges.Count);
            Assert.AreEqual(1, updateResponse.Surcharges.First().TaxInfoList.Count);
            #endregion Update

            #region Clone

            //EntityActionChangeResponse cloneResponse = null;
            //cloneResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{createdOrderItem.ID}/clone", null, HttpStatusCode.OK);
            //Assert.IsNotNull(cloneResponse);

            #endregion Clone

            #region Delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItem.ID}", HttpStatusCode.NoContent);

            //await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{cloneResponse.Id}", HttpStatusCode.NoContent);
            //await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{cloneResponse.Id}", HttpStatusCode.NotFound);

            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion Delete
        }
    }
}
