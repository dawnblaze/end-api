﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    [TestClass]
    public class OrderItemSubStatusTests : CommonControllerTestClass
    {
        public const string apiUrl = "/API/OrderItemSubStatus";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        protected string testSubStatusName1, testSubStatusName2, testSubStatusName3;

        [TestInitialize]
        public override void Init()
        {
            testSubStatusName1 = "test." + DateTime.UtcNow.Ticks.ToString();
            testSubStatusName2 = "test." + DateTime.UtcNow.Ticks.ToString();
            testSubStatusName3 = "test." + DateTime.UtcNow.Ticks.ToString();
            base.Init();
        }

        [TestMethod]
        public async Task TestCRUD()
        {
            // Create
            var created = new OrderItemSubStatus()
            {
                Name = this.testSubStatusName1,
                Description = this.testSubStatusName1
            };
            var saved = await EndpointTests.AssertPostStatusCode<OrderItemSubStatus>(client, $"{apiUrl}", JsonConvert.SerializeObject(created), HttpStatusCode.OK);
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleOrderItemSubStatus[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);

            // Retrieve
            var read = await EndpointTests.AssertGetStatusCode<OrderItemSubStatus>(client, $"{apiUrl}/{saved.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(read);

            var readAll = await EndpointTests.AssertGetStatusCode<List<OrderItemSubStatus>>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(readAll);

            // Update
            saved.Description = "saved";

            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{saved.ID}", JsonConvert.SerializeObject(saved), HttpStatusCode.OK);
            read = await EndpointTests.AssertGetStatusCode<OrderItemSubStatus>(client, $"{apiUrl}/{saved.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(read);
            Assert.AreEqual(saved.Description, read.Description);

            // Link and Unlink
            string apiItemStatusUrl = "/api/orderitemstatus";

            long ticks = DateTime.Now.Ticks;
            byte[] bytes = BitConverter.GetBytes(ticks);
            string id = Convert.ToBase64String(bytes)
                                    .Replace('+', '_')
                                    .Replace('/', '-')
                                    .TrimEnd('=');
            
            var testOrderItemStatus = new OrderItemStatus()
            {
                BID = 1,
                Name = "UNIT TEST - " + id,
                IsActive = true,
                IsSystem = false,
                IsDefault = true,
                TransactionType = (byte)OrderTransactionType.Order,
                StatusIndex = 50,
                OrderStatusID = OrderOrderStatus.OrderWIP
            };
            var savedOrderItemStatus = await EndpointTests.AssertPostStatusCode<OrderItemStatus>(client, $"{apiItemStatusUrl}/", JsonConvert.SerializeObject(testOrderItemStatus), HttpStatusCode.OK);

            await EndpointTests.AssertPostStatusCode<OrderItemSubStatus>(client, $"{apiUrl}/{saved.ID}/action/linkstatus/{savedOrderItemStatus.ID}", null, HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode<OrderItemSubStatus>(client, $"{apiUrl}/{saved.ID}/action/unlinkstatus/{savedOrderItemStatus.ID}", null, HttpStatusCode.OK);

            await EndpointTests.AssertPostStatusCode<OrderItemSubStatus>(client, $"{apiItemStatusUrl}/{savedOrderItemStatus.ID}/action/linksubstatus/{saved.ID}", null, HttpStatusCode.OK);

            // Filters ------
            var readAllFiltered = await EndpointTests.AssertGetStatusCode<List<OrderItemSubStatus>>(client, $"{apiUrl}/?OrderItemStatuses=Simple", HttpStatusCode.OK);
            var readFiltered = await EndpointTests.AssertGetStatusCode<List<OrderItemSubStatus>>(client, $"{apiUrl}/?OrderItemStatusID={savedOrderItemStatus.ID}", HttpStatusCode.OK);
            var simpleListFiltered = await EndpointTests.AssertGetStatusCode<SimpleOrderItemSubStatus[]>(client, $"{apiUrl}/SimpleList?OrderItemStatusID={savedOrderItemStatus.ID}", HttpStatusCode.OK);
            // --------------

            await EndpointTests.AssertPostStatusCode<OrderItemSubStatus>(client, $"{apiItemStatusUrl}/{savedOrderItemStatus.ID}/action/unlinksubstatus/{saved.ID}", null, HttpStatusCode.OK);

            // Delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiItemStatusUrl}/{savedOrderItemStatus.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiItemStatusUrl}/{savedOrderItemStatus.ID}", HttpStatusCode.NotFound);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{saved.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{saved.ID}", HttpStatusCode.NotFound);
        }

        [TestCleanup]
        public void Teardown()
        {
            try
            {
                Task.Run(async () =>
                {
                    var readAll = await EndpointTests.AssertGetStatusCode<List<OrderItemSubStatus>>(client, $"{apiUrl}", HttpStatusCode.OK);

                    var created = readAll.FirstOrDefault(x => x.Name == testSubStatusName1);
                    if (created != null)
                    {
                        await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{created.ID}", HttpStatusCode.NoContent);
                    }
                    created = readAll.FirstOrDefault(x => x.Name == testSubStatusName2);
                    if (created != null)
                    {
                        await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{created.ID}", HttpStatusCode.NoContent);
                    }
                    created = readAll.FirstOrDefault(x => x.Name == testSubStatusName3);
                    if (created != null)
                    {
                        await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{created.ID}", HttpStatusCode.NoContent);
                    }
                }).Wait();
            }
            catch { }
        }

    }
}
