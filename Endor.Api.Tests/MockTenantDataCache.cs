﻿using Endor.Tenant;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    public class MockTenantDataCache : ITenantDataCache
    {
        private readonly string _sql;
        private readonly string _blob;

        public MockTenantDataCache(string sql, string blobStorage)
        {
            this._sql = sql;
            this._blob = blobStorage;
        }

        public Task<TenantData> Get(short bid)
        {
            return Task.FromResult(new TenantData()
            {
                BusinessDBConnectionString = _sql,
                StorageConnectionString = _blob,
                LoggingURL = "http://fakeurl.com",
                LoggingDBConnectionString = "FakeConnectionString",
                //PaymentURL = "http://endorpayment.localcyriousdevelopment.com:5011/" //For local testing...
                PaymentURL = "http://localhost:12898/"
            });
        }

        public void InvalidateCache()
        {
            
        }

        public void InvalidateCache(short bid)
        {
            
        }
    }
}
