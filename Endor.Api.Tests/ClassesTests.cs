﻿using Endor.Api.Web.Classes;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Api.Tests
{
    [TestClass]
    public class ClassesTests
    {
        [TestMethod]
        public void LayoutComponentConstructsCorrectly()
        {
            var layoutComponent = new LayoutComponent();
            Assert.AreEqual("LayoutComponent", layoutComponent.Name);//            The Name is "LayoutComponent"
            Assert.AreEqual(1, layoutComponent.ID);//The ID = 1(the enum value)
            TestVariableOnAssembly(layoutComponent, "Bleed", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "BleedBottom", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "BleedLeft", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "BleedRight", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "BleedTop", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "ColorBarLength", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "ColorBarPosition", DataType.String, AssemblyElementType.SingleLineText);
            TestVariableOnAssembly(layoutComponent, "ColorBarThickness", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "ColorsBack", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "ColorsFront", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "Gutter", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "GutterHorizontal", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "GutterVertical", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "ImageBackUniquenessRule", DataType.String, AssemblyElementType.SingleLineText);
            TestVariableOnAssembly(layoutComponent, "ImageItemUniquenessRule", DataType.String, AssemblyElementType.SingleLineText);
            TestVariableOnAssembly(layoutComponent, "ImageOrderUniquenessRule", DataType.String, AssemblyElementType.SingleLineText);
            TestVariableOnAssembly(layoutComponent, "ImpositionType", DataType.String, AssemblyElementType.SingleLineText);
            TestVariableOnAssembly(layoutComponent, "IncludeColorBar", DataType.Boolean, AssemblyElementType.Checkbox);
            TestVariableOnAssembly(layoutComponent, "IncludeCuts", DataType.Boolean, AssemblyElementType.Checkbox);
            TestVariableOnAssembly(layoutComponent, "IncludeVisualization", DataType.Boolean, AssemblyElementType.Checkbox);
            TestVariableOnAssembly(layoutComponent, "Is2Sided", DataType.Boolean, AssemblyElementType.Checkbox);
            TestVariableOnAssembly(layoutComponent, "ItemHeight", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "ItemQuantity", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "ItemWidth", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "LayoutType", DataType.String, AssemblyElementType.SingleLineText);
            TestVariableOnAssembly(layoutComponent, "LeaderBottom", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "LeaderLeft", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "LeaderRight", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "LeaderTop", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "MaterialHeight", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "MaterialMargin", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "MaterialMarginBottom", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "MaterialMarginLeft", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "MaterialMarginRight", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "MaterialMarginTop", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "MaterialRunningWastePercentage", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "MaterialSetupWasteCount", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "MaterialWidth", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "MaxMaterialsPerCut", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "OrderedQuantity", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "RotateItem", DataType.Boolean, AssemblyElementType.Checkbox);
            TestVariableOnAssembly(layoutComponent, "RotateMaterial", DataType.Boolean, AssemblyElementType.Checkbox);
            TestVariableOnAssembly(layoutComponent, "ShowColorBar", DataType.Boolean, AssemblyElementType.Checkbox);
            TestVariableOnAssembly(layoutComponent, "ShowCutLines", DataType.Boolean, AssemblyElementType.Checkbox);
            TestVariableOnAssembly(layoutComponent, "ShowFoldLines", DataType.Boolean, AssemblyElementType.Checkbox);
            TestVariableOnAssembly(layoutComponent, "ShowRegistrationMarks", DataType.Boolean, AssemblyElementType.Checkbox);
            TestVariableOnAssembly(layoutComponent, "TotalMaterialWasteCount", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "Machine", DataType.String, AssemblyElementType.SingleLineText);
            TestVariableOnAssembly(layoutComponent, "Material", DataType.String, AssemblyElementType.SingleLineText);
            TestVariableOnAssembly(layoutComponent, "MachineMaxHorizontal", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "MachineMaxVertical", DataType.Number, AssemblyElementType.Number);
            TestVariableOnAssembly(layoutComponent, "ShowBlanks", DataType.Boolean, AssemblyElementType.Checkbox);

            Assert.AreEqual(52, layoutComponent.Variables.Count);
        }

        private void TestVariableOnAssembly(AssemblyData assemblyData, string name, DataType dataType, AssemblyElementType elementType)
        {
            var variable = assemblyData.Variables.FirstOrDefault(t => t.Name == name);
            Assert.IsNotNull(variable);
            Assert.AreEqual(dataType, variable.DataType);
            Assert.AreEqual(elementType, variable.ElementType);
        }
    }
}
