﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Pricing;
using Microsoft.EntityFrameworkCore;
using Endor.DocumentStorage.Models;

namespace Endor.Api.Tests
{
    [TestClass]
    public class DestinationEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/destination";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }
        private string NewTestDestinationName;
        private int testTemplateID = -99;
        private short testTemplateTableID = -99;
        private byte testLocationID;
        private short testBID = 1;

        #region Initialize and Cleanup

        [TestInitialize]
        [TestCleanup]
        public async Task TestInitialize()
        {
            NewTestDestinationName = $"TEST DESTINATION {DateTime.UtcNow}";
            using (var ctx = GetApiContext(testBID))
            {
                var destinationProfileTables = await ctx.DestinationProfileTable
                    .Where(x => x.BID == testBID && x.TableID == testTemplateTableID).ToListAsync();
                if (destinationProfileTables != null && destinationProfileTables.Count > 0)
                {
                    ctx.RemoveRange(destinationProfileTables);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }

                var destinationProfiles = await ctx.DestinationProfile
                    .Where(x => x.BID == testBID && x.TemplateID == testTemplateID).ToListAsync();
                if (destinationProfiles != null && destinationProfiles.Count > 0)
                {
                    ctx.RemoveRange(destinationProfiles);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }

                var destinations = await ctx.DestinationData
                    .Where(x => x.BID == testBID && x.TemplateID == testTemplateID).ToListAsync();
                if (destinations != null && destinations.Count > 0)
                {
                    ctx.RemoveRange(destinations);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }

                var assemblyProfileTables = await ctx.AssemblyTable
                    .Where(x => x.BID == testBID && x.ID == testTemplateTableID).ToListAsync();
                if (assemblyProfileTables != null && assemblyProfileTables.Count > 0)
                {
                    ctx.RemoveRange(assemblyProfileTables);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }

                var assemblies = await ctx.AssemblyData
                    .Where(x => x.BID == testBID && x.ID == testTemplateID).ToListAsync();
                if (assemblies != null && assemblies.Count > 0)
                {
                    ctx.RemoveRange(assemblies);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }

                var location = await ctx.LocationData.FirstOrDefaultAsync(x => x.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("Database contains no Location data.");

                testLocationID = location.ID;
            }
        }

        private async Task<AssemblyData> GetTestDestinationTemplate()
        {
            AssemblyData template;
            using (var ctx = GetApiContext(testBID))
            {
                var incomeAccount = await ctx.GLAccount.FirstOrDefaultAsync(x => x.BID == testBID && x.IsIncome == true);
                if (incomeAccount == null)
                    Assert.Inconclusive("Database contains no Income GLAccount data.");

                template = new AssemblyData()
                {
                    BID = testBID,
                    ID = testTemplateID,
                    Name = "TEST DESTINATION TEMPLATE",
                    IsActive = true,
                    IncomeAccountID = incomeAccount.ID,
                    AssemblyType = AssemblyType.Destination,
                };

                var templateTable = new AssemblyTable()
                {
                    BID = testBID,
                    ID = testTemplateTableID,
                    AssemblyID = testTemplateID,
                    Label = "TEST TABLE",
                    VariableName = "TESTTABLE",
                    CellDataJSON = "{}"
                };

                ctx.AssemblyData.Add(template);
                ctx.AssemblyTable.Add(templateTable);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                template.Tables = new List<AssemblyTable>() { templateTable };
            }
            return template;
        }

        private async Task<DestinationData> GetNewModel()
        {
            var template = await GetTestDestinationTemplate();

            DestinationData destination;
            using (var ctx = GetApiContext(testBID))
            {
                var incomeAccount = await ctx.GLAccount.FirstOrDefaultAsync(x => x.BID == testBID && x.IsIncome == true);
                if (incomeAccount == null)
                    Assert.Inconclusive("Database contains no Income GLAccount data.");
                
                var expenseAccount = await ctx.GLAccount.FirstOrDefaultAsync(x => x.BID == testBID && x.IsExpense == true);
                if (expenseAccount == null)
                    Assert.Inconclusive("Database contains no Income GLAccount data.");

                var taxCode = await ctx.TaxabilityCodes.FirstOrDefaultAsync(x => x.BID == testBID);
                if (taxCode == null)
                    Assert.Inconclusive("Database contains no TaxCode data.");

                destination = new DestinationData()
                {
                    Name = NewTestDestinationName,
                    IncomeAccountID = incomeAccount.ID,
                    ExpenseAccountID = expenseAccount.ID,
                    TaxCodeID = taxCode.ID,
                    IsActive = true,
                    BID = testBID,
                    TemplateID = template.ID,
                    ActiveProfileCount = 0,
                    Description = "Description",
                    HasImage = false,
                    SKU = "SKU",
                    Profiles = new List<DestinationProfile>
                    {
                        GetTestDestinationProfile()
                    }
                };
            }
            return destination;            
        }

        private DestinationProfile GetTestDestinationProfile()
        {
            return new DestinationProfile
            {
                IsActive = true,
                IsDefault = true,
                Name = this.NewTestDestinationName + " - Profile",
                TemplateID = this.testTemplateID,
                AssemblyOVDataJSON = "{}",
                BID = testBID,
                DestinationProfileTables = new List<DestinationProfileTable>
                {
                    GetTestDestinationProfileTable()
                }
            };
        }

        private DestinationProfileTable GetTestDestinationProfileTable()
        {
            return new DestinationProfileTable
            {
                BID = testBID,
                TableID = testTemplateTableID,
                Label = "TEST DESTINATION TABLE 1",
                OverrideDefault = false
            };
        }

        #endregion

        [TestMethod]
        public async Task TestDestinationCRUD()
        {
            var itemTest = await GetNewModel();

            // test post
            var destinationData = await EndpointTests.AssertPostStatusCode<DestinationData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(destinationData);
            Assert.IsNotNull(destinationData.Profiles);
            Assert.IsNotNull(destinationData.Profiles.FirstOrDefault().DestinationProfileTables);

            // test get
            var getDestination = await EndpointTests.AssertGetStatusCode<DestinationData>(client, $"{apiUrl}/{destinationData.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(getDestination);
            var list = await EndpointTests.AssertGetStatusCode<List<DestinationData>>(client, apiUrl, HttpStatusCode.OK);
            Assert.IsNotNull(list);
            Assert.IsTrue(list.Count > 0);
            var simpleList = await EndpointTests.AssertGetStatusCode<List<DestinationData>>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            Assert.IsNotNull(simpleList);
            Assert.IsTrue(simpleList.Count > 0);

            // test put
            destinationData.Name += "Updated";
            destinationData = await EndpointTests.AssertPutStatusCode<DestinationData>(client, $"{ apiUrl}/{destinationData.ID}", JsonConvert.SerializeObject(destinationData), HttpStatusCode.OK);
            Assert.IsNotNull(destinationData);

            // test delete
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/-12345/Action/CanDelete", HttpStatusCode.NotFound);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{destinationData.ID}/Action/CanDelete", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{destinationData.ID}", HttpStatusCode.NoContent);

            // confirm delete
            await EndpointTests.AssertGetStatusCode<DestinationData>(client, $"{ apiUrl}/{destinationData.ID}", HttpStatusCode.NotFound);
            using (var ctx = GetApiContext(testBID))
            {
                var tables = await ctx.DestinationProfileTable.Where(x => x.BID == testBID && x.TableID == testTemplateTableID).ToListAsync();
                Assert.AreEqual(0, tables.Count());
                var profiles = await ctx.DestinationProfile.Where(x => x.BID == testBID && x.TemplateID == testTemplateID).ToListAsync();
                Assert.AreEqual(0, profiles.Count());
            }
        }

        [TestMethod]
        public async Task TestDestinationCustomField()
        {
            var itemTest = await GetNewModel();

            // create
            var destinationData = await EndpointTests.AssertPostStatusCode<DestinationData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(destinationData);

            // custom field definition
            var customFieldDefinition = new CustomFieldDefinition()
            {
                ClassTypeID = ClassType.CustomFieldDefinition.ID(),
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                AppliesToClassTypeID = (int)ClassType.Destination,
                AppliesToID = destinationData.ID,
                Name = destinationData.Name,
                Label = "Test Custom Field Label",
                Description = "Test Custom Field Desc",
                DataType = DataType.String,
                ElementType = AssemblyElementType.SingleLineText,
                DisplayFormatString = "",
                AllowMultipleValues = false,
                HasValidators = false
            };

            var createdCustomFieldDefinition = await EndpointTests.AssertPostStatusCode<CustomFieldDefinition>(client, "/api/cf/definition", JsonConvert.SerializeObject(customFieldDefinition), HttpStatusCode.OK);

            var values = new CustomFieldValue[1];
            values[0] = new CustomFieldValue()
            {
                ID = createdCustomFieldDefinition.ID,
                V = "TestDestination"
            };

            await EndpointTests.AssertPutStatusCode(client, $"/api/cf/value/{ClassType.Destination.ID()}/{destinationData.ID}", JsonConvert.SerializeObject(values), HttpStatusCode.OK);
            var retrievedCFDefValue = await EndpointTests.AssertGetStatusCode<CustomFieldValue[]>(client, $"/api/cf/value/{ClassType.Destination.ID()}/{destinationData.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedCFDefValue);
            Assert.AreEqual(values[0].ID, retrievedCFDefValue[0].ID);
            Assert.AreEqual(values[0].V, retrievedCFDefValue[0].V);

            // delete
            using (var ctx = GetApiContext())
            {
                ctx.CustomFieldDefinition.RemoveRange(ctx.CustomFieldDefinition.Where(t => t.BID == testBID
                    && t.AppliesToID == destinationData.ID && (t.AppliesToClassTypeID == (int)ClassType.Destination)));
                await ctx.SaveChangesAsync();
            }
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{destinationData.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode<DestinationData>(client, $"{ apiUrl}/{destinationData.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode<CustomFieldValue[]>(client, $"/api/cf/value/{ClassType.Destination.ID()}/{destinationData.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task TestDestinationDocuments()
        {
            var itemTest = await GetNewModel();

            Bucket bucket = Bucket.Documents;
            int ctid = ClassType.Destination.ID();

            // create
            var destinationData = await EndpointTests.AssertPostStatusCode<DestinationData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(destinationData);

            await EndpointTests.AssertPostStatusCode(client, $"api/dm/{bucket}/{ctid}/{destinationData.ID}/foo", "hello world", HttpStatusCode.OK);
            var item = await EndpointTests.AssertGetStatusCode<object[]>(client, $"api/dm/{bucket}/{ctid}/{destinationData.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(item);
            Assert.AreEqual(1, item.Count());

            // delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{destinationData.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode<DestinationData>(client, $"{ apiUrl}/{destinationData.ID}", HttpStatusCode.NotFound);
            var deletedItem = await EndpointTests.AssertGetStatusCode<object[]>(client, $"api/dm/{bucket}/{ctid}/{destinationData.ID}", HttpStatusCode.OK);
            Assert.AreEqual(0, deletedItem.Count());
        }

        [TestMethod]
        public async Task TestDestinationClone()
        {
            var itemTest = await GetNewModel();

            // create
            var destinationData = await EndpointTests.AssertPostStatusCode<DestinationData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(destinationData);

            var cloned = await EndpointTests.AssertPostStatusCode<DestinationData>(client, $"{apiUrl}/{destinationData.ID}/Clone", null, HttpStatusCode.OK);
            Assert.IsNotNull(cloned);

            Assert.AreEqual($"{destinationData.Name} (Clone)", cloned.Name);
            Assert.AreEqual(destinationData.Profiles.Count, cloned.Profiles.Count);
            Assert.AreEqual(destinationData.TaxabilityCode, cloned.TaxabilityCode);
            Assert.AreEqual(destinationData.ExpenseAccount, cloned.ExpenseAccount);

            // delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{cloned.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode<DestinationData>(client, $"{ apiUrl}/{cloned.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{destinationData.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode<DestinationData>(client, $"{ apiUrl}/{destinationData.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task TestDestinationCloneCustomField()
        {
            var itemTest = await GetNewModel();

            // create
            var destinationData = await EndpointTests.AssertPostStatusCode<DestinationData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(destinationData);

            // custom field definition
            var customFieldDefinition = new CustomFieldDefinition()
            {
                ClassTypeID = ClassType.CustomFieldDefinition.ID(),
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                AppliesToClassTypeID = (int)ClassType.Destination,
                AppliesToID = destinationData.ID,
                Name = destinationData.Name,
                Label = "Test Custom Field Label",
                Description = "Test Custom Field Desc",
                DataType = DataType.String,
                ElementType = AssemblyElementType.SingleLineText,
                DisplayFormatString = "",
                AllowMultipleValues = false,
                HasValidators = false
            };

            var createdCustomFieldDefinition = await EndpointTests.AssertPostStatusCode<CustomFieldDefinition>(client, "/api/cf/definition", JsonConvert.SerializeObject(customFieldDefinition), HttpStatusCode.OK);

            var values = new CustomFieldValue[1];
            values[0] = new CustomFieldValue()
            {
                ID = createdCustomFieldDefinition.ID,
                V = "TestDestination"
            };

            await EndpointTests.AssertPutStatusCode(client, $"/api/cf/value/{ClassType.Destination.ID()}/{destinationData.ID}", JsonConvert.SerializeObject(values), HttpStatusCode.OK);
            var retrievedCFDefValue = await EndpointTests.AssertGetStatusCode<CustomFieldValue[]>(client, $"/api/cf/value/{ClassType.Destination.ID()}/{destinationData.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedCFDefValue);
            Assert.AreEqual(values[0].ID, retrievedCFDefValue[0].ID);
            Assert.AreEqual(values[0].V, retrievedCFDefValue[0].V);

            var cloned = await EndpointTests.AssertPostStatusCode<DestinationData>(client, $"{apiUrl}/{destinationData.ID}/Clone", null, HttpStatusCode.OK);
            Assert.IsNotNull(cloned);

            Assert.AreEqual($"{destinationData.Name} (Clone)", cloned.Name);
            Assert.AreEqual(destinationData.Profiles.Count, cloned.Profiles.Count);
            Assert.AreEqual(destinationData.TaxabilityCode, cloned.TaxabilityCode);
            Assert.AreEqual(destinationData.ExpenseAccount, cloned.ExpenseAccount);

            retrievedCFDefValue = await EndpointTests.AssertGetStatusCode<CustomFieldValue[]>(client, $"/api/cf/value/{ClassType.Destination.ID()}/{cloned.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedCFDefValue);
            Assert.AreEqual(values[0].ID, retrievedCFDefValue[0].ID);
            Assert.AreEqual(values[0].V, retrievedCFDefValue[0].V);

            // delete
            using (var ctx = GetApiContext())
            {
                ctx.CustomFieldDefinition.RemoveRange(ctx.CustomFieldDefinition.Where(t => t.BID == testBID
                    && t.AppliesToID == destinationData.ID && (t.AppliesToClassTypeID == (int)ClassType.Destination)));
                ctx.CustomFieldDefinition.RemoveRange(ctx.CustomFieldDefinition.Where(t => t.BID == testBID
                    && t.AppliesToID == cloned.ID && (t.AppliesToClassTypeID == (int)ClassType.Destination)));
                await ctx.SaveChangesAsync();
            }
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{cloned.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode<DestinationData>(client, $"{ apiUrl}/{cloned.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode<CustomFieldValue[]>(client, $"/api/cf/value/{ClassType.Destination.ID()}/{cloned.ID}", HttpStatusCode.NotFound);

            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{destinationData.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode<DestinationData>(client, $"{ apiUrl}/{destinationData.ID}", HttpStatusCode.NotFound);
            await EndpointTests.AssertGetStatusCode<CustomFieldValue[]>(client, $"/api/cf/value/{ClassType.Destination.ID()}/{destinationData.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        public async Task TestDestinationCloneDocuments()
        {
            var itemTest = await GetNewModel();

            Bucket bucket = Bucket.Documents;
            int ctid = ClassType.Destination.ID();

            // create
            var destinationData = await EndpointTests.AssertPostStatusCode<DestinationData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(destinationData);

            await EndpointTests.AssertPostStatusCode(client, $"api/dm/{bucket}/{ctid}/{destinationData.ID}/foo", "hello world", HttpStatusCode.OK);
            var item = await EndpointTests.AssertGetStatusCode<object[]>(client, $"api/dm/{bucket}/{ctid}/{destinationData.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(item);
            Assert.AreEqual(1, item.Count());

            // clone
            var cloned = await EndpointTests.AssertPostStatusCode<DestinationData>(client, $"{apiUrl}/{destinationData.ID}/Clone", null, HttpStatusCode.OK);
            Assert.IsNotNull(cloned);

            Assert.AreEqual($"{destinationData.Name} (Clone)", cloned.Name);
            Assert.AreEqual(destinationData.Profiles.Count, cloned.Profiles.Count);
            Assert.AreEqual(destinationData.TaxabilityCode, cloned.TaxabilityCode);
            Assert.AreEqual(destinationData.ExpenseAccount, cloned.ExpenseAccount);

            var clonedItem = await EndpointTests.AssertGetStatusCode<object[]>(client, $"api/dm/{bucket}/{ctid}/{cloned.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(clonedItem);
            Assert.AreEqual(1, clonedItem.Count());

            // delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{cloned.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode<DestinationData>(client, $"{ apiUrl}/{cloned.ID}", HttpStatusCode.NotFound);
            var deletedClonedItem = await EndpointTests.AssertGetStatusCode<object[]>(client, $"api/dm/{bucket}/{ctid}/{cloned.ID}", HttpStatusCode.OK);
            Assert.AreEqual(0, deletedClonedItem.Count());
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{destinationData.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode<DestinationData>(client, $"{ apiUrl}/{destinationData.ID}", HttpStatusCode.NotFound);
            var deletedItem = await EndpointTests.AssertGetStatusCode<object[]>(client, $"api/dm/{bucket}/{ctid}/{destinationData.ID}", HttpStatusCode.OK);
            Assert.AreEqual(0, deletedItem.Count());
        }

        [TestMethod]
        public async Task TestDestinationActionEndpoints()
        {
            var itemTest = await GetNewModel();
            var destinationData = await EndpointTests.AssertPostStatusCode<DestinationData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(destinationData);
            Assert.IsTrue(destinationData.IsActive);

            var activeChangeResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{destinationData.ID}/Action/SetInactive", null, HttpStatusCode.OK);
            Assert.IsNotNull(activeChangeResp);
            Assert.IsTrue(activeChangeResp.Success);
            var getDestination = await EndpointTests.AssertGetStatusCode<DestinationData>(client, $"{apiUrl}/{destinationData.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(getDestination);
            Assert.IsFalse(getDestination.IsActive);

            activeChangeResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{destinationData.ID}/Action/SetActive", null, HttpStatusCode.OK);
            Assert.IsNotNull(activeChangeResp);
            Assert.IsTrue(activeChangeResp.Success);
            getDestination = await EndpointTests.AssertGetStatusCode<DestinationData>(client, $"{apiUrl}/{destinationData.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(getDestination);
            Assert.IsTrue(getDestination.IsActive);

            var canDeleteResp = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{destinationData.ID}/Action/CanDelete", HttpStatusCode.OK);
            Assert.IsNotNull(canDeleteResp);
            Assert.IsTrue(canDeleteResp.Success);
            Assert.IsTrue(canDeleteResp.Value.Value);

            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{destinationData.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode<DestinationData>(client, $"{ apiUrl}/{destinationData.ID}", HttpStatusCode.NotFound);
        }

        [TestMethod]
        [TestCategory("compute")]
        public async Task TestDestinationCompute()
        {
            //test invalid request
            object invalidRequest = JsonConvert.DeserializeObject(@"{ 'EngineType': 'Free' }"); // force invalid request by setting OrderpriceRequest.Result property to invalid type
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(invalidRequest), HttpStatusCode.BadRequest);

            //test valid request
            DestinationPriceRequest testRequest = new DestinationPriceRequest()
            {
                EngineType = DestinationEngineType.Pickedup,
            };

            OrderPriceResult result = await EndpointTests.AssertPostStatusCode<OrderPriceResult>(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);

            Assert.IsNotNull(result);
            Assert.AreEqual(0m, result.PricePreTax);
        }

    }
}
