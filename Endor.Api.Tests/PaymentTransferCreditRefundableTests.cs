﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Request;
using PaymentApplication = Endor.Models.PaymentApplication;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Classes.Responses;

namespace Endor.Api.Tests
{
    [TestClass]
    [TestCategory("Payment")]
    public class PaymentTransferCreditRefundableTests : PaymentTestHelper
    {
        private const string transferCreditApiUrl = "/api/payment/transfertocredit";
        private System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        private PaymentTransferCreditRequest GetTransferCreditRequest(int companyID, int? masterID, int orderID, bool isRefundable, decimal amount)
        {
            return new PaymentTransferCreditRequest()
            {
                PaymentTransactionType = isRefundable ? PaymentTransactionType.Refund_to_Refundable_Credit : PaymentTransactionType.Refund_to_Nonrefundable_Credit,
                MasterPaymentID = masterID,
                CompanyID = companyID,
                PaymentMethodId = isRefundable ? (int)PaymentMethodType.RefundableCredit : (int)PaymentMethodType.NonRefundableCredit,
                Applications = new List<PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest()
                    {
                        OrderID = orderID,
                        Amount = -amount,
                    },
                    new PaymentApplicationRequest()
                    {
                        OrderID = null,
                        Amount = amount,
                    },
                }
            };
        }

        [TestInitialize]
        public async Task Initialize()
        {
            await CleanUpTestData();
        }

        [TestCleanup]
        public async Task Teardown()
        {
            await CleanUpTestData();
        }

        [TestMethod]
        public async Task TransferCreditShouldUpdatePaymentApplications()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID,
                price: 100m
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                companyID: company.ID,
                amount: 60m,
                isRefundable: true
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                orderID: order.ID,
                amount: 60,
                isRefundable: true
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            var applications = ctx.PaymentApplication.Where(t => t.MasterID == master.ID).ToList();
            
            var paymentApplication = applications.First(t => t.OrderID == order.ID);
            var refundApplication = applications.Last(t => t.PaymentTransactionType == (byte)PaymentTransactionType.Refund_to_Refundable_Credit);
            var refundCreditApplication = applications.Last(t => t.PaymentTransactionType == (byte)PaymentTransactionType.Refund_to_Refundable_Credit && t.OrderID==null);
            Assert.AreEqual(-60, refundApplication.Amount);
            Assert.AreEqual(0, refundApplication.RefBalance);
            Assert.AreEqual(60, refundCreditApplication.RefBalance);
            Assert.AreEqual(0, paymentApplication.RefBalance);
            Assert.AreEqual(true, paymentApplication.HasAdjustments);
        }



        [TestMethod]
        public async Task TransferCreditShouldUpdateCompanysBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID,
                price: 100m
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                companyID: company.ID,
                amount: 60m,
                isRefundable: true
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                orderID: order.ID,
                amount: 60,
                isRefundable: true
            );
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            Assert.AreEqual(0, company.RefundableCredit);
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            ctx.Entry<CompanyData>(company).Reload();
            Assert.AreEqual(60, company.RefundableCredit);
        }


        [TestMethod]
        public async Task TransferCreditShouldUpdateMastersBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID,
                price: 100m
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                companyID: company.ID,
                amount: 60m,
                isRefundable: true
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                orderID: order.ID,
                amount: 60,
                isRefundable: true
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            Assert.AreEqual(60, master.RefBalance); //no change in master balance
            Assert.AreEqual(60, master.ChangeInRefCredit);
        }




        [TestMethod]
        public async Task TransferCreditOneOrderOneMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 2.05m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount,
                isRefundable: true
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            Assert.AreEqual(amount - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            Assert.AreEqual(amount, master.RefBalance); // master should have same amount
        }

        [TestMethod]
        public async Task TransferCreditOneOrderTwoMasters()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 15m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount/2,
                isRefundable: true,
                companyID: company.ID
            );
            var master2 = await CreatePaymentForOrder(
                ID: -98,
                orderID: order.ID,
                amount: amount/2,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount,
                isRefundable: true
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            master2 = ctx.PaymentMaster.First(t => t.ID == master2.ID);

            Assert.AreEqual(amount - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            Assert.AreEqual(10m, master.RefBalance);
            Assert.AreEqual(amount/2, master2.RefBalance);
        }


        [TestMethod]
        public async Task TransferCreditTwoOrdersOneMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 2.05m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: 10,
                amountPaid: 10
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: 10,
                amountPaid: 10
            );
            var master = await CreatePaymentForMultipleOrders(
                ID: -99,
                orderIDs: new int[]{ order.ID, order2.ID },
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount * 2,
                isRefundable: true
            );
            request.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = -refund_amount,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order2.ID,
                    Amount = -refund_amount,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = refund_amount * 2,
                },
            };
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            order2 = ctx.OrderData.First(t => t.ID == order2.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            Assert.AreEqual(10 - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            Assert.AreEqual(10 - refund_amount, order2.PaymentPaid);
            Assert.AreEqual(refund_amount, order2.PaymentBalanceDue);
            Assert.AreEqual(amount, master.RefBalance); // master should have same amount
        }

        [TestMethod]
        public async Task TransferCreditTwoOrdersTwoMasters()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var refund_amount = 2.05m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: 10,
                amountPaid: 10
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: 10,
                amountPaid: 10
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: 10,
                isRefundable: true,
                companyID: company.ID
            );
            var master2 = await CreatePaymentForOrder(
                ID: -98,
                orderID: order2.ID,
                amount: 10,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount * 2,
                isRefundable: true
            );
            request.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = -refund_amount,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order2.ID,
                    Amount = -refund_amount,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = refund_amount * 2,
                },
            };
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            order2 = ctx.OrderData.First(t => t.ID == order2.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            master2 = ctx.PaymentMaster.First(t => t.ID == master2.ID);
            Assert.AreEqual(10 - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            Assert.AreEqual(10 - refund_amount, order2.PaymentPaid);
            Assert.AreEqual(refund_amount, order2.PaymentBalanceDue);
            Assert.AreEqual(10, master.RefBalance); // master should have same amount
            Assert.AreEqual(10, master2.RefBalance); // master should have same amount
        }


        [TestMethod]
        public async Task TransferCreditTwiceOnOneMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 2.05m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForMultipleOrders(
                ID: -99,
                orderIDs: new int[] { order.ID, order2.ID },
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount,
                isRefundable: true
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            Assert.AreEqual(amount - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            Assert.AreEqual(amount, master.RefBalance); // master should have same amount
            // refund again
            request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order2.ID,
                amount: refund_amount,
                isRefundable: true
            );
            result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            order2 = ctx.OrderData.First(t => t.ID == order2.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            Assert.AreEqual(amount - refund_amount, order2.PaymentPaid);
            Assert.AreEqual(refund_amount, order2.PaymentBalanceDue);
            Assert.AreEqual(amount, master.RefBalance); // master should have same amount
        }


        [TestMethod]
        public async Task TransferCreditPullsTooMuchFromAnOrderShouldFail()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 20.05m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount,
                isRefundable: true
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);

            order = ctx.OrderData.First(t => t.ID == order.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);

            Assert.AreEqual(amount, order.PaymentPaid);
            Assert.AreEqual(0, order.PaymentBalanceDue);
            Assert.AreEqual(amount, master.RefBalance);
        }


        [TestMethod]
        public async Task TransferCreditPullsTooMuchFromOneOfTheOrdersShouldFail()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 20m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            var master2 = await CreatePaymentForOrder(
                ID: -98,
                orderID: order.ID,
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount,
                isRefundable: true
            );
            request.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = -refund_amount,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order2.ID,
                    Amount = -100m,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = refund_amount + 100m,
                },
            };
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);

            order = ctx.OrderData.First(t => t.ID == order.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);

            Assert.AreEqual(amount, order.PaymentPaid);
            Assert.AreEqual(0, order.PaymentBalanceDue);
            Assert.AreEqual(amount, master.RefBalance);
        }


        [TestMethod]
        public async Task TransferCreditFromRefundableAndNonRefundableMasters()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 2.05m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            var master2 = await CreatePaymentForOrder(
                ID: -98,
                orderID: order2.ID,
                amount: amount,
                isRefundable: false,
                companyID: company.ID
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount,
                isRefundable: true
            );
            request.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = -refund_amount,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order2.ID,
                    Amount = -refund_amount,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = refund_amount * 2
                },
            };
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);

            order = ctx.OrderData.First(t => t.ID == order.ID);
            order2 = ctx.OrderData.First(t => t.ID == order2.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            master2 = ctx.PaymentMaster.First(t => t.ID == master2.ID);

            Assert.AreEqual(amount - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            Assert.AreEqual(amount - refund_amount, order2.PaymentPaid);
            Assert.AreEqual(refund_amount, order2.PaymentBalanceDue);
            Assert.AreEqual(amount, master.RefBalance);
            Assert.AreEqual(amount, master2.NonRefBalance);
        }
    }
}
