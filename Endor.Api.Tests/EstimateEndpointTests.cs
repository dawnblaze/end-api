﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Clones;
using Endor.Pricing;
using Endor.Api.Web.Controllers;
using AssertX = Xunit.Assert;
using Microsoft.AspNetCore.Mvc;
using Endor.DocumentStorage.Models;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Tests
{
    [TestClass]
    public class EstimateEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/estimate";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        [TestMethod]
        public async Task TestEstimateCRUDOperations()
        {
            var ctx = GetApiContext();
            CompanyData company = new CompanyData()
            {
                ID = -101,
                Name = "Test Company",
                IsActive = true,
                BID = TestConstants.BID,
                StatusID = (byte)CompanyStatus.Lead,
                LocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID,
            };
            var existingCompanies = ctx.CompanyData.Where(c => c.ID == company.ID && c.BID == company.BID);
            if (existingCompanies.Count() > 0) company = existingCompanies.First();
            else
            {
                ctx.CompanyData.Add(company);
                ctx.SaveChanges();
            }
            
            #region CREATE
            var testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.BadRequest);

            testOrder.CompanyID = company.ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            #endregion

            #region RETRIEVE

            // Test Get By ID
            var retrievedOrder = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder);

            var getAll = await EndpointTests.AssertGetStatusCode<EstimateData[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(getAll);
            Assert.IsTrue(getAll.Count() > 0);
            Assert.IsTrue(getAll.All(t => t.TransactionType == (byte)OrderTransactionType.Estimate));

            ctx.Entry<CompanyData>(company).Reload();
            Assert.IsTrue(company.StatusText.Contains("Prospect"));
            #endregion

            #region UPDATE

            var holdFormattedNumber = createdOrder.FormattedNumber;
            createdOrder.FormattedNumber = ""; //invalid length string;
            var updatedOrder = await EndpointTests.AssertPutStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}", JsonConvert.SerializeObject(createdOrder), HttpStatusCode.OK);
            Assert.IsTrue(holdFormattedNumber == updatedOrder.FormattedNumber);
            createdOrder.FormattedNumber = holdFormattedNumber;
            createdOrder.Description = "TEST DESCRIPTION";
            var testPut = await EndpointTests.AssertPutStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}", JsonConvert.SerializeObject(createdOrder), HttpStatusCode.OK);
            Assert.IsNotNull(testPut);
            Assert.AreEqual("TEST DESCRIPTION", testPut.Description);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            try
            {
                ctx.CompanyData.Remove(company);
                ctx.SaveChanges();
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }

            #endregion
        }

        [TestMethod]
        public async Task TestEstimatePutError()
        {
            #region CREATE
            var testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.LocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = testOrder.LocationID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.LocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            #endregion


            #region UPDATE
            createdOrder.FormattedNumber = "INV-123";
            var testPut = await EndpointTests.AssertPutStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}",JsonConvert.SerializeObject(createdOrder), HttpStatusCode.OK);
            Assert.IsNotNull(testPut);

            createdOrder.TransactionType = (byte)OrderTransactionType.Order;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdOrder.ID}", JsonConvert.SerializeObject(createdOrder), HttpStatusCode.BadRequest);
            
            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);

            #endregion
        }

        [TestMethod]
        public async Task TestEstimateKeyDateCreation()
        {
            var testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;
            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdEstimate1 = await assertChangingOrderStatus(testOrder, OrderOrderStatus.EstimateLost, HttpStatusCode.OK);
            var retrievedEstimate1 = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"{apiUrl}/{createdEstimate1.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedEstimate1?.Dates);
            Assert.IsTrue(IsFound(retrievedEstimate1.Dates, OrderKeyDateType.Created));
            Assert.IsTrue(IsFound(retrievedEstimate1.Dates, OrderKeyDateType.Lost));

            var createdEstimate2 = await assertChangingOrderStatus(testOrder, OrderOrderStatus.EstimateVoided, HttpStatusCode.OK);
            var retrievedEstimate2 = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"{apiUrl}/{createdEstimate2.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedEstimate2?.Dates);
            Assert.IsTrue(IsFound(retrievedEstimate2.Dates, OrderKeyDateType.Created));
            Assert.IsTrue(IsFound(retrievedEstimate2.Dates, OrderKeyDateType.Voided));


            var createdEstimate3 = await assertChangingOrderStatus(testOrder, OrderOrderStatus.EstimateApproved, HttpStatusCode.OK);
            var retrievedEstimate3 = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"{apiUrl}/{createdEstimate3.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedEstimate3?.Dates);
            Assert.IsTrue(IsFound(retrievedEstimate3.Dates, OrderKeyDateType.Created));
            Assert.IsTrue(IsFound(retrievedEstimate3.Dates, OrderKeyDateType.Converted));

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEstimate1.ID}/action/date/Created/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEstimate1.ID}/action/date/Lost/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEstimate2.ID}/action/date/Created/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEstimate2.ID}/action/date/Voided/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEstimate3.ID}/action/date/Created/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEstimate3.ID}/action/date/Converted/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEstimate1.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEstimate2.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEstimate3.ID}", HttpStatusCode.NoContent);

            #endregion
        }

        private bool IsFound(ICollection<OrderKeyDate> OrderKeyDates, OrderKeyDateType okdt)
        {
            var foundKeyDates = OrderKeyDates.Where(t => t.KeyDateType == okdt);
            if (foundKeyDates.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [TestMethod]
        public async Task TestEstimateChangeCreatedKeyDateToCurrentDate()
        {
            var testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/CREATED/", HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.Dates);
            var createdDate = retrievedOrder.Dates.Where(t => t.KeyDateType == OrderKeyDateType.Created);
            Assert.IsNotNull(createdDate);
            Assert.AreEqual(1, createdDate.Count());

            var dateDiff = DateTime.Now - createdDate.First().KeyDT;
            Assert.IsTrue(dateDiff.TotalMinutes/60/60 < 1);

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/CREATED/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            
            #endregion
        }

        [TestMethod]
        public async Task TestEstimateChangeCreatedKeyDateToSpecifiedDate()
        {
            var testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            DateTime myDate = DateTime.Now.AddDays(2).Date;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/created/{myDate.ToString("yyyy-MM-dd")}", HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.Dates);
            var createdDate = retrievedOrder.Dates.Where(t => t.KeyDateType == OrderKeyDateType.Created);
            Assert.IsNotNull(createdDate);
            Assert.AreEqual(1, createdDate.Count());

            Assert.AreEqual(myDate, createdDate.First().KeyDT);

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/CREATED/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);

            #endregion
        }

        [TestMethod]
        public async Task TestEstimateChangeCreatedKeyDateToSpecifiedDateTime()
        {
            var testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            DateTime myDate = DateTime.Now.AddDays(2).Date.Add(new TimeSpan(2, 13, 0));
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/created/{myDate.ToString("yyyy-MM-ddTHH:mm:ss")}", HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.Dates);
            var createdDate = retrievedOrder.Dates.Where(t => t.KeyDateType == OrderKeyDateType.Created);
            Assert.IsNotNull(createdDate);
            Assert.AreEqual(1, createdDate.Count());

            Assert.AreEqual(myDate, createdDate.First().KeyDT);

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/CREATED/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);

            #endregion
        }

        [TestMethod]
        public async Task TestEstimateSetPrimaryContact()
        {
            var testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", testOrder, HttpStatusCode.OK);

            int contactId = await Utils.GetContactID(client, testOrder.CompanyID);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkcontact/primary/{contactId}", HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}?ContactLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.ContactRoles);
            var primaryRows = retrievedOrder.ContactRoles.Where(t => t.RoleType == OrderContactRoleType.Primary);
            Assert.IsNotNull(primaryRows);
            Assert.AreEqual(1, primaryRows.Count());
            Assert.AreEqual(OrderContactRoleType.Primary, primaryRows.ToList()[0].RoleType);
            Assert.AreEqual(contactId, primaryRows.ToList()[0].ContactID);

            #region DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        [TestMethod]
        public async Task TestEstimateDeletePrimaryContact()
        {
            var testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", testOrder, HttpStatusCode.OK);

            int contactId = await Utils.GetContactID(client, testOrder.CompanyID);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkcontact/primary/{contactId}", HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}?ContactLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.ContactRoles);
            var primaryRows = retrievedOrder.ContactRoles.Where(t => t.RoleType == OrderContactRoleType.Primary);
            Assert.IsNotNull(primaryRows);
            Assert.AreEqual(1, primaryRows.Count());
            Assert.AreEqual(OrderContactRoleType.Primary, primaryRows.ToList()[0].RoleType);
            Assert.AreEqual(contactId, primaryRows.ToList()[0].ContactID);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkcontact/primary/222", HttpStatusCode.BadRequest);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkcontact/primary/{contactId}", HttpStatusCode.OK);

            retrievedOrder = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}?ContactLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.ContactRoles);
            primaryRows = retrievedOrder.ContactRoles.Where(t => t.RoleType == OrderContactRoleType.Primary);
            Assert.IsNotNull(primaryRows);
            Assert.AreEqual(0, primaryRows.Count());

            #region DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        [TestMethod]
        public async Task TestEstimateSetEnteredByEmployee()
        {
            var testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", testOrder, HttpStatusCode.OK);

            int employeeId = await Utils.GetEmployeeID(client);
            var ctx = GetApiContext();
            var employeeRoleId = ctx.EmployeeRole.Where(er => er.Name == "Entered By").FirstOrDefault().ID;
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkemployee/{employeeRoleId}/{employeeId}", HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}?EmployeeLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.EmployeeRoles);
            var enteredByRows = retrievedOrder.EmployeeRoles.Where(t => t.RoleID == 254);
            Assert.IsNotNull(enteredByRows);
            Assert.AreEqual(1, enteredByRows.Count());
            Assert.AreEqual(employeeId, enteredByRows.ToList()[0].EmployeeID);

            #region DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        [TestMethod]
        public async Task TestEstimateDeleteEnteredByEmployee()
        {
            var testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", testOrder, HttpStatusCode.OK);

            int employeeId = await Utils.GetEmployeeID(client);
            var ctx = GetApiContext();
            var employeeRoleId = ctx.EmployeeRole.Where(er => er.Name == "Entered By").FirstOrDefault().ID;
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkEmployee/{employeeRoleId}/{employeeId}", HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}?EmployeeLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.EmployeeRoles);
            var enteredByRows = retrievedOrder.EmployeeRoles.Where(t => t.RoleID == employeeRoleId);
            Assert.IsNotNull(enteredByRows);
            Assert.AreEqual(1, enteredByRows.Count());
            Assert.AreEqual(employeeId, enteredByRows.ToList()[0].EmployeeID);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkEmployee/{employeeRoleId}/222", HttpStatusCode.BadRequest);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkEmployee/{employeeRoleId}/{employeeId}", HttpStatusCode.OK);

            retrievedOrder = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}?EmployeeLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.EmployeeRoles);
            enteredByRows = retrievedOrder.EmployeeRoles.Where(t => t.RoleID == 254);
            Assert.IsNotNull(enteredByRows);
            Assert.AreEqual(0, enteredByRows.Count());

            #region DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        [TestMethod]
        public async Task TestEstimateNotesCRUDOperations()
        {
            OrderData testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            OrderData createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            OrderNote orderNote = new OrderNote()
            {
                BID = TestConstants.BID,
                ModifiedDT = DateTime.Now,
                NoteType = OrderNoteType.Sales,
                Note = "Test Note",
                IsActive = true,
            };

            string createUpdateOrDeleteNoteEndpoint = $"{apiUrl}/{createdOrder.ID}/action/notes/{orderNote.NoteType}";

            //Create Note
            var createdNote = await EndpointTests.AssertPostStatusCode<OrderNote>(this.client, createUpdateOrDeleteNoteEndpoint, JsonConvert.SerializeObject(orderNote), HttpStatusCode.OK);
            Assert.IsNotNull(createdNote);

            //Get Note by ID
            var savedNote = await EndpointTests.AssertGetStatusCode<OrderNote>(this.client, $"{apiUrl}/{createdOrder.ID}/notes/{createdNote.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(savedNote);
            Assert.AreEqual(savedNote.ID, createdNote.ID);
            Assert.AreEqual(savedNote.Note, createdNote.Note);

            //Update Note
            string updatedNoteText = "Test Note Updated!";
            createdNote.Note = updatedNoteText;
            var updatedNote = await EndpointTests.AssertPutStatusCode<OrderNote>(this.client, $"{apiUrl}/{createdOrder.ID}/action/notes/{createdNote.ID}", JsonConvert.SerializeObject(createdNote), HttpStatusCode.OK);
            Assert.AreEqual(updatedNoteText, updatedNote.Note);

            // Get all notes
            OrderNote[] orderNotes = await EndpointTests.AssertGetStatusCode<OrderNote[]>(this.client, $"{apiUrl}/{createdOrder.ID}/notes?orderNoteType={orderNote.NoteType}", HttpStatusCode.OK);
            updatedNote = orderNotes.FirstOrDefault(n => n.ID == createdNote.ID && n.Note == updatedNoteText);
            Assert.IsNotNull(updatedNote);

            // Set OrderNote IsActive=false
            await EndpointTests.AssertPostStatusCode<OrderNote>(this.client, $"{apiUrl}/{createdOrder.ID}/action/notes/{createdNote.ID}/setinactive", JsonConvert.SerializeObject(orderNote), HttpStatusCode.OK);
            savedNote = await EndpointTests.AssertGetStatusCode<OrderNote>(this.client, $"{apiUrl}/{createdOrder.ID}/notes/{createdNote.ID}", HttpStatusCode.OK);
            Assert.IsFalse(savedNote.IsActive);

            // Set OrderNote IsActive=true
            await EndpointTests.AssertPostStatusCode<OrderNote>(this.client, $"{apiUrl}/{createdOrder.ID}/action/notes/{createdNote.ID}/setactive", JsonConvert.SerializeObject(orderNote), HttpStatusCode.OK);
            savedNote = await EndpointTests.AssertGetStatusCode<OrderNote>(this.client, $"{apiUrl}/{createdOrder.ID}/notes/{createdNote.ID}", HttpStatusCode.OK);
            Assert.IsTrue(savedNote.IsActive);

            //Delete Order Note
            EntityActionChangeResponse entityActionChangeResponse = await EndpointTests.AssertDeleteStatusCode<EntityActionChangeResponse>(this.client, $"{apiUrl}/{createdOrder.ID}/action/notes/{createdNote.ID}", HttpStatusCode.OK);
            // Assert.IsTrue(entityActionChangeResponse.Success);
            // Assert.IsFalse(entityActionChangeResponse.HasError);

            #region DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        [TestMethod]
        public async Task EstimateOrderLinkTest()
        {
            OrderData testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            var clonedOrder = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            string linkOrderEndpoint = $"{apiUrl}/{createdOrder.ID}/action/linkOrder/{OrderOrderLinkType.ClonedTo}/{clonedOrder.ID}";
            string unlinkOrderEndpoint = $"{apiUrl}/{createdOrder.ID}/action/unlinkOrder/{OrderOrderLinkType.ClonedTo}/{clonedOrder.ID}";
            string updatelinkOrderEndpoint = $"{apiUrl}/{createdOrder.ID}/action/updateorderlink/{OrderOrderLinkType.ClonedTo}/{clonedOrder.ID}";

            OrderOrderLink createdOrderCreatedLink = Utils.GetOrderOrderLink(createdOrder, clonedOrder);
            //Create Link
            var createdLink = await EndpointTests.AssertPostStatusCode<OrderOrderLink>(this.client, linkOrderEndpoint, JsonConvert.SerializeObject(createdOrderCreatedLink), HttpStatusCode.OK);

            OrderOrderLink[] links = await EndpointTests.AssertGetStatusCode<OrderOrderLink[]>(this.client, $"{apiUrl}/{createdOrder.ID}/links", HttpStatusCode.OK);
            OrderOrderLink[] linkedLinks = await EndpointTests.AssertGetStatusCode<OrderOrderLink[]>(this.client, $"{apiUrl}/{clonedOrder.ID}/links", HttpStatusCode.OK);

            Assert.IsNotNull(links.FirstOrDefault(t=>t.BID == createdOrderCreatedLink.BID && createdOrderCreatedLink.OrderID == t.OrderID  && createdOrderCreatedLink.LinkedOrderID == t.LinkedOrderID));
            Assert.IsNotNull(linkedLinks.FirstOrDefault(t => t.BID == createdOrderCreatedLink.BID && createdOrderCreatedLink.LinkedOrderID == t.OrderID && createdOrderCreatedLink.OrderID == t.LinkedOrderID));
            //Update Link
            string updatedOrderLinkDescription = "Order to Order New Description";

            createdLink.Description = updatedOrderLinkDescription;

            var updatedLink = await EndpointTests.AssertPostStatusCode<OrderOrderLink>(this.client, updatelinkOrderEndpoint, JsonConvert.SerializeObject(createdLink), HttpStatusCode.OK);
            Assert.AreEqual(updatedOrderLinkDescription, updatedLink.Description);
            
            //Delete Order Note
            await EndpointTests.AssertPostStatusCode(this.client, unlinkOrderEndpoint, HttpStatusCode.OK);

            await EndpointTests.AssertGetResponseContent(client, $"{apiUrl}/{createdOrder.ID}/links", "");
        }

        [TestMethod]
        public async Task EstimateToOrderOrderLinkTest()
        {
            OrderData testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            testOrder.TransactionType = (byte)OrderTransactionType.Order;
            testOrder.OrderStatusID = OrderOrderStatus.OrderPreWIP;
            testOrder.ClassTypeID = (int)ClassType.Order;
            var clonedOrder = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            string linkOrderEndpoint = $"{apiUrl}/{createdOrder.ID}/action/linkOrder/{OrderOrderLinkType.ClonedTo}/{clonedOrder.ID}";
            string unlinkOrderEndpoint = $"{apiUrl}/{createdOrder.ID}/action/unlinkOrder/{OrderOrderLinkType.ClonedTo}/{clonedOrder.ID}";
            string updatelinkOrderEndpoint = $"{apiUrl}/{createdOrder.ID}/action/updateorderlink/{OrderOrderLinkType.ClonedTo}/{clonedOrder.ID}";

            OrderOrderLink createdOrderCreatedLink = Utils.GetOrderOrderLink(createdOrder, clonedOrder);
            //Create Link
            var createdLink = await EndpointTests.AssertPostStatusCode<OrderOrderLink>(this.client, linkOrderEndpoint, JsonConvert.SerializeObject(createdOrderCreatedLink), HttpStatusCode.OK);

            OrderOrderLink[] links = await EndpointTests.AssertGetStatusCode<OrderOrderLink[]>(this.client, $"{apiUrl}/{createdOrder.ID}/links", HttpStatusCode.OK);
            OrderOrderLink[] linkedLinks = await EndpointTests.AssertGetStatusCode<OrderOrderLink[]>(this.client, $"{apiUrl}/{clonedOrder.ID}/links", HttpStatusCode.OK);

            Assert.IsNotNull(links.FirstOrDefault(t => t.BID == createdOrderCreatedLink.BID && createdOrderCreatedLink.OrderID == t.OrderID && createdOrderCreatedLink.LinkedOrderID == t.LinkedOrderID));
            Assert.IsNotNull(linkedLinks.FirstOrDefault(t => t.BID == createdOrderCreatedLink.BID && createdOrderCreatedLink.LinkedOrderID == t.OrderID && createdOrderCreatedLink.OrderID == t.LinkedOrderID));
            //Update Link
            string updatedOrderLinkDescription = "Order to Order New Description";

            createdLink.Description = updatedOrderLinkDescription;

            var updatedLink = await EndpointTests.AssertPostStatusCode<OrderOrderLink>(this.client, updatelinkOrderEndpoint, JsonConvert.SerializeObject(createdLink), HttpStatusCode.OK);
            Assert.AreEqual(updatedOrderLinkDescription, updatedLink.Description);

            //Delete Order Note
            await EndpointTests.AssertPostStatusCode(this.client, unlinkOrderEndpoint, HttpStatusCode.OK);

            await EndpointTests.AssertGetResponseContent(client, $"{apiUrl}/{createdOrder.ID}/links", "");
        }

        [TestMethod]
        public async Task ConvertEstimateToOrderTest()
        {
            var ctx = GetApiContext();
            CompanyData company = new CompanyData()
            {
                ID = -102,
                Name = "Test Company",
                IsActive = true,
                BID = TestConstants.BID,
                StatusID = (byte)CompanyStatus.Lead,
                LocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID,
            };
            var existingCompanies = ctx.CompanyData.Where(c => c.ID == company.ID && c.BID == company.BID);
            if (existingCompanies.Count() > 0) company = existingCompanies.First();
            else
            {
                ctx.CompanyData.Add(company);
                ctx.SaveChanges();
            }
            var testController = GetDMController();

            OrderData testEstimate = Utils.GetTestOrderData();
            testEstimate.TransactionType = (byte)OrderTransactionType.Estimate;
            testEstimate.OrderStatusID = OrderOrderStatus.EstimatePending;
            testEstimate.ClassTypeID = (int)ClassType.Estimate;

            testEstimate.CompanyID = company.ID;
            testEstimate.PickupLocationID = await Utils.GetLocationID(client);
            testEstimate.TaxGroupID = await Utils.GetTaxGroupID(client);
            testEstimate.ProductionLocationID = testEstimate.PickupLocationID;

            int contactId = await Utils.GetContactID(client, testEstimate.CompanyID);
            int employeeId = await Utils.GetEmployeeID(client);
            var employeeRoleEnteredById = ctx.EmployeeRole.Where(er => er.Name == "Entered By").FirstOrDefault().ID;
            var employeeRoleSalesPersonId = ctx.EmployeeRole.Where(er => er.Name == "Salesperson").FirstOrDefault().ID;

            var itemStatusPending = ctx.OrderItemStatus.Where(i =>
                        i.BID == TestConstants.BID &&
                        i.TransactionType == (byte)OrderTransactionType.Estimate &&
                        i.OrderStatusID == testEstimate.OrderStatusID &&
                        i.IsDefault == true
                    ).FirstOrDefault().ID;

            var itemStatusLost = ctx.OrderItemStatus.Where(i =>
                       i.BID == TestConstants.BID &&
                       i.TransactionType == (byte)OrderTransactionType.Estimate &&
                       i.OrderStatusID == OrderOrderStatus.EstimateLost &&
                       i.IsDefault == true
                   ).FirstOrDefault().ID;

            var itemStatusApproved = ctx.OrderItemStatus.Where(i =>
                       i.BID == TestConstants.BID &&
                       i.TransactionType == (byte)OrderTransactionType.Estimate &&
                       i.OrderStatusID == OrderOrderStatus.EstimateApproved &&
                       i.IsDefault == true
                   ).FirstOrDefault().ID;

            testEstimate.Items = new List<OrderItemData>();
            // Add Line Item Lost Status
            testEstimate.Items.Add(new OrderItemData()
            {
                BID = testEstimate.BID,
                HasCustomImage = false,
                HasDocuments = false,
                HasProof = false,
                ID = 1,
                OrderID = 0,
                ItemNumber = 1,
                Quantity = 1,
                Name = "test",
                IsOutsourced = false,
                OrderStatusID = testEstimate.OrderStatusID,
                ItemStatusID = itemStatusLost,
                ProductionLocationID = testEstimate.ProductionLocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = testEstimate.TaxGroupID,
                TaxGroupOV = false,
                IsTaxExempt = false,
                TransactionType = testEstimate.TransactionType
            });

            // Add Line Item Pending Status
            testEstimate.Items.Add(new OrderItemData()
            {
                BID = testEstimate.BID,
                HasCustomImage = false,
                HasDocuments = false,
                HasProof = false,
                ID = 2,
                OrderID = 0,
                ItemNumber = 1,
                Quantity = 1,
                Name = "test",
                IsOutsourced = false,
                OrderStatusID = testEstimate.OrderStatusID,
                ItemStatusID = itemStatusPending,
                ProductionLocationID = testEstimate.ProductionLocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = testEstimate.TaxGroupID,
                TaxGroupOV = false,
                IsTaxExempt = false,
                TransactionType = testEstimate.TransactionType
            });

            var contactLinksForCompany = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = company.BID,
                        ContactID = contactId,
                        CompanyID = company.ID,
                        Roles = (ContactRole.Primary | ContactRole.Billing)
                    }
                };

            var existingCompanyContactLink = ctx.CompanyContactLink.Where(cl => cl.CompanyID == company.ID && cl.BID == company.BID && cl.ContactID == contactId);
            if (existingCompanyContactLink.Count() > 0) contactLinksForCompany = existingCompanyContactLink.ToList();
            else
            {
                ctx.CompanyContactLink.AddRange(contactLinksForCompany);
                ctx.SaveChanges();
            }

            EstimateData createdEstimate = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testEstimate), HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEstimate.ID}/action/linkcontact/primary/{contactId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEstimate.ID}/action/linkcontact/billing/{contactId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEstimate.ID}/action/linkEmployee/{employeeRoleEnteredById}/{employeeId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEstimate.ID}/action/linkEmployee/{employeeRoleSalesPersonId}/{employeeId}", HttpStatusCode.OK);

            var estimateResult = await testController.Post(new DMController.RequestModel()
            {
                id = createdEstimate.ID,
                ctid = Convert.ToInt32(ClassType.Estimate),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            }, null, "https://corebridge.net/wp-content/uploads/2019/01/AdobeStock_202169599-e1551199093506.jpg");

            AssertX.IsType<OkObjectResult>(estimateResult);
            AssertX.IsType<DMItem>(((OkObjectResult)estimateResult).Value);

            var estimateLineItemResult = await testController.Post(new DMController.RequestModel()
            {
                id = createdEstimate.Items.FirstOrDefault(i => i.OrderStatusID != OrderOrderStatus.EstimateLost && i.OrderStatusID != OrderOrderStatus.EstimateVoided).ID,
                ctid = Convert.ToInt32(ClassType.OrderItem),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            }, null, "https://corebridge.net/wp-content/uploads/2019/01/AdobeStock_202169599-e1551199093506.jpg");

            AssertX.IsType<OkObjectResult>(estimateLineItemResult);
            AssertX.IsType<DMItem>(((OkObjectResult)estimateLineItemResult).Value);

            var convertOption = new ConvertEstimateOption()
            {
                DueDate = DateTime.Now,
                KeepFiles = true,
                KeepNotes = true,
                OrderStatus = OrderOrderStatus.OrderPreWIP,
                UpdatePricing = true
            };

            ConvertEstimateResponse convertedOrder = await EndpointTests.AssertPostStatusCode<ConvertEstimateResponse>(client, $"{apiUrl}/{createdEstimate.ID}/action/convertestimate", JsonConvert.SerializeObject(convertOption), HttpStatusCode.OK);

            Assert.IsNotNull(convertedOrder);
            Assert.IsTrue((convertedOrder.OrderID != 0));
            Assert.IsTrue((convertedOrder.EstimateID == createdEstimate.ID));

            List<OrderItemData> retrievedEstimateLineItems = ctx.OrderItemData.Where(oid => oid.BID == TestConstants.BID
                                                                    && oid.OrderID == createdEstimate.ID).ToList();

            // Check if Line Item Status Lost should not be updated
            Assert.IsTrue((retrievedEstimateLineItems.ElementAt(0).ItemStatusID == itemStatusLost));
            // Check if Line Item Status Pending should be updated to approved status
            Assert.IsTrue((retrievedEstimateLineItems.ElementAt(1).ItemStatusID == itemStatusApproved));

            var orderResult = await testController.GetDocuments(new DMController.RequestModel()
            {
                id = convertedOrder.OrderID,
                ctid = Convert.ToInt32(ClassType.Order),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            });
            AssertX.IsType<OkObjectResult>(orderResult);
            AssertX.IsType<List<DMItem>>(((OkObjectResult)orderResult).Value);
            Assert.IsTrue(((List<DMItem>)((OkObjectResult)orderResult).Value).Count > 0);

            var retrievedOrderWithContact = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"/api/order/{convertedOrder.OrderID}?ContactLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderWithContact?.ContactRoles);
            var contactRows = retrievedOrderWithContact.ContactRoles.Where(t => t.OrderID == convertedOrder.OrderID);
            Assert.IsNotNull(contactRows);
            Assert.AreEqual(2, contactRows.Count());
            Assert.AreEqual(OrderContactRoleType.Primary, contactRows.ToList()[0].RoleType);
            Assert.AreEqual(OrderContactRoleType.Billing, contactRows.ToList()[1].RoleType);
            Assert.AreEqual(contactId, contactRows.ToList()[0].ContactID);
            Assert.AreEqual(contactId, contactRows.ToList()[1].ContactID);

            var retrievedOrderWithEmployee = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"/api/order/{convertedOrder.OrderID}?EmployeeLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderWithEmployee?.EmployeeRoles);
            var employeeRows = retrievedOrderWithEmployee.EmployeeRoles.Where(t => t.OrderID == convertedOrder.OrderID);
            Assert.IsNotNull(employeeRows);
            Assert.AreEqual(2, employeeRows.Count());
            Assert.AreEqual(employeeRoleEnteredById, employeeRows.ToList()[0].RoleID);
            Assert.AreEqual(employeeRoleSalesPersonId, employeeRows.ToList()[1].RoleID);
            Assert.AreEqual(employeeId, employeeRows.ToList()[0].EmployeeID);
            Assert.AreEqual(employeeId, employeeRows.ToList()[1].EmployeeID);

            var retrievedOrderWithOrderItem = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"/api/order/{convertedOrder.OrderID}?ItemLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderWithOrderItem.Items);

            var orderItemResult = await testController.GetDocuments(new DMController.RequestModel()
            {
                id = retrievedOrderWithOrderItem.Items.FirstOrDefault().ID,
                ctid = Convert.ToInt32(ClassType.OrderItem),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            });
            AssertX.IsType<OkObjectResult>(orderItemResult);
            AssertX.IsType<List<DMItem>>(((OkObjectResult)orderItemResult).Value);
            Assert.IsTrue(((List<DMItem>)((OkObjectResult)orderItemResult).Value).Count > 0);

            ctx.Entry<CompanyData>(company).Reload();
            Assert.IsTrue(company.StatusText.Contains("Client"));

            //CLEAN UP...
            var createdEstimateLink = ctx.OrderOrderLink.Where(l => l.BID == TestConstants.BID && l.LinkedOrderID == createdEstimate.ID).FirstOrDefault();
            Assert.IsNotNull(createdEstimateLink);
            var convertedOrderLink = ctx.OrderOrderLink.Where(l => l.BID == TestConstants.BID && l.LinkedOrderID == convertedOrder.OrderID).FirstOrDefault();
            Assert.IsNotNull(convertedOrderLink);
            ctx.OrderOrderLink.Remove(createdEstimateLink);
            ctx.OrderOrderLink.Remove(convertedOrderLink);
            ctx.SaveChanges();

            var deleteEstimateResult = testController.DeleteAll(Convert.ToInt32(ClassType.Estimate), convertedOrder.EstimateID);
            var deleteEstimateLineItemResult = testController.DeleteAll(Convert.ToInt32(ClassType.OrderItem), createdEstimate.Items.FirstOrDefault().ID);
            var deleteOrderResult = testController.DeleteAll(Convert.ToInt32(ClassType.Order), convertedOrder.OrderID);
            var deleteOrderLineItemResult = testController.DeleteAll(Convert.ToInt32(ClassType.OrderItem), retrievedOrderWithOrderItem.Items.FirstOrDefault().ID);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEstimate.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{convertedOrder.OrderID}", HttpStatusCode.NoContent);

            var estimateCompanies = await ctx.EstimateData.Where(o => o.BID == TestConstants.BID && o.CompanyID == -102).ToListAsync();
            if (estimateCompanies.Count != 0)
            {
                foreach (var cleanupEstimate in estimateCompanies)
                {
                    var cleanupEstimateKeyDate = await ctx.OrderKeyDate.Where(x => x.BID == TestConstants.BID && x.OrderID == cleanupEstimate.ID).ToListAsync();
                    ctx.RemoveRange(cleanupEstimateKeyDate);
                    await ctx.SaveChangesAsync();
                    var cleanupEstimateContactRole = await ctx.OrderContactRole.Where(x => x.BID == TestConstants.BID && x.OrderID == cleanupEstimate.ID).ToListAsync();
                    ctx.RemoveRange(cleanupEstimateContactRole);
                    await ctx.SaveChangesAsync();
                    var cleanupEstimateItem = await ctx.OrderItemData.Where(x => x.BID == TestConstants.BID && x.OrderID == cleanupEstimate.ID).ToListAsync();
                    ctx.RemoveRange(cleanupEstimateItem);
                    await ctx.SaveChangesAsync();
                    var cleanupEstimateLink = await ctx.OrderOrderLink.Where(x => x.BID == TestConstants.BID && x.LinkedOrderID == cleanupEstimate.ID).ToListAsync();
                    ctx.RemoveRange(cleanupEstimateLink);
                    await ctx.SaveChangesAsync();
                    var cleanupEstimateEmployeeRole = await ctx.OrderEmployeeRole.Where(x => x.BID == TestConstants.BID && x.OrderID == cleanupEstimate.ID).ToListAsync();
                    ctx.RemoveRange(cleanupEstimateEmployeeRole);
                    await ctx.SaveChangesAsync();
                }
            }
            ctx.RemoveRange(estimateCompanies);
            await ctx.SaveChangesAsync();

            var orderCompanies = await ctx.OrderData.Where(o => o.BID == TestConstants.BID && o.CompanyID == -102).ToListAsync();
            if (orderCompanies.Count != 0)
            {
                foreach (var cleanupOrder in orderCompanies)
                {
                    var cleanupOrderKeyDate = await ctx.OrderKeyDate.Where(x => x.BID == TestConstants.BID && x.OrderID == cleanupOrder.ID).ToListAsync();
                    ctx.RemoveRange(cleanupOrderKeyDate);
                    await ctx.SaveChangesAsync();
                    var cleanupOrderContactRole = await ctx.OrderContactRole.Where(x => x.BID == TestConstants.BID && x.OrderID == cleanupOrder.ID).ToListAsync();
                    ctx.RemoveRange(cleanupOrderContactRole);
                    await ctx.SaveChangesAsync();
                    var cleanupOrderItem = await ctx.OrderItemData.Where(x => x.BID == TestConstants.BID && x.OrderID == cleanupOrder.ID).ToListAsync();
                    ctx.RemoveRange(cleanupOrderItem);
                    await ctx.SaveChangesAsync();
                    var cleanupOrderLink = await ctx.OrderOrderLink.Where(x => x.BID == TestConstants.BID && x.LinkedOrderID == cleanupOrder.ID).ToListAsync();
                    ctx.RemoveRange(cleanupOrderLink);
                    await ctx.SaveChangesAsync();
                    var cleanupOrderEmployeeRole = await ctx.OrderEmployeeRole.Where(x => x.BID == TestConstants.BID && x.OrderID == cleanupOrder.ID).ToListAsync();
                    ctx.RemoveRange(cleanupOrderEmployeeRole);
                    await ctx.SaveChangesAsync();
                }
            }
            ctx.RemoveRange(orderCompanies);
            await ctx.SaveChangesAsync();

            try
            {
                ctx.CompanyData.Remove(company);
                ctx.SaveChanges();
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public async Task CloneEstimateToOrderTest()
        {
            var ctx = GetApiContext();
            CompanyData company = new CompanyData()
            {
                ID = -101,
                Name = "Test Company",
                IsActive = true,
                BID = TestConstants.BID,
                StatusID = (byte)CompanyStatus.Lead,
                LocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID,
            };
            var existingCompanies = ctx.CompanyData.Where(c => c.ID == company.ID && c.BID == company.BID);
            if (existingCompanies.Count() > 0) company = existingCompanies.First();
            else
            {
                ctx.CompanyData.Add(company);
                ctx.SaveChanges();
            }

            var testController = GetDMController();

            OrderData testEstimate = Utils.GetTestOrderData();
            testEstimate.TransactionType = (byte)OrderTransactionType.Estimate;
            testEstimate.OrderStatusID = OrderOrderStatus.EstimatePending;
            testEstimate.ClassTypeID = (int)ClassType.Estimate;

            testEstimate.CompanyID = company.ID;
            testEstimate.PickupLocationID = await Utils.GetLocationID(client);
            testEstimate.TaxGroupID = await Utils.GetTaxGroupID(client);
            testEstimate.ProductionLocationID = testEstimate.PickupLocationID;

            int contactId = await Utils.GetContactID(client, testEstimate.CompanyID);
            int employeeId = await Utils.GetEmployeeID(client);
            var employeeRoleEnteredById = ctx.EmployeeRole.Where(er => er.Name == "Entered By").FirstOrDefault().ID;
            var employeeRoleSalesPersonId = ctx.EmployeeRole.Where(er => er.Name == "Salesperson").FirstOrDefault().ID;

            OrderItemData testOrderItem = new OrderItemData()
            {
                BID = testEstimate.BID,
                HasCustomImage = false,
                HasDocuments = false,
                HasProof = false,
                ID = 1,
                OrderID = 0,
                ItemNumber = 1,
                Quantity = 1,
                Name = "test",
                IsOutsourced = false,
                OrderStatusID = testEstimate.OrderStatusID,
                ItemStatusID = 18,
                ProductionLocationID = testEstimate.ProductionLocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = testEstimate.TaxGroupID,
                TaxGroupOV = false,
                IsTaxExempt = false,
                TransactionType = testEstimate.TransactionType
            };

            testEstimate.Items = new List<OrderItemData>();
            testEstimate.Items.Add(testOrderItem);

            EstimateData createdEstimate = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testEstimate), HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEstimate.ID}/action/linkcontact/primary/{contactId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEstimate.ID}/action/linkcontact/billing/{contactId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEstimate.ID}/action/linkEmployee/{employeeRoleEnteredById}/{employeeId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEstimate.ID}/action/linkEmployee/{employeeRoleSalesPersonId}/{employeeId}", HttpStatusCode.OK);

            var estimateResult = await testController.Post(new DMController.RequestModel()
            {
                id = createdEstimate.ID,
                ctid = Convert.ToInt32(ClassType.Estimate),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            }, null, "https://corebridge.net/wp-content/uploads/2019/01/AdobeStock_202169599-e1551199093506.jpg");

            AssertX.IsType<OkObjectResult>(estimateResult);
            AssertX.IsType<DMItem>(((OkObjectResult)estimateResult).Value);

            var estimateLineItemResult = await testController.Post(new DMController.RequestModel()
            {
                id = createdEstimate.Items.FirstOrDefault().ID,
                ctid = Convert.ToInt32(ClassType.OrderItem),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            }, null, "https://corebridge.net/wp-content/uploads/2019/01/AdobeStock_202169599-e1551199093506.jpg");

            AssertX.IsType<OkObjectResult>(estimateLineItemResult);
            AssertX.IsType<DMItem>(((OkObjectResult)estimateLineItemResult).Value);

            OrderData convertedOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/{createdEstimate.ID}/clone?SaveAsOrder=true&CopyFiles=true", JsonConvert.SerializeObject(createdEstimate), HttpStatusCode.OK);

            Assert.IsNotNull(convertedOrder);
            Assert.IsTrue((convertedOrder.ID != 0));

            var orderResult = await testController.GetDocuments(new DMController.RequestModel()
            {
                id = convertedOrder.ID,
                ctid = Convert.ToInt32(ClassType.Order),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            });
            AssertX.IsType<OkObjectResult>(orderResult);
            AssertX.IsType<List<DMItem>>(((OkObjectResult)orderResult).Value);
            Assert.IsTrue(((List<DMItem>)((OkObjectResult)orderResult).Value).Count > 0);

            var retrievedOrderWithContact = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"/api/order/{convertedOrder.ID}?ContactLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderWithContact?.ContactRoles);
            var contactRows = retrievedOrderWithContact.ContactRoles.Where(t => t.OrderID == convertedOrder.ID);
            Assert.IsNotNull(contactRows);
            Assert.AreEqual(2, contactRows.Count());
            Assert.AreEqual(OrderContactRoleType.Primary, contactRows.ToList()[0].RoleType);
            Assert.AreEqual(OrderContactRoleType.Billing, contactRows.ToList()[1].RoleType);
            Assert.AreEqual(contactId, contactRows.ToList()[0].ContactID);
            Assert.AreEqual(contactId, contactRows.ToList()[1].ContactID);

            var retrievedOrderWithEmployee = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"/api/order/{convertedOrder.ID}?EmployeeLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderWithEmployee?.EmployeeRoles);
            var employeeRows = retrievedOrderWithEmployee.EmployeeRoles.Where(t => t.OrderID == convertedOrder.ID);
            Assert.IsNotNull(employeeRows);
            Assert.AreEqual(2, employeeRows.Count());
            Assert.AreEqual(ctx.UserLink.Where(ul => ul.BID==TestConstants.BID && ul.ID==TestConstants.AuthUserLinkID).First().EmployeeID, employeeRows.Where(er => er.RoleID==254).First().EmployeeID);
            Assert.AreEqual(employeeId, employeeRows.Where(er => er.RoleID == 1).First().EmployeeID);

            var retrievedOrderWithOrderItem = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"/api/order/{convertedOrder.ID}?ItemLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderWithOrderItem.Items);

            var orderItemResult = await testController.GetDocuments(new DMController.RequestModel()
            {
                id = retrievedOrderWithOrderItem.Items.FirstOrDefault().ID,
                ctid = Convert.ToInt32(ClassType.OrderItem),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            });
            AssertX.IsType<OkObjectResult>(orderItemResult);
            AssertX.IsType<List<DMItem>>(((OkObjectResult)orderItemResult).Value);
            Assert.IsTrue(((List<DMItem>)((OkObjectResult)orderItemResult).Value).Count > 0);

            ctx.Entry<CompanyData>(company).Reload();
            Assert.IsTrue(company.StatusText.Contains("Client"));

            //CLEAN UP...
            var createdEstimateLink = ctx.OrderOrderLink.Where(l => l.BID == TestConstants.BID && l.LinkedOrderID == createdEstimate.ID).FirstOrDefault();
            Assert.IsNotNull(createdEstimateLink);
            var convertedOrderLink = ctx.OrderOrderLink.Where(l => l.BID == TestConstants.BID && l.LinkedOrderID == convertedOrder.ID).FirstOrDefault();
            Assert.IsNotNull(convertedOrderLink);
            ctx.OrderOrderLink.Remove(createdEstimateLink);
            ctx.OrderOrderLink.Remove(convertedOrderLink);
            ctx.SaveChanges();

            var deleteEstimateResult = testController.DeleteAll(Convert.ToInt32(ClassType.Estimate), createdEstimate.ID);
            var deleteEstimateLineItemResult = testController.DeleteAll(Convert.ToInt32(ClassType.OrderItem), createdEstimate.Items.FirstOrDefault().ID);
            var deleteOrderResult = testController.DeleteAll(Convert.ToInt32(ClassType.Order), convertedOrder.ID);
            var deleteOrderLineItemResult = testController.DeleteAll(Convert.ToInt32(ClassType.OrderItem), retrievedOrderWithOrderItem.Items.FirstOrDefault().ID);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEstimate.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{convertedOrder.ID}", HttpStatusCode.NoContent);

            try
            {
                ctx.CompanyData.Remove(company);
                ctx.SaveChanges();
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public async Task CloneEstimateWithOption()
        {
            var cloneOption = new EstimateCloneOption()
            {
                FollowupDate = DateTime.Now,
                KeepFiles = true,
                KeepNotes = true,
                OrderStatus = OrderOrderStatus.OrderPreWIP
            };
            await TestCloneWithOption(cloneOption, true);
        }

        private async Task TestCloneWithOption(EstimateCloneOption cloneOption, bool saveAsOrder)
        {

            #region SETUP

            var ctx = GetApiContext();
            CompanyData company = new CompanyData()
            {
                ID = -103,
                Name = "Test Company",
                IsActive = true,
                BID = TestConstants.BID,
                StatusID = (byte)CompanyStatus.Lead,
                LocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID,
            };
            var existingCompanies = ctx.CompanyData.Where(c => c.ID == company.ID && c.BID == company.BID);
            if (existingCompanies.Count() > 0) company = existingCompanies.First();
            else
            {
                ctx.CompanyData.Add(company);
                ctx.SaveChanges();
            }
            var testController = GetDMController();

            OrderData testEstimate = Utils.GetTestOrderData();
            testEstimate.TransactionType = (byte)OrderTransactionType.Estimate;
            testEstimate.OrderStatusID = OrderOrderStatus.EstimatePending;
            testEstimate.ClassTypeID = (int)ClassType.Estimate;

            testEstimate.CompanyID = company.ID;
            testEstimate.PickupLocationID = await Utils.GetLocationID(client);
            testEstimate.TaxGroupID = await Utils.GetTaxGroupID(client);
            testEstimate.ProductionLocationID = testEstimate.PickupLocationID;

            int contactId = await Utils.GetContactID(client, testEstimate.CompanyID);
            int employeeId = await Utils.GetEmployeeID(client);
            var employeeRoleEnteredById = ctx.EmployeeRole.FirstOrDefault(er => er.Name == "Entered By").ID;
            var employeeRoleSalesPersonId = ctx.EmployeeRole.FirstOrDefault(er => er.Name == "Salesperson").ID;

            var itemStatusPending = ctx.OrderItemStatus.FirstOrDefault(i => i.BID == TestConstants.BID &&
                                                                            i.TransactionType == (byte)OrderTransactionType.Estimate &&
                                                                            i.OrderStatusID == testEstimate.OrderStatusID &&
                                                                            i.IsDefault == true).ID;

            var itemStatusLost = ctx.OrderItemStatus.FirstOrDefault(i => i.BID == TestConstants.BID &&
                                                                         i.TransactionType == (byte)OrderTransactionType.Estimate &&
                                                                         i.OrderStatusID == OrderOrderStatus.EstimateLost &&
                                                                         i.IsDefault == true).ID;

            var itemStatusApproved = ctx.OrderItemStatus.FirstOrDefault(i => i.BID == TestConstants.BID &&
                                                                             i.TransactionType == (byte)OrderTransactionType.Estimate &&
                                                                             i.OrderStatusID == OrderOrderStatus.EstimateApproved &&
                                                                             i.IsDefault == true).ID;

            OrderItemData testOrderItem = new OrderItemData()
            {
                BID = testEstimate.BID,
                HasCustomImage = false,
                HasDocuments = false,
                HasProof = false,
                ID = 1,
                OrderID = 0,
                ItemNumber = 1,
                Quantity = 1,
                Name = "test",
                IsOutsourced = false,
                OrderStatusID = testEstimate.OrderStatusID,
                ItemStatusID = 16,
                ProductionLocationID = testEstimate.ProductionLocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = testEstimate.TaxGroupID,
                TaxGroupOV = false,
                IsTaxExempt = false,
                TransactionType = testEstimate.TransactionType
            };

            testEstimate.Items = new List<OrderItemData>();
            testEstimate.Items.Add(testOrderItem);




            #endregion

            #region CREATE
            var createdEstimate = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testEstimate), HttpStatusCode.OK);
            Assert.IsNotNull(createdEstimate);
            var dmRequestModel = new DMController.RequestModel()
            {
                id = createdEstimate.ID,
                ctid = Convert.ToInt32(ClassType.Order),
                Bucket = Web.BucketRequest.Documents
            };

            //Add file for estimate
            var dmController = GetDMController();
            AddFileToForm(dmController, TestTextFileName);
            var create = dmRequestModel.Clone();
            create.PathAndFilename = TestTextFileName;
            var okPost = await dmController.Post(create, null, null);
            Assert.IsTrue(okPost is OkObjectResult);
            dmController.Request.Form = null;
            #endregion


            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEstimate.ID}/action/linkcontact/primary/{contactId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEstimate.ID}/action/linkcontact/billing/{contactId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEstimate.ID}/action/linkEmployee/{employeeRoleEnteredById}/{employeeId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdEstimate.ID}/action/linkEmployee/{employeeRoleSalesPersonId}/{employeeId}", HttpStatusCode.OK);

            var orderItem = Utils.GetTestOrderItemData(testEstimate);
            orderItem.ItemStatusID = 18;

            testEstimate.Items = new List<OrderItemData>()
            {
                orderItem
            };
  

            var extraParams = "";
            if (saveAsOrder)
            {
                extraParams = "?SaveAsOrder= true";
            }

            

            var estimateResult = await testController.Post(new DMController.RequestModel()
            {
                id = createdEstimate.ID,
                ctid = Convert.ToInt32(ClassType.Estimate),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            }, null, "https://corebridge.net/wp-content/uploads/2019/01/AdobeStock_202169599-e1551199093506.jpg");

            AssertX.IsType<OkObjectResult>(estimateResult);
            AssertX.IsType<DMItem>(((OkObjectResult)estimateResult).Value);

            var estimateLineItemResult = await testController.Post(new DMController.RequestModel()
            {
                id = createdEstimate.Items.FirstOrDefault().ID,
                ctid = Convert.ToInt32(ClassType.OrderItem),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            }, null, "https://corebridge.net/wp-content/uploads/2019/01/AdobeStock_202169599-e1551199093506.jpg");

            AssertX.IsType<OkObjectResult>(estimateLineItemResult);
            AssertX.IsType<DMItem>(((OkObjectResult)estimateLineItemResult).Value);

            OrderData convertedOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/{createdEstimate.ID}/cloneWithOption" + extraParams, cloneOption, HttpStatusCode.OK);

            Assert.IsNotNull(convertedOrder);
            Assert.IsTrue((convertedOrder.ID != 0));

            var orderResult = await testController.GetDocuments(new DMController.RequestModel()
            {
                id = convertedOrder.ID,
                ctid = Convert.ToInt32(ClassType.Order),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            });
            AssertX.IsType<OkObjectResult>(orderResult);
            AssertX.IsType<List<DMItem>>(((OkObjectResult)orderResult).Value);
            Assert.IsTrue(((List<DMItem>)((OkObjectResult)orderResult).Value).Count > 0);

            var retrievedOrderWithContact = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"/api/order/{convertedOrder.ID}?ContactLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderWithContact?.ContactRoles);
            var contactRows = retrievedOrderWithContact.ContactRoles.Where(t => t.OrderID == convertedOrder.ID);
            Assert.IsNotNull(contactRows);
            Assert.AreEqual(2, contactRows.Count());
            Assert.AreEqual(OrderContactRoleType.Primary, contactRows.ToList()[0].RoleType);
            Assert.AreEqual(OrderContactRoleType.Billing, contactRows.ToList()[1].RoleType);
            Assert.AreEqual(contactId, contactRows.ToList()[0].ContactID);
            Assert.AreEqual(contactId, contactRows.ToList()[1].ContactID);

            var retrievedOrderWithEmployee = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"/api/order/{convertedOrder.ID}?EmployeeLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderWithEmployee?.EmployeeRoles);
            var employeeRows = retrievedOrderWithEmployee.EmployeeRoles.Where(t => t.OrderID == convertedOrder.ID);
            Assert.IsNotNull(employeeRows);
            Assert.AreEqual(2, employeeRows.Count());
            Assert.AreEqual(ctx.UserLink.Where(ul => ul.BID == TestConstants.BID && ul.ID == TestConstants.AuthUserLinkID).First().EmployeeID, employeeRows.Where(er => er.RoleID == 254).First().EmployeeID);
            Assert.AreEqual(employeeId, employeeRows.Where(er => er.RoleID == 1).First().EmployeeID);

            var retrievedOrderWithOrderItem = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"/api/order/{convertedOrder.ID}?ItemLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderWithOrderItem.Items);

            var orderItemResult = await testController.GetDocuments(new DMController.RequestModel()
            {
                id = retrievedOrderWithOrderItem.Items.FirstOrDefault().ID,
                ctid = Convert.ToInt32(ClassType.OrderItem),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            });
            AssertX.IsType<OkObjectResult>(orderItemResult);
            AssertX.IsType<List<DMItem>>(((OkObjectResult)orderItemResult).Value);
            Assert.IsTrue(((List<DMItem>)((OkObjectResult)orderItemResult).Value).Count > 0);

            ctx.Entry<CompanyData>(company).Reload();
            Assert.IsTrue(company.StatusText.Contains("Client"));

            //CLEAN UP...
            var createdEstimateLink = ctx.OrderOrderLink.FirstOrDefault(l => l.BID == TestConstants.BID && l.LinkedOrderID == createdEstimate.ID);
            Assert.IsNotNull(createdEstimateLink);
            var convertedOrderLink = ctx.OrderOrderLink.FirstOrDefault(l => l.BID == TestConstants.BID && l.LinkedOrderID == convertedOrder.ID);
            Assert.IsNotNull(convertedOrderLink);
            ctx.OrderOrderLink.Remove(createdEstimateLink);
            ctx.OrderOrderLink.Remove(convertedOrderLink);
            ctx.SaveChanges();

            var deleteEstimateResult = testController.DeleteAll(Convert.ToInt32(ClassType.Estimate), createdEstimate.ID);
            var deleteEstimateLineItemResult = testController.DeleteAll(Convert.ToInt32(ClassType.OrderItem), createdEstimate.Items.FirstOrDefault().ID);
            var deleteOrderResult = testController.DeleteAll(Convert.ToInt32(ClassType.Order), convertedOrder.ID);
            var deleteOrderLineItemResult = testController.DeleteAll(Convert.ToInt32(ClassType.OrderItem), retrievedOrderWithOrderItem.Items.FirstOrDefault().ID);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEstimate.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{convertedOrder.ID}", HttpStatusCode.NoContent);

            try
            {
                ctx.CompanyData.Remove(company);
                ctx.SaveChanges();
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }

        }

        private async Task CleanUpEstimateAndCompanies(short testBID)
        {
            var cleanupCtx = GetApiContext(testBID);
            var cleanupEstimates = await cleanupCtx.EstimateData.Where(x => x.CompanyID <= 0).ToListAsync();
            if (cleanupEstimates.Count != 0)
            {
                foreach (var cleanupEstimate in cleanupEstimates)
                {
                    var cleanupOrderKeyDate = await cleanupCtx.OrderKeyDate.Where(x => x.OrderID == cleanupEstimate.ID).ToListAsync();
                    cleanupCtx.RemoveRange(cleanupOrderKeyDate);
                    await cleanupCtx.SaveChangesAsync();
                    var cleanupOrderContactRole = await cleanupCtx.OrderContactRole.Where(x => x.OrderID == cleanupEstimate.ID).ToListAsync();
                    cleanupCtx.RemoveRange(cleanupOrderContactRole);
                    await cleanupCtx.SaveChangesAsync();
                    var cleanupOrderItem = await cleanupCtx.OrderItemData.Where(x => x.OrderID == cleanupEstimate.ID).ToListAsync();
                    cleanupCtx.RemoveRange(cleanupOrderItem);
                    await cleanupCtx.SaveChangesAsync();
                }
                cleanupCtx.RemoveRange(cleanupEstimates);
                await cleanupCtx.SaveChangesAsync();

                var cleanupCompanies = await cleanupCtx.CompanyData.Include(i => i.Orders).Where(x => x.ID <= 0).ToListAsync();
                if (cleanupCompanies.Count != 0)
                {
                    foreach (var companyToRemove in cleanupCompanies)
                    {
                        if (companyToRemove.Orders.Any(x => x.TransactionType != (byte)OrderTransactionType.Order))
                        {
                            cleanupCtx.CompanyData.Remove(companyToRemove);
                        }
                    }
                    await cleanupCtx.SaveChangesAsync();
                }
                var cleanupContact = await cleanupCtx.ContactData.Where(x => x.ID <= 0).ToListAsync();
                if (cleanupContact != null)
                {
                    cleanupCtx.RemoveRange(cleanupContact);
                    await cleanupCtx.SaveChangesAsync();
                }

            }
        }

        [TestMethod]
        public async Task TestEstimateWithCustomerContact()
        {
            short testBID = TestConstants.BID;
            try
            {
                #region Clean Up Data
                await CleanUpEstimateAndCompanies(testBID);
                #endregion
                var ctx = GetApiContext(testBID);


                #region Create Data
                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var contactA = new ContactData()
                {
                    ID = -95,
                    First = "Julius",
                    Last = "Bacosa",
                    BID = TestConstants.BID
                };

                ctx.ContactData.AddRange(new[] { contactA });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var companyA = new CompanyData()
                {
                    ID = -95,
                    Name = "Test Company A",
                    BID = TestConstants.BID,
                    IsActive = true,
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                ctx.CompanyData.Add(companyA);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);


                var contactLinksForCompanyA = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        ContactID = contactA.ID,
                        CompanyID = companyA.ID,
                        Roles = (ContactRole.Primary | ContactRole.Billing)
                    }
                };
                ctx.CompanyContactLink.AddRange(contactLinksForCompanyA);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                #endregion

                var createdContact = await ctx.ContactData.FirstOrDefaultAsync(x => x.ID == contactA.ID);
                Assert.AreEqual(1, createdContact.StatusID);


                OrderData testOrder = new OrderData()
                {
                    BID = TestConstants.BID,
                    ClassTypeID = (int)ClassType.Estimate,
                    ModifiedDT = DateTime.UtcNow,
                    LocationID = location.ID,
                    TransactionType = (byte)OrderTransactionType.Estimate,
                    OrderStatusID = OrderOrderStatus.EstimatePending,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-1000",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                    TaxGroupID = 0,
                    ContactRoles = new List<OrderContactRole>()
                    {
                         new OrderContactRole ()
                         {
                              ContactID = contactA.ID,
                              RoleType = OrderContactRoleType.Primary
                         }
                    }
                };
                testOrder.Items = new List<OrderItemData>()
                {
                    new OrderItemData()
                    {
                        BID = TestConstants.BID,
                        HasCustomImage = false,
                        HasDocuments = false,
                        HasProof = false,
                        ID = 1,
                        OrderID = testOrder.ID,
                        ItemNumber = 1,
                        Quantity = 1,
                        Name = "test",
                        IsOutsourced = false,
                        OrderStatusID = testOrder.OrderStatusID,
                        ItemStatusID = 11,
                        ProductionLocationID = testOrder.ProductionLocationID,
                        PriceIsLocked = false,
                        PriceTaxableOV = false,
                        TaxGroupID = testOrder.TaxGroupID,
                        TaxGroupOV = false,
                        IsTaxExempt = false,
                        TransactionType = testOrder.TransactionType
                    }
                };

                testOrder.CompanyID = companyA.ID;

                var response = await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
                Assert.IsNotNull(response);

                var assertResultCtx = GetApiContext(testBID);
                var updatedContact = await assertResultCtx.ContactData.FirstOrDefaultAsync(x => x.ID == contactA.ID);
                Assert.AreEqual(2, updatedContact.StatusID);
            }
            finally
            {
                #region Clean Up Data
                await CleanUpEstimateAndCompanies(testBID);
                #endregion
            }
        }

        [TestMethod]
        public async Task ChangeEstimateStatusTest()
        {
            var allowedOrderTypeStatusMap = new Dictionary<OrderTransactionType, OrderOrderStatus[]>()
            {
                {
                    OrderTransactionType.Estimate,
                    new OrderOrderStatus[] {
                        OrderOrderStatus.EstimateApproved,
                        OrderOrderStatus.EstimateLost,
                        OrderOrderStatus.EstimatePending,
                        OrderOrderStatus.EstimateVoided,
                    }
                }
            };

            OrderOrderStatus[] voidStatuses = new OrderOrderStatus[] {
                OrderOrderStatus.EstimateVoided,
            };

            OrderData testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            // Order Estimates can't be initally void
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimateVoided;
            await assertChangingOrderStatus(testOrder, OrderOrderStatus.OrderWIP, HttpStatusCode.BadRequest);

            // Orders can only change the status within its allowed OrderStatus
            foreach (KeyValuePair<OrderTransactionType, OrderOrderStatus[]> itemMap in allowedOrderTypeStatusMap)
            {
                // get other OrderTypes status
                //List<OrderOrderStatus> otherInvalidStatuses = allowedOrderTypeStatusMap.Where(item => item.Key != itemMap.Key).SelectMany(x => x.Value).ToList();

                // run through all the non-void statuses
                foreach (OrderOrderStatus orderStatus in itemMap.Value)
                {
                    testOrder.TransactionType = (byte)itemMap.Key;
                    testOrder.OrderStatusID = orderStatus;

                    switch (orderStatus)
                    {
                        case OrderOrderStatus.EstimateApproved:
                            foreach (OrderOrderStatus changeStatus in itemMap.Value.Where(item => item != orderStatus))
                            {
                                await assertChangingOrderStatus(testOrder, changeStatus, HttpStatusCode.BadRequest);
                            }
                            break;

                        case OrderOrderStatus.EstimateLost:
                        case OrderOrderStatus.EstimateVoided:
                            foreach (OrderOrderStatus changeStatus in itemMap.Value.Where(item => item != orderStatus))
                            {
                                if (changeStatus == OrderOrderStatus.EstimatePending)
                                {
                                    await assertChangingOrderStatus(testOrder, changeStatus, HttpStatusCode.OK);
                                }
                                else
                                {
                                    await assertChangingOrderStatus(testOrder, changeStatus, HttpStatusCode.BadRequest);
                                }
                            }
                            break;

                        default:
                            foreach (OrderOrderStatus changeStatus in itemMap.Value.Where(item => item != orderStatus))
                            {
                                await assertChangingOrderStatus(testOrder, changeStatus, HttpStatusCode.OK);
                            }
                            break;
                    }



                    //// test valid order status change
                    //var otherValidStatuses = itemMap.Value.Where(item => item != orderStatus && !voidStatuses.Contains(item)).ToList();
                    //foreach (OrderOrderStatus otherStatus in otherValidStatuses)
                    //{
                    //    //await assertChangingOrderStatus(testOrder, otherStatus, HttpStatusCode.OK);
                    //}
                }
            }
        }

        private async Task<EstimateData> assertChangingOrderStatus(OrderData testOrder, OrderOrderStatus newOrderStatus, HttpStatusCode expectedStatusCode)
        {
            var order = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            string orderStatusEndpoint = $"{apiUrl}/{order.ID}/action/changeorderstatus/{Convert.ToInt32(newOrderStatus)}";
            await EndpointTests.AssertPostStatusCode(this.client, orderStatusEndpoint, expectedStatusCode);
            return order;
        }

        [TestMethod]
        public async Task CheckEstimateStatusTest()
        {
            OrderData testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            // Estimate transaction types should return 400 'Invalid transaction type'
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimateApproved;
            await testCheckOrderStatus(testOrder, OrderOrderStatus.OrderInvoiced, HttpStatusCode.BadRequest);
        }

        private async Task testCheckOrderStatus(OrderData testOrder, OrderOrderStatus newOrderStatus, HttpStatusCode expectedStatusCode)
        {
            var order = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            string orderStatusEndpoint = $"{apiUrl}/{order.ID}/action/checkorderstatus/";
            await EndpointTests.AssertPostStatusCode(this.client, orderStatusEndpoint, expectedStatusCode);
        }

        [TestMethod]
        public async Task RecomputeEstimateGLTest()
        {
            var priority = "low";
            var testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            // test INVALID priority
            priority = "invalid";
            await EndpointTests.AssertPostStatusCode(this.client, $"{apiUrl}/{createdOrder.ID}/action/recomputeGL?priority={priority}", HttpStatusCode.BadRequest);

            // test low priority
            priority = "low";
            await EndpointTests.AssertPostStatusCode(this.client, $"{apiUrl}/{createdOrder.ID}/action/recomputeGL?priority={priority}", HttpStatusCode.BadRequest);
            // test medium priority
            priority = "medium";
            await EndpointTests.AssertPostStatusCode(this.client, $"{apiUrl}/{createdOrder.ID}/action/recomputeGL?priority={priority}", HttpStatusCode.BadRequest);
            // test high priority
            priority = "high";
            await EndpointTests.AssertPostStatusCode(this.client, $"{apiUrl}/{createdOrder.ID}/action/recomputeGL?priority={priority}", HttpStatusCode.BadRequest);
            // Test RecomputeGL with non existing order ID
            var failID = -12345;
            string orderStatusEndpoint = $"{apiUrl}/{failID}/action/recomputeGL?priority={priority}";
            await EndpointTests.AssertPostStatusCode(this.client, orderStatusEndpoint, HttpStatusCode.BadRequest);
        }

        [TestMethod]
        [TestCategory("compute")]
        public async Task TestEstimateCompute()
        {
            //test invalid request
            object invalidRequest = JsonConvert.DeserializeObject(@"{ 'Items': false }"); // force invalid request by setting OrderpriceRequest.Result property to invalid type
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(invalidRequest), HttpStatusCode.BadRequest);

            //test valid request
            OrderPriceRequest testRequest = new OrderPriceRequest()
            {
                Items = new List<ItemPriceRequest>()
                {
                   new ItemPriceRequest()
                   {
                       EngineType = PricingEngineType.Supplied,
                       Components = new List<ComponentPriceRequest>
                       {
                           new ComponentPriceRequest
                           {
                               ComponentType = OrderItemComponentType.Assembly,
                               TotalQuantity = 1,
                               PriceUnit = 100m,
                               PriceUnitOV = true,
                               Variables = new Dictionary<string, VariableValue>()
                               {
                                   { "Height", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 24m } },
                                   { "Width", new VariableValue() { DataType = DataType.Number, ValueOV = true, ValueAsDecimal = 36m } },
                               }
                           }
                       },
                   },
                   new ItemPriceRequest()
                   {
                       EngineType = PricingEngineType.Supplied,
                       Components = new List<ComponentPriceRequest>
                       {
                           new ComponentPriceRequest
                           {
                               ComponentType = OrderItemComponentType.Assembly,
                               TotalQuantity = 1,
                               PriceUnit = 50m,
                               PriceUnitOV = true,
                           }
                       },
                   }
               }
            };

            string jsonString = JsonConvert.SerializeObject(testRequest);
            //{"Height":{"Type":15,"Value":"24","ValueOV":true},"Width":{"Type":15,"Value":"36","ValueOV":true}}
            //jsonString = jsonString.Replace("100.0","100.0, \"Variables\":{\"Height\":{\"Type\":15,\"Value\":\"24\",\"ValueOV\":true},\"Width\":{\"Type\":15,\"Value\":\"36\",\"ValueOV\":true}}");
            //jsonString = jsonString.Replace("100.0", "100.0, \"Variables\":{\"Height\":null}");
            //jsonString = "{\"Items\":[{\"EngineType\":\"Supplied\",\"Quantity\":0.0,\"Variables\":{\"Height\":{\"Type\":0,\"Value\":\"24\"},\"Width\":{\"Type\":0,\"Value\":\"36\"}},\"PriceNet\":100.0},{\"EngineType\":\"Supplied\",\"Quantity\":0.0,\"PriceNet\":50.0}]}";
            //var test = JsonConvert.DeserializeObject<OrderPriceRequest>(jsonString);
            OrderPriceResult result = await EndpointTests.AssertPostStatusCode<OrderPriceResult>(client, $"{apiUrl}/compute", jsonString, HttpStatusCode.OK);

            Assert.IsNotNull(result);
            Assert.AreEqual(150m, result.PricePreTax);

            Assert.IsNotNull(result.Items);
            Assert.AreEqual(2, result.Items.Count);

            Assert.AreEqual(100m, result.Items[0].PricePreTax);
            Assert.AreEqual(1, result.Items[0].Components.Count);
            Assert.IsNotNull(result.Items[0].Components[0].Variables);
            Assert.AreEqual(2, result.Items[0].Components[0].Variables.Count);

            VariableValue varValue;

            varValue = result.Items[0].Components[0].Variables.GetValueOrDefault("Height");
            Assert.IsNotNull(varValue);
            Assert.AreEqual(24m, varValue.ValueAsDecimal);

            varValue = result.Items[0].Components[0].Variables.GetValueOrDefault("Width");
            Assert.IsNotNull(varValue);
            Assert.AreEqual(36m, varValue.ValueAsDecimal);


            Assert.AreEqual(50m, result.Items[1].PricePreTax);
        }

        [TestMethod]
        public async Task GetOrderDestinationsForEstimate()
        {
            //Create Order
            var testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            //Create Item
            var testOrderItem = Utils.GetTestOrderItemData(createdOrder);
            var createdOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"/api/orderitem/", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);

            // setup test data
            EstimateData orderData = (await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"api/estimate/{createdOrder.ID}", HttpStatusCode.OK));
            OrderItemStatus orderItemStatus = (await EndpointTests.AssertGetStatusCode<OrderItemStatus>(client, $"api/orderitemstatus/{createdOrderItem.ItemStatusID}", HttpStatusCode.OK));

            //Start orderdestination
            string destinationApiUrl = "/api/orderdestination";
            
            OrderDestinationData postRequestModel = Utils.GetTestOrderDestinationData("Test", 1, orderItemStatus.ID, createdOrderItem.ID, orderData.ID, 0m, (OrderTransactionType)createdOrder.TransactionType);
            var postResult = await EndpointTests.AssertPostStatusCode<OrderDestinationData>(client, destinationApiUrl, JsonConvert.SerializeObject(postRequestModel), HttpStatusCode.OK);
            var retrievedPostResult = await EndpointTests.AssertGetStatusCode<OrderDestinationData[]>(client, $"api/estimate/{orderData.ID}/orderdestination/", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedPostResult);

            var retrievedSimplePostResult = await EndpointTests.AssertGetStatusCode< List<SimpleOrderDestinationData>>(client, $"api/estimate/{createdOrder.ID}/orderdestination/simplelist", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedSimplePostResult);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);

            await EndpointTests.AssertDeleteStatusCode(client, $"/api/orderitem/{createdOrderItem.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestEstimateCanDelete()
        {
            #region CREATE
            var testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.BadRequest);

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            #endregion

            #region Can DELETE OrderOrderStatus.EstimatePending
            await EndpointTests.AssertGetStatusCode<BooleanResponse>(this.client, $"{apiUrl}/{createdOrder.ID}/action/CanDelete", HttpStatusCode.BadRequest);
            #endregion

            #region Can DELETE invalid ID
            await EndpointTests.AssertGetStatusCode<BooleanResponse>(this.client, $"{apiUrl}/-100/action/CanDelete", HttpStatusCode.NotFound);
            #endregion

            #region Can DELETE valid ID and Aprroved
            createdOrder.OrderStatusID = OrderOrderStatus.EstimateApproved;
            await EndpointTests.AssertPutStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}", JsonConvert.SerializeObject(createdOrder), HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode<BooleanResponse>(this.client, $"{apiUrl}/{createdOrder.ID}/action/CanDelete", HttpStatusCode.OK);
            #endregion

            #region DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        [TestMethod]
        public async Task ClonedEstimateLinkTest()
        {
            #region SETUP
            var ctx = GetApiContext();
            EstimateData testEstimate = Utils.GetTestEstimateData();
            testEstimate.CompanyID = await Utils.GetCompanyID(client);
            testEstimate.PickupLocationID = await Utils.GetLocationID(client);
            testEstimate.TaxGroupID = await Utils.GetTaxGroupID(client);
            testEstimate.ProductionLocationID = testEstimate.PickupLocationID;
            #endregion

            #region CREATE 
            OrderData createdEstimate = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testEstimate), HttpStatusCode.OK);
            Assert.IsNotNull(createdEstimate);
            #endregion

            #region CLONE 
            OrderData clonedEstimate = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/{createdEstimate.ID}/clone", null, HttpStatusCode.OK);
            Assert.IsNotNull(clonedEstimate);
            #endregion

            #region GET Estimate Links
            var estimateLink = ctx.OrderOrderLink.Where(l => l.BID == TestConstants.BID && l.LinkType == OrderOrderLinkType.ClonedTo && l.LinkedOrderID == clonedEstimate.ID).FirstOrDefault();
            Assert.IsNotNull(estimateLink);
            var clonedEstimateLink = ctx.OrderOrderLink.Where(l => l.BID == TestConstants.BID && l.LinkType == OrderOrderLinkType.ClonedFrom && l.LinkedOrderID == createdEstimate.ID).FirstOrDefault();
            Assert.IsNotNull(clonedEstimateLink);
            #endregion

            #region Test KeyDates
            var retrievedClone = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{clonedEstimate.ID}", HttpStatusCode.OK);
            var keyDate = retrievedClone.Dates.Where(t => t.KeyDateType == OrderKeyDateType.Followup).FirstOrDefault();
            Assert.IsNotNull(keyDate);
            Assert.IsTrue(keyDate.KeyDT > DateTime.UtcNow);
            Assert.IsTrue(keyDate.KeyDT <= DateTime.UtcNow.AddDays(1));
            #endregion

            #region CLEANUP
            ctx.OrderOrderLink.Remove(estimateLink);
            ctx.OrderOrderLink.Remove(clonedEstimateLink);
            ctx.SaveChanges();

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdEstimate.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{clonedEstimate.ID}", HttpStatusCode.NoContent);
            #endregion
        }

    }
}
