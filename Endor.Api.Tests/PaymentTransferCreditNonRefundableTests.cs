﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Request;
using PaymentApplication = Endor.Models.PaymentApplication;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Classes.Responses;

namespace Endor.Api.Tests
{
    [TestClass]
    [TestCategory("Payment")]
    public class PaymentTransferCreditNonRefundableTests : PaymentTestHelper
    {
        private const string transferCreditApiUrl = "/api/payment/transfertocredit";
        private System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        private PaymentTransferCreditRequest GetTransferCreditRequest(int companyID, int? masterID, int orderID, bool isRefundable, decimal amount)
        {
            return new PaymentTransferCreditRequest()
            {
                PaymentTransactionType = isRefundable ? PaymentTransactionType.Refund_to_Refundable_Credit : PaymentTransactionType.Refund_to_Nonrefundable_Credit,
                MasterPaymentID = masterID,
                CompanyID = companyID,
                PaymentMethodId = isRefundable ? (int)PaymentMethodType.RefundableCredit : (int)PaymentMethodType.NonRefundableCredit,
                Applications = new List<PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest()
                    {
                        OrderID = orderID,
                        Amount = -amount,
                    },
                    new PaymentApplicationRequest()
                    {
                        OrderID = null,
                        Amount = amount,
                    },
                }
            };
        }

        [TestInitialize]
        public async Task Initialize()
        {
            await CleanUpTestData();
        }

        [TestCleanup]
        public async Task Teardown()
        {
            await CleanUpTestData();
        }

        [TestMethod]
        public async Task TransferCreditOneOrderOneMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 2.05m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount,
                isRefundable: false,
                companyID: company.ID
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount,
                isRefundable: false
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            Assert.AreEqual(amount - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            Assert.AreEqual(amount, master.NonRefBalance); // master should have same amount
        }

        [TestMethod]
        public async Task TransferCreditOneOrderTwoMasters()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 15m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount / 2,
                isRefundable: false,
                companyID: company.ID
            );
            var master2 = await CreatePaymentForOrder(
                ID: -98,
                orderID: order.ID,
                amount: amount / 2,
                isRefundable: false,
                companyID: company.ID
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount,
                isRefundable: true
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            master2 = ctx.PaymentMaster.First(t => t.ID == master2.ID);

            Assert.AreEqual(amount - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            Assert.AreEqual(10m, master.NonRefBalance);
            Assert.AreEqual(amount / 2, master2.NonRefBalance);
        }

        [TestMethod]
        public async Task TransferCreditTwoOrdersOneMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 2.05m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: 10,
                amountPaid: 10
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: 10,
                amountPaid: 10
            );
            var master = await CreatePaymentForMultipleOrders(
                ID: -99,
                orderIDs: new int[] { order.ID, order2.ID },
                amount: amount,
                isRefundable: false,
                companyID: company.ID
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount * 2,
                isRefundable: false
            );
            request.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = -refund_amount,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order2.ID,
                    Amount = -refund_amount,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = refund_amount * 2,
                },
            };
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            order2 = ctx.OrderData.First(t => t.ID == order2.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            Assert.AreEqual(10 - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            Assert.AreEqual(10 - refund_amount, order2.PaymentPaid);
            Assert.AreEqual(refund_amount, order2.PaymentBalanceDue);
            Assert.AreEqual(amount, master.NonRefBalance); // master should have same amount
        }

        [TestMethod]
        public async Task TransferCreditTwoOrdersTwoMasters()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var refund_amount = 2.05m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: 10,
                amountPaid: 10
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: 10,
                amountPaid: 10
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: 10,
                isRefundable: false,
                companyID: company.ID
            );
            var master2 = await CreatePaymentForOrder(
                ID: -98,
                orderID: order2.ID,
                amount: 10,
                isRefundable: false,
                companyID: company.ID
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount * 2,
                isRefundable: false
            );
            request.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = -refund_amount,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order2.ID,
                    Amount = -refund_amount,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = refund_amount * 2,
                },
            };
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            order2 = ctx.OrderData.First(t => t.ID == order2.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            master2 = ctx.PaymentMaster.First(t => t.ID == master2.ID);
            Assert.AreEqual(10 - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            Assert.AreEqual(10 - refund_amount, order2.PaymentPaid);
            Assert.AreEqual(refund_amount, order2.PaymentBalanceDue);
            Assert.AreEqual(10, master.NonRefBalance); // master should have same amount
            Assert.AreEqual(10, master2.NonRefBalance); // master should have same amount
        }

        [TestMethod]
        public async Task TransferCreditTwiceOnOneMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 2.05m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForMultipleOrders(
                ID: -99,
                orderIDs: new int[] { order.ID, order2.ID },
                amount: amount,
                isRefundable: false,
                companyID: company.ID
            );
            var request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount,
                isRefundable: false
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            Assert.AreEqual(amount - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            Assert.AreEqual(amount, master.NonRefBalance); // master should have same amount
            // refund again
            request = GetTransferCreditRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order2.ID,
                amount: refund_amount,
                isRefundable: false
            );
            result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            order2 = ctx.OrderData.First(t => t.ID == order2.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            Assert.AreEqual(amount - refund_amount, order2.PaymentPaid);
            Assert.AreEqual(refund_amount, order2.PaymentBalanceDue);
            Assert.AreEqual(amount, master.NonRefBalance); // master should have same amount
        }
        
    }
}
