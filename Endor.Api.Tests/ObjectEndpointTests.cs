﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AssertX = Xunit.Assert;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using Microsoft.AspNetCore.Mvc;
using Endor.Models;
using Endor.Api.Web.Controllers;
using Endor.Api.Web.Services;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.EF;
using Newtonsoft.Json;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.Net;

namespace Endor.Api.Tests
{
    [TestClass]
    public class ObjectEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/API/Object";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task TestObjectActions()
        {
            var ctx = GetApiContext();
            var order = ctx.OrderData.FirstOrDefault();
            int CTID = (int)ClassType.Order;
            int ID = order.ID;
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{CTID}/{ID}/Action/SetUrgency/hi",null, HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{CTID}/{ID}/Action/SetPriority/2", null, HttpStatusCode.OK);
            ctx = GetApiContext();
            var updatedOrder = ctx.OrderData.Where(o => o.ID == order.ID).First();
            Assert.IsTrue(updatedOrder.IsUrgent.HasValue);
            Assert.IsTrue(updatedOrder.IsUrgent.Value);
            Assert.IsTrue(updatedOrder.Priority.Value == 2);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{CTID}/{ID}/Action/SetUrgency/normal", null, HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{CTID}/{ID}/Action/SetPriority/4", null, HttpStatusCode.OK);
            ctx = GetApiContext();
            updatedOrder = ctx.OrderData.Where(o => o.ID == order.ID).First();
            Assert.IsTrue(updatedOrder.IsUrgent.HasValue);
            Assert.IsFalse(updatedOrder.IsUrgent.Value);
            Assert.IsTrue(updatedOrder.Priority.Value == 4);
        }

        [TestMethod]
        public async Task TestObjectGet()
        {
            var result = await EndpointTests.AssertGetStatusCode<GenericResponse<string[]>>(client, $"{apiUrl}/{ClassType.Order.ID()}/GetSupportedFeatures", HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(4, result.Data.Length);
            Assert.IsTrue(result.Data.Contains("SetUrgency"));
            Assert.IsTrue(result.Data.Contains("SetPriority"));
            Assert.IsTrue(result.Data.Contains("AddTag"));
            Assert.IsTrue(result.Data.Contains("RemoveTag"));

            result = await EndpointTests.AssertGetStatusCode<GenericResponse<string[]>>(client, $"{apiUrl}/{ClassType.OrderItem.ID()}/GetSupportedFeatures", HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(4, result.Data.Length);
            Assert.IsTrue(result.Data.Contains("SetUrgency"));
            Assert.IsTrue(result.Data.Contains("SetPriority"));
            Assert.IsTrue(result.Data.Contains("AddTag"));
            Assert.IsTrue(result.Data.Contains("RemoveTag"));

            result = await EndpointTests.AssertGetStatusCode<GenericResponse<string[]>>(client, $"{apiUrl}/{ClassType.OrderDestination.ID()}/GetSupportedFeatures", HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(4, result.Data.Length);
            Assert.IsTrue(result.Data.Contains("SetUrgency"));
            Assert.IsTrue(result.Data.Contains("SetPriority"));
            Assert.IsTrue(result.Data.Contains("AddTag"));
            Assert.IsTrue(result.Data.Contains("RemoveTag"));

            result = await EndpointTests.AssertGetStatusCode<GenericResponse<string[]>>(client, $"{apiUrl}/{ClassType.Estimate.ID()}/GetSupportedFeatures", HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(4, result.Data.Length);
            Assert.IsTrue(result.Data.Contains("SetUrgency"));
            Assert.IsTrue(result.Data.Contains("SetPriority"));
            Assert.IsTrue(result.Data.Contains("AddTag"));
            Assert.IsTrue(result.Data.Contains("RemoveTag"));

            result = await EndpointTests.AssertGetStatusCode<GenericResponse<string[]>>(client, $"{apiUrl}/{ClassType.Company.ID()}/GetSupportedFeatures", HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(4, result.Data.Length);
            Assert.IsTrue(result.Data.Contains("SetActive"));
            Assert.IsTrue(result.Data.Contains("SetInactive"));
            Assert.IsTrue(result.Data.Contains("AddTag"));
            Assert.IsTrue(result.Data.Contains("RemoveTag"));

            result = await EndpointTests.AssertGetStatusCode<GenericResponse<string[]>>(client, $"{apiUrl}/{ClassType.Contact.ID()}/GetSupportedFeatures", HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(4, result.Data.Length);
            Assert.IsTrue(result.Data.Contains("SetActive"));
            Assert.IsTrue(result.Data.Contains("SetInactive"));
            Assert.IsTrue(result.Data.Contains("AddTag"));
            Assert.IsTrue(result.Data.Contains("RemoveTag"));
        }
    }
}
