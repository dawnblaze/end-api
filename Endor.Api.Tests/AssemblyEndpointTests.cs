﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Endor.CBEL.Interfaces;
using Endor.Tenant;
using Endor.AzureStorage;
using Endor.DocumentStorage.Models;
using Endor.Pricing;
using Endor.CBEL.ObjectGeneration;
using Endor.CBEL.Common;
using Endor.CBEL.Compute;
using Endor.Api.Web.Classes;
using Endor.Units;
using Endor.Logging.Client;

namespace Endor.Api.Tests
{
    [TestClass]
    public class AssemblyEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/assemblypart";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestAssemblyName;
        private string NewTestAssemblyElementName;
        private string NewTestAssemblyVariableName;
        private int NewTestAssemblyElementCount = 0;
        private short testBID = 1;
        public TestContext TestContext { get; set; }

        private async Task<MachineData> GetNewMachine(string machineName, int machineTemplateID)
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;

            var simpleTaxCodes = await EndpointTests.AssertGetStatusCode<SimpleTaxabilityCode[]>(client, $"/api/taxcode/SimpleList", HttpStatusCode.OK);
            short taxCodeID = simpleTaxCodes.FirstOrDefault().ID;

            var simpleLocations = await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, $"/api/location/SimpleList", HttpStatusCode.OK);
            byte locationID = simpleLocations.FirstOrDefault().ID;

            return new MachineData()
            {
                Name = machineName,
                IncomeAccountID = glAccountID,
                ExpenseAccountID = glAccountID,
                TaxCodeID = taxCodeID,
                IsActive = true,
                BID = testBID,
                ActiveInstanceCount = 0,
                ActiveProfileCount = 0,
                Description = "Description",
                HasImage = false,
                SKU = "SKU",
                WorksheetData = "[]",
                MachineTemplateID = machineTemplateID,
                Instances = new List<MachineInstance>
                {
                    new MachineInstance
                    {
                        IsActive = true,
                        Name = machineName + " - Instance",
                        LocationID = locationID,
                        Manufacturer = "Manufacturer",
                        SerialNumber = "SerialNumber",
                        IPAddress = "IPAddress",
                        PurchaseDate = DateTime.Now,
                        HasServiceContract = true,
                        ContractNumber = "",
                        ContractStartDate = DateTime.Now,
                        ContractEndDate = DateTime.Now,
                        ContractInfo = "ContractInfo",
                        Model = "Model",
                        BID = testBID
                    }
                },
                Profiles = new List<MachineProfile>
                {
                    new MachineProfile
                    {
                        IsActive = true,
                        IsDefault = true,
                        Name = machineName + " - Profile",
                        MachineTemplateID = machineTemplateID,
                        AssemblyOVDataJSON = "{}",
                        BID = testBID
                    }
                }
            };
        }

        private AssemblyData GetNewModel()
        {
            return new AssemblyData()
            {
                Name = this.NewTestAssemblyName,
                IsActive = true,
                BID = TestConstants.BID,
            };
        }

        private List<AssemblyElement> GetNewElements()
        {
            NewTestAssemblyElementCount++;
            var item1 = new AssemblyElement()
            {
                BID = TestConstants.BID,
                VariableName = "unknown",
            };
            NewTestAssemblyElementCount++;
            var item2 = new AssemblyElement()
            {
                BID = TestConstants.BID,
                VariableName = "unknown",
            };
            return new List<AssemblyElement> { item1, item2 };
        }

        [TestInitialize]
        public void Initialize()
        {
            NewTestAssemblyName = "TEST_ASSEMBLY_DATA_" + DateTime.UtcNow;
            NewTestAssemblyElementName = "_TEST_ASSEMBLY_ELEMENT_DATA" + DateTime.UtcNow;
            NewTestAssemblyVariableName = "_TEST_ASSEMBLY_VARIABLE_DATA" + ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeSeconds();
        }

        [TestMethod]
        public async Task GetAssemblyWithNoChildrenTest()
        {
            var ctx = GetApiContext();
            if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
                await ctx.Database.MigrateAsync();

            AssemblyData layoutTest = GetNewModel();

            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, apiUrl, JsonConvert.SerializeObject(layoutTest), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);

            try
            {
                List<AssemblyData> list;
                AssemblyData myItem;

                list = await EndpointTests.AssertGetStatusCode<List<AssemblyData>>(client, apiUrl, HttpStatusCode.OK);

                Assert.IsTrue(list.Count > 0);

                myItem = await EndpointTests.AssertGetStatusCode<AssemblyData>(client, $"{apiUrl}/{assemblyData.ID}", HttpStatusCode.OK);

                Assert.IsNotNull(myItem);
                Assert.IsTrue(myItem.Layouts == null || myItem.Layouts.Count < 1);
                Assert.IsTrue(myItem.Variables == null || myItem.Layouts.Count < 1);
            }

            finally
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{assemblyData.ID}", HttpStatusCode.NoContent);
            }
        }

        [TestMethod]
        public async Task GetAssemblyWithLayoutsTest()
        {
            var ctx = GetApiContext();
            if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
                await ctx.Database.MigrateAsync();

            AssemblyData layoutTest = GetNewModel();
            layoutTest.Layouts = new List<AssemblyLayout>()
            {
                new AssemblyLayout()
                {
                    BID = TestConstants.BID,
                    Elements = GetNewElements(),
                    LayoutType = AssemblyLayoutType.AssemblyBasic,
                    Name = "basic"
                }
            };

            //CREATE
            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, apiUrl, JsonConvert.SerializeObject(layoutTest), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);
            int testAssemblyID = assemblyData.ID;
            //DLL Saving Test
            ITenantDataCache cache = this.GetMockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);
            (ICBELAssembly assembly, string assemblyCode) = await CBELAssemblyHelper.LoadAssemblyFromStorage(assemblyData.ID, ClassType.Assembly.ID(), 1, cache, logger, ctx, TestConstants.BID);
            Assert.IsTrue(assembly.Variables.Count > 0);

            //READ
            //read all
            List<AssemblyData> list = await EndpointTests.AssertGetStatusCode<List<AssemblyData>>(client, apiUrl, HttpStatusCode.OK);
            Assert.IsTrue(list.Count > 0);

            // read one
            // Test default of IncludeLayouts=Full 
            AssemblyData myItem = await EndpointTests.AssertGetStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", HttpStatusCode.OK);

            Assert.IsNotNull(myItem);
            Assert.IsNotNull(myItem.Layouts);
            Assert.IsNotNull(myItem.Layouts.FirstOrDefault());
            int firstLayoutID = myItem.Layouts.FirstOrDefault().ID;
            Assert.IsNotNull(myItem.Layouts.FirstOrDefault().Elements);

            // read one
            // Test excluding Layouts
            myItem = await EndpointTests.AssertGetStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}?IncludeLayouts=None", HttpStatusCode.OK);

            Assert.IsNotNull(myItem);
            Assert.IsNull(myItem.Layouts);

            var layouts = await EndpointTests.AssertGetStatusCode<AssemblyLayout[]>(client, $"{apiUrl}/{testAssemblyID}/layout?LayoutTypes={AssemblyLayoutType.AssemblyBasic}&LayoutTypes={AssemblyLayoutType.AssemblyAdvanced}", HttpStatusCode.OK);
            Assert.IsTrue(layouts != null && layouts.Length > 0 && layouts.FirstOrDefault(y => y.ID == firstLayoutID) != null);

            layouts = await EndpointTests.AssertGetStatusCode<AssemblyLayout[]>(client, $"{apiUrl}/{testAssemblyID}/layout?LayoutTypes={AssemblyLayoutType.MachineTemplateSetup}", HttpStatusCode.OK);
            Assert.IsTrue(layouts != null && !layouts.Any(y => y.ID == firstLayoutID));

            //UPDATE
            //update existing
            const string AssemblyUpdateTestString = "assembly update test";
            assemblyData.Description = AssemblyUpdateTestString;
            assemblyData.Layouts.FirstOrDefault().Name = AssemblyUpdateTestString;
            assemblyData.Layouts.FirstOrDefault().Elements.FirstOrDefault().Tooltip = AssemblyUpdateTestString;
            var updatedAssembly = await EndpointTests.AssertPutStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", JsonConvert.SerializeObject(assemblyData), HttpStatusCode.OK);
            Assert.IsNotNull(updatedAssembly);
            Assert.AreEqual(AssemblyUpdateTestString, updatedAssembly.Description);
            Assert.IsNotNull(updatedAssembly.Layouts.FirstOrDefault(x => x.Name == AssemblyUpdateTestString));
            Assert.IsNotNull(updatedAssembly.Layouts.FirstOrDefault(x => x.Name == AssemblyUpdateTestString).Elements.FirstOrDefault(y => y.Tooltip == AssemblyUpdateTestString));
            //DLL Saving Test
            (ICBELAssembly updateAssembly, string updateAssemblyCode) = await CBELAssemblyHelper.LoadAssemblyFromStorage(assemblyData.ID, ClassType.Assembly.ID(), 1, cache, logger, ctx, TestConstants.BID);
            Assert.IsTrue(updateAssembly.Variables.Count > 0);

            //update to new element
            const string AddedElementViaPUTTooltip = "added element via PUT";
            GetNewElements().ForEach(x =>
            {
                x.Tooltip = AddedElementViaPUTTooltip;
                updatedAssembly.Layouts.FirstOrDefault().Elements.Add(x);
            });
            updatedAssembly = await EndpointTests.AssertPutStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", JsonConvert.SerializeObject(updatedAssembly), HttpStatusCode.OK);
            Assert.IsNotNull(updatedAssembly.Layouts.Where(x => x.Elements.FirstOrDefault(y => y.Tooltip == AddedElementViaPUTTooltip) != null).FirstOrDefault());

            //DELETE
            EntityStorageClient storageClient = new EntityStorageClient((await cache.Get(TestConstants.BID)).StorageConnectionString, TestConstants.BID);
            var assemblyDM = new DMID() { id = assembly.ID, ctid = ClassType.Assembly.ID() };
            string assemblyName = CBELAssemblyHelper.AssemblyName(TestConstants.BID, assemblyData.ID, ClassType.Assembly.ID(), 1);
            await storageClient.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDM, $"source/{assemblyName}.cs");

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testAssemblyID}", HttpStatusCode.NoContent);
            Assert.IsFalse(ctx.AssemblyLayout.Any(x => x.AssemblyID == testAssemblyID));
            Assert.IsFalse(ctx.AssemblyElement.Any(x => x.AssemblyID == testAssemblyID));
            Assert.IsFalse(ctx.AssemblyVariable.Any(x => x.AssemblyID == testAssemblyID));
            Assert.IsFalse(ctx.AssemblyCategoryLink.Any(x => x.PartID == testAssemblyID));
        }

        [TestMethod]
        public async Task TestAssemblyCanDelete()
        {
            AssemblyData itemTest = GetNewModel();

            //test: expect OK
            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{assemblyData.ID}/action/candelete", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{assemblyData.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task AssemblyWithVariablesTest()
        {
            var ctx = GetApiContext();
            if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
                await ctx.Database.MigrateAsync();

            AssemblyData layoutTest = GetNewModel();
            layoutTest.Variables = new List<AssemblyVariable>()
            {
                GetNewVariable()
            };

            //CREATE
            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, apiUrl, JsonConvert.SerializeObject(layoutTest), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);
            int testAssemblyID = assemblyData.ID;

            //READ
            //read all
            List<AssemblyData> list = await EndpointTests.AssertGetStatusCode<List<AssemblyData>>(client, apiUrl, HttpStatusCode.OK);
            Assert.IsTrue(list.Count > 0);

            // read one
            // Test default of IncludeVariables=Full 
            AssemblyData myItem = await EndpointTests.AssertGetStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", HttpStatusCode.OK);

            Assert.IsNotNull(myItem);
            Assert.IsNotNull(myItem.Variables);
            Assert.AreEqual(1, myItem.Variables.Count);

            // read one
            // Test excluding Layouts
            myItem = await EndpointTests.AssertGetStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}?IncludeVariables=None", HttpStatusCode.OK);

            Assert.IsNotNull(myItem);
            Assert.IsNull(myItem.Variables);

            //UPDATE
            //update existing
            const string AssemblyUpdateTestString = "assembly_update_test";
            assemblyData.Description = AssemblyUpdateTestString;
            assemblyData.Variables.FirstOrDefault().Name = AssemblyUpdateTestString;
            var updatedAssembly = await EndpointTests.AssertPutStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", JsonConvert.SerializeObject(assemblyData), HttpStatusCode.OK);
            Assert.IsNotNull(updatedAssembly);
            Assert.AreEqual(AssemblyUpdateTestString, updatedAssembly.Description);
            Assert.IsNotNull(updatedAssembly.Variables.FirstOrDefault(x => x.Name == AssemblyUpdateTestString));

            //update to new element
            const string AddedVariableViaPUTName = "added_variable_via_PUT";
            updatedAssembly.Variables.Add(GetNewVariable(AddedVariableViaPUTName));
            updatedAssembly = await EndpointTests.AssertPutStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", JsonConvert.SerializeObject(updatedAssembly), HttpStatusCode.OK);
            Assert.IsNotNull(updatedAssembly.Variables.FirstOrDefault(y => y.Name == AddedVariableViaPUTName));
            Assert.AreEqual(testAssemblyID, updatedAssembly.Variables.FirstOrDefault(y => y.Name == AddedVariableViaPUTName).AssemblyID);

            //DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testAssemblyID}", HttpStatusCode.NoContent);
            Assert.IsFalse(ctx.AssemblyLayout.Any(x => x.AssemblyID == testAssemblyID));
            Assert.IsFalse(ctx.AssemblyElement.Any(x => x.AssemblyID == testAssemblyID));
            Assert.IsFalse(ctx.AssemblyVariable.Any(x => x.AssemblyID == testAssemblyID));
            Assert.IsFalse(ctx.AssemblyCategoryLink.Any(x => x.PartID == testAssemblyID));
        }

        [TestMethod]
        public async Task ComputeAssembly_Test()
        {
            var ctx = GetApiContext();
            if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
                await ctx.Database.MigrateAsync();

            AssemblyData machineTemplate = new AssemblyData()
            {
                BID = this.testBID,
                Name = DateTime.UtcNow + " TEST MACHINE TEMPLATE",
                AssemblyType = AssemblyType.MachineTemplate,
                MachineLayoutTypes = (MachineLayoutType)3,
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        BID = TestConstants.BID,
                        Name = "TestCheckboxVariable",
                        ElementType = AssemblyElementType.Checkbox,
                        DataType = DataType.Boolean,
                        Label = "label",
                        DefaultValue = null,
                        IsFormula = false,
                    }
                }
            };

            //CREATE
            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, apiUrl, JsonConvert.SerializeObject(machineTemplate), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);
            int testMachineTemplateID = assemblyData.ID;

            var request = new Dictionary<string, VariableValue>()
            {
                { "Item1", new VariableValue() { DataType = DataType.None, ValueOV = true, ValueAsDecimal = 5m, IsIncluded = true } },
                { "Item2", new VariableValue() { DataType = DataType.None, ValueOV = true, ValueAsDecimal = 15m, IsIncluded = true } },
            };

            Dictionary<string, VariableValue> result = await EndpointTests.AssertPostStatusCode<Dictionary<string, VariableValue>>(client, $"{apiUrl}/{testMachineTemplateID}/compute", request, HttpStatusCode.OK);
            Assert.IsNotNull(result);

            //DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testMachineTemplateID}", HttpStatusCode.NoContent);
            Assert.IsFalse(ctx.AssemblyLayout.Any(x => x.AssemblyID == testMachineTemplateID));
            Assert.IsFalse(ctx.AssemblyElement.Any(x => x.AssemblyID == testMachineTemplateID));
            Assert.IsFalse(ctx.AssemblyVariable.Any(x => x.AssemblyID == testMachineTemplateID));
            Assert.IsFalse(ctx.AssemblyCategoryLink.Any(x => x.PartID == testMachineTemplateID));
        }

        private AssemblyVariable GetNewVariable(string name = null, string defaultValue = null)
        {
            return new AssemblyVariable()
            {
                BID = TestConstants.BID,
                Name = name ?? NewTestAssemblyVariableName,
                ElementType = AssemblyElementType.Checkbox,
                DataType = DataType.Boolean,
                Label = "label",
                DefaultValue = defaultValue,
                IsFormula = (defaultValue != null)
            };
        }

        [TestMethod]
        public async Task AssemblyWithLinkedVariables()
        {
            var ctx = GetApiContext();
            if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
                await ctx.Database.MigrateAsync();

            AssemblyData layoutTest = GetNewModel();
            AssemblyVariable laborVariable = GetNewVariable();
            laborVariable.DefaultValue = "Test";
            laborVariable.IsFormula = false;
            laborVariable.DataType = DataType.LaborPart;
            laborVariable.LinkedLaborID = ctx.LaborData.FirstOrDefault().ID;
            laborVariable.ElementType = AssemblyElementType.LinkedLabor;
            laborVariable.IsConsumptionFormula = true;
            laborVariable.ConsumptionDefaultValue = "1";
            layoutTest.Variables = new List<AssemblyVariable>()
            {
                laborVariable
            };

            //CREATE
            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, apiUrl, JsonConvert.SerializeObject(layoutTest), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);
            int testAssemblyID = assemblyData.ID;

            //READ
            //read all
            List<AssemblyData> list = await EndpointTests.AssertGetStatusCode<List<AssemblyData>>(client, apiUrl, HttpStatusCode.OK);
            Assert.IsTrue(list.Count > 0);

            // read one
            // Test default of IncludeVariables=Full 
            AssemblyData myItem = await EndpointTests.AssertGetStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", HttpStatusCode.OK);

            Assert.IsNotNull(myItem);
            Assert.IsNotNull(myItem.Variables);
            Assert.AreEqual(layoutTest.Variables.Count, myItem.Variables.Count);
            // Assert LaborVariable was saved
            AssemblyVariable savedLaborVariable = layoutTest.Variables.First();
            Assert.AreEqual(laborVariable.DefaultValue, savedLaborVariable.DefaultValue);
            Assert.AreEqual(laborVariable.IsFormula, savedLaborVariable.IsFormula);
            Assert.AreEqual(laborVariable.DataType, savedLaborVariable.DataType);
            Assert.AreEqual(laborVariable.LinkedLaborID, savedLaborVariable.LinkedLaborID);
            Assert.AreEqual(laborVariable.IsConsumptionFormula, savedLaborVariable.IsConsumptionFormula);
            Assert.AreEqual(laborVariable.ConsumptionDefaultValue, savedLaborVariable.ConsumptionDefaultValue);

            // read one
            // Test excluding Layouts
            myItem = await EndpointTests.AssertGetStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}?IncludeVariables=None", HttpStatusCode.OK);

            Assert.IsNotNull(myItem);
            Assert.IsNull(myItem.Variables);

            //UPDATE
            //update existing
            const string AssemblyUpdateTestString = "assembly_update_test";
            assemblyData.Description = AssemblyUpdateTestString;
            assemblyData.Variables.FirstOrDefault().Name = AssemblyUpdateTestString;
            var updatedAssembly = await EndpointTests.AssertPutStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", JsonConvert.SerializeObject(assemblyData), HttpStatusCode.OK);
            Assert.IsNotNull(updatedAssembly);
            Assert.AreEqual(AssemblyUpdateTestString, updatedAssembly.Description);
            Assert.IsNotNull(updatedAssembly.Variables.FirstOrDefault(x => x.Name == AssemblyUpdateTestString));

            //update to new element with MaterialVariable
            const string AddedVariableViaPUTName = "added_variable_via_PUT";
            AssemblyVariable materialVariable = GetNewVariable(AddedVariableViaPUTName);
            materialVariable.DefaultValue = "Test";
            materialVariable.IsFormula = false;
            materialVariable.DataType = DataType.MaterialPart;
            materialVariable.LinkedMaterialID = ctx.MaterialData.FirstOrDefault().ID;
            materialVariable.ElementType = AssemblyElementType.LinkedMaterial;
            materialVariable.IsConsumptionFormula = false;
            materialVariable.ConsumptionDefaultValue = "1";
            updatedAssembly.Variables.Add(materialVariable);
            updatedAssembly = await EndpointTests.AssertPutStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", JsonConvert.SerializeObject(updatedAssembly), HttpStatusCode.OK);
            var savedMaterialVariable = updatedAssembly.Variables.FirstOrDefault(y => y.Name == AddedVariableViaPUTName);
            Assert.IsNotNull(savedMaterialVariable);
            Assert.AreEqual(testAssemblyID, savedMaterialVariable.AssemblyID);
            Assert.AreEqual(materialVariable.DefaultValue, savedMaterialVariable.DefaultValue);
            Assert.AreEqual(materialVariable.IsFormula, savedMaterialVariable.IsFormula);
            Assert.AreEqual(materialVariable.DataType, savedMaterialVariable.DataType);
            Assert.AreEqual(materialVariable.LinkedLaborID, savedMaterialVariable.LinkedLaborID);
            Assert.AreEqual(materialVariable.IsConsumptionFormula, savedMaterialVariable.IsConsumptionFormula);
            Assert.AreEqual(materialVariable.ConsumptionDefaultValue, savedMaterialVariable.ConsumptionDefaultValue);

            //DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testAssemblyID}", HttpStatusCode.NoContent);
            Assert.IsFalse(ctx.AssemblyLayout.Any(x => x.AssemblyID == testAssemblyID));
            Assert.IsFalse(ctx.AssemblyElement.Any(x => x.AssemblyID == testAssemblyID));
            Assert.IsFalse(ctx.AssemblyVariable.Any(x => x.AssemblyID == testAssemblyID));
            Assert.IsFalse(ctx.AssemblyCategoryLink.Any(x => x.PartID == testAssemblyID));
        }

        [TestMethod]
        public async Task AssemblyWithTablesTest()
        {
            var ctx = GetApiContext();
            if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
                await ctx.Database.MigrateAsync();

            AssemblyData model = GetNewModel();
            model.Tables = new List<AssemblyTable>()
            {
                GetNewTable()
            };

            //CREATE
            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, apiUrl, JsonConvert.SerializeObject(model), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);
            int testAssemblyID = assemblyData.ID;

            //READ
            //read all
            List<AssemblyData> list = await EndpointTests.AssertGetStatusCode<List<AssemblyData>>(client, apiUrl, HttpStatusCode.OK);
            Assert.IsTrue(list.Count > 0);

            // read one
            // Test default of IncludeTables=None
            AssemblyData myItem = await EndpointTests.AssertGetStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", HttpStatusCode.OK);

            Assert.IsNotNull(myItem);
            Assert.IsNull(myItem.Tables);

            // read one
            // Test IncludeTables=Full
            myItem = await EndpointTests.AssertGetStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}?IncludeTables=Full", HttpStatusCode.OK);

            Assert.IsNotNull(myItem);
            Assert.IsNotNull(myItem.Tables);
            Assert.AreEqual(1, myItem.Tables.Count);

            //UPDATE
            //update existing
            const string AssemblyUpdateTestString = "assembly_update_test";
            assemblyData.Description = AssemblyUpdateTestString;
            assemblyData.Tables.FirstOrDefault().Label = AssemblyUpdateTestString;
            var updatedAssembly = await EndpointTests.AssertPutStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", JsonConvert.SerializeObject(assemblyData), HttpStatusCode.OK);
            Assert.IsNotNull(updatedAssembly);
            Assert.AreEqual(AssemblyUpdateTestString, updatedAssembly.Description);
            Assert.IsNotNull(updatedAssembly.Tables.FirstOrDefault(x => x.Label == AssemblyUpdateTestString));

            //update to add new table
            const string AddedTableViaPUTName = "added_variable_via_PUT";
            updatedAssembly.Tables.Add(GetNewTable(AddedTableViaPUTName));
            updatedAssembly = await EndpointTests.AssertPutStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", JsonConvert.SerializeObject(updatedAssembly), HttpStatusCode.OK);
            Assert.IsNotNull(updatedAssembly.Tables.FirstOrDefault(y => y.Label == AddedTableViaPUTName));
            Assert.AreEqual(testAssemblyID, updatedAssembly.Tables.FirstOrDefault(y => y.Label == AddedTableViaPUTName).AssemblyID);

            //update to remove all tables
            updatedAssembly.Tables = null;
            updatedAssembly = await EndpointTests.AssertPutStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", JsonConvert.SerializeObject(updatedAssembly), HttpStatusCode.OK);
            Assert.AreEqual(null, updatedAssembly.Tables);

            //DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testAssemblyID}", HttpStatusCode.NoContent);
            Assert.IsFalse(ctx.AssemblyLayout.Any(x => x.AssemblyID == testAssemblyID));
            Assert.IsFalse(ctx.AssemblyElement.Any(x => x.AssemblyID == testAssemblyID));
            Assert.IsFalse(ctx.AssemblyVariable.Any(x => x.AssemblyID == testAssemblyID));
            Assert.IsFalse(ctx.AssemblyTable.Any(x => x.AssemblyID == testAssemblyID));
            Assert.IsFalse(ctx.AssemblyCategoryLink.Any(x => x.PartID == testAssemblyID));
        }

        [TestMethod]
        public async Task AssemblyGeneratorTestCompilationReturnsFailure()
        {
            string varName = "Test";
            string varFxText = "=2 * Blah.Value";
            AssemblyData model = GetNewModel();
            model.Variables = new List<AssemblyVariable>()
            {
                GetNewVariable(varName, varFxText)
            };

            //VALIDATE
            AssemblyCompilationResponse resp = await EndpointTests.AssertPostStatusCode<AssemblyCompilationResponse>(client, $"{apiUrl}/Action/Validate", JsonConvert.SerializeObject(model), HttpStatusCode.BadRequest);

            Assert.IsFalse(resp.Success);
            Assert.IsNotNull(resp.Errors);
            Assert.AreEqual(1, resp.Errors.Count);
            Assert.AreEqual(varName, resp.Errors[0].VariableName);
            Assert.AreEqual(varFxText, resp.Errors[0].FormulaText);
            Assert.IsFalse(resp.Errors[0].IsValidation);
            Assert.AreEqual("The name 'Blah' does not exist in the current context", resp.Errors[0].Message);
            Assert.AreEqual("CS0103", resp.Errors[0].ReferenceID);
        }

        [TestMethod]
        public async Task SaveAssemblyDataReturnsFailure()
        {
            string varName = "<Name>";
            string varFxText = "=2 * Blah";
            AssemblyData model = GetNewModel();
            var variable = GetNewVariable(varName, varFxText);

            model.Variables = new List<AssemblyVariable>()
            {
                variable
            };


            var resp = await EndpointTests.AssertPostStatusCode<ApiValidationErrorBody>(client, $"{apiUrl}/", JsonConvert.SerializeObject(model), HttpStatusCode.BadRequest);
            Assert.IsNotNull(resp);
            Assert.IsTrue(resp.ErrorMessage.Contains("Variable has an invalid name: <Name>"), "Error message did not contain 'Variable has an invalid name: <Name>'");
        }

        [TestMethod]
        public async Task AssemblyGeneratorTestCompilationReturnsNoError()
        {
            AssemblyData model = GetNewModel();
            model.Variables = new List<AssemblyVariable>()
            {
                GetNewVariable("Test")
            };

            //VALIDATE
            AssemblyCompilationResponse result = (await EndpointTests.AssertPostStatusCode<AssemblyCompilationResponse>(client, $"{apiUrl}/Action/Validate", JsonConvert.SerializeObject(model), HttpStatusCode.OK));
            Assert.IsTrue(result.Success);

        }


        [TestMethod]
        public async Task AssemblyPOSTFailsWithInvalidPricingType()
        {
            var ctx = GetApiContext();

            AssemblyData testAssemblyData = GetNewModel();
            testAssemblyData.Variables = new List<AssemblyVariable>()
            {
                GetNewVariable()
            };
            testAssemblyData.PricingType = (AssemblyPricingType)10; //10 will do for making it invalid, as it's not defined yet in AssemblyPricingType enum.
            //CREATE
            var validationError = await EndpointTests.AssertPostStatusCode<ApiValidationErrorBody>(client, apiUrl, JsonConvert.SerializeObject(testAssemblyData), HttpStatusCode.BadRequest);
            Assert.IsNotNull(validationError);
            Assert.IsTrue(validationError.ErrorMessage.Contains("Pricing Type"), "Error Message did not contain 'Pricing Type'");
        }

        private AssemblyTable GetNewTable(string name = null)
        {
            return new AssemblyTable()
            {
                BID = TestConstants.BID,
                Label = name ?? "Test.AssemblyTable",
                TableType = AssemblyTableType.Custom,
                CellDataJSON = "[]",
                VariableName = name ?? "TestAssemblyTable",
                CellDataType = DataType.Number,
                ColumnCount = 1,
                ColumnDataType = DataType.Number,
                ColumnIsSorted = false,
                ColumnLabel = "Tier",
                ColumnMatchType = Models.AssemblyTableMatchType.ExactMatch,
                ColumnUnitID = 0,
                ColumnValuesJSON = "[{\"index\":0,\"value\":1,\"name\":\"Default\"}]",
                Description = "",
                ModifiedDT = DateTime.Parse("2019-04-24T16:07:46.7515121"),
                RowCount = 3,
                RowDataType = DataType.Number,
                RowDefaultMarkupJSON = "",
                RowIsSorted = false,
                RowLabel = "Row",
                RowMatchType = 0,
                RowUnitID = 0,
                RowValuesJSON = "[{\"index\":0,\"value\":100}]",
                IsTierTable = true,
                ClassTypeID = 12047
            };
        }

        [TestMethod]
        public async Task TestAssemblyFilterEndpoint()
        {
            var ctx = GetApiContext();
            ctx.AssemblyData.RemoveRange(ctx.AssemblyData.Where(x => x.ID == -200));
            ctx.SaveChanges();
            var glAccountID = ctx.GLAccount.FirstOrDefault(t => t.BID == 1).ID;

            var assembly = new AssemblyData()
            {
                ID = -200,
                BID = 1,
                Name = "Test Assembly For Pricing",
                IncomeAccountID = glAccountID,
                AssemblyType = AssemblyType.Embedded
            };

            ctx.AssemblyData.Add(assembly);
            ctx.SaveChanges();
            var endpointWithFilters = $"{apiUrl}?AssemblyType=0";
            var resp = await EndpointTests.AssertGetStatusCode<AssemblyData[]>(client, endpointWithFilters, HttpStatusCode.OK);
            Assert.AreEqual(0, resp.Where(x => x.ID == -200).Count());
            endpointWithFilters = $"{apiUrl}?AssemblyType=1";
            resp = await EndpointTests.AssertGetStatusCode<AssemblyData[]>(client, endpointWithFilters, HttpStatusCode.OK);
            Assert.AreEqual(1, resp.Where(x => x.ID == -200).Count());

            endpointWithFilters = $"{apiUrl}/SimpleList?AssemblyType=0";
            var resp2 = await EndpointTests.AssertGetStatusCode<SimpleAssemblyData[]>(client, endpointWithFilters, HttpStatusCode.OK);
            Assert.AreEqual(0, resp2.Where(x => x.ID == -200).Count());
            endpointWithFilters = $"{apiUrl}/SimpleList?AssemblyType=1";
            resp2 = await EndpointTests.AssertGetStatusCode<SimpleAssemblyData[]>(client, endpointWithFilters, HttpStatusCode.OK);
            Assert.AreEqual(1, resp2.Where(x => x.ID == -200).Count());

            ctx.AssemblyData.RemoveRange(ctx.AssemblyData.Where(x => x.ID == -200));
            ctx.SaveChanges();

        }

        [TestCleanup]
        public async Task Teardown()
        {
            System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}");
            string responseString = await response.Content.ReadAsStringAsync();
            AssemblyData[] result = JsonConvert.DeserializeObject<AssemblyData[]>(responseString);
            if (result != null)
            {
                result.Where(x => x.Name.Contains(NewTestAssemblyName)).ToList().ForEach(async (assembly) =>
                {
                    if (assembly != null)
                    {
                        await client.DeleteAsync($"{apiUrl}/{assembly.ID}");
                    }
                });
            }

        }

        [TestMethod]
        public async Task AssemblyWithVariableFormulaTest()
        {
            var ctx = GetApiContext();
            if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
                await ctx.Database.MigrateAsync();

            string defaultValue = null;

            AssemblyData layoutTest = GetNewModel();
            layoutTest.Variables = new List<AssemblyVariable>()
            {
                new AssemblyVariable()
                {
                    BID = TestConstants.BID,
                    Name = NewTestAssemblyVariableName,
                    ElementType = AssemblyElementType.Checkbox,
                    DataType = DataType.Boolean,
                    Label = "label",
                    DefaultValue = defaultValue,
                    IsFormula = (defaultValue != null),
                    ID = -99,
                    Formulas = new List<AssemblyVariableFormula>()
                    {
                        new AssemblyVariableFormula()
                        {
                            BID = TestConstants.BID,
                            FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                            FormulaUseType = AssemblyFormulaUseType.DefaultValue,
                            VariableID = -99,
                            ID = -99,
                            ChildVariableName = "Child Variable Name_1"
                        }
                    }
                }
            };


            var serializedData = JsonConvert.SerializeObject(layoutTest);

            //CREATE
            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, apiUrl, JsonConvert.SerializeObject(layoutTest), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);
            int testAssemblyID = assemblyData.ID;

            //READ ALL
            List<AssemblyData> list = await EndpointTests.AssertGetStatusCode<List<AssemblyData>>(client, apiUrl, HttpStatusCode.OK);
            Assert.IsTrue(list.Count > 0);

            // READ ONE
            // Test default of IncludeVariables=Full 
            AssemblyData myItem = await EndpointTests.AssertGetStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", HttpStatusCode.OK);

            Assert.IsNotNull(myItem);
            Assert.IsNotNull(myItem.Variables);
            Assert.AreEqual(1, myItem.Variables.Count);

            //UPDATE
            const string AssemblyUpdateTestString = "assembly_variable_formula_update_test";
            var formula = assemblyData.Variables.FirstOrDefault().Formulas.FirstOrDefault();
            
            formula.ChildVariableName = AssemblyUpdateTestString;

            var updatedAssembly = await EndpointTests.AssertPutStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", JsonConvert.SerializeObject(assemblyData), HttpStatusCode.OK);

            Assert.IsNotNull(updatedAssembly);
            Assert.IsNotNull(updatedAssembly.Variables.FirstOrDefault().Formulas.FirstOrDefault(x => x.ChildVariableName == AssemblyUpdateTestString));

            //DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testAssemblyID}", HttpStatusCode.NoContent);
            Assert.IsFalse(ctx.AssemblyVariable.Any(x => x.AssemblyID == testAssemblyID));
            Assert.IsFalse(ctx.AssemblyVariableFormula.Any(x => x.VariableID == myItem.Variables.FirstOrDefault().ID));
        }

        [TestMethod]
        public async Task GetMemberListTest()
        {
            /// API / AssemblyPart /{ ID}/ memberlist
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/memberlist", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/9999/memberlist", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/1000/memberlist", HttpStatusCode.OK);

        }

        [TestMethod]
        public async Task TestChildAssemblyFormulas()
        {
            short bid = 1;
            int parentID = -99;
            int companyID = -99;
            decimal parentQuantity = 500m;
            string parentName = "ParentAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            int childID = -98;
            string childName = "ChildAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            int currentVariableID = -99;
            int currentFormulaID = -99;


            var ctx = GetApiContext();

            ITenantDataCache cache = this.GetMockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            // This will try to delete all assemblydata where ID < -1 but doesn't account for any FK dependencies
            var assembliesToDelete = ctx.AssemblyData.Where(x => x.ID < -1)
                    .Include(t => t.Variables)
                    .ThenInclude(t => t.Formulas);
            ctx.RemoveRange(assembliesToDelete);
            ctx.RemoveRange(assembliesToDelete.SelectMany(t=>t.Variables));
            ctx.RemoveRange(assembliesToDelete.SelectMany(t=>t.Variables.SelectMany(y=>y.Formulas)));

            ctx.SaveChanges();
            var glAccountID = ctx.GLAccount.FirstOrDefault(t => t.BID == 1).ID;

            AssemblyVariable childAssemblyHeight = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "8",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyWidth = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "9",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=Quantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData childAssembly = new AssemblyData()
            {
                BID = bid,
                ID = childID,
                Name = childName,
                Variables = new List<AssemblyVariable>()
                {
                    childAssemblyHeight,
                    childAssemblyWidth,
                    childAssemblyPrice
                }
            };

            AssemblyVariable parentAssemblyHeight = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyWidth = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyChildAssembly = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = childName,
                Label = "Child Assembly",
                DataType = DataType.Assembly,
                DefaultValue = childName,
                ElementType = AssemblyElementType.LinkedAssembly,
                LinkedAssemblyID = childID,
                ConsumptionDefaultValue = "=Quantity.Value * 10 + 100",
                IsConsumptionFormula = true,
            };

            AssemblyVariableFormula parentAssemblyQuantityFormula = new AssemblyVariableFormula()
            {
                ID = currentFormulaID--,
                BID = bid,
                ChildVariableName = "Quantity",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                FormulaText = "Quantity",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyHeightFormula = new AssemblyVariableFormula()
            {
                ID = currentFormulaID--,
                BID = bid,
                ChildVariableName = "Height",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                FormulaText = "=ROUND(Height.Value/2, 1)",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = true,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyWidthFormula = new AssemblyVariableFormula()
            {
                ID = currentFormulaID--,
                BID = bid,
                ChildVariableName = "Width",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                FormulaText = "=ROUND(Width.Value/2, 1)",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = true,
                VariableID = parentAssemblyChildAssembly.ID
            };

            parentAssemblyChildAssembly.Formulas = new List<AssemblyVariableFormula>()
            {
                parentAssemblyQuantityFormula,
                parentAssemblyHeightFormula,
                parentAssemblyWidthFormula
            };

            AssemblyVariable parentAssemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=Quantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };


            AssemblyData parentAssembly = new AssemblyData()
            {
                BID = 1,
                ID = parentID,
                Name = parentName,
                Variables = new List<AssemblyVariable>()
                {
                    parentAssemblyHeight,
                    parentAssemblyWidth,
                    parentAssemblyChildAssembly,
                    parentAssemblyPrice
                }
            };

            ctx.AssemblyData.Add(parentAssembly);
            ctx.AssemblyData.Add(childAssembly);
            ctx.SaveChanges();

            try
            {

                CBELAssemblyGenerator parentGen = CBELAssemblyGenerator.Create(ctx, parentAssembly);
                ICBELAssembly cbelParentAssembly = await parentGen.GetAssemblyInstance(ctx, cache, logger);

                Assert.IsNotNull(cbelParentAssembly);

                CBELAssemblyGenerator childGen = CBELAssemblyGenerator.Create(ctx, childAssembly);
                AssemblyCompilationResponse compileTestResult = await childGen.Save(cache);
                Assert.IsTrue(compileTestResult.Success);

                CBELOverriddenValues ovValues = new CBELOverriddenValues()
                {
                    ComponentID = parentID,
                    Quantity = 1m,
                    QuantityOV = true,
                    CompanyID = companyID,
                    ParentQuantity = parentQuantity,
                    IsVended = true,
                    VariableData = new List<IVariableData>()
                    {
                        new VariableData()
                        {
                            VariableName = "Height",
                            IsOV = true,
                            Value = "2",
                        },
                        new VariableData()
                        {
                            VariableName = "Width",
                            IsOV = true,
                            Value = "16",
                        }
                    },
                    OVChildValues = new List<ICBELOverriddenValues>()
                    {
                        new CBELOverriddenValues()
                        {
                            ComponentID = childID,
                            VariableName = childName
                        }
                    }
                };
                ICBELComputeResult result = cbelParentAssembly.Compute(ovValues);
                Assert.IsNotNull(result);
                var myAssembly = (ICBELAssembly)result.Assembly;
                Assert.IsNotNull(myAssembly);
                Assert.IsNotNull(myAssembly.Components);
                Assert.IsTrue(myAssembly.Components.Count > 0);
                var subAssemblies = myAssembly.Components.Where(c => c.ComponentClassType == OrderItemComponentType.Assembly).FirstOrDefault();
                Assert.IsNotNull(subAssemblies);
                Assert.IsNotNull(subAssemblies.Formulas);
                Assert.AreEqual(3, subAssemblies.Formulas.Count);
            }
            finally
            {

                ctx.AssemblyVariableFormula.RemoveRange(ctx.AssemblyVariableFormula.Where(x => x.ID == childAssemblyHeight.ID));
                ctx.AssemblyVariableFormula.RemoveRange(ctx.AssemblyVariableFormula.Where(x => x.ID == childAssemblyWidth.ID));
                ctx.AssemblyVariableFormula.RemoveRange(ctx.AssemblyVariableFormula.Where(x => x.ID == childAssemblyPrice.ID));
                ctx.SaveChanges();

                ctx.AssemblyVariable.RemoveRange(ctx.AssemblyVariable.Where(x => x.ID == childAssemblyHeight.ID));
                ctx.AssemblyVariable.RemoveRange(ctx.AssemblyVariable.Where(x => x.ID == childAssemblyWidth.ID));
                ctx.AssemblyVariable.RemoveRange(ctx.AssemblyVariable.Where(x => x.ID == childAssemblyPrice.ID));
                ctx.AssemblyVariable.RemoveRange(ctx.AssemblyVariable.Where(x => x.ID == parentAssemblyHeight.ID));
                ctx.AssemblyVariable.RemoveRange(ctx.AssemblyVariable.Where(x => x.ID == parentAssemblyWidth.ID));
                ctx.AssemblyVariable.RemoveRange(ctx.AssemblyVariable.Where(x => x.ID == parentAssemblyChildAssembly.ID));
                ctx.AssemblyVariable.RemoveRange(ctx.AssemblyVariable.Where(x => x.ID == parentAssemblyPrice.ID));
                ctx.SaveChanges();

                ctx.AssemblyData.RemoveRange(ctx.AssemblyData.Where(x => x.ID == childAssembly.ID));
                ctx.SaveChanges();
                ctx.AssemblyData.RemoveRange(ctx.AssemblyData.Where(x => x.ID == parentAssembly.ID));
                ctx.SaveChanges();
            }

            //ctx.AssemblyData.RemoveRange(ctx.AssemblyData.Where(x => x.ID == -1));
        }

        [TestMethod]
        public async Task AssemblyFailsWithEmptyFormula()
        {
            var ctx = GetApiContext();
            if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
                await ctx.Database.MigrateAsync();
            
            AssemblyData testAssemblyData = GetNewModel();
            testAssemblyData.Variables = new List<AssemblyVariable>()
            {
                new AssemblyVariable()
                {
                    BID = TestConstants.BID,
                    Name = NewTestAssemblyVariableName,
                    ElementType = AssemblyElementType.Number,
                    DataType = DataType.Number,
                    DefaultValue = null,
                    IsFormula = true
                }
            };

            //POST test
            var validationError = await EndpointTests.AssertPostStatusCode<ApiValidationErrorBody>(client, apiUrl, JsonConvert.SerializeObject(testAssemblyData), HttpStatusCode.BadRequest);
            Assert.IsNotNull(validationError);
            Assert.IsTrue(validationError.ErrorMessage.Contains("Formula is empty"), "Error Message did not contain 'Formula is empty'");

            // PUT test
            AssemblyData testAssembly = GetNewModel();
            testAssembly.Variables = new List<AssemblyVariable>()
            {
                new AssemblyVariable()
                {
                    BID = TestConstants.BID,
                    Name = NewTestAssemblyVariableName,
                    ElementType = AssemblyElementType.Number,
                    DataType = DataType.Number,
                }
            };

            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, apiUrl, JsonConvert.SerializeObject(testAssembly), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);
            int testAssemblyID = assemblyData.ID;
            try
            {
                AssemblyData assembly = await EndpointTests.AssertGetStatusCode<AssemblyData>(client, $"{apiUrl}/{testAssemblyID}", HttpStatusCode.OK);
                Assert.IsNotNull(assembly);
                Assert.IsNotNull(assembly.Variables);
                Assert.AreEqual(1,assembly.Variables.Count());
                AssemblyVariable variable = assembly.Variables.FirstOrDefault();

                //test null
                variable.IsFormula = true;
                variable.DefaultValue = null;

                validationError = await EndpointTests.AssertPutStatusCode<ApiValidationErrorBody>(client, $"{apiUrl}/{testAssemblyID}", JsonConvert.SerializeObject(assembly), HttpStatusCode.BadRequest);
                Assert.IsNotNull(validationError);
                Assert.IsTrue(validationError.ErrorMessage.Contains("Formula is empty"), "Error Message did not contain 'Formula is empty'");

                //test whitespace
                variable.IsFormula = true;
                variable.DefaultValue = " ";
                validationError = await EndpointTests.AssertPutStatusCode<ApiValidationErrorBody>(client, $"{apiUrl}/{testAssemblyID}", JsonConvert.SerializeObject(assembly), HttpStatusCode.BadRequest);
                Assert.IsNotNull(validationError);
                Assert.IsTrue(validationError.ErrorMessage.Contains("Formula is empty"), "Error Message did not contain 'Formula is empty'");

                //test formula character only
                variable.IsFormula = true;
                variable.DefaultValue = "=";
                validationError = await EndpointTests.AssertPutStatusCode<ApiValidationErrorBody>(client, $"{apiUrl}/{testAssemblyID}", JsonConvert.SerializeObject(assembly), HttpStatusCode.BadRequest);
                Assert.IsNotNull(validationError);
                Assert.IsTrue(validationError.ErrorMessage.Contains("Formula is empty"), "Error Message did not contain 'Formula is empty'");
            }
            finally
            {
                //Cleanup
                await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testAssemblyID}", HttpStatusCode.NoContent);
                Assert.IsFalse(ctx.AssemblyLayout.Any(x => x.AssemblyID == testAssemblyID));
                Assert.IsFalse(ctx.AssemblyElement.Any(x => x.AssemblyID == testAssemblyID));
                Assert.IsFalse(ctx.AssemblyVariable.Any(x => x.AssemblyID == testAssemblyID));
                Assert.IsFalse(ctx.AssemblyCategoryLink.Any(x => x.PartID == testAssemblyID));
            }
        }

        [TestMethod]
        public async Task CustomListByVariable()
        {
            var ctx = GetApiContext();
            MachineData testMachine = await GetNewMachine(DateTime.UtcNow + " TEST MACHINE DATA", -1);
            // Create 2 Machine Template
            AssemblyData machineTemplate1 = new AssemblyData()
            {
                BID = this.testBID,
                Name = DateTime.UtcNow + " TEST MACHINE TEMPLATE 1",
                AssemblyType = AssemblyType.MachineTemplate,
                MachineLayoutTypes = (MachineLayoutType)3,
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        BID = TestConstants.BID,
                        Name = "TestCheckboxVariable",
                        ElementType = AssemblyElementType.Checkbox,
                        DataType = DataType.Boolean,
                        Label = "label",
                        DefaultValue = null,
                        IsFormula = false,
                    }
                }
            };
            var newMachineTemplate1 = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, $"/api/assemblypart", JsonConvert.SerializeObject(machineTemplate1), HttpStatusCode.OK);
            Assert.IsNotNull(newMachineTemplate1);

            // test post
            testMachine.MachineTemplateID = newMachineTemplate1.ID;
            testMachine.Profiles.First().MachineTemplateID = newMachineTemplate1.ID;
            var machineData = await EndpointTests.AssertPostStatusCode<MachineData>(client, "/api/machinepart", JsonConvert.SerializeObject(testMachine), HttpStatusCode.OK);
            Assert.IsNotNull(machineData);

            AssemblyData model = new AssemblyData()
            {
                Name = "TEST_ASSEMBLY_DATA_" + DateTime.UtcNow,
                IsActive = true,
                BID = TestConstants.BID,
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        BID = TestConstants.BID,
                        Name = "TestVariable",
                        ElementType = AssemblyElementType.DropDown,
                        IsConsumptionFormula = true,
                        DataType = DataType.String,
                        ListDataType = DataType.String,
                        Label = "label",
                        DefaultValue = null,
                        ConsumptionDefaultValue = "1",
                        IsFormula = false,
                        ListValuesJSON = $"[{{\"Name\": \"one\"}},{{\"Name\": \"two\"}},{{\"Name\": \"three\"}}]"
                    }
                }
            };

            //CREATE
            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, "/api/assemblypart", JsonConvert.SerializeObject(model), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);
            var variableID = assemblyData.Variables.First().ID;
            var profileName = machineData.Profiles.First().Name;
            var simpleListCollection = await EndpointTests.AssertGetStatusCode<ICollection<NamedQuark>>(client, $"{apiUrl}/simplelist/byvariable/{variableID}?MachineID={machineData.ID}&ProfileName=\"{profileName}\"", HttpStatusCode.OK);
            Assert.IsNotNull(simpleListCollection);
            Assert.AreEqual(3, simpleListCollection.Count);
            //Assert.AreEqual("one", simpleListCollection.ToList()[0].Name);
            //Assert.AreEqual("two", simpleListCollection.ToList()[1].Name);
            //Assert.AreEqual("three", simpleListCollection.ToList()[2].Name);

            // test delete
            await EndpointTests.AssertGetStatusCode(client, $"/api/machinepart/{machineData.ID}/action/candelete", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/machinepart/{machineData.ID}", HttpStatusCode.NoContent);

            await EndpointTests.AssertDeleteStatusCode(client, $"/api/assemblypart/{assemblyData.ID}", HttpStatusCode.NoContent);
            Assert.IsFalse(ctx.AssemblyData.Any(x => x.ID == assemblyData.ID));
        }
 
        [TestMethod]
        public async Task TestValidationRuleforAssemblyDestinationPropertiesWithBooleanFlagsSetToTrue()
        {
            var ctx = GetApiContext(1);
            if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
                await ctx.Database.MigrateAsync();

            #region clean up data
            ctx.Database.ExecuteSqlRaw(@"
  delete 
  from [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Variable] 
  where ID in (  select V.ID
  from [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Variable] as V inner join 
       [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Data] as D on V.SubassemblyID = D.ID
  where D.[Name] like  '%TEST_ASSEMBLY_DATA%')

  delete 
  from  [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Data]
  where [Name] like '%TEST_ASSEMBLY_DATA%'");
            #endregion

            AssemblyData assemblyTest = GetNewModel();
            assemblyTest.EnableShipByDate = true;
            assemblyTest.EnableArriveByDate = true;
            assemblyTest.EnableFromAddress = true;
            assemblyTest.EnableToAddress = true;
            assemblyTest.EnableBlindShipping = true;
            assemblyTest.EnablePackagesTab = true;


            var serializedData = JsonConvert.SerializeObject(assemblyTest);

            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, apiUrl, JsonConvert.SerializeObject(assemblyTest), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);

            Assert.AreEqual("Ship By Date", assemblyData.ShipByDateLabel);
            Assert.AreEqual("Arrive By Date", assemblyData.ArriveByDateLabel);
            Assert.AreEqual("From Address", assemblyData.FromAddressLabel);
            Assert.AreEqual("To Address", assemblyData.ToAddressLabel);
            Assert.AreEqual("Blind Ship From Address", assemblyData.BlindShippingLabel);

            #region clean up data
            ctx.Database.ExecuteSqlRaw(@"
  delete 
  from [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Variable] 
  where ID in (  select V.ID
  from [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Variable] as V inner join 
       [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Data] as D on V.SubassemblyID = D.ID
  where D.[Name] like  '%TEST_ASSEMBLY_DATA%')

  delete 
  from  [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Data]
  where [Name] like '%TEST_ASSEMBLY_DATA%'");
            #endregion
        }

        [TestMethod]
        public async Task TestValidationRuleforAssemblyDestinationPropertiesWithBooleanFlagsSetToFalse()
        {
            var ctx = GetApiContext(1);
            if ((await ctx.Database.GetPendingMigrationsAsync()).Count() > 0)
                await ctx.Database.MigrateAsync();

            #region clean up data
            ctx.Database.ExecuteSqlRaw(@"
  delete 
  from [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Variable] 
  where ID in (  select V.ID
  from [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Variable] as V inner join 
       [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Data] as D on V.SubassemblyID = D.ID
  where D.[Name] like  '%TEST_ASSEMBLY_DATA%')

  delete 
  from  [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Data]
  where [Name] like '%TEST_ASSEMBLY_DATA%'");
            #endregion

            AssemblyData assemblyTest = GetNewModel();
            var serializedData = JsonConvert.SerializeObject(assemblyTest);

            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, apiUrl, JsonConvert.SerializeObject(assemblyTest), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);

            Assert.AreEqual(null, assemblyData.ShipByDateLabel);
            Assert.AreEqual(null, assemblyData.ArriveByDateLabel);
            Assert.AreEqual(null, assemblyData.FromAddressLabel);
            Assert.AreEqual(null, assemblyData.ToAddressLabel);
            Assert.AreEqual(null, assemblyData.BlindShippingLabel);

            #region clean up data
            ctx.Database.ExecuteSqlRaw(@"
  delete 
  from [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Variable] 
  where ID in (  select V.ID
  from [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Variable] as V inner join 
       [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Data] as D on V.SubassemblyID = D.ID
  where D.[Name] like  '%TEST_ASSEMBLY_DATA%')

  delete 
  from  [Dev.Endor.Business.DB1].[dbo].[Part.Subassembly.Data]
  where [Name] like '%TEST_ASSEMBLY_DATA%'");
            #endregion
        }
    }
}
