﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using AssertX = Xunit.Assert;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using Microsoft.AspNetCore.Mvc;
using Endor.Models;
using Endor.Api.Web.Controllers;
using Endor.Api.Web.Services;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Net;
using Endor.EF.Migrations;

namespace Endor.Api.Tests
{
    [TestClass]
    public class LocationControllerTests: CommonControllerTestClass
    {
        public const string apiUrl = "/Api/Location";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }
        public const short testBID = 1;

        public static IEnumerable<object[]> GetLocationCollectionPropertyNames()
        {
            yield return new object[] { (short)1,  unchecked((byte)-99), new List<string>() { "LocationLocators" } };
        }

        [DataTestMethod]
        [DynamicData(nameof(GetLocationCollectionPropertyNames), DynamicDataSourceType.Method)]
        public async Task LocationEndpointContract(short bid,  byte locationID, List<string> relationshipObjectNames)
        {
            CleanupTestLocation(bid, locationID);
            SetupTestIndustries(bid, locationID);
            await Utils.TestJSONRelationshipProperties(client, apiUrl, locationID, relationshipObjectNames);
            CleanupTestLocation(bid, locationID);
        }

        private void CleanupTestLocation(short bid, byte locationID)
        {
            var ctx = GetApiContext(bid);
            var location = ctx.LocationData.FirstOrDefault(t => t.BID == bid && t.ID == locationID);
            if (location != null)
            {
                ctx.LocationData.Remove(location);
                ctx.SaveChanges();
            }
        }

        private void SetupTestIndustries(short bid, byte locationID)
        {
            var ctx = GetApiContext(bid);
            var location = new LocationData()
            {
                BID = bid,
                ID = locationID,
                Name = $"testLocation{locationID}"
            };

            ctx.LocationData.Add(location);
            ctx.SaveChanges();
        }

        [TestMethod]
        public async Task TestClone()
        {
            var ctx = GetApiContext();
            var locationID = ctx.LocationData.Where(loc => loc.BID == testBID).FirstOrDefault().ID;
            string newName = "CLONE UNIT TEST";

            var resp = await EndpointTests.AssertPostStatusCode<LocationData>(client, $"{apiUrl}/{locationID}/Clone", HttpStatusCode.OK);
            Assert.IsNotNull(resp);
            Assert.AreNotEqual(newName, resp.Name);

            resp = await EndpointTests.AssertPostStatusCode<LocationData>(client, $"{apiUrl}/{locationID}/Clone/?NewName={newName}", HttpStatusCode.OK);
            Assert.IsNotNull(resp);
            Assert.AreEqual(newName, resp.Name);
        }
    }
}
