﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Request;
using Endor.Api.Web.Classes.Responses;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    [TestCategory("GL")]
    [TestCategory("Payment")]
    public class PaymentPaymentTests : PaymentTestHelper
    {

        private const string apiUrl = "/api/payment/payment";
        public TestContext TestContext { get; set; }


        [TestInitialize]
        public async Task Initialize()
        {
            await CleanUpTestData();
        }

        [TestCleanup]
        public async Task Teardown()
        {
            await CleanUpTestData();
        }

        private PaymentRequest GetPaymentRequest(int companyID, decimal amount)
        {
            var ctx = GetApiContext();
            var company = ctx.CompanyData.First(t => t.ID == companyID);
            return new PaymentRequest()
            {
                LocationID = company.LocationID,
                CompanyID = company.ID,
                Amount = amount,
                PaymentTransactionType = (int)PaymentTransactionType.Payment,
                ReceivedLocationID = company.LocationID,
                PaymentMethodID = 1,
                PaymentType = (int)PaymentMethodType.Cash,
                Applications = new List<PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest
                    {
                        OrderID = null,
                        Amount = amount
                    }
                }
            };
        }

        [TestMethod]
        public async Task PaymentShouldCreateAMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var request = GetPaymentRequest(
                companyID: company.ID, 
                amount: 70
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(_client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            var master = ctx.PaymentMaster.FirstOrDefault(t => t.CompanyID == company.ID && t.PaymentTransactionType == (byte)PaymentTransactionType.Payment);
            Assert.IsNotNull(master);
            Assert.AreEqual(request.Amount, master.RefBalance);
        }

        [TestMethod]
        public async Task PaymentShouldCreateAnApplication()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var request = GetPaymentRequest(
                companyID: company.ID, 
                amount: 70
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(_client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            var application = ctx.PaymentApplication.FirstOrDefault(t => t.CompanyID == company.ID && t.PaymentTransactionType == (byte)PaymentTransactionType.Payment);
            Assert.IsNotNull(application);
            Assert.AreEqual(request.Amount, application.RefBalance);
            Assert.AreEqual(request.Amount, application.Amount);
        }




        [TestMethod]
        public async Task PaymentShouldUpdateCompanysBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var request = GetPaymentRequest(
                companyID: company.ID, 
                amount: 70
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(_client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            ctx.Entry(company).Reload();
            Assert.AreEqual(request.Amount, company.RefundableCredit);
        }
    }
}
