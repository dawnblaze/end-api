﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    [TestClass]
    public class AssemblyCategoryEndpointTests
    {
        public const string apiUrl = "/api/assemblycategory";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestAssemblyDataName;
        private string NewTestAssemblyCategoryName;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            this.NewTestAssemblyDataName = DateTime.UtcNow + " TEST PART ASSEMBLY DATA";
            this.NewTestAssemblyCategoryName = DateTime.UtcNow + " TEST PART ASSEMBLY DATA";
        }

        [TestMethod]
        public async Task TestAssemblyCategorySingleEndpoints()
        {
            AssemblyCategory testObject = await GetPostAssertNewAssemblyCategory();

            // GET api/PaymentTerms/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleAssemblyCategory[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleAssemblyCategory sga in simpleList)
            {
                if (sga.DisplayName == testObject.Name)
                {
                    testObject.ID = sga.ID;
                }
            }

            testObject.Description = "test test";

            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + testObject.ID, JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            //test: expect OK
            var updatedAssemblyCategory = await EndpointTests.AssertGetStatusCode<AssemblyCategory>(client, $"{apiUrl}/" + testObject.ID, HttpStatusCode.OK);
            Assert.AreEqual(updatedAssemblyCategory.Description, testObject.Description);

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testObject.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestAssemblyCategoryFilters()
        {
            //setup: get new assembly category
            AssemblyCategory testObject = await GetPostAssertNewAssemblyCategory();

            //test: make sure it exists when filtering simple list for isactive=true
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleAssemblyCategory[]>(client, $"{apiUrl}/SimpleList?isActive=true", HttpStatusCode.OK);
            Assert.IsNotNull(simpleList.FirstOrDefault(x => x.ID == testObject.ID));

            //test: expect only active categories
            var active = await EndpointTests.AssertGetStatusCode<AssemblyCategory[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(active.All(x => x.IsActive));
            Assert.IsTrue(active.Count() > 0);

            //test: set isactive false
            testObject.IsActive = false;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + testObject.ID, JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            //test: expect only inactive categories
            var inactive = await EndpointTests.AssertGetStatusCode<AssemblyCategory[]>(client, $"{apiUrl}?IsActive=false", HttpStatusCode.OK);
            Assert.IsTrue(inactive.All(x => !x.IsActive));
            Assert.IsTrue(inactive.Count() > 0);

            //setup: link child
            AssemblyData testChild = await GetPostAssertNewAssemblyData();
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{testObject.ID}/action/linkassembly/{testChild.ID}", "", HttpStatusCode.OK);

            //test: make sure there are no expanded assemblys
            var noneChild = await EndpointTests.AssertGetStatusCode<AssemblyCategory>(client, $"{apiUrl}/{testObject.ID}", HttpStatusCode.OK);
            Assert.IsTrue(noneChild.Assemblies == null || noneChild.Assemblies.Count() == 0);
            Assert.IsTrue(noneChild.SimpleAssemblies == null || noneChild.SimpleAssemblies.Count() == 0);

            //test: make sure there are expanded assemblys
            var fullChild = await EndpointTests.AssertGetStatusCode<AssemblyCategory>(client, $"{apiUrl}/{testObject.ID}?AssemblyParts=Full", HttpStatusCode.OK);
            Assert.IsTrue(fullChild.Assemblies != null && fullChild.Assemblies.Count() > 0);
            Assert.IsTrue(fullChild.SimpleAssemblies == null || fullChild.SimpleAssemblies.Count() == 0);

            //test: make sure there are  expanded simpleassemblys
            var simpleChild = await EndpointTests.AssertGetStatusCode<AssemblyCategory>(client, $"{apiUrl}/{testObject.ID}?AssemblyParts=Simple", HttpStatusCode.OK);
            Assert.IsTrue(simpleChild.Assemblies == null || simpleChild.Assemblies.Count() == 0);
            Assert.IsTrue(simpleChild.SimpleAssemblies != null && simpleChild.SimpleAssemblies.Count() > 0);
        }

        private async Task<AssemblyCategory> GetPostAssertNewAssemblyCategory()
        {
            AssemblyCategory testObject = new AssemblyCategory()
            {
                Name = this.NewTestAssemblyDataName,
                IsActive = true,
                BID = 1,
                Description = "blah blah"
            };

            //test: expect OK
            var newCategory = await EndpointTests.AssertPostStatusCode<AssemblyCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);
            return newCategory;
        }

        [TestMethod]
        public async Task TestAssemblyCategoryActionEndpoints()
        {
            AssemblyData testAssemblyData = new AssemblyData()
            {
                Name = this.NewTestAssemblyDataName,
                IsActive = true,
                BID = 1,
            };
            var newAssemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, $"/api/assemblypart", JsonConvert.SerializeObject(testAssemblyData), HttpStatusCode.OK);

            AssemblyCategory testAssemblyCat = new AssemblyCategory()
            {
                Name = this.NewTestAssemblyCategoryName,
                IsActive = true,
                BID = 1,
                Description = "blah blah"
            };

            //test: expect OK
            var newAssemblyCategory = await EndpointTests.AssertPostStatusCode<AssemblyCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(testAssemblyCat), HttpStatusCode.OK);

            //POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newAssemblyCategory.ID}/action/setinactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newAssemblyCategory.ID}/action/setactive", HttpStatusCode.OK);

            //POST candelete
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newAssemblyCategory.ID}/action/candelete", HttpStatusCode.OK);

            //POST link assembly
            EntityActionChangeResponse linkResponse;
            linkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newAssemblyCategory.ID}/action/linkassembly/{newAssemblyData.ID}", HttpStatusCode.OK);
            Assert.IsTrue(linkResponse.Success);
            //unable to link twice
            linkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newAssemblyCategory.ID}/action/linkassembly/{newAssemblyData.ID}", HttpStatusCode.BadRequest);
            Assert.IsFalse(linkResponse.Success);

            //POST unlink assembly
            EntityActionChangeResponse unlinkResponse;
            unlinkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newAssemblyCategory.ID}/action/unlinkassembly/{newAssemblyData.ID}", HttpStatusCode.OK);
            Assert.IsTrue(unlinkResponse.Success);
            //unable to unlink twice
            unlinkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newAssemblyCategory.ID}/action/unlinkassembly/{newAssemblyData.ID}", HttpStatusCode.BadRequest);
            Assert.IsFalse(unlinkResponse.Success);
        }

        [TestMethod]
        public async Task TestAssemblyCategoryCanDelete()
        {
            AssemblyData testAssemblyData = new AssemblyData()
            {
                Name = this.NewTestAssemblyDataName,
                IsActive = true,
                BID = 1
            };
            var newAssemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, $"/api/assemblypart", JsonConvert.SerializeObject(testAssemblyData), HttpStatusCode.OK);

            AssemblyCategory testAssemblyCat = new AssemblyCategory()
            {
                Name = this.NewTestAssemblyCategoryName,
                IsActive = true,
                BID = 1,
                Description = "blah blah"
            };

            //test: expect OK
            var newAssemblyCategory = await EndpointTests.AssertPostStatusCode<AssemblyCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(testAssemblyCat), HttpStatusCode.OK);

            //POST candelete
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newAssemblyCategory.ID}/action/candelete", HttpStatusCode.OK);

        }

        [TestMethod]
        public async Task TestAssemblyCategoryLinkingEndpoints()
        {
            //setup
            AssemblyCategory testCategory = await GetPostAssertNewAssemblyCategory();
            AssemblyData testChild = await GetPostAssertNewAssemblyData();

            //test: link from category to child
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{testCategory.ID}/action/linkassembly/{testChild.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{testCategory.ID}/action/unlinkassembly/{testChild.ID}", HttpStatusCode.OK);

            //test: link from child to category
            await EndpointTests.AssertPostStatusCode(client, $"/api/assemblypart/{testChild.ID}/action/linkassemblycategory/{testCategory.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"/api/assemblypart/{testChild.ID}/action/unlinkassemblycategory/{testCategory.ID}", HttpStatusCode.OK);
        }

        private async Task<AssemblyData> GetPostAssertNewAssemblyData()
        {
            AssemblyData testObject = new AssemblyData()
            {
                Name = this.NewTestAssemblyDataName,
                IsActive = true,
                BID = 1
            };

            //test: expect OK
            var newObject = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, $"api/assemblypart", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);
            return newObject;
        }

        [TestCleanup]
        public void Teardown()
        {
            //you can actually do test-specific things like this
            //switch (TestContext.TestName)
            //{
            //    case "TestPaymentTermSingleEndpoints":
            //    case "TestFiltering":
            //        // do stuff
            //        break;
            //    default:
            //        break;
            //}
            this.DeleteTestRecord();
        }

        private void DeleteTestRecord()
        {
            string newTestAssemblyCategoryName = NewTestAssemblyCategoryName;
            string newTestAssemblyDataName = NewTestAssemblyDataName;
            Task.Run(async () =>
            {
                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                SimpleAssemblyCategory[] result = JsonConvert.DeserializeObject<SimpleAssemblyCategory[]>(responseString);
                if (result != null)
                {
                    SimpleAssemblyCategory sga = result.FirstOrDefault(x => x.DisplayName.Contains(newTestAssemblyCategoryName));
                    if (sga != null)
                    {
                        await client.DeleteAsync($"{apiUrl}/{sga.ID}?Force=true");
                    }
                }
                response = await client.GetAsync($"api/assemblypart/SimpleList");
                responseString = await response.Content.ReadAsStringAsync();
                SimpleAssemblyData[] dataResult = JsonConvert.DeserializeObject<SimpleAssemblyData[]>(responseString);
                if (result != null)
                {
                    SimpleAssemblyData sga = dataResult.FirstOrDefault(x => x.DisplayName.Contains(newTestAssemblyDataName));
                    if (sga != null)
                    {
                        await client.DeleteAsync($"api/assemblypart/{sga.ID}");
                    }
                }
            }).Wait();
        }
    }
}
