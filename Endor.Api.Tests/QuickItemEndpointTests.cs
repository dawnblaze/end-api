﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Newtonsoft.Json.Linq;

namespace Endor.Api.Tests
{
    [TestClass]
    public class QuickItemEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/QuickItem";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        private QuickItemData GetNewModel()
        {
            return new QuickItemData()
            {
                Name = "Test quick item",
                IsActive = true,
                BID = 1,
                Categories = new FlatListItem[] { },
            };
        }

        [TestMethod]
        public async Task TestQuickItemSingleEndpoints()
        {
            QuickItemData model = GetNewModel();

            //test: expect OK
            var postedResponse = await EndpointTests.AssertPostStatusCode<QuickItemData>(client, $"{apiUrl}", JsonConvert.SerializeObject(model), HttpStatusCode.OK);
            model.ID = postedResponse.ID;

            // GET api/QuickItem/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleQuickItemData[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleQuickItemData sga in simpleList)
            {
                if (sga.DisplayName == model.Name)
                {
                    model.ID = sga.ID;
                }
            }

            //POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{model.ID}/action/setinactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{model.ID}/action/setactive", HttpStatusCode.OK);

            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + model.ID, JsonConvert.SerializeObject(model), HttpStatusCode.OK);

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{model.ID}", HttpStatusCode.NoContent);
            
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/32767", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestCanDelete()
        {

            QuickItemData model = GetNewModel();

            //test: expect OK
            var savedModel = await EndpointTests.AssertPostStatusCode<QuickItemData>(client, $"{apiUrl}", JsonConvert.SerializeObject(model), HttpStatusCode.OK);

            var getModel = await EndpointTests.AssertGetStatusCode<PaymentTerm>(client, $"{apiUrl}/{savedModel.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(getModel);
            Assert.AreEqual(true, getModel.IsActive);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{getModel.ID}/action/candelete", HttpStatusCode.OK);

            // CLEANUP
            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{savedModel.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestQuickItemLinks()
        {
            QuickItemData model = GetNewModel();

            //test: expect OK
            var postedResponse = await EndpointTests.AssertPostStatusCode<QuickItemData>(client, $"{apiUrl}", JsonConvert.SerializeObject(model), HttpStatusCode.OK);
            model.ID = postedResponse.ID;

            string categoryName = DateTime.UtcNow.ToString("o");
            // create QuickItemCategory for test
            FlatListItem testCategory = Utils.GetFlatListItem(FlatListType.QuickItemCategories);
            testCategory.Name = categoryName;
            testCategory = await EndpointTests.AssertPostStatusCode<FlatListItem>(client, $"/api/flatlist", JsonConvert.SerializeObject(testCategory), HttpStatusCode.OK);

            //POST linkCategory
            EntityActionChangeResponse linkReponse;
            linkReponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{model.ID}/action/LinkCategory/{testCategory.ID}", HttpStatusCode.OK);
            Assert.IsTrue(linkReponse.Success);
            //unable to link twice
            linkReponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{model.ID}/action/LinkCategory/{testCategory.ID}", HttpStatusCode.BadRequest);
            Assert.IsFalse(linkReponse.Success);

            var quickItemWithCategories = await EndpointTests.AssertGetStatusCode<QuickItemData>(client, $"{apiUrl}/{model.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(quickItemWithCategories);
            Assert.IsNotNull(quickItemWithCategories.Categories);
            Assert.IsTrue(quickItemWithCategories.Categories.Count > 0);

            //POST unlinkCategory
            EntityActionChangeResponse unlinkReponse;
            unlinkReponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{model.ID}/action/UnlinkCategory/{testCategory.ID}", HttpStatusCode.OK);
            Assert.IsTrue(unlinkReponse.Success);
            //unable to unlink twice
            unlinkReponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{model.ID}/action/UnlinkCategory/{testCategory.ID}", HttpStatusCode.BadRequest);
            Assert.IsFalse(unlinkReponse.Success);

            quickItemWithCategories = await EndpointTests.AssertGetStatusCode<QuickItemData>(client, $"{apiUrl}/{model.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(quickItemWithCategories);
            Assert.IsNull(quickItemWithCategories.Categories);

            // CLEANUP
            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/flatlist/{testCategory.ID}", HttpStatusCode.NoContent, HttpStatusCode.OK);
            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{model.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestQuickItemSingleEndpointsWithDataJSON()
        {
            QuickItemData model = GetNewModel();
            var items = new List<OrderItemData>()
            {
                new OrderItemData()
                {
                    BID = 1,
                    ID = 1,
                    Components = new List<OrderItemComponent>()
                    {
                        new OrderItemComponent()
                        {
                            BID = 1,
                            ID = 1,
                        },
                    }
                },
                new OrderItemData()
                {
                    BID = 1,
                    ID = 2,
                    Components = new List<OrderItemComponent>()
                    {
                        new OrderItemComponent()
                        {
                            BID = 1,
                            ID = 1,
                        },
                    }
                },
            };
            var jsonObj = new Dictionary<string, List<OrderItemData>>()
            {
                {"Items", items},
            };
            model.DataJSON = JsonConvert.SerializeObject(jsonObj);
            this.RenumberItems(model);
            JObject jsonObjWithoutIDs = JsonConvert.DeserializeObject<JObject>(model.DataJSON);
            this.RemoveIDs(jsonObjWithoutIDs);

            //test: expect OK
            var postedResponse = await EndpointTests.AssertPostStatusCode<QuickItemData>(client, $"{apiUrl}", JsonConvert.SerializeObject(model), HttpStatusCode.OK);
            model.ID = postedResponse.ID;
            Assert.AreEqual(jsonObjWithoutIDs.ToString(), postedResponse.DataJSON);

            // GET api/QuickItem/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleQuickItemData[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleQuickItemData sga in simpleList)
            {
                if (sga.DisplayName == model.Name)
                {
                    model.ID = sga.ID;
                }
            }

            //test: expect OK
            model.DataJSON = JsonConvert.SerializeObject(jsonObj);
            var putResponse = await EndpointTests.AssertPutStatusCode<QuickItemData>(client, $"{apiUrl}/" + model.ID, JsonConvert.SerializeObject(model), HttpStatusCode.OK);
            Assert.AreEqual(jsonObjWithoutIDs.ToString(), putResponse.DataJSON);

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{model.ID}", HttpStatusCode.NoContent);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/32767", HttpStatusCode.NoContent);
        }

        private void RenumberItems(QuickItemData quickItem)
        {
            var quickItemSchema = JsonConvert.DeserializeObject<QuickItemDef>(quickItem.DataJSON);
            var lineItems = quickItemSchema.Items;
            int lineItemNumber = 0;
            foreach (var lineItem in lineItems)
            {
                lineItemNumber++;
                lineItem.ItemNumber = lineItemNumber;
            }
            quickItem.DataJSON = JsonConvert.SerializeObject(quickItemSchema);
        }

        private void RemoveIDs(JProperty prop)
        {
            if (prop.Name == "ID")
                prop.Value = null;

            else foreach (JToken tok in prop.Children())
            {
                RemoveIDs(tok);
            }
        }

        private void RemoveIDs(JToken tok)
        {

            if (tok.Type == JTokenType.Array)
                RemoveIDs(tok.Value<JArray>());

            else if (tok.Type == JTokenType.Object)
                RemoveIDs(tok.Value<JObject>());

            else if (tok.Type == JTokenType.Property)
                RemoveIDs(tok.Value<JProperty>());
        }

        private void RemoveIDs(JObject obj)
        {
            foreach (JToken tok in obj.Children())
            {
                RemoveIDs(tok);
            }
        }

        private void RemoveIDs(JArray jArray)
        {
            foreach (JObject jo in jArray)
            {
                RemoveIDs(jo);
            }
        }
    }
}
