﻿using Endor.Api.Web.Classes.Request;
using Endor.Api.Web.Classes.Responses;
using Endor.EF;
using Endor.GLEngine.Classes;
using Endor.GLEngine.Models;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class ReconciliationEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/reconciliation";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }
        private ApiContext ctx;

        private short BID = 1;
        private int _reconciliationID = -99;
        private List<int> _reconciliationItems = new List<int> { -99, -98, -97 };

        [TestInitialize]
        public async Task InitializeTests()
        {
            ctx = GetApiContext();
            await Cleanup();
            await InitializeReconciliations();

            var employeeToUse = ctx.EmployeeData.Include(e => e.UserLinks).FirstOrDefault(e => e.UserLinks.Count() > 0);
            if (employeeToUse != null)
                client = EndpointTests.GetHttpClient(Convert.ToInt16(employeeToUse.UserLinks.FirstOrDefault().AuthUserID), null, employeeToUse.ID);
        }

        private async Task InitializeReconciliations()
        {
            var glActivity = await new GLEngine.GLEngine(BID, ctx).CreateGLActivity("Reconciliation", "Reconciliation", null, null, DateTime.UtcNow, GLEntryType.Reconciliation, new List<GLData>(), (ctx, bid, id) => Task.FromResult(-100));
            ctx.ActivityGlactivity.Add(glActivity);
            await ctx.SaveChangesAsync();
            int? paymentMethodID = (await ctx.PaymentMethod.FirstOrDefaultAsync()).ID;

            var reconciliation = new Reconciliation
            {
                ID = _reconciliationID,
                BID = BID,
                GLActivityID = glActivity.ID,
                ExportedDT = DateTime.UtcNow,
                Description = "Test Reconciliation",
                LocationID = 1,
                AccountingDT = DateTime.UtcNow.AddDays(0),
                LastAccountingDT = DateTime.UtcNow.AddDays(-10),
                CreatedDT = DateTime.UtcNow,
                ModifiedDT = DateTime.UtcNow,
                ClassTypeID = (int)ClassType.Reconciliation,
                Items = _reconciliationItems.Select(id => new ReconciliationItem
                {
                    ID = id,
                    BID = BID,
                    ClassTypeID = (int)ClassType.ReconciliationItem,
                    PaymentMethodID = paymentMethodID,
                    ModifiedDT = DateTime.UtcNow.AddMinutes(id)
                }).ToList()
            };

            var reconciliationLast = new Reconciliation
            {
                ID = _reconciliationID - 1,
                BID = BID,
                ExportedDT = DateTime.UtcNow,
                Description = "Test Reconciliation",
                LocationID = 1,
                AccountingDT = DateTime.UtcNow,
                LastAccountingDT = DateTime.UtcNow,
                CreatedDT = DateTime.UtcNow,
                ModifiedDT = DateTime.UtcNow,
                ClassTypeID = (int)ClassType.Reconciliation
            };

            ctx.Reconciliation.Add(reconciliation);
            ctx.Reconciliation.Add(reconciliationLast);
            await ctx.SaveChangesAsync();
        }

        [TestMethod]
        public async Task TestSimpleReconciliationEndPoints()
        {
            var responseReconciliations = await EndpointTests.AssertGetStatusCode<Reconciliation[]>(client, $"{apiUrl}", System.Net.HttpStatusCode.OK);
            var responseReconciliationsLocation = await EndpointTests.AssertGetStatusCode<Reconciliation[]>(client, $"{apiUrl}?LocationID=1", System.Net.HttpStatusCode.OK);
            var responseReconciliationsNoLocation = await EndpointTests.AssertGetStatusCode<Reconciliation[]>(client, $"{apiUrl}?LocationID=-20", System.Net.HttpStatusCode.BadRequest);
            var responseReconciliation = await EndpointTests.AssertGetStatusCode<Reconciliation>(client, $"{apiUrl}/-99", System.Net.HttpStatusCode.OK);
            var responseReconciliationsSimpleList = await EndpointTests.AssertGetStatusCode<SimpleReconciliation[]>(client, $"{apiUrl}/simplelist", System.Net.HttpStatusCode.OK);
            var responseReconciliationsSimpleListLocation = await EndpointTests.AssertGetStatusCode<SimpleReconciliation[]>(client, $"{apiUrl}/simplelist?LocationID=1", System.Net.HttpStatusCode.OK);

            await EndpointTests.AssertGetStatusCode<Reconciliation>(client, $"{apiUrl}/-93", System.Net.HttpStatusCode.NotFound);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/-100", System.Net.HttpStatusCode.BadRequest);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/-99", System.Net.HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/-98", System.Net.HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/-93", System.Net.HttpStatusCode.OK); // idempotency

            Assert.IsTrue(responseReconciliations.Count() > 0);
            Assert.IsTrue(responseReconciliationsLocation.Count() > 0);
            Assert.IsTrue(responseReconciliationsNoLocation == null);
            Assert.IsTrue(responseReconciliation.ID == _reconciliationID);
            Assert.IsTrue(responseReconciliation.Items.Count >= 3);
            Assert.IsTrue(responseReconciliationsSimpleList.Count() > 0);
            Assert.IsTrue(responseReconciliationsSimpleListLocation.Count() > 0);
        }

        [TestMethod]
        public async Task TestReconciliationAdjustments()
        {
            await AddReconciliationsAdjustments();

            var responseReconciliationsHasAdjustments = await EndpointTests.AssertGetStatusCode<HasAdjustmentsResponse>(client, $"{apiUrl}/hasadjustments", System.Net.HttpStatusCode.OK);
            var responseReconciliationsHasAdjustmentsLocation = await EndpointTests.AssertGetStatusCode<HasAdjustmentsResponse>(client, $"{apiUrl}/hasadjustments?LocationID=1", System.Net.HttpStatusCode.OK);
            var responseReconciliationsHasAdjustmentsNoLocation = await EndpointTests.AssertGetStatusCode<HasAdjustmentsResponse>(client, $"{apiUrl}/hasadjustments?LocationID=4", System.Net.HttpStatusCode.OK);

            Assert.IsTrue(responseReconciliationsHasAdjustments.HasAdjustments == true);
            Assert.IsTrue(responseReconciliationsHasAdjustments.Adjustments.Count >= 1);

            Assert.IsTrue(responseReconciliationsHasAdjustmentsLocation.HasAdjustments == true);
            Assert.IsTrue(responseReconciliationsHasAdjustmentsLocation.Adjustments.Count == 1);

            Assert.IsTrue(responseReconciliationsHasAdjustmentsNoLocation.HasAdjustments == false);
            Assert.IsTrue(responseReconciliationsHasAdjustmentsNoLocation.Adjustments.Count == 0);
        }

        [TestMethod]
        public async Task TestReconciliationAdjustmentsPreview()
        {
            await AddReconciliationsAdjustments();

            var responseReconciliation = await EndpointTests.AssertGetStatusCode<Reconciliation[]>(client, $"{apiUrl}/preview?LocationID=1&IncludeAdjustments=Yes", System.Net.HttpStatusCode.OK);
            Assert.IsTrue(responseReconciliation.Count() >= 1);
            Assert.IsTrue(responseReconciliation.Any(a => a.IsAdjustmentEntry));

            var responseReconciliationOnly = await EndpointTests.AssertGetStatusCode<Reconciliation[]>(client, $"{apiUrl}/preview?LocationID=1&IncludeAdjustments=Only", System.Net.HttpStatusCode.OK);
            Assert.IsTrue(responseReconciliationOnly.Count() >= 1);
            Assert.IsTrue(responseReconciliationOnly.All(a => a.IsAdjustmentEntry));


            var responseReconciliationNo = await EndpointTests.AssertGetStatusCode<Reconciliation[]>(client, $"{apiUrl}/preview?LocationID=1&IncludeAdjustments=No", System.Net.HttpStatusCode.OK);
            Assert.IsTrue(responseReconciliationNo.Count() >= 1);
            Assert.IsTrue(responseReconciliationNo.All(a => !a.IsAdjustmentEntry));

        }

        [TestMethod]
        public async Task TestCashDrawer()
        {
            var reconciliation = await ctx.Reconciliation.FirstAsync(a => a.ID == _reconciliationID);
            //Reconcile a Location to Start
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/reconcile?LocationID={reconciliation.LocationID}&CreateAdjustments=true", null, System.Net.HttpStatusCode.OK);
            System.Threading.Thread.Sleep(1000);
            //View GET /api/reconciliation/preview/cashdrawer and confirm there are no Adjustment Amounts
            var result1 = await EndpointTests.AssertGetStatusCode<List<CashDrawerBalance>>(client, $"{apiUrl}/preview/cashdrawer", HttpStatusCode.OK);
            Assert.AreEqual(result1.Count, 1);
            Assert.AreEqual(result1.First().StartingBalance, result1.First().EndingBalance);
            //Add one or more cash payments (so you have cash balances)
            var payment = GetSampleCashPaymentRequest(reconciliation.LocationID);
            string paymentApiUrl = "/api/payment";
            var paymentResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{paymentApiUrl}/payment", JsonConvert.SerializeObject(payment), HttpStatusCode.OK);
            Assert.IsNotNull(paymentResult);
            //POST a new Reconciliation with a Cash In, Cash Out, and Cash to Deposit amount. Save the ID returned.
            var cashAdjustment = new CashDrawerAdjustment()
            {
                AdjustmentsIn = 3m,
                AdjustmentsOut = 2m,
                DepositTransfer = payment.Amount+1,
                LocationID = reconciliation.LocationID
            };
            System.Threading.Thread.Sleep(1000);
            var reconciliations = await EndpointTests.AssertPostStatusCode<List<Reconciliation>>(client, $"{apiUrl}/reconcile?LocationID={reconciliation.LocationID}", JsonConvert.SerializeObject(new List<CashDrawerAdjustment>() { cashAdjustment }), System.Net.HttpStatusCode.OK);
            //View GET /api/reconciliation/preview/cashdrawer and confirm there are no Adjustment Amounts
            result1 = await EndpointTests.AssertGetStatusCode<List<CashDrawerBalance>>(client, $"{apiUrl}/preview/cashdrawer", HttpStatusCode.OK);
            Assert.AreEqual(result1.Count, 1);
            //View GET /api/reconciliation/<<savedid>>/cashdrawer and confirm the amounts match
            var reconciliationResult = await EndpointTests.AssertGetStatusCode<List<CashDrawerBalance>>(client, $"{apiUrl}/{reconciliations.First().ID}/cashdrawer", HttpStatusCode.OK);
            Assert.AreEqual(reconciliationResult.Count, 1);
            //Assert.AreEqual(reconciliationResult.First().StartingBalance, reconciliationResult.First().EndingBalance);
            Assert.AreEqual(reconciliationResult.First().AdjustmentsIn, 3m);
            Assert.AreEqual(reconciliationResult.First().AdjustmentsOut, 2m);
            Assert.AreEqual(reconciliationResult.First().DepositTransfer, -(payment.Amount + 1));



        }

        private PaymentRequest GetSampleCashPaymentRequest(byte locationID)
        {
            List<PaymentApplicationRequest> paymentApplications = new List<PaymentApplicationRequest>();
            paymentApplications.Add(new PaymentApplicationRequest
            {
                OrderID = null,
                Amount = 5m
            });

            var ctx = GetApiContext();
            return new PaymentRequest
            {
                LocationID = locationID,
                CompanyID = ctx.CompanyData.First().ID,
                Amount = 5m,
                PaymentTransactionType = (int)PaymentTransactionType.Payment,
                ReceivedLocationID = ctx.LocationData.First().ID,
                Applications = paymentApplications,
                PaymentMethodID = (byte)PaymentMethodType.Cash
            };
        }

        private async Task AddReconciliationsAdjustments()
        {
            int nextGLId = -99;
            var glEngine = new Endor.GLEngine.GLEngine(BID, ctx);
            var incomeAccounts = ctx.GLAccount.Where(account => account.GLAccountType == 40).ToList(); //Income

            CompanyData company = new CompanyData()
            {
                ID = -99,
                Name = "Test Company",
                IsActive = true,
                BID = TestConstants.BID,
                StatusID = (byte)CompanyStatus.Lead,
                LocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID,
            };
            var existingCompanies = ctx.CompanyData.Where(c => c.ID == company.ID && c.BID == company.BID);
            
            if (existingCompanies.Count() > 0) 
                company = existingCompanies.First();
            else
            {
                ctx.CompanyData.Add(company);
                ctx.SaveChanges();
            }

            var order = await ctx.OrderData.FirstOrDefaultAsync(a => a.ID == -99);
            var reconciliation = await ctx.Reconciliation.FirstAsync(a => a.ID == _reconciliationID);

            if (order == null)
            {
                order = Utils.GetTestOrderData();
                order.ID = -99;
                order.CompanyID = -99;
                order.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
                order.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
                order.ProductionLocationID = order.PickupLocationID;
                var orderItemData = Utils.GetTestOrderItemData(order);
                orderItemData.ID = -99;
                orderItemData.ItemStatusID = 21;
                var orderItemComponentData = Utils.GetTestOrderItemComponentData(orderItemData, incomeAccounts.First());
                orderItemData.Components = new List<OrderItemComponent> { orderItemComponentData };
                order.Items = new List<OrderItemData> { orderItemData };
                order.LocationID = reconciliation.LocationID;
                ctx.OrderData.Add(order);
                await ctx.SaveChangesAsync();
            } else
            {
                order.LocationID = reconciliation.LocationID;
                await ctx.SaveChangesAsync();
            }

            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            glEntries.ForEach(gl => gl.AccountingDT = reconciliation.AccountingDT.AddMinutes(-2));
            glEntries.ForEach(gl => gl.RecordedDT = reconciliation.AccountingDT.AddMinutes(-2));
            glEntries.ForEach(gl => gl.ActivityID = reconciliation.GLActivityID);
            glEntries.ForEach(gl => gl.ID = nextGLId++);
            ctx.GLData.AddRange(glEntries);
            reconciliation.EndingGLID = glEntries.ToList().Last().ID;
            await ctx.SaveChangesAsync();

            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            await ctx.SaveChangesAsync();

            var newglEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            newglEntries.ForEach(gl => gl.AccountingDT = reconciliation.AccountingDT.AddMinutes(-1));
            newglEntries.ForEach(gl => gl.RecordedDT = reconciliation.AccountingDT.AddMinutes(1));
            newglEntries.ForEach(gl => gl.ActivityID = reconciliation.GLActivityID);
            nextGLId = -80;
            newglEntries.ForEach(gl => gl.ID = nextGLId++);
            ctx.GLData.AddRange(newglEntries);
            await ctx.SaveChangesAsync();
        }

        [TestMethod]
        public async Task TestReconciliationPreviewEndPoint()
        {
            var context = GetApiContext(BID);
            var employee = context.EmployeeData.First(t => t.BID == BID && t.IsActive);
            var _client = EndpointTests.GetHttpClient(employeeID: employee.ID);
            var location = context.LocationData.First(t => t.BID == BID);
            var accountingDT = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

            // test invalid location
            await EndpointTests.AssertGetStatusCode<Reconciliation[]>(_client, $"{apiUrl}/preview?LocationID=-6705", System.Net.HttpStatusCode.BadRequest);

            // test valid location
            var responseReconciliation = await EndpointTests.AssertGetStatusCode<Reconciliation[]>(_client, $"{apiUrl}/preview?LocationID={location.ID}", System.Net.HttpStatusCode.OK);
            Assert.AreEqual(1, responseReconciliation.Count());
            // test valid location and date
            responseReconciliation = await EndpointTests.AssertGetStatusCode<Reconciliation[]>(_client, $"{apiUrl}/preview?LocationID={location.ID}&AccountingDT={JsonConvert.SerializeObject(accountingDT).Replace("\"", "")}", System.Net.HttpStatusCode.OK);
            Assert.IsTrue(responseReconciliation.Length > 0);
        }

        [TestMethod]
        public async Task TestReconciliationPreviewWithAdjustmentsEndPoint()
        {
            var context = GetApiContext(BID);
            var employee = context.EmployeeData.First(t => t.BID == BID && t.IsActive);
            var _client = EndpointTests.GetHttpClient(employeeID: employee.ID);
            var accountingDT = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var reconciliation = await ctx.Reconciliation.FirstAsync(a => a.ID == _reconciliationID);

            // test invalid location
            await EndpointTests.AssertGetStatusCode<Reconciliation[]>(_client, $"{apiUrl}/preview?LocationID=-67056", System.Net.HttpStatusCode.BadRequest);

            await AddReconciliationsAdjustments();

            // test valid location
            var responseReconciliation = await EndpointTests.AssertGetStatusCode<Reconciliation[]>(_client, $"{apiUrl}/preview?LocationID={reconciliation.LocationID}&IncludeAdjustments=Yes", System.Net.HttpStatusCode.OK);
            Assert.IsTrue(responseReconciliation.Count() >= 1);
            Assert.IsTrue(responseReconciliation.Any(a => a.IsAdjustmentEntry));
            // test valid location and date
            responseReconciliation = await EndpointTests.AssertGetStatusCode<Reconciliation[]>(_client, $"{apiUrl}/preview?LocationID={reconciliation.LocationID}&IncludeAdjustments=Yes&AccountingDT={JsonConvert.SerializeObject(accountingDT).Replace("\"", "")}", System.Net.HttpStatusCode.OK);
            Assert.IsTrue(responseReconciliation.Length > 0);
            Assert.IsTrue(responseReconciliation.Any(a => a.IsAdjustmentEntry));
        }

        [TestMethod]
        public async Task TestReconcileEndPointAllLocations()
        {
            var _client = EndpointTests.GetHttpClient(employeeID: -1);
            await EndpointTests.AssertPostStatusCode(_client, $"{apiUrl}/reconcile?CreateAdjustments=true", JsonConvert.SerializeObject(""), System.Net.HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestReconcileEndPoint()
        {
            var context = GetApiContext(BID);
            var _client = EndpointTests.GetHttpClient(employeeID: -1);
            var location = context.LocationData.First(t => t.BID == BID);
            var accountingDT = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

            // test invalid location
            await EndpointTests.AssertPostStatusCode(_client, $"{apiUrl}/reconcile?LocationID=-54345", "", System.Net.HttpStatusCode.BadRequest);

            // test valid location
            await EndpointTests.AssertPostStatusCode(_client, $"{apiUrl}/reconcile?LocationID={location.ID}", "", System.Net.HttpStatusCode.OK);
        }


        [TestCleanup]
        public async Task Cleanup()
        {
            var recons = await ctx.Reconciliation.Include(a => a.Items).Where(a => a.ID <= 0 || a.EnteredByID == -1 || a.StartingGLID <= 0 || a.AdjustedReconciliationID <= 0).ToListAsync();

            if (recons != null && recons.Any())
            {
                recons.Reverse();
                foreach (var recon in recons)
                {
                    if (recon.Items != null && recon.Items.Any())
                        ctx.ReconciliationItem.RemoveRange(recon.Items);
                    ctx.Reconciliation.Remove(recon);
                }
            }
            await ctx.SaveChangesAsync();
            ctx.GLData.RemoveRange(ctx.GLData.Where(o => o.ID <= 0));
            ctx.ActivityGlactivity.RemoveRange(ctx.ActivityGlactivity.Where(o => o.ID <= 0));
            ctx.OrderItemComponent.RemoveRange(ctx.OrderItemComponent.Where(o => o.ID <= 0));
            ctx.OrderItemData.RemoveRange(ctx.OrderItemData.Where(o => o.ID <= 0 || o.Order.ID <= 0));
            ctx.OrderCustomData.RemoveRange(ctx.OrderCustomData.Where(o => o.ID <= 0 || o.Order.ID <= 0));
            ctx.OrderEmployeeRole.RemoveRange(ctx.OrderEmployeeRole.Where(o => o.ID <= 0 || o.Order.ID <= 0));
            ctx.OrderData.RemoveRange(ctx.OrderData.Where(o => o.ID <= 0));
            ctx.CompanyData.RemoveRange(ctx.CompanyData.Where(o => o.ID <= 0));

            await ctx.SaveChangesAsync();
        }
    }
}
