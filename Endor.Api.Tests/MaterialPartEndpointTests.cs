﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.DocumentStorage.Models;
using Endor.EF;

namespace Endor.Api.Tests
{
    [TestClass]
    public class MaterialPartEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/materialpart";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestMaterialDataName;
        private ApiContext ctx;
        private MaterialCategory materialpartCategory;
        private MaterialData materialpart;

        public TestContext TestContext { get; set; }
        private string NewTestMachineName;
        private int testMachineTemplateID;
        private byte testLocationID;
        private short testBID = 1;

        [TestInitialize]
        public async Task Initialize()
        {
            this.NewTestMachineName = DateTime.UtcNow + " TEST PART MACHINE DATA";
            this.NewTestMaterialDataName = DateTime.UtcNow + " TEST PART MATERIAL DATA";
            ctx = GetApiContext();
            materialpartCategory = ctx.MaterialCategory.FirstOrDefault(mc => mc.IsActive == true);
            materialpart = ctx.MaterialData.FirstOrDefault(md => md.IsActive == true);

            var assemblies = await EndpointTests.AssertGetStatusCode<AssemblyData[]>(client, $"/api/assemblypart/", HttpStatusCode.OK);
            var tables = assemblies.FirstOrDefault(e => e.Tables != null && e.AssemblyType == AssemblyType.MachineTemplate);
            this.testMachineTemplateID = assemblies.FirstOrDefault(e => e.AssemblyType == AssemblyType.MachineTemplate).ID;
            var locations = await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK);
            this.testLocationID = locations.FirstOrDefault().ID;
            
        }

        [TestMethod]
        public async Task TestMaterialDataCRUDEndpoints()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleGLAccount[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;

            MaterialData testObject = new MaterialData()
            {
                Name = this.NewTestMaterialDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,
                InvoiceText = "blah blah",
                Description = "blah"
            };

            //test: CREATE expect OK
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            // GET api/PaymentTerms/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleMaterialData[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleMaterialData sga in simpleList)
            {
                if (sga.DisplayName == testObject.Name)
                {
                    testObject.ID = sga.ID;
                }
            }

            testObject.Description = "test test";

            //test: UPDATE expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + testObject.ID, JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            //test: READ expect OK
            var updatedMaterialData = await EndpointTests.AssertGetStatusCode<MaterialData>(client, $"{apiUrl}/" + testObject.ID, HttpStatusCode.OK);
            Assert.AreEqual(updatedMaterialData.Description, testObject.Description);

            //test: DELETE expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testObject.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestMaterialDataRelatedEnumEndpoints()
        {
            await EndpointTests.AssertGetStatusCode<EnumMaterialConsumptionMethod[]>(client, $"/api/partconsumptionmethod", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode<EnumMaterialCostingMethod[]>(client, $"/api/partcostingmethod", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestMaterialDataFilters()
        {
            //setup- get a test object
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleMaterialData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            MaterialData newObject = new MaterialData()
            {
                Name = this.NewTestMaterialDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,
                InvoiceText = "blah blah",
                Description = "blah"
            };

            //test: expect OK
            MaterialData testObject = await EndpointTests.AssertPostStatusCode<MaterialData>(client, $"{apiUrl}", JsonConvert.SerializeObject(newObject), HttpStatusCode.OK);

            //test: expect GET /simplelist ?isActive=true to only have active objects
            var simpleAllActive = await EndpointTests.AssertGetStatusCode<SimpleMaterialData[]>(client, $"{apiUrl}/SimpleList?isActive=true", HttpStatusCode.OK);
            Assert.IsTrue(simpleAllActive.All(x => x.IsActive));
            Assert.IsNotNull(simpleAllActive.FirstOrDefault(x => x.DisplayName == NewTestMaterialDataName));

            //test: expect GET (all) ?isActive=true to only have active objects
            var fullAllActive = await EndpointTests.AssertGetStatusCode<MaterialData[]>(client, $"{apiUrl}?isActive=true", HttpStatusCode.OK);
            Assert.IsTrue(fullAllActive.All(x => x.IsActive));
            Assert.IsNotNull(fullAllActive.FirstOrDefault(x => x.Name == NewTestMaterialDataName));

            //setup: set object to inactive
            testObject.IsActive = false;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + testObject.ID, JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            //test: expect GET /simplelist ?isActive=false to only have inactive objects
            var simpleAllInactive = await EndpointTests.AssertGetStatusCode<SimpleMaterialData[]>(client, $"{apiUrl}/SimpleList?isActive=false", HttpStatusCode.OK);
            Assert.IsTrue(simpleAllInactive.All(x => !x.IsActive));
            Assert.IsNotNull(simpleAllInactive.FirstOrDefault(x => x.DisplayName == NewTestMaterialDataName));

            //test: expect GET (all) ?isActive=false to only have inactive objects
            var fullAllInactive = await EndpointTests.AssertGetStatusCode<MaterialData[]>(client, $"{apiUrl}?isActive=false", HttpStatusCode.OK);
            Assert.IsTrue(fullAllInactive.All(x => !x.IsActive));
            Assert.IsNotNull(fullAllInactive.FirstOrDefault(x => x.Name == NewTestMaterialDataName));
        }

        [TestMethod]
        public async Task TestMaterialDataActionEndpoints()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleMaterialData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            MaterialData testProject = new MaterialData()
            {
                Name = this.NewTestMaterialDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,
                InvoiceText = "blah blah",
                Description = "blah"
            };

            //test: expect OK
            var newMaterialData = await EndpointTests.AssertPostStatusCode<MaterialData>(client, $"{apiUrl}", JsonConvert.SerializeObject(testProject), HttpStatusCode.OK);

            //test: POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newMaterialData.ID}/action/setinactive", HttpStatusCode.OK);

            //test: POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newMaterialData.ID}/action/setactive", HttpStatusCode.OK);

            //test: POST setactive
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newMaterialData.ID}/action/candelete", HttpStatusCode.OK);

            string materialCategoryName = DateTime.UtcNow.ToString("o");
            // create Materialcategory for test
            MaterialCategory testMaterialCat = new MaterialCategory()
            {
                BID = 1,
                Name = materialCategoryName,
                Description = "Description",
                ModifiedDT = DateTime.UtcNow,
                ParentID = null
            };
            var newMaterialCat = await EndpointTests.AssertPostStatusCode<MaterialCategory>(client, $"/api/Materialcategory", JsonConvert.SerializeObject(testMaterialCat), HttpStatusCode.OK);

            //POST linkMaterialcategory
            EntityActionChangeResponse linkReponse;
            linkReponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newMaterialData.ID}/action/linkMaterialcategory/{newMaterialCat.ID}", HttpStatusCode.OK);
            Assert.IsTrue(linkReponse.Success);
            //unable to link twice
            linkReponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newMaterialData.ID}/action/linkMaterialcategory/{newMaterialCat.ID}", HttpStatusCode.BadRequest);
            Assert.IsFalse(linkReponse.Success);

            //POST linkMaterialcategory
            EntityActionChangeResponse unlinkReponse;
            unlinkReponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newMaterialData.ID}/action/unlinkMaterialcategory/{newMaterialCat.ID}", HttpStatusCode.OK);
            Assert.IsTrue(unlinkReponse.Success);
            //unable to unlink twice
            unlinkReponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newMaterialData.ID}/action/unlinkMaterialcategory/{newMaterialCat.ID}", HttpStatusCode.BadRequest);
            Assert.IsFalse(unlinkReponse.Success);
        }

        [TestCleanup]
        public void Teardown()
        {
            //you can actually do test-specific things like this
            //switch (TestContext.TestName)
            //{
            //    case "TestPaymentTermSingleEndpoints":
            //    case "TestFiltering":
            //        // do stuff
            //        break;
            //    default:
            //        break;
            //}
            this.DeleteTestRecord();
        }

        private void DeleteTestRecord()
        {
            string newTestMaterialDataName = NewTestMaterialDataName;
            Task.Run(async () =>
            {

                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                SimpleMaterialData[] result = JsonConvert.DeserializeObject<SimpleMaterialData[]>(responseString);
                if (result != null)
                {
                    SimpleMaterialData sga = result.FirstOrDefault(x => x.DisplayName.Contains(newTestMaterialDataName));
                    if (sga != null)
                    {
                        await client.DeleteAsync($"{apiUrl}/{sga.ID}");
                    }
                }
            }).Wait();
        }


        public async Task CleanupClonedMaterial(string materialName = null)
        {
            var ctx = GetApiContext();

            bool lamdaExp(MaterialData t) => t.InvoiceText.Contains(materialName ?? "TestInvoiceText");
            var materials = ctx.MaterialData.Where(lamdaExp).ToList();
            var materialLinks = ctx.MaterialCategoryLink.Where( t => ctx.MaterialCategoryLink.Select( y => y.PartID).Contains(t.PartID)).ToList();
            var categoryIDs = materialLinks.Select( t => t.CategoryID).ToList();
            var materialIds = materials.Select(t => t.ID).ToList();


            ctx.MaterialCategoryLink.RemoveRange(materialLinks);
            ctx.MaterialCategory.RemoveRange(ctx.MaterialCategory.Where(t => categoryIDs.Contains(t.ID)));
            ctx.MaterialData.RemoveRange(materials);

            ctx.CustomFieldDefinition.RemoveRange(ctx.CustomFieldDefinition.Where(t => t.AppliesToID != null && (t.AppliesToClassTypeID == (int)ClassType.Material) && t.Name == "Test Custom Field Definition"));
            ctx.CustomFieldOtherData.RemoveRange(ctx.CustomFieldOtherData.Where(t => t.AppliesToClassTypeID == (int)ClassType.Material && materialIds.Contains((int)t.ID)));

            await ctx.SaveChangesAsync();


            foreach (var material in materials)
            {
                await EndpointTests.AssertPostStatusCode(client, $"/api/dm/delete/{(int)ClassType.Material}/{material.ID}", null, HttpStatusCode.OK);
            }

        }

        [TestMethod]
        public async Task TestMaterialClonedEndpoint()
        {
            var materialName = "Material Labor Data Category Test";

            var ctx = GetApiContext();
            await CleanupClonedMaterial(materialName);

            

            var materialCategory = new MaterialCategory()
            {
                BID = 1,
                Name = materialName + "Category",
                IsActive = true
            };

            // material category endpoint
            var category = await EndpointTests.AssertPostStatusCode<MaterialCategory>(client, $"/api/materialcategory", JsonConvert.SerializeObject(materialCategory), HttpStatusCode.OK);


            var materialData = new MaterialData()
            {
                Name = materialName,
                IsActive = true,
                BID = 1,
                InvoiceText = materialName,
                ExpenseAccountID = ctx.GLAccount.First().ID,
                IncomeAccountID = ctx.GLAccount.First().ID
            };

            // material endpoint
            var material = await EndpointTests.AssertPostStatusCode<MaterialCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(materialData), HttpStatusCode.OK);

            await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"/api/materialcategory/{category.ID}/action/linkmaterial/{material.ID}", null, HttpStatusCode.OK);


            // cloning of material
            var cloned = await EndpointTests.AssertPostStatusCode<MaterialCategory>(client, $"{apiUrl}/{material.ID}/clone", null, HttpStatusCode.OK);

            Assert.AreEqual($"{materialName} (Clone)", cloned.Name);

            var clonedLinks = ctx.MaterialCategoryLink.FirstOrDefault(t => t.PartID == cloned.ID);

            Assert.IsNotNull(clonedLinks);


            // if the laborData.InvoiceText == laborData.Name, the clonedLaborData.InvoiceText should equal to the clonedLaborData.Name
            if (materialData.Name.Equals(materialData.InvoiceText))
            {
                var clonedMaterial = await EndpointTests.AssertGetStatusCode<MaterialData>(client, $"{apiUrl}/{cloned.ID}", HttpStatusCode.OK);
                Assert.AreEqual(clonedMaterial.InvoiceText, clonedMaterial.Name);
            }


            await CleanupClonedMaterial(materialName);
        }

        [TestMethod]
        public async Task TestMaterialCloneFiles()
        {
            var ctx = GetApiContext();
            await CleanupClonedMaterial();

            var materialName = "Material Category - " + new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();

            var materialData = new MaterialData()
            {
                Name = materialName,
                IsActive = true,
                BID = 1,
                InvoiceText = "TestInvoiceText",
                ExpenseAccountID = ctx.GLAccount.First().ID,
                IncomeAccountID = ctx.GLAccount.First().ID
            };
            
            var createdMaterial = await EndpointTests.AssertPostStatusCode<MaterialData>(client, $"{apiUrl}", JsonConvert.SerializeObject(materialData), HttpStatusCode.OK);

            var fooTextFileName = "foo.txt";
            var filePathFragment = $"{(int)ClassType.Material}/{createdMaterial.ID}/{fooTextFileName}";
            var someform = this.GetMultipartFormDataContent("foo", fooTextFileName);

            await AssertPostFileStatusCode(client, $"/api/dm/{Bucket.Documents}/{filePathFragment}", someform, HttpStatusCode.OK);

            var cloned = await EndpointTests.AssertPostStatusCode<MaterialData>(client, $"{apiUrl}/{createdMaterial.ID}/clone", null, HttpStatusCode.OK);


            var dm = GetDocumentManager(createdMaterial.BID, createdMaterial.ID, ClassType.Material, Web.BucketRequest.Documents);
            var createdDocuments = await dm.GetDocumentsAsync();
            var cloneDm = GetDocumentManager(cloned.BID, cloned.ID, ClassType.Material, Web.BucketRequest.Documents);
            var clonedDocuments = await cloneDm.GetDocumentsAsync();

            Assert.AreEqual(createdDocuments.Count, clonedDocuments.Count);
            Assert.AreEqual(createdDocuments[0].Name, clonedDocuments[0].Name);

            await CleanupClonedMaterial();
        }

        [TestMethod]
        public async Task TestMaterialDataCloneWithCustomFieldDefinition()
        {
            var ctx = GetApiContext();
            await CleanupClonedMaterial();

            var materialName = "Material-" + new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();


            // create Labor
            MaterialData testObject = new MaterialData()
            {
                Name = materialName,
                IsActive = true,
                BID = 1,
                InvoiceText = "TestInvoiceText",
                ExpenseAccountID = ctx.GLAccount.First().ID,
                IncomeAccountID = ctx.GLAccount.First().ID
            };
            var created = await EndpointTests.AssertPostStatusCode<MaterialData>(client, $"{apiUrl}", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            // create custom field definition
            var customFieldDefinition = new CustomFieldDefinition()
            {
                ClassTypeID = (int)ClassType.CustomFieldDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                AppliesToClassTypeID = (int)ClassType.Material,
                AppliesToID = created.ID,
                Name = materialName,
                Label = "Test Custom Field Label",
                Description = "Test Custom Field Desc",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DisplayType = CustomFieldNumberDisplayType.General,
                DisplayFormatString = "",
                AllowMultipleValues = false,
                HasValidators = false
            };

            var createdCustomFieldDefinition = await EndpointTests.AssertPostStatusCode<CustomFieldDefinition>(client, "/api/cf/definition", JsonConvert.SerializeObject(customFieldDefinition), HttpStatusCode.OK);

            var values = new CustomFieldValue[1];
            values[0] = new CustomFieldValue()
            {
                ID = createdCustomFieldDefinition.ID,
                V = "Test"
            };

            await EndpointTests.AssertPutStatusCode(client, $"/api/cf/value/{ClassType.Material.ID()}/{createdCustomFieldDefinition.AppliesToID}", JsonConvert.SerializeObject(values), HttpStatusCode.OK);

            var cloned = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"{apiUrl}/{created.ID}/clone", null, HttpStatusCode.OK);

            var retrievedCustomFieldDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldValue[]>(client, $"/api/cf/value/{ClassType.Material.ID()}/{cloned.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedCustomFieldDefinitions);
            Assert.AreEqual(1, retrievedCustomFieldDefinitions.Count());

            await CleanupClonedMaterial();
        }

        private async Task<MachineData> GetNewModel()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleMachineData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;

            var simpleTaxCodes = await EndpointTests.AssertGetStatusCode<SimpleTaxabilityCode[]>(client, $"/api/taxcode/SimpleList", HttpStatusCode.OK);
            short taxCodeID = simpleTaxCodes.FirstOrDefault().ID;

            return new MachineData()
            {
                Name = this.NewTestMachineName + " - MachineData",
                IncomeAccountID = glAccountID,
                ExpenseAccountID = glAccountID,
                TaxCodeID = taxCodeID,
                IsActive = true,
                BID =  1,
                MachineTemplateID = this.testMachineTemplateID,
                ActiveInstanceCount = 0,
                ActiveProfileCount = 0,
                Description = "Description",
                HasImage = false,
                SKU = "SKU",
                WorksheetData = "[]",
                Instances = new List<MachineInstance>
                {
                    GetTestMachineInstance()
                },
                Profiles = new List<MachineProfile>
                {
                    GetTestMachineProfile()
                }
            };
        }

        private MachineInstance GetTestMachineInstance()
        {
            return new MachineInstance
            {
                IsActive = true,
                Name = this.NewTestMachineName + " - Instance",
                LocationID = this.testLocationID,
                Manufacturer = "Manufacturer",
                SerialNumber = "SerialNumber",
                IPAddress = "IPAddress",
                PurchaseDate = DateTime.Now,
                HasServiceContract = true,
                ContractNumber = "",
                ContractStartDate = DateTime.Now,
                ContractEndDate = DateTime.Now,
                ContractInfo = "ContractInfo",
                Model = "Model",
                BID = testBID
            };
        }

        private MachineProfile GetTestMachineProfile()
        {

            return new MachineProfile
            {
                IsActive = true,
                IsDefault = true,
                Name = this.NewTestMachineName + " - Profile",
                MachineTemplateID = this.testMachineTemplateID,
                AssemblyOVDataJSON = "{}",
                BID = testBID,
                ID = 2345678,

                MachineProfileVariables = new List<MachineProfileVariable>
                {
                    GetTestMachineProfileVariable()
                }
            };
        }

        private MachineProfileVariable GetTestMachineProfileVariable()
        {
            
            return new MachineProfileVariable
            {
                BID = testBID,
                Name = "Test Machine Variable - Material",
                ProfileID = 0,
                ListValuesJSON = "{\"Categories\":[{\"Id\":"+ materialpartCategory.ID+"}],\"Components\":[{\"Id\":"+materialpart.ID+"}]}",
                Label = "MachineProfile Variable 1",
                OverrideDefault = true
            };
        }

        [TestMethod]
        public async Task MaterialListByVariable()
        {
            var ctx = GetApiContext();
            MachineData itemTest = await GetNewModel();
            // Create 2 Machine Template
            AssemblyData machineTemplate1 = new AssemblyData()
            {
                BID = TestConstants.BID,
                Name = DateTime.UtcNow + " TEST MATERIAL TEMPLATE 1",
                AssemblyType = AssemblyType.MachineTemplate,
                MachineLayoutTypes = (MachineLayoutType)MachineLayoutType.Sheet,
                Variables = new List<AssemblyVariable>()
                {
                   new AssemblyVariable()
                    {
                        BID = TestConstants.BID,
                        Name = "TestCheckboxVariable",
                        ElementType = AssemblyElementType.Checkbox,
                        DataType = DataType.Boolean,
                        Label = "label",
                        DefaultValue = null,
                        IsFormula = false,
                    }
                }
            };
            var newMachineTemplate1 = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, $"/api/assemblypart", JsonConvert.SerializeObject(machineTemplate1), HttpStatusCode.OK);
            Assert.IsNotNull(newMachineTemplate1);

            // test post
            itemTest.MachineTemplateID = newMachineTemplate1.ID;
            itemTest.Profiles.First().MachineTemplateID = newMachineTemplate1.ID;
            itemTest.Profiles.First().MachineProfileVariables.First().VariableID = newMachineTemplate1.Variables.FirstOrDefault().ID;
            var machineData = await EndpointTests.AssertPostStatusCode<MachineData>(client, $"/api/machinepart", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(machineData);

            AssemblyData model = new AssemblyData()
            {
                Name = "TEST_ASSEMBLY_DATA_" + DateTime.UtcNow,
                IsActive = true,
                BID = TestConstants.BID,
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        BID = TestConstants.BID,
                        Name = "TestVariable",
                        ElementType = AssemblyElementType.LinkedMachine,
                        LinkedMachineID = machineData.ID,
                        IsConsumptionFormula = true,
                        DataType = DataType.MachinePart,
                        Label = "label",
                        DefaultValue = null,
                        ConsumptionDefaultValue = "1",
                        IsFormula = false,
                    }
                }
            };

            //CREATE
            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, "/api/assemblypart", JsonConvert.SerializeObject(model), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);
            var variableID = newMachineTemplate1.Variables.First().ID;
            var profileName = machineData.Profiles.First().Name;
            var simpleMaterialDataJSONCollection = await EndpointTests.AssertGetStatusCode<ICollection<SimpleMaterialData>>(client, $"{apiUrl}/simplelist/byvariable/{variableID}?MachineID={machineData.ID}&ProfileName={profileName}", HttpStatusCode.OK);
            Assert.IsNotNull(simpleMaterialDataJSONCollection);
            Assert.AreEqual(this.materialpart.Name, simpleMaterialDataJSONCollection.FirstOrDefault().DisplayName);

            // test delete
            await EndpointTests.AssertGetStatusCode(client, $"/api/machinepart/{machineData.ID}/action/candelete", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/machinepart/{machineData.ID}", HttpStatusCode.NoContent);
            //TODO: Create an api call to delete the machineprofilevariable data 
            //await EndpointTests.AssertDeleteStatusCode(client, $"/api/machinepartvariable/{variableID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/assemblypart/{assemblyData.ID}", HttpStatusCode.NoContent);
            Assert.IsFalse(ctx.AssemblyData.Any(x => x.ID == assemblyData.ID));
        }
    }

}
