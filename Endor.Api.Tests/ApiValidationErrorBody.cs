﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Api.Tests
{
    public class ApiValidationErrorBody
    {
        public bool HasError { get; set; }
        public string ErrorMessage { get; set; }
    }
}
