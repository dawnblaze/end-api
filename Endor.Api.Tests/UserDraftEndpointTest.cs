﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using System.Globalization;

namespace Endor.Api.Tests
{
    [TestClass]
    public class UserDraftEndpointTest : CommonControllerTestClass
    {
        public const string apiUrl = "/api/draft";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task UserDraftCRUDTest()
        {
            // POST ../api/draft/
            var newUserDraftModel = GetNewUserDraftModel();
            var newlyCreatedUserDraft = await EndpointTests.AssertPostStatusCode<UserDraft>(client, apiUrl, newUserDraftModel, HttpStatusCode.OK);
            Assert.IsNotNull(newlyCreatedUserDraft);

            // GET ../api/draft/
            var userDraft = await EndpointTests.AssertGetStatusCode<UserDraft[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(userDraft);

            // PUT ../api/draft/{draftid}
            newlyCreatedUserDraft.MetaData = "Test Update";
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{newlyCreatedUserDraft.ID}", JsonConvert.SerializeObject(newlyCreatedUserDraft), HttpStatusCode.OK);

            // GET ../api/draft/{draftid}
            var updatedUserDraft = await EndpointTests.AssertGetStatusCode<UserDraft>(client, $"{apiUrl}/{newlyCreatedUserDraft.ID}", HttpStatusCode.OK);
            Assert.AreEqual(newlyCreatedUserDraft.MetaData, updatedUserDraft.MetaData);

            // DELETE ../api/draft/{draftid}
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{newlyCreatedUserDraft.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task UserDraftEmployeeDeleteBlobTest()
        {
            var employees = await EndpointTests.AssertGetStatusCode<SimpleEmployeeData[]>(client, $"/api/employee/simplelist", HttpStatusCode.OK);
            var employee = employees.FirstOrDefault(x => x.ID > 0);

            // POST ../api/draft/
            var newUserDraftModel = GetNewUserDraftModel();
            newUserDraftModel.ObjectCTID = (int)ClassType.Employee;
            newUserDraftModel.ObjectID = (int)employee.ID;
            var newlyCreatedUserDraft = await EndpointTests.AssertPostStatusCode<UserDraft>(client, apiUrl, newUserDraftModel, HttpStatusCode.OK);
            Assert.IsNotNull(newlyCreatedUserDraft);

            // GET ../api/draft/id
            var userDraft = await EndpointTests.AssertGetStatusCode<UserDraft>(client, $"{apiUrl}/"+newlyCreatedUserDraft.ID, HttpStatusCode.OK);
            Assert.IsNotNull(userDraft);

            var fullEmployee = await EndpointTests.AssertGetStatusCode<EmployeeData>(client, $"/api/employee/"+employee.ID, HttpStatusCode.OK);
            fullEmployee.Position = fullEmployee.Position + "1";
            var savedEmployee = await EndpointTests.AssertPutStatusCode<EmployeeData>(client, $"/api/employee/"+employee.ID, JsonConvert.SerializeObject(fullEmployee),  HttpStatusCode.OK);

            userDraft = await EndpointTests.AssertGetStatusCode<UserDraft>(client, $"{apiUrl}/" + newlyCreatedUserDraft.ID, HttpStatusCode.NotFound);

        }

        [TestMethod]
        public async Task UserDraftModifiedDTAndExpiryDTTest()
        {
            // POST ../api/draft/
            var newUserDraftModel = GetNewUserDraftModel();
            var newlyCreatedUserDraft = await EndpointTests.AssertPostStatusCode<UserDraft>(client, apiUrl, newUserDraftModel, HttpStatusCode.OK);
            Assert.IsNotNull(newlyCreatedUserDraft);

            // PUT ../api/draft/{draftid}
            newlyCreatedUserDraft.MetaData = "Test Update";
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{newlyCreatedUserDraft.ID}", JsonConvert.SerializeObject(newlyCreatedUserDraft), HttpStatusCode.OK);

            // GET ../api/draft/id
            var userDraft = await EndpointTests.AssertGetStatusCode<UserDraft>(client, $"{apiUrl}/" + newlyCreatedUserDraft.ID, HttpStatusCode.OK);
            Assert.IsNotNull(userDraft);

            //test ExpirationDT based from ModifiedDT plus 30 days.
            var modifiedDT = userDraft.ModifiedDT;
            var expirationDT = modifiedDT.AddDays(30);
            Assert.AreEqual(expirationDT.Date, userDraft.ExpirationDT.Date);

            // DELETE ../api/draft/{draftid}
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{userDraft.ID}", HttpStatusCode.NoContent);

        }

        private UserDraft GetNewUserDraftModel()
        {
            return new UserDraft()
            {
                BID = 1,
                ExpirationDT = DateTime.Now,
                UserLinkID = 1,
                Description = "Teddy Bear Banner",
                ObjectJSON = "{}"
            };
        }
    }
}
