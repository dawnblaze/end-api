﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Endor.Api.Web.Classes;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Controllers;
using Endor.Api.Web.Classes.Responses;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.DocumentStorage.Models;
using Endor.Api.Web;

namespace Endor.Api.Tests
{
    [TestClass]
    public class ULAEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/ula";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private static DateTime checkDate = new DateTime(2019, 1, 1);


        [TestMethod]
        public async Task TestCheckULA()
        {
            var ctx = GetApiContext();
            var optSvc = new OptionService(ctx, new MigrationHelper(new MockTaskQueuer(GetMockTenantDataCache())));
            ctx.OptionData.RemoveRange(ctx.OptionData.Where(opt => opt.UserLinkID < 0));
            ctx.UserLink.RemoveRange(ctx.UserLink.Where(ul => ul.ID < 0));
            await ctx.SaveChangesAsync();
            var testUserLink = new UserLink()
            {
                BID = TestConstants.BID,
                ID = -99,
                AuthUserID = -99,
                EmployeeID = ctx.EmployeeData.Where(e => e.BID == TestConstants.BID && e.IsActive && e.ID > 0).OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault(),
                UserAccessType = UserAccessType.EmployeeAdministrator
            };
            ctx.UserLink.Add(testUserLink);
            var testUserLink2 = new UserLink()
            {
                BID = TestConstants.BID,
                ID = -98,
                AuthUserID = -98,
                ContactID = ctx.ContactData.Include(c => c.CompanyContactLinks)
                    .Where(e => e.BID == TestConstants.BID && e.ID > 0
                        && e.CompanyContactLinks.FirstOrDefault(l => l.IsActive == true).IsActive == true)
                    .OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault(),
                UserAccessType = UserAccessType.Contact
            };
            ctx.UserLink.Add(testUserLink2);
            var testUserLink3 = new UserLink()
            {
                BID = TestConstants.BID,
                ID = -97,
                AuthUserID = -97,
                EmployeeID = ctx.EmployeeData.Where(e => e.BID == TestConstants.BID && e.IsActive && e.ID > 0).OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault(),
                UserAccessType = UserAccessType.FranchiseStaff
            };
            ctx.UserLink.Add(testUserLink3);
            var testUserLink4 = new UserLink()
            {
                BID = TestConstants.BID,
                ID = -96,
                AuthUserID = -96,
                EmployeeID = ctx.EmployeeData.Where(e => e.BID == TestConstants.BID && e.IsActive && e.ID > 0).OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault(),
                UserAccessType = UserAccessType.EmployeeAdministrator
            };
            ctx.UserLink.Add(testUserLink4);
            await ctx.SaveChangesAsync();


            var option = await optSvc.Get(null, "MULA.AcceptedDT", testUserLink.BID, null, null, null, null, null);
            DateTime? mulaAcceptedDt;

           
            client = EndpointTests.GetHttpClient((short)testUserLink.AuthUserID);
            var result = await EndpointTests.AssertGetStatusCode<CheckULA>(client, $"{apiUrl}/check", HttpStatusCode.OK);
            client = EndpointTests.GetHttpClient((short)testUserLink4.AuthUserID);
            var result4 = await EndpointTests.AssertGetStatusCode<CheckULA>(client, $"{apiUrl}/check", HttpStatusCode.OK);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.EULA.Needed);

            if (option != null && option.Success && option.Value.Value != null)
            {
                DateTime.TryParse(option.Value.Value, out DateTime tempDate);
                mulaAcceptedDt = tempDate;
                if (((mulaAcceptedDt.Value.CompareTo(checkDate) <= 0)))
                {
                    Assert.IsTrue(result.MULA.Needed);
                }
                else
                {
                    Assert.IsFalse(result.MULA.Needed);
                }

                Assert.IsTrue(result.MULA.AcceptedDT.Value.CompareTo(result4.MULA.AcceptedDT.Value) == 0);

            }
            else
            {
                Assert.IsTrue(result.MULA.Needed);
            }
            

            client = EndpointTests.GetHttpClient((short)testUserLink2.AuthUserID);
            var result2 = await EndpointTests.AssertGetStatusCode<CheckULA>(client, $"{apiUrl}/check", HttpStatusCode.OK);
            Assert.IsNotNull(result2);
            Assert.IsTrue(result2.EULA.Needed);
            Assert.IsFalse(result2.MULA.Needed);
            client = EndpointTests.GetHttpClient((short)testUserLink3.AuthUserID);
            var result3 = await EndpointTests.AssertGetStatusCode<CheckULA>(client, $"{apiUrl}/check", HttpStatusCode.OK);
            Assert.IsNotNull(result3);
            Assert.IsFalse(result3.MULA.Needed);
            ctx.UserLink.RemoveRange(ctx.UserLink.Where(ul => ul.ID < 0));
            await ctx.SaveChangesAsync();

        }

        [TestMethod]
        public async Task TestGetEULA()
        {
            short? aid = 1;
            var dmid = new DocumentStorage.Models.DMID()
            {
                ctid = (int?)ClassType.Business,
                classFolder = "association"
            };
            var storage = new StorageContext(TestConstants.BID, BucketRequest.Data, dmid, aid);
            var dm = new DocumentManager(this.GetMockTenantDataCache(), storage);
            var dmItem = new DocumentStorage.Models.DMItem() { Path = "" };
            var ctx = GetApiContext();
            var employeeID = ctx.EmployeeData.Where(e => e.BID == TestConstants.BID && e.IsActive && e.ID > 0).OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault();
            await this.VerifyFile(dm, dmItem, "EULA.html", employeeID);
            await this.VerifyFile(dm, dmItem, "MULA.html", employeeID);
            await this.VerifyFile(dm, dmItem, "CULA.html", employeeID);

            ctx.UserLink.RemoveRange(ctx.UserLink.Where(ul => ul.ID < 0));
            await ctx.SaveChangesAsync();
            var testUserLink = new UserLink()
            {
                BID = TestConstants.BID,
                ID = -99,
                AuthUserID = -99,
                EmployeeID = ctx.EmployeeData.Where(e => e.BID == TestConstants.BID && e.IsActive && e.ID > 0).OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault(),
                UserAccessType = UserAccessType.EmployeeAdministrator
            };
            ctx.UserLink.Add(testUserLink);
            var testUserLink2 = new UserLink()
            {
                BID = TestConstants.BID,
                ID = -98,
                AuthUserID = -98,
                ContactID = ctx.ContactData.Include(c => c.CompanyContactLinks)
                    .Where(e => e.BID == TestConstants.BID && e.ID > 0
                        && e.CompanyContactLinks.FirstOrDefault(l => l.IsActive == true).IsActive == true)
                    .OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault(),
                UserAccessType = UserAccessType.Contact
            };
            ctx.UserLink.Add(testUserLink2);
            var testUserLink3 = new UserLink()
            {
                BID = TestConstants.BID,
                ID = -97,
                AuthUserID = -97,
                EmployeeID = ctx.EmployeeData.Where(e => e.BID == TestConstants.BID && e.IsActive && e.ID > 0).OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault(),
                UserAccessType = UserAccessType.FranchiseStaff
            };
            ctx.UserLink.Add(testUserLink3);
            await ctx.SaveChangesAsync();

            client = EndpointTests.GetHttpClient((short)testUserLink.AuthUserID);
            var result = await EndpointTests.AssertGetStatusCode<GenericResponse<String>>(client, $"{apiUrl}/eula", HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.IsTrue(result.Success);

            client = EndpointTests.GetHttpClient((short)testUserLink2.AuthUserID);
            var result2 = await EndpointTests.AssertGetStatusCode<GenericResponse<String>>(client, $"{apiUrl}/eula", HttpStatusCode.OK);
            Assert.IsNotNull(result2);
            Assert.IsNotNull(result2.Data);
            Assert.IsTrue(result2.Success);

            client = EndpointTests.GetHttpClient((short)testUserLink3.AuthUserID);
            var result3 = await EndpointTests.AssertGetStatusCode<GenericResponse<String>>(client, $"{apiUrl}/mula", HttpStatusCode.OK);
            Assert.IsNotNull(result3);
            Assert.IsNotNull(result3.Data);
            Assert.IsTrue(result3.Success);

            ctx.UserLink.RemoveRange(ctx.UserLink.Where(ul => ul.ID < 0));
            await ctx.SaveChangesAsync();
        }

        [TestMethod]
        public async Task TestAcceptEULA()
        {
            short? aid = 1;
            var dmid = new DocumentStorage.Models.DMID()
            {
                ctid = (int?)ClassType.Business,
                classFolder = "association"
            };
            var storage = new StorageContext(TestConstants.BID, BucketRequest.Data, dmid, aid);
            var dm = new DocumentManager(this.GetMockTenantDataCache(), storage);
            var dmItem = new DocumentStorage.Models.DMItem() { Path = "" };
            var ctx = GetApiContext();
            var employeeID = ctx.EmployeeData.Where(e => e.BID == TestConstants.BID && e.IsActive && e.ID > 0).OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault();
            await this.VerifyFile(dm, dmItem, "EULA.html", employeeID);
            await this.VerifyFile(dm, dmItem, "MULA.html", employeeID);
            await this.VerifyFile(dm, dmItem, "CULA.html", employeeID);


            var optSvc = new OptionService(ctx, new MigrationHelper(new MockTaskQueuer(GetMockTenantDataCache())));
            ctx.UserLink.RemoveRange(ctx.UserLink.Where(ul => ul.ID < 0));
            await ctx.SaveChangesAsync();
            var testUserLink = new UserLink()
            {
                BID = TestConstants.BID,
                ID = -99,
                AuthUserID = -99,
                EmployeeID = ctx.EmployeeData.Where(e => e.BID == TestConstants.BID && e.IsActive && e.ID > 0).OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault(),
                UserAccessType = UserAccessType.EmployeeAdministrator
            };
            ctx.UserLink.Add(testUserLink);
            var testUserLink2 = new UserLink()
            {
                BID = TestConstants.BID,
                ID = -98,
                AuthUserID = -98,
                ContactID = ctx.ContactData.Include(c => c.CompanyContactLinks)
                .Where(e => e.BID == TestConstants.BID && e.ID > 0
                    && e.CompanyContactLinks.FirstOrDefault(l => l.IsActive == true).IsActive == true)
                .OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault(),
                UserAccessType = UserAccessType.Contact
            };
            ctx.UserLink.Add(testUserLink2);
            await ctx.SaveChangesAsync();

            client = EndpointTests.GetHttpClient((short)testUserLink.AuthUserID);
            var result = await EndpointTests.AssertPostStatusCode<GenericResponse<EULAInfo>>(client, $"{apiUrl}/eulaaccepted", null, HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.IsTrue(result.Success);
            Assert.IsNotNull(result.Data.Subject);
            Assert.AreEqual("EULA Accepted", result.Data.Subject);
            Assert.IsNotNull(result.Data.UserName);
            Assert.IsNotNull(result.Data.AcceptedDT);
            Assert.IsNotNull(result.Data.EmployeeID);
            Assert.IsNotNull(result.Data.IP);
            Assert.IsNotNull(result.Data.ComputerName);
            Assert.IsNotNull(result.Data.Browser);
            var option = await optSvc.Get(null, "EULA.AcceptedDT", testUserLink.BID, null, null, testUserLink.ID, null, null);
            Assert.IsNotNull(option.Value.Value);


            client = EndpointTests.GetHttpClient((short)testUserLink2.AuthUserID);
            var result2 = await EndpointTests.AssertPostStatusCode<GenericResponse<EULAInfo>>(client, $"{apiUrl}/eulaaccepted", null, HttpStatusCode.OK);
            Assert.IsNotNull(result2);
            Assert.IsNotNull(result2.Data);
            Assert.IsTrue(result2.Success);
            Assert.IsNotNull(result2.Data.Subject);
            Assert.IsNotNull(result2.Data.UserName);
            Assert.IsNotNull(result2.Data.AcceptedDT);
            Assert.IsNotNull(result2.Data.ContactID);
            Assert.IsNotNull(result2.Data.IP);
            Assert.IsNotNull(result2.Data.ComputerName);
            Assert.IsNotNull(result2.Data.Browser);
            option = await optSvc.Get(null, "EULA.AcceptedDT", testUserLink2.BID, null, null, testUserLink2.ID, null, null);
            Assert.IsNotNull(option.Value.Value);

            ctx.UserLink.RemoveRange(ctx.UserLink.Where(ul => ul.ID < 0));
            await ctx.SaveChangesAsync();
        }
        

        [TestMethod]
        public async Task TestAcceptMULA()
        {
            short? aid = 1;
            var dmid = new DocumentStorage.Models.DMID()
            {
                ctid = (int?)ClassType.Business,
                classFolder = "association"
            };
            var storage = new StorageContext(TestConstants.BID, BucketRequest.Data, dmid, aid);
            var dm = new DocumentManager(this.GetMockTenantDataCache(), storage);
            var dmItem = new DocumentStorage.Models.DMItem() { Path = "" };
            var ctx = GetApiContext();
            var employeeID = ctx.EmployeeData.Where(e => e.BID == TestConstants.BID && e.IsActive && e.ID > 0).OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault();
            await this.VerifyFile(dm, dmItem, "EULA.html", employeeID);
            await this.VerifyFile(dm, dmItem, "MULA.html", employeeID);
            await this.VerifyFile(dm, dmItem, "CULA.html", employeeID);

            var optSvc = new OptionService(ctx, new MigrationHelper(new MockTaskQueuer(GetMockTenantDataCache())));
            ctx.UserLink.RemoveRange(ctx.UserLink.Where(ul => ul.ID < 0));
            await ctx.SaveChangesAsync();
            var testUserLink = new UserLink()
            {
                BID = TestConstants.BID,
                ID = -99,
                AuthUserID = -99,
                EmployeeID = ctx.EmployeeData.Where(e => e.BID == TestConstants.BID && e.IsActive && e.ID > 0).OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault(),
                UserAccessType = UserAccessType.EmployeeAdministrator
            };
            ctx.UserLink.Add(testUserLink);

            var testUserLink2 = new UserLink()
            {
                BID = TestConstants.BID,
                ID = -98,
                AuthUserID = -98,
                EmployeeID = ctx.EmployeeData.Where(e => e.BID == TestConstants.BID && e.IsActive && e.ID > 0).OrderBy(e => e.ID).Select(e => e.ID).FirstOrDefault(),
                UserAccessType = UserAccessType.EmployeeAdministrator
            };
            ctx.UserLink.Add(testUserLink2);

            await ctx.SaveChangesAsync();

            client = EndpointTests.GetHttpClient((short)testUserLink.AuthUserID);
            var result = await EndpointTests.AssertPostStatusCode<GenericResponse<EULAInfo>>(client, $"{apiUrl}/mulaaccepted", null, HttpStatusCode.OK);

            client = EndpointTests.GetHttpClient((short)testUserLink2.AuthUserID);
            var result2 = await EndpointTests.AssertPostStatusCode<GenericResponse<EULAInfo>>(client, $"{apiUrl}/mulaaccepted", null, HttpStatusCode.OK);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.IsTrue(result.Success);
            Assert.IsNotNull(result.Data.Subject);
            Assert.AreEqual("MULA Accepted", result.Data.Subject);
            Assert.IsNotNull(result.Data.UserName);
            Assert.IsNotNull(result.Data.AcceptedDT);
            Assert.IsNotNull(result.Data.EmployeeID);
            Assert.IsNotNull(result.Data.IP);
            Assert.IsNotNull(result.Data.ComputerName);
            Assert.IsNotNull(result.Data.Browser);
            var eulaAccepted = await optSvc.Get(null, "EULA.AcceptedDT", testUserLink.BID, null, null, testUserLink.ID, null, null);
            var option = await optSvc.Get(null, "MULA.AcceptedDT", testUserLink.BID, null, null, /*testUserLink.ID*/ null, null, null);
            var option2 = await optSvc.Get(null, "MULA.AcceptedDT", testUserLink2.BID, null, null, /*testUserLink.ID*/ null, null, null);
            Assert.IsNotNull(option.Value.Value);
            Assert.IsNotNull(option2.Value.Value);
            Assert.IsNotNull(eulaAccepted.Value.Value);

            Assert.IsTrue(option.Value.Value.Equals(option2.Value.Value));

            ctx.UserLink.RemoveRange(ctx.UserLink.Where(ul => ul.ID < 0));
            await ctx.SaveChangesAsync();
        }

        private async Task<bool> VerifyFile(DocumentManager dm, DMItem dmItem, string filepath, int employeeID)
        {
            try
            {
                dmItem.Name = filepath;
                var doc = await dm.ReadTextAsync(dmItem);
            }
            catch (Exception e)
            {
                //must not exist so we need to add one
                var upload = await dm.UploadTextAsync(filepath, "foo", employeeID, (int)ClassType.Employee);
                Console.WriteLine(e.Message);
            }
            return true;
        }
    }
}
