﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Endor.Api.Web.Controllers;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using AssertX = Xunit.Assert;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using Newtonsoft.Json;
using Endor.DocumentStorage.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using Endor.Util;

namespace Endor.Api.Tests
{
    [TestClass]
    public class EndorUtilTests: CommonControllerTestClass
    {
        [TestMethod]
        public void Test_DateTime_Quarter()
        {
            DateTime dt = new DateTime(2019, 06, 11, 18, 55, 30);
            DateTime quarterstart = new DateTime(2019, 04, 01);
            DateTime quarterend = new DateTime(2019, 06, 30, 23, 59, 59);

            DateRange range = new DateRange(StandardDateRangeType.ThisQuarter, dt);

            Assert.AreEqual(quarterstart, range.StartDT, "Quarter Start Date Failed");
            Assert.AreEqual(quarterend, range.EndDT, "Quarter End Date Failed");
        }

        [TestMethod]
        public void Test_FriendlyText_Annotation()
        {
            StandardDateRangeType range = StandardDateRangeType.ThisQuarter;

            Assert.AreEqual(range.DisplayText(), "This Quarter", "Annotation on StandardDateRangeType failed.");
        }

        [TestMethod]
        public void Test_DateTimeTZ_Quarter()
        {
            DateTime dt = new DateTime(2019, 06, 11, 18, 55, 30);
            DateTime quarterstart = new DateTime(2019, 04, 01);
            DateTime quarterend = new DateTime(2019, 06, 30, 23, 59, 59);

            DateRange PST = new DateRangeTZ(StandardDateRangeType.ThisQuarter, dt, "Pacific Standard Time");
            Assert.AreEqual(PST.StartDT, quarterstart.AddHours(7), "PST Quarter Start Date Failed");
            Assert.AreEqual(PST.EndDT, quarterend.AddHours(7), "PST Quarter End Date Failed");


            // now compare timeZone differentials

            DateRange EST = new DateRangeTZ(StandardDateRangeType.ThisQuarter, dt, "Eastern Standard Time");
            double diff = (EST.StartDT - PST.StartDT).TotalHours;

            // compute the timezone offset of the machine the test is running on

            Assert.AreNotEqual(EST, PST, "TimeZones Should have different times");
            Assert.AreEqual(diff, -3.0, "EST and PST timeZones Should only 3 hours apart");
        }
    }
}
