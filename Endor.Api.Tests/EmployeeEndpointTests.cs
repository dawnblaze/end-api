﻿using Endor.Api.Web.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Endor.Models;
using System.Threading.Tasks;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.EF;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Tests
{
    [TestClass]
    public class EmployeeEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/employee";

        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient(rights: new Security.SecurityRight[] { Security.SecurityRight.CanManageEmployees });

        public TestContext TestContext { get; set; }

        [TestInitialize]
        public async Task Initialize()
        {
            short testBID = 1;
            using (var ctx = GetApiContext(testBID))
            {
                var emailAccounts = await ctx.EmailAccountData.Where(x => x.BID == testBID && (x.ID < 0 || x.DomainEmailID < 0)).ToListAsync();
                if (emailAccounts.Count > 0)
                    ctx.RemoveRange(emailAccounts);

                var emailDomains = await ctx.DomainEmail.Where(d => d.BID == testBID && d.ID < 0).ToListAsync();
                if (emailDomains.Count > 0)
                    ctx.RemoveRange(emailDomains);

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task EmployeeGetSimpleEndpointTest()
        {
            await EndpointTests.AssertGetStatusCode<SimpleEmployeeData[]>(client, $"{apiUrl}/simplelist", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task EmployeeCRUDTest()
        {
            // POST A new employee 
            ApiContext ctx = GetApiContext();
            var location = ctx.LocationData.First();
            var postEmployeeRequestModel = GetPOSTEmployee();
            postEmployeeRequestModel.LocationID = location.ID;
            var postedEmployee = await EndpointTests.AssertPostStatusCode<EmployeeData>(client, apiUrl, postEmployeeRequestModel, HttpStatusCode.OK);

            var allEmployees = await EndpointTests.AssertGetStatusCode<EmployeeData[]>(client, apiUrl, HttpStatusCode.OK);

            // Update
            postedEmployee.Last = $"ChangedMyName{DateTime.UtcNow}";

            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{postedEmployee.ID}", JsonConvert.SerializeObject(postedEmployee), HttpStatusCode.OK);
            var updatedEmployee = await EndpointTests.AssertGetStatusCode<EmployeeData>(client, $"{apiUrl}/{postedEmployee.ID}", HttpStatusCode.OK);
            Assert.AreEqual(updatedEmployee.DefaultModule, Module.Accounting);
            Assert.AreEqual(postedEmployee.Last, updatedEmployee.Last);

            // Delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{postedEmployee.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task EmployeeDefaultEmailAccountTests()
        {
            using (var ctx = GetApiContext(1))
            {
                var location = ctx.LocationData.First();
                var postEmployeeRequestModel = GetPOSTEmployee();
                postEmployeeRequestModel.LocationID = location.ID;
                var postedEmployee = await EndpointTests.AssertPostStatusCode<EmployeeData>(client, apiUrl, postEmployeeRequestModel, HttpStatusCode.OK);

                if (!ctx.DomainEmail.Where(d => d.BID == 1 && d.ID == -99).Any())
                {
                    //insert
                    var domainEmail = Utils.GetCorebridgeDomainEmail();
                    domainEmail.ID = -99;
                    ctx.DomainEmail.Add(domainEmail);
                    ctx.SaveChanges();
                }

                var newEmailAccount = Utils.GetEmailAccount(1, -99, -99);
                newEmailAccount.StatusType = EmailAccountStatus.Authorized;
                newEmailAccount.IsActive = true;
                newEmailAccount.EmployeeID = postedEmployee.ID;
                ctx.EmailAccountData.Add(newEmailAccount);
                ctx.SaveChanges();

                var resp = await EndpointTests.AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/-32768/action/setdefault/emailaccount/{newEmailAccount.ID}", null, HttpStatusCode.NotFound);
                Assert.IsNotNull(resp);
                Assert.IsFalse(resp.Success);
                resp = await EndpointTests.AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/{postedEmployee.ID}/action/setdefault/emailaccount/-32768", null, HttpStatusCode.NotFound);
                Assert.IsNotNull(resp);
                Assert.IsFalse(resp.Success);
                resp = await EndpointTests.AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/{postedEmployee.ID}/action/setdefault/emailaccount/{newEmailAccount.ID}", null, HttpStatusCode.OK);
                Assert.IsNotNull(resp);
                Assert.IsTrue(resp.Success);
                resp = await EndpointTests.AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/{postedEmployee.ID}/action/cleardefault/emailaccount", null, HttpStatusCode.OK);
                Assert.IsNotNull(resp);
                Assert.IsTrue(resp.Success);
                resp = await EndpointTests.AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/-32768/action/cleardefault/emailaccount", null, HttpStatusCode.NotFound);
                Assert.IsNotNull(resp);
                Assert.IsFalse(resp.Success);

                newEmailAccount.StatusType = EmailAccountStatus.Inactive;
                ctx.EmailAccountData.Update(newEmailAccount);
                await ctx.SaveChangesAsync();

                await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{postedEmployee.ID}/action/setdefault/emailaccount/{newEmailAccount.ID}", null, HttpStatusCode.Forbidden);
            }
        }

        [TestMethod]
        //Requires Auth Project to be running
        public async Task SendInviteTest()
        {
            var postBody = new
            {
                email = "nobody@cyrious.com",
                UserAccessType = UserAccessType.Employee
            };

            ApiContext ctx = GetApiContext(1);
            EmployeeData emp = ctx.EmployeeData.FirstOrDefault(e => e.IsActive);

            EntityActionChangeResponse resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{emp.ID}/action/sendinvite", JsonConvert.SerializeObject(postBody), HttpStatusCode.OK);

            Assert.IsTrue(Guid.TryParse(resp.Message, out Guid tempID1));
            UserLink userLink = ctx.UserLink.FirstOrDefault(l => l.TempUserID == tempID1);

            try
            {
                Assert.IsNotNull(userLink);
                Assert.AreEqual(emp.ID, userLink.EmployeeID);
                Assert.AreEqual(UserAccessType.Employee, userLink.UserAccessType);

                var postBody2 = new
                {
                    email = "nobody@cyrious.com",
                    UserAccessType = UserAccessType.DeveloperGod
                };

                resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{emp.ID}/action/sendinvite", JsonConvert.SerializeObject(postBody2), HttpStatusCode.OK);
                Assert.IsTrue(Guid.TryParse(resp.Message, out Guid tempID2));
                userLink = ctx.UserLink.FirstOrDefault(l => l.TempUserID == tempID2);

                Assert.IsNull(ctx.UserLink.FirstOrDefault(l => l.TempUserID == tempID1));

                Assert.IsNotNull(userLink);
                Assert.AreEqual(emp.ID, userLink.EmployeeID);
                Assert.AreEqual(UserAccessType.DeveloperGod, userLink.UserAccessType);
            }
            finally
            {
                ctx.UserLink.Remove(userLink);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        //Requires Auth Project to be running
        public async Task RevokeAccessTest()
        {
            var postBody = new
            {
                email = "nobody@cyrious.com",
                UserAccessType = UserAccessType.Employee
            };

            ApiContext ctx = GetApiContext(1);
            EmployeeData emp = ctx.EmployeeData.FirstOrDefault(e => e.IsActive);

            EntityActionChangeResponse resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{emp.ID}/action/sendinvite", JsonConvert.SerializeObject(postBody), HttpStatusCode.OK);

            Assert.IsTrue(Guid.TryParse(resp.Message, out Guid tempID1));
            UserLink userLink = ctx.UserLink.FirstOrDefault(l => l.TempUserID == tempID1);

            try
            {
                Assert.IsNotNull(userLink);
                Assert.AreEqual(emp.ID, userLink.EmployeeID);
                Assert.AreEqual(UserAccessType.Employee, userLink.UserAccessType);

                var postBody2 = new
                {
                    email = "nobody@cyrious.com",
                    UserAccessType = UserAccessType.DeveloperGod
                };

                resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{emp.ID}/action/sendinvite", JsonConvert.SerializeObject(postBody2), HttpStatusCode.OK);
                Assert.IsTrue(Guid.TryParse(resp.Message, out Guid tempID2));
                userLink = ctx.UserLink.FirstOrDefault(l => l.TempUserID == tempID2);

                Assert.IsNull(ctx.UserLink.FirstOrDefault(l => l.TempUserID == tempID1));

                Assert.IsNotNull(userLink);
                Assert.AreEqual(emp.ID, userLink.EmployeeID);
                Assert.AreEqual(UserAccessType.DeveloperGod, userLink.UserAccessType);

                resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{emp.ID}/action/revokeaccess", JsonConvert.SerializeObject(postBody2), HttpStatusCode.OK);
                userLink = ctx.UserLink.FirstOrDefault(l => l.EmployeeID == emp.ID);
                Assert.IsNull(userLink);

            }
            finally
            {
                if (userLink != null)
                {
                    ctx.UserLink.Remove(userLink);
                    await ctx.SaveChangesAsync();
                }
            }
        }

        [TestMethod]
        //Requires Auth Project to be running
        public async Task SendResetPasswordEmailTest()
        {
            var postBody = new
            {
                email = "nobody@cyrious.com",
                securityrole = "Employee"
            };

            ApiContext ctx = GetApiContext(1);
            EmployeeData emp = ctx.EmployeeData.FirstOrDefault(e => e.IsActive);

            EntityActionChangeResponse resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{emp.ID}/action/sendresetpasswordemail", JsonConvert.SerializeObject(postBody), HttpStatusCode.OK);

            Assert.IsFalse(resp.HasError);
        }

        [TestMethod]
        public async Task EmployeeCloneTest()
        {
            ApiContext ctx = GetApiContext(1);
            EmployeeData emp = ctx.EmployeeData.FirstOrDefault(e => e.IsActive);
            EmployeeClone ec = new EmployeeClone()
            {
                FirstName = $"FirstCLONED",
                LastName = $"LastCLONED"
                // email and permissions are a separate task
                //Email = "unit.test@noreply.com",
                //PermissionsID = (emp.UserLinks != null && emp.UserLinks.Count > 0) ? emp.UserLinks.FirstOrDefault().ID : 0
            };

            EmployeeData resp = await EndpointTests.AssertPostStatusCode<EmployeeData>(client, $"{apiUrl}/{emp.ID}/clone", JsonConvert.SerializeObject(ec), HttpStatusCode.OK);
            Assert.IsNotNull(resp);
            Assert.AreEqual("FirstCLONED", resp.First);
            Assert.AreEqual("LastCLONED", resp.Last);

            // next needs to be implemented on other tasks
            /*
            var newEmail = resp.EmployeeLocators.Where(e => 
                e.LocatorType == (byte)LocatorType.Email
                && e.MetaDataXML.Contains("unit.test@noreply.com")).FirstOrDefault();
            Assert.IsNotNull(newEmail);

            if (ec.PermissionsID != 0)
            {
                Assert.IsNotNull(resp.UserLinks);
                Assert.IsTrue(resp.UserLinks.Count > 0);
                Assert.AreEqual(ec.PermissionsID, resp.UserLinks.FirstOrDefault().ID);
            }
            */

            // Output vary on EmployeeTeamLinks. Currently, it's not allowed to delete Employee if there's EmployeeTeamLink
            HttpStatusCode expectedOutput = resp.EmployeeTeamLinks.Count() > 0 ? HttpStatusCode.BadRequest : HttpStatusCode.NoContent;

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{resp.ID}", expectedOutput);
        }

        [TestMethod]
        public async Task EmployeeTimeCardActionEndPoints()
        {
            var employee = await EndpointTests.AssertGetStatusCode<EmployeeData[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            var employeeID = employee.FirstOrDefault()?.ID ?? 0;

            // POST ../api/employee/{employeeid}/action/clockin
            var employeeClockInResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{employeeID}/action/clockin", null, HttpStatusCode.OK);
            Assert.IsTrue(employeeClockInResponse.Success);

            // POST ../api/employee/{employeeid}/action/clockontask
            var employeeClockOnResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{employeeID}/action/clockontask", null, HttpStatusCode.OK);
            Assert.IsTrue(employeeClockOnResponse.Success);

            // POST ../api/employee/{employeeid}/action/clockofftask
            var employeeClockOffResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{employeeID}/action/clockofftask", null, HttpStatusCode.OK);
            Assert.IsTrue(employeeClockOffResponse.Success);

            // POST ../api/employee/{employeeid}/action/clockout
            var employeeClockOutResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{employeeID}/action/clockout", null, HttpStatusCode.OK);
            Assert.IsTrue(employeeClockOutResponse.Success);

            // GET ../api/employee/{employeeid}/timecard
            var employeeTimeCard = await EndpointTests.AssertGetStatusCode<TimeCard[]>(client, $"{apiUrl}/{employeeID}/timecard", HttpStatusCode.OK);
            Assert.IsNotNull(employeeTimeCard);

            // GET ../api/employee/{employeeid}/timecardsummary
            var employeeTimeCardSummary = await EndpointTests.AssertGetStatusCode<GenericResponse<EmployeeTimeCardSummaryData>>(client, $"{apiUrl}/{employeeID}/timecardsummary", HttpStatusCode.OK);
            Assert.IsTrue(employeeTimeCardSummary.Success && employeeTimeCardSummary.Data != null);

        }

        [TestMethod]
        public async Task TestEmployeeDeleteWithForceEndpoint()
        {
            ApiContext ctx = GetApiContext();

            LocationData location = ctx.LocationData.First();
            EmployeeData newEmployeeRequestModel = GetPOSTEmployee();
            newEmployeeRequestModel.LocationID = location.ID;

            var postedEmployee = await EndpointTests.AssertPostStatusCode<EmployeeData>(client, apiUrl, newEmployeeRequestModel, HttpStatusCode.OK);

            var testUserLink = await Utils.GetAuthUserLink(ctx);
            if (testUserLink != null && testUserLink.UserAccessType < UserAccessType.SupportManager)
            {
                testUserLink.UserAccessType = UserAccessType.SupportManager;
                ctx.SaveChanges();
            }

            AlertDefinition newAlertDefinition =
                await ctx.AlertDefinition.Where(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID).FirstOrDefaultAsync();

            if (newAlertDefinition == null)
            {
                var triggers = await EndpointTests.AssertGetStatusCode<SystemAutomationTriggerDefinition[]>(client, "/api/system/alert/triggers", HttpStatusCode.OK);

                newAlertDefinition = new AlertDefinition()
                {
                    ID = (short)-postedEmployee.ID,
                    Name = "Unit Test - New Alert Definition",
                    EmployeeID = postedEmployee.ID,
                    HasImage = false,
                    IsActive = true,
                    IsSystem = false,
                    TriggerID = triggers.FirstOrDefault()?.ID ?? 0,
                    DataType = DataType.Order,
                    BID = TestConstants.BID
                };

                await ctx.AlertDefinition.AddAsync(newAlertDefinition);
            }

            BoardEmployeeLink boardEmployeeLink =
                await ctx.BoardEmployeeLink.Where(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID).FirstOrDefaultAsync();

            if (boardEmployeeLink == null)
            {
                boardEmployeeLink = new BoardEmployeeLink()
                {
                    BID = TestConstants.BID,
                    BoardID = (await ctx.BoardDefinitionData.FirstAsync()).ID,
                    EmployeeID = postedEmployee.ID,
                    SortIndex = 1,
                    ModuleType = Module.Management
                };

                await ctx.BoardEmployeeLink.AddAsync(boardEmployeeLink);
            }

            EmployeeLocator employeeLocator =
                await ctx.EmployeeLocator.Where(x => x.BID == TestConstants.BID && x.ParentID == postedEmployee.ID).FirstOrDefaultAsync();

            if (employeeLocator == null)
            {
                employeeLocator = new EmployeeLocator()
                {
                    ID = -postedEmployee.ID,
                    BID = TestConstants.BID,
                    ParentID = postedEmployee.ID,
                    LocatorType = 2,
                    Locator = "(801) 836-7117",
                    RawInput = "8018367117",
                    SortIndex = 1,
                    IsVerified = false,
                    IsValid = true
                };

                await ctx.EmployeeLocator.AddAsync(employeeLocator);
            }

            EmployeeTeamLink employeeTeamLink =
                await ctx.EmployeeTeamLink.Where(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID).FirstOrDefaultAsync();

            if (employeeTeamLink == null)
            {
                employeeTeamLink = new EmployeeTeamLink()
                {
                    BID = TestConstants.BID,
                    EmployeeID = postedEmployee.ID,
                    TeamID = (await ctx.EmployeeTeam.FirstAsync()).ID,
                    RoleID = (await ctx.EmployeeRole.FirstAsync()).ID
                };

                await ctx.EmployeeTeamLink.AddAsync(employeeTeamLink);
            }

            TimeCard timeCard =
                await ctx.TimeCard.Where(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID).FirstOrDefaultAsync();

            if (timeCard == null)
            {
                timeCard = new TimeCard()
                {
                    ID = (short)-postedEmployee.ID,
                    BID = TestConstants.BID,
                    EmployeeID = postedEmployee.ID,
                    StartDT = DateTime.UtcNow,
                    EndDT = DateTime.UtcNow,
                    TimeInMin = 0,
                    PaidTimeInMin = 0,
                    IsClosed = true,
                    IsAdjusted = true,
                    AdjustedByEmployeeID = postedEmployee.ID,
                    AdjustedDT = DateTime.UtcNow,
                    MetaData = "Test"
                };

                await ctx.TimeCard.AddAsync(timeCard);
            }

            TimeCardDetail timeCardDetail =
                await ctx.TimeCardDetail.Where(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID).FirstOrDefaultAsync();

            if (timeCardDetail == null)
            {
                timeCardDetail = new TimeCardDetail()
                {
                    ID = (short)-postedEmployee.ID,
                    BID = TestConstants.BID,
                    EmployeeID = postedEmployee.ID,
                    StartDT = DateTime.UtcNow,
                    AdjustedByEmployeeID = postedEmployee.ID,
                    MetaData = "Test",
                    TimeCardID = timeCard.ID
                };

                await ctx.TimeCardDetail.AddAsync(timeCardDetail);
            }

            Message message = new Message()
            {
                ID = -postedEmployee.ID,
                ModifiedDT = DateTime.UtcNow,
                ReceivedDT = DateTime.UtcNow,
                Channels = MessageChannelType.None,
                Subject = "The Quick",
                HasBody = true,
                Body = "<b>brown fox</>",
                EmployeeID = postedEmployee.ID,
                BID = TestConstants.BID
            };

            var participantInfoFrom = GetParticipantInfo();
            participantInfoFrom.ParticipantRoleType = MessageParticipantRoleType.From;
            participantInfoFrom.EmployeeID = postedEmployee.ID;

            var participantInfoTo = GetParticipantInfo();
            participantInfoTo.ParticipantRoleType = MessageParticipantRoleType.To;
            participantInfoTo.EmployeeID = postedEmployee.ID;

            message.EmployeeID = postedEmployee.ID;
            message.Participants = new[] { participantInfoFrom, participantInfoTo };

            // this one should add new messageheader and messageparticipantinfo
            var postedMessageHeader = await EndpointTests.AssertPostStatusCode<Message>(client, "/api/message", message, HttpStatusCode.OK);

            MessageParticipantInfo messageParticipantInfo =
                await ctx.MessageParticipantInfo.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID);

            MessageDeliveryRecord messageDeliveryRecord =
                await ctx.MessageDeliveryRecord.Where(x => x.BID == TestConstants.BID && x.ParticipantID == messageParticipantInfo.ID).FirstOrDefaultAsync();

            if (messageDeliveryRecord == null)
            {
                messageDeliveryRecord = new MessageDeliveryRecord()
                {
                    ID = -postedEmployee.ID,
                    BID = TestConstants.BID,
                    ParticipantID = messageParticipantInfo.ID,
                    AttemptNumber = 10,
                    WasSuccessful = false
                };

                await ctx.MessageDeliveryRecord.AddAsync(messageDeliveryRecord);
            }

            UserLink userLink =
                await ctx.UserLink.Where(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID).FirstOrDefaultAsync();

            if (userLink == null)
            {
                userLink = new UserLink()
                {
                    ID = (short)-postedEmployee.ID,
                    BID = TestConstants.BID,
                    EmployeeID = postedEmployee.ID,
                    UserAccessType = UserAccessType.DeveloperGod
                };

                await ctx.UserLink.AddAsync(userLink);
            }

            await ctx.SaveChangesAsync();

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{postedEmployee.ID}?force=true", HttpStatusCode.OK);

            Assert.IsNull(await ctx.UserLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID));
            Assert.IsNull(await ctx.MessageHeader.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID));
            Assert.IsNull(await ctx.MessageParticipantInfo.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID));
            Assert.IsNull(await ctx.TimeCardDetail.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID));
            Assert.IsNull(await ctx.TimeCard.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID));
            Assert.IsNull(await ctx.EmployeeTeamLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID));
            Assert.IsNull(await ctx.EmployeeLocator.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ParentID == postedEmployee.ID));
            Assert.IsNull(await ctx.BoardEmployeeLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID));
            Assert.IsNull(await ctx.AlertDefinition.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID));
        }

        [TestMethod]
        public async Task TestCanDelete()
        {
            ApiContext ctx = GetApiContext();
            ctx.DashboardData.RemoveRange(ctx.DashboardData.Where(d => d.UserLinkID == -99));
            ctx.UserLink.RemoveRange(ctx.UserLink.Where(d => d.ID == -99));
            await ctx.SaveChangesAsync();

            // POST A new employee 
            var location = ctx.LocationData.First();
            var postEmployeeRequestModel = GetPOSTEmployee();
            postEmployeeRequestModel.LocationID = location.ID;
            var postedEmployee = await EndpointTests.AssertPostStatusCode<EmployeeData>(client, apiUrl, postEmployeeRequestModel, HttpStatusCode.OK);
            ctx.BoardDefinitionData.RemoveRange(ctx.BoardDefinitionData.Where(bdf => bdf.ID == -99));
            ctx.SaveChanges();
            BoardDefinitionData bd = new BoardDefinitionData()
            {
                BID = TestConstants.BID,
                ID = -99,
                Name = "Test"
            };
            ctx.BoardDefinitionData.Add(bd);
            await ctx.SaveChangesAsync();
            BoardEmployeeLink boardEmployeeLink =
                await ctx.BoardEmployeeLink.Where(x => x.BID == TestConstants.BID && x.EmployeeID == postedEmployee.ID).FirstOrDefaultAsync();

            if (boardEmployeeLink == null)
            {
                boardEmployeeLink = new BoardEmployeeLink()
                {
                    BID = TestConstants.BID,
                    BoardID = (await ctx.BoardDefinitionData.FirstAsync()).ID,
                    EmployeeID = postedEmployee.ID,
                    SortIndex = 1,
                    ModuleType = Module.Management
                };

                await ctx.BoardEmployeeLink.AddAsync(boardEmployeeLink);
            }

            // Setup Dashboard
            var userLink = new UserLink()
            {
                ID = -99,
                BID = TestConstants.BID,
                EmployeeID = postedEmployee.ID,
                UserName = "johndoe",
                UserAccessType = UserAccessType.SupportManager
            };
            ctx.UserLink.Add(userLink);
            await ctx.SaveChangesAsync();

            var newDashboard = Utils.GetTestDashboard("TestDashboard");
            newDashboard.ID = -99;
            newDashboard.UserLinkID = userLink.ID;
            ctx.DashboardData.Add(newDashboard);
            await ctx.SaveChangesAsync();

            #region CanDelete Conflict test
            BooleanResponse canDeleteResponse = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{postedEmployee.ID}/action/candelete", HttpStatusCode.OK);
            Assert.IsTrue(canDeleteResponse.Success);
            Assert.IsFalse(canDeleteResponse.Value.Value);
            #endregion

            #region CanDelete test
            ctx.BoardEmployeeLink.Remove(boardEmployeeLink);
            ctx.BoardDefinitionData.RemoveRange(ctx.BoardDefinitionData.Where(bdf => bdf.ID == -99));
            ctx.SaveChanges();
            canDeleteResponse = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{postedEmployee.ID}/action/candelete", HttpStatusCode.OK);
            Assert.IsTrue(canDeleteResponse.Success);
            Assert.IsTrue(canDeleteResponse.Value.Value);
            #endregion

            // Delete
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{postedEmployee.ID}", HttpStatusCode.NoContent);
            var testDashboard = ctx.DashboardData.FirstOrDefault(i => i.UserLinkID == -99);
            Assert.IsNull(testDashboard);
        }

        private EmployeeData GetPOSTEmployee()
        {
            return new EmployeeData()
            {
                BID = 1,
                IsActive = true,
                First = "John",
                Last = "Doe",
                DefaultModule = Module.Accounting
            };
        }

        private MessageParticipantInfo GetParticipantInfo()
        {
            return new MessageParticipantInfo()
            {
                ID = -99,
                BID = TestConstants.BID,
                Channel = MessageChannelType.Email,
                IsMergeField = false,
                ModifiedDT = DateTime.UtcNow,
            };
        }

        [TestCleanup]
        public void Teardown()
        {
            try
            {
                var ctx = GetApiContext();
                var employee = ctx.EmployeeData.Where(x => x.BID == TestConstants.BID && x.First == GetPOSTEmployee().First && x.Last == GetPOSTEmployee().Last).FirstOrDefault();

                if (TestContext.TestName == "TestEmployeeDeleteWithForceEndpoint")
                {
                    var userLink = ctx.UserLink.FirstOrDefault(x => x.BID == TestConstants.BID && x.EmployeeID == employee.ID);
                    if (userLink != null)
                        ctx.RemoveRange(userLink);

                    var messageHeader = ctx.MessageHeader.FirstOrDefault(x => x.BID == TestConstants.BID && x.EmployeeID == employee.ID);
                    if (messageHeader != null)
                        ctx.RemoveRange(messageHeader);

                    var messageParticipantInfo = ctx.MessageParticipantInfo.FirstOrDefault(x => x.BID == TestConstants.BID && x.EmployeeID == employee.ID);
                    if (messageParticipantInfo != null)
                        ctx.RemoveRange(messageParticipantInfo);

                    var messageDeliveryRecord = ctx.MessageDeliveryRecord.FirstOrDefault(x => x.BID == TestConstants.BID && x.ID == -employee.ID);
                    if (messageDeliveryRecord != null)
                        ctx.RemoveRange(messageDeliveryRecord);

                    var timeCardDetail = ctx.TimeCardDetail.FirstOrDefault(x => x.BID == TestConstants.BID && x.EmployeeID == employee.ID);
                    if (timeCardDetail != null)
                        ctx.RemoveRange(timeCardDetail);

                    var timeCard = ctx.TimeCard.FirstOrDefault(x => x.BID == TestConstants.BID && x.EmployeeID == employee.ID);
                    if (timeCard != null)
                        ctx.RemoveRange(timeCard);

                    var employeeTeamLink = ctx.EmployeeTeamLink.FirstOrDefault(x => x.BID == TestConstants.BID && x.EmployeeID == employee.ID);
                    if (employeeTeamLink != null)
                        ctx.RemoveRange(employeeTeamLink);

                    var employeeLocator = ctx.EmployeeLocator.FirstOrDefault(x => x.BID == TestConstants.BID && x.ParentID == employee.ID);
                    if (employeeLocator != null)
                        ctx.RemoveRange(employeeLocator);

                    var boardEmployeeLink = ctx.BoardEmployeeLink.FirstOrDefault(x => x.BID == TestConstants.BID && x.EmployeeID == employee.ID);
                    if (boardEmployeeLink != null)
                        ctx.RemoveRange(boardEmployeeLink);

                    var alertDefinition = ctx.AlertDefinition.FirstOrDefault(x => x.BID == TestConstants.BID && x.EmployeeID == employee.ID);
                    if (alertDefinition != null)
                        ctx.RemoveRange(alertDefinition);
                }

                ctx.RemoveRange(employee);
                ctx.SaveChanges();
            }
            catch { }
        }
    }
}
