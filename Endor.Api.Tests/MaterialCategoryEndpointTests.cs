﻿using Endor.Api.Web.Classes;
using Endor.Models;
using Endor.Units;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class MaterialCategoryEndpointTests
    {
        public const string apiUrl = "/api/materialcategory";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestMaterialDataName;
        private string NewTestMaterialCategoryName;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            this.NewTestMaterialDataName = DateTime.UtcNow + " TEST PART MACHINE DATA";
            this.NewTestMaterialCategoryName = DateTime.UtcNow + " TEST PART MACHINE DATA";
        }

        [TestMethod]
        public async Task TestMaterialCategorySingleEndpoints()
        {
            MaterialCategory testObject = await GetPostAssertNewMaterialCategory();

            // GET api/PaymentTerms/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleMaterialCategory[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleMaterialCategory sga in simpleList)
            {
                if (sga.DisplayName == testObject.Name)
                {
                    testObject.ID = sga.ID;
                }
            }

            testObject.Description = "test test";

            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + testObject.ID, JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            //test: expect OK
            var updatedMaterialCategory = await EndpointTests.AssertGetStatusCode<MaterialCategory>(client, $"{apiUrl}/" + testObject.ID, HttpStatusCode.OK);
            Assert.AreEqual(updatedMaterialCategory.Description, testObject.Description);

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testObject.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestMaterialCategoryFilters()
        {
            //setup: get new material category
            MaterialCategory testObject = await GetPostAssertNewMaterialCategory();

            //test: make sure it exists when filtering simple list for isactive=true
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleMaterialCategory[]>(client, $"{apiUrl}/SimpleList?isActive=true", HttpStatusCode.OK);
            Assert.IsNotNull(simpleList.FirstOrDefault(x => x.ID == testObject.ID));

            //test: expect only active categories
            var active = await EndpointTests.AssertGetStatusCode<MaterialCategory[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(active.All(x => x.IsActive));
            Assert.IsTrue(active.Count() > 0);

            //test: set isactive false
            testObject.IsActive = false;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + testObject.ID, JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            //test: expect only inactive categories
            var inactive = await EndpointTests.AssertGetStatusCode<MaterialCategory[]>(client, $"{apiUrl}?IsActive=false", HttpStatusCode.OK);
            Assert.IsTrue(inactive.All(x => !x.IsActive));
            Assert.IsTrue(inactive.Count() > 0);

            //setup: link child
            MaterialData testChild = await GetPostAssertNewMaterialData();
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{testObject.ID}/action/linkmaterial/{testChild.ID}", "", HttpStatusCode.OK);

            //test: make sure there are no expanded materials
            var noneChild = await EndpointTests.AssertGetStatusCode<MaterialCategory>(client, $"{apiUrl}/{testObject.ID}", HttpStatusCode.OK);
            Assert.IsTrue(noneChild.Materials == null || noneChild.Materials.Count() == 0);
            Assert.IsTrue(noneChild.SimpleMaterials == null || noneChild.SimpleMaterials.Count() == 0);

            //test: make sure there are expanded materials
            var fullChild = await EndpointTests.AssertGetStatusCode<MaterialCategory>(client, $"{apiUrl}/{testObject.ID}?MaterialParts=Full", HttpStatusCode.OK);
            Assert.IsTrue(fullChild.Materials != null && fullChild.Materials.Count() > 0);
            Assert.IsTrue(fullChild.SimpleMaterials == null || fullChild.SimpleMaterials.Count() == 0);

            //test: make sure there are  expanded simplematerials
            var simpleChild = await EndpointTests.AssertGetStatusCode<MaterialCategory>(client, $"{apiUrl}/{testObject.ID}?MaterialParts=Simple", HttpStatusCode.OK);
            Assert.IsTrue(simpleChild.Materials == null || simpleChild.Materials.Count() == 0);
            Assert.IsTrue(simpleChild.SimpleMaterials != null && simpleChild.SimpleMaterials.Count() > 0);
        }

        private async Task<MaterialCategory> GetPostAssertNewMaterialCategory()
        {
            MaterialCategory testObject = new MaterialCategory()
            {
                Name = this.NewTestMaterialDataName,
                IsActive = true,
                BID = 1,
                Description = "blah blah"
            };

            //test: expect OK
            var newCategory = await EndpointTests.AssertPostStatusCode<MaterialCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);
            return newCategory;
        }

        [TestMethod]
        public async Task TestMaterialCategoryActionEndpoints()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleMaterialData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            MaterialData testMaterialData = new MaterialData()
            {
                Name = this.NewTestMaterialDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,
                ConsumptionUnit = Unit.None,
                EstimatingConsumptionMethod = MaterialConsumptionMethod.Each,
                EstimatingCostingMethod = MaterialCostingMethod.Manual,
                HasImage = false,
                PhysicalMaterialType = MaterialPhysicalType.None,
                InvoiceText = "Test Material Data",
                SKU = "TMD123"
            };
            var newMaterialData = await EndpointTests.AssertPostStatusCode<MaterialData>(client, $"/api/materialpart", JsonConvert.SerializeObject(testMaterialData), HttpStatusCode.OK);

            MaterialCategory testMaterialCat = new MaterialCategory()
            {
                Name = this.NewTestMaterialCategoryName,
                IsActive = true,
                BID = 1,
                Description = "blah blah"
            };

            //test: expect OK
            var newMaterialCategory = await EndpointTests.AssertPostStatusCode<MaterialCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(testMaterialCat), HttpStatusCode.OK);

            //POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newMaterialCategory.ID}/action/setinactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newMaterialCategory.ID}/action/setactive", HttpStatusCode.OK);

            //POST candelete
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newMaterialCategory.ID}/action/candelete", HttpStatusCode.OK);

            //POST link material
            EntityActionChangeResponse linkResponse;
            linkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newMaterialCategory.ID}/action/linkmaterial/{newMaterialData.ID}", HttpStatusCode.OK);
            Assert.IsTrue(linkResponse.Success);
            //unable to link twice
            linkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newMaterialCategory.ID}/action/linkmaterial/{newMaterialData.ID}", HttpStatusCode.BadRequest);
            Assert.IsFalse(linkResponse.Success);

            //POST unlink material
            EntityActionChangeResponse unlinkResponse;
            unlinkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newMaterialCategory.ID}/action/unlinkmaterial/{newMaterialData.ID}", HttpStatusCode.OK);
            Assert.IsTrue(unlinkResponse.Success);
            //unable to unlink twice
            unlinkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newMaterialCategory.ID}/action/unlinkmaterial/{newMaterialData.ID}", HttpStatusCode.BadRequest);
            Assert.IsFalse(unlinkResponse.Success);
        }

        [TestMethod]
        public async Task TestMaterialCategoryCanDelete()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleMaterialData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            MaterialData testMaterialData = new MaterialData()
            {
                Name = this.NewTestMaterialDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,
                ConsumptionUnit = Unit.None,
                EstimatingConsumptionMethod = MaterialConsumptionMethod.Each,
                EstimatingCostingMethod = MaterialCostingMethod.Manual,
                HasImage = false,
                PhysicalMaterialType = MaterialPhysicalType.None,
                InvoiceText = "Test Material Data",
                SKU = "TMD123"
            };
            var newMaterialData = await EndpointTests.AssertPostStatusCode<MaterialData>(client, $"/api/materialpart", JsonConvert.SerializeObject(testMaterialData), HttpStatusCode.OK);

            MaterialCategory testMaterialCat = new MaterialCategory()
            {
                Name = this.NewTestMaterialCategoryName,
                IsActive = true,
                BID = 1,
                Description = "blah blah"
            };

            //test: expect OK
            var newMaterialCategory = await EndpointTests.AssertPostStatusCode<MaterialCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(testMaterialCat), HttpStatusCode.OK);

            //POST candelete
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newMaterialCategory.ID}/action/candelete", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestMaterialCategoryLinkingEndpoints()
        {
            //setup
            MaterialCategory testCategory = await GetPostAssertNewMaterialCategory();
            MaterialData testChild = await GetPostAssertNewMaterialData();

            //test: link from category to child
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{testCategory.ID}/action/linkmaterial/{testChild.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{testCategory.ID}/action/unlinkmaterial/{testChild.ID}", HttpStatusCode.OK);

            //test: link from child to category
            await EndpointTests.AssertPostStatusCode(client, $"/api/materialpart/{testChild.ID}/action/linkmaterialcategory/{testCategory.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"/api/materialpart/{testChild.ID}/action/unlinkmaterialcategory/{testCategory.ID}", HttpStatusCode.OK);
        }

        private async Task<MaterialData> GetPostAssertNewMaterialData()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleMaterialData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            MaterialData testObject = new MaterialData()
            {
                Name = this.NewTestMaterialDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,
                InvoiceText = "blah blah",
                Description = "blah"
            };

            //test: expect OK
            var newObject = await EndpointTests.AssertPostStatusCode<MaterialData>(client, $"api/materialpart", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);
            return newObject;
        }

        [TestCleanup]
        public void Teardown()
        {
            //you can actually do test-specific things like this
            //switch (TestContext.TestName)
            //{
            //    case "TestPaymentTermSingleEndpoints":
            //    case "TestFiltering":
            //        // do stuff
            //        break;
            //    default:
            //        break;
            //}
            this.DeleteTestRecord();
        }

        private void DeleteTestRecord()
        {
            string newTestMaterialCategoryName = NewTestMaterialCategoryName;
            string newTestMaterialDataName = NewTestMaterialDataName;
            Task.Run(async () =>
            {
                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                SimpleMaterialCategory[] result = JsonConvert.DeserializeObject<SimpleMaterialCategory[]>(responseString);
                if (result != null)
                {
                    SimpleMaterialCategory sga = result.FirstOrDefault(x => x.DisplayName.Contains(newTestMaterialCategoryName));
                    if (sga != null)
                    {
                        await client.DeleteAsync($"{apiUrl}/{sga.ID}?Force=true");
                    }
                }
                response = await client.GetAsync($"api/materialpart/SimpleList");
                responseString = await response.Content.ReadAsStringAsync();
                SimpleMaterialData[] dataResult = JsonConvert.DeserializeObject<SimpleMaterialData[]>(responseString);
                if (result != null)
                {
                    SimpleMaterialData sga = dataResult.FirstOrDefault(x => x.DisplayName.Contains(newTestMaterialDataName));
                    if (sga != null)
                    {
                        await client.DeleteAsync($"api/materialpart/{sga.ID}");
                    }
                }
            }).Wait();
        }
    }
}