﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Endor.Api.Web.Classes;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Services;

namespace Endor.Api.Tests
{
    [TestClass]
    public class LocationGoalTests : CommonControllerTestClass
    {
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public const string apiUrl = "/api/locationgoal";

        [TestInitialize]
        public override void Init()
        {
            base.Init();
            var ctx = GetApiContext();
            var oldEntries = ctx.LocationGoal.Where(lg => lg.ID <= -95);
            foreach (var entry in oldEntries)
            {
                ctx.Remove(entry);
            }
            ctx.SaveChanges();
        }

        [TestMethod]
        public async Task TestGetLocationGoal()
        {
            var ctx = GetApiContext();
            var locationID = ctx.LocationData.FirstOrDefault().ID;
            LocationGoal locationGoal = new LocationGoal()
            {
                BID = 1,
                ID = -99,
                Year = 2018,
                Actual = 10000.00m,
                LocationID = locationID,
                Month = 1,
                IsYearlyTotal = true
            };
            LocationGoal locationGoal1 = new LocationGoal()
            {
                BID = 1,
                ID = -98,
                Year = 2018,
                Actual = 12000.00m,
                LocationID = locationID,
                Month = 2,
                IsYearlyTotal = true
            };
            ctx.LocationGoal.Add(locationGoal);
            ctx.LocationGoal.Add(locationGoal1);
            ctx.SaveChanges();

            try
            {
                var retrievedLocationGoals = await EndpointTests.AssertGetStatusCode<GenericResponse<List<LocationGoal>>>(client, $"{apiUrl}?Year=2018", HttpStatusCode.OK);
                Assert.IsNotNull(retrievedLocationGoals);
                Assert.IsTrue(retrievedLocationGoals.Data.Count() >= 1);

                var retrievedLocationMonthGoals = await EndpointTests.AssertGetStatusCode<LocationGoal>(client, $"{apiUrl}/{locationID}/2018/2", HttpStatusCode.OK);
                Assert.IsNotNull(retrievedLocationMonthGoals);

                var retrievedLocationGoals2 = await EndpointTests.AssertGetStatusCode<GenericResponse<List<LocationGoal>>>(client, $"{apiUrl}?Year=1500", HttpStatusCode.OK);
                Assert.IsNotNull(retrievedLocationGoals2);
                Assert.IsTrue(retrievedLocationGoals2.Data.Count() >= 1);

            }
            finally
            {
                ctx.LocationGoal.Remove(locationGoal);
                ctx.LocationGoal.Remove(locationGoal1);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public async Task TestGetMultipleLocationGoal()
        {
            var ctx = GetApiContext();
            var locationID = ctx.LocationData.FirstOrDefault().ID;
            var locationID2 = ctx.LocationData.ToArray()[2].ID;
            LocationGoal locationGoal = new LocationGoal()
            {
                BID = 1,
                ID = -99,
                Year = 2018,
                Actual = 10000.00m,
                LocationID = locationID,
                Month = 1,
                IsYearlyTotal = true
            };
            LocationGoal locationGoal1 = new LocationGoal()
            {
                BID = 1,
                ID = -98,
                Year = 2018,
                Actual = 12000.00m,
                LocationID = locationID2,
                Month = 2,
                IsYearlyTotal = true
            };
            ctx.LocationGoal.Add(locationGoal);
            ctx.LocationGoal.Add(locationGoal1);
            ctx.SaveChanges();

            try
            {
                var retrievedLocationGoals = await EndpointTests.AssertGetStatusCode<GenericResponse<List<LocationGoal>>>(client, $"{apiUrl}?LocationID={locationID}&LocationID={locationID2}&Year=2018", HttpStatusCode.OK);
                Assert.IsNotNull(retrievedLocationGoals);
                Assert.IsTrue(retrievedLocationGoals.Data.Count() >= 1);

                var retrievedLocationGoals2 = await EndpointTests.AssertGetStatusCode<GenericResponse<List<LocationGoal>>>(client, $"{apiUrl}?LocationID={locationID}&Year=2018", HttpStatusCode.OK);
                Assert.IsNotNull(retrievedLocationGoals2);
                Assert.IsTrue(retrievedLocationGoals2.Data.Where(lg => lg.LocationID == locationID).Count() == retrievedLocationGoals2.Data.Count());

                var retrievedLocationGoals3 = await EndpointTests.AssertGetStatusCode<GenericResponse<List<LocationGoal>>>(client, $"{apiUrl}?LocationID={locationID2}&Year=2018", HttpStatusCode.OK);
                Assert.IsNotNull(retrievedLocationGoals3);
                Assert.IsTrue(retrievedLocationGoals3.Data.Where(lg => lg.LocationID == locationID2).Count() == retrievedLocationGoals3.Data.Count());

            }
            finally
            {
                ctx.LocationGoal.Remove(locationGoal);
                ctx.LocationGoal.Remove(locationGoal1);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public async Task TestPutLocationGoal()
        {
            var ctx = GetApiContext();
            var locationID = ctx.LocationData.Where(l => l.BID == 1).FirstOrDefault().ID;
            var actual = 20000.00m;
            var budgeted = 19000.00m;
            
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{locationID}/2018/3?budgeted={budgeted}&actual={actual}", HttpStatusCode.OK);

            var retrievedLocationMonthGoals = await EndpointTests.AssertGetStatusCode<LocationGoal>(client, $"{apiUrl}/{locationID}/2018/3", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedLocationMonthGoals);
            Assert.IsTrue(retrievedLocationMonthGoals.Budgeted == budgeted);
            Assert.IsTrue(retrievedLocationMonthGoals.Actual == actual);

            budgeted = 17500.00m;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{locationID}/2018/3?budgeted={budgeted}", HttpStatusCode.OK);
            retrievedLocationMonthGoals = await EndpointTests.AssertGetStatusCode<LocationGoal>(client, $"{apiUrl}/{locationID}/2018/3", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedLocationMonthGoals);
            Assert.IsTrue(retrievedLocationMonthGoals.Budgeted == budgeted);
            Assert.IsTrue(retrievedLocationMonthGoals.Actual == actual);

            LocationGoal ctxGoals = ctx.LocationGoal.FirstOrDefault(g => g.BID == TestConstants.BID && g.ID == retrievedLocationMonthGoals.ID);
            ctx.Remove(ctxGoals);
            ctx.SaveChanges();
        }

        [TestCleanup]
        public void Teardown()
        {
            /*try
            {
                var ctx = GetApiContext();
                ctx.RemoveRange(ctx.LocationGoal.Where(x => x.ID <= -95));
                ctx.SaveChanges();
            }
            catch { }*/
        }
    }
}
