﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Request;
using PaymentApplication = Endor.Models.PaymentApplication;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Classes.Responses;

namespace Endor.Api.Tests
{
    [TestClass]
    [TestCategory("GL")]
    [TestCategory("Payment")]
    public class PaymentRefundTests : PaymentTestHelper
    {
        public const string apiUrl = "/api/payment/refund";
        public const string paymentApiUrl = "/api/payment";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }
        public PaymentMaster paymentMaster;

        [TestInitialize]
        public async Task Initialize()
        {
            await CleanUpTestData();

        }

        private async Task OldTestInitialize()
        {
            await Teardown();
            var ctx = GetApiContext();
            var testOrder = GetNewOrder();
            testOrder.PaymentPaid = (decimal)testOrder.PriceProductTotal;
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            var paymentMethod = GetNewPaymentMethod();
            var paymentMaster = GetNewPaymentMaster(
                amount: (decimal)createdOrder.PriceProductTotal, //Full payment
                orderID: createdOrder.ID
            );
            ctx.PaymentMethod.Add(paymentMethod);
            ctx.PaymentMaster.Add(paymentMaster);
            this.paymentMaster = paymentMaster;
            ctx.SaveChanges();
        }

        [TestCleanup]
        public async Task Teardown()
        {
            await CleanUpTestData();
        }

        private PaymentRefundRequest GetRefundRequest(int companyID, int? masterID, int? orderID, decimal amount)
        {
            return new PaymentRefundRequest()
            {
                PaymentTransactionType = PaymentTransactionType.Refund,
                MasterPaymentID = masterID,
                CompanyID = companyID,
                Amount = -amount,
                Applications = new List<PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest()
                    {
                        OrderID = orderID,
                        Amount = -amount,
                    }
                },
            };
        }

        private OrderData GetNewOrder()
        {
            var ctx = GetApiContext();
            var locationID = ctx.LocationData.First().ID;
            var companyID = ctx.CompanyData.First().ID;
            var taxGroupID = ctx.TaxGroup.First().ID;
            var order = Utils.GetTestOrderData();
            order.CompanyID = companyID;
            order.PickupLocationID = locationID;
            order.TaxGroupID = taxGroupID;
            order.ProductionLocationID = locationID;
            order.PriceProductTotal = 20m;
            order.InvoiceNumber = -999;
            return order;
        }

        private PaymentMethod GetNewPaymentMethod()
        {
            var ctx = GetApiContext();
            return new PaymentMethod()
            {
                BID = 1,
                ID = -99,
                IsActive = true,
                Name = "TestPaymentMethodForApplication",
                PaymentMethodType = PaymentMethodType.CreditCard,
                DepositGLAccountID = ctx.GLAccount.First().ID,
            };
        }

        private PaymentMaster GetNewPaymentMaster(decimal amount, int? orderID = null)
        {
            var ctx = GetApiContext();
            var masterID = -99;
            var changeInRefCredit = orderID == null ? amount : 0;
            var locationID = ctx.LocationData.First().ID;
            var companyID = ctx.CompanyData.First().ID;
            return new PaymentMaster()
            {
                BID = 1,
                ID = masterID,
                LocationID = locationID,
                ReceivedLocationID = locationID,
                Amount = amount,
                RefBalance = amount,
                ChangeInRefCredit = changeInRefCredit,
                PaymentTransactionType = (byte)PaymentTransactionType.Payment,
                CompanyID = companyID,
                PaymentApplications = new List<PaymentApplication>()
                {
                    new PaymentApplication()
                    {
                        BID = 1,
                        ID = -99,
                        PaymentType = PaymentMethodType.Visa,
                        PaymentTransactionType = (byte)PaymentTransactionType.Payment,
                        CompanyID = companyID,
                        ApplicationGroupID = -99,
                        LocationID = locationID,
                        Amount = amount,
                        RefBalance = amount,
                        OrderID = orderID,
                        PaymentMethodID = 1
                    }
                }
            };
        }

        private PaymentRefundRequest GetNewRequest()
        {
            var ctx = GetApiContext();

            return new PaymentRefundRequest()
            {
                PaymentTransactionType = PaymentTransactionType.Refund,
                MasterPaymentID = -99,
                //PaymentMethodID = (int)PaymentMethodType.Visa,
                Amount = -2.05m,
                Applications = new List<Web.Classes.Request.PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest()
                    {
                        OrderID = this.paymentMaster.PaymentApplications.First().OrderID,
                        Amount = -2.05m
                    }
                }
            };
        }




        [TestMethod]
        public async Task RefundOrderAdjustment()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 2.05m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetRefundRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                orderID: order.ID,
                amount: refund_amount
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            order = ctx.OrderData.First(t => t.ID == order.ID);

            Assert.AreEqual(amount-refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
        }


        [TestMethod]
        public async Task RefundOrderWithFullAmountAdjustment()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 20m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetRefundRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                orderID: order.ID,
                amount: refund_amount
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);


            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            Assert.AreEqual(amount - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
        }



        [TestMethod]
        public async Task RefundOrderClosedAdjustment()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 2.05m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            // Simulate closed Order
            order = ctx.OrderData.First(x => x.ID == order.ID);
            order.OrderStatusID = OrderOrderStatus.OrderClosed;
            ctx.OrderData.Update(order);
            Assert.AreEqual(order.PaymentBalanceDue, 0m);
            ctx.SaveChanges();

            var request = GetRefundRequest(
                companyID: master.CompanyID,
                masterID: master.ID,
                orderID: order.ID,
                amount: refund_amount
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            ctx.Entry<OrderData>(order).Reload();

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            Assert.IsTrue(order.PaymentBalanceDue > 0);
            Assert.AreEqual(OrderOrderStatus.OrderInvoiced, order.OrderStatusID);
        }



        [TestMethod]
        public async Task RefundShouldUpdateMastersBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var master = await CreatePaymentForOrder(
                ID: -99, 
                orderID: null,
                amount: 100m, 
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetRefundRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: null,
                amount: 70
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            Assert.AreEqual(30, master.RefBalance);
            Assert.AreEqual(30, master.ChangeInRefCredit);
        }


        [TestMethod]
        public async Task RefundShouldUpdatePaymentApplications()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var master = await CreatePaymentForOrder(
                ID: -99, 
                orderID: null,
                amount: 100m, 
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetRefundRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: null,
                amount: 70
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            //Assert PaymentApplication entry created and first application was updated
            master = ctx.PaymentMaster.Include(t => t.PaymentApplications).First(t => t.ID == master.ID);
            var paymentApplication = master.PaymentApplications.FirstOrDefault(t => t.PaymentTransactionType == (byte)PaymentTransactionType.Payment);
            var refundApplication = master.PaymentApplications.FirstOrDefault(t => t.PaymentTransactionType == (byte)PaymentTransactionType.Refund);
            Assert.IsNotNull(refundApplication);
            Assert.AreEqual(-70, refundApplication.Amount);
            Assert.AreEqual(0, refundApplication.RefBalance);
            Assert.AreEqual(30, paymentApplication.RefBalance);
            Assert.AreEqual(true, paymentApplication.HasAdjustments);
        }

        [TestMethod]
        public async Task RefundShouldUpdateCompanysBalance()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var master = await CreatePaymentForOrder(
                ID: -99, 
                orderID: null,
                amount: 100m, 
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetRefundRequest(
                companyID: master.CompanyID, 
                masterID: master.ID,
                orderID: null,
                amount: 70
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentApplyCreditResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            Assert.AreEqual(30, company.RefundableCredit);
        }

        [TestMethod]
        public async Task ValidateMasterHasAmount()
        {
            await OldTestInitialize();
            var ctx = GetApiContext();
            var testRequest = GetNewRequest();
            testRequest.PaymentTransactionType = PaymentTransactionType.Refund_to_Refundable_Credit;

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            paymentMaster.Amount = -1;
            ctx.PaymentMaster.Update(paymentMaster);
            ctx.Entry(paymentMaster).Property(t => t.ValidToDT).IsModified = false;
            ctx.SaveChanges();

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.BadRequest);

        }

        [TestMethod]
        public async Task ValidateMasterSumsApplicationsRefBalance()
        {
            await OldTestInitialize();
            var ctx = GetApiContext();
            var testRequest = GetNewRequest();
            testRequest.PaymentTransactionType = PaymentTransactionType.Refund_to_Refundable_Credit;

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            paymentMaster.RefBalance = -1;
            ctx.PaymentMaster.Update(paymentMaster);
            ctx.Entry(paymentMaster).Property(t => t.ValidToDT).IsModified = false;
            ctx.SaveChanges();

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.BadRequest);

        }

        [TestMethod]
        public async Task ValidateMasterNonRefBalanceMustBeNull()
        {
            await OldTestInitialize();
            var ctx = GetApiContext();
            var testRequest = GetNewRequest();
            testRequest.PaymentTransactionType = PaymentTransactionType.Refund_to_Refundable_Credit;

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            paymentMaster.NonRefBalance = 100;
            ctx.PaymentMaster.Update(paymentMaster);
            ctx.Entry(paymentMaster).Property(t => t.ValidToDT).IsModified = false;
            ctx.SaveChanges();

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.BadRequest);

        }

        [TestMethod]
        public async Task ValidateMasterChangeInNonRefCreditMustBeNull()
        {
            await OldTestInitialize();
            var ctx = GetApiContext();
            var testRequest = GetNewRequest();
            testRequest.PaymentTransactionType = PaymentTransactionType.Refund_to_Refundable_Credit;

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            paymentMaster.ChangeInNonRefCredit = 100;
            ctx.PaymentMaster.Update(paymentMaster);
            ctx.Entry(paymentMaster).Property(t => t.ValidToDT).IsModified = false;
            ctx.SaveChanges();

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public async Task ValidateApplicationLocationMatchesMaster()
        {
            await OldTestInitialize();
            var ctx = GetApiContext();
            var testRequest = GetNewRequest();
            testRequest.PaymentTransactionType = PaymentTransactionType.Refund_to_Refundable_Credit;

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            paymentMaster.LocationID = ctx.LocationData.FirstOrDefault().ID;
            ctx.PaymentMaster.Update(paymentMaster);
            ctx.Entry(paymentMaster).Property(t => t.ValidToDT).IsModified = false;
            ctx.SaveChanges();

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public async Task ValidateApplicationCompanyMatchesMaster()
        {
            await OldTestInitialize();
            var ctx = GetApiContext();
            var testRequest = GetNewRequest();
            testRequest.PaymentTransactionType = PaymentTransactionType.Refund_to_Refundable_Credit;

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            paymentMaster.CompanyID = ctx.CompanyData.FirstOrDefault().ID;
            ctx.PaymentMaster.Update(paymentMaster);
            ctx.Entry(paymentMaster).Property(t => t.ValidToDT).IsModified = false;
            ctx.SaveChanges();

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public async Task ValidateApplicationNonRefBalanceIsNull()
        {
            await OldTestInitialize();
            var ctx = GetApiContext();
            var testRequest = GetNewRequest();
            testRequest.PaymentTransactionType = PaymentTransactionType.Refund_to_Refundable_Credit;

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            var paymentApplication = paymentMaster.PaymentApplications.First();
            paymentApplication.NonRefBalance = 100;
            ctx.PaymentMaster.Update(paymentMaster);
            ctx.Entry(paymentMaster).Property(t => t.ValidToDT).IsModified = false;
            ctx.SaveChanges();

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public async Task ValidateApplicationAmountIsMoreThanOrderBalance()
        {
            await OldTestInitialize();
            var ctx = GetApiContext();

            var testRequest = GetNewRequest();
            testRequest.PaymentTransactionType = PaymentTransactionType.Refund_to_Refundable_Credit;
            testRequest.Applications.First().Amount = -500m;
            testRequest.Amount = -500m;

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.BadRequest);

        }

        [TestMethod]
        public async Task RefundTwoOrdersAdjustment()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 2.05m;
            var testOrder = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var testOrder2 = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            
            var paymentMethod = GetNewPaymentMethod();
            var paymentMaster = GetNewPaymentMaster(
                amount: amount, //Full payment
                orderID: testOrder.ID
            );
            paymentMaster.CompanyID = company.ID;
            paymentMaster.PaymentApplications.First().CompanyID = company.ID;
            paymentMaster.PaymentApplications.Add(
                new PaymentApplication()
                {
                    BID = 1,
                    ID = -98,
                    PaymentType = PaymentMethodType.Visa,
                    PaymentTransactionType = (byte)PaymentTransactionType.Payment,
                    CompanyID = company.ID,
                    ApplicationGroupID = -99,
                    LocationID = testOrder.LocationID,
                    Amount = amount,
                    RefBalance = amount,
                    OrderID = testOrder2.ID,
                    PaymentMethodID = 1
                }
            );
            paymentMaster.RefBalance += amount;
            paymentMaster.Amount += amount;

            ctx.PaymentMethod.Add(paymentMethod);
            ctx.PaymentMaster.Add(paymentMaster);
            this.paymentMaster = paymentMaster;
            ctx.SaveChanges();

            var order = ctx.OrderData.First(t => t.ID == this.paymentMaster.PaymentApplications.FirstOrDefault().OrderID);
            Assert.AreEqual(amount, order.PaymentPaid);
            Assert.AreEqual(0, order.PaymentBalanceDue);

            var testRequest = GetRefundRequest(
                companyID: paymentMaster.CompanyID,
                masterID: paymentMaster.ID,
                orderID: testOrder.ID,
                amount: refund_amount
            );
            testRequest.Amount = testRequest.Amount * 2;
            testRequest.Applications.Add(
            new PaymentApplicationRequest()
            {
                OrderID = testOrder2.ID,
                Amount = -2.05m
            }
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.AreEqual((testRequest.Amount / 2), result.MasterPayments.First().Applications.Last().Amount);
            var result2 = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result2);
            Assert.AreEqual((testRequest.Amount / 2), result2.MasterPayments.First().Applications.Last().Amount);
            ctx.Entry(order).Reload();
            Assert.AreEqual(amount + (testRequest.Amount), order.PaymentPaid);
            Assert.AreEqual((-testRequest.Amount), order.PaymentBalanceDue);
            var order2 = ctx.OrderData.First(t => t.ID == testOrder.ID);
            Assert.AreEqual(amount + (testRequest.Amount), order2.PaymentPaid);
            Assert.AreEqual((-testRequest.Amount), order2.PaymentBalanceDue);


        }

        [TestMethod]
        public async Task RefundOneOrderOneMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 2.05m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetRefundRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);

            Assert.AreEqual(amount - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            Assert.AreEqual(amount - refund_amount, master.RefBalance);
        }

        [TestMethod]
        public async Task RefundOneOrderTwoMasters()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 15m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount/2,
                isRefundable: true,
                companyID: company.ID
            );
            var master2 = await CreatePaymentForOrder(
                ID: -98,
                orderID: order.ID,
                amount: amount/2,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetRefundRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            order = ctx.OrderData.First(t => t.ID == order.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            master2 = ctx.PaymentMaster.First(t => t.ID == master2.ID);

            Assert.AreEqual(amount - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            Assert.AreEqual(0m, master.RefBalance);
            Assert.AreEqual(amount/2 - (refund_amount-amount/2), master2.RefBalance);
        }

        [TestMethod]
        public async Task RefundTwoOrdersOneMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 15m;
            var testOrder = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var testOrder2 = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );

            var paymentMethod = GetNewPaymentMethod();
            var paymentMaster = GetNewPaymentMaster(
                amount: amount, //Full payment
                orderID: testOrder.ID
            );
            paymentMaster.CompanyID = company.ID;
            paymentMaster.PaymentApplications.First().CompanyID = company.ID;
            paymentMaster.PaymentApplications.Add(
                new PaymentApplication()
                {
                    BID = 1,
                    ID = -98,
                    PaymentType = PaymentMethodType.Visa,
                    PaymentTransactionType = (byte)PaymentTransactionType.Payment,
                    CompanyID = company.ID,
                    ApplicationGroupID = -99,
                    LocationID = testOrder.LocationID,
                    Amount = amount,
                    RefBalance = amount,
                    OrderID = testOrder2.ID,
                    PaymentMethodID = 1
                }
            );
            paymentMaster.RefBalance += amount;
            paymentMaster.Amount += amount;

            ctx.PaymentMethod.Add(paymentMethod);
            ctx.PaymentMaster.Add(paymentMaster);
            this.paymentMaster = paymentMaster;
            ctx.SaveChanges();

            var order = ctx.OrderData.First(t => t.ID == this.paymentMaster.PaymentApplications.FirstOrDefault().OrderID);
            Assert.AreEqual(amount, order.PaymentPaid);
            Assert.AreEqual(0, order.PaymentBalanceDue);

            var testRequest = GetRefundRequest(
                companyID: paymentMaster.CompanyID,
                masterID: null,
                orderID: testOrder.ID,
                amount: refund_amount
            );
            testRequest.Amount = testRequest.Amount * 2;
            testRequest.Applications.Add(
                new PaymentApplicationRequest()
                {
                    OrderID = testOrder2.ID,
                    Amount = -refund_amount
                }
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.AreEqual((testRequest.Amount / 2), result.MasterPayments.First().Applications.Last().Amount);
            ctx.Entry(order).Reload();
            Assert.AreEqual(amount + (testRequest.Amount/2), order.PaymentPaid);
            Assert.AreEqual((-testRequest.Amount/2), order.PaymentBalanceDue);
            var order2 = ctx.OrderData.First(t => t.ID == testOrder.ID);
            Assert.AreEqual(amount + (testRequest.Amount/2), order2.PaymentPaid);
            Assert.AreEqual((-testRequest.Amount/2), order2.PaymentBalanceDue);


        }

        [TestMethod]
        public async Task RefundTwoOrdersTwoMasters()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 25m;
            var testOrder = await CreateOrder(
                companyID: company.ID,
                price: amount*2,
                amountPaid: amount*2
            );
            var testOrder2 = await CreateOrder(
                companyID: company.ID,
                price: amount*2,
                amountPaid: amount*2
            );

            var paymentMethod = GetNewPaymentMethod();
            var paymentMaster = GetNewPaymentMaster(
                amount: amount, //Full payment
                orderID: testOrder.ID
            );
            paymentMaster.CompanyID = company.ID;
            paymentMaster.PaymentApplications.First().CompanyID = company.ID;
            paymentMaster.PaymentApplications.Add(
                new PaymentApplication()
                {
                    BID = 1,
                    ID = -98,
                    PaymentType = PaymentMethodType.Visa,
                    PaymentTransactionType = (byte)PaymentTransactionType.Payment,
                    CompanyID = company.ID,
                    ApplicationGroupID = -99,
                    LocationID = testOrder.LocationID,
                    Amount = amount,
                    RefBalance = amount,
                    OrderID = testOrder2.ID,
                    PaymentMethodID = 1
                }
            );
            paymentMaster.RefBalance += amount;
            paymentMaster.Amount += amount;

            var paymentMaster2 = GetNewPaymentMaster(
                amount: amount, //Full payment
                orderID: testOrder.ID
            );
            paymentMaster2.ID = -98;
            paymentMaster2.CompanyID = company.ID;
            paymentMaster2.PaymentApplications.First().CompanyID = company.ID;
            paymentMaster2.PaymentApplications.First().ID = -97;
            paymentMaster2.PaymentApplications.Add(
                new PaymentApplication()
                {
                    BID = 1,
                    ID = -96,
                    PaymentType = PaymentMethodType.Visa,
                    PaymentTransactionType = (byte)PaymentTransactionType.Payment,
                    CompanyID = company.ID,
                    ApplicationGroupID = -99,
                    LocationID = testOrder.LocationID,
                    Amount = amount,
                    RefBalance = amount,
                    OrderID = testOrder2.ID,
                    PaymentMethodID = 1
                }
            );
            paymentMaster2.RefBalance += amount;
            paymentMaster2.Amount += amount;

            ctx.PaymentMethod.Add(paymentMethod);
            ctx.PaymentMaster.Add(paymentMaster);
            ctx.PaymentMaster.Add(paymentMaster2);
            this.paymentMaster = paymentMaster;
            ctx.SaveChanges();

            var order = ctx.OrderData.First(t => t.ID == this.paymentMaster.PaymentApplications.FirstOrDefault().OrderID);
            Assert.AreEqual(amount*2, order.PaymentPaid);
            Assert.AreEqual(0, order.PaymentBalanceDue);

            var testRequest = GetRefundRequest(
                companyID: paymentMaster.CompanyID,
                masterID: null,
                orderID: testOrder.ID,
                amount: refund_amount
            );
            testRequest.Amount = refund_amount * 2;
            testRequest.Applications.Add(
                new PaymentApplicationRequest()
                {
                    OrderID = testOrder2.ID,
                    Amount = -refund_amount
                }
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.AreEqual((-amount), result.MasterPayments.First().Applications.Last().Amount);
            Assert.AreEqual((-refund_amount+amount), result.MasterPayments.Last().Applications.Last().Amount);
            ctx.Entry(order).Reload();
            Assert.AreEqual(2*amount - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            var order2 = ctx.OrderData.First(t => t.ID == testOrder.ID);
            Assert.AreEqual(2 * amount - refund_amount, order2.PaymentPaid);
            Assert.AreEqual(refund_amount, order2.PaymentBalanceDue);


        }

        [TestMethod]
        public async Task RefundPullsTooMuchFromAnOrderShouldFail()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 20.05m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetRefundRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);

            order = ctx.OrderData.First(t => t.ID == order.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);

            Assert.AreEqual(amount, order.PaymentPaid);
            Assert.AreEqual(0, order.PaymentBalanceDue);
            Assert.AreEqual(amount, master.RefBalance);
        }

        [TestMethod]
        public async Task RefundPullsTooMuchFromOneOfTheOrdersShouldFail()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 20m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var order2 = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            var master2 = await CreatePaymentForOrder(
                ID: -98,
                orderID: order.ID,
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetRefundRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount
            );
            request.Amount = -(refund_amount + 100m);
            request.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                    OrderID = order.ID,
                    Amount = -refund_amount,
                },
                new PaymentApplicationRequest()
                {
                    OrderID = order2.ID,
                    Amount = -100m,
                },
            };
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);

            order = ctx.OrderData.First(t => t.ID == order.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);

            Assert.AreEqual(amount, order.PaymentPaid);
            Assert.AreEqual(0, order.PaymentBalanceDue);
            Assert.AreEqual(amount, master.RefBalance);
        }

        [TestMethod]
        public async Task RefundPullsFromNonRefTransaction()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 20m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount,
                isRefundable: false,
                companyID: company.ID
            );
            var request = GetRefundRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);

            order = ctx.OrderData.First(t => t.ID == order.ID);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);

            Assert.AreEqual(amount, order.PaymentPaid);
            Assert.AreEqual(0, order.PaymentBalanceDue);
            Assert.AreEqual(0, master.RefBalance);
        }

        [TestMethod]
        public async Task RefundTwiceFromSameMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 2.05m;
            var testOrder = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );

            var paymentMethod = GetNewPaymentMethod();
            var paymentMaster = GetNewPaymentMaster(
                amount: amount, //Full payment
                orderID: testOrder.ID
            );
            paymentMaster.CompanyID = company.ID;
            paymentMaster.PaymentApplications.First().CompanyID = company.ID;

            ctx.PaymentMethod.Add(paymentMethod);
            ctx.PaymentMaster.Add(paymentMaster);
            this.paymentMaster = paymentMaster;
            ctx.SaveChanges();

            var order = ctx.OrderData.First(t => t.ID == this.paymentMaster.PaymentApplications.FirstOrDefault().OrderID);
            Assert.AreEqual(amount, order.PaymentPaid);
            Assert.AreEqual(0, order.PaymentBalanceDue);

            var testRequest = GetRefundRequest(
                companyID: paymentMaster.CompanyID,
                masterID: null,
                orderID: testOrder.ID,
                amount: refund_amount
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.AreEqual((testRequest.Amount ), result.MasterPayments.First().Applications.Last().Amount);
            var result2 = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result2);
            Assert.AreEqual((testRequest.Amount), result2.MasterPayments.First().Applications.Last().Amount);
            ctx.Entry(order).Reload();
            Assert.AreEqual(amount + (testRequest.Amount*2), order.PaymentPaid);
            Assert.AreEqual((-testRequest.Amount*2), order.PaymentBalanceDue);


        }

        [TestMethod]
        public async Task RefundOneISCOneMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 2.05m;
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: null,
                amount: amount,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetRefundRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: null,
                amount: refund_amount
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);

            Assert.AreEqual(amount - refund_amount, master.RefBalance);
        }

        [TestMethod]
        public async Task RefundOneISCTwoMasters()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 15m;
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: null,
                amount: amount / 2,
                isRefundable: true,
                companyID: company.ID
            );
            var master2 = await CreatePaymentForOrder(
                ID: -98,
                orderID: null,
                amount: amount / 2,
                isRefundable: true,
                companyID: company.ID
            );
            var request = GetRefundRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: null,
                amount: refund_amount
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.OK);
            Assert.IsNotNull(result);

            var paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").First(t => t.BID == 1 && t.ID == -99);
            master = ctx.PaymentMaster.First(t => t.ID == master.ID);
            master2 = ctx.PaymentMaster.First(t => t.ID == master2.ID);

            Assert.AreEqual(0m, master.RefBalance);
            Assert.AreEqual(amount / 2 - (refund_amount - amount / 2), master2.RefBalance);
        }

        [TestMethod]
        public async Task RefundTwoISCsOneMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 15m;

            var paymentMethod = GetNewPaymentMethod();
            var paymentMaster = GetNewPaymentMaster(
                amount: amount, //Full payment
                orderID: null
            );
            paymentMaster.CompanyID = company.ID;
            paymentMaster.PaymentApplications.First().CompanyID = company.ID;
            paymentMaster.PaymentApplications.Add(
                new PaymentApplication()
                {
                    BID = 1,
                    ID = -98,
                    PaymentType = PaymentMethodType.Visa,
                    PaymentTransactionType = (byte)PaymentTransactionType.Payment,
                    CompanyID = company.ID,
                    ApplicationGroupID = -99,
                    LocationID = company.LocationID,
                    Amount = amount,
                    RefBalance = amount,
                    OrderID = null,
                    PaymentMethodID = 1
                }
            );
            paymentMaster.RefBalance += amount;
            paymentMaster.Amount += amount;

            ctx.PaymentMethod.Add(paymentMethod);
            ctx.PaymentMaster.Add(paymentMaster);
            this.paymentMaster = paymentMaster;
            ctx.SaveChanges();

            var testRequest = GetRefundRequest(
                companyID: paymentMaster.CompanyID,
                masterID: null,
                orderID: null,
                amount: refund_amount
            );
            testRequest.Amount = testRequest.Amount * 2;
            testRequest.Applications.Add(
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = -refund_amount
                }
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.AreEqual((testRequest.Amount / 2), result.MasterPayments.First().Applications.Last().Amount);
        }

        [TestMethod]
        public async Task RefundTwoISCsTwoMasters()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 25m;

            var paymentMethod = GetNewPaymentMethod();
            var paymentMaster = GetNewPaymentMaster(
                amount: amount, //Full payment
                orderID: null
            );
            paymentMaster.CompanyID = company.ID;
            paymentMaster.PaymentApplications.First().CompanyID = company.ID;
            paymentMaster.PaymentApplications.Add(
                new PaymentApplication()
                {
                    BID = 1,
                    ID = -98,
                    PaymentType = PaymentMethodType.Visa,
                    PaymentTransactionType = (byte)PaymentTransactionType.Payment,
                    CompanyID = company.ID,
                    ApplicationGroupID = -99,
                    LocationID = company.LocationID,
                    Amount = amount,
                    RefBalance = amount,
                    OrderID = null,
                    PaymentMethodID = 1
                }
            );
            paymentMaster.RefBalance += amount;
            paymentMaster.Amount += amount;

            var paymentMaster2 = GetNewPaymentMaster(
                amount: amount, //Full payment
                orderID: null
            );
            paymentMaster2.ID = -98;
            paymentMaster2.CompanyID = company.ID;
            paymentMaster2.PaymentApplications.First().CompanyID = company.ID;
            paymentMaster2.PaymentApplications.First().ID = -97;
            paymentMaster2.PaymentApplications.Add(
                new PaymentApplication()
                {
                    BID = 1,
                    ID = -96,
                    PaymentType = PaymentMethodType.Visa,
                    PaymentTransactionType = (byte)PaymentTransactionType.Payment,
                    CompanyID = company.ID,
                    ApplicationGroupID = -99,
                    LocationID = company.LocationID,
                    Amount = amount,
                    RefBalance = amount,
                    OrderID = null,
                    PaymentMethodID = 1
                }
            );
            paymentMaster2.RefBalance += amount;
            paymentMaster2.Amount += amount;

            ctx.PaymentMethod.Add(paymentMethod);
            ctx.PaymentMaster.Add(paymentMaster);
            ctx.PaymentMaster.Add(paymentMaster2);
            this.paymentMaster = paymentMaster;
            ctx.SaveChanges();

            var testRequest = GetRefundRequest(
                companyID: paymentMaster.CompanyID,
                masterID: null,
                orderID: null,
                amount: refund_amount
            );
            testRequest.Amount = refund_amount * 2;
            testRequest.Applications.Add(
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = -refund_amount
                }
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.AreEqual(-((2*amount)- refund_amount), result.MasterPayments.First().Applications.Last().Amount);
            Assert.AreEqual((-(2*refund_amount) + (2*amount)), result.MasterPayments.Last().Applications.Last().Amount);


        }

        [TestMethod]
        public async Task RefundISCTwiceFromSameMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 2.05m;

            var paymentMethod = GetNewPaymentMethod();
            var paymentMaster = GetNewPaymentMaster(
                amount: amount, //Full payment
                orderID: null
            );
            paymentMaster.CompanyID = company.ID;
            paymentMaster.PaymentApplications.First().CompanyID = company.ID;

            ctx.PaymentMethod.Add(paymentMethod);
            ctx.PaymentMaster.Add(paymentMaster);
            this.paymentMaster = paymentMaster;
            ctx.SaveChanges();

            var testRequest = GetRefundRequest(
                companyID: paymentMaster.CompanyID,
                masterID: null,
                orderID: null,
                amount: refund_amount
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.AreEqual((testRequest.Amount), result.MasterPayments.First().Applications.Last().Amount);
            var result2 = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result2);
            Assert.AreEqual((testRequest.Amount), result2.MasterPayments.First().Applications.Last().Amount);
            
        }

        [TestMethod]
        public async Task RefundOneOrderOneISCOneMaster()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 15m;
            var testOrder = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );

            var paymentMethod = GetNewPaymentMethod();
            var paymentMaster = GetNewPaymentMaster(
                amount: amount, //Full payment
                orderID: testOrder.ID
            );
            paymentMaster.CompanyID = company.ID;
            paymentMaster.PaymentApplications.First().CompanyID = company.ID;
            paymentMaster.PaymentApplications.Add(
                new PaymentApplication()
                {
                    BID = 1,
                    ID = -98,
                    PaymentType = PaymentMethodType.Visa,
                    PaymentTransactionType = (byte)PaymentTransactionType.Payment,
                    CompanyID = company.ID,
                    ApplicationGroupID = -99,
                    LocationID = testOrder.LocationID,
                    Amount = amount,
                    RefBalance = amount,
                    OrderID = null,
                    PaymentMethodID = 1
                }
            );
            paymentMaster.RefBalance += amount;
            paymentMaster.Amount += amount;

            ctx.PaymentMethod.Add(paymentMethod);
            ctx.PaymentMaster.Add(paymentMaster);
            this.paymentMaster = paymentMaster;
            ctx.SaveChanges();

            var order = ctx.OrderData.First(t => t.ID == this.paymentMaster.PaymentApplications.FirstOrDefault().OrderID);
            Assert.AreEqual(amount, order.PaymentPaid);
            Assert.AreEqual(0, order.PaymentBalanceDue);

            var testRequest = GetRefundRequest(
                companyID: paymentMaster.CompanyID,
                masterID: null,
                orderID: testOrder.ID,
                amount: refund_amount
            );
            testRequest.Amount = testRequest.Amount * 2;
            testRequest.Applications.Add(
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = -refund_amount
                }
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.AreEqual((testRequest.Amount / 2), result.MasterPayments.First().Applications.Last().Amount);
            ctx.Entry(order).Reload();
            Assert.AreEqual(amount + (testRequest.Amount / 2), order.PaymentPaid);
            Assert.AreEqual((-testRequest.Amount / 2), order.PaymentBalanceDue);
            var order2 = ctx.OrderData.First(t => t.ID == testOrder.ID);
            Assert.AreEqual(amount + (testRequest.Amount / 2), order2.PaymentPaid);
            Assert.AreEqual((-testRequest.Amount / 2), order2.PaymentBalanceDue);


        }

        [TestMethod]
        public async Task RefundOneOrderOneISCTwoMasters()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 25m;
            var testOrder = await CreateOrder(
                companyID: company.ID,
                price: amount * 2,
                amountPaid: amount * 2
            );            

            var paymentMethod = GetNewPaymentMethod();
            var paymentMaster = GetNewPaymentMaster(
                amount: amount, //Full payment
                orderID: testOrder.ID
            );
            paymentMaster.CompanyID = company.ID;
            paymentMaster.PaymentApplications.First().CompanyID = company.ID;
            paymentMaster.PaymentApplications.Add(
                new PaymentApplication()
                {
                    BID = 1,
                    ID = -98,
                    PaymentType = PaymentMethodType.Visa,
                    PaymentTransactionType = (byte)PaymentTransactionType.Payment,
                    CompanyID = company.ID,
                    ApplicationGroupID = -99,
                    LocationID = testOrder.LocationID,
                    Amount = amount,
                    RefBalance = amount,
                    OrderID = null,
                    PaymentMethodID = 1
                }
            );
            paymentMaster.RefBalance += amount;
            paymentMaster.Amount += amount;

            var paymentMaster2 = GetNewPaymentMaster(
                amount: amount, //Full payment
                orderID: testOrder.ID
            );
            paymentMaster2.ID = -98;
            paymentMaster2.CompanyID = company.ID;
            paymentMaster2.PaymentApplications.First().CompanyID = company.ID;
            paymentMaster2.PaymentApplications.First().ID = -97;
            paymentMaster2.PaymentApplications.Add(
                new PaymentApplication()
                {
                    BID = 1,
                    ID = -96,
                    PaymentType = PaymentMethodType.Visa,
                    PaymentTransactionType = (byte)PaymentTransactionType.Payment,
                    CompanyID = company.ID,
                    ApplicationGroupID = -99,
                    LocationID = testOrder.LocationID,
                    Amount = amount,
                    RefBalance = amount,
                    OrderID = null,
                    PaymentMethodID = 1
                }
            );
            paymentMaster2.RefBalance += amount;
            paymentMaster2.Amount += amount;

            ctx.PaymentMethod.Add(paymentMethod);
            ctx.PaymentMaster.Add(paymentMaster);
            ctx.PaymentMaster.Add(paymentMaster2);
            this.paymentMaster = paymentMaster;
            ctx.SaveChanges();

            var order = ctx.OrderData.First(t => t.ID == this.paymentMaster.PaymentApplications.FirstOrDefault().OrderID);
            Assert.AreEqual(amount * 2, order.PaymentPaid);
            Assert.AreEqual(0, order.PaymentBalanceDue);

            var testRequest = GetRefundRequest(
                companyID: paymentMaster.CompanyID,
                masterID: null,
                orderID: testOrder.ID,
                amount: refund_amount
            );
            testRequest.Amount = refund_amount * 2;
            testRequest.Applications.Add(
                new PaymentApplicationRequest()
                {
                    OrderID = null,
                    Amount = -refund_amount
                }
            );
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(testRequest), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.AreEqual((-amount), result.MasterPayments.First().Applications.Last().Amount);
            Assert.AreEqual((-refund_amount + amount), result.MasterPayments.Last().Applications.Last().Amount);
            ctx.Entry(order).Reload();
            Assert.AreEqual(2 * amount - refund_amount, order.PaymentPaid);
            Assert.AreEqual(refund_amount, order.PaymentBalanceDue);
            var order2 = ctx.OrderData.First(t => t.ID == testOrder.ID);
            Assert.AreEqual(2 * amount - refund_amount, order2.PaymentPaid);
            Assert.AreEqual(refund_amount, order2.PaymentBalanceDue);


        }

        [TestMethod]
        public async Task RefundOneOrderTwoMastersWhereOneIsNonRefundable()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var amount = 20m;
            var refund_amount = 15m;
            var order = await CreateOrder(
                companyID: company.ID,
                price: amount,
                amountPaid: amount
            );
            var master = await CreatePaymentForOrder(
                ID: -99,
                orderID: order.ID,
                amount: amount / 2,
                isRefundable: true,
                companyID: company.ID
            );
            var master2 = await CreatePaymentForOrder(
                ID: -98,
                orderID: order.ID,
                amount: amount / 2,
                isRefundable: false,
                companyID: company.ID
            );
            var request = GetRefundRequest(
                companyID: master.CompanyID,
                masterID: null,
                orderID: order.ID,
                amount: refund_amount
            );
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}", JsonConvert.SerializeObject(request), HttpStatusCode.BadRequest);
            Assert.IsNotNull(result);

        }

    }
}
