﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using AssertX = Xunit.Assert;
using Microsoft.AspNetCore.Mvc;
using Endor.Api.Web.Controllers;
using Endor.Api.Web.Services;

namespace Endor.Api.Tests
{
    [TestClass]
    public class EmployeeTeamEndpointTests : CommonControllerTestClass
    {
        #region Test Setup/Initialize/Cleanup

        public const string apiUrl = "/Api/EmployeeTeam";
        public const string companyApiUrl = "/api/company";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string newEmployeeTeam;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            newEmployeeTeam = DateTime.UtcNow + " TEST EMPLOYEE TEAM";
        }

        [TestCleanup]
        public void Teardown()
        {
            DeleteTestRecord();
        }

        private void DeleteTestRecord()
        {
            Task.Run(async () =>
            {
                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                SimpleEmployeeTeam[] result = JsonConvert.DeserializeObject<SimpleEmployeeTeam[]>(responseString);
                if (result != null)
                {
                    SimpleEmployeeTeam team = result.FirstOrDefault(x => x.DisplayName.Contains(newEmployeeTeam));
                    if (team != null)
                    {
                        await client.DeleteAsync($"{apiUrl}/{team.ID}");
                    }
                }
            }).Wait();
        }

        private EmployeeTeam GetTestTeam()
        {
            return new EmployeeTeam()
            {
                IsActive = true,
                Name = this.newEmployeeTeam,
                ClassTypeID = (int)ClassType.EmployeeTeam,
                CreatedDate = DateTime.UtcNow,
                ModifiedDT = DateTime.UtcNow,
                HasImage = false,
                IsAdHocTeam = true
            };
        }

        #endregion

        [TestMethod]
        public async Task TestEmployeeTeamCRUDEndpoints()
        {
            var newModel = GetTestTeam();
            var newTeam = await EndpointTests.AssertPostStatusCode<EmployeeTeam>(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}?IsActiveOnly=true", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}?IsActiveOnly=false", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}?LocationID=1", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}?IsActiveOnly=true&LocationID=1", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}?IsActiveOnly=false&LocationID=1", HttpStatusCode.OK);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/SimpleList?IsActiveOnly=true", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/SimpleList?IsActiveOnly=false", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/SimpleList?LocationID=1", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/SimpleList?IsActiveOnly=true&LocationID=1", HttpStatusCode.OK);
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/SimpleList?IsActiveOnly=false&LocationID=1", HttpStatusCode.OK);

            var getTeam = await EndpointTests.AssertGetStatusCode<EmployeeTeam>(client, $"{apiUrl}/{newTeam.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(getTeam);

            getTeam.IsActive = false;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{getTeam.ID}", JsonConvert.SerializeObject(getTeam), HttpStatusCode.OK);

            getTeam = await EndpointTests.AssertGetStatusCode<EmployeeTeam>(client, $"{apiUrl}/{newTeam.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(getTeam);
            Assert.IsFalse(getTeam.IsActive);

            var cloneTeam = await EndpointTests.AssertPostStatusCode<EmployeeTeam>(client, $"{apiUrl}/{getTeam.ID}/Clone", null, HttpStatusCode.OK);
            Assert.IsNotNull(cloneTeam);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{cloneTeam.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{getTeam.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestEmployeeTeamSetActiveSetInactive()
        {
            var adHocTeam = GetTestTeam();

            //test: expect OK
            adHocTeam = await EndpointTests.AssertPostStatusCode<EmployeeTeam>(client, $"{apiUrl}", JsonConvert.SerializeObject(adHocTeam), HttpStatusCode.OK);
            Assert.IsNotNull(adHocTeam);

            // set inactive
            var statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{adHocTeam.ID}/Action/SetInactive", HttpStatusCode.OK);
            Assert.IsNotNull(statusResp);
            Assert.IsTrue(statusResp.Success);

            var adHocTeamTest = await EndpointTests.AssertGetStatusCode<CompanyData>(client, $"{apiUrl}/{adHocTeam.ID}", HttpStatusCode.OK);
            Assert.IsFalse(adHocTeamTest.IsActive);

            // set active
            statusResp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{adHocTeam.ID}/Action/SetActive", HttpStatusCode.OK);
            Assert.IsNotNull(statusResp);
            Assert.IsTrue(statusResp.Success);

            // confirm adhoc is now active again
            adHocTeamTest = await EndpointTests.AssertGetStatusCode<CompanyData>(client, $"{apiUrl}/{adHocTeam.ID}", HttpStatusCode.OK);
            Assert.IsTrue(adHocTeamTest.IsActive);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{adHocTeam.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestEmployeeTeamCanDelete()
        {
            var adHocTeam = GetTestTeam();

            //test: expect OK
            adHocTeam = await EndpointTests.AssertPostStatusCode<EmployeeTeam>(client, $"{apiUrl}", JsonConvert.SerializeObject(adHocTeam), HttpStatusCode.OK);
            Assert.IsNotNull(adHocTeam);

            CompanyData acntNameTest = new CompanyData()
            {
                Name = DateTime.UtcNow + " TEST COMPANY",
                IsActive = true,
                BID = 1,
                TeamID = adHocTeam.ID
            };

            //test: expect OK
            var coResp = await EndpointTests.AssertPostStatusCode<CompanyData>(client, $"{companyApiUrl}", JsonConvert.SerializeObject(acntNameTest), HttpStatusCode.OK);
            Assert.IsNotNull(coResp);
            Assert.AreEqual(acntNameTest.TeamID, coResp.TeamID);

            // check CanDelete
            var statusResp = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{adHocTeam.ID}/Action/CanDelete", HttpStatusCode.OK);
            Assert.IsTrue(statusResp.Success);
            Assert.IsFalse(statusResp.Value.Value);

            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, $"{companyApiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleCompanyData sga in simpleList)
            {
                if (sga.DisplayName == acntNameTest.Name)
                {
                    acntNameTest.ID = sga.ID;
                }
            }

            acntNameTest.TeamID = null;
            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{companyApiUrl}/" + acntNameTest.ID, JsonConvert.SerializeObject(acntNameTest), HttpStatusCode.OK);

            statusResp = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{adHocTeam.ID}/Action/CanDelete", HttpStatusCode.OK);
            Assert.IsTrue(statusResp.Success);
            Assert.IsTrue(statusResp.Value.Value);

            await EndpointTests.AssertDeleteStatusCode(client, $"{companyApiUrl}/{acntNameTest.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{adHocTeam.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestEmployeeTeamLinkLocation()
        {
            var newModel = GetTestTeam();
            var newTeam = await EndpointTests.AssertPostStatusCode<EmployeeTeam>(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);

            var getTeam = await EndpointTests.AssertGetStatusCode<EmployeeTeam>(client, $"{apiUrl}/{newTeam.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(getTeam);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{getTeam.ID}/action/linklocation/1", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{getTeam.ID}/action/unlinklocation/1", HttpStatusCode.OK);
        }

    }
}
