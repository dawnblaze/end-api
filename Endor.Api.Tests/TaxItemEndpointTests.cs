﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    [TestClass]
    public class TaxItemEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/taxitem";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestTaxItemName;
        private string NewTestTaxGroupName;
        public TestContext TestContext { get; set; }
        
        [TestInitialize]
        public void Initialize()
        {
            this.NewTestTaxItemName = DateTime.UtcNow + " TEST TAX ITEM";
            this.NewTestTaxGroupName = DateTime.UtcNow + " TEST TAXGROUP DATA";
        }

        [TestMethod]
        public async Task TestTaxItemLinkUnlinkTaxGroup()
        {
            var taxGroup = GetNewTaxGroupModel();

            TaxItem taxItem = new TaxItem()
            {
                Name = this.NewTestTaxItemName,
                InvoiceText = "TEST",
                IsActive = true,
                BID = 1,
                TaxRate = 0
            };

            var postedTaxGroup = await EndpointTests.AssertPostStatusCode<TaxGroup>(client, $"/Api/TaxGroup", JsonConvert.SerializeObject(taxGroup), HttpStatusCode.OK);
            var postedTaxItem = await EndpointTests.AssertPostStatusCode<TaxItem>(client, $"{apiUrl}", JsonConvert.SerializeObject(taxItem), HttpStatusCode.OK);
            
            var resp = await EndpointTests.AssertPostStatusCode<GenericResponse<TaxGroup>>(client, $"{apiUrl}/{postedTaxItem.ID}/action/linktaxgroup/{postedTaxGroup.ID}", null, HttpStatusCode.OK);
            Assert.IsTrue(resp.Success);

            TaxItem testLinkTaxItem = await EndpointTests.AssertGetStatusCode<TaxItem>(client, $"{apiUrl}/{postedTaxItem.ID}?TaxGroups=Simple", HttpStatusCode.OK);
            Assert.IsNotNull(testLinkTaxItem);
            Assert.IsNotNull(testLinkTaxItem.SimpleTaxGroups);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{postedTaxItem.ID}/action/unlinktaxgroup/{postedTaxGroup.ID}", null, HttpStatusCode.OK);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{postedTaxItem.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/Api/TaxGroup/{postedTaxGroup.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestTaxItemSingleEndpoints()
        {
            TaxItem acntNameTest = new TaxItem()
            {
                Name = this.NewTestTaxItemName,
                InvoiceText = "TEST",
                IsActive = true,
                BID = 1,
                TaxRate = 0
            };

            //test: expect OK
            var postedResponse = await EndpointTests.AssertPostStatusCode<TaxItem>(client, $"{apiUrl}", JsonConvert.SerializeObject(acntNameTest), HttpStatusCode.OK);
            acntNameTest.GLAccountID = postedResponse.GLAccountID;
            acntNameTest.ID = postedResponse.ID;

            // GET api/TaxItems/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleTaxItem[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleTaxItem sga in simpleList)
            {
                if (sga.DisplayName == acntNameTest.Name)
                {
                    acntNameTest.ID = sga.ID;
                }
            }

            //POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{acntNameTest.ID}/action/setinactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{acntNameTest.ID}/action/setactive", HttpStatusCode.OK);

            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + acntNameTest.ID, JsonConvert.SerializeObject(acntNameTest), HttpStatusCode.OK);

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{acntNameTest.ID}", HttpStatusCode.NoContent);
            
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/9241657", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestTaxItemSetActiveSetInactive()
        {
            TaxItem taxItemTest = GetNewModel();

            //test: expect OK
            var taxItem = await EndpointTests.AssertPostStatusCode<TaxItem>(client, $"{apiUrl}", JsonConvert.SerializeObject(taxItemTest), HttpStatusCode.OK);
            Assert.IsNotNull(taxItem);

            // set taxitem inactive
            var resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{taxItem.ID}/action/setinactive", HttpStatusCode.OK);
            Assert.IsNotNull(resp);
            Assert.AreEqual(true, resp.Success);

            // set active
            resp = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{taxItem.ID}/action/setactive", HttpStatusCode.OK);
            Assert.IsNotNull(resp);
            Assert.AreEqual(true, resp.Success);
       }

        [TestMethod]
        public async Task TestTaxItemCanDelete()
        {
            TaxItem taxItemTest = GetNewModel();

            //test: expect OK
            var taxItem = await EndpointTests.AssertPostStatusCode<TaxItem>(client, $"{apiUrl}", JsonConvert.SerializeObject(taxItemTest), HttpStatusCode.OK);
            Assert.IsNotNull(taxItem);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{taxItem.ID}/action/candelete", HttpStatusCode.OK);
        }

        private TaxItem GetNewModel()
        {
            return new TaxItem()
            {
                Name = this.NewTestTaxItemName,
                InvoiceText = "TEST",
                IsActive = true,
                BID = 1,
                TaxRate = 0
            };
        }

        private TaxGroup GetNewTaxGroupModel()
        {
            return new TaxGroup()
            {
                Name = this.NewTestTaxGroupName,
                IsActive = true,
                BID = 1,
                TaxRate = 0
            };
        }

        [TestMethod]
        public async Task TestFiltering()
        {

            TaxItem newModel = GetNewModel();

            //test: expect OK
            var savedAccount = await EndpointTests.AssertPostStatusCode<TaxItem>(client, $"{apiUrl}", JsonConvert.SerializeObject(newModel), HttpStatusCode.OK);

            //test that our item shows under no filters
            var all = await EndpointTests.AssertGetStatusCode<TaxItem[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(all.FirstOrDefault(x => x.Name == newModel.Name));

            //test that our item DOES show under actives
            var active = await EndpointTests.AssertGetStatusCode<TaxItem[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(active.All(x => x.IsActive));
            Assert.IsNotNull(active.FirstOrDefault(x => x.Name == newModel.Name));

            var activeSimple = await EndpointTests.AssertGetStatusCode<SimpleTaxItem[]>(client, $"{apiUrl}/simplelist?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(activeSimple.All(x => x.IsActive));
            Assert.IsNotNull(activeSimple.FirstOrDefault(x => x.DisplayName.Contains(newModel.Name)));

            //test that our item DOES NOT show under inactives
            var inactive = await EndpointTests.AssertGetStatusCode<TaxItem[]>(client, $"{apiUrl}?IsActive=false", HttpStatusCode.OK);
            Assert.IsTrue(inactive.All(x => !x.IsActive));
            Assert.IsNull(inactive.FirstOrDefault(x => x.Name == newModel.Name));

            var inactiveSimple = await EndpointTests.AssertGetStatusCode<SimpleTaxItem[]>(client, $"{apiUrl}/simplelist?IsActive=false", HttpStatusCode.OK);
            Assert.IsTrue(inactiveSimple.All(x => !x.IsActive));
            Assert.IsNull(inactiveSimple.FirstOrDefault(x => x.DisplayName.Contains(newModel.Name)));

        }

        //[TestMethod]
        //public async Task TestTaxItemSimpleListIncludes()
        //{
        //    var accounttaxgroup = await EndpointTests.AssertGetStatusCode<TaxGroup>(client, $"/api/taxgroup/1000?TaxItems=1", HttpStatusCode.OK);
        //    Assert.IsNotNull(accounttaxgroup);

        //    var account = await EndpointTests.AssertGetStatusCode<TaxItem>(client, $"{apiUrl}/1008?TaxGroups=1", HttpStatusCode.OK);
        //    Assert.IsNotNull(account.SimpleTaxGroups);
        //}

        [TestCleanup]
        public void Teardown()
        {
            var ctx = GetApiContext();
            var taxItem = ctx.TaxItem.Where(x => x.Name == NewTestTaxItemName).FirstOrDefault();
            if (taxItem != null)
            {
                ctx.TaxItem.Remove(taxItem);
                ctx.SaveChanges();
            }

            var taxGroup = ctx.TaxGroup.Where(x => x.Name == NewTestTaxGroupName).FirstOrDefault();
            if (taxGroup != null)
            {
                ctx.TaxGroup.Remove(taxGroup);
                ctx.SaveChanges();
            }

            // this.DeleteTestAccountRecord();
        }

        private void DeleteTestAccountRecord()
        {
            string newTestTaxItemName = NewTestTaxItemName;
            Task.Run(async () =>
            {

                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                SimpleTaxItem[] result = JsonConvert.DeserializeObject<SimpleTaxItem[]>(responseString);
                if (result != null)
                {
                    SimpleTaxItem sga = result.FirstOrDefault(x => x.DisplayName.Contains(newTestTaxItemName));
                    if (sga != null)
                    {
                        await client.DeleteAsync($"{apiUrl}/{sga.ID}");
                    }
                }
            }).Wait();
        }
    }
}
