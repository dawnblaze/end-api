﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Clones;
using Endor.Pricing;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Endor.DocumentStorage.Models;
using AssertX = Xunit.Assert;
using Endor.EF;
using Endor.Tenant;
using Endor.AzureStorage;
using Endor.CBEL.Common;
using Endor.Units;

namespace Endor.Api.Tests
{
    [TestClass]
    public class OrderEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/order";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }
        private ApiContext ctx;
        private const int BID = 1;
        private const int _orderID = -99;
        private const int _companyID = -99;
        private const int _contactID = -99;
        private const int _orderItemID = -99;
        private const int _taxGroupID = -99;
        private short taxCodeID = 0;

        [TestInitialize]
        public async Task InitializePricingTests()
        {
            ctx = GetApiContext();
            await Cleanup();
            await InitializeCompanyAndGLAccountIDs();
        }

        /// <summary>
        /// Initializes the Company and GLAccount IDs
        /// </summary>
        /// <returns></returns>
        protected async Task InitializeCompanyAndGLAccountIDs()
        {
            // check for existing company
            var company = await ctx.CompanyData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _companyID);
            if (company == null)
            {
                company = new CompanyData()
                {
                    BID = BID,
                    ID = _companyID,
                    Name = "Test Pricing Company",
                    LocationID = 1,
                    IsAdHoc = true,
                    StatusID = 1
                };

                ctx.CompanyData.Add(company);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            var contact = await ctx.ContactData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _contactID);
            if(contact == null)
            {
                contact = new ContactData
                {
                    BID = BID,
                    ID = _contactID,
                    First = "Test",
                    Last = "Last",
                    LocationID = 1
                };

                ctx.ContactData.Add(contact);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            var contactCompanyLink = await ctx.CompanyContactLink.FirstOrDefaultAsync(t => t.BID == BID && t.ContactID == _contactID && t.CompanyID == _companyID);
            if(contactCompanyLink == null)
            {
                contactCompanyLink = new CompanyContactLink
                {
                    BID = BID,
                    ContactID = _contactID,
                    CompanyID = _companyID,
                    IsActive = true
                };

                ctx.CompanyContactLink.Add(contactCompanyLink);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            // there should always be at least one GLAccount and TaxCode
            glAccountID = ctx.GLAccount.FirstOrDefault(t => t.BID == BID).ID;
            taxCodeID = ctx.TaxabilityCodes.FirstOrDefault(t => t.BID == BID).ID;

            var taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _taxGroupID);
            if (taxGroup == null)
            {
                taxGroup = new TaxGroup()
                {
                    BID = BID,
                    ID = _taxGroupID,
                    Name = "Test Pricing TaxGroup"
                };

                ctx.TaxGroup.Add(taxGroup);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            var taxItem = await ctx.TaxItem.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _taxGroupID);
            if (taxItem == null)
            {
                taxItem = new TaxItem()
                {
                    BID = BID,
                    ID = _taxGroupID,
                    Name = "Test Pricing taxItem",
                    TaxRate = 10,
                    InvoiceText = "test",
                    GLAccountID = glAccountID
                };

                ctx.TaxItem.Add(taxItem);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            var TaxGroupItemLink = await ctx.TaxGroupItemLink.FirstOrDefaultAsync(t => t.BID == BID && t.GroupID == _taxGroupID);
            if (TaxGroupItemLink == null)
            {
                TaxGroupItemLink = new TaxGroupItemLink()
                {
                    BID = BID,
                    GroupID = _taxGroupID,
                    ItemID = _taxGroupID,
                };

                ctx.TaxGroupItemLink.Add(TaxGroupItemLink);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            
        }


        [TestCleanup]
        public async Task Cleanup()
        {
            try
            {
                var _assemblyTables = ctx.AssemblyTable.Where(t => t.BID == BID && (t.ID < 0 || t.AssemblyID < 0));
                if (_assemblyTables.Any())
                {
                    ctx.AssemblyTable.RemoveRange(_assemblyTables);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }
                var _assemblyVariableFormulas = ctx.AssemblyVariableFormula.Where(t => t.BID == BID && (t.ID < 0 || t.VariableID < 0));
                if (_assemblyVariableFormulas.Any())
                {
                    ctx.AssemblyVariableFormula.RemoveRange(_assemblyVariableFormulas);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }
                var _assemblyVariables = ctx.AssemblyVariable.Where(t => t.BID == BID && (t.ID < 0 || t.AssemblyID < 0 || t.LinkedMaterialID < 0));
                if (_assemblyVariables.Any())
                {
                    ctx.AssemblyVariable.RemoveRange(_assemblyVariables);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _orderItemComponents = ctx.OrderItemComponent.Where(t => t.BID == BID && (t.AssemblyID < 0 || t.ID < 0));
                if (_orderItemComponents.Any())
                {
                    ctx.OrderItemComponent.RemoveRange(_orderItemComponents);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
                }

                var _machineProfileTables = ctx.MachineProfileTable.Where(t => t.BID == BID && (t.ID < 0 || t.ProfileID < 0 || t.TableID < 0));
                if (_machineProfileTables.Count() > 0)
                {
                    ctx.MachineProfileTable.RemoveRange(_machineProfileTables);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _machineProfiles = ctx.MachineProfile.Where(t => t.BID == BID && (t.ID < 0 || t.MachineID < 0 || t.MachineTemplateID < 0));
                if (_machineProfiles.Count() > 0)
                {
                    ctx.MachineProfile.RemoveRange(_machineProfiles);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _assembly = await ctx.AssemblyData.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
                if (_assembly != null)
                {
                    ITenantDataCache cache = this.GetMockTenantDataCache();
                    EntityStorageClient client = new EntityStorageClient((await cache.Get(BID)).StorageConnectionString, BID);
                    var assemblyDoc = new DMID() { id = _assembly.ID, ctid = ClassType.Assembly.ID() };
                    string assemblyName = CBELAssemblyHelper.AssemblyName(BID, _assembly.ID, _assembly.ClassTypeID, 1);

                    await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"bin/{assemblyName}.dll");
                    await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"source/{assemblyName}.cs");
                    ctx.MachineData.RemoveRange(ctx.MachineData.Where(t => t.BID == BID && t.MachineTemplateID == _assembly.ID));
                    ctx.MachineProfile.RemoveRange(ctx.MachineProfile.Where(t => t.BID == BID && t.MachineTemplateID == _assembly.ID));
                    ctx.AssemblyData.Remove(_assembly);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _partMaterialCategoryLinks = ctx.MaterialCategoryLink.Where(t => t.BID == BID && t.PartID < 0);
                if (_partMaterialCategoryLinks.Any())
                {
                    ctx.MaterialCategoryLink.RemoveRange(_partMaterialCategoryLinks);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
                }

                var _material1 = await ctx.MaterialData.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
                if (_material1 != null)
                {
                    ctx.MaterialData.Remove(_material1);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                /*var _orderItemSurcharges = ctx.OrderItemSurcharge.Where(t => t.BID == BID && (t.ID == _orderItemSurchargeID || t.SurchargeDefID == _surchargeID));
                if (_orderItemSurcharges.Any())
                {
                    ctx.OrderItemSurcharge.RemoveRange(_orderItemSurcharges);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
                }*/

                var _orderItem = await ctx.OrderItemData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _orderItemID);
                if (_orderItem != null)
                {
                    ctx.OrderItemData.Remove(_orderItem);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
                }

                ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
                ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
                ctx.OrderContactRole.RemoveRange(await ctx.OrderContactRole.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
                ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == BID && x.Parent.Order.ID == _orderID).ToArrayAsync());
                ctx.OrderCustomData.RemoveRange(await ctx.OrderCustomData.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());

                var _order = await ctx.OrderData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _orderID);
                if (_order != null)
                {
                    ctx.OrderData.Remove(_order);
                    await RemoveTransheaderItems(_order.ID);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
                }

                var _orderItemSurchages = ctx.OrderItemSurcharge.Where(t => t.BID == BID && t.SurchargeDefID < 0);
                var orderItemTaxAssessmentIDsToDelete = _orderItemSurchages.Select(x => x.ID).ToList();
                ctx.OrderItemTaxAssessment.RemoveRange(ctx.OrderItemTaxAssessment.Where(t => t.BID == BID && orderItemTaxAssessmentIDsToDelete.Contains(t.ItemSurchargeID.GetValueOrDefault())));
                ctx.OrderItemSurcharge.RemoveRange(_orderItemSurchages);
                await ctx.SaveChangesAsync();

                var _surcharge = await ctx.SurchargeDef.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
                if (_surcharge != null)
                {
                    ctx.SurchargeDef.RemoveRange(_surcharge);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _taxGroupTaxItem = await ctx.TaxGroupItemLink.FirstOrDefaultAsync(t => t.BID == BID && t.GroupID < 0);
                if (_taxGroupTaxItem != null)
                {
                    ctx.TaxGroupItemLink.Remove(_taxGroupTaxItem);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _taxItem = await ctx.TaxItem.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
                if (_taxItem != null)
                {
                    ctx.OrderItemTaxAssessment.RemoveRange(ctx.OrderItemTaxAssessment.Where(t => t.BID == BID && t.TaxItemID == _taxItem.ID));
                    ctx.TaxItem.Remove(_taxItem);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
                if (_taxGroup != null)
                {
                    ctx.TaxGroup.Remove(_taxGroup);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _company = await ctx.CompanyData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _companyID);
                if (_company != null)
                {
                    var _orders = await ctx.OrderData.Where(t => t.BID == BID && t.CompanyID == _companyID).ToListAsync();
                    foreach (var o in _orders)
                    {

                        ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(t => t.BID == BID && t.OrderID == o.ID && t.IsOrderNote));
                        var items = ctx.OrderItemData.Where(t => t.BID == BID && t.OrderID == o.ID);
                        var itemIDs = items.Select(x => x.ID);
                        ctx.OrderItemData.RemoveRange(items);
                        ctx.OrderItemSurcharge.RemoveRange(ctx.OrderItemSurcharge.Where(t => t.BID == BID && itemIDs.Contains(t.OrderItemID)));
                        ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());
                        ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());
                        ctx.OrderContactRole.RemoveRange(await ctx.OrderContactRole.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());
                        ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == BID && x.Parent.Order.ID == o.ID).ToArrayAsync());
                        ctx.OrderCustomData.RemoveRange(await ctx.OrderCustomData.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());

                        ctx.OrderData.Remove(o);
                        await RemoveTransheaderItems(o.ID);
                    }

                    ctx.CompanyData.Remove(_company);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _labor = ctx.LaborData.Where(t => t.BID == BID && t.ID < 0);
                if (_labor.Count() > 0)
                {
                    ctx.LaborData.RemoveRange(_labor);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }


                var _machine = ctx.MachineData.Where(t => t.BID == BID && t.ID < 0);
                if (_machine.Count() > 0)
                {
                    ctx.MachineData.RemoveRange(_machine);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        private async Task RemoveTransheaderItems(int orderID)
        {
            var items = await ctx.OrderItemData.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync();
            foreach (var orderItem in items)
            {
                ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(t => t.BID == BID && t.OrderItemID == orderItem.ID));
                ctx.OrderItemData.Remove(orderItem);
                foreach (var component in await ctx.OrderItemComponent.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync())
                {
                    ctx.OrderItemComponent.Remove(component);
                }
            }
            foreach (var role in await ctx.OrderEmployeeRole.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync())
            {
                ctx.OrderEmployeeRole.Remove(role);
            }
            foreach (var keydate in await ctx.OrderKeyDate.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync())
            {
                ctx.OrderKeyDate.Remove(keydate);
            }

        }

        private async Task CleanUpEstimates(ApiContext ctx, int companyId)
        {
            var cleanupOrderKeyDate = await ctx.OrderKeyDate.Where(x => x.OrderID == companyId).ToListAsync();
            ctx.RemoveRange(cleanupOrderKeyDate);
            await ctx.SaveChangesAsync();
            var cleanupOrderContactRole = await ctx.OrderContactRole.Where(x => x.OrderID == companyId).ToListAsync();
            ctx.RemoveRange(cleanupOrderContactRole);
            await ctx.SaveChangesAsync();
            var cleanupOrderItem = await ctx.OrderItemData.Where(x => x.OrderID == companyId).ToListAsync();
            ctx.RemoveRange(cleanupOrderItem);
            await ctx.SaveChangesAsync();
        }

        private async Task CleanUpOrderAndCompanies(short testBID)
        {
            var cleanupCtx = GetApiContext(testBID);

            var sql = @"
delete from 
[Order.Contact.Role]
where OrderItemID in 
(select OID.ID
 from dbo.[Order.Item.Data] as OID
 where OID.OrderID in 
 (
   select OD.ID as OrderID
   from [Order.Data] as OD
   where OD.CompanyID < 0
 ))

 delete from 
[Order.Contact.Role]
where ContactID < 0

delete from
[Order.Item.Component]
where OrderItemID in 
(select OID.ID
 from dbo.[Order.Item.Data] as OID
 where OID.OrderID in 
 (
   select OD.ID as OrderID
   from [Order.Data] as OD
   where OD.CompanyID < 0
 ))

delete from
[Order.Item.Data]
where OrderID in 
(select Odata.ID
 from [Order.Data] as Odata
 where Odata.CompanyID < 0)

 delete from
[Order.KeyDate]
where OrderID in 
(select Odata.ID
 from [Order.Data] as Odata
 where Odata.CompanyID < 0)

 delete from 
 [Order.Data]
 where CompanyID < 0

 delete from 
 [Company.Data]
 where ID < 0";
            var result = ctx.Database.ExecuteSqlRaw(sql);

            var cleanupOrders = await cleanupCtx.OrderData.Where(x => x.CompanyID <= 0).ToListAsync();
            if (cleanupOrders.Count != 0)
            {
                foreach (var cleanupOrder in cleanupOrders)
                {
                    var cleanupOrderKeyDate = await cleanupCtx.OrderKeyDate.Where(x => x.OrderID == cleanupOrder.ID).ToListAsync();
                    cleanupCtx.RemoveRange(cleanupOrderKeyDate);
                    await cleanupCtx.SaveChangesAsync();
                    var cleanupOrderContactRole = await cleanupCtx.OrderContactRole.Where(x => x.OrderID == cleanupOrder.ID).ToListAsync();
                    cleanupCtx.RemoveRange(cleanupOrderContactRole);
                    await cleanupCtx.SaveChangesAsync();
                    var cleanupOrderItem = await cleanupCtx.OrderItemData.Where(x => x.OrderID == cleanupOrder.ID).ToListAsync();
                    cleanupCtx.RemoveRange(cleanupOrderItem);
                    await cleanupCtx.SaveChangesAsync();
                }
                cleanupCtx.RemoveRange(cleanupOrders);
                await cleanupCtx.SaveChangesAsync();
            }
            var cleanupCompanies = await cleanupCtx.CompanyData.Include(i => i.Orders).Where(x => x.ID <= 0).ToListAsync();
            if (cleanupCompanies.Count != 0)
            {
                foreach (var companyToRemove in cleanupCompanies)
                {
                    if (companyToRemove.Orders.Any(x => x.TransactionType != (byte)OrderTransactionType.Estimate))
                    {
                        await CleanUpEstimates(cleanupCtx, companyToRemove.ID);
                    }
                }
                cleanupCtx.RemoveRange(cleanupCompanies);
                await cleanupCtx.SaveChangesAsync();
            }
            var cleanupContact = await cleanupCtx.ContactData.Where(x => x.ID <= 0).ToListAsync();
            if (cleanupContact != null)
            {
                cleanupCtx.RemoveRange(cleanupContact);
                await cleanupCtx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task TestOrderWithCustomerContact()
        {
            short testBID = TestConstants.BID;
            try
            {
                #region Clean Up Data
                await CleanUpOrderAndCompanies(testBID);
                #endregion
                var ctx = GetApiContext(testBID);

                #region Create Data
                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var contactA = new ContactData()
                {
                    ID = -94,
                    First = "Julius",
                    Last = "Bacosa",
                    BID = TestConstants.BID
                };

                ctx.ContactData.AddRange(new[] { contactA });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var companyA = new CompanyData()
                {
                    ID = -94,
                    Name = "Test Company A",
                    BID = TestConstants.BID,
                    IsActive = true,
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                ctx.CompanyData.Add(companyA);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);


                var contactLinksForCompanyA = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = TestConstants.BID,
                        ContactID = contactA.ID,
                        CompanyID = companyA.ID,
                        Roles = (ContactRole.Primary | ContactRole.Billing)
                    }
                };
                ctx.CompanyContactLink.AddRange(contactLinksForCompanyA);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                #endregion

                var createdContact = await ctx.ContactData.FirstOrDefaultAsync(x => x.ID == contactA.ID);
                Assert.AreEqual(1, createdContact.StatusID);

                OrderData testOrder = new OrderData()
                {
                    BID = TestConstants.BID,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    LocationID = location.ID,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-1000",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                    TaxGroupID = 0,
                    ContactRoles = new List<OrderContactRole>()
                    {
                         new OrderContactRole ()
                         {
                              ContactID = contactA.ID,
                              RoleType = OrderContactRoleType.Primary
                         }
                    }
                };
                testOrder.Items = new List<OrderItemData>()
                {
                    new OrderItemData()
                    {
                        BID = TestConstants.BID,
                        HasCustomImage = false,
                        HasDocuments = false,
                        HasProof = false,
                        ID = 1,
                        OrderID = testOrder.ID,
                        ItemNumber = 1,
                        Quantity = 1,
                        Name = "test",
                        IsOutsourced = false,
                        OrderStatusID = testOrder.OrderStatusID,
                        ItemStatusID = 21,
                        ProductionLocationID = testOrder.ProductionLocationID,
                        PriceIsLocked = false,
                        PriceTaxableOV = false,
                        TaxGroupID = testOrder.TaxGroupID,
                        TaxGroupOV = false,
                        IsTaxExempt = false,
                        TransactionType = testOrder.TransactionType
                    }
                };

                testOrder.CompanyID = companyA.ID;

                var response = await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
                Assert.IsNotNull(response);

                var assertResultCtx = GetApiContext(testBID);
                var updatedContact = await assertResultCtx.ContactData.FirstOrDefaultAsync(x => x.ID == contactA.ID);
                Assert.AreEqual(4, updatedContact.StatusID);
            } 
            finally
            {
                #region Clean Up Data
                await CleanUpOrderAndCompanies(testBID);
                #endregion
            }
        }

        [TestMethod]
        public async Task TestOrderWithContactAndEmployeeRoles()
        {
            var ctx = GetApiContext();
            OrderData testOrder = Utils.GetTestOrderData();
            testOrder.CompanyID = ctx.CompanyData.FirstOrDefault(x => x.BID == TestConstants.BID).ID;
            testOrder.Items = new List<OrderItemData>()
            {
                Utils.GetTestOrderItemData(testOrder)
            };
            testOrder.ContactRoles = new List<OrderContactRole>()
            {
                new OrderContactRole()
                {
                    BID = TestConstants.BID,
                    ContactName = "",
                    RoleType = OrderContactRoleType.Primary,
                },
            };

            var response = await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.BadRequest);
            Assert.IsNotNull(response);
            var errorContent = await response.Content.ReadAsStringAsync();
            Assert.IsNotNull(errorContent);
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(errorContent);
            Assert.AreEqual(result.Last().Value, "Contact Name/ID is required");

            testOrder.ContactRoles = new List<OrderContactRole>()
            {
                new OrderContactRole()
                {
                    BID = TestConstants.BID,
                    ContactName = "Test Contact",
                    ContactID = -123,
                    RoleType = OrderContactRoleType.Primary,
                },
            };
            response = await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.BadRequest);
            Assert.IsNotNull(response);
            errorContent = await response.Content.ReadAsStringAsync();
            Assert.IsNotNull(errorContent);
            result = JsonConvert.DeserializeObject<Dictionary<string, string>>(errorContent);
            Assert.AreEqual(result.Last().Value, "Invalid ContactID");

            testOrder.ContactRoles = new List<OrderContactRole>()
            {
                new OrderContactRole()
                {
                    BID = TestConstants.BID,
                    ContactName = "Test Contact",
                    ContactID = await Utils.GetContactID(client, testOrder.CompanyID),
                    RoleType = OrderContactRoleType.DesignReviewer & OrderContactRoleType.DesignApproval, //invalid roletype
                },
            };
            response = await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.BadRequest);
            Assert.IsNotNull(response);
            errorContent = await response.Content.ReadAsStringAsync();
            Assert.IsNotNull(errorContent);
            result = JsonConvert.DeserializeObject<Dictionary<string, string>>(errorContent);
            Assert.AreEqual(result.Last().Value, "Invalid Contact\'s RoleType");

            testOrder.ContactRoles = null;
            testOrder.EmployeeRoles= new List<OrderEmployeeRole>()
            {
                new OrderEmployeeRole()
                {
                EmployeeID = -123,
                RoleID = 1,
                },
            };
            response = await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.BadRequest);
            Assert.IsNotNull(response);
            errorContent = await response.Content.ReadAsStringAsync();
            Assert.IsNotNull(errorContent);
            result = JsonConvert.DeserializeObject<Dictionary<string, string>>(errorContent);
            Assert.AreEqual(result.Last().Value, "Invalid EmployeeID");

            testOrder.EmployeeRoles = new List<OrderEmployeeRole>()
            {
                new OrderEmployeeRole()
                {
                EmployeeID = (short)await Utils.GetEmployeeID(client),
                RoleID = -123,
                },
            };
            response = await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.BadRequest);
            Assert.IsNotNull(response);
            errorContent = await response.Content.ReadAsStringAsync();
            Assert.IsNotNull(errorContent);
            result = JsonConvert.DeserializeObject<Dictionary<string, string>>(errorContent);
            Assert.AreEqual(result.Last().Value, "Invalid Employee\'s RoleID");
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task TestOrderCRUDOperations()
        {
            var ctx = GetApiContext();

            CompanyData company = new CompanyData()
            {
                ID = _companyID,
                Name = "Test Company",
                IsActive = true,
                BID = TestConstants.BID,
                StatusID = (byte)CompanyStatus.Lead,
                LocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID,
            };
            var existingCompanies = ctx.CompanyData.Where(c=> c.ID == company.ID && c.BID==company.BID);
            if (existingCompanies.Count() > 0) company = existingCompanies.First();
            else
            {
                ctx.CompanyData.Add(company);
                ctx.SaveChanges();
            }
            #region CREATE
            var testOrder = Utils.GetTestOrderData();
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.BadRequest);

            testOrder.CompanyID = company.ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            #endregion

            #region RETRIEVE

            // Test Get By ID
            var retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder);

            var getAll = await EndpointTests.AssertGetStatusCode<OrderData[]>(client, $"{apiUrl}", HttpStatusCode.OK);
            Assert.IsNotNull(getAll);
            Assert.IsTrue(getAll.Count() > 0);
            Assert.IsTrue(getAll.All(t => t.TransactionType == (byte)OrderTransactionType.Order));

            ctx.Entry<CompanyData>(company).Reload();
            Assert.IsTrue(company.StatusText.Contains("Client"));

            #endregion

            #region UPDATE

            var holdFormattedNumber = createdOrder.FormattedNumber;
            createdOrder.FormattedNumber = ""; //invalid length string;
            var updatedOrder = await EndpointTests.AssertPutStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}", JsonConvert.SerializeObject(createdOrder), HttpStatusCode.OK);
            Assert.IsTrue(holdFormattedNumber == updatedOrder.FormattedNumber);
            createdOrder.FormattedNumber = holdFormattedNumber;
            createdOrder.Description = "TEST DESCRIPTION";
            var testPut = await EndpointTests.AssertPutStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}", JsonConvert.SerializeObject(createdOrder), HttpStatusCode.OK);
            Assert.IsNotNull(testPut);
            Assert.AreEqual("TEST DESCRIPTION", testPut.Description);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            try
            {
                ctx.CompanyData.Remove(company);
                ctx.SaveChanges();
            } catch (Exception e)
            {
                Assert.Fail(e.Message);
            }

            #endregion
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task TestOrderCRUDOperationsWithTaxes()
        {
            var ctx = GetApiContext();

            CompanyData company = new CompanyData()
            {
                ID = _companyID,
                Name = "Test Company",
                IsActive = true,
                BID = TestConstants.BID,
                StatusID = (byte)CompanyStatus.Lead,
                LocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID,
            };
            var existingCompanies = ctx.CompanyData.Where(c => c.ID == company.ID && c.BID == company.BID);
            if (existingCompanies.Count() > 0) company = existingCompanies.First();
            else
            {
                ctx.CompanyData.Add(company);
                ctx.SaveChanges();
            }
            #region CREATE
            var testOrder = Utils.GetTestOrderData();
            testOrder.CompanyID = company.ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;
            var testOrderItem = Utils.GetTestOrderItemData(testOrder);
            testOrderItem.ItemStatusID = 21;
            testOrder.Items = new List<OrderItemData>()
            {
                testOrderItem
            };
            var incomeAccounts = ctx.GLAccount.Where(account => account.GLAccountType == 40).ToList(); //Income
            var testOrderItemComponent = Utils.GetTestOrderItemComponentData(testOrderItem, incomeAccounts.First());
            testOrderItemComponent.TaxInfoList = new List<TaxAssessmentResult>()
            {
                new TaxAssessmentResult()
                {
                    TaxGroupID = ctx.TaxGroup.First(x => x.BID ==TestConstants.BID).ID,
                    TaxAmount = 1,
                    PriceTaxable = 10,
                    TaxRate = 0.1m,
                    InvoiceText = "TEst",
                    Items = new List<TaxAssessmentItemResult>()
                    {
                        new TaxAssessmentItemResult()
                        {
                            TaxItemID = ctx.TaxItem.First(x => x.BID ==TestConstants.BID).ID,
                            InvoiceText = "Test",
                            TaxRate=0.1m,
                            TaxAmount = 1,
                            GLAccountID = ctx.GLAccount.First(x => x.BID ==TestConstants.BID).ID
                        }
                    }
                }
            };
            var surcharge1 = new OrderItemSurcharge()
            {
                BID = TestConstants.BID,
                Name = "Test Surcharge",
                TaxCode = ctx.TaxabilityCodes.FirstOrDefault(x => x.BID == TestConstants.BID),
                IncomeAccountID = ctx.GLAccount.FirstOrDefault(x => x.BID == TestConstants.BID).ID,
                SurchargeDefID = ctx.SurchargeDef.FirstOrDefault(x => x.BID ==TestConstants.BID).ID
            };
            surcharge1.TaxInfoList = new List<TaxAssessmentResult>()
            {
                new TaxAssessmentResult()
                {
                    TaxGroupID = ctx.TaxGroup.First().ID,
                    TaxAmount = 1,
                    PriceTaxable = 10,
                    TaxRate = 0.1m,
                    InvoiceText = "Test",
                    Items = new List<TaxAssessmentItemResult>()
                    {
                        new TaxAssessmentItemResult()
                        {
                            TaxItemID = ctx.TaxItem.First(x => x.BID ==TestConstants.BID).ID,
                            InvoiceText = "Test",
                            TaxRate=0.1m,
                            TaxAmount = 1,
                            GLAccountID = ctx.GLAccount.First(x => x.BID ==TestConstants.BID).ID
                        }
                    }
                }
            };
            testOrderItem.Components = new List<OrderItemComponent>()
            {
                testOrderItemComponent
            };
            testOrderItem.Surcharges = new List<OrderItemSurcharge>()
            {
                surcharge1
            };

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            var createdOrderItemId = createdOrder.Items.First().ID;
            Assert.AreEqual(ctx.OrderItemTaxAssessment.Where(x => x.BID==TestConstants.BID && x.OrderItemID== createdOrderItemId).Count(),2);
            #endregion

            #region RETRIEVE

            // Test Get By ID
            var retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}?ItemLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder);
            Assert.AreEqual(retrievedOrder.Items.First().Components.First().TaxInfoList.Count,1);
            Assert.AreEqual(retrievedOrder.Items.First().Surcharges.First().TaxInfoList.Count, 1);

            #endregion

            #region UPDATE

            var holdFormattedNumber = createdOrder.FormattedNumber;
            createdOrder.FormattedNumber = ""; //invalid length string;
            var updatedOrder = await EndpointTests.AssertPutStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}", JsonConvert.SerializeObject(createdOrder), HttpStatusCode.OK);
            Assert.IsTrue(holdFormattedNumber == updatedOrder.FormattedNumber);
            Assert.AreEqual(ctx.OrderItemTaxAssessment.Where(x => x.BID == TestConstants.BID && x.OrderItemID == createdOrderItemId).Count(), 2);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            Assert.AreEqual(ctx.OrderItemTaxAssessment.Where(x => x.BID == TestConstants.BID && x.OrderItemID == createdOrderItemId).Count(), 0);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            try
            {
                ctx.CompanyData.Remove(company);
                ctx.SaveChanges();
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }

            #endregion
        }

        [TestMethod]
        public async Task TestOrderPutError()
        {
            #region CREATE
            var testOrder = Utils.GetTestOrderData();

            testOrder.LocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = testOrder.LocationID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.LocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            #endregion


            #region UPDATE
            createdOrder.FormattedNumber = "INV-123";
            var testPut = await EndpointTests.AssertPutStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}", JsonConvert.SerializeObject(createdOrder), HttpStatusCode.OK);
            Assert.IsNotNull(testPut);

            createdOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdOrder.ID}", JsonConvert.SerializeObject(createdOrder), HttpStatusCode.BadRequest);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);

            #endregion
        }

        [TestMethod]
        public async Task TestOrderChangeCreatedKeyDateToCurrentDate()
        {
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/CREATED/", HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.Dates);
            var createdDate = retrievedOrder.Dates.Where(t => t.KeyDateType == OrderKeyDateType.Created);
            Assert.IsNotNull(createdDate);
            Assert.AreEqual(1, createdDate.Count());

            var dateDiff = DateTime.Now - createdDate.First().KeyDT;
            Assert.IsTrue(dateDiff.TotalMinutes / 60 / 60 < 1);

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/CREATED/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);

            #endregion
        }

        [TestMethod]
        public async Task TestOrderChangeCreatedKeyDateToSpecifiedDate()
        {
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            DateTime myDate = DateTime.Now.AddDays(2).Date;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/created/{myDate.ToString("yyyy-MM-dd")}", HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.Dates);
            var createdDate = retrievedOrder.Dates.Where(t => t.KeyDateType == OrderKeyDateType.Created);
            Assert.IsNotNull(createdDate);
            Assert.AreEqual(1, createdDate.Count());

            Assert.AreEqual(myDate.ToUniversalTime(), createdDate.First().KeyDT);

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/CREATED/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);

            #endregion
        }

        [TestMethod]
        public async Task TestOrderChangeCreatedKeyDateToSpecifiedDateTime()
        {
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            DateTime myDate = DateTime.Now.AddDays(2).Date.Add(new TimeSpan(2, 13, 0));
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/created/{myDate.ToString("yyyy-MM-ddTHH:mm:ss")}", HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.Dates);
            var createdDate = retrievedOrder.Dates.Where(t => t.KeyDateType == OrderKeyDateType.Created);
            Assert.IsNotNull(createdDate);
            Assert.AreEqual(1, createdDate.Count());

            Assert.AreEqual(myDate.ToUniversalTime(), createdDate.First().KeyDT);

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/CREATED/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);

            #endregion
        }

        [TestMethod]
        public async Task TestOrderKeyDateCreation()
        {
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await assertChangingOrderStatus(testOrder, OrderOrderStatus.OrderInvoiced, HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.Dates);

            Assert.IsTrue(IsFound(retrievedOrder.Dates, OrderKeyDateType.Created));
            Assert.IsTrue(IsFound(retrievedOrder.Dates, OrderKeyDateType.ReleasedToWIP));
            Assert.IsTrue(IsFound(retrievedOrder.Dates, OrderKeyDateType.Built));
            Assert.IsTrue(IsFound(retrievedOrder.Dates, OrderKeyDateType.InInvoicing));
            Assert.IsTrue(IsFound(retrievedOrder.Dates, OrderKeyDateType.Invoiced));

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/Created/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/ReleasedToWIP/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/Built/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/InInvoicing/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/date/Invoiced/", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);

            #endregion
        }

        private bool IsFound(ICollection<OrderKeyDate> OrderKeyDates, OrderKeyDateType okdt)
        {
            var foundKeyDates = OrderKeyDates.Where(t => t.KeyDateType == okdt);
            if (foundKeyDates.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [TestMethod]
        public async Task TestOrderSetPrimaryContact()
        {
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = _companyID;
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", testOrder, HttpStatusCode.OK);

            int contactId = await Utils.GetContactID(client, testOrder.CompanyID);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkcontact/primary/{contactId}", HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}?ContactLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.ContactRoles);
            var primaryRows = retrievedOrder.ContactRoles.Where(t => t.RoleType == OrderContactRoleType.Primary);
            Assert.IsNotNull(primaryRows);
            Assert.AreEqual(1, primaryRows.Count());
            Assert.AreEqual(OrderContactRoleType.Primary, primaryRows.ToList()[0].RoleType);
            Assert.AreEqual(contactId, primaryRows.ToList()[0].ContactID);

            #region DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        [TestMethod]
        public async Task TestOrderDeletePrimaryContact()
        {
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = _companyID;
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", testOrder, HttpStatusCode.OK);

            int contactId = await Utils.GetContactID(client, testOrder.CompanyID);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkcontact/primary/{contactId}", HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}?ContactLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.ContactRoles);
            var primaryRows = retrievedOrder.ContactRoles.Where(t => t.RoleType == OrderContactRoleType.Primary);
            Assert.IsNotNull(primaryRows);
            Assert.AreEqual(1, primaryRows.Count());
            Assert.AreEqual(OrderContactRoleType.Primary, primaryRows.ToList()[0].RoleType);
            Assert.AreEqual(contactId, primaryRows.ToList()[0].ContactID);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkcontact/primary/222", HttpStatusCode.BadRequest);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkcontact/primary/{contactId}", HttpStatusCode.OK);

            retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}?ContactLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.ContactRoles);
            primaryRows = retrievedOrder.ContactRoles.Where(t => t.RoleType == OrderContactRoleType.Primary);
            Assert.IsNotNull(primaryRows);
            Assert.AreEqual(0, primaryRows.Count());

            #region DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        [TestMethod]
        public async Task TestOrderSetEnteredByEmployee()
        {
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", testOrder, HttpStatusCode.OK);

            int employeeId = await Utils.GetEmployeeID(client);
            var ctx = GetApiContext();
            var employeeRoleId = ctx.EmployeeRole.Where(er => er.Name == "Entered By").FirstOrDefault().ID;
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkemployee/{employeeRoleId}/{employeeId}", HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}?EmployeeLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.EmployeeRoles);
            var enteredByRows = retrievedOrder.EmployeeRoles.Where(t => t.RoleID == 254);
            Assert.IsNotNull(enteredByRows);
            Assert.AreEqual(1, enteredByRows.Count());
            Assert.AreEqual(employeeId, enteredByRows.ToList()[0].EmployeeID);

            #region DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        [TestMethod]
        public async Task TestOrderDeleteEnteredByEmployee()
        {
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", testOrder, HttpStatusCode.OK);

            int employeeId = await Utils.GetEmployeeID(client);
            var ctx = GetApiContext();
            var employeeRoleId = ctx.EmployeeRole.Where(er => er.Name == "Entered By").FirstOrDefault().ID;
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkEmployee/{employeeRoleId}/{employeeId}", HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}?EmployeeLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.EmployeeRoles);
            var enteredByRows = retrievedOrder.EmployeeRoles.Where(t => t.RoleID == employeeRoleId);
            Assert.IsNotNull(enteredByRows);
            Assert.AreEqual(1, enteredByRows.Count());
            Assert.AreEqual(employeeId, enteredByRows.ToList()[0].EmployeeID);

            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkEmployee/{employeeRoleId}/222", HttpStatusCode.BadRequest);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkEmployee/{employeeRoleId}/{employeeId}", HttpStatusCode.OK);

            retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}?EmployeeLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder?.EmployeeRoles);
            enteredByRows = retrievedOrder.EmployeeRoles.Where(t => t.RoleID == 254);
            Assert.IsNotNull(enteredByRows);
            Assert.AreEqual(0, enteredByRows.Count());

            #region DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        [TestMethod]
        public async Task TestOrderNotesCRUDOperations()
        {
            OrderData testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            OrderData createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            OrderNote orderNote = new OrderNote()
            {
                BID = TestConstants.BID,
                ModifiedDT = DateTime.Now,
                NoteType = OrderNoteType.Sales,
                Note = "Test Note",
                IsActive = true,
            };

            string createUpdateOrDeleteNoteEndpoint = $"{apiUrl}/{createdOrder.ID}/action/notes/{orderNote.NoteType}";

            //Create Note
            var createdNote = await EndpointTests.AssertPostStatusCode<OrderNote>(this.client, createUpdateOrDeleteNoteEndpoint, JsonConvert.SerializeObject(orderNote), HttpStatusCode.OK);
            Assert.IsNotNull(createdNote);

            //Get Note by ID
            var savedNote = await EndpointTests.AssertGetStatusCode<OrderNote>(this.client, $"{apiUrl}/{createdOrder.ID}/notes/{createdNote.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(savedNote);
            Assert.AreEqual(savedNote.ID, createdNote.ID);
            Assert.AreEqual(savedNote.Note, createdNote.Note);

            //Update Note
            string updatedNoteText = "Test Note Updated!";
            createdNote.Note = updatedNoteText;
            var updatedNote = await EndpointTests.AssertPutStatusCode<OrderNote>(this.client, $"{apiUrl}/{createdOrder.ID}/action/notes/{createdNote.ID}", JsonConvert.SerializeObject(createdNote), HttpStatusCode.OK);
            Assert.AreEqual(updatedNoteText, updatedNote.Note);

            // Get all notes
            OrderNote[] orderNotes = await EndpointTests.AssertGetStatusCode<OrderNote[]>(this.client, $"{apiUrl}/{createdOrder.ID}/notes?orderNoteType={orderNote.NoteType}", HttpStatusCode.OK);
            updatedNote = orderNotes.FirstOrDefault(n => n.ID == createdNote.ID && n.Note == updatedNoteText);
            Assert.IsNotNull(updatedNote);

            // Set OrderNote IsActive=false
            await EndpointTests.AssertPostStatusCode<OrderNote>(this.client, $"{apiUrl}/{createdOrder.ID}/action/notes/{createdNote.ID}/setinactive", JsonConvert.SerializeObject(orderNote), HttpStatusCode.OK);
            savedNote = await EndpointTests.AssertGetStatusCode<OrderNote>(this.client, $"{apiUrl}/{createdOrder.ID}/notes/{createdNote.ID}", HttpStatusCode.OK);
            Assert.IsFalse(savedNote.IsActive);

            // Set OrderNote IsActive=true
            await EndpointTests.AssertPostStatusCode<OrderNote>(this.client, $"{apiUrl}/{createdOrder.ID}/action/notes/{createdNote.ID}/setactive", JsonConvert.SerializeObject(orderNote), HttpStatusCode.OK);
            savedNote = await EndpointTests.AssertGetStatusCode<OrderNote>(this.client, $"{apiUrl}/{createdOrder.ID}/notes/{createdNote.ID}", HttpStatusCode.OK);
            Assert.IsTrue(savedNote.IsActive);

            //Delete Order Note
            EntityActionChangeResponse entityActionChangeResponse = await EndpointTests.AssertDeleteStatusCode<EntityActionChangeResponse>(this.client, $"{apiUrl}/{createdOrder.ID}/action/notes/{createdNote.ID}", HttpStatusCode.OK);
            // Assert.IsTrue(entityActionChangeResponse.Success);
            // Assert.IsFalse(entityActionChangeResponse.HasError);

            #region DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        [TestMethod]
        public async Task OrderOrderLinkTest()
        {
            OrderData testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            OrderData createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            OrderData clonedOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            string linkOrderEndpoint = $"{apiUrl}/{createdOrder.ID}/action/linkOrder/{OrderOrderLinkType.ClonedTo}/{clonedOrder.ID}";
            string unlinkOrderEndpoint = $"{apiUrl}/{createdOrder.ID}/action/unlinkOrder/{OrderOrderLinkType.ClonedTo}/{clonedOrder.ID}";
            string updatelinkOrderEndpoint = $"{apiUrl}/{createdOrder.ID}/action/updateorderlink/{OrderOrderLinkType.ClonedTo}/{clonedOrder.ID}";

            OrderOrderLink createdOrderCreatedLink = Utils.GetOrderOrderLink(createdOrder, clonedOrder);
            //Create Link
            var createdLink = await EndpointTests.AssertPostStatusCode<OrderOrderLink>(this.client, linkOrderEndpoint, JsonConvert.SerializeObject(createdOrderCreatedLink), HttpStatusCode.OK);

            OrderOrderLink[] links = await EndpointTests.AssertGetStatusCode<OrderOrderLink[]>(this.client, $"{apiUrl}/{createdOrder.ID}/links", HttpStatusCode.OK);
            OrderOrderLink[] linkedLinks = await EndpointTests.AssertGetStatusCode<OrderOrderLink[]>(this.client, $"{apiUrl}/{clonedOrder.ID}/links", HttpStatusCode.OK);

            Assert.IsNotNull(links.FirstOrDefault(t => t.BID == createdOrderCreatedLink.BID && createdOrderCreatedLink.OrderID == t.OrderID && createdOrderCreatedLink.LinkedOrderID == t.LinkedOrderID));
            Assert.IsNotNull(linkedLinks.FirstOrDefault(t => t.BID == createdOrderCreatedLink.BID && createdOrderCreatedLink.LinkedOrderID == t.OrderID && createdOrderCreatedLink.OrderID == t.LinkedOrderID));
            //Update Link
            string updatedOrderLinkDescription = "Order to Order New Description";

            createdLink.Description = updatedOrderLinkDescription;

            var updatedLink = await EndpointTests.AssertPostStatusCode<OrderOrderLink>(this.client, updatelinkOrderEndpoint, JsonConvert.SerializeObject(createdLink), HttpStatusCode.OK);
            Assert.AreEqual(updatedOrderLinkDescription, updatedLink.Description);

            //Delete Order Note
            await EndpointTests.AssertPostStatusCode(this.client, unlinkOrderEndpoint, HttpStatusCode.OK);

            await EndpointTests.AssertGetResponseContent(client, $"{apiUrl}/{createdOrder.ID}/links", "");
        }

        [TestMethod]
        public async Task CloneOrderToEstimateTest()
        {
            var ctx = GetApiContext();
            var testController = GetDMController();

            OrderData testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Order;
            testOrder.OrderStatusID = OrderOrderStatus.OrderWIP;
            testOrder.ClassTypeID = (int)ClassType.Order;

            testOrder.CompanyID = _companyID;
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            int contactId = await Utils.GetContactID(client, testOrder.CompanyID);
            int employeeId = await Utils.GetEmployeeID(client);
            var employeeRoleEnteredById = ctx.EmployeeRole.Where(er => er.Name == "Entered By").FirstOrDefault().ID;
            var employeeRoleSalesPersonId = ctx.EmployeeRole.Where(er => er.Name == "Salesperson").FirstOrDefault().ID;

            OrderItemData testOrderItem = new OrderItemData()
            {
                BID = testOrder.BID,
                HasCustomImage = false,
                HasDocuments = false,
                HasProof = false,
                ID = 1,
                OrderID = 0,
                ItemNumber = 1,
                Quantity = 1,
                Name = "test",
                IsOutsourced = false,
                OrderStatusID = testOrder.OrderStatusID,
                ItemStatusID = 18,
                ProductionLocationID = testOrder.ProductionLocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = testOrder.TaxGroupID,
                TaxGroupOV = false,
                IsTaxExempt = false,
                TransactionType = testOrder.TransactionType
            };

            testOrder.Items = new List<OrderItemData>();
            testOrder.Items.Add(testOrderItem);

            OrderData createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkcontact/primary/{contactId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkcontact/billing/{contactId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkEmployee/{employeeRoleEnteredById}/{employeeId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkEmployee/{employeeRoleSalesPersonId}/{employeeId}", HttpStatusCode.OK);

            var orderResult2 = await testController.Post(new DMController.RequestModel()
            {
                id = createdOrder.ID,
                ctid = Convert.ToInt32(ClassType.Order),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            }, null, "https://corebridge.net/wp-content/uploads/2019/01/AdobeStock_202169599-e1551199093506.jpg");

            AssertX.IsType<OkObjectResult>(orderResult2);
            AssertX.IsType<DMItem>(((OkObjectResult)orderResult2).Value);

            var orderLineItemResult = await testController.Post(new DMController.RequestModel()
            {
                id = createdOrder.Items.FirstOrDefault().ID,
                ctid = Convert.ToInt32(ClassType.OrderItem),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            }, null, "https://corebridge.net/wp-content/uploads/2019/01/AdobeStock_202169599-e1551199093506.jpg");

            AssertX.IsType<OkObjectResult>(orderLineItemResult);
            AssertX.IsType<DMItem>(((OkObjectResult)orderLineItemResult).Value);

            EstimateData convertedEstimate = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}/clone?SaveAsEstimate=true&CopyFiles=true", JsonConvert.SerializeObject(createdOrder), HttpStatusCode.OK);

            Assert.IsNotNull(convertedEstimate);
            Assert.IsTrue((convertedEstimate.ID != 0));

            var estimateResult = await testController.GetDocuments(new DMController.RequestModel()
            {
                id = convertedEstimate.ID,
                ctid = Convert.ToInt32(ClassType.Estimate),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            });
            AssertX.IsType<OkObjectResult>(estimateResult);
            AssertX.IsType<List<DMItem>>(((OkObjectResult)estimateResult).Value);
            Assert.IsTrue(((List<DMItem>)((OkObjectResult)estimateResult).Value).Count > 0);

            var retrievedOrderWithContact = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"/api/estimate/{convertedEstimate.ID}?ContactLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderWithContact?.ContactRoles);
            var contactRows = retrievedOrderWithContact.ContactRoles.Where(t => t.OrderID == convertedEstimate.ID);
            Assert.IsNotNull(contactRows);
            Assert.AreEqual(2, contactRows.Count());
            Assert.AreEqual(OrderContactRoleType.Primary, contactRows.ToList()[0].RoleType);
            Assert.AreEqual(OrderContactRoleType.Billing, contactRows.ToList()[1].RoleType);
            Assert.AreEqual(contactId, contactRows.ToList()[0].ContactID);
            Assert.AreEqual(contactId, contactRows.ToList()[1].ContactID);

            var retrievedEstimateWithEmployee = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"/api/estimate/{convertedEstimate.ID}?EmployeeLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedEstimateWithEmployee?.EmployeeRoles);
            var employeeRows = retrievedEstimateWithEmployee.EmployeeRoles.Where(t => t.OrderID == convertedEstimate.ID);
            Assert.IsNotNull(employeeRows);
            Assert.AreEqual(2, employeeRows.Count());
            Assert.IsNotNull(employeeRows.FirstOrDefault(t => t.RoleID == employeeRoleEnteredById));
            Assert.IsNotNull(employeeRows.FirstOrDefault(t => t.RoleID == employeeRoleSalesPersonId));

            var retrievedEstimateWithOrderItem = await EndpointTests.AssertGetStatusCode<EstimateData>(client, $"/api/estimate/{convertedEstimate.ID}?ItemLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedEstimateWithOrderItem.Items);

            var orderItemResult = await testController.GetDocuments(new DMController.RequestModel()
            {
                id = retrievedEstimateWithOrderItem.Items.FirstOrDefault().ID,
                ctid = Convert.ToInt32(ClassType.OrderItem),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            });
            AssertX.IsType<OkObjectResult>(orderItemResult);
            AssertX.IsType<List<DMItem>>(((OkObjectResult)orderItemResult).Value);
            Assert.IsTrue(((List<DMItem>)((OkObjectResult)orderItemResult).Value).Count > 0);

            //CLEAN UP...
            var createdEstimateLink = ctx.OrderOrderLink.Where(l => l.BID == TestConstants.BID && l.LinkedOrderID == createdOrder.ID).FirstOrDefault();
            Assert.IsNotNull(createdEstimateLink);
            var convertedOrderLink = ctx.OrderOrderLink.Where(l => l.BID == TestConstants.BID && l.LinkedOrderID == convertedEstimate.ID).FirstOrDefault();
            Assert.IsNotNull(convertedOrderLink);
            ctx.OrderOrderLink.Remove(createdEstimateLink);
            ctx.OrderOrderLink.Remove(convertedOrderLink);
            ctx.SaveChanges();

            var deleteEstimateResult = testController.DeleteAll(Convert.ToInt32(ClassType.Estimate), convertedEstimate.ID);
            var deleteEstimateLineItemResult = testController.DeleteAll(Convert.ToInt32(ClassType.OrderItem), createdOrder.Items.FirstOrDefault().ID);
            var deleteOrderResult = testController.DeleteAll(Convert.ToInt32(ClassType.Order), createdOrder.ID);
            var deleteOrderLineItemResult = testController.DeleteAll(Convert.ToInt32(ClassType.OrderItem), retrievedEstimateWithOrderItem.Items.FirstOrDefault().ID);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/estimate/{convertedEstimate.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task LineItemStatusChangeTest()
        {
            var ctx = GetApiContext();
            var testController = GetDMController();

            OrderData testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Order;
            testOrder.OrderStatusID = OrderOrderStatus.OrderPreWIP;
            testOrder.ClassTypeID = (int)ClassType.Order;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            int contactId = await Utils.GetContactID(client, testOrder.CompanyID);
            int employeeId = await Utils.GetEmployeeID(client);
            var employeeRoleEnteredById = ctx.EmployeeRole.Where(er => er.Name == "Entered By").FirstOrDefault().ID;
            var employeeRoleSalesPersonId = ctx.EmployeeRole.Where(er => er.Name == "Salesperson").FirstOrDefault().ID;

            OrderItemData testOrderItem = new OrderItemData()
            {
                BID = testOrder.BID,
                HasCustomImage = false,
                HasDocuments = false,
                HasProof = false,
                ID = 1,
                OrderID = 0,
                ItemNumber = 1,
                Quantity = 1,
                Name = "TestA",
                IsOutsourced = false,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                ItemStatusID = 21, //21=Pre-WIP
                ProductionLocationID = testOrder.ProductionLocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = testOrder.TaxGroupID,
                TaxGroupOV = false,
                IsTaxExempt = false,
                TransactionType = testOrder.TransactionType
            };

            OrderItemData testOrderItem2 = new OrderItemData()
            {
                BID = testOrder.BID,
                HasCustomImage = false,
                HasDocuments = false,
                HasProof = false,
                ID = 1,
                OrderID = 0,
                ItemNumber = 1,
                Quantity = 1,
                Name = "TestB",
                IsOutsourced = false,
                OrderStatusID = OrderOrderStatus.OrderInvoiced,
                ItemStatusID = (byte)OrderOrderStatus.OrderInvoiced,
                ProductionLocationID = testOrder.ProductionLocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = testOrder.TaxGroupID,
                TaxGroupOV = false,
                IsTaxExempt = false,
                TransactionType = testOrder.TransactionType
            };


            testOrder.Items = new List<OrderItemData>();
            testOrder.Items.Add(testOrderItem);
            testOrder.Items.Add(testOrderItem2);

            OrderData createdOrder = null;
            try
            {
                //create order
                createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
                Assert.IsNotNull(createdOrder);

                //change order status from Pre-WIP to WIP
                string orderStatusEndpoint = $"{apiUrl}/{createdOrder.ID}/action/changeorderstatus/{Convert.ToInt32(OrderOrderStatus.OrderWIP)}";
                await EndpointTests.AssertPostStatusCode(this.client, orderStatusEndpoint, HttpStatusCode.OK);

                var items = await ctx.OrderItemData.Where(i => i.OrderID == createdOrder.ID).ToListAsync();
                Assert.IsNotNull(items);
                Assert.AreEqual(items.Count, 2);

                //expected: line item 1 should have order line item status equal with order's status 
                Assert.AreEqual(items.FirstOrDefault().ItemStatusID, 22);

                //expected: line item 2 status shuld be left unchanged because OrderPreWIP is less that OrderInvoiced status.
                Assert.AreEqual(items.LastOrDefault().ItemStatusID, 25);

            }
            finally
            {
                //CLEAN UP...
                await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            }

        }

        [TestMethod]
        public async Task OrderToEstimateOrderLinkTest()
        {
            OrderData testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Order;
            testOrder.OrderStatusID = OrderOrderStatus.OrderPreWIP;
            testOrder.ClassTypeID = (int)ClassType.Order;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            OrderData createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            testOrder.TransactionType = (byte)OrderTransactionType.Estimate;
            testOrder.OrderStatusID = OrderOrderStatus.EstimatePending;
            testOrder.ClassTypeID = (int)ClassType.Estimate;
            OrderData clonedOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            string linkOrderEndpoint = $"{apiUrl}/{createdOrder.ID}/action/linkOrder/{OrderOrderLinkType.ClonedTo}/{clonedOrder.ID}";
            string unlinkOrderEndpoint = $"{apiUrl}/{createdOrder.ID}/action/unlinkOrder/{OrderOrderLinkType.ClonedTo}/{clonedOrder.ID}";
            string updatelinkOrderEndpoint = $"{apiUrl}/{createdOrder.ID}/action/updateorderlink/{OrderOrderLinkType.ClonedTo}/{clonedOrder.ID}";

            OrderOrderLink createdOrderCreatedLink = Utils.GetOrderOrderLink(createdOrder, clonedOrder);
            //Create Link
            var createdLink = await EndpointTests.AssertPostStatusCode<OrderOrderLink>(this.client, linkOrderEndpoint, JsonConvert.SerializeObject(createdOrderCreatedLink), HttpStatusCode.OK);

            OrderOrderLink[] links = await EndpointTests.AssertGetStatusCode<OrderOrderLink[]>(this.client, $"{apiUrl}/{createdOrder.ID}/links", HttpStatusCode.OK);
            OrderOrderLink[] linkedLinks = await EndpointTests.AssertGetStatusCode<OrderOrderLink[]>(this.client, $"{apiUrl}/{clonedOrder.ID}/links", HttpStatusCode.OK);

            Assert.IsNotNull(links.FirstOrDefault(t => t.BID == createdOrderCreatedLink.BID && createdOrderCreatedLink.OrderID == t.OrderID && createdOrderCreatedLink.LinkedOrderID == t.LinkedOrderID));
            Assert.IsNotNull(linkedLinks.FirstOrDefault(t => t.BID == createdOrderCreatedLink.BID && createdOrderCreatedLink.LinkedOrderID == t.OrderID && createdOrderCreatedLink.OrderID == t.LinkedOrderID));
            //Update Link
            string updatedOrderLinkDescription = "Order to Order New Description";

            createdLink.Description = updatedOrderLinkDescription;

            var updatedLink = await EndpointTests.AssertPostStatusCode<OrderOrderLink>(this.client, updatelinkOrderEndpoint, JsonConvert.SerializeObject(createdLink), HttpStatusCode.OK);
            Assert.AreEqual(updatedOrderLinkDescription, updatedLink.Description);

            //Delete Order Note
            await EndpointTests.AssertPostStatusCode(this.client, unlinkOrderEndpoint, HttpStatusCode.OK);

            await EndpointTests.AssertGetResponseContent(client, $"{apiUrl}/{createdOrder.ID}/links", "");
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task ChangeOrderStatusTest()
        {
            var allowedOrderTypeStatusMap = new Dictionary<OrderTransactionType, OrderOrderStatus[]>()
            {
                {
                    OrderTransactionType.Order,
                    new OrderOrderStatus[] {
                        OrderOrderStatus.OrderBuilt,
                        OrderOrderStatus.OrderClosed,
                        OrderOrderStatus.OrderInvoiced,
                        OrderOrderStatus.OrderInvoicing,
                        OrderOrderStatus.OrderPreWIP,
                        OrderOrderStatus.OrderVoided,
                        OrderOrderStatus.OrderWIP,
                    }
                }
            };

            OrderOrderStatus[] voidStatuses = new OrderOrderStatus[] {
                OrderOrderStatus.OrderVoided,
                OrderOrderStatus.EstimateVoided,
                OrderOrderStatus.POVoided
            };

            OrderData testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            // Regular Order status can't be initally void
            testOrder.TransactionType = (byte)OrderTransactionType.Order;
            testOrder.OrderStatusID = OrderOrderStatus.OrderVoided;
            await assertChangingOrderStatus(testOrder, OrderOrderStatus.OrderWIP, HttpStatusCode.BadRequest);

            // Orders can only change the status within its allowed OrderStatus
            foreach (KeyValuePair<OrderTransactionType, OrderOrderStatus[]> itemMap in allowedOrderTypeStatusMap)
            {
                // get other OrderTypes status
                List<OrderOrderStatus> otherInvalidStatuses = allowedOrderTypeStatusMap.Where(item => item.Key != itemMap.Key).SelectMany(x => x.Value).ToList();

                // run through all the non-void statuses
                foreach (OrderOrderStatus orderStatus in itemMap.Value.Where(item => !voidStatuses.Contains(item)))
                {
                    testOrder.TransactionType = (byte)itemMap.Key;
                    testOrder.OrderStatusID = orderStatus;

                    // test invalid order status change
                    foreach (OrderOrderStatus otherStatus in otherInvalidStatuses)
                    {
                        await assertChangingOrderStatus(testOrder, otherStatus, HttpStatusCode.BadRequest);
                    }

                    // test valid order status change
                    var otherValidStatuses = itemMap.Value.Where(item => item != orderStatus && !voidStatuses.Contains(item)).ToList();
                    foreach (OrderOrderStatus otherStatus in otherValidStatuses)
                    {
                        await assertChangingOrderStatus(testOrder, otherStatus, HttpStatusCode.OK);
                    }
                }
            }
        }

        private async Task<OrderData> assertChangingOrderStatus(OrderData testOrder, OrderOrderStatus newOrderStatus, HttpStatusCode expectedStatusCode)
        {
            OrderData order = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            string orderStatusEndpoint = $"{apiUrl}/{order.ID}/action/changeorderstatus/{Convert.ToInt32(newOrderStatus)}";
            await EndpointTests.AssertPostStatusCode(this.client, orderStatusEndpoint, expectedStatusCode);
            return order;
        }

        [TestMethod]
        public async Task CheckOrderStatusTest()
        {
            OrderData testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            // Valid Order change
            testOrder.TransactionType = (byte)OrderTransactionType.Order;
            testOrder.OrderStatusID = OrderOrderStatus.OrderInvoicing;
            await testCheckOrderStatus(testOrder, OrderOrderStatus.OrderInvoiced, HttpStatusCode.OK);
        }

        private async Task testCheckOrderStatus(OrderData testOrder, OrderOrderStatus newOrderStatus, HttpStatusCode expectedStatusCode)
        {
            OrderData order = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            string orderStatusEndpoint = $"{apiUrl}/{order.ID}/action/checkorderstatus/";
            await EndpointTests.AssertPostStatusCode(this.client, orderStatusEndpoint, expectedStatusCode);
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task RecomputeOrderGLTest()
        {
            var priority = "low";
            OrderData testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            // test INVALID priority
            priority = "invalid";
            await EndpointTests.AssertPostStatusCode(this.client, $"{apiUrl}/{createdOrder.ID}/action/recomputeGL?priority={priority}", HttpStatusCode.BadRequest);

            // test low priority
            priority = "low";
            await EndpointTests.AssertPostStatusCode(this.client, $"{apiUrl}/{createdOrder.ID}/action/recomputeGL?priority={priority}", HttpStatusCode.OK);
            // test medium priority
            priority = "medium";
            await EndpointTests.AssertPostStatusCode(this.client, $"{apiUrl}/{createdOrder.ID}/action/recomputeGL?priority={priority}", HttpStatusCode.OK);
            // test high priority
            priority = "high";
            await EndpointTests.AssertPostStatusCode(this.client, $"{apiUrl}/{createdOrder.ID}/action/recomputeGL?priority={priority}", HttpStatusCode.OK);
            // Test RecomputeGL with non existing order ID
            var failID = -12345;
            string orderStatusEndpoint = $"{apiUrl}/{failID}/action/recomputeGL?priority={priority}";
            await EndpointTests.AssertPostStatusCode(this.client, orderStatusEndpoint, HttpStatusCode.NotFound);
        }

        [TestMethod]
        [TestCategory("GL")]
        [TestCategory("compute")]
        public async Task TestOrderCompute()
        {
            await CreateMaterial(ctx);
            //test invalid request
            object invalidRequest = JsonConvert.DeserializeObject(@"{ 'Items': false }"); // force invalid request by setting OrderpriceRequest.Result property to invalid type
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(invalidRequest), HttpStatusCode.BadRequest);
            decimal qty = 3;

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>(){
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material.ID,
                    TotalQuantity  = qty,
                    TotalQuantityOV = true,
                    PriceUnit = _materialPrice
                }
            };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = qty,
                EngineType = PricingEngineType.SimplePart,
                Components = components,
                TaxInfoList = new List<TaxAssessmentRequest>()
                {
                    new TaxAssessmentRequest()
                    {
                        Quantity = qty,
                        IsTaxExempt = false,
                        NexusID = _taxGroupID.ToString()
                    }
                }
            };
            OrderPriceRequest orderRequest = new OrderPriceRequest()
            {
                Items = new List<ItemPriceRequest>()
                {
                    itemPriceRequest
                }
            };

            string jsonString = JsonConvert.SerializeObject(orderRequest);
            OrderPriceResult result = await EndpointTests.AssertPostStatusCode<OrderPriceResult>(client, $"{apiUrl}/compute", jsonString, HttpStatusCode.OK);

            Assert.IsNotNull(result);
            Assert.AreEqual(15m, result.PricePreTax);

            Assert.IsNotNull(result.Items);
            Assert.AreEqual(1, result.Items.Count);

            Assert.AreEqual(15m, result.Items[0].PricePreTax);
            Assert.AreEqual(1, result.Items[0].Components.Count);
            
        }
        private MaterialData material = null;
        private const int _materialID = -99;
        private const decimal _materialPrice = 5m;
        private const decimal _materialCost = 2.5m;
        private int glAccountID = 0;

        /// <summary>
        /// Creates a new Material for testing
        /// </summary>
        /// <param name="estimatingPrice">EstimatingPrice to use (default 5m)</param>
        /// <param name="estimatingCost">EstimatingCost to use (default 2.5m)</param>
        /// <param name="ID">MaterialID (default -99)</param>
        /// <returns></returns>
        protected async Task CreateMaterial(ApiContext ctx, decimal estimatingPrice = _materialPrice, decimal estimatingCost = _materialCost, int ID = _materialID, string materialName = "Test Material For Pricing")
        {
            glAccountID = ctx.GLAccount.FirstOrDefault(t => t.BID == BID).ID;
            material = await ctx.MaterialData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == ID);
            if (material == null)
            {
                material = new MaterialData()
                {
                    ID = ID,
                    BID = BID,
                    Name = materialName,
                    InvoiceText = materialName,
                    ExpenseAccountID = glAccountID,
                    IncomeAccountID = glAccountID,
                    InventoryAccountID = glAccountID,
                    EstimatingPrice = estimatingPrice,
                    EstimatingCost = estimatingCost,
                    Length = new Measurement(1, Unit.Inch),
                    Width = new Measurement(1, Unit.Inch),
                    Height = new Measurement(1, Unit.Inch),
                    Weight = new Measurement(1, Unit.Inch),
                };

                ctx.MaterialData.Add(material);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
            }
        }

        [TestMethod]
        [TestCategory("GL")]
        [TestCategory("compute")]
        public async Task TestOrderTaxCompute()
        {
            await CreateMaterial(ctx);
            //test invalid request
            object invalidRequest = JsonConvert.DeserializeObject(@"{ 'Items': false }"); // force invalid request by setting OrderpriceRequest.Result property to invalid type
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(invalidRequest), HttpStatusCode.BadRequest);


            decimal qty = 3;

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>(){
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material.ID,
                    TotalQuantity = qty,
                    TotalQuantityOV = true,
                    PriceUnit = _materialPrice
                }
            };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = qty,
                EngineType = PricingEngineType.SimplePart,
                Components = components,
                
            };
            OrderPriceRequest orderRequest = new OrderPriceRequest()
            {
                Items = new List<ItemPriceRequest>()
                {
                    itemPriceRequest
                }
            };

            string jsonString = JsonConvert.SerializeObject(orderRequest);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/compute?computeTax=true", jsonString, HttpStatusCode.BadRequest);
            itemPriceRequest.TaxInfoList = new List<TaxAssessmentRequest>()
                {
                    new TaxAssessmentRequest()
                    {
                        Quantity = qty,
                        IsTaxExempt = false,
                        NexusID = _taxGroupID.ToString()
                    }
                };
            jsonString = JsonConvert.SerializeObject(orderRequest);
            OrderPriceResult result = await EndpointTests.AssertPostStatusCode<OrderPriceResult>(client, $"{apiUrl}/compute?computeTax=true", jsonString, HttpStatusCode.OK);

            Assert.IsNotNull(result);
            Assert.AreEqual(15m, result.PricePreTax);
            Assert.AreEqual(16.5m, result.PriceTotal);
            Assert.AreEqual(1.5m, result.TaxAmount);

            Assert.IsNotNull(result.Items);
            Assert.AreEqual(1, result.Items.Count);

            Assert.AreEqual(15m, result.Items[0].PricePreTax);
            Assert.AreEqual(16.5m, result.Items[0].PriceTotal);
            Assert.AreEqual(1.5m, result.Items[0].TaxAmount);
            Assert.AreEqual(1, result.Items[0].Components.Count);

        }

        [TestMethod]
        public async Task TestOrderOnlineTaxCompute() ///Used for testing retrieval of Registration Key
        {

            await CreateMaterial(ctx);
            decimal qty = 3;

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>(){
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material.ID,
                    TotalQuantity = qty,
                    TotalQuantityOV = true,
                    PriceUnit = _materialPrice
                }
            };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = qty,
                EngineType = PricingEngineType.SimplePart,
                Components = components,

            };
            OrderPriceRequest orderRequest = new OrderPriceRequest()
            {
                Items = new List<ItemPriceRequest>()
                {
                    itemPriceRequest
                }
            };

            itemPriceRequest.TaxInfoList = new List<TaxAssessmentRequest>()
                {
                    new TaxAssessmentRequest()
                    {
                        Quantity = qty,
                        IsTaxExempt = false,
                        NexusID = "1" //default for taxJar
                    }
                };
            itemPriceRequest.TaxNexusList = new Dictionary<string, TaxAssessmentNexus>()
            {
                {
                    "1", new TaxAssessmentNexus()
                    {
                        EngineType=TaxEngineType.TaxJar,
                        ToAddress= new Address()
                        {
                            Country = "US",
                            State = "LA",
                            PostalCode = "70809"
                        },
                        UseSameToFromAddress=true
                    } 
                }
            };
            var jsonString = JsonConvert.SerializeObject(orderRequest);
            OrderPriceResult result = await EndpointTests.AssertPostStatusCode<OrderPriceResult>(client, $"{apiUrl}/compute?computeTax=true", jsonString, HttpStatusCode.OK);

            Assert.IsNotNull(result);
            Assert.AreEqual(15m, result.PricePreTax);
            Assert.AreEqual(16.5m, result.PriceTotal);
            Assert.AreEqual(1.5m, result.TaxAmount);

            Assert.IsNotNull(result.Items);
            Assert.AreEqual(1, result.Items.Count);

            Assert.AreEqual(15m, result.Items[0].PricePreTax);
            Assert.AreEqual(16.5m, result.Items[0].PriceTotal);
            Assert.AreEqual(1.5m, result.Items[0].TaxAmount);
            Assert.AreEqual(1, result.Items[0].Components.Count);

        }

        [TestMethod]
        [TestCategory("GL")]
        [TestCategory("compute")]
        public async Task TestItemTaxCompute()
        {
            await CreateMaterial(ctx);
            //test invalid request
            object invalidRequest = JsonConvert.DeserializeObject(@"{ 'Items': false }"); // force invalid request by setting OrderpriceRequest.Result property to invalid type
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/compute", JsonConvert.SerializeObject(invalidRequest), HttpStatusCode.BadRequest);
            decimal qty = 3;

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>(){
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material.ID,
                    TotalQuantity = qty,
                    TotalQuantityOV = true,
                    PriceUnit = _materialPrice
                }
            };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = qty,
                EngineType = PricingEngineType.SimplePart,
                Components = components,
                
            };

            string jsonString = JsonConvert.SerializeObject(itemPriceRequest);
            await EndpointTests.AssertPostStatusCode(client, $"/api/orderitem/compute?computeTax=true", jsonString, HttpStatusCode.BadRequest);
            itemPriceRequest.TaxInfoList = new List<TaxAssessmentRequest>()
                {
                    new TaxAssessmentRequest()
                    {
                        Quantity = qty,
                        IsTaxExempt = false,
                        NexusID = _taxGroupID.ToString()
                    }
                };
            jsonString = JsonConvert.SerializeObject(itemPriceRequest);
            ItemPriceResult result = await EndpointTests.AssertPostStatusCode<ItemPriceResult>(client, $"/api/orderitem/compute?computeTax=true", jsonString, HttpStatusCode.OK);

            Assert.IsNotNull(result);
            Assert.AreEqual(15m, result.PricePreTax);
            Assert.AreEqual(16.5m, result.PriceTotal);
            Assert.AreEqual(1.5m, result.TaxAmount);

        }

        [TestMethod]
        public async Task GetOrderDestinationsForOrder()
        {
            //Create Order
            var testOrder = Utils.GetTestOrderData();

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            //Create Item
            var testOrderItem = Utils.GetTestOrderItemData(createdOrder);
            var createdOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"/api/orderitem/", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);

            // setup test data
            OrderData orderData = (await EndpointTests.AssertGetStatusCode<OrderData>(client, $"api/order/{createdOrderItem.OrderID}", HttpStatusCode.OK));
            OrderItemStatus orderItemStatus = (await EndpointTests.AssertGetStatusCode<OrderItemStatus>(client, $"api/orderitemstatus/{createdOrderItem.ItemStatusID}", HttpStatusCode.OK));

            //Start orderdestination
            string destinationApiUrl = "/api/orderdestination";

            OrderDestinationData postRequestModel = Utils.GetTestOrderDestinationData("Test", 1, orderItemStatus.ID, createdOrderItem.ID, orderData.ID, 0m, (OrderTransactionType)createdOrder.TransactionType);
            var postResult = await EndpointTests.AssertPostStatusCode<OrderDestinationData>(client, destinationApiUrl, JsonConvert.SerializeObject(postRequestModel), HttpStatusCode.OK);
            var retrievedPostResult = await EndpointTests.AssertGetStatusCode<OrderDestinationData[]>(client, $"api/order/{orderData.ID}/orderdestination/", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedPostResult);

            var retrievedSimplePostResult = await EndpointTests.AssertGetStatusCode<List<SimpleOrderDestinationData>>(client, $"api/order/{createdOrder.ID}/orderdestination/simplelist", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedSimplePostResult);

            await EndpointTests.AssertDeleteStatusCode(client, $"{destinationApiUrl}/{postResult.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertGetStatusCode(client, $"{destinationApiUrl}/{postResult.ID}", HttpStatusCode.NotFound);

            await EndpointTests.AssertDeleteStatusCode(client, $"/api/orderitem/{createdOrderItem.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestOrderCanDelete()
        {
            #region CREATE
            var testOrder = Utils.GetTestOrderData();
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.BadRequest);

            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            #endregion

            #region Can DELETE invalid ID
            await EndpointTests.AssertGetStatusCode<BooleanResponse>(this.client, $"{apiUrl}/-100/action/CanDelete", HttpStatusCode.NotFound);
            #endregion

            #region Can DELETE valid ID
            await EndpointTests.AssertGetStatusCode<BooleanResponse>(this.client, $"{apiUrl}/{createdOrder.ID}/action/CanDelete", HttpStatusCode.OK);
            #endregion

            #region DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }


        [TestMethod]
        public async Task TestOrderDeleteWithForceEndpoint()
        {
            var ctx = GetApiContext();
            //make sure user is support manager level
            var testUserLink = await Utils.GetAuthUserLink(ctx);
            if (testUserLink != null && testUserLink.UserAccessType < UserAccessType.SupportManager)
            {
                testUserLink.UserAccessType = UserAccessType.SupportManager;
                ctx.SaveChanges();
            }


            OrderData newOrder = Utils.GetTestOrderData();
            newOrder.CompanyID = ctx.CompanyData.FirstOrDefault(x => x.BID == TestConstants.BID).ID;
            newOrder.Items = new List<OrderItemData>()
            {
                Utils.GetTestOrderItemData(newOrder)
            };
            newOrder.ContactRoles = new List<OrderContactRole>()
            {
                Utils.GetOrderContactRole()
            };
            newOrder.ContactRoles.First().OrderContactRoleLocators = new List<OrderContactRoleLocator>(){
                new OrderContactRoleLocator()
                {
                    BID = TestConstants.BID,
                    RawInput = "test",
                    IsValid = true,
                    IsVerified = true,
                    HasImage = false,
                    SortIndex = 0,
                    LocatorType = (int)LocatorType.Email
                }
            };
            newOrder.EmployeeRoles = new List<OrderEmployeeRole>()
            {
                Utils.GetOrderEmployeeRole(await ctx.EmployeeData.FirstOrDefaultAsync(x => x.BID == 1))
            };
            newOrder.Dates = new List<OrderKeyDate>()
            {
                new OrderKeyDate()
                {
                    BID = TestConstants.BID,
                    KeyDateType = OrderKeyDateType.Approved,
                    KeyDT = DateTime.UtcNow
                }
            };

            //test: expect OK
            newOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}", JsonConvert.SerializeObject(newOrder), HttpStatusCode.OK);
            Assert.IsNotNull(newOrder);

            int orderItemID = newOrder.Items.FirstOrDefault().ID;
            int orderContactRoleID = newOrder.ContactRoles.First().ID;
            await ctx.OrderCustomData.FirstOrAddAsync((OrderCustomData x) => x.BID == TestConstants.BID && x.ID == newOrder.ID, () => new OrderCustomData()
            {
                BID = TestConstants.BID,
                ID = newOrder.ID,
                AppliesToClassTypeID = (int)ClassType.Order
            });

            int orderDestinationID = -newOrder.ID;
            await ctx.OrderDestinationData.FirstOrAddAsync((OrderDestinationData x) => x.BID == TestConstants.BID && x.OrderID == newOrder.ID, () => new OrderDestinationData()
            {
                BID = TestConstants.BID,
                OrderID = newOrder.ID,
                ID = -newOrder.ID,
                TransactionType = (byte)OrderTransactionType.Destination,
                Name = "test"
            });
            await ctx.OrderDestinationTagLink.FirstOrAddAsync((OrderDestinationTagLink x) => x.BID == TestConstants.BID && x.OrderDestinationID == orderDestinationID, () => new OrderDestinationTagLink()
            {
                BID = TestConstants.BID,
                OrderDestinationID = orderDestinationID,
                TagID = ctx.ListTag.FirstOrDefault(x => x.BID == TestConstants.BID).ID,
            });
            await ctx.OrderNote.FirstOrAddAsync((OrderNote x) => x.BID == TestConstants.BID && x.OrderID == newOrder.ID, () => new OrderNote()
            {
                ID = -newOrder.ID,
                BID = TestConstants.BID,
                OrderID = newOrder.ID,
                NoteType = OrderNoteType.Design,
                Note = "test"
            });
            await ctx.OrderTagLink.FirstOrAddAsync((OrderTagLink x) => x.BID == TestConstants.BID && x.OrderID == newOrder.ID, () => new OrderTagLink()
            {
                BID = TestConstants.BID,
                TagID = ctx.ListTag.FirstOrDefault(x => x.BID == TestConstants.BID).ID,
                OrderID = newOrder.ID
            });
            await ctx.OrderOrderLink.FirstOrAddAsync((OrderOrderLink l) => l.BID == TestConstants.BID && l.OrderID == newOrder.ID, () => new OrderOrderLink()
            {
                BID = TestConstants.BID,
                LinkType = OrderOrderLinkType.AlternateVariationOf,
                Description = "test",
                LinkedOrderID = ctx.OrderData.FirstOrDefault(x => x.BID == TestConstants.BID).ID,
                LinkedFormattedNumber = "test",
                OrderID = newOrder.ID
            });


            await ctx.SaveChangesAsync();


            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{newOrder.ID}?force=true", HttpStatusCode.OK);

            Assert.IsNull(await ctx.OrderData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ID == newOrder.ID));
            Assert.IsNull(await ctx.OrderCustomData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ID == newOrder.ID));

            Assert.IsNull(await ctx.OrderItemData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.OrderID == newOrder.ID));
            Assert.IsNull(await ctx.OrderItemTagLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.OrderItemID == orderItemID));
            Assert.IsNull(await ctx.OrderDestinationData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.OrderID == newOrder.ID));
            Assert.IsNull(await ctx.OrderDestinationTagLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.OrderDestinationID == orderDestinationID));
            Assert.IsNull(await ctx.OrderEmployeeRole.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.OrderID == newOrder.ID));
            Assert.IsNull(await ctx.OrderContactRole.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.OrderID == newOrder.ID));
            Assert.IsNull(await ctx.OrderContactRoleLocator.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ParentID == orderContactRoleID));
            Assert.IsNull(await ctx.OrderOrderLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.OrderID == newOrder.ID));
            Assert.IsNull(await ctx.OrderNote.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.OrderID == newOrder.ID));
            Assert.IsNull(await ctx.OrderKeyDate.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.OrderID == newOrder.ID));
            //order.version.log
            Assert.IsNull(await ctx.OrderTagLink.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.OrderID == newOrder.ID));
            //order.tax.item.assessment
        }
        [TestMethod]
        public async Task ClonedOrderLinkTest()
        {
            #region SETUP
            var ctx = GetApiContext();
            var testOrder = Utils.GetTestOrderData();
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.BadRequest);

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;
            #endregion

            #region CREATE
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrder);
            #endregion

            #region CLONE
            var clonedOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}/clone", null, HttpStatusCode.OK);
            Assert.IsNotNull(clonedOrder);
            #endregion

            #region GET Order Links
            var cloneToLink = ctx.OrderOrderLink.Where(l => l.BID == TestConstants.BID && l.LinkType == OrderOrderLinkType.ClonedTo && l.LinkedOrderID == clonedOrder.ID).FirstOrDefault();
            Assert.IsNotNull(cloneToLink);

            //Test ClonedFrom Order Link
            var cloneFromLink = ctx.OrderOrderLink.Where(l => l.BID == TestConstants.BID && l.LinkType == OrderOrderLinkType.ClonedFrom && l.LinkedOrderID == createdOrder.ID).FirstOrDefault();
            Assert.IsNotNull(cloneFromLink);
            #endregion

            #region CLEANUP
            ctx.OrderOrderLink.Remove(cloneToLink);
            ctx.OrderOrderLink.Remove(cloneFromLink);
            ctx.SaveChanges();

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{clonedOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task CloneEverything()
        {
            #region SETUP
            var ctx = GetApiContext();
            var testOrder = Utils.GetTestOrderData();
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.BadRequest);

            testOrder.CompanyID = _companyID;
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;
            testOrder.Notes = new List<OrderNote>()
            {
                new OrderNote()
                {
                    BID = TestConstants.BID,
                    ModifiedDT = DateTime.UtcNow,
                    NoteType = OrderNoteType.Sales,
                    Note = "Test Note",
                    IsActive = true,
                },
                new OrderNote()
                {
                    BID = TestConstants.BID,
                    ModifiedDT = DateTime.UtcNow,
                    NoteType = OrderNoteType.Sales,
                    Note = "Another Test Note",
                    IsActive = true,
                }
            };
            testOrder.ContactRoles = new List<OrderContactRole>()
            {
                new OrderContactRole()
                {
                    BID = TestConstants.BID,
                    ContactName = "Test Contact 1",
                    RoleType = OrderContactRoleType.Primary,
                },
                new OrderContactRole()
                {
                    BID = TestConstants.BID,
                    ContactName = "Test Contact 2",
                    RoleType = OrderContactRoleType.Billing,
                },
            };
            testOrder.EmployeeRoles = new List<OrderEmployeeRole>()
            {
                Utils.GetOrderEmployeeRole(await ctx.EmployeeData.FirstOrDefaultAsync(x => x.BID == 1))
            };
            var testOrderItem = Utils.GetTestOrderItemData(testOrder);
            testOrderItem.Notes = new List<OrderNote>()
            {
                new OrderNote()
                {
                    BID = TestConstants.BID,
                    ModifiedDT = DateTime.UtcNow,
                    NoteType = OrderNoteType.Sales,
                    Note = "Test Note",
                    IsActive = true,
                },
                new OrderNote()
                {
                    BID = TestConstants.BID,
                    ModifiedDT = DateTime.UtcNow,
                    NoteType = OrderNoteType.Sales,
                    Note = "Another Test Note",
                    IsActive = true,
                }
            };
            testOrder.Items = new List<OrderItemData>()
            {
                testOrderItem
            };
            #endregion

            #region CREATE
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrder);
            #endregion

            #region SETUP CustomData
            var customData = new OrderCustomData()
            {
                BID = TestConstants.BID,
                ID = createdOrder.ID,
                AppliesToClassTypeID = (int)ClassType.Order
            };
            ctx.OrderCustomData.Add(customData);
            Assert.IsTrue(ctx.SaveChanges() > 0);
            #endregion

            #region CLONE
            var clonedOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}/clone", null, HttpStatusCode.OK);
            Assert.IsNotNull(clonedOrder);
            // Test cloned contact roles
            Assert.AreEqual(testOrder.ContactRoles.Count, clonedOrder.ContactRoles.Count);
            foreach (var role in testOrder.ContactRoles)
            {
                var clonedRole = clonedOrder.ContactRoles.FirstOrDefault(t => t.ContactName == role.ContactName);
                Assert.IsNotNull(clonedRole);
                Assert.AreEqual(clonedRole.OrderID, clonedOrder.ID);
            }
            // Test cloned employee roles
            Assert.AreEqual(testOrder.EmployeeRoles.Count, clonedOrder.EmployeeRoles.Count);
            foreach (var role in testOrder.EmployeeRoles)
            {
                var clonedRole = clonedOrder.EmployeeRoles.FirstOrDefault(t => t.EmployeeID == role.EmployeeID);
                Assert.IsNotNull(clonedRole);
                Assert.AreEqual(clonedRole.OrderID, clonedOrder.ID);
            }
            // Test cloned notes
            foreach (var note in testOrder.Notes)
            {
                var clonedNote = clonedOrder.Notes.FirstOrDefault(t => t.Note == note.Note);
                Assert.IsNotNull(clonedNote);
                Assert.AreEqual(clonedNote.OrderID, clonedOrder.ID);
            }
            // Test orderItems
            Assert.AreEqual(testOrder.Items.Count, clonedOrder.Items.Count);
            foreach (var item in testOrder.Items)
            {
                var clonedItem = clonedOrder.Items.FirstOrDefault(t => t.Name == item.Name);
                Assert.IsNotNull(clonedItem);
                Assert.AreEqual(clonedItem.OrderID, clonedOrder.ID);
                // Test orderItem Notes
                var notes = testOrder.Items.FirstOrDefault().Notes;
                Assert.IsNotNull(clonedItem.Notes);
                Assert.AreEqual(notes.Count, clonedItem.Notes.Count);
                foreach (var note in notes)
                {
                    var clonedNote = clonedItem.Notes.FirstOrDefault(t => t.Note == note.Note);
                    Assert.IsNotNull(clonedNote);
                    Assert.AreEqual(clonedNote.OrderItemID, clonedItem.ID);
                }
            }
            // Test customData
            var clonedCustomData = await ctx.OrderCustomData.FirstOrDefaultAsync(x => x.BID == TestConstants.BID && x.ID == clonedOrder.ID);
            Assert.IsNotNull(clonedCustomData);
            // Test Production Due Date
            var retrievedClone = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{clonedOrder.ID}?ItemLevel=Full", HttpStatusCode.OK);
            var keyDate = retrievedClone.Dates.Where(t => t.KeyDateType == OrderKeyDateType.ProductionDue).FirstOrDefault();
            Assert.IsNotNull(keyDate);
            Assert.IsTrue(keyDate.KeyDT > DateTime.UtcNow);
            //Why this KeyDT is not getting UTC time correctly ?
            //Assert.IsTrue(keyDate.KeyDT <= DateTime.UtcNow.AddDays(1));
            foreach (var item in retrievedClone.Items)
            {
                keyDate = item.Dates.Where(t => t.KeyDateType == OrderKeyDateType.ProductionDue).FirstOrDefault();
                Assert.IsNotNull(keyDate);
                Assert.IsTrue(keyDate.KeyDT > DateTime.UtcNow);
            }
            #endregion

            #region GET Order Links
            var cloneToLink = ctx.OrderOrderLink.Where(l => l.BID == TestConstants.BID && l.LinkType == OrderOrderLinkType.ClonedTo && l.LinkedOrderID == clonedOrder.ID).FirstOrDefault();
            Assert.IsNotNull(cloneToLink);

            //Test ClonedFrom Order Link
            var cloneFromLink = ctx.OrderOrderLink.Where(l => l.BID == TestConstants.BID && l.LinkType == OrderOrderLinkType.ClonedFrom && l.LinkedOrderID == createdOrder.ID).FirstOrDefault();
            Assert.IsNotNull(cloneFromLink);
            #endregion

            #region CLEANUP
            ctx.OrderOrderLink.Remove(cloneToLink);
            ctx.OrderOrderLink.Remove(cloneFromLink);
            ctx.SaveChanges();

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}?force=true", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{clonedOrder.ID}?force=true", HttpStatusCode.OK);
            #endregion
        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task CloneWitOption()
        {
            var cloneOption = new OrderCloneOption()
            {
                DueDate = DateTime.Now,
                KeepFiles = true,
                KeepNotes = true,
                UpdatePricing = true
            };
            await TestCloneWithOption(cloneOption);
        }

        [TestMethod]
        [DeploymentItem(TestTextFilePath)]
        [TestCategory("GL")]
        public async Task TestCloneItemsWithCopyFiles()
        {
            var resp = await EndpointTests.AssertGetStatusCode<GetOptionValueResponse>(client, $"/api/options/6058?bid=1", HttpStatusCode.OK);
            await TestCloneWithOrderItem(null, resp.ValueAsBoolean());
            await TestCloneWithOrderItem(true, true);
            await TestCloneWithOrderItem(false, true);
        }

        private async Task TestCloneWithOrderItem(bool? CopyFiles, bool ExpectFilesToBeCopied)
        {
            #region SETUP
            var ctx = GetApiContext();
            var testOrder = Utils.GetTestOrderData();
            testOrder.CompanyID = await Utils.GetCompanyID(client);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var orderItemWithFile = Utils.GetTestOrderItemData(testOrder);

            testOrder.Items = new List<OrderItemData>()
            {
                orderItemWithFile
            };

            

            #endregion

            #region CREATE
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrder);

            var dmRequestModel = new DMController.RequestModel()
            {
                id = createdOrder.ID,
                ctid = Convert.ToInt32(ClassType.Order),
                Bucket = Web.BucketRequest.Documents
            };
            //Add file for order
            var dmController = GetDMController();
            AddFileToForm(dmController, TestTextFileName);
            var create = dmRequestModel.Clone();
            create.PathAndFilename = TestTextFileName;
            var okPost = await dmController.Post(create, null, null);
            Assert.IsTrue(okPost is OkObjectResult);
            dmController.Request.Form = null;
            #endregion

            #region CLONE
            string parameter = "";
            if (CopyFiles.HasValue) {
                string val = CopyFiles.Value ? "true" : "false";
                parameter = $"?CopyFile={CopyFiles}";
            }



            var clonedOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}/clone"+parameter, null, HttpStatusCode.OK);
            Assert.IsNotNull(clonedOrder);
            #endregion

            #region READ
            var expectedFiles = 0;
            if (ExpectFilesToBeCopied) expectedFiles = 1;
            var forCloneDMRequestModel = new DMController.RequestModel()
            {
                id = clonedOrder.ID,
                ctid = Convert.ToInt32(ClassType.Order),
                Bucket = Web.BucketRequest.Documents
            };
            var okGet = await dmController.GetDocuments(forCloneDMRequestModel);

            Assert.IsTrue(okGet is OkObjectResult);
            var items = ((List<DMItem>)((OkObjectResult)okGet).Value);
            Assert.AreEqual(expectedFiles, items.Where(x => x.Name == TestTextFileName).Count());

            //Read OrderItem
            var forCloneItemDMRequestModel = new DMController.RequestModel()
            {
                id = clonedOrder.Items.First().ID,
                ctid = Convert.ToInt32(ClassType.OrderItem),
                Bucket = Web.BucketRequest.Documents
            };
            okGet = await dmController.GetDocuments(forCloneDMRequestModel);
            Assert.IsTrue(okGet is OkObjectResult);
            items = ((List<DMItem>)((OkObjectResult)okGet).Value);
            Assert.AreEqual(expectedFiles, items.Where(x => x.Name == TestTextFileName).Count());

            #endregion

            #region CLEANUP

            dmController.Request.Form = null;
            var delete = dmRequestModel.Clone();
            delete.PathAndFilename = TestTextFileName;
            var okDelete = await dmController.Delete(delete);
            Assert.IsTrue(okDelete is OkObjectResult);

            dmController.Request.Form = null;
            var deleteClone = forCloneDMRequestModel.Clone();
            delete.PathAndFilename = TestTextFileName;
            var okDeleteClone = await dmController.Delete(delete);
            Assert.IsTrue(okDelete is OkObjectResult);
            ctx.OrderOrderLink.RemoveRange(ctx.OrderOrderLink.Where(x => x.OrderID == createdOrder.ID||x.OrderID==clonedOrder.ID));
            await ctx.SaveChangesAsync();
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{clonedOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        private async Task TestCloneWithOption(OrderCloneOption cloneOption)
        {
            #region SETUP
            var ctx = GetApiContext();
            var testOrder = Utils.GetTestOrderData();
            testOrder.CompanyID = await Utils.GetCompanyID(client);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            var orderItemWithFile = Utils.GetTestOrderItemData(testOrder);
            orderItemWithFile.ItemStatusID = 21;

            testOrder.Items = new List<OrderItemData>()
            {
                orderItemWithFile
            };

            testOrder.Notes = new List<OrderNote>()
            {
                Utils.GetTestOrderNoteData(testOrder)
            };

            var expectedNotes = 0;
            if (cloneOption.KeepNotes != null && cloneOption.KeepNotes == true)
            {
                expectedNotes = 1;
            }

            #endregion

            #region CREATE
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            Assert.IsNotNull(createdOrder);

            var dmRequestModel = new DMController.RequestModel()
            {
                id = createdOrder.ID,
                ctid = Convert.ToInt32(ClassType.Order),
                Bucket = Web.BucketRequest.Documents
            };
            //Add file for order
            var dmController = GetDMController();
            AddFileToForm(dmController, TestTextFileName);
            var create = dmRequestModel.Clone();
            create.PathAndFilename = TestTextFileName;
            var okPost = await dmController.Post(create, null, null);
            Assert.IsTrue(okPost is OkObjectResult);
            dmController.Request.Form = null;
            #endregion

            #region CLONE
            string parameter = "";


            var clonedOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}/cloneWithOption" + parameter, cloneOption, HttpStatusCode.OK);
            Assert.IsNotNull(clonedOrder);
            Assert.AreEqual(expectedNotes, clonedOrder.Notes.Count);


            #endregion

            #region READ
            var expectedFiles = 0;
            if (cloneOption.KeepFiles != null && cloneOption.KeepFiles == true)
            {
                expectedFiles = 1;
            }
            
            var forCloneDMRequestModel = new DMController.RequestModel()
            {
                id = clonedOrder.ID,
                ctid = Convert.ToInt32(ClassType.Order),
                Bucket = Web.BucketRequest.Documents
            };
            var okGet = await dmController.GetDocuments(forCloneDMRequestModel);

            Assert.IsTrue(okGet is OkObjectResult);
            var items = ((List<DMItem>)((OkObjectResult)okGet).Value);
            Assert.AreEqual(expectedFiles, items.Where(x => x.Name == TestTextFileName).Count());

            //Read OrderItem
            var forCloneItemDMRequestModel = new DMController.RequestModel()
            {
                id = clonedOrder.Items.First().ID,
                ctid = Convert.ToInt32(ClassType.OrderItem),
                Bucket = Web.BucketRequest.Documents
            };
            okGet = await dmController.GetDocuments(forCloneDMRequestModel);
            Assert.IsTrue(okGet is OkObjectResult);
            items = ((List<DMItem>)((OkObjectResult)okGet).Value);
            Assert.AreEqual(expectedFiles, items.Where(x => x.Name == TestTextFileName).Count());

            #endregion

            #region CLEANUP

            dmController.Request.Form = null;
            var delete = dmRequestModel.Clone();
            delete.PathAndFilename = TestTextFileName;
            var okDelete = await dmController.Delete(delete);
            Assert.IsTrue(okDelete is OkObjectResult);

            dmController.Request.Form = null;
            var deleteClone = forCloneDMRequestModel.Clone();
            delete.PathAndFilename = TestTextFileName;
            var okDeleteClone = await dmController.Delete(delete);
            Assert.IsTrue(okDelete is OkObjectResult);
            ctx.OrderOrderLink.RemoveRange(ctx.OrderOrderLink.Where(x => x.OrderID == createdOrder.ID || x.OrderID == clonedOrder.ID));
            await ctx.SaveChangesAsync();
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{clonedOrder.ID}", HttpStatusCode.NoContent);
            #endregion
        }

        [TestMethod]
        public async Task OrderLineItemStatusChangeOnOrderStatusChangeTest()
        {
            OrderData testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Order;
            testOrder.OrderStatusID = OrderOrderStatus.OrderPreWIP;
            testOrder.ClassTypeID = (int)ClassType.Order;

            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            OrderItemData testOrderItem = new OrderItemData()
            {
                BID = testOrder.BID,
                HasCustomImage = false,
                HasDocuments = false,
                HasProof = false,
                ID = 1,
                OrderID = 0,
                ItemNumber = 1,
                Quantity = 1,
                Name = "test",
                IsOutsourced = false,
                OrderStatusID = testOrder.OrderStatusID,
                ItemStatusID = 21, //21=Pre-WIP
                ProductionLocationID = testOrder.ProductionLocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = testOrder.TaxGroupID,
                TaxGroupOV = false,
                IsTaxExempt = false,
                TransactionType = testOrder.TransactionType
            };

            testOrder.Items = new List<OrderItemData>();
            testOrder.Items.Add(testOrderItem);

            OrderData createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            string orderStatusEndpoint = $"{apiUrl}/{createdOrder.ID}/action/changeorderstatus/{Convert.ToInt32(OrderOrderStatus.OrderWIP)}";
            await EndpointTests.AssertPostStatusCode(this.client, orderStatusEndpoint, HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"{apiUrl}/{createdOrder.ID}?ItemLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrder);

            foreach (OrderItemData lineItemData in retrievedOrder.Items)
            {
                Assert.AreEqual(retrievedOrder.OrderStatusID, lineItemData.OrderStatusID);
            }


            //CLEAN UP...
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task CloneOrderToCreditMemoTest()
        {
            var ctx = GetApiContext();
            var testController = GetDMController();

            OrderData testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Order;
            testOrder.OrderStatusID = OrderOrderStatus.OrderWIP;
            testOrder.ClassTypeID = (int)ClassType.Order;

            testOrder.CompanyID = _companyID;
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            int contactId = await Utils.GetContactID(client, testOrder.CompanyID);
            int employeeId = await Utils.GetEmployeeID(client);
            var employeeRoleEnteredById = ctx.EmployeeRole.Where(er => er.Name == "Entered By").FirstOrDefault().ID;
            var employeeRoleSalesPersonId = ctx.EmployeeRole.Where(er => er.Name == "Salesperson").FirstOrDefault().ID;

            OrderItemData testOrderItem = new OrderItemData()
            {
                BID = testOrder.BID,
                HasCustomImage = false,
                HasDocuments = false,
                HasProof = false,
                ID = 1,
                OrderID = 0,
                ItemNumber = 1,
                Quantity = 1,
                Name = "test",
                IsOutsourced = false,
                OrderStatusID = testOrder.OrderStatusID,
                ItemStatusID = 22,
                ProductionLocationID = testOrder.ProductionLocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = testOrder.TaxGroupID,
                TaxGroupOV = false,
                IsTaxExempt = false,
                TransactionType = testOrder.TransactionType
            };

            testOrder.Items = new List<OrderItemData>();
            testOrder.Items.Add(testOrderItem);

            OrderData createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkcontact/primary/{contactId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkcontact/billing/{contactId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkEmployee/{employeeRoleEnteredById}/{employeeId}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{createdOrder.ID}/action/linkEmployee/{employeeRoleSalesPersonId}/{employeeId}", HttpStatusCode.OK);

            var orderResult2 = await testController.Post(new DMController.RequestModel()
            {
                id = createdOrder.ID,
                ctid = Convert.ToInt32(ClassType.Order),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            }, null, "https://corebridge.net/wp-content/uploads/2019/01/AdobeStock_202169599-e1551199093506.jpg");

            AssertX.IsType<OkObjectResult>(orderResult2);
            AssertX.IsType<DMItem>(((OkObjectResult)orderResult2).Value);

            var orderLineItemResult = await testController.Post(new DMController.RequestModel()
            {
                id = createdOrder.Items.FirstOrDefault().ID,
                ctid = Convert.ToInt32(ClassType.OrderItem),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            }, null, "https://corebridge.net/wp-content/uploads/2019/01/AdobeStock_202169599-e1551199093506.jpg");

            AssertX.IsType<OkObjectResult>(orderLineItemResult);
            AssertX.IsType<DMItem>(((OkObjectResult)orderLineItemResult).Value);

            EstimateData convertedEstimate = await EndpointTests.AssertPostStatusCode<EstimateData>(client, $"{apiUrl}/{createdOrder.ID}/clone?SaveAsCreditMemo=true&CopyFiles=true", JsonConvert.SerializeObject(createdOrder), HttpStatusCode.OK);

            Assert.IsNotNull(convertedEstimate);
            Assert.IsTrue((convertedEstimate.ID != 0));

            var estimateResult = await testController.GetDocuments(new DMController.RequestModel()
            {
                id = convertedEstimate.ID,
                ctid = Convert.ToInt32(ClassType.Estimate),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            });
            AssertX.IsType<OkObjectResult>(estimateResult);
            AssertX.IsType<List<DMItem>>(((OkObjectResult)estimateResult).Value);
            Assert.IsTrue(((List<DMItem>)((OkObjectResult)estimateResult).Value).Count > 0);

            var retrievedOrderWithContact = await EndpointTests.AssertGetStatusCode<CreditMemoData>(client, $"/api/creditmemo/{convertedEstimate.ID}?ContactLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderWithContact?.ContactRoles);
            var contactRows = retrievedOrderWithContact.ContactRoles.Where(t => t.OrderID == convertedEstimate.ID);
            Assert.IsNotNull(contactRows);
            Assert.AreEqual(2, contactRows.Count());
            Assert.AreEqual(OrderContactRoleType.Primary, contactRows.ToList()[0].RoleType);
            Assert.AreEqual(OrderContactRoleType.Billing, contactRows.ToList()[1].RoleType);
            Assert.AreEqual(contactId, contactRows.ToList()[0].ContactID);
            Assert.AreEqual(contactId, contactRows.ToList()[1].ContactID);

            var retrievedEstimateWithEmployee = await EndpointTests.AssertGetStatusCode<CreditMemoData>(client, $"/api/creditmemo/{convertedEstimate.ID}?EmployeeLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedEstimateWithEmployee?.EmployeeRoles);
            var employeeRows = retrievedEstimateWithEmployee.EmployeeRoles;
            Assert.IsNotNull(employeeRows);
            Assert.AreEqual(2, employeeRows.Count());
            Assert.IsNotNull(employeeRows.FirstOrDefault(t => t.RoleID == employeeRoleEnteredById));
            Assert.IsNotNull(employeeRows.FirstOrDefault(t => t.RoleID == employeeRoleSalesPersonId));

            var retrievedEstimateWithOrderItem = await EndpointTests.AssertGetStatusCode<CreditMemoData>(client, $"/api/creditmemo/{convertedEstimate.ID}?ItemLevel=Full", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedEstimateWithOrderItem.Items);

            var orderItemResult = await testController.GetDocuments(new DMController.RequestModel()
            {
                id = retrievedEstimateWithOrderItem.Items.FirstOrDefault().ID,
                ctid = Convert.ToInt32(ClassType.OrderItem),
                Bucket = Web.BucketRequest.Documents,
                PathAndFilename = "test.jpg"
            });
            AssertX.IsType<OkObjectResult>(orderItemResult);
            AssertX.IsType<List<DMItem>>(((OkObjectResult)orderItemResult).Value);
            Assert.IsTrue(((List<DMItem>)((OkObjectResult)orderItemResult).Value).Count > 0);

            //CLEAN UP...
            var createdEstimateLink = ctx.OrderOrderLink.Where(l => l.BID == TestConstants.BID && l.LinkedOrderID == createdOrder.ID).FirstOrDefault();
            Assert.IsNotNull(createdEstimateLink);
            var convertedOrderLink = ctx.OrderOrderLink.Where(l => l.BID == TestConstants.BID && l.LinkedOrderID == convertedEstimate.ID).FirstOrDefault();
            Assert.IsNotNull(convertedOrderLink);
            ctx.OrderOrderLink.Remove(createdEstimateLink);
            ctx.OrderOrderLink.Remove(convertedOrderLink);
            ctx.SaveChanges();

            var deleteEstimateResult = testController.DeleteAll(Convert.ToInt32(ClassType.Estimate), convertedEstimate.ID);
            var deleteEstimateLineItemResult = testController.DeleteAll(Convert.ToInt32(ClassType.OrderItem), createdOrder.Items.FirstOrDefault().ID);
            var deleteOrderResult = testController.DeleteAll(Convert.ToInt32(ClassType.Order), createdOrder.ID);
            var deleteOrderLineItemResult = testController.DeleteAll(Convert.ToInt32(ClassType.OrderItem), retrievedEstimateWithOrderItem.Items.FirstOrDefault().ID);

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrder.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/creditmemo/{convertedEstimate.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestGetcurrentOrderNumberSucceed()
        {
            OrderData testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Order;
            testOrder.OrderStatusID = OrderOrderStatus.OrderWIP;
            testOrder.ClassTypeID = (int)ClassType.Order;

            testOrder.CompanyID = _companyID;
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            OrderData createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            await EndpointTests.AssertGetResponseContent(client, $"{apiUrl}/GetCurrentNumber?TransactionType=Order", createdOrder.Number.ToString(), HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestNextOrderNumberSucceed()
        {
            var resultNextNumber = await EndpointTests.AssertGetStatusContent(client, $"{apiUrl}/GetNextNumber?TransactionType=Order", HttpStatusCode.OK);

            OrderData testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Order;
            testOrder.OrderStatusID = OrderOrderStatus.OrderWIP;
            testOrder.ClassTypeID = (int)ClassType.Order;

            testOrder.CompanyID = _companyID;
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            OrderData createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            Assert.IsTrue(createdOrder.Number.ToString().Equals(resultNextNumber));
        }

        [TestMethod]
        public async Task TestSetNextOrderNumber()
        {
            var currentNumber = await EndpointTests.AssertGetStatusContent(client, $"{apiUrl}/GetCurrentNumber?TransactionType=Order", HttpStatusCode.OK);

            var newNumber = int.Parse(currentNumber) + 1000;

            var resultSetNextNumber = await EndpointTests.AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/SetNextNumber/{newNumber}?TransactionType=Order", HttpStatusCode.OK);

            Assert.IsTrue(resultSetNextNumber.Success);

            var nextNumber = await EndpointTests.AssertGetStatusContent(client, $"{apiUrl}/GetNextNumber?TransactionType=Order", HttpStatusCode.OK);

            Assert.IsTrue(newNumber == int.Parse(nextNumber));

            OrderData testOrder = Utils.GetTestOrderData();
            testOrder.TransactionType = (byte)OrderTransactionType.Order;
            testOrder.OrderStatusID = OrderOrderStatus.OrderWIP;
            testOrder.ClassTypeID = (int)ClassType.Order;

            testOrder.CompanyID = _companyID;
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            OrderData createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            Assert.IsTrue(createdOrder.Number.ToString().Equals(nextNumber));

            resultSetNextNumber = await EndpointTests.AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/SetNextNumber/{newNumber-1000}?TransactionType=Order", HttpStatusCode.BadRequest);

            Assert.IsTrue(resultSetNextNumber.Success == false);

            currentNumber = await EndpointTests.AssertGetStatusContent(client, $"{apiUrl}/GetCurrentNumber?TransactionType=Order", HttpStatusCode.OK);

            newNumber = int.Parse(currentNumber) + 1000;

            resultSetNextNumber = await EndpointTests.AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/SetNextNumber/{newNumber}?TransactionType=Order", HttpStatusCode.OK);

            Assert.IsTrue(resultSetNextNumber.Success);

            nextNumber = await EndpointTests.AssertGetStatusContent(client, $"{apiUrl}/GetNextNumber?TransactionType=Order", HttpStatusCode.OK);

            Assert.IsTrue(newNumber == int.Parse(nextNumber));

            resultSetNextNumber = await EndpointTests.AssertPostStatusCode<BooleanResponse>(client, $"{apiUrl}/SetNextNumber/{newNumber - 500}?TransactionType=Order", HttpStatusCode.OK);

            Assert.IsTrue(resultSetNextNumber.Success);

            nextNumber = await EndpointTests.AssertGetStatusContent(client, $"{apiUrl}/GetNextNumber?TransactionType=Order", HttpStatusCode.OK);

            Assert.IsTrue(newNumber - 500 == int.Parse(nextNumber));

        }
    }
}
