﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Request;
using PaymentApplication = Endor.Models.PaymentApplication;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Classes.Responses;

namespace Endor.Api.Tests
{
    [TestClass]
    [TestCategory("Payment")]
    public class PaymentUseCaseTests : PaymentTestHelper
    {
        private const string creditAdjustmentApiUrl = "/api/payment/creditadjustment";
        private const string transferCreditApiUrl = "/api/payment/transfertocredit";
        private const string applyCreditApiUrl = "/api/payment/applycredit";
        private const string refundApiUrl = "/api/payment/refund";
        private const string paymentApiUrl = "/api/payment/payment";
        private const string creditGiftApiUrl = "/api/payment/creditgift";
        private System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestInitialize]
        public async Task Initialize()
        {
            await CleanUpTestData();
        }

        [TestCleanup]
        public async Task Teardown()
        {
            await CleanUpTestData();
        }
        private PaymentRequest GetPaymentRequest(int companyID, int orderID, decimal amount)
        {
            var ctx = GetApiContext();
            var company = ctx.CompanyData.First(t => t.ID == companyID);
            return new PaymentRequest
            {
                LocationID = company.LocationID,
                CompanyID = companyID,
                Amount = amount,
                PaymentTransactionType = (int)PaymentTransactionType.Payment,
                ReceivedLocationID = company.LocationID,
                PaymentMethodID = 1,
                Applications = new List<PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest()
                    {
                        OrderID = orderID,
                        Amount = amount,
                    }
                }
            };
        }

        private PaymentTransferCreditRequest GetTransferCreditRequest(int companyID, int? masterID, int orderID, bool isRefundable, decimal amount)
        {
            return new PaymentTransferCreditRequest()
            {
                PaymentTransactionType = isRefundable ? PaymentTransactionType.Refund_to_Refundable_Credit : PaymentTransactionType.Refund_to_Nonrefundable_Credit,
                MasterPaymentID = masterID,
                CompanyID = companyID,
                PaymentMethodId = isRefundable ? (int)PaymentMethodType.RefundableCredit : (int)PaymentMethodType.NonRefundableCredit,
                Applications = new List<PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest()
                    {
                        OrderID = orderID,
                        Amount = -amount,
                    },
                    new PaymentApplicationRequest()
                    {
                        OrderID = null,
                        Amount = amount,
                    },
                }
            };
        }


        private PaymentApplyCreditRequest GetApplyCreditRequest(int companyID, int? masterID, int orderID, bool isRefundable, decimal amount)
        {
            return new PaymentApplyCreditRequest()
            {
                PaymentTransactionType = isRefundable ? PaymentTransactionType.Payment_from_Refundable_Credit : PaymentTransactionType.Payment_from_Nonrefundable_Credit,
                MasterPaymentID = masterID,
                PaymentMethodId = 250,
                CompanyID = companyID,
                Applications = new List<PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest()
                    {
                        OrderID = orderID,
                        Amount = amount,
                    },
                    new PaymentApplicationRequest()
                    {
                        OrderID = null,
                        Amount = -amount,
                    },
                },
            };
        }

        private PaymentRefundRequest GetRefundRequest(int companyID, int? masterID, int? orderID, decimal amount)
        {
            return new PaymentRefundRequest()
            {
                PaymentTransactionType = PaymentTransactionType.Refund,
                MasterPaymentID = masterID,
                CompanyID = companyID,
                Amount = -amount,
                Applications = new List<PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest()
                    {
                        OrderID = orderID,
                        Amount = -amount,
                    }
                },
            };
        }

        private PaymentCreditAdjustmentRequest GetCreditAdjustmentRequest(int companyID, int? masterID, int? orderID, decimal amount)
        {
            return new PaymentCreditAdjustmentRequest()
            {
                PaymentTransactionType = PaymentTransactionType.Credit_Adjustment,
                PaymentMethodId = (int)PaymentMethodType.NonRefundableCredit,
                MasterPaymentID = masterID,
                CompanyID = companyID,
                Amount = -amount,
                Applications = new List<Web.Classes.Request.PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest()
                    {
                        OrderID = orderID,
                        Amount = -amount
                    }
                }
            };
        }

        private CreditGiftRequest GetCreditGiftRequest(int companyID, int? orderID, decimal amount)
        {
            var ctx = GetApiContext();
            var company = ctx.CompanyData.First(t => t.ID == companyID);
            return new CreditGiftRequest()
            {
                LocationID = company.LocationID,
                CompanyID = company.ID,
                Amount = amount,
                PaymentTransactionType = PaymentTransactionType.Credit_Gift,
                ReceivedLocationID = company.LocationID,
                PaymentMethodId = 251,
                Applications = new List<PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest
                    {
                        OrderID = orderID,
                        Amount = amount
                    }
                }
            };
        }

        /**
         * The sequence:
         * Create payment on order -> Refund payment to ISC -> Apply payment to order again -> Refund back to customer
         */
        [TestMethod]
        public async Task TestCase1()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID,
                price: 100m
            );
            var paymentRequest = GetPaymentRequest(
                companyID: company.ID,
                orderID: order.ID,
                amount: 60
            );
            var paymentResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{paymentApiUrl}", JsonConvert.SerializeObject(paymentRequest), HttpStatusCode.OK);
            Assert.IsNotNull(paymentResult);
            var transferCreditRequest = GetTransferCreditRequest(
                companyID: company.ID,
                masterID: null,
                orderID: order.ID,
                amount: 60,
                isRefundable: true
            );
            var transferCreditResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(transferCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(transferCreditResult);
            var applyCreditRequest = GetApplyCreditRequest(
                companyID: company.ID,
                masterID: null,
                orderID: order.ID,
                amount: 60,
                isRefundable: true
            );
            var applyCreditResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(applyCreditResult);
            var refundRequest = GetRefundRequest(
                companyID: company.ID,
                masterID: null,
                orderID: order.ID,
                amount: 60
            );
            var refundResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{refundApiUrl}", JsonConvert.SerializeObject(refundRequest), HttpStatusCode.OK);
            Assert.IsNotNull(refundResult);
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            Assert.AreEqual(0, company.RefundableCredit);
        }


        /**
         * The sequence:
         * Create credit gift of order -> Refund to ISC -> Credit Adjustment
         */
        [TestMethod]
        public async Task TestCase2()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID,
                price: 100m
            );
            var paymentRequest = GetCreditGiftRequest(
                companyID: company.ID,
                orderID: order.ID,
                amount: 60
            );
            var paymentResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{creditGiftApiUrl}", JsonConvert.SerializeObject(paymentRequest), HttpStatusCode.OK);
            Assert.IsNotNull(paymentResult);
            var transferCreditRequest = GetTransferCreditRequest(
                companyID: company.ID,
                masterID: null,
                orderID: order.ID,
                amount: 60,
                isRefundable: false
            );
            var transferCreditResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(transferCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(transferCreditResult);
            var creditAdjustmentRequest = GetCreditAdjustmentRequest(
                companyID: company.ID,
                masterID: null,
                orderID: null,
                amount: 60
            );
            var creditAdjustmentResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{creditAdjustmentApiUrl}", JsonConvert.SerializeObject(creditAdjustmentRequest), HttpStatusCode.OK);
            Assert.IsNotNull(creditAdjustmentResult);
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            Assert.AreEqual(0, company.NonRefundableCredit);
        }



        /**
         * The sequence:
         * Create credit gift of ISC -> Apply to order -> Refund to ISC - Credit Adjustment 
         */
        [TestMethod]
        public async Task TestCase3()
        {
            var ctx = GetApiContext();
            var company = await CreateCompany();
            var order = await CreateOrder(
                companyID: company.ID,
                price: 100m
            );
            var paymentRequest = GetCreditGiftRequest(
                companyID: company.ID,
                orderID: null,
                amount: 60
            );
            var paymentResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{paymentApiUrl}", JsonConvert.SerializeObject(paymentRequest), HttpStatusCode.OK);
            Assert.IsNotNull(paymentResult);
            var applyCreditRequest = GetApplyCreditRequest(
                companyID: company.ID,
                masterID: null,
                orderID: order.ID,
                amount: 60,
                isRefundable: true
            );
            var applyCreditResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{applyCreditApiUrl}", JsonConvert.SerializeObject(applyCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(applyCreditResult);
            var transferCreditRequest = GetTransferCreditRequest(
                companyID: company.ID,
                masterID: null,
                orderID: order.ID,
                amount: 60,
                isRefundable: false
            );
            var transferCreditResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{transferCreditApiUrl}", JsonConvert.SerializeObject(transferCreditRequest), HttpStatusCode.OK);
            Assert.IsNotNull(transferCreditResult);
            var creditAdjustmentRequest = GetCreditAdjustmentRequest(
                companyID: company.ID,
                masterID: null,
                orderID: null,
                amount: 60
            );
            var creditAdjustmentResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{creditAdjustmentApiUrl}", JsonConvert.SerializeObject(creditAdjustmentRequest), HttpStatusCode.OK);
            Assert.IsNotNull(creditAdjustmentResult);
            company = ctx.CompanyData.First(t => t.ID == company.ID);
            Assert.AreEqual(0, company.NonRefundableCredit);
        }
    }
}
