﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    [TestClass]
    public class RightsGroupListEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/rightsgrouplist";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestDataName;
        public TestContext TestContext { get; set; }
        

        [TestInitialize]
        public void Initialize()
        {
            this.NewTestDataName = DateTimeOffset.Now.ToUnixTimeSeconds() + " TEST RightsGroupList";
        }

        [TestMethod]
        public async Task TestRightsGroupListCRUDEndpoints()
        {
            var testObject = new RightsGroupList()
            {
                BID = 1,
                ClassTypeID = (int)ClassType.RightsGroupList,
                Name = this.NewTestDataName,
                RightsArray = "TestArray",
            };

            //test: CREATE expect OK
            testObject = await EndpointTests.AssertPostStatusCode<RightsGroupList>(client, $"{apiUrl}", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            testObject.RightsArray = "Test update";

            //test: UPDATE expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + testObject.ID, JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            //test: READ expect OK
            var updatedTestObject = await EndpointTests.AssertGetStatusCode<RightsGroupList>(client, $"{apiUrl}/" + testObject.ID, HttpStatusCode.OK);
            Assert.AreEqual(updatedTestObject.RightsArray, testObject.RightsArray);

            //test: DELETE expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testObject.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestRightsGroupListActionEndpoints()
        {
            var ctx = GetApiContext();
            var testObject = new RightsGroupList()
            {
                BID = 1,
                ClassTypeID = (int)ClassType.RightsGroupList,
                Name = this.NewTestDataName,
                RightsArray = "TestArray",
            };

            //test: expect OK
            testObject = await EndpointTests.AssertPostStatusCode<RightsGroupList>(client, $"{apiUrl}", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);
            int rightsGroupID = 1000;

            //POST link RightGroups
            EntityActionChangeResponse linkReponse;
            linkReponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{testObject.ID}/action/linkrightsgroup/{rightsGroupID}", HttpStatusCode.OK);
            Assert.IsTrue(linkReponse.Success);

            //POST unlink RightGroups
            EntityActionChangeResponse unlinkReponse;
            unlinkReponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{testObject.ID}/action/unlinkrightsgroup/{rightsGroupID}", HttpStatusCode.OK);
            Assert.IsTrue(linkReponse.Success);

            //test: DELETE expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testObject.ID}", HttpStatusCode.NoContent);
        }
    }
}
