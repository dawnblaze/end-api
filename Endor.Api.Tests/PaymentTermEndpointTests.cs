﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.EF;

namespace Endor.Api.Tests
{
    [TestClass]
    public class PaymentTermEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/paymentterm";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestPaymentTermName;
        public TestContext TestContext { get; set; }
        
        [TestInitialize]
        public void Initialize()
        {
            this.NewTestPaymentTermName = DateTime.UtcNow + " TEST PAYMENT TERM";
        }

        [TestMethod]
        public async Task TestPaymentTermSingleEndpoints()
        {
            PaymentTerm paymentTermTestObject = new PaymentTerm()
            {
                Name = this.NewTestPaymentTermName,
                IsActive = true,
                BID = 1,
                DaysDue = 1,
                PaymentDueBasedOnType = PaymentDueBasedOnType.DaysAfterBuilt
            };

            //test: expect OK
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(paymentTermTestObject), HttpStatusCode.OK);

            // GET api/PaymentTerms/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimplePaymentTerm[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimplePaymentTerm sga in simpleList)
            {
                if (sga.DisplayName == paymentTermTestObject.Name)
                {
                    paymentTermTestObject.ID = sga.ID;
                }
            }

            //POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{paymentTermTestObject.ID}/action/setinactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{paymentTermTestObject.ID}/action/setactive", HttpStatusCode.OK);

            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + paymentTermTestObject.ID, JsonConvert.SerializeObject(paymentTermTestObject), HttpStatusCode.OK);

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{paymentTermTestObject.ID}", HttpStatusCode.NoContent);
            
            //test: expect BAD REQUEST
            //await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/9241657", HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public async Task TestPaymentTermPostNewInactive()
        {

            PaymentTerm paymentTermTestObject = new PaymentTerm()
            {
                Name = this.NewTestPaymentTermName,
                IsActive = false,
                BID = 1,
                PaymentDueBasedOnType = PaymentDueBasedOnType.DaysAfterBuilt
            };

            //test: expect OK
            var savedAccount = await EndpointTests.AssertPostStatusCode<PaymentTerm>(client, $"{apiUrl}", JsonConvert.SerializeObject(paymentTermTestObject), HttpStatusCode.OK);
            
            var account = await EndpointTests.AssertGetStatusCode<PaymentTerm>(client, $"{apiUrl}/{savedAccount.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(account);
            Assert.AreEqual(false, account.IsActive);
        }

        [TestMethod]
        public async Task TestPaymentTermSetDefault()
        {
            var ctx = GetApiContext(1);
            var defaultPaymentTermId = ctx.SystemOptionDefinition
                .FirstOrDefault(
                    x => x.Name == "Accounting.PaymentTerm.DefaultID")
                ?.DefaultValue;
            var paymentTermId = ctx.PaymentTerm.LastOrDefault(pt => pt.BID == 1)?.ID;
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client,
                $"{apiUrl}/{paymentTermId}/action/setdefault", HttpStatusCode.OK);
            Assert.IsTrue(result.Success);
            await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client,
                $"{apiUrl}/{defaultPaymentTermId}/action/setdefault", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestPaymentTermClearDefault()
        {
            var ctx = GetApiContext(1);
            var defaultPaymentTermId = ctx.SystemOptionDefinition
                .FirstOrDefault(
                    x => x.Name == "Accounting.PaymentTerm.DefaultID")
                ?.DefaultValue;
            var result = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client,
                $"{apiUrl}/action/cleardefault", HttpStatusCode.OK);
            Assert.IsTrue(result.Success);
            await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client,
                $"{apiUrl}/{defaultPaymentTermId}/action/setdefault", HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task TestPaymentTermCanDelete()
        {

            PaymentTerm paymentTermTestObject = new PaymentTerm()
            {
                Name = this.NewTestPaymentTermName,
                IsActive = false,
                BID = 1,
                PaymentDueBasedOnType = PaymentDueBasedOnType.DaysAfterBuilt
            };

            //test: expect OK
            var savedAccount = await EndpointTests.AssertPostStatusCode<PaymentTerm>(client, $"{apiUrl}", JsonConvert.SerializeObject(paymentTermTestObject), HttpStatusCode.OK);

            var account = await EndpointTests.AssertGetStatusCode<PaymentTerm>(client, $"{apiUrl}/{savedAccount.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(account);
            Assert.AreEqual(false, account.IsActive);

            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{savedAccount.ID}/action/candelete", HttpStatusCode.OK);
        }


        [TestMethod]
        public async Task TestClone()
        {
            var termName = this.NewTestPaymentTermName;
            PaymentTerm paymentTermTestObject = new PaymentTerm()
            {
                Name = termName,
                IsActive = false,
                BID = 1,
                PaymentDueBasedOnType = PaymentDueBasedOnType.DaysAfterBuilt
            };

            var created = await EndpointTests.AssertPostStatusCode<PaymentTerm>(client, $"{apiUrl}", JsonConvert.SerializeObject(paymentTermTestObject), HttpStatusCode.OK);
            var cloned = await EndpointTests.AssertPostStatusCode<PaymentTerm>(client, $"{apiUrl}/{created.ID}/clone", HttpStatusCode.OK);

            Assert.AreEqual($"{termName} (Clone)", cloned.Name);


            var cloned2 = await EndpointTests.AssertPostStatusCode<PaymentTerm>(client, $"{apiUrl}/{created.ID}/clone", null, HttpStatusCode.OK);
            Assert.AreEqual($"{termName} (Clone) (2)", cloned2.Name);

            var cloned3 = await EndpointTests.AssertPostStatusCode<PaymentTerm>(client, $"{apiUrl}/{created.ID}/clone", null, HttpStatusCode.OK);
            Assert.AreEqual($"{termName} (Clone) (3)", cloned3.Name);

            var cloneCloned = await EndpointTests.AssertPostStatusCode<PaymentTerm>(client, $"{apiUrl}/{cloned.ID}/clone", null, HttpStatusCode.OK);
            Assert.AreEqual($"{termName} (Clone) (Clone)", cloneCloned.Name);

            var cloneCloned2 = await EndpointTests.AssertPostStatusCode<PaymentTerm>(client, $"{apiUrl}/{cloned3.ID}/clone", null, HttpStatusCode.OK);
            Assert.AreEqual($"{termName} (Clone) (3) (Clone)", cloneCloned2.Name);



        }

        [TestCleanup]
        public void Teardown()
        {
            //you can actually do test-specific things like this
            //switch (TestContext.TestName)
            //{
            //    case "TestPaymentTermSingleEndpoints":
            //    case "TestFiltering":
            //        // do stuff
            //        break;
            //    default:
            //        break;
            //}
            this.DeleteTestAccountRecord();
        }

        private void DeleteTestAccountRecord()
        {
            string newTestPaymentTermName = NewTestPaymentTermName;
            Task.Run(async () =>
            {

                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                SimplePaymentTerm[] result = JsonConvert.DeserializeObject<SimplePaymentTerm[]>(responseString);
                if (result != null)
                {



                    var spts = result.Where(x => x.DisplayName.Contains(newTestPaymentTermName));

                    foreach (var spt in spts)
                    {
                        await client.DeleteAsync($"{apiUrl}/{spt.ID}");
                    }

                }
            }).Wait();
        }
    }
}
