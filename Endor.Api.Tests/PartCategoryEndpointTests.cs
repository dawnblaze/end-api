﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;

namespace Endor.Api.Tests
{
    [TestClass]
    public class PartCategoryEndpointTests
    {
        public const string apiUrl = "/api/laborcategory";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestLaborDataName;
        private string NewTestLaborCategoryName;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            this.NewTestLaborDataName = DateTime.UtcNow + " TEST PART LABOR DATA";
            this.NewTestLaborCategoryName = DateTime.UtcNow + " TEST PART LABOR DATA";
        }

        [TestMethod]
        public async Task TestLaborCategorySingleEndpoints()
        {
            LaborCategory testObject = await GetPostAssertNewLaborCategory();

            // GET api/PaymentTerms/simplelist
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleLaborCategory[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            foreach (SimpleLaborCategory sga in simpleList)
            {
                if (sga.DisplayName == testObject.Name)
                {
                    testObject.ID = sga.ID;
                }
            }

            testObject.Description = "test test";

            //test: expect OK
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + testObject.ID, JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);

            //test: expect OK
            var updatedLaborCategory = await EndpointTests.AssertGetStatusCode<LaborCategory>(client, $"{apiUrl}/" + testObject.ID, HttpStatusCode.OK);
            Assert.AreEqual(updatedLaborCategory.Description, testObject.Description);

            //test: expect No Content
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{testObject.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestLaborCategoryFilters()
        {
            //setup: get new labor category
            LaborCategory testObject = await GetPostAssertNewLaborCategory();

            //test: make sure it exists when filtering simple list for isactive=true
            var simpleList = await EndpointTests.AssertGetStatusCode<SimpleLaborCategory[]>(client, $"{apiUrl}/SimpleList?isActive=true", HttpStatusCode.OK);
            Assert.IsNotNull(simpleList.FirstOrDefault(x => x.ID == testObject.ID));

            //test: expect only active categories
            var active = await EndpointTests.AssertGetStatusCode<LaborCategory[]>(client, $"{apiUrl}?IsActive=true", HttpStatusCode.OK);
            Assert.IsTrue(active.All(x => x.IsActive));
            Assert.IsTrue(active.Count() > 0);

            //test: set isactive false
            testObject.IsActive = false;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/" + testObject.ID, JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);
            
            //test: expect only inactive categories
            var inactive = await EndpointTests.AssertGetStatusCode<LaborCategory[]>(client, $"{apiUrl}?IsActive=false", HttpStatusCode.OK);
            Assert.IsTrue(inactive.All(x => !x.IsActive));
            Assert.IsTrue(inactive.Count() > 0);

            //setup: link child
            LaborData testChild = await GetPostAssertNewLaborData();
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{testObject.ID}/action/linklabor/{testChild.ID}", null, HttpStatusCode.OK);

            //test: make sure there are no expanded labors
            var noneChild = await EndpointTests.AssertGetStatusCode<LaborCategory>(client, $"{apiUrl}/{testObject.ID}", HttpStatusCode.OK);
            Assert.IsTrue(noneChild.Labors == null || noneChild.Labors.Count() == 0);
            Assert.IsTrue(noneChild.SimpleLabors == null || noneChild.SimpleLabors.Count() == 0);

            //test: make sure there are expanded labors
            var fullChild = await EndpointTests.AssertGetStatusCode<LaborCategory>(client, $"{apiUrl}/{testObject.ID}?LaborParts=Full", HttpStatusCode.OK);
            Assert.IsTrue(fullChild.Labors != null && fullChild.Labors.Count() > 0);
            Assert.IsTrue(fullChild.SimpleLabors == null || fullChild.SimpleLabors.Count() == 0);

            //test: make sure there are  expanded simplelabors
            var simpleChild = await EndpointTests.AssertGetStatusCode<LaborCategory>(client, $"{apiUrl}/{testObject.ID}?LaborParts=Simple", HttpStatusCode.OK);
            Assert.IsTrue(simpleChild.Labors == null || simpleChild.Labors.Count() == 0);
            Assert.IsTrue(simpleChild.SimpleLabors != null && simpleChild.SimpleLabors.Count() > 0);
        }

        private async Task<LaborCategory> GetPostAssertNewLaborCategory()
        {
            //cleanup test labor data name...
            var simpleLaboCategory = await EndpointTests.AssertGetStatusCode<SimpleLaborCategory[]>(client, $"{apiUrl}/SimpleList", HttpStatusCode.OK);
            var laborCategory = simpleLaboCategory.FirstOrDefault(s => s.DisplayName == this.NewTestLaborCategoryName);

            if (laborCategory != null)
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{laborCategory.ID}", HttpStatusCode.NoContent);
            }

            LaborCategory testObject = new LaborCategory()
            {
                Name = this.NewTestLaborCategoryName,
                IsActive = true,
                BID = 1,
                Description = "blah blah"
            };

            //test: expect OK
            var newCategory = await EndpointTests.AssertPostStatusCode<LaborCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);
            return newCategory;
        }

        [TestMethod]
        public async Task TestLaborCategoryActionEndpoints()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleLaborData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;
            LaborData testLaborData = new LaborData()
            {
                Name = this.NewTestLaborDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InvoiceText = "blah blah"
            };
            var newLaborData = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"/api/laborpart", JsonConvert.SerializeObject(testLaborData), HttpStatusCode.OK);

            LaborCategory testLaborCat = new LaborCategory()
            {
                Name = this.NewTestLaborCategoryName,
                IsActive = true,
                BID = 1,
                Description = "blah blah"
            };

            //test: expect OK
            var newLaborCategory = await EndpointTests.AssertPostStatusCode<LaborCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(testLaborCat), HttpStatusCode.OK);

            //POST setinactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newLaborCategory.ID}/action/setinactive", HttpStatusCode.OK);

            //POST setactive
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{newLaborCategory.ID}/action/setactive", HttpStatusCode.OK);

            //POST candelete
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{newLaborCategory.ID}/action/candelete", HttpStatusCode.OK);

            //POST link labor
            EntityActionChangeResponse linkResponse;
            linkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newLaborCategory.ID}/action/linklabor/{newLaborData.ID}", HttpStatusCode.OK);
            Assert.IsTrue(linkResponse.Success);
            //unable to link twice
            linkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newLaborCategory.ID}/action/linklabor/{newLaborData.ID}", HttpStatusCode.BadRequest);
            Assert.IsFalse(linkResponse.Success);

            //POST unlink labor
            EntityActionChangeResponse unlinkResponse;
            unlinkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newLaborCategory.ID}/action/unlinklabor/{newLaborData.ID}", HttpStatusCode.OK);
            Assert.IsTrue(unlinkResponse.Success);
            //unable to unlink twice
            unlinkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"{apiUrl}/{newLaborCategory.ID}/action/unlinklabor/{newLaborData.ID}", HttpStatusCode.BadRequest);
            Assert.IsFalse(unlinkResponse.Success);
        }

        [TestMethod]
        public async Task TestLaborCategoryLinkingEndpoints()
        {
            //setup
            LaborCategory testCategory = await GetPostAssertNewLaborCategory();
            LaborData testChild = await GetPostAssertNewLaborData();

            //test: link from category to child
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{testCategory.ID}/action/linklabor/{testChild.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}/{testCategory.ID}/action/unlinklabor/{testChild.ID}", HttpStatusCode.OK);

            //test: link from child to category
            await EndpointTests.AssertPostStatusCode(client, $"/api/laborpart/{testChild.ID}/action/linklaborcategory/{testCategory.ID}", HttpStatusCode.OK);
            await EndpointTests.AssertPostStatusCode(client, $"/api/laborpart/{testChild.ID}/action/unlinklaborcategory/{testCategory.ID}", HttpStatusCode.OK);
        }

        private async Task<LaborData> GetPostAssertNewLaborData()
        {
            //cleanup test labor data name...
            var simpleLaboData = await EndpointTests.AssertGetStatusCode<SimpleLaborData[]>(client, $"api/laborpart/SimpleList", HttpStatusCode.OK);
            var laborData = simpleLaboData.FirstOrDefault(s => s.DisplayName == this.NewTestLaborDataName);

            if (laborData != null)
            {
                await EndpointTests.AssertDeleteStatusCode(client, $"api/laborpart/{laborData.ID}", HttpStatusCode.NoContent);
            }

            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleLaborData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;

            LaborData testObject = new LaborData()
            {
                Name = this.NewTestLaborDataName,
                IsActive = true,
                BID = 1,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InvoiceText = "blah blah"
            };

            //test: expect OK
            var newObject = await EndpointTests.AssertPostStatusCode<LaborData>(client, $"api/laborpart", JsonConvert.SerializeObject(testObject), HttpStatusCode.OK);
            return newObject;
        }

        [TestCleanup]
        public void Teardown()
        {
            //you can actually do test-specific things like this
            //switch (TestContext.TestName)
            //{
            //    case "TestPaymentTermSingleEndpoints":
            //    case "TestFiltering":
            //        // do stuff
            //        break;
            //    default:
            //        break;
            //}
            this.DeleteTestRecord();
        }

        private void DeleteTestRecord()
        {
            string newTestLaborCategoryName = NewTestLaborCategoryName;
            string newTestLaborDataName = NewTestLaborDataName;
            Task.Run(async () =>
            {
                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}/SimpleList");
                string responseString = await response.Content.ReadAsStringAsync();
                SimpleLaborCategory[] result = JsonConvert.DeserializeObject<SimpleLaborCategory[]>(responseString);
                if (result != null)
                {
                    SimpleLaborCategory sga = result.FirstOrDefault(x => x.DisplayName.Contains(newTestLaborCategoryName));
                    if (sga != null)
                    {
                        await client.DeleteAsync($"{apiUrl}/{sga.ID}?Force=true");
                    }
                }
                response = await client.GetAsync($"api/laborpart/SimpleList");
                responseString = await response.Content.ReadAsStringAsync();
                SimpleLaborData[] dataResult = JsonConvert.DeserializeObject<SimpleLaborData[]>(responseString);
                if (result != null)
                {
                    SimpleLaborData sga = dataResult.FirstOrDefault(x => x.DisplayName.Contains(newTestLaborDataName));
                    if (sga != null)
                    {
                        await client.DeleteAsync($"api/laborpart/{sga.ID}");
                    }
                }
            }).Wait();
        }
    }
}
