﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Endor.Api.Web.Controllers;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using AssertX = Xunit.Assert;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using Newtonsoft.Json;
using Endor.DocumentStorage.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Endor.Tenant;
using System.IO;
using System.Linq;
using System.Reflection;
using Endor.EF;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Services;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
using Endor.Api.Web.Classes;
using Endor.Api.Web;
using System.Net.Http;
using System.Net;
using Endor.Tenant;
using System.Text;

namespace Endor.Api.Tests
{
    public class CommonControllerTestClass
    {
        protected const string SQLConfigName = "SQL";
        protected const string BlobStorageConfigName = "BlobStorage";

        protected const string TestProjectName = "Endor.Api.Tests";
        protected const string TestTextFileName = "test.txt";
        protected const string TestTextFilePath = TestProjectName + "\\" + TestTextFileName;

        protected const string TestTextTwoFileName = "test_two.txt";
        protected const string TestTextTwoFilePath = TestProjectName + "\\" + TestTextTwoFileName;

        protected const string Test1x1PixelPngFileName = "1x1pixel.png";
        protected const string Test1x1PixelPngFilePath = TestProjectName + "\\" + Test1x1PixelPngFileName;
        protected const int AuthUserID = 1;
        protected const int UserLinkID = -98;

        protected IConfigurationRoot _config;

        [TestInitialize]
        public virtual void Init()
        {
            string file = "..\\..\\..\\client-secrets.json";
            if (!File.Exists(file))
            {
                File.WriteAllText(file, @"{
    ""SQL"": ""Data Source=DESKTOP-2MQDL0P\\SQLEXPRESS;Initial Catalog=\""Dev.Endor.Business.DB1\"";User ID=cyrious;Password=watankahani"",
    ""BlobStorage"":  ""DefaultEndpointsProtocol=https;AccountName=endordevgurramang;AccountKey=3uBQ8V0r4NFm9wEBrO/HdCzgfHig/JBnXMdJppUHPpm4uHwW5NjKhKNZKZuSmOI5AH/rcxVvsRmwZ6ziVy0SSw==;EndpointSuffix=core.windows.net""
}");
            }

            string text = File.ReadAllText(file);
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(text);

            this._config = new ConfigurationBuilder().AddInMemoryCollection(result).Build();
            Assert.IsTrue(_config[SQLConfigName] != null && _config[SQLConfigName].Length > 0);
            Assert.IsTrue(_config[BlobStorageConfigName] != null && _config[BlobStorageConfigName].Length > 0);
        }

        /// <summary>
        /// Uploads a file on disk with name `<fileSystemFileName>` to blob `<pathAndFileName>` and asserts it succeeded
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="testController"></param>
        /// <param name="fileSystemFileName"></param>
        /// <param name="pathAndFileName">defaults to fileSystemFileName if null</param>
        /// <returns></returns>
        protected async Task UploadFile(DMController.RequestModel entity, DMController testController, string fileSystemFileName, string pathAndFileName = null)
        {
            AddFileToForm(testController, fileSystemFileName);

            entity.PathAndFilename = pathAndFileName ?? fileSystemFileName;
            var okPost = AssertX.IsType<OkObjectResult>(await testController.Post(entity, null, null));
            Assert.IsTrue(okPost.StatusCode == 200);
            AssertX.IsType<DMItem>(okPost.Value);

            //CreatedByID is expected to be the current User.UserLinkID
            DMItem file = okPost.Value as DMItem;
            Assert.IsTrue(file.CreatedByID == UserLinkID);

            entity.PathAndFilename = null;
            //stop sending form files
            RemoveUploadFormFromRequest(testController);
        }

        protected async Task<M> AssertCreate<M, S, I>(CRUDController<M, S, I> controller, M toCreate)
            where M : class, IAtom<I>
            where S : AtomCRUDService<M, I>
            where I : struct, IConvertible
        {
            var result = await controller.Create(toCreate);
            var okPost = AssertX.IsType<OkObjectResult>(result);
            return AssertX.IsType<M>(okPost.Value);
        }

        protected static R AssertIsObjectResponseWithObject<T, R>(IActionResult response)
            where T : ObjectResult
        {
            var ok = AssertX.IsType<T>(response);
            Assert.IsNotNull(ok.Value);
            return AssertX.IsType<R>(ok.Value);
        }

        protected void AddFileToForm(DMController testController, params string[] fileNames)
        {
            var formFiles = new Microsoft.AspNetCore.Http.FormFileCollection();
            foreach (string fileName in fileNames)
            {
                //we keep this filestream open so we can read it later
                var fileStream = File.OpenRead(fileName);
                var formFile = new Microsoft.AspNetCore.Http.FormFile(fileStream, 0, fileStream.Length, fileName, fileName);

                var mimeType = GetMimeType(fileName);
                formFile.Headers = new HeaderDictionary();
                formFile.Headers.Add("Content-Type", mimeType);
                formFiles.Add(formFile);
            }

            testController.Request.Form = new FormCollection(null, formFiles);
        }

        protected static void RemoveUploadFormFromRequest(DMController testController)
        {
            testController.Request.Form = null;
        }

        protected async Task CleanSlate(DMController testController, DMController.RequestModel entity)
        {
            var deleteAllReq = entity.Clone();
            deleteAllReq.Bucket = Web.BucketRequest.All;
            await testController.Delete(deleteAllReq);
        }

        protected DMController GetDMController(short BID = TestConstants.BID)
        {
            MockTenantDataCache tdc = GetMockTenantDataCache();
            return AuthorizeTestController(new DMController(GetApiContext(BID), new Logging.Client.RemoteLogger(tdc), tdc, new MigrationHelper(new MockTaskQueuer(tdc))));
        }

        protected MockTenantDataCache GetMockTenantDataCache()
        {
            return new MockTenantDataCache(_config[SQLConfigName], _config[BlobStorageConfigName]);
        }

        protected ColumnsController GetColumnsController(short BID = TestConstants.BID)
        {
            MockTenantDataCache tdc = GetMockTenantDataCache();
            return AuthorizeTestController(new ColumnsController(GetApiContext(BID), new Logging.Client.RemoteLogger(tdc), new MockRealtimeMessagingPushClient(), new MockTaskQueuer(tdc), new MigrationHelper(new MockTaskQueuer(tdc))));
        }
        protected CriteriaController GetCriteriaController(short BID = TestConstants.BID)
        {
            MockTenantDataCache tdc = GetMockTenantDataCache();
            return AuthorizeTestController(new CriteriaController(GetApiContext(BID), new MigrationHelper(new MockTaskQueuer(tdc))));
        }
        protected FilterController GetFilterController(short BID = TestConstants.BID)
        {
            MockTenantDataCache tdc = GetMockTenantDataCache();
            return AuthorizeTestController(new FilterController(GetApiContext(), new Logging.Client.RemoteLogger(tdc), new MockRealtimeMessagingPushClient(), new MockTaskQueuer(tdc), tdc, new MigrationHelper(new MockTaskQueuer(tdc))));
        }
        protected EmployeeController GetEmployeeController(short BID = TestConstants.BID)
        {
            MockTenantDataCache tdc = GetMockTenantDataCache();
            return AuthorizeTestController(new EmployeeController(GetApiContext(), new Logging.Client.RemoteLogger(tdc), new MockRealtimeMessagingPushClient(), new MockTaskQueuer(tdc), tdc, null, new MigrationHelper(new MockTaskQueuer(tdc))));
        }

        protected T GetAtomCRUDController<T, M, S, I>(short BID = TestConstants.BID)
            where T : CRUDController<M, S, I>
            where M : class, IAtom<I>
            where S : AtomCRUDService<M, I>
            where I : struct, IConvertible
        {
            MockTenantDataCache tdc = GetMockTenantDataCache();
            return AuthorizeTestController((T)Activator.CreateInstance(typeof(T), GetApiContext(BID), new Logging.Client.RemoteLogger(tdc), new MockRealtimeMessagingPushClient(), new MockTaskQueuer(tdc), tdc, new MigrationHelper(new MockTaskQueuer(tdc))), BID);
        }

        protected T GetGenericController<T, M, S, I>(short BID = TestConstants.BID)
            where T : GenericController<M, S, I>
            where M : class
            where S : BaseGenericService
            where I : struct, IConvertible
        {
            EndorOptions endorOptions = new EndorOptions { };
            MockTenantDataCache tdc = GetMockTenantDataCache();
            IMigrationHelper migrationHelper = new MigrationHelper(new MockTaskQueuer(tdc));
            return AuthorizeTestController((T)Activator.CreateInstance(typeof(T), GetApiContext(BID), new Logging.Client.RemoteLogger(tdc), new MockRealtimeMessagingPushClient(), endorOptions, migrationHelper), BID);
        }

        protected T GetGenericControllerNoOptions<T, M, S, I>(short BID = TestConstants.BID)
            where T : GenericController<M, S, I>
            where M : class
            where S : BaseGenericService
            where I : struct, IConvertible
        {
            MockTenantDataCache tdc = GetMockTenantDataCache();
            IMigrationHelper migrationHelper = new MigrationHelper(new MockTaskQueuer(tdc));
            return AuthorizeTestController((T)Activator.CreateInstance(typeof(T), GetApiContext(BID), new Logging.Client.RemoteLogger(tdc), new MockRealtimeMessagingPushClient(), migrationHelper), BID);
        }

        protected T GetGenericControllerWithEtagCache<T>(short BID = TestConstants.BID)
            where T : Controller
        {
            EtagCache etagCache = new EtagCache { };
            MockTenantDataCache tdc = GetMockTenantDataCache();
            return AuthorizeTestController((T)Activator.CreateInstance(typeof(T), tdc, GetApiContext(BID), etagCache), BID);
        }

        protected ApiContext GetApiContext(short BID = TestConstants.BID)
        {
            var result = new ApiContext(new DbContextOptionsBuilder<ApiContext>().Options, GetMockTenantDataCache(), BID);
            new MigrationHelper(new MockTaskQueuer(GetMockTenantDataCache())).MigrateDb(result);
            return result;
        }

        protected DocumentManager GetDocumentManager(short bid, int id, ClassType classType, BucketRequest bucketRequest)
        {
            var dmid = new DocumentStorage.Models.DMID()
            {
                ctid = (int?)classType,
                id = id,
            };
            var storage = new StorageContext(bid, bucketRequest, dmid);
            MockTenantDataCache tdc = GetMockTenantDataCache();
            return new DocumentManager(tdc, storage);
        }

        protected T AuthorizeTestController<T>(T controller, short BID = TestConstants.BID)
            where T : Controller
        {
            var context = this.GetApiContext();
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                 new Claim(ClaimNameConstants.UserID, AuthUserID.ToString()),
                 new Claim(ClaimNameConstants.BID, BID.ToString()),
                 new Claim(ClaimNameConstants.AID, "1"),
                 new Claim(ClaimNameConstants.UserLinkID, UserLinkID.ToString()),
                 new Claim(ClaimNameConstants.EmployeeID, context.EmployeeData.First(t => t.BID == BID && t.IsActive).ID.ToString())
            }));
            var httpContext = new DefaultHttpContext() { User = user };
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext
            };
            /*
            controller.ObjectValidator = new DefaultObjectValidator(
                new DefaultModelMetadataProvider(
                    new DefaultCompositeMetadataDetailsProvider(
                        new List<IMetadataDetailsProvider>(new MvcOptions().ModelMetadataDetailsProviders) { })),
                new List<IModelValidatorProvider>() { new DefaultModelValidatorProvider() });
            */
            return controller;
        }

        protected void AssertHasTestBusiness()
        {
            var ctx = GetApiContext();
            var business = ctx.BusinessData.FirstOrDefault(x => x.BID == TestConstants.BID);
            if (business == null)
            {
                var templateBusiness = ctx.Set<BusinessData>().AsNoTracking().FirstOrDefault();
                if (templateBusiness == null)
                    throw new InvalidOperationException("No business in table to work off of!");
                else
                {
                    templateBusiness.BID = TestConstants.BID;
                    ctx.BusinessData.Add(templateBusiness);
                    ctx.SaveChanges();
                    business = ctx.BusinessData.FirstOrDefault(x => x.BID == TestConstants.BID);
                    Assert.IsNotNull(business);
                }
            }
        }

        private static string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            using (Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext))
            {
                if (regKey != null)
                {
                    var contentType = regKey.GetValue("Content Type");

                    mimeType = contentType != null ? contentType.ToString() : mimeType;
                }
            }
            return mimeType;
        }

        protected async Task<HttpResponseMessage> AssertPostFileStatusCode(HttpClient client, string url, HttpContent fileContent, params HttpStatusCode[] statusCodes)
        {
            HttpResponseMessage response = await client.PostAsync(url, fileContent);
            AssertAtLeastOneMatch(response, statusCodes);
            return response;
        }

        protected static void AssertAtLeastOneMatch(HttpResponseMessage response, HttpStatusCode[] statusCodes)
        {
            Assert.IsNotNull(response);
            if (statusCodes == null)
                throw new ArgumentNullException("statusCodes");
            else if (statusCodes.Length == 0)
                throw new InvalidOperationException("Must pass at least one expected status code");
            else if (statusCodes.Length == 1)
                Assert.AreEqual(statusCodes[0], response.StatusCode);
            else
            {
                if (!statusCodes.Any(x => response.StatusCode == x))
                {
                    string expected = String.Join(" or ", statusCodes);
                    Assert.Fail($"Expected {expected}, instead got {response.StatusCode}");
                }
            }
        }

        protected MultipartFormDataContent GetMultipartFormDataContent(string fieldName, string fileName)
        {
            MemoryStream stream = new MemoryStream();
            TextWriter tw = new StreamWriter(stream, Encoding.ASCII);
            tw.WriteLine("Hello World!");
            MultipartFormDataContent form = new MultipartFormDataContent();
            form.Add(new StreamContent(stream), fieldName, fileName);
            return form;
        }
    }
}
