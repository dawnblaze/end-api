﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Controllers;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Endor.Api.Tests
{
    [TestClass]
    public class EmployeeMessageEndpointTest : CommonControllerTestClass
    {
        public const string apiUrl = "/api/employee";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();


        private Message GetMessage()
        {
            return new Message()
            {
                ID = -99,
                ModifiedDT = DateTime.UtcNow,
                ReceivedDT = DateTime.UtcNow,
                Channels = MessageChannelType.None,
                Subject = "The Quick",
                HasBody = true,
                Body = "<b>brown fox</>",

            };
        }
        private MessageParticipantInfo GetParticipantInfo()
        {
            return new MessageParticipantInfo()
            {
                ID = -99,

                Channel = MessageChannelType.Email,
                IsMergeField = false,
                ModifiedDT = DateTime.UtcNow,

            };
        }

        [TestMethod]
        public async Task TestEmployeeMessageGetAllEmails()
        {
            await TestMessageHeader(0);
        }

        [TestMethod]
        public async Task TestEmployeeMessageHeaderMarkAsAllAsDeleted()
        {
            await TestMessageHeader(1);
        }
        [TestMethod]
        public async Task TestEmployeeMessageHeaderMarkAllAsRead()
        {
            await TestMessageHeader(2);
        }

        private async Task TestMessageHeader(int testType)
        {
            ApiContext ctx = GetApiContext(1);
            EmployeeData emp = ctx.EmployeeData.FirstOrDefault(e => e.IsActive);

            var participantInfoFrom = GetParticipantInfo();
            participantInfoFrom.ParticipantRoleType = MessageParticipantRoleType.From;
            participantInfoFrom.EmployeeID = emp.ID;

            var participantInfoTo = GetParticipantInfo();
            participantInfoTo.ParticipantRoleType = MessageParticipantRoleType.To;
            participantInfoTo.EmployeeID = emp.ID;

            var message = GetMessage();
            message.EmployeeID = emp.ID;
            message.Participants = new[] { participantInfoFrom, participantInfoTo };

            var postedMessage = await EndpointTests.AssertPostStatusCode<Message>(client, "/api/message", message, HttpStatusCode.OK);

            //test api/employee/notificationsinfo
            var notificationInfo = await EndpointTests
                    .AssertGetStatusCode<Dictionary<string,object>>(client, $"/api/employee/notificationsinfo?EmployeeID={emp.ID}", HttpStatusCode.OK);

            switch (testType)
            {
                case 0:
                    await EndpointTests.AssertGetStatusCode(client,
                        $"{apiUrl}/{emp.ID}/messages/channel?channelId={MessageChannelType.Email}",
                        HttpStatusCode.OK);
                    break;

                case 1:
                    await EndpointTests.AssertPostStatusCode(client,
                        $"{apiUrl}/{emp.ID}/messages/channel/{MessageChannelType.Email}/action/markallread",
                        HttpStatusCode.OK);
                    break;
                case 2:
                    await EndpointTests.AssertPostStatusCode(client,
                        $"{apiUrl}/{emp.ID}/messages/channel/{MessageChannelType.Email}/action/deleteall",
                        HttpStatusCode.OK);
                    break;
            }
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/message/{postedMessage.ID}", HttpStatusCode.OK);
        }
    }
}
