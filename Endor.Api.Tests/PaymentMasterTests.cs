﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Request;
using Endor.Api.Web.Classes.Responses;
using Endor.Api.Web.Services;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    [TestCategory("GL")]
    [TestCategory("Payment")]
    public class PaymentMasterTests : CommonControllerTestClass
    {

        public const string apiUrl = "/api/payment/master";
        public const string paymentApiUrl = "/api/payment";
        public System.Net.Http.HttpClient client;
        private string NewTestDataName;
        public TestContext TestContext { get; set; }


        [TestInitialize]
        public void Initialize()
        {
            this.NewTestDataName = "TestPaymentMaster-" + DateTimeOffset.Now.ToUnixTimeSeconds();

            Teardown();
            var ctx = GetApiContext();
            var authUserID = ctx.UserLink.Where(ul => ul.EmployeeID != null && ul.AuthUserID != null).First().AuthUserID;
            client = EndpointTests.GetHttpClient((short)authUserID);

            var paymentApplication = new Models.PaymentApplication
            {
                BID = 1,
                ID = -99,
                LocationID = 1,//ctx.LocationData.First().ID,
                CompanyID = 1000,//ctx.CompanyData.First().ID,
                ReceivedLocationID = ctx.LocationData.First().ID,
                MasterID = -99,
                Amount = 100,
                CurrencyType = (byte)AccountingCurrencyType.USDollar,
                DisplayNumber = "Test Payment Application Display Number",
                PaymentTransactionType = (byte)PaymentTransactionType.Payment_from_Nonrefundable_Credit,
                ApplicationGroupID = -99,
                OrderID = ctx.OrderData.First().ID,
                PaymentMethodID = 1
            };

            var paymentApplication2 = new Models.PaymentApplication
            {
                BID = 1,
                ID = -98,
                LocationID = 1,//ctx.LocationData.First().ID,
                CompanyID = 1000,//ctx.CompanyData.First().ID,
                ReceivedLocationID = ctx.LocationData.First().ID,
                MasterID = -99,
                Amount = 100,
                CurrencyType = (byte)AccountingCurrencyType.USDollar,
                DisplayNumber = "Test Payment Application Display Number",
                PaymentTransactionType = (byte)PaymentTransactionType.Payment_from_Nonrefundable_Credit,
                ApplicationGroupID = -99,
                OrderID = ctx.OrderData.First().ID,
                PaymentMethodID = 1
            };

            ctx.PaymentApplication.Add(paymentApplication);
            ctx.PaymentApplication.Add(paymentApplication2);


            var paymentMaster = new PaymentMaster()
            {
                BID = 1,
                ID = -99,
                LocationID = ctx.LocationData.First().ID,
                CompanyID = 1000,//ctx.CompanyData.First().ID,
                Amount = 5m,
                PaymentTransactionType = (byte)PaymentTransactionType.Payment,
                ReceivedLocationID = ctx.LocationData.First().ID
            };
            ctx.PaymentMaster.Add(paymentMaster);

            ctx.SaveChanges();
        }

        [TestCleanup]
        public void Teardown()
        {
            var ctx = GetApiContext();

            ctx.GLData.RemoveRange(ctx.GLData.Where(x => x.PaymentID < 0 || x.CompanyID < 0));
            var paymentApplicationCondition = ctx.PaymentApplication.Where(t => t.BID == 1 && t.MasterID == -99 || t.BID == 1 && t.MasterID == -98);
            var paymentApplicationCondition2 = ctx.PaymentApplication.Where(t => t.CompanyID < 0);
            var paymentMasterCondition = ctx.PaymentMaster.Where(t => t.BID == 1 && t.ID == -99);
            var paymentMasterCondition2 = ctx.PaymentMaster.Where(t => t.CompanyID < 0);
            ctx.PaymentApplication.RemoveRange(paymentApplicationCondition);
            ctx.PaymentApplication.RemoveRange(paymentApplicationCondition2);
            ctx.PaymentMaster.RemoveRange(paymentMasterCondition);
            ctx.PaymentMaster.RemoveRange(paymentMasterCondition2);
            ctx.SaveChanges();
            var orders = ctx.OrderData.Where(x => x.CompanyID < 0).ToList<TransactionHeaderData>();
            orders.AddRange(ctx.EstimateData.Where(x => x.CompanyID < 0).ToList<TransactionHeaderData>());
            ctx.OrderEmployeeRole.RemoveRange(ctx.OrderEmployeeRole.Where(x => orders.Select(t => t.ID).Contains(x.OrderID)));
            ctx.OrderContactRole.RemoveRange(ctx.OrderContactRole.Where(x => orders.Select(t => t.ID).Contains(x.OrderID)));
            ctx.OrderOrderLink.RemoveRange(ctx.OrderOrderLink.Where(x => orders.Select(t => t.ID).Contains(x.OrderID)));
            ctx.OrderOrderLink.RemoveRange(ctx.OrderOrderLink.Where(x => orders.Select(t => t.ID).Contains(x.LinkedOrderID)));
            ctx.TransactionHeaderData.RemoveRange(orders);
            ctx.OrderKeyDate.RemoveRange(ctx.OrderKeyDate.Where(x => orders.Select(t => t.ID).Contains(x.OrderID)));
            var orderItems = ctx.OrderItemData.Where(x => orders.Select(t => t.ID).Contains(x.OrderID));
            ctx.OrderItemData.RemoveRange(orderItems);
            ctx.OrderItemSurcharge.RemoveRange(ctx.OrderItemSurcharge.Where(x => orderItems.Select(t => t.ID).Contains(x.OrderItemID)));
            ctx.SaveChanges();

        }

        [TestMethod]
        public async Task TestGetByID()
        {

            var result = await EndpointTests.AssertGetStatusCode<PaymentMaster>(client, $"{apiUrl}/-99", HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.AreEqual(-99, result.ID);
            Assert.AreEqual(1, result.LocationID);
            Assert.AreEqual(1000, result.CompanyID);
        }

        [TestMethod]
        public async Task TestGetByIDWithIncludeApplications()
        {

            var result = await EndpointTests.AssertGetStatusCode<PaymentMaster>(client, $"{apiUrl}/-99?IncludeApplications=true", HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.AreEqual(-99, result.ID);
            Assert.AreEqual(1, result.LocationID);
            Assert.AreEqual(1000, result.CompanyID);
            Assert.IsNotNull(result.PaymentApplications);
            Assert.IsNotNull(result.ReceivedLocationID);
        }

        [TestMethod]
        public async Task TestGetList()
        {

            var result = await EndpointTests.AssertGetStatusCode<List<PaymentMaster>>(client, $"{apiUrl}?LocationID=1&CompanyID=1000&IncludeApplications=true", HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count() > 0);
            Assert.AreEqual(-99, result.First().ID);
            Assert.AreEqual(1, result.First().LocationID);
            Assert.AreEqual(1000, result.First().CompanyID);
            Assert.IsNotNull(result.First().PaymentApplications);
            Assert.AreEqual(2, result.First().PaymentApplications.Count());
        }

        [TestMethod]
        public async Task TestGetAvailableCredit()
        {
            using (var ctx = GetApiContext())
            {
                try
                {
                    var companiesToRemove = ctx.CompanyData.Where(x => x.ID < 0);
                    var companiesToRemoveIds = companiesToRemove.Select(x => x.ID).ToList();
                    ctx.PaymentApplication.RemoveRange(ctx.PaymentApplication.Where(x => companiesToRemoveIds.Contains(x.CompanyID.GetValueOrDefault())));
                    ctx.PaymentMaster.RemoveRange(ctx.PaymentMaster.Where(x => companiesToRemoveIds.Contains(x.CompanyID)));
                    ctx.CompanyData.RemoveRange(companiesToRemove);
                    await ctx.SaveChangesAsync();

                    CompanyData acntNameTest = new CompanyData()
                    {
                        Name = "TestCompany",
                        IsActive = true,
                        BID = 1,
                        ID = -125,
                        LocationID = ctx.LocationData.First().ID,
                        StatusID = ctx.EnumCrmCompanyStatus.First().ID

                    };
                    ctx.CompanyData.Add(acntNameTest);
                    await ctx.SaveChangesAsync();

                    // Non-Refundable Credit
                    var paymentLocation = ctx.LocationData.First();
                    var payment = GetSamplePaymentRequest();
                    payment.PaymentTransactionType = (int)PaymentTransactionType.Credit_Gift;
                    payment.Amount = 8.05m;
                    payment.Applications.FirstOrDefault().Amount = 8.05m;
                    payment.Applications.FirstOrDefault().OrderID = null;
                    payment.LocationID = paymentLocation.ID;
                    payment.PaymentMethodID = 251;
                    var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{paymentApiUrl}/creditgift", JsonConvert.SerializeObject(payment), HttpStatusCode.OK);

                    // Refundable
                    var paymentLocation2 = ctx.LocationData.Skip(1).FirstOrDefault();
                    if (paymentLocation2 == null)
                        paymentLocation2 = paymentLocation;

                    var payment2 = GetSamplePaymentRequest();
                    payment2.LocationID = paymentLocation2.ID;

                    var result2 = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{paymentApiUrl}/payment", JsonConvert.SerializeObject(payment2), HttpStatusCode.OK);

                    var getResult = await EndpointTests.AssertGetStatusCode<List<LocationGroupByResult>>(client, $"{paymentApiUrl}/availablecredit/{payment.CompanyID}", HttpStatusCode.OK);
                    Assert.IsNotNull(getResult);
                    Assert.AreEqual(getResult.Count(), 2);
                    var resultOne = getResult.Where(x => x.LocationID == payment.LocationID).First();
                    //Assert.AreEqual(resultOne.LocationName, paymentLocation.Name);
                    Assert.AreEqual(resultOne.NonRefundableCredit, 8.05m);
                    Assert.AreEqual(resultOne.RefundableCredit, 0);
                    Assert.AreEqual(resultOne.TotalCredit, 8.05m);

                    var resultTwo = getResult.Where(x => x.LocationID == payment2.LocationID).First();
                    //Assert.AreEqual(resultTwo.LocationName, paymentLocation2.Name);
                    Assert.AreEqual(resultTwo.NonRefundableCredit, 0);
                    Assert.AreEqual(resultTwo.RefundableCredit, 5m);
                    Assert.AreEqual(resultTwo.TotalCredit, 5m);
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
                finally
                {
                    var companiesToRemove = ctx.CompanyData.Where(x => x.ID < 0);
                    var companiesToRemoveIds = companiesToRemove.Select(x => x.ID).ToList();
                    ctx.PaymentApplication.RemoveRange(ctx.PaymentApplication.Where(x => companiesToRemoveIds.Contains(x.CompanyID.GetValueOrDefault())));
                    ctx.PaymentMaster.RemoveRange(ctx.PaymentMaster.Where(x => companiesToRemoveIds.Contains(x.CompanyID)));
                    ctx.CompanyData.RemoveRange(companiesToRemove);
                    await ctx.SaveChangesAsync();
                }
            }
        }

        [TestMethod]
        public async Task PaymentMasterAddPayment()
        {
            var payment = GetSamplePaymentRequest();
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{paymentApiUrl}/payment", JsonConvert.SerializeObject(payment), HttpStatusCode.OK);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.MasterPayments);
            Assert.IsTrue(result.MasterPayments.Count() > 0);
            Assert.AreEqual((int)PaymentTransactionType.Payment, result.PaymentTransactionType);
            //cleanup
            var ctx = GetApiContext();
            var applicationTrans = ctx.PaymentApplication.Where(x => x.MasterID == result.MasterPayments.First().ID);
            Assert.AreEqual(applicationTrans.First().ID, applicationTrans.First().ApplicationGroupID);
            ctx.PaymentApplication.RemoveRange(applicationTrans);
            ctx.PaymentMaster.RemoveRange(ctx.PaymentMaster.Where(x => x.ID == result.MasterPayments.First().ID));
            await ctx.SaveChangesAsync();
        }

        [TestMethod]
        public async Task PaymentBadPayment()
        {
            var payment = GetSamplePaymentRequest();
            payment.Applications.FirstOrDefault().Amount = 6m;
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{paymentApiUrl}/payment", JsonConvert.SerializeObject(payment), HttpStatusCode.BadRequest);

        }

        [TestMethod]
        public async Task PaymentThatIsMoreThanOrder()
        {
            var payment = GetSamplePaymentRequest();
            payment.Amount = 19m;
            var testOrder = Utils.GetTestOrderData();
            testOrder.CompanyID = await Utils.GetCompanyID(client);
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            testOrder.PaymentBalanceDue = 19m;
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            payment.Applications.FirstOrDefault().Amount = 20m;
            payment.Applications.FirstOrDefault().OrderID = createdOrder.ID;
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{paymentApiUrl}/payment", JsonConvert.SerializeObject(payment), HttpStatusCode.BadRequest);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.NoContent);

        }

        [TestMethod]
        public async Task PaymentOnOrder()
        {
            var payment = GetSamplePaymentRequest();
            payment.Amount = 8m;
            var testOrder = Utils.GetTestOrderData();
            var ctx = GetApiContext();
            testOrder.CompanyID = ctx.CompanyData.First().ID;
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;

            testOrder.PriceProductTotal = 10m;
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);


            payment.Applications.FirstOrDefault().Amount = 8m;
            payment.Applications.FirstOrDefault().OrderID = createdOrder.ID;
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{paymentApiUrl}/payment", JsonConvert.SerializeObject(payment), HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.OK);
            Assert.AreEqual(2m, retrievedOrder.PaymentBalanceDue);
            Assert.AreEqual(8m, retrievedOrder.PaymentPaid);
            var applicationTrans = ctx.PaymentApplication.Where(x => x.MasterID == result.MasterPayments.First().ID);
            ctx.PaymentApplication.RemoveRange(applicationTrans);
            ctx.PaymentMaster.RemoveRange(ctx.PaymentMaster.Where(x => x.ID == result.MasterPayments.First().ID));
            await ctx.SaveChangesAsync(); await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.NoContent);

        }

        [TestMethod]
        public async Task PaymentAndRefundableCreditExampleTest()
        {
            // Create the order
            var testOrder = Utils.GetTestOrderData();
            var ctx = GetApiContext();
            testOrder.CompanyID = ctx.CompanyData.First().ID;
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;
            testOrder.PriceProductTotal = 1000;
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            // Example #1a Simple Payment
            var payment = GetSamplePaymentRequest();
            payment.Amount = 100m;
            payment.Applications.FirstOrDefault().Amount = 100m;
            payment.Applications.FirstOrDefault().OrderID = createdOrder.ID;
            var paymentResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{paymentApiUrl}/payment", JsonConvert.SerializeObject(payment), HttpStatusCode.OK);

            // Test the order information
            var retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.OK);
            Assert.AreEqual(testOrder.PriceProductTotal - payment.Amount, retrievedOrder.PaymentBalanceDue);
            Assert.AreEqual(payment.Amount, retrievedOrder.PaymentPaid);

            // Test the payment master/application information
            var retrievedMasterPayment = await EndpointTests.AssertGetStatusCode<PaymentMaster>(client, $"/api/payment/master/{paymentResult.MasterPayments.First().ID}?IncludeApplications=true", HttpStatusCode.OK);
            Assert.AreEqual(payment.Amount, retrievedMasterPayment.Amount);
            Assert.AreEqual(payment.Amount, retrievedMasterPayment.RefBalance);
            Assert.AreEqual(0, retrievedMasterPayment.ChangeInRefCredit);
            Assert.IsNull(retrievedMasterPayment.ChangeInNonRefCredit);
            Assert.IsNull(retrievedMasterPayment.NonRefBalance);
            Assert.AreEqual(payment.PaymentTransactionType, retrievedMasterPayment.PaymentTransactionType);
            Assert.IsNotNull(retrievedMasterPayment.PaymentApplications);
            Assert.AreEqual(1, retrievedMasterPayment.PaymentApplications.Count);
            Assert.AreEqual(payment.Amount, retrievedMasterPayment.PaymentApplications.First().Amount);
            Assert.AreEqual(payment.PaymentTransactionType, retrievedMasterPayment.PaymentApplications.First().PaymentTransactionType);
            Assert.AreEqual(payment.Amount, retrievedMasterPayment.PaymentApplications.First().RefBalance);
            Assert.IsNull(retrievedMasterPayment.PaymentApplications.First().NonRefBalance);

            // Example #1b Refund from payment
            var orderReductionAmount = 30m;
            retrievedOrder.PriceProductTotal += -orderReductionAmount;
            var updatedOrder = await EndpointTests.AssertPutStatusCode<OrderData>(client, $"/api/order/{retrievedOrder.ID}", JsonConvert.SerializeObject(retrievedOrder), HttpStatusCode.OK);

            retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"/api/order/{updatedOrder.ID}", HttpStatusCode.OK);
            Assert.AreEqual(testOrder.PriceProductTotal - orderReductionAmount - payment.Amount, retrievedOrder.PaymentBalanceDue);
            Assert.AreEqual(payment.Amount, retrievedOrder.PaymentPaid);

            var refund = new PaymentRefundRequest();
            refund.MasterPaymentID = paymentResult.MasterPayments.First().ID;
            refund.Amount = -orderReductionAmount;
            refund.PaymentTransactionType = PaymentTransactionType.Refund;
            refund.Applications = new List<PaymentApplicationRequest>()
            {
                new PaymentApplicationRequest()
                {
                     Amount = refund.Amount,
                     OrderID = retrievedOrder.ID,
                },
            };
            //refund.PaymentMethodID = 1;

            var refundResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{paymentApiUrl}/refund", JsonConvert.SerializeObject(refund), HttpStatusCode.OK);

            var retrievedOrderAfterRefund = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.OK);
            Assert.AreEqual(retrievedOrder.PriceProductTotal - payment.Amount - refund.Amount, retrievedOrderAfterRefund.PaymentBalanceDue);
            Assert.AreEqual(payment.Amount - orderReductionAmount, retrievedOrderAfterRefund.PaymentPaid);

            // Test the payment master/application information after the refund
            var retrievedMasterPaymentAfterRefund = await EndpointTests.AssertGetStatusCode<PaymentMaster>(client, $"/api/payment/master/{paymentResult.MasterPayments.First().ID}?IncludeApplications=true", HttpStatusCode.OK);
            Assert.AreEqual(payment.Amount, retrievedMasterPaymentAfterRefund.Amount);
            Assert.AreEqual(payment.Amount - orderReductionAmount, retrievedMasterPaymentAfterRefund.RefBalance);
            Assert.AreEqual(0, retrievedMasterPaymentAfterRefund.ChangeInRefCredit);
            Assert.IsNull(retrievedMasterPaymentAfterRefund.ChangeInNonRefCredit);
            Assert.IsNull(retrievedMasterPaymentAfterRefund.NonRefBalance);
            Assert.AreEqual(PaymentTransactionType.Payment, (PaymentTransactionType)retrievedMasterPaymentAfterRefund.PaymentTransactionType);
            Assert.IsNotNull(retrievedMasterPaymentAfterRefund.PaymentApplications);
            Assert.AreEqual(2, retrievedMasterPaymentAfterRefund.PaymentApplications.Count);
            Assert.AreEqual(payment.Amount, retrievedMasterPaymentAfterRefund.PaymentApplications.First().Amount);
            Assert.AreEqual(refund.Amount, retrievedMasterPaymentAfterRefund.PaymentApplications.Skip(1).First().Amount);
            Assert.AreEqual(PaymentTransactionType.Payment, (PaymentTransactionType)retrievedMasterPaymentAfterRefund.PaymentApplications.First().PaymentTransactionType);
            Assert.AreEqual(PaymentTransactionType.Refund, (PaymentTransactionType)retrievedMasterPaymentAfterRefund.PaymentApplications.Skip(1).First().PaymentTransactionType);
            Assert.AreEqual(payment.Amount - orderReductionAmount, retrievedMasterPaymentAfterRefund.PaymentApplications.First().RefBalance);
            Assert.AreEqual(0, retrievedMasterPaymentAfterRefund.PaymentApplications.Skip(1).First().RefBalance);
            Assert.IsNull(retrievedMasterPaymentAfterRefund.PaymentApplications.First().NonRefBalance);
            Assert.IsNull(retrievedMasterPaymentAfterRefund.PaymentApplications.Skip(1).First().NonRefBalance);


            var refundToCreditAmount = 20m;
            var refundToCredit = new PaymentTransferCreditRequest()
            {
                MasterPaymentID = retrievedMasterPaymentAfterRefund.ID,
                PaymentTransactionType = PaymentTransactionType.Refund_to_Refundable_Credit,
                Applications = new List<PaymentApplicationRequest>()
                {
                    new PaymentApplicationRequest()
                    {
                        Amount = -refundToCreditAmount,
                        OrderID = retrievedOrderAfterRefund.ID,
                    },
                    new PaymentApplicationRequest()
                    {
                        Amount = refundToCreditAmount,
                        OrderID = null,
                    },
                },
                PaymentMethodId = 250
            };

            var refundToCreditResult = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{paymentApiUrl}/transfertocredit", JsonConvert.SerializeObject(refundToCredit), HttpStatusCode.OK);

            // Test the payment master/application information after the refund
            var retrievedMasterPaymentAfterRefundToCredit = await EndpointTests.AssertGetStatusCode<PaymentMaster>(client, $"/api/payment/master/{paymentResult.MasterPayments.First().ID}?IncludeApplications=true", HttpStatusCode.OK);
            Assert.AreEqual(retrievedMasterPaymentAfterRefund.Amount, retrievedMasterPaymentAfterRefundToCredit.Amount);
            Assert.AreEqual(retrievedMasterPaymentAfterRefund.RefBalance, retrievedMasterPaymentAfterRefundToCredit.RefBalance);
            Assert.AreEqual(refundToCreditAmount, retrievedMasterPaymentAfterRefundToCredit.ChangeInRefCredit);
            Assert.IsNull(retrievedMasterPaymentAfterRefundToCredit.ChangeInNonRefCredit);
            Assert.IsNull(retrievedMasterPaymentAfterRefundToCredit.NonRefBalance);
            Assert.AreEqual(PaymentTransactionType.Payment, (PaymentTransactionType)retrievedMasterPaymentAfterRefundToCredit.PaymentTransactionType);
            Assert.IsNotNull(retrievedMasterPaymentAfterRefundToCredit.PaymentApplications);
            Assert.AreEqual(4, retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.Count);
            Assert.AreEqual(payment.Amount, retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.First().Amount);
            Assert.AreEqual(refund.Amount, retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.Skip(1).First().Amount);
            Assert.AreEqual(-refundToCreditAmount, retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.Skip(3).First().Amount);
            Assert.AreEqual(refundToCreditAmount, retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.Skip(2).First().Amount);
            Assert.AreEqual(PaymentTransactionType.Payment, (PaymentTransactionType)retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.First().PaymentTransactionType);
            Assert.AreEqual(PaymentTransactionType.Refund, (PaymentTransactionType)retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.Skip(1).First().PaymentTransactionType);
            Assert.AreEqual(PaymentTransactionType.Refund_to_Refundable_Credit, (PaymentTransactionType)retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.Skip(2).First().PaymentTransactionType);
            Assert.AreEqual(PaymentTransactionType.Refund_to_Refundable_Credit, (PaymentTransactionType)retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.Skip(3).First().PaymentTransactionType);
            Assert.AreEqual(payment.Amount - orderReductionAmount - refundToCreditAmount, retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.First().RefBalance);
            Assert.AreEqual(0, retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.Skip(1).First().RefBalance);
            Assert.AreEqual(0, retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.Skip(3).First().RefBalance);
            Assert.AreEqual(refundToCreditAmount, retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.Skip(2).First().RefBalance);
            Assert.IsNull(retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.First().NonRefBalance);
            Assert.IsNull(retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.Skip(1).First().NonRefBalance);
            Assert.IsNull(retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.Skip(2).First().NonRefBalance);
            Assert.IsNull(retrievedMasterPaymentAfterRefundToCredit.PaymentApplications.Skip(3).First().NonRefBalance);


            // cleanup order and payments
            var applicationTrans = ctx.PaymentApplication.Where(x => x.MasterID == paymentResult.MasterPayments.First().ID && x.BID == testOrder.BID);
            ctx.PaymentApplication.RemoveRange(applicationTrans);
            ctx.PaymentMaster.RemoveRange(ctx.PaymentMaster.Where(x => x.ID == paymentResult.MasterPayments.First().ID && x.BID == testOrder.BID));
            await ctx.SaveChangesAsync();
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task PaymentOnOrderCloseOrder()
        {
            var payment = GetSamplePaymentRequest();
            payment.Amount = 10m;
            var testOrder = Utils.GetTestOrderData();
            var ctx = GetApiContext();
            testOrder.CompanyID = ctx.CompanyData.First().ID;
            testOrder.PickupLocationID = await Utils.GetLocationID(client);
            testOrder.TaxGroupID = await Utils.GetTaxGroupID(client);
            testOrder.ProductionLocationID = testOrder.PickupLocationID;
            testOrder.OrderStatusID = OrderOrderStatus.OrderInvoiced;
            testOrder.PriceProductTotal = 10m;
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"/api/order/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);


            payment.Applications.FirstOrDefault().Amount = 10m;
            payment.Applications.FirstOrDefault().OrderID = createdOrder.ID;
            var result = await EndpointTests.AssertPostStatusCode<PaymentResponse>(client, $"{paymentApiUrl}/payment", JsonConvert.SerializeObject(payment), HttpStatusCode.OK);

            var retrievedOrder = await EndpointTests.AssertGetStatusCode<OrderData>(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.OK);
            Assert.AreEqual(0m, retrievedOrder.PaymentBalanceDue);
            Assert.AreEqual(10m, retrievedOrder.PaymentPaid);
            Assert.AreEqual(OrderOrderStatus.OrderClosed, retrievedOrder.OrderStatusID);
            //check GL

            //cleanup
            var applicationTrans = ctx.PaymentApplication.Where(x => x.MasterID == result.MasterPayments.First().ID);
            ctx.PaymentApplication.RemoveRange(applicationTrans);
            ctx.PaymentMaster.RemoveRange(ctx.PaymentMaster.Where(x => x.ID == result.MasterPayments.First().ID));
            await ctx.SaveChangesAsync(); await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.NoContent);

        }


        private PaymentRequest GetSamplePaymentRequest()
        {
            List<PaymentApplicationRequest> paymentApplications = new List<PaymentApplicationRequest>();
            paymentApplications.Add(new PaymentApplicationRequest
            {
                OrderID = null,
                Amount = 5m
            });

            var ctx = GetApiContext();
            return new PaymentRequest
            {
                LocationID = ctx.LocationData.First().ID,
                CompanyID = ctx.CompanyData.First().ID,
                Amount = 5m,
                PaymentTransactionType = (int)PaymentTransactionType.Payment,
                ReceivedLocationID = ctx.LocationData.First().ID,
                Applications = paymentApplications,
                PaymentMethodID = 1
            };
        }
    }
}
