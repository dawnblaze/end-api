﻿using Endor.Api.Web.Classes;
using Endor.Models;
using Microsoft.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class DashboardWidgetDefinitionTests : CommonControllerTestClass
    {
        public const string apiUrl = "/Api/System/Dashboard";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        [TestMethod]
        public async Task DashboardWidgetDefinitionTest()
        {
            var ctx = GetApiContext();
            try
            {
                ctx.DashboardWidgetCategoryLink.RemoveRange(ctx.DashboardWidgetCategoryLink.Where(x => x.WidgetDefID < 0));
                ctx.DashboardWidgetDefinition.RemoveRange(ctx.DashboardWidgetDefinition.Where(x => x.ID < 0));
                await ctx.SaveChangesAsync();
                var dw = new DashboardWidgetDefinition()
                {
                    BID = 1,
                    ID = -99,
                    DefaultName = "Test",
                };
                var dw2 = new DashboardWidgetDefinition()
                {
                    BID = 1,
                    ID = -98,
                    DefaultName = "Test",
                };
                ctx.DashboardWidgetDefinition.Add(dw);
                ctx.DashboardWidgetDefinition.Add(dw2);
                await ctx.SaveChangesAsync();

                DashboardWidgetCategoryLink link1 = new DashboardWidgetCategoryLink()
                {
                    WidgetDefID = ctx.DashboardWidgetDefinition.First().ID,
                    CategoryType = DashboardWidgetCategoryType.Ecommerce,
                };
                DashboardWidgetCategoryLink link2 = new DashboardWidgetCategoryLink()
                {
                    WidgetDefID = ctx.DashboardWidgetDefinition.Skip(1).First().ID,
                    CategoryType = DashboardWidgetCategoryType.Ecommerce,
                };
                DashboardWidgetCategoryLink link3 = new DashboardWidgetCategoryLink()
                {
                    WidgetDefID = ctx.DashboardWidgetDefinition.First().ID,
                    CategoryType = DashboardWidgetCategoryType.Production,
                };
                ctx.DashboardWidgetCategoryLink.Add(link1);
                ctx.DashboardWidgetCategoryLink.Add(link2);
                ctx.DashboardWidgetCategoryLink.Add(link3);
                ctx.SaveChanges();
                var getAll = await EndpointTests.AssertGetStatusCode<DashboardWidgetDefinition[]>(client, $"{apiUrl}/WidgetDefinitions", HttpStatusCode.OK);
                Assert.IsNotNull(getAll);
                var tmpList = getAll.SelectMany(x => x.WidgetCategoryLinks);
                var FirstCategoriy = tmpList.Select(x => x.CategoryType.ToString()).First();
                string joiner = "CategoryType=";
                var queryList = "?" + joiner + FirstCategoriy;
                var getSubset = await EndpointTests.AssertGetStatusCode<DashboardWidgetDefinition[]>(client, $"{apiUrl}/WidgetDefinitions" + queryList, HttpStatusCode.OK);
                Assert.IsTrue(getSubset.Count() < getAll.Count());
                var categories = getAll.SelectMany(x => x.WidgetCategoryLinks)
                    .Select(x => x.CategoryType.ToString()).First();
                queryList = string.Join("&" + joiner, categories);
                queryList = "?" + joiner + queryList;
                var getAllCategories = await EndpointTests.AssertGetStatusCode<DashboardWidgetDefinition[]>(client, $"{apiUrl}/WidgetDefinitions" + queryList, HttpStatusCode.OK);
                Assert.IsTrue(getAllCategories.Count() < getAll.Count());

                var getForID = await EndpointTests.AssertGetStatusCode<DashboardWidgetDefinition>(client, $"{apiUrl}/WidgetDefinitions/{getAll.FirstOrDefault().ID}", HttpStatusCode.OK);
                Assert.IsNotNull(getForID);

                var getCategories = await EndpointTests.AssertGetStatusCode<string[]>(client, $"{apiUrl}/WidgetCategories", HttpStatusCode.OK);
                Assert.IsNotNull(getCategories);
                // note: updated to 7 as there are 5 widgets, including the utc time
                Assert.AreEqual(7, getCategories.Count());
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx.DashboardWidgetCategoryLink.RemoveRange(ctx.DashboardWidgetCategoryLink.Where(x => x.WidgetDefID < 0));
                ctx.DashboardWidgetDefinition.RemoveRange(ctx.DashboardWidgetDefinition.Where(x => x.ID < 0));
                await ctx.SaveChangesAsync();
            }

        }

        [TestMethod]
        public async Task LineItemStatusCountTest()
        {
            string sqlExecute = "";

            #region copy of SPROC

            // @BID, @LocationID, and @StatusID must be declared
            // via sqlparameter
            sqlExecute = @"
            BEGIN
                --  --- There are helpful for testing
                --  DECLARE @BID SMALLINT = 1;
                --  DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
                --  DECLARE @StatusID VARCHAR(255) = '21, 22';
                DECLARE @AsTable BIT = 0;

                -- ---------------------------------------------
                -- Convert the StatusIDs to a list of IDs
                -- ---------------------------------------------
                DECLARE @IDs TABLE (ID TINYINT PRIMARY KEY);

                INSERT INTO @IDs
                    SELECT value from String_Split(@StatusID, ',');
    
                --  if none, add in WIP
                IF (NOT EXISTS(SELECT * FROM @IDs))
                    INSERT INTO @IDs (ID) 
                        VALUES (22);
                -- ---------------------------------------------

                -- ---------------------------------------------
                -- Pull a list and count of the line item status in those order statuses
                -- Store them in a temporary table so we can transform the output
                -- ---------------------------------------------
                DECLARE @Results Table (
                                  ItemStatusID SMALLINT
                                , OrderStatusID TINYINT
                                , LocationID TINYINT
                                , [BreakdownCount] INT
                                );

                INSERT INTO @Results
                    SELECT OI.ItemStatusID
                         , O.OrderStatusID
                         , O.LocationID
                         , COUNT(*) as BreakdownCount
                    FROM [Order.Data] O
                    JOIN [Order.Item.Data] OI ON O.ID = OI.OrderID AND O.BID = OI.BID
                    JOIN @IDs IDs ON O.OrderStatusID = IDs.ID
                    WHERE O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL)
                    GROUP BY O.OrderStatusID, O.LocationID, OI.ItemStatusID
                    ORDER BY ItemStatusID, O.OrderStatusID
                ;
                -- ---------------------------------------------

                -- ---------------------------------------------
                -- Now format the output as a table or JSON
                -- ---------------------------------------------
                IF (@AsTable = 1)
                    SELECT R.ItemStatusID, OIS.Name as 'Status'
                         , SUM(R.BreakdownCount) OVER( PARTITION BY R.ItemStatusID ) as 'Count'
                         , R.LocationID as 'Breakdown.LocationID'
                         , R.OrderStatusID as 'Breakdown.OrderStatusID'
                         , R.BreakdownCount as 'Breakdown.Count'
                    FROM @Results R
                    JOIN [Order.Item.Status] OIS ON ItemStatusID = OIS.ID AND @BID = OIS.BID
                    ORDER BY 3 DESC, 2
                    ;

                ELSE
                BEGIN
                    SELECT DISTINCT R.ItemStatusID, OIS.Name as 'Status'
                         , SUM(R.BreakdownCount) OVER( PARTITION BY R.ItemStatusID ) as 'Count'
                         , (SELECT   R2.LocationID as 'LocationID'
                                   , R2.OrderStatusID as 'OrderStatusID'
                                   , R2.BreakdownCount as 'Count'
                            FROM @Results R2 WHERE R2.ItemStatusID = R.ItemStatusID
                            FOR JSON AUTO) AS Breakdown
                    FROM @Results R
                    JOIN [Order.Item.Status] OIS ON ItemStatusID = OIS.ID AND @BID = OIS.BID
                    ORDER BY 3 DESC, 2
                    FOR JSON PATH
                    ;
                END;
                -- ---------------------------------------------

            END
            

            ";
            #endregion

            var resultStringFromSQL = await GetLineItemStatusCountSQLResult(sqlExecute);

            sqlExecute = "EXEC [dbo].[Dashboard.Widget.Definition.005] @BID, @LocationID, @StatusID";

            var resultStringFromSPROC = await GetLineItemStatusCountSQLResult(sqlExecute);

            ;
            Assert.IsTrue(resultStringFromSQL.Equals(resultStringFromSPROC), "result from SQL is not equal to result from Stored Procedure");
        }

        private async Task<string> GetLineItemStatusCountSQLResult(string sqlExecute)
        {
            var ctx = GetApiContext();
            LocationData location = ctx.LocationData.First();
            var BID = TestConstants.BID;
            var locationID = location.ID;
            var statusIDCSV = "21, 22";
            var resultStringFromSQL = "";

            var result = await ctx.Database.ExecuteSqlRawAsync2(
                sqlExecute, 
                CancellationToken.None, 
                new SqlParameter("BID", BID),
                new SqlParameter("LocationID", locationID),
                new SqlParameter("StatusID", statusIDCSV)
            );
            using (var reader = result.DbDataReader)
            {
                // get one value only
                if (await reader.ReadAsync())
                {
                    resultStringFromSQL = reader.GetString(0);
                }
            }
            Assert.IsFalse(String.IsNullOrEmpty(resultStringFromSQL), "result from sql query is null or empty");
            
            return resultStringFromSQL;
        }

        [TestMethod]
        public async Task OrderThroughputTest()
        {
            string sqlExecute = "";

            #region copy of SPROC

            // @BID, @LocationID, @StartUTCDT, @EndUTCDT must be declared
            // via sqlparameter
            sqlExecute = @"
                BEGIN

                    --  --- There are helpful for testing
                    -- DECLARE @BID SMALLINT = 1;
                    -- DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
                    -- DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
                    -- DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
                    DECLARE @AsTable BIT = 0;


                    --  Note
                    --     OrderStatusID = 21 for Pre-WIP      KeyDateType = 1 for Created
                    --                   = 22 for WIP                      = 5 for WIP
                    --                   = 23 for Built                    = 6 for Built
                    --                   = 24 for Invoicing                = 7 for Invoicing
                    --                   = 25 for Invoiced                 = 8 for Invoiced
                    --                   = 26 for Closed                   = 9 for Closed
                    --                   = 29 for Voided                   = 2 for Voided
    
                    -- ---------------------------------------------
                    -- Pull the data
                    -- ---------------------------------------------
                    DECLARE   @Results Table 
                                ( 
                                      LocationID TINYINT
                                    , OrderStatusID TINYINT
                                    , Name VARCHAR(100)
                                    , Count INT
                                    , Amount DECIMAL(18,4)
                                );


                    DECLARE @MidDate DateTime2(2) = (SELECT DATEADD(SECOND, DATEDIFF(SECOND, @StartUTCDT, @EndUTCDT )/2, @StartUTCDT));

                    -- Pull ID = 0 - Monthly Goal
                    INSERT INTO @Results
                        SELECT @LocationID, 0, 'Sales Goal', 1, SUM(LG.Budgeted)
                        FROM [Location.Goal] LG
                        WHERE LG.BID = @BID AND (@LocationID = LG.LocationID OR @LocationID IS NULL)
                          AND LG.Year = DatePart(Year, @MidDate)
                          AND LG.Month = DatePart(Month, @MidDate)
                    ;
                    -- pull orders that were created in the given range
                    INSERT INTO @Results
                        SELECT @LocationID, 1, 'Created', COUNT(*), SUM(E.[Price.PreTax])
                        FROM [Order.Data] E
                        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
                        AND EXISTS( SELECT * 
                                    FROM [Order.KeyDate] OD 
                                    WHERE OD.BID = E.BID
                                        AND OD.OrderID = E.ID 
                                        AND OD.OrderItemID IS NULL
                                        AND OD.KeyDateType = 21 -- Created
                                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                        )
                    ;

                    -- pull orders that were moved THROUGH Pre-WIP (into WIP) in the current range
                    INSERT INTO @Results
                        SELECT @LocationID, 21, 'Pre-WIP', COUNT(*), SUM(E.[Price.PreTax])
                        FROM [Order.Data] E
                        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
                        AND EXISTS( SELECT * 
                                    FROM [Order.KeyDate] OD 
                                    WHERE OD.BID = E.BID
                                        AND OD.OrderID = E.ID 
                                        AND OD.OrderItemID IS NULL
                                        AND OD.KeyDateType = 5 -- WIP
                                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                        )
                    ;

                    -- pull orders that were moved THROUGH WIP (into Built) in the current range
                    INSERT INTO @Results
                        SELECT @LocationID, 22, 'WIP', COUNT(*), SUM(E.[Price.PreTax])
                        FROM [Order.Data] E
                        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
                        AND EXISTS( SELECT * 
                                    FROM [Order.KeyDate] OD 
                                    WHERE OD.BID = E.BID
                                        AND OD.OrderID = E.ID 
                                        AND OD.OrderItemID IS NULL
                                        AND OD.KeyDateType = 6 -- Built
                                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                        )
                    ;

                    -- pull orders that were moved THROUGH Built (into Invoicing) in the current range
                    INSERT INTO @Results
                        SELECT @LocationID, 23, 'Built', COUNT(*), SUM(E.[Price.PreTax])
                        FROM [Order.Data] E
                        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
                        AND EXISTS( SELECT * 
                                    FROM [Order.KeyDate] OD 
                                    WHERE OD.BID = E.BID
                                        AND OD.OrderID = E.ID 
                                        AND OD.OrderItemID IS NULL
                                        AND OD.KeyDateType = 7 -- Invoicing
                                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                        )
                    ;

                    -- pull orders that were moved THROUGH Invoicing (into Invoiced) in the current range
                    INSERT INTO @Results
                        SELECT @LocationID, 24, 'Invoicing', COUNT(*), SUM(E.[Price.PreTax])
                        FROM [Order.Data] E
                        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
                        AND EXISTS( SELECT * 
                                    FROM [Order.KeyDate] OD 
                                    WHERE OD.BID = E.BID
                                        AND OD.OrderID = E.ID 
                                        AND OD.OrderItemID IS NULL
                                        AND OD.KeyDateType = 8 -- Invoiced
                                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                        )
                    ;

                    -- pull orders that were moved THROUGH Invoiced (into Closed) in the current range
                    INSERT INTO @Results
                        SELECT @LocationID, 25, 'Invoiced', COUNT(*), SUM(E.[Price.PreTax])
                        FROM [Order.Data] E
                        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
                        AND EXISTS( SELECT * 
                                    FROM [Order.KeyDate] OD 
                                    WHERE OD.BID = E.BID
                                        AND OD.OrderID = E.ID 
                                        AND OD.OrderItemID IS NULL
                                        AND OD.KeyDateType = 7 -- Closed
                                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                        )
                    ;

                    -- pull orders that were voided in the current range
                    INSERT INTO @Results
                        SELECT @LocationID, 29, 'Voided', COUNT(*), SUM(E.[Price.PreTax])
                        FROM [Order.Data] E
                        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
                        AND EXISTS( SELECT * 
                                    FROM [Order.KeyDate] OD 
                                    WHERE OD.BID = E.BID
                                        AND OD.OrderID = E.ID 
                                        AND OD.OrderItemID IS NULL
                                        AND OD.KeyDateType = 9 -- Closed
                                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                        )
                    ;


                    -- ---------------------------------------------

                    -- ---------------------------------------------
                    -- Now format the output as a table or JSON
                    -- ---------------------------------------------
                    IF (@AsTable = 1)
                        SELECT *
                        FROM @Results R
                        ORDER BY 1 DESC, 2
                        ;

                    ELSE
                    BEGIN
                        SELECT *
                        FROM @Results R
                        ORDER BY 1 DESC, 2
                        FOR JSON PATH
                        ;
                    END;
                    ---- ---------------------------------------------

                END
            ";
            #endregion

            var resultStringFromSQL = await GetOrderThroughputSQLResult(sqlExecute);

            sqlExecute = "EXEC [dbo].[Dashboard.Widget.Definition.003] @BID, @LocationID, @StartUTCDT, @EndUTCDT";

            var resultStringFromSPROC = await GetOrderThroughputSQLResult(sqlExecute);

            Assert.IsTrue(resultStringFromSQL.Equals(resultStringFromSPROC), "result from SQL is not equal to result from Stored Procedure");
        }

        private async Task<string> GetOrderThroughputSQLResult(string sqlExecute)
        {
            var ctx = GetApiContext();
            LocationData location = ctx.LocationData.First();
            var BID = TestConstants.BID;
            var locationID = location.ID;
            DateTime date = DateTime.UtcNow;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            var resultStringFromSQL = "";

            var result = await ctx.Database.ExecuteSqlRawAsync2(
                sqlExecute,
                CancellationToken.None,
                new SqlParameter("BID", BID),
                new SqlParameter("LocationID", locationID),
                new SqlParameter("StartUTCDT", firstDayOfMonth),
                new SqlParameter("EndUTCDT", lastDayOfMonth)
            );
            using (var reader = result.DbDataReader)
            {
                // get one value only
                if (await reader.ReadAsync())
                {
                    resultStringFromSQL = reader.GetString(0);
                }
            }
            Assert.IsFalse(String.IsNullOrEmpty(resultStringFromSQL), "result from sql query is null or empty");

            return resultStringFromSQL;
        }

        [TestMethod]
        public async Task FinancialSnapshotTest()
        {
            string sqlExecute = "";

            #region copy of SPROC

            // @BID, @LocationID, @StartUTCDT, @EndUTCDT must be declared
            // via sqlparameter
            sqlExecute = @"
                BEGIN
                    --  --- There are helpful for testing
                    -- DECLARE @BID SMALLINT = 1;
                    -- DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
                    -- DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
                    -- DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
                         DECLARE @AsTable BIT = 0;


                    --  Note
                    --     OrderStatusID = 21 for Pre-WIP      KeyDateType = 1 for Created
                    --                   = 22 for WIP                      = 5 for WIP
                    --                   = 23 for Built                    = 6 for Built
                    --                   = 24 for Invoicing                = 7 for Invoicing
                    --                   = 25 for Invoiced                 = 8 for Invoiced
                    --                   = 26 for Closed                   = 9 for Closed
                    --                   = 29 for Voided                   = 2 for Voided
    

                    -- ---------------------------------------------
                    -- Compute some working Data
                    -- ---------------------------------------------
                    DECLARE @MidDate DateTime2(2) = (SELECT DATEADD(SECOND, DATEDIFF(SECOND, @StartUTCDT, @EndUTCDT )/2, @StartUTCDT));


                    -- ---------------------------------------------
                    -- Pull the data
                    -- ---------------------------------------------
                    DECLARE   @Results Table 
                                ( 
                                      LocationID TINYINT
                                    , ID TINYINT
                                    , Name VARCHAR(100)
                                    , Count INT
                                    , Amount DECIMAL(18,4)
                                );


                    -- Pull ID = 1 - Monthly Goal
                    INSERT INTO @Results
                        SELECT @LocationID, 1, 'Monthly Goal', 1, SUM(LG.Budgeted)
                        FROM [Location.Goal] LG
                        WHERE LG.BID = @BID AND (@LocationID = LG.LocationID OR @LocationID IS NULL)
                          AND LG.Year = DatePart(Year, @MidDate)
                          AND LG.Month = DatePart(Month, @MidDate)
                    ;

                    -- Pull ID = 2 - MTD Sales
                    -- Rework to pull from GL .. but for now ... 
                    INSERT INTO @Results
                        SELECT @LocationID, 2, 'MTD Sales', COUNT(*), SUM(O.[Price.PreTax])
                        FROM [Order.Data] O
                        WHERE O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL)
                        AND EXISTS( SELECT * 
                                    FROM [Order.KeyDate] OD 
                                    WHERE OD.BID = O.BID
                                        AND OD.OrderID = O.ID 
                                        AND OD.OrderItemID IS NULL
                                        AND OD.KeyDateType = 8 -- Invoiced
                                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                        )
                    ;

                    -- Pull ID = 3 - MTD Payments
                    -- to get this, we can look at all new master payments (money in) and all payment applications refunds (money out)
                    DECLARE @MoneyIn DECIMAL(18,4) = 
                                    ( SELECT SUM(Amount)
                                       FROM [Accounting.Payment.Master] P
                                       WHERE P.BID = @BID AND (@LocationID = P.LocationID OR @LocationID IS NULL)
                                       AND P.AccountingDT BETWEEN @StartUTCDT AND @EndUTCDT
                                       AND P.PaymentTransactionType = 1 
                                    )
                    ;

                    DECLARE @MoneyOut DECIMAL(18,4) = 
                                    (   SELECT SUM(Amount)
                                        FROM [Accounting.Payment.Application] P
                                        WHERE P.BID = @BID AND (@LocationID = P.LocationID OR @LocationID IS NULL)
                                        AND P.AccountingDT BETWEEN @StartUTCDT AND @EndUTCDT
                                        AND P.PaymentTransactionType = 2
                                    )
                    ;

                    INSERT INTO @Results
                        SELECT @LocationID, 3, 'MTD Payments', 1, ISNULL(@MoneyIn, 0) + IsNull(@MoneyOut, 0)
                    ;

                    -- Pull ID = 4 - Current A/Rs
                    -- Pull ID = 6 - Current Pre-WIP
                    -- Pull ID = 7 - Current WIP
                    -- Pull ID = 8 - Current Built
                    -- Pull ID = 9 - Current Invoicing
                    INSERT INTO @Results
                        SELECT @LocationID
                                , CASE OrderStatusID WHEN 21 THEN 6 WHEN 22 THEN 7 WHEN 23 THEN 8 WHEN 24 THEN 9 WHEN 25 THEN 4 END
                                , 'Current ' + CASE OrderStatusID WHEN 21 THEN 'Pre-WIP' WHEN 22 THEN 'WIP' WHEN 23 THEN 'Built' WHEN 24 THEN 'Invoicing' WHEN 25 THEN 'A/Rs' END
                                , TheCount
                                , TheSum
                        FROM 
                        (
                            SELECT OS.ID AS [OrderStatusID]
                                    , COUNT(O.ID) TheCount
                                    , SUM(O.[Price.PreTax]) TheSum 
                            FROM [Order.Data] O
                            RIGHT JOIN (Values (21), (22), (23), (24), (25)) AS OS(ID) ON O.OrderStatusID = OS.ID
                            WHERE (O.BID IS NULL)
                               OR (O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL))
                            GROUP BY OS.ID
                        ) AS Temp
                    ;

                    -- Pull ID = 5 - MTD Closed
                    INSERT INTO @Results
                        SELECT @LocationID, 5, 'MTD Closed', COUNT(*), SUM(O.[Price.PreTax])
                        FROM [Order.Data] O
                        WHERE O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL)
                        AND EXISTS( SELECT * 
                                    FROM [Order.KeyDate] OD 
                                    WHERE OD.BID = O.BID
                                        AND OD.OrderID = O.ID 
                                        AND OD.OrderItemID IS NULL
                                        AND OD.KeyDateType = 9 -- Closed
                                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                        )
                    ;

                    -- ---------------------------------------------

                    -- ---------------------------------------------
                    -- Now format the output as a table or JSON
                    -- ---------------------------------------------
                    IF (@AsTable = 1)
                        SELECT *
                        FROM @Results R
                        ORDER BY 2
                        ;

                    ELSE
                    BEGIN
                        SELECT *
                        FROM @Results R
                        ORDER BY 2
                        FOR JSON PATH
                        ;
                    END;
                    ---- ---------------------------------------------
                END

            ";
            #endregion

            var resultStringFromSQL = await GetFinancialSnapshotSQLResult(sqlExecute);

            sqlExecute = "EXEC [dbo].[Dashboard.Widget.Definition.006] @BID, @LocationID, @StartUTCDT, @EndUTCDT";

            var resultStringFromSPROC = await GetFinancialSnapshotSQLResult(sqlExecute);

            Assert.IsTrue(resultStringFromSQL.Equals(resultStringFromSPROC), "result from SQL is not equal to result from Stored Procedure");
        }

        private async Task<string> GetFinancialSnapshotSQLResult(string sqlExecute)
        {
            var ctx = GetApiContext();
            LocationData location = ctx.LocationData.First();
            var BID = TestConstants.BID;
            var locationID = location.ID;
            DateTime date = DateTime.UtcNow;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            var resultStringFromSQL = "";

            var result = await ctx.Database.ExecuteSqlRawAsync2(
                sqlExecute,
                CancellationToken.None,
                new SqlParameter("BID", BID),
                new SqlParameter("LocationID", locationID),
                new SqlParameter("StartUTCDT", firstDayOfMonth),
                new SqlParameter("EndUTCDT", lastDayOfMonth)
            );
            using (var reader = result.DbDataReader)
            {
                // get one value only
                if (await reader.ReadAsync())
                {
                    resultStringFromSQL = reader.GetString(0);
                }
            }
            Assert.IsFalse(String.IsNullOrEmpty(resultStringFromSQL), "result from sql query is null or empty");

            return resultStringFromSQL;
        }

        [TestMethod]
        public async Task CurrentEstimateStatusTest()
        {
            string sqlExecute = "";

            #region copy of SPROC

            // @BID, @LocationID, @StartUTCDT, @EndUTCDT must be declared
            // via sqlparameter
            sqlExecute = @"
                BEGIN
                    --  --- There are helpful for testing
                    -- DECLARE @BID SMALLINT = 1;
                    -- DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
                    -- DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
                    -- DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
                    DECLARE @AsTable BIT = 0;


                    --  Note
                    --     OrderStatusID = 11 for Pending Estimate      KeyDateType = 1 for Created
                    --                   = 12 for Approved (Won)                    = 14 for Approved
                    --                   = 18 for Lost                              = 3 for Lost
                    --                   = 19 for Voided                            = 2 for Voided
    
                    -- ---------------------------------------------
                    -- Pull the data
                    -- ---------------------------------------------
                    DECLARE   @Results Table 
                                ( 
                                      LocationID TINYINT
                                    , OrderStatusID TINYINT
                                    , Name VARCHAR(100)
                                    , Count INT
                                    , Amount DECIMAL(18,4)
                                );


                    INSERT INTO @Results
                        SELECT @LocationID, 11, 'Pending', COUNT(*), SUM(E.[Price.PreTax])
                        FROM [Order.Data] E
                        JOIN [enum.Order.OrderStatus] OS ON OS.ID = E.OrderStatusID
                        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
                        AND E.OrderStatusID = 11 -- Pending
                    ;

                    INSERT INTO @Results
                        SELECT @LocationID, 1, 'Created', COUNT(*), SUM(E.[Price.PreTax])
                        FROM [Order.Data] E
                        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
                        AND EXISTS( SELECT * 
                                    FROM [Order.KeyDate] OD 
                                    WHERE OD.BID = E.BID
                                        AND OD.OrderID = E.ID 
                                        AND OD.OrderItemID IS NULL
                                        AND OD.KeyDateType = 1 -- Created
                                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                        )
                    ;

                    INSERT INTO @Results
                        SELECT @LocationID, 12, 'Approved', COUNT(*), SUM(E.[Price.PreTax])
                        FROM [Order.Data] E
                        WHERE E.BID = @BID AND E.LocationID = ISNULL(@LocationID, E.LocationID)
                        AND E.OrderStatusID = 12
                        AND EXISTS( SELECT * 
                                    FROM [Order.KeyDate] OD 
                                    WHERE OD.BID = E.BID
                                        AND OD.OrderID = E.ID 
                                        AND OD.OrderItemID IS NULL
                                        AND OD.KeyDateType = 14
                                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                        )
                    ;

                    INSERT INTO @Results
                        SELECT @LocationID, 3, 'Lost', COUNT(*), SUM(E.[Price.PreTax])
                        FROM [Order.Data] E
                        WHERE E.BID = @BID AND E.LocationID = ISNULL(@LocationID, E.LocationID)
                        AND E.OrderStatusID = 18
                        AND EXISTS( SELECT * 
                                    FROM [Order.KeyDate] OD 
                                    WHERE OD.BID = E.BID
                                        AND OD.OrderID = E.ID 
                                        AND OD.OrderItemID IS NULL
                                        AND OD.KeyDateType = 3
                                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                        )
                    ;

                    INSERT INTO @Results
                        SELECT @LocationID, 19, 'Voided', COUNT(*), SUM(E.[Price.PreTax])
                        FROM [Order.Data] E
                        WHERE E.BID = @BID AND E.LocationID = ISNULL(@LocationID, E.LocationID)
                        AND E.OrderStatusID = 19
                        AND EXISTS( SELECT * 
                                    FROM [Order.KeyDate] OD 
                                    WHERE OD.BID = E.BID
                                        AND OD.OrderID = E.ID 
                                        AND OD.OrderItemID IS NULL
                                        AND OD.KeyDateType = 2
                                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                        )
                    ;

                    -- ---------------------------------------------

                    -- ---------------------------------------------
                    -- Now format the output as a table or JSON
                    -- ---------------------------------------------
                    IF (@AsTable = 1)
                        SELECT *
                        FROM @Results R
                        ORDER BY 1 DESC, 2
                        ;

                    ELSE
                    BEGIN
                        SELECT *
                        FROM @Results R
                        ORDER BY 1 DESC, 2
                        FOR JSON PATH
                        ;
                    END;
                    ---- ---------------------------------------------

                END

            ";
            #endregion

            var resultStringFromSQL = await GetCurrentEstimateStatusSQLResult(sqlExecute);

            sqlExecute = "EXEC [dbo].[Dashboard.Widget.Definition.007] @BID, @LocationID, @StartUTCDT, @EndUTCDT";

            var resultStringFromSPROC = await GetCurrentEstimateStatusSQLResult(sqlExecute);

            Assert.IsTrue(resultStringFromSQL.Equals(resultStringFromSPROC), "result from SQL is not equal to result from Stored Procedure");
        }

        private async Task<string> GetCurrentEstimateStatusSQLResult(string sqlExecute)
        {
            var ctx = GetApiContext();
            LocationData location = ctx.LocationData.First();
            var BID = TestConstants.BID;
            var locationID = location.ID;
            DateTime date = DateTime.UtcNow;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            var resultStringFromSQL = "";

            var result = await ctx.Database.ExecuteSqlRawAsync2(
                sqlExecute,
                CancellationToken.None,
                new SqlParameter("BID", BID),
                new SqlParameter("LocationID", locationID),
                new SqlParameter("StartUTCDT", firstDayOfMonth),
                new SqlParameter("EndUTCDT", lastDayOfMonth)
            );
            using (var reader = result.DbDataReader)
            {
                // get one value only
                if (await reader.ReadAsync())
                {
                    resultStringFromSQL = reader.GetString(0);
                }
            }
            Assert.IsFalse(String.IsNullOrEmpty(resultStringFromSQL), "result from sql query is null or empty");

            return resultStringFromSQL;
        }


        [TestMethod]
        public async Task CurrentOrderStatusTest()
        {
            string sqlExecute = "";

            #region copy of SPROC

            // @BID, @LocationID, and @StatusID must be declared
            // via sqlparameter
            sqlExecute = @"
                BEGIN

                    --  --- There are helpful for testing
                    --   DECLARE @BID SMALLINT = 1;
                    --   DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
                    --   DECLARE @StatusID VARCHAR(255) = '21, 22, 23, 24, 25';
                    DECLARE @AsTable BIT = 0;


                    --  Note
                    --     OrderStatusID = 21 for Pre-WIP      KeyDateType = 1 for Created
                    --                   = 22 for WIP                      = 5 for WIP
                    --                   = 23 for Built                    = 6 for Built
                    --                   = 24 for Invoicing                = 7 for Invoicing
                    --                   = 25 for Invoiced                 = 8 for Invoiced
    
                    -- ---------------------------------------------
                    -- Pull the data
                    -- ---------------------------------------------
                    SELECT @LocationID [LocationID], OSE.ID, OSE.Name, COUNT(O.ID) [Count], ISNULL(SUM(O.[Price.PreTax]),0.0) [Amount]

                    FROM [Order.Data] O

                    FULL OUTER JOIN 
                    ( SELECT CONVERT(TINYINT, value) [ID] from String_Split(@StatusID, ',') ) OS ON O.BID = @BID AND O.OrderStatusID = OS.ID

                    JOIN [enum.Order.OrderStatus] OSE ON OS.ID = OSE.ID

                    WHERE (@LocationID IS NULL OR O.LocationID IS NULL OR O.LocationID = @LocationID)
                    GROUP BY OSE.ID, OSE.Name
                    ORDER BY 1 DESC, 2
                    FOR JSON PATH
                    ;

                END
            ";
            #endregion

            var resultStringFromSQL = await GetCurrentOrderStatusSQLResult(sqlExecute);

            sqlExecute = "EXEC [dbo].[Dashboard.Widget.Definition.004] @BID, @LocationID, @StatusID";

            var resultStringFromSPROC = await GetCurrentOrderStatusSQLResult(sqlExecute);

            ;
            Assert.IsTrue(resultStringFromSQL.Equals(resultStringFromSPROC), "result from SQL is not equal to result from Stored Procedure");
        }

        private async Task<string> GetCurrentOrderStatusSQLResult(string sqlExecute)
        {
            var ctx = GetApiContext();
            LocationData location = ctx.LocationData.First();
            var BID = TestConstants.BID;
            var locationID = location.ID;
            var statusIDCSV = "21, 22";
            var resultStringFromSQL = "";

            var result = await ctx.Database.ExecuteSqlRawAsync2(
                sqlExecute,
                CancellationToken.None,
                new SqlParameter("BID", BID),
                new SqlParameter("LocationID", locationID),
                new SqlParameter("StatusID", statusIDCSV)
            );
            using (var reader = result.DbDataReader)
            {
                // get one value only
                if (await reader.ReadAsync())
                {
                    resultStringFromSQL = reader.GetString(0);
                }
            }
            Assert.IsFalse(String.IsNullOrEmpty(resultStringFromSQL), "result from sql query is null or empty");

            return resultStringFromSQL;
        }
    }
}
