﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Endor.Api.Web.Classes;
using Endor.Pricing;
using Microsoft.EntityFrameworkCore;
using Endor.CBEL.Elements;
using Endor.EF;

namespace Endor.Api.Tests
{

    [TestClass]
    public class OrderItemComponentEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/orderitemcomponent";
        public const string apiItemUrl = "/api/orderitem";
        public const string apiOrderUrl = "/api/order";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public ApiContext ctx { get; set; }

        [TestInitialize]
        public async Task Initialize()
        {
            ctx = GetApiContext();
            var PartAssembliesToRemove = ctx.AssemblyData.Where(x => x.Name == "TestAssembly" && x.AssemblyType == AssemblyType.Product && x.BID == 1).ToList();
            var AssemblyIDs = PartAssembliesToRemove.Select(y => y.ID);
            ctx.RemoveRange(ctx.AssemblyTable.Where(x => x.BID == 1 && AssemblyIDs.Contains(x.AssemblyID)));
            ctx.RemoveRange(ctx.AssemblyVariable.Where(x => x.BID == 1 && AssemblyIDs.Contains(x.AssemblyID)));
            await ctx.SaveChangesAsync();
            ctx.RemoveRange(PartAssembliesToRemove);
            await ctx.SaveChangesAsync();

        }

        [TestMethod]
        [TestCategory("GL")]
        public async Task TestOrderItemComponentCRUDOperations()
        {
            #region CREATE ORDER AND ORDERITEM FIRST
            var testOrder = Utils.GetTestOrderData();
            testOrder.CompanyID = (await EndpointTests.AssertGetStatusCode<SimpleCompanyData[]>(client, "/api/company/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.PickupLocationID = (await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.TaxGroupID = (await EndpointTests.AssertGetStatusCode<SimpleTaxGroup[]>(client, "/api/taxgroup/simplelist", HttpStatusCode.OK)).FirstOrDefault().ID;
            testOrder.ProductionLocationID = testOrder.PickupLocationID;
            var createdOrder = await EndpointTests.AssertPostStatusCode<OrderData>(client, $"{apiOrderUrl}/", JsonConvert.SerializeObject(testOrder), HttpStatusCode.OK);

            var testOrderItem = Utils.GetTestOrderItemData(createdOrder);
            var createdOrderItem = await EndpointTests.AssertPostStatusCode<OrderItemData>(client, $"{apiItemUrl}/", JsonConvert.SerializeObject(testOrderItem), HttpStatusCode.OK);

            #endregion

            #region CREATE
            var incomeAccounts = ctx.GLAccount.Where(account => account.GLAccountType == 40).ToList(); //Income

            var testOrderItemComponent = Utils.GetTestOrderItemComponentData(createdOrderItem, incomeAccounts.First());
            testOrderItemComponent.TaxInfoList = new List<TaxAssessmentResult>()
            { 
                new TaxAssessmentResult()
                {
                    TaxGroupID = ctx.TaxGroup.First().ID,
                    TaxAmount = 1,
                    PriceTaxable = 10,
                    TaxRate = 0.1m,
                    InvoiceText = "TEst",
                    Items = new List<TaxAssessmentItemResult>()
                    {
                        new TaxAssessmentItemResult()
                        {
                            TaxItemID = ctx.TaxItem.First().ID,
                            InvoiceText = "Test",
                            TaxRate=0.1m,
                            TaxAmount = 1,
                            GLAccountID = ctx.GLAccount.First().ID
                        }
                    }
                }
            };

            var createdOrderItemComponent = await EndpointTests.AssertPostStatusCode<OrderItemComponent>(client, $"{apiUrl}/", JsonConvert.SerializeObject(testOrderItemComponent), HttpStatusCode.OK);
            Assert.AreEqual(ctx.OrderItemTaxAssessment.Where(x => x.ItemComponentID == createdOrderItemComponent.ID).Count(), 1);
            #endregion

            #region RETRIEVE

            // Test Get By ID
            var retrievedOrderItemComponent = await EndpointTests.AssertGetStatusCode<OrderItemComponent>(client, $"{apiUrl}/{createdOrderItemComponent.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedOrderItemComponent);
            Assert.AreEqual(retrievedOrderItemComponent.TaxInfoList.Count,1);
            Assert.AreEqual(retrievedOrderItemComponent.TaxInfoList.First().Items.Count, 1);


            #endregion

            #region UPDATE

            createdOrderItemComponent.Description = "TEST DESCRIPTION";
            var testPut = await EndpointTests.AssertPutStatusCode<OrderItemComponent>(client, $"{apiUrl}/{createdOrderItemComponent.ID}", JsonConvert.SerializeObject(createdOrderItemComponent), HttpStatusCode.OK);
            Assert.IsNotNull(testPut);
            Assert.AreEqual("TEST DESCRIPTION", testPut.Description);
            Assert.AreEqual(ctx.OrderItemTaxAssessment.Where(x => x.ItemComponentID == createdOrderItemComponent.ID).Count(), 1);

            #endregion

            #region DELETE

            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdOrderItemComponent.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiItemUrl}/{createdOrderItem.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/order/{createdOrder.ID}", HttpStatusCode.NoContent);
            Assert.AreEqual(ctx.OrderItemTaxAssessment.Where(x => x.ItemComponentID == createdOrderItemComponent.ID).Count(), 0);

            #endregion
        }

    }
}
