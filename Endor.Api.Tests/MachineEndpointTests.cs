﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using Endor.Tenant;
using Endor.Api.Web.Classes;
using System.Xml;
using Newtonsoft.Json.Linq;
using Endor.CBEL.Common;
using Endor.DocumentStorage.Models;
using Endor.CBEL.Interfaces;
using Endor.Logging.Client;

namespace Endor.Api.Tests
{
    [TestClass]
    public class MachineEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/machinepart";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        private string NewTestMachineName;
        private int testMachineTemplateID;
        private byte testLocationID;
        private short tableID;
        private short testBID = 1;
        public TestContext TestContext { get; set; }

        private async Task<MachineData> GetNewModel()
        {
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleMachineData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;

            var simpleTaxCodes = await EndpointTests.AssertGetStatusCode<SimpleTaxabilityCode[]>(client, $"/api/taxcode/SimpleList", HttpStatusCode.OK);
            short taxCodeID = simpleTaxCodes.FirstOrDefault().ID;

            return new MachineData()
            {
                Name = this.NewTestMachineName,
                IncomeAccountID = glAccountID,
                ExpenseAccountID = glAccountID,
                TaxCodeID = taxCodeID,
                IsActive = true,
                BID = testBID,
                MachineTemplateID = this.testMachineTemplateID,
                ActiveInstanceCount = 0,
                ActiveProfileCount = 0,
                Description = "Description",
                HasImage = false,
                SKU = "SKU",
                WorksheetData = "[]",
                Instances = new List<MachineInstance>
                {
                    GetTestMachineInstance()
                },
                Profiles = new List<MachineProfile>
                {
                    GetTestMachineProfile()
                }
            };
        }

        private MachineInstance GetTestMachineInstance()
        {
            return new MachineInstance
            {
                IsActive = true,
                Name = this.NewTestMachineName + " - Instance",
                LocationID = this.testLocationID,
                Manufacturer = "Manufacturer",
                SerialNumber = "SerialNumber",
                IPAddress = "IPAddress",
                PurchaseDate = DateTime.Now,
                HasServiceContract = true,
                ContractNumber = "",
                ContractStartDate = DateTime.Now,
                ContractEndDate = DateTime.Now,
                ContractInfo = "ContractInfo",
                Model = "Model",
                BID = testBID
            };
        }

        private MachineProfile GetTestMachineProfile()
        {



            return new MachineProfile
            {
                IsActive = true,
                IsDefault = true,
                Name = this.NewTestMachineName + " - Profile",
                MachineTemplateID = this.testMachineTemplateID,
                AssemblyOVDataJSON = "{}",
                BID = testBID,

                MachineProfileTables = new List<MachineProfileTable>
                {
                    GetTestMachineProfileTable()
                }
            };
        }

        private MachineProfileTable GetTestMachineProfileTable()
        {
            var ctx = GetApiContext();

            return new MachineProfileTable
            {
                BID = -99,
                ProfileID = -99,
                TableID = ctx.AssemblyTable.FirstOrDefault().ID,
                Label = "Assembly table 1",
                OverrideDefault = false
            };
        }

        [TestInitialize]
        public async Task Initialize()
        {
            this.NewTestMachineName = DateTime.UtcNow + " TEST MACHINE DATA";

            var assemblies = await EndpointTests.AssertGetStatusCode<AssemblyData[]>(client, $"/api/assemblypart/", HttpStatusCode.OK);
            var tables = assemblies.FirstOrDefault(e => e.Tables != null && e.AssemblyType == AssemblyType.MachineTemplate);
            this.testMachineTemplateID = assemblies.FirstOrDefault(e => e.AssemblyType == AssemblyType.MachineTemplate)?.ID ?? 0;

            var locations = await EndpointTests.AssertGetStatusCode<SimpleLocationData[]>(client, "/api/location/simplelist", HttpStatusCode.OK);
            this.testLocationID = locations.FirstOrDefault().ID;
        }

        [TestMethod]
        public async Task TestMachineCRUD()
        {
            MachineData itemTest = await GetNewModel();

            // NOTE: END-6965 - Removing Profile and Instance validation for now

            //// test validation
            //var tempIncomeAccountID = itemTest.IncomeAccountID;
            //itemTest.IncomeAccountID = 0;

            //await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.BadRequest);
            //itemTest.IncomeAccountID = tempIncomeAccountID;

            //var tempExpenseAccountID = itemTest.ExpenseAccountID;
            ////itemTest.ExpenseAccountID = 0;
            //await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.BadRequest);
            //itemTest.ExpenseAccountID = tempExpenseAccountID;

            //var tempTaxCodeID = itemTest.TaxCodeID;
            ////itemTest.TaxCodeID = 0;
            //await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.BadRequest);
            //itemTest.TaxCodeID = tempTaxCodeID;

            //var tempMachineTemplateID = itemTest.MachineTemplateID;
            ////itemTest.MachineTemplateID = 0;
            //await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.BadRequest);
            //itemTest.MachineTemplateID = tempMachineTemplateID;

            //// dissallow same name within instance
            //var tempInstanceClone = GetTestMachineInstance();
            //itemTest.Instances.Add(tempInstanceClone);
            //await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.BadRequest);
            //tempInstanceClone.Name = " - Clone";

            //var tempLocationID = tempInstanceClone.LocationID;
            ////tempInstanceClone.LocationID = 0;
            //await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.BadRequest);
            //tempInstanceClone.LocationID = tempLocationID;

            //// dissallow same name within profiles
            //var tempProfileClone = GetTestMachineProfile();
            //itemTest.Profiles.Add(tempProfileClone);
            //await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.BadRequest);
            //tempProfileClone.Name = " - Clone";

            //// dissallow 2 or more is default profiles
            //await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.BadRequest);
            //tempProfileClone.IsDefault = false;

            // test post
            var machineData = await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(machineData);

            // test get
            var list = await EndpointTests.AssertGetStatusCode<List<MachineData>>(client, apiUrl, HttpStatusCode.OK);
            Assert.IsTrue(list.Count > 0);

            // test put
            machineData.Name += "Updated";
            machineData = await EndpointTests.AssertPutStatusCode<MachineData>(client, $"{ apiUrl}/{machineData.ID}", JsonConvert.SerializeObject(machineData), HttpStatusCode.OK);
            Assert.IsNotNull(machineData);

            // test delete
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{machineData.ID}/action/candelete", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{machineData.ID}", HttpStatusCode.NoContent);

            // confirm delete
            await EndpointTests.AssertGetStatusCode<MachineData>(client, $"{ apiUrl}/{machineData.ID}", HttpStatusCode.BadRequest);
        }

        [TestMethod]
        public async Task TestMachineProfileTable()
        {
            var ctx = GetApiContext();

            AssemblyData model = new AssemblyData()
            {
                Name = "TEST_ASSEMBLY_DATA_" + DateTime.UtcNow,
                IsActive = true,
                BID = TestConstants.BID,
            };

            model.Tables = new List<AssemblyTable>()
            {
                new AssemblyTable()
                {
                    BID = TestConstants.BID,
                    Label = "Test.AssemblyTable",
                    TableType = AssemblyTableType.Custom,
                    CellDataJSON = "[]",
                    VariableName = "TestAssemblyTable",
                    CellDataType = DataType.Number,
                    ColumnCount = 1,
                    ColumnDataType = DataType.Number,
                    ColumnIsSorted = false,
                    ColumnLabel = "Tier",
                    ColumnMatchType = AssemblyTableMatchType.ExactMatch,
                    ColumnUnitID = 0,
                    ColumnValuesJSON = "[{\"index\":0,\"value\":1,\"name\":\"Default\"}]",
                    Description = "",
                    ModifiedDT = DateTime.Parse("2019-04-24T16:07:46.7515121"),
                    RowCount = 3,
                    RowDataType = DataType.Number,
                    RowDefaultMarkupJSON = "",
                    RowIsSorted = false,
                    RowLabel = "Row",
                    RowMatchType = 0,
                    RowUnitID = 0,
                    RowValuesJSON = "[{\"index\":0,\"value\":100}]",
                    IsTierTable = true,
                    ClassTypeID = 12047
                }
            };

            //CREATE
            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, "/api/assemblypart", JsonConvert.SerializeObject(model), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);

            this.tableID = assemblyData.Tables.FirstOrDefault().ID;


            MachineData itemTest = await GetNewModel();

            // test post
            var machineData = await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(machineData);

            // test get
            var list = await EndpointTests.AssertGetStatusCode<List<MachineData>>(client, apiUrl, HttpStatusCode.OK);
            var item = list.Where(x => x.ID == machineData.ID).First();
            Assert.IsNotNull(item.Profiles.First().MachineProfileTables);
            Assert.IsTrue(list.Count > 0);

            // test put
            machineData.Name = this.NewTestMachineName + "Updated";
            machineData = await EndpointTests.AssertPutStatusCode<MachineData>(client, $"{ apiUrl}/{machineData.ID}", JsonConvert.SerializeObject(machineData), HttpStatusCode.OK);
            Assert.IsNotNull(machineData);

            // test put failures
            //var table = machineData.Profiles.First().MachineProfileTables.First();
            //table.OverrideDefault = true;
            //table.RowValuesJSON = "[{\"index\":0,\"value\":100,\"value\":100}]";
            //await EndpointTests.AssertPutStatusCode<MachineData>(client, $"{ apiUrl}/{machineData.ID}", JsonConvert.SerializeObject(machineData), HttpStatusCode.BadRequest);

            // test delete
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{machineData.ID}/action/candelete", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{machineData.ID}", HttpStatusCode.NoContent);

            await EndpointTests.AssertDeleteStatusCode(client, $"/api/assemblypart/{assemblyData.ID}", HttpStatusCode.NoContent);
            Assert.IsFalse(ctx.AssemblyData.Any(x => x.ID == assemblyData.ID));
        }

        [TestMethod]
        public async Task TestMachineAssemblyCSandDLL()
        {
            var ctx = GetApiContext();
            MachineData itemTest = await GetNewModel();

            ITenantDataCache cache = this.GetMockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            // test post
            var machineData = await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(machineData);

            (ICBELAssembly assembly, string assemblyCode) = await CBELAssemblyHelper.LoadAssemblyFromStorage(machineData.ID, ClassType.Machine.ID(), 1, cache, logger, ctx, TestConstants.BID);
            Assert.IsNotNull(assembly);
            Assert.AreEqual(assembly.ClassTypeID, ClassType.Machine.ID());

            // test delete
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{machineData.ID}/action/candelete", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{machineData.ID}", HttpStatusCode.NoContent);
        }

        [TestMethod]
        public async Task TestMachinesWithTemplateID()
        {
            var ctx = GetApiContext();

            // Create 2 Machine Category
            MachineCategory machineCategory1 = new MachineCategory()
            {
                Name = DateTime.UtcNow + " TEST MACHINE CATEGORY 1",
                IsActive = true,
                BID = this.testBID,
            };

            MachineCategory machineCategory2 = new MachineCategory()
            {
                Name = DateTime.UtcNow + " TEST MACHINE CATEGORY 2",
                IsActive = true,
                BID = this.testBID,
            };
            
            var newCategory1 = await EndpointTests.AssertPostStatusCode<MachineCategory>(client, $"/api/machinecategory", JsonConvert.SerializeObject(machineCategory1), HttpStatusCode.OK);
            Assert.IsNotNull(newCategory1);
            var newCategory2 = await EndpointTests.AssertPostStatusCode<MachineCategory>(client, $"/api/machinecategory", JsonConvert.SerializeObject(machineCategory2), HttpStatusCode.OK);
            Assert.IsNotNull(newCategory2);

            // Create 2 Machine Template
            AssemblyData machineTemplate1 = new AssemblyData()
            {
                BID = this.testBID,
                Name = DateTime.UtcNow + " TEST MACHINE TEMPLATE 1",
                AssemblyType = AssemblyType.MachineTemplate,
                MachineLayoutTypes = (MachineLayoutType)3
            };

            AssemblyData machineTemplate2 = new AssemblyData()
            {
                BID = this.testBID,
                Name = DateTime.UtcNow + " TEST MACHINE TEMPLATE 2",
                AssemblyType = AssemblyType.MachineTemplate,
                MachineLayoutTypes = (MachineLayoutType)3
            };

            var newMachineTemplate1 = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, $"/api/assemblypart", JsonConvert.SerializeObject(machineTemplate1), HttpStatusCode.OK);
            Assert.IsNotNull(newMachineTemplate1);
            var newMachineTemplate2 = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, $"/api/assemblypart", JsonConvert.SerializeObject(machineTemplate2), HttpStatusCode.OK);
            Assert.IsNotNull(newMachineTemplate2);

            // Create 2 Machines with a MachineTemplateID of machineTemplate1
            var simpleGLAccounts = await EndpointTests.AssertGetStatusCode<SimpleMachineData[]>(client, $"/api/glaccounts/SimpleList", HttpStatusCode.OK);
            int glAccountID = simpleGLAccounts.FirstOrDefault().ID;

            var simpleTaxCodes = await EndpointTests.AssertGetStatusCode<SimpleTaxabilityCode[]>(client, $"/api/taxcode/SimpleList", HttpStatusCode.OK);
            short taxCodeID = simpleTaxCodes.FirstOrDefault().ID;

            var machineData1 = new MachineData()
            {
                Name = DateTime.UtcNow + " TEST MACHINE 1",
                IncomeAccountID = glAccountID,
                ExpenseAccountID = glAccountID,
                TaxCodeID = taxCodeID,
                IsActive = true,
                BID = testBID,
                MachineTemplateID = newMachineTemplate1.ID,
                ActiveInstanceCount = 0,
                ActiveProfileCount = 0,
                Description = "Description",
                HasImage = false,
                SKU = "SKU",
                WorksheetData = "[]",
                Instances = new List<MachineInstance>
                {
                    GetTestMachineInstance()
                },
                Profiles = new List<MachineProfile>
                {
                    GetTestMachineProfile()
                }
            };

            var machineData2 = new MachineData()
            {
                Name = DateTime.UtcNow + " TEST MACHINE 2",
                IncomeAccountID = glAccountID,
                ExpenseAccountID = glAccountID,
                TaxCodeID = taxCodeID,
                IsActive = false,
                BID = testBID,
                MachineTemplateID = newMachineTemplate1.ID,
                ActiveInstanceCount = 0,
                ActiveProfileCount = 0,
                Description = "Description",
                HasImage = false,
                SKU = "SKU",
                WorksheetData = "[]",
                Instances = new List<MachineInstance>
                {
                    GetTestMachineInstance()
                },
                Profiles = new List<MachineProfile>
                {
                    GetTestMachineProfile()
                }
            };

            var newMachineData1 = await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(machineData1), HttpStatusCode.OK);
            Assert.IsNotNull(newMachineData1);
            var newMachineData2 = await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(machineData2), HttpStatusCode.OK);
            Assert.IsNotNull(newMachineData2);

            // Link the machineData1 with machineCategory1
            EntityActionChangeResponse linkResponse;
            linkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"api/machinecategory/{newCategory1.ID}/action/linkmachine/{newMachineData1.ID}", "", HttpStatusCode.OK);
            Assert.IsTrue(linkResponse.Success);

            // ====================== Test the /api/machinepart/list/template/{templateID} endpoint ======================
            // Get Machines with MachineTemplate of machineTemplate1
            var machinesWithTemplateID = await EndpointTests.AssertGetStatusCode<Object>(client, $"{apiUrl}/list/template/{newMachineTemplate1.ID}", HttpStatusCode.OK);
            string str = JsonConvert.SerializeObject(machinesWithTemplateID);
            JObject json = JObject.Parse(str);
            // Expected result: 1 Category
            Assert.AreEqual(1, json["Categories"].Count());
            // Expected result: 1 Machines
            Assert.AreEqual(1, json["Machines"].Count());

            // Get Machines with MachineTemplate of machineTemplate1 includes inactive machines
            machinesWithTemplateID = await EndpointTests.AssertGetStatusCode<Object>(client, $"{apiUrl}/list/template/{newMachineTemplate1.ID}?IncludeInactive=true", HttpStatusCode.OK);
            str = JsonConvert.SerializeObject(machinesWithTemplateID);
            json = JObject.Parse(str);
            // Expected result: 1 Category
            Assert.AreEqual(1, json["Categories"].Count());
            // Expected result: 2 Machines
            Assert.AreEqual(2, json["Machines"].Count());

            // Get Machines with MachineTemplate of machineTemplate1 that has MachineCategory of machineCategory1
            machinesWithTemplateID = await EndpointTests.AssertGetStatusCode<Object>(client, $"{apiUrl}/list/template/{newMachineTemplate1.ID}?CategoryID={newCategory1.ID}", HttpStatusCode.OK);
            str = JsonConvert.SerializeObject(machinesWithTemplateID);
            json = JObject.Parse(str);
            // Expected result: 1 Category
            Assert.AreEqual(1, json["Categories"].Count());
            // Expected result: 1 Machines
            Assert.AreEqual(1, json["Machines"].Count());

            // Get Machines with MachineTemplate of machineTemplate1 that has MachineCategory of machineCategory1
            machinesWithTemplateID = await EndpointTests.AssertGetStatusCode<Object>(client, $"{apiUrl}/list/template/{newMachineTemplate1.ID}?CategoryID={newCategory1.ID}", HttpStatusCode.OK);
            str = JsonConvert.SerializeObject(machinesWithTemplateID);
            json = JObject.Parse(str);
            // Expected result: 1 Category
            Assert.AreEqual(1, json["Categories"].Count());
            // Expected result: 1 Machines
            Assert.AreEqual(1, json["Machines"].Count());

            // Get Machines with MachineTemplate of machineTemplate1 that has MachineCategory of machineCategory1, includes machineData1 and machineData2
            machinesWithTemplateID = await EndpointTests.AssertGetStatusCode<Object>(client, $"{apiUrl}/list/template/{newMachineTemplate1.ID}?CategoryID={newCategory1.ID}&MachineID={newMachineData1.ID}&MachineID={newMachineData2.ID}", HttpStatusCode.OK);
            str = JsonConvert.SerializeObject(machinesWithTemplateID);
            json = JObject.Parse(str);
            // Expected result: 1 Category
            Assert.AreEqual(1, json["Categories"].Count());
            // Expected result: 2 Machines
            Assert.AreEqual(2, json["Machines"].Count());

            // Get Machines with MachineTemplate of machineTemplate1 that has MachineCategory of machineCategory2
            await EndpointTests.AssertGetStatusCode<Object>(client, $"{apiUrl}/list/template/{newMachineTemplate1.ID}?CategoryID={newCategory2.ID}", HttpStatusCode.NotFound);

            // Get Machines with MachineTemplate of machineTemplate1 that has MachineCategory of machineCategory2, includes machineData1 and machineData2
            machinesWithTemplateID = await EndpointTests.AssertGetStatusCode<Object>(client, $"{apiUrl}/list/template/{newMachineTemplate1.ID}?CategoryID={newCategory2.ID}&MachineID={newMachineData1.ID}&MachineID={newMachineData2.ID}", HttpStatusCode.OK);
            str = JsonConvert.SerializeObject(machinesWithTemplateID);
            json = JObject.Parse(str);
            // Expected result: Category is null
            Assert.IsNull(json["Categories"]);
            // Expected result: 2 Machines
            Assert.AreEqual(2, json["Machines"].Count());

            // Get Machines with MachineTemplate of machineTemplate2
            await EndpointTests.AssertGetStatusCode<Object>(client, $"{apiUrl}/list/template/{newMachineTemplate2.ID}", HttpStatusCode.NotFound);

            // Get Machines with MachineTemplate of machineTemplate2 that has MachineCategory of machineCategory2, includes machineData1 and machineData2
            machinesWithTemplateID = await EndpointTests.AssertGetStatusCode<Object>(client, $"{apiUrl}/list/template/{newMachineTemplate2.ID}?CategoryID={newCategory2.ID}&MachineID={newMachineData1.ID}&MachineID={newMachineData2.ID}", HttpStatusCode.NotFound);

            // ================================================= Cleanup =================================================
            // Unlink the machineData1 with machineCategory1
            EntityActionChangeResponse unlinkResponse;
            unlinkResponse = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"api/machinecategory/{newCategory1.ID}/action/unlinkmachine/{newMachineData1.ID}", HttpStatusCode.OK);
            Assert.IsTrue(unlinkResponse.Success);

            // Delete the 2 Machine Category
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/machinecategory/{newCategory1.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/machinecategory/{newCategory2.ID}", HttpStatusCode.NoContent);

            // Delete the 2 Machine
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{newMachineData1.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{newMachineData2.ID}", HttpStatusCode.NoContent);

            // Delete the 2 Machine Template
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/assemblypart/{newMachineTemplate1.ID}", HttpStatusCode.NoContent);
            await EndpointTests.AssertDeleteStatusCode(client, $"/api/assemblypart/{newMachineTemplate2.ID}", HttpStatusCode.NoContent);

        }

        public async Task CleanupClonedMachine()
        {
            var ctx = GetApiContext();

            bool lamdaExp(MachineData t) => t.Description == "XMachineDescriptionTest";
            var machines = ctx.MachineData.Where(lamdaExp).ToList();
            var machineInstance = ctx.MachineInstance.Where( t => machines.Select(y => y.ID).Contains(t.MachineID)).ToList();
            var machineProfile = ctx.MachineProfile.Where(t => machines.Select(y => y.ID).Contains(t.MachineID)).ToList();
            var machineProfileTable = ctx.MachineProfileTable.Where( t => machineProfile.Select( y => y.ID).Contains(t.ProfileID)).ToList();
            var machineLinks = ctx.MachineCategoryLink.Where(t => ctx.MachineCategoryLink.Select(y => y.PartID).Contains(t.PartID)).ToList();
            var categoryIDs = machineLinks.Select(t => t.CategoryID).ToList();
            var machineIds = machines.Select(t => t.ID).ToList();

            ctx.MachineCategoryLink.RemoveRange(machineLinks);

            ctx.MachineProfileTable.RemoveRange(machineProfileTable);
            ctx.MachineProfile.RemoveRange(machineProfile);
            ctx.MachineInstance.RemoveRange(machineInstance);


            ctx.CustomFieldDefinition.RemoveRange(ctx.CustomFieldDefinition.Where(t => t.AppliesToID != null && (t.AppliesToClassTypeID == (int)ClassType.Machine && machineIds.Contains((short)t.AppliesToID))));
            ctx.CustomFieldOtherData.RemoveRange(ctx.CustomFieldOtherData.Where(t => t.AppliesToClassTypeID == (int)ClassType.Machine && machineIds.Contains((short)t.ID)));

            ctx.SaveChanges();

            ctx.MachineCategory.RemoveRange(ctx.MachineCategory.Where(t => categoryIDs.Contains(t.ID)));
            ctx.MachineData.RemoveRange(machines);

            await ctx.SaveChangesAsync();

            foreach (var machine in machines)

            {
                await EndpointTests.AssertPostStatusCode(client, $"/api/dm/delete/{(int)ClassType.Machine}/{machine.ID}", null, HttpStatusCode.OK);
            }


        }


        [TestMethod]
        public async Task TestMachineDataClone()
        {
            var ctx = GetApiContext();
            //await CleanupClonedMachine();

            var machineName = "Machine Category - " + new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();

            var machineCategory = new MachineCategory()
            {
                BID = 1,
                Name = machineName + " Category",
                IsActive = true
            };


            AssemblyData machineTemplate1 = new AssemblyData()
            {
                BID = this.testBID,
                Name = DateTime.UtcNow + " TEST MACHINE TEMPLATE 1",
                AssemblyType = AssemblyType.MachineTemplate,
                MachineLayoutTypes = (MachineLayoutType)3
            };

            var newMachineTemplate1 = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, $"/api/assemblypart", JsonConvert.SerializeObject(machineTemplate1), HttpStatusCode.OK);
            Assert.IsNotNull(newMachineTemplate1);

            // machine category endpoint
            var category = await EndpointTests.AssertPostStatusCode<MachineCategory>(client, $"/api/machinecategory", JsonConvert.SerializeObject(machineCategory), HttpStatusCode.OK);

            var simpleTaxCodes = await EndpointTests.AssertGetStatusCode<SimpleTaxabilityCode[]>(client, $"/api/taxcode/SimpleList", HttpStatusCode.OK);
            short taxCodeID = simpleTaxCodes.FirstOrDefault().ID;


            var machineData = new MachineData()
            {
                Name = machineName,
                IsActive = true,
                BID = 1,
                Description = "XMachineDescriptionTest",
                ExpenseAccountID = ctx.GLAccount.First().ID,
                IncomeAccountID = ctx.GLAccount.First().ID,
                HasImage = false,
                SKU = "SKU",
                WorksheetData = "[]",
                MachineTemplateID = newMachineTemplate1.ID,
                TaxCodeID = taxCodeID,
                Instances = new List<MachineInstance>
                {
                    GetTestMachineInstance()
                },
                Profiles = new List<MachineProfile>
                {
                    GetTestMachineProfile()
                }
            };


            // machine endpoint
            var machine = await EndpointTests.AssertPostStatusCode<MachineCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(machineData), HttpStatusCode.OK);

            var linkMachine = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"/api/machinecategory/{category.ID}/action/linkmachine/{machine.ID}", null, HttpStatusCode.OK);


            // cloning of material
            var cloned = await EndpointTests.AssertPostStatusCode<MachineCategory>(client, $"{apiUrl}/{machine.ID}/clone", null, HttpStatusCode.OK);

            Assert.AreEqual($"{machineName} (Clone)", cloned.Name);

            var clonedLinks = ctx.MachineCategoryLink.FirstOrDefault(t => t.PartID == cloned.ID);

            Assert.IsNotNull(clonedLinks);

            await CleanupClonedMachine();
        }

        [TestMethod]
        public async Task TestMachineDataCloneFiles()
        {
            var ctx = GetApiContext();
            //await CleanupClonedMachine();

            var machineName = "Machine Category - " + new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();

            var machineCategory = new MachineCategory()
            {
                BID = 1,
                Name = machineName + " Category",
                IsActive = true
            };


            AssemblyData machineTemplate1 = new AssemblyData()
            {
                BID = this.testBID,
                Name = DateTime.UtcNow + " TEST MACHINE TEMPLATE 1",
                AssemblyType = AssemblyType.MachineTemplate,
                MachineLayoutTypes = (MachineLayoutType)3
            };

            var newMachineTemplate1 = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, $"/api/assemblypart", JsonConvert.SerializeObject(machineTemplate1), HttpStatusCode.OK);
            Assert.IsNotNull(newMachineTemplate1);

            // machine category endpoint
            var category = await EndpointTests.AssertPostStatusCode<MachineCategory>(client, $"/api/machinecategory", JsonConvert.SerializeObject(machineCategory), HttpStatusCode.OK);

            var simpleTaxCodes = await EndpointTests.AssertGetStatusCode<SimpleTaxabilityCode[]>(client, $"/api/taxcode/SimpleList", HttpStatusCode.OK);
            short taxCodeID = simpleTaxCodes.FirstOrDefault().ID;


            var machineData = new MachineData()
            {
                Name = machineName,
                IsActive = true,
                BID = 1,
                Description = "XMachineDescriptionTest",
                ExpenseAccountID = ctx.GLAccount.First().ID,
                IncomeAccountID = ctx.GLAccount.First().ID,
                HasImage = false,
                SKU = "SKU",
                WorksheetData = "[]",
                MachineTemplateID = newMachineTemplate1.ID,
                TaxCodeID = taxCodeID,
                Instances = new List<MachineInstance>
                {
                    GetTestMachineInstance()
                },
                Profiles = new List<MachineProfile>
                {
                    GetTestMachineProfile()
                }
            };


            // machine endpoint
            var machine = await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(machineData), HttpStatusCode.OK);


            var fooTextFileName = "foo.txt";
            var filePathFragment = $"{(int)ClassType.Machine}/{machine.ID}/{fooTextFileName}";
            var someform = this.GetMultipartFormDataContent("foo", fooTextFileName);


            await AssertPostFileStatusCode(client, $"/api/dm/{Bucket.Documents}/{filePathFragment}", someform, HttpStatusCode.OK);

            var cloned = await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}/{machine.ID}/clone", null, HttpStatusCode.OK);

            var dm = GetDocumentManager(machine.BID, machine.ID, ClassType.Machine, Web.BucketRequest.Documents);
            var createdDocuments = await dm.GetDocumentsAsync();
            var cloneDm = GetDocumentManager(cloned.BID, cloned.ID, ClassType.Machine, Web.BucketRequest.Documents);
            var clonedDocuments = await cloneDm.GetDocumentsAsync();

            Assert.AreEqual(createdDocuments.Count, clonedDocuments.Count);
            Assert.AreEqual(createdDocuments[0].Name, clonedDocuments[0].Name);

            await CleanupClonedMachine();

        }


        [TestMethod]
        public async Task TestMachineDataCloneWithCustomFieldDefinition()
        {
            var ctx = GetApiContext();
            //await CleanupClonedMachine();

            var machineName = "Machine Category - " + new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();

            var machineCategory = new MachineCategory()
            {
                BID = 1,
                Name = machineName + " Category",
                IsActive = true
            };


            AssemblyData machineTemplate1 = new AssemblyData()
            {
                BID = this.testBID,
                Name = DateTime.UtcNow + " TEST MACHINE TEMPLATE 1",
                AssemblyType = AssemblyType.MachineTemplate,
                MachineLayoutTypes = (MachineLayoutType)3
            };

            var newMachineTemplate1 = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, $"/api/assemblypart", JsonConvert.SerializeObject(machineTemplate1), HttpStatusCode.OK);
            Assert.IsNotNull(newMachineTemplate1);

            // machine category endpoint
            var category = await EndpointTests.AssertPostStatusCode<MachineCategory>(client, $"/api/machinecategory", JsonConvert.SerializeObject(machineCategory), HttpStatusCode.OK);

            var simpleTaxCodes = await EndpointTests.AssertGetStatusCode<SimpleTaxabilityCode[]>(client, $"/api/taxcode/SimpleList", HttpStatusCode.OK);
            short taxCodeID = simpleTaxCodes.FirstOrDefault().ID;


            var machineData = new MachineData()
            {
                Name = machineName,
                IsActive = true,
                BID = 1,
                Description = "XMachineDescriptionTest",
                ExpenseAccountID = ctx.GLAccount.First().ID,
                IncomeAccountID = ctx.GLAccount.First().ID,
                HasImage = false,
                SKU = "SKU",
                WorksheetData = "[]",
                MachineTemplateID = newMachineTemplate1.ID,
                TaxCodeID = taxCodeID,
                Instances = new List<MachineInstance>
                {
                    GetTestMachineInstance()
                },
                Profiles = new List<MachineProfile>
                {
                    GetTestMachineProfile()
                }
            };


            // machine endpoint
            var machine = await EndpointTests.AssertPostStatusCode<MachineCategory>(client, $"{apiUrl}", JsonConvert.SerializeObject(machineData), HttpStatusCode.OK);
            var linkMachine = await EndpointTests.AssertPostStatusCode<EntityActionChangeResponse>(client, $"/api/machinecategory/{category.ID}/action/linkmachine/{machine.ID}", null, HttpStatusCode.OK);


            // custom field definition
            var customFieldDefinition = new CustomFieldDefinition()
            {
                ClassTypeID = (int)ClassType.CustomFieldDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                AppliesToClassTypeID = (int)ClassType.Machine,
                AppliesToID = machine.ID,
                Name = machineName,
                Label = "Test Custom Field Label",
                Description = "Test Custom Field Desc",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DisplayType = CustomFieldNumberDisplayType.General,
                DisplayFormatString = "",
                AllowMultipleValues = false,
                HasValidators = false
            };


            var createdCustomFieldDefinition = await EndpointTests.AssertPostStatusCode<CustomFieldDefinition>(client, "/api/cf/definition", JsonConvert.SerializeObject(customFieldDefinition), HttpStatusCode.OK);

            var values = new CustomFieldValue[1];
            values[0] = new CustomFieldValue()
            {
                ID = createdCustomFieldDefinition.ID,
                V = "TestMachine"
            };

            await EndpointTests.AssertPutStatusCode(client, $"/api/cf/value/{ClassType.Machine.ID()}/{createdCustomFieldDefinition.AppliesToID}", JsonConvert.SerializeObject(values), HttpStatusCode.OK);



            // cloning of material
            var cloned = await EndpointTests.AssertPostStatusCode<MachineCategory>(client, $"{apiUrl}/{machine.ID}/clone", null, HttpStatusCode.OK);
            Assert.AreEqual($"{machineName} (Clone)", cloned.Name);

            var clonedLinks = ctx.MachineCategoryLink.FirstOrDefault(t => t.PartID == cloned.ID);
            Assert.IsNotNull(clonedLinks);


            var retrievedCustomFieldDefinitions = await EndpointTests.AssertGetStatusCode<CustomFieldValue[]>(client, $"/api/cf/value/{ClassType.Machine.ID()}/{cloned.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(retrievedCustomFieldDefinitions);
            Assert.IsTrue(retrievedCustomFieldDefinitions.Count() == 1);

            await CleanupClonedMachine();
        }

        [TestMethod]
        public async Task MachineListByVariable()
        {
            var ctx = GetApiContext();
            MachineData itemTest = await GetNewModel();
            // Create 2 Machine Template
            AssemblyData machineTemplate1 = new AssemblyData()
            {
                BID = this.testBID,
                Name = DateTime.UtcNow + " TEST MACHINE TEMPLATE 1",
                AssemblyType = AssemblyType.MachineTemplate,
                MachineLayoutTypes = (MachineLayoutType)3,
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        BID = TestConstants.BID,
                        Name = "TestCheckboxVariable",
                        ElementType = AssemblyElementType.Checkbox,
                        DataType = DataType.Boolean,
                        Label = "label",
                        DefaultValue = null,
                        IsFormula = false,
                    }
                }
            };
            var newMachineTemplate1 = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, $"/api/assemblypart", JsonConvert.SerializeObject(machineTemplate1), HttpStatusCode.OK);
            Assert.IsNotNull(newMachineTemplate1);

            // test post
            itemTest.MachineTemplateID = newMachineTemplate1.ID;
            itemTest.Profiles.First().MachineTemplateID = newMachineTemplate1.ID;
            var machineData = await EndpointTests.AssertPostStatusCode<MachineData>(client, $"{apiUrl}", JsonConvert.SerializeObject(itemTest), HttpStatusCode.OK);
            Assert.IsNotNull(machineData);

            AssemblyData model = new AssemblyData()
            {
                Name = "TEST_ASSEMBLY_DATA_" + DateTime.UtcNow,
                IsActive = true,
                BID = TestConstants.BID,
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        BID = TestConstants.BID,
                        Name = "TestVariable",
                        ElementType = AssemblyElementType.LinkedMachine,
                        LinkedMachineID = machineData.ID,
                        IsConsumptionFormula = true,
                        DataType = DataType.MachinePart,
                        Label = "label",
                        DefaultValue = null,
                        ConsumptionDefaultValue = "1",
                        IsFormula = false,
                        ListValuesJSON = $"{{components: [{{\"ID\": {machineData.ID}}}]}}"
                    }
                }
            };

            //CREATE
            var assemblyData = await EndpointTests.AssertPostStatusCode<AssemblyData>(client, "/api/assemblypart", JsonConvert.SerializeObject(model), HttpStatusCode.OK);
            Assert.IsNotNull(assemblyData);
            var variableID = assemblyData.Variables.First().ID;
            var profileName = machineData.Profiles.First().Name;
            var simpleMachineCollection = await EndpointTests.AssertGetStatusCode<ICollection<SimpleMachineData>>(client, $"{apiUrl}/simplelist/byvariable/{variableID}?MachineID={machineData.ID}&ProfileName=\"{profileName}\"", HttpStatusCode.OK);
            Assert.IsNotNull(simpleMachineCollection);
            Assert.AreEqual(1, simpleMachineCollection.Count);
            Assert.AreEqual(machineData.ID, simpleMachineCollection.First().ID);

            // test delete
            await EndpointTests.AssertGetStatusCode(client, $"{apiUrl}/{machineData.ID}/action/candelete", HttpStatusCode.OK);
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{machineData.ID}", HttpStatusCode.NoContent);

            await EndpointTests.AssertDeleteStatusCode(client, $"/api/assemblypart/{assemblyData.ID}", HttpStatusCode.NoContent);
            Assert.IsFalse(ctx.AssemblyData.Any(x => x.ID == assemblyData.ID));
        }
    }
}
