﻿using Endor.Api.Web.Classes;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Tests
{
    [TestClass]
    public class AlertDefinitionActionEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/alert/definitionaction";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public TestContext TestContext { get; set; }

        private string AlertDefinitionActionSubject;

        [TestInitialize]
        public void Initialize()
        {
            this.AlertDefinitionActionSubject = $"TestAlertDefinitionAction{DateTime.UtcNow}";
        }

        [TestMethod]
        public async Task AlertDefinitionActionCRUDTest()
        {
            // CREATE
            var newAlertDefinitionActionModel = await GetNewAlertDefinitionActionModel();
            var createdAlertDef = await EndpointTests.AssertPostStatusCode<AlertDefinitionAction>(client, apiUrl, newAlertDefinitionActionModel, HttpStatusCode.OK);

            // READ
            var allAlerts = await EndpointTests.AssertGetStatusCode<AlertDefinitionAction[]>(client, apiUrl, HttpStatusCode.OK);
            Assert.IsNotNull(allAlerts.FirstOrDefault(x => x.ID == createdAlertDef.ID));

            var myAlert = await EndpointTests.AssertGetStatusCode<AlertDefinitionAction>(client, $"{apiUrl}/{createdAlertDef.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(myAlert);

            // UPDATE
            createdAlertDef.Body = $"Body{DateTime.UtcNow}";

            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{createdAlertDef.ID}", JsonConvert.SerializeObject(createdAlertDef), HttpStatusCode.OK);
            var updatedAlert = await EndpointTests.AssertGetStatusCode<AlertDefinitionAction>(client, $"{apiUrl}/{createdAlertDef.ID}", HttpStatusCode.OK);

            Assert.AreEqual(createdAlertDef.Body, updatedAlert.Body);

            await EndpointTests.AssertPostStatusCode<AlertDefinitionAction>(client, $"{apiUrl}/{createdAlertDef.ID}/clone", null, HttpStatusCode.OK);

            // DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{createdAlertDef.ID}", HttpStatusCode.NoContent);

        }

        [TestMethod]
        public async Task AlertDefinitionActionAPIActionTest()
        {
            // CREATE/READ
            var newAlertDefinitionActionModel = await GetNewAlertDefinitionActionModel();
            var createdAlertActionDef = await EndpointTests.AssertPostStatusCode<AlertDefinitionAction>(client, apiUrl, newAlertDefinitionActionModel, HttpStatusCode.OK);
            var myAlert = await EndpointTests.AssertGetStatusCode<AlertDefinitionAction>(client, $"{apiUrl}/{createdAlertActionDef.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(myAlert);

            var canDelete = await EndpointTests.AssertGetStatusCode<BooleanResponse>(client, $"{apiUrl}/{createdAlertActionDef.ID}/action/candelete", HttpStatusCode.OK);
            Assert.AreEqual(true, canDelete.Value);

            // DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdAlertActionDef.ID}", HttpStatusCode.NoContent);

        }

        [TestMethod]
        public async Task AlertDefinitionFilterTest()
        {
            // CREATE/READ
            var newAlertDefinitionActionModel = await GetNewAlertDefinitionActionModel();
            var createdAlertActionDef = await EndpointTests.AssertPostStatusCode<AlertDefinitionAction>(client, apiUrl, newAlertDefinitionActionModel, HttpStatusCode.OK);
            var myAlert = await EndpointTests.AssertGetStatusCode<AlertDefinitionAction>(client, $"{apiUrl}/{createdAlertActionDef.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(myAlert);

            var myAlerts = await EndpointTests.AssertGetStatusCode<AlertDefinitionAction[]>(client, $"{apiUrl}/?AlertDefinitionID={newAlertDefinitionActionModel.AlertDefinitionID}", HttpStatusCode.OK);
            Assert.IsNotNull(myAlerts);
            Assert.IsNotNull(myAlerts.FirstOrDefault(x => x.ID == createdAlertActionDef.ID));

            myAlerts = await EndpointTests.AssertGetStatusCode<AlertDefinitionAction[]>(client, $"{apiUrl}/?AlertDefinitionID={-newAlertDefinitionActionModel.AlertDefinitionID}", HttpStatusCode.OK);
            Assert.IsNotNull(myAlerts);
            Assert.IsNull(myAlerts.FirstOrDefault(x => x.ID == createdAlertActionDef.ID));

            // DELETE
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{createdAlertActionDef.ID}", HttpStatusCode.NoContent);

        }

        private async Task<AlertDefinitionAction> GetNewAlertDefinitionActionModel()
        {
            var actions = await EndpointTests.AssertGetStatusCode<SystemAutomationActionDefinition[]>(client, "/api/system/alert/actions", HttpStatusCode.OK);
            var alert = await AlertDefinitionEndpointTests.GetNewAlertDefinitionModel(AlertDefinitionActionSubject, client);
            var createdAlertDef = await EndpointTests.AssertPostStatusCode<AlertDefinition>(client, "/api/alert/definition", alert, HttpStatusCode.OK);
            return new AlertDefinitionAction()
            {
                Subject = AlertDefinitionActionSubject,
                ActionDefinitionID = actions.FirstOrDefault()?.ID ?? 0,
                AlertDefinitionID = createdAlertDef.ID,
                SubjectHasMergeFields = false,
                BodyHasMergeFields = false,
                SortIndex = 0,
            };
        }



        [TestCleanup]
        public void Teardown()
        {
            this.DeleteTestRecord();
        }

        private void DeleteTestRecord()
        {
            string testAlertDefName = AlertDefinitionActionSubject;
            Task.Run(async () =>
            {
                System.Net.Http.HttpResponseMessage response = await client.GetAsync($"{apiUrl}");
                string responseString = await response.Content.ReadAsStringAsync();
                AlertDefinitionAction[] result = JsonConvert.DeserializeObject<AlertDefinitionAction[]>(responseString);
                if (result != null)
                {
                    result.Where(x => x.Subject != null && x.Subject.Contains(testAlertDefName)).ToList().ForEach(async (ad) =>
                   {
                       if (ad != null)
                       {
                           await client.DeleteAsync($"{apiUrl}/{ad.ID}");
                       }
                   });
                }

                response = await client.GetAsync($"/api/alert/definition");
                responseString = await response.Content.ReadAsStringAsync();
                AlertDefinition[] results = JsonConvert.DeserializeObject<AlertDefinition[]>(responseString);
                if (results != null)
                {
                    AlertDefinition ad = results.FirstOrDefault(x => x.Name.Contains(testAlertDefName));
                    if (ad != null)
                    {
                        await client.DeleteAsync($"/api/alert/definition/{ad.ID}");
                    }
                }
            }).Wait();
        }
    }
}
