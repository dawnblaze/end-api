﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

namespace Endor.Api.Tests
{
    [TestClass]
    public class TimeCardEndpointTest : CommonControllerTestClass
    {
        public const string apiUrl = "/api/timecard";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();

        [TestMethod]
        public async Task TimeCardCRUDTest()
        {
            // POST ../api/timecard/
            var newTimeCardModel = await GetNewTimeCardModel();
            var newlyCreatedTimeCard = await EndpointTests.AssertPostStatusCode<TimeCard>(client, apiUrl, newTimeCardModel, HttpStatusCode.OK);
            Assert.IsNotNull(newlyCreatedTimeCard);

            // GET ../api/timecard/
            var timeCard = await EndpointTests.AssertGetStatusCode<TimeCard[]>(client, $"{apiUrl}?EmployeeID={newlyCreatedTimeCard.ID}", HttpStatusCode.OK);
            Assert.IsNotNull(timeCard);

            // PUT ../api/timecard/{timecardid}
            newlyCreatedTimeCard.MetaData = "Test Update";
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{newlyCreatedTimeCard.ID}", JsonConvert.SerializeObject(newlyCreatedTimeCard), HttpStatusCode.OK);

            // GET ../api/timecard/{timecardid}
            var updatedTimeCard = await EndpointTests.AssertGetStatusCode<TimeCard>(client, $"{apiUrl}/{newlyCreatedTimeCard.ID}", HttpStatusCode.OK);
            Assert.AreEqual(newlyCreatedTimeCard.MetaData, updatedTimeCard.MetaData);

            // DELETE ../api/timecard/{timecardid}
            await EndpointTests.AssertDeleteStatusCode(client, $"{ apiUrl}/{newlyCreatedTimeCard.ID}", HttpStatusCode.NoContent);
        }

        private async Task<TimeCard> GetNewTimeCardModel()
        {
            var employee = await EndpointTests.AssertGetStatusCode<EmployeeData[]>(client, "/api/employee", HttpStatusCode.OK);

            return new TimeCard()
            {
                BID = 1,
                EmployeeID = employee.FirstOrDefault()?.ID ?? 0,
                StartDT = DateTime.Now,
                EndDT = DateTime.Now,
                TimeInMin = 0,
                PaidTimeInMin = 0,
                IsClosed = true,
                IsAdjusted = true,
                AdjustedByEmployeeID = employee.FirstOrDefault()?.ID ?? 0,
                AdjustedDT = DateTime.Now,
                MetaData = "Test"
            };
        }
    }
}
