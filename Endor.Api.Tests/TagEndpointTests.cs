﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

namespace Endor.Api.Tests
{
    [TestClass]
    public class TagEndpointTests : CommonControllerTestClass
    {
        public const string apiUrl = "/api/tag";
        public System.Net.Http.HttpClient client = EndpointTests.GetHttpClient();
        public string NameToTest = "UnitTest";
        public string RGB = "D72727";

        private ListTag GetNewAdHocTag()
        {
            var ctx = GetApiContext();
            return new ListTag()
            {
                BID = 1,
                AssociatedClassTypeID = (int)ClassType.Order,
                Name = NameToTest,
                RGB = RGB,
                //ColorID = ctx.SystemColor.First().ID,
                IsDeleted = false
            };
        }

        [TestMethod]
        public async Task ListTagTest()
        {
            //Initialize
            var ctx = GetApiContext();
            var tagsToRemove = ctx.ListTag.Where(t => t.Name.Contains("UnitTest"));
            ctx.RemoveRange(tagsToRemove);
            await ctx.SaveChangesAsync();

            var adHocTag = GetNewAdHocTag();
            var adHocTag2 = GetNewAdHocTag();
            adHocTag2.Name = adHocTag2.Name + "2";
            //Test Post
            adHocTag2 = await EndpointTests.AssertPostStatusCode<ListTag>(client, $"{apiUrl}", JsonConvert.SerializeObject(adHocTag2), HttpStatusCode.OK);
            adHocTag = await EndpointTests.AssertPostStatusCode<ListTag>(client, $"{apiUrl}", JsonConvert.SerializeObject(adHocTag), HttpStatusCode.OK);
            Assert.IsNotNull(adHocTag);
            Assert.AreEqual(adHocTag.Name, NameToTest);
            var baseId = adHocTag.ID;
            Assert.IsFalse(adHocTag2.IsDeleted);
            adHocTag2.IsDeleted = true;
            var tmp = await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(adHocTag2), HttpStatusCode.BadRequest);


            //Test Get By ID
            adHocTag = await EndpointTests.AssertGetStatusCode<ListTag>(client, $"{apiUrl}/{baseId}", HttpStatusCode.OK);
            Assert.AreEqual(adHocTag.Name, NameToTest);

            //Test Get List
            var list = await EndpointTests.AssertGetStatusCode<List<ListTag>>(client, apiUrl, HttpStatusCode.OK);
            Assert.IsTrue(list.Count > 0);

            //Test Sorting Alphabetically
            Assert.IsTrue(adHocTag2.ID < adHocTag.ID);
            var Id = list.FindIndex(t => t.Name == NameToTest);
            var Id2 = list.FindIndex(t => t.Name == NameToTest+"2");
            Assert.IsTrue(Id < Id2);

            //Test Put
            adHocTag.Name = NameToTest + "3";
            adHocTag = await EndpointTests.AssertPutStatusCode<ListTag>(client, $"{apiUrl}/{baseId}", JsonConvert.SerializeObject(adHocTag), HttpStatusCode.OK);
            Assert.IsNotNull(adHocTag);
            Assert.AreEqual(adHocTag.Name, NameToTest + "3");

            //Test Delete
            adHocTag2 = await EndpointTests.AssertDeleteStatusCodeWithContent<ListTag>(client, $"{apiUrl}/{adHocTag2.ID}", HttpStatusCode.OK);
            Assert.IsTrue(adHocTag2.IsDeleted);

            //Test Retrieving Delete
            adHocTag2 = await EndpointTests.AssertGetStatusCode<ListTag>(client, $"{apiUrl}/{adHocTag2.ID}", HttpStatusCode.OK);
            Assert.AreEqual(adHocTag2.Name, NameToTest + "2");
            Assert.IsTrue(adHocTag2.IsDeleted);

            //Test Make sure you can't write IsDeleted
            adHocTag.IsDeleted = true;
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{baseId}", JsonConvert.SerializeObject(adHocTag), HttpStatusCode.BadRequest);
            
            //Test System Tag CRUD
            var adHocTag3 = GetNewAdHocTag();
            var newName = NameToTest + "3";
            adHocTag3.Name = newName;
            adHocTag3.IsSystem = true;
            ctx.ListTag.Add(adHocTag3);
            ctx.SaveChanges();
            var IdToUse = ctx.ListTag.Where(t => t.Name == newName).First().ID;
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(adHocTag3), HttpStatusCode.BadRequest);
            await EndpointTests.AssertPutStatusCode(client, $"{apiUrl}/{IdToUse}", JsonConvert.SerializeObject(adHocTag3), HttpStatusCode.BadRequest);
            await EndpointTests.AssertDeleteStatusCode(client, $"{apiUrl}/{IdToUse}", HttpStatusCode.BadRequest);

            //Test Unique Names
            await EndpointTests.AssertPostStatusCode(client, $"{apiUrl}", JsonConvert.SerializeObject(adHocTag), HttpStatusCode.BadRequest);
            adHocTag2.IsDeleted = false;
            await EndpointTests.AssertPostStatusCode<ListTag>(client, $"{apiUrl}", JsonConvert.SerializeObject(adHocTag2), HttpStatusCode.OK);

            //Test Link Action Endpoints
            var contactID = ctx.ContactData.OrderBy(t => t.ID).First().ID;
            var ctid = (int)ClassType.Contact;
            await EndpointTests.AssertPostStatusCode<ListTag>(client, $"{apiUrl}/{baseId}/action/link/{ctid}/{contactID}", JsonConvert.SerializeObject(adHocTag), HttpStatusCode.OK);

            //Test Get Parameters
            var filteredTag = await EndpointTests.AssertGetStatusCode<List<ListTag>>(client, $"{apiUrl}?AssociatedID={contactID}&AssociatedClassTypeID={ctid}", HttpStatusCode.OK);
            var filteredTag2 = await EndpointTests.AssertGetStatusCode<List<ListTag>>(client, $"{apiUrl}?AssociatedID={contactID-1}&AssociatedClassTypeID={ctid}", HttpStatusCode.OK);
            Assert.IsTrue(filteredTag.Count>0);
            Assert.IsFalse(filteredTag2.Count > 0);

            //Test Link Action Endpoints
            await EndpointTests.AssertPostStatusCode<ListTag>(client, $"{apiUrl}/{baseId}/action/unlink/{ctid}/{contactID}", HttpStatusCode.OK);

            //Cleanup
            tagsToRemove = ctx.ListTag.Where(t => t.Name.Contains("UnitTest"));
            ctx.RemoveRange(tagsToRemove);
        }



    }
}
