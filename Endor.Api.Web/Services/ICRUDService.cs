﻿using System;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Interface for CRUD Service
    /// </summary>
    public interface ICRUDService<M> where M: class
    {
        /// <summary>
        /// Create model
        /// </summary>
        Task<M> CreateAsync(M newModel, Guid? tempGuid = null);
        /// <summary>
        /// Called before Model is Created
        /// </summary>
        Task DoBeforeCreateAsync(M newModel, Guid? tempGuid = null);

        /// <summary>
        /// Update model
        /// </summary>
        Task<M> UpdateAsync(M model, string connectionID);
        /// <summary>
        /// Get existing Model
        /// </summary>
        Task<M> GetOldModelAsync(M model);
        /// <summary>
        /// Called Before Model is updated
        /// </summary>
        Task DoBeforeUpdateAsync(M oldModel, M newModel);

        /// <summary>
        /// Delete model
        /// </summary>
        Task<bool> DeleteAsync(M model);
        /// <summary>
        /// Called Before Model is deleted
        /// </summary>
        Task DoBeforeDeleteAsync(M model);
    }

    /// <summary>
    /// Interface for CRUD CloneableChild Service
    /// </summary>
    public interface ICloneableChildCRUDService<pM, cM> : ICRUDService<cM>
        where pM: class
        where cM: class
    {
        /// <summary>
        /// Called before Clone of Parent
        /// </summary>
        Task DoBeforeCloneForParent(pM parentModel, cM childModel);
    }
}
