﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Models;
using Endor.Util;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Endor.Tenant;
using Newtonsoft.Json.Linq;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Endor.Logging.Client;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Payment APE Service
    /// </summary>
    public class PaymentAPEService : BaseGenericService
    {
        /// <summary>
        /// Business ID
        /// </summary>
        public readonly short BID;
        /// <summary>
        /// object to get cached tenant data
        /// </summary>
        protected readonly ITenantDataCache cache;

        private const int OptionId = 7020;
        private const string OptionName = "Integration.CreditCard.BusinessKey";

        private OptionService _optionService;
        private static RemoteLogger _logger;
        private static short _staticBID;

        /// <summary>
        /// Constructor
        /// </summary>
        public PaymentAPEService(ApiContext context, RemoteLogger logger, short bid, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, migrationHelper)
        {
            this.BID = bid;
            this.cache = cache;
            this._optionService = new OptionService(context, migrationHelper);
            _logger = logger;
            _staticBID = bid;
        }

        private async Task<string> GetPaymentURLAsync(short bid)
        {
            return (await cache.Get(bid)).PaymentURL;
        }

        /// <summary>
        /// Retrieve existing APE BusinessKey from Option Definition ID = 7020, [Integration.CreditCard.BusinessKey].
        /// If the Key doesn't exist, Register the Business in APE and stores the BusinessKey in the Option.
        /// </summary>
        /// <returns></returns>
        private async Task<Guid> GetBusinessKeyAsync()
        {
            return await GetGenericBusinessKeyAsync(this.ctx, this.BID, await GetPaymentURLAsync(this.BID), "ape", _optionService, OptionId, OptionName);
        }

        public async static Task<Guid> GetGenericBusinessKeyAsync(ApiContext ctx, short bid, string baseURL, string type, OptionService optionService, int optionID, string optionName)
        {
            // Retrieve existing APE BusinessKey from Option Definition
            Guid businessKey = Guid.Empty;
            try
            {
                var option = await optionService.Get(optionID, optionName, bid, null, null, null, null, null);
                Guid.TryParse(option.Value.Value,out businessKey);
            }
            catch (Exception ex)
            {
                await _logger.Error(_staticBID, ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
            
            if (businessKey==Guid.Empty)
            {
                BusinessData businessData = await ctx.BusinessData.FirstOrDefaultAsync(b => b.BID == bid);

                if (businessData == null) throw new Exception($"Unable to get Business data from the BID {bid}");

                StringContent content = null;
                
                if (type == "ape") 
                    content = new StringContent(JsonConvert.SerializeObject(new
                    {
                        appType = "Endor",
                        companyName = businessData.Name,
                        associationID = businessData.AssociationType ?? 0,
                        businessID = businessData.ID
                    }), Encoding.UTF8, "application/json");
                else if (type == "ate")
                    content = new StringContent(JsonConvert.SerializeObject(new
                    {
                        appType = "Endor",
                        businessName = businessData.Name,
                        associationID = businessData.AssociationType ?? 0,
                        BID = (int)businessData.ID
                    }), Encoding.UTF8, "application/json");

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
                    HttpResponseMessage httpResponse = await client.PostAsync(type+"/businesskey", content);
                    httpResponse.EnsureSuccessStatusCode();

                    string resultContent = await httpResponse.Content.ReadAsStringAsync();
                    APEBusinessKeyResponse response = JsonConvert.DeserializeObject<APEBusinessKeyResponse>(resultContent);
                    businessKey = response.BusinessKey;
                }

                // Stores the BusinessKey in the Option
                var putResult = await optionService.Put(OptionId, null, businessKey.ToString(), null, bid, null, null, null, null, null);

                return businessKey;
            }
            else
            {
                // return existing business key
                return businessKey;
            }
        }

        /// <summary>
        /// Gets the registration key of the business
        /// </summary>
        /// <returns></returns>
        public async Task<APERegistrationResponse> GetRegistrationAsync(string currency)
        {
            var businessKey = await GetBusinessKeyAsync();

            const int CardConnectProviderID = 1;
            const int expiryMinutes = 15;

            string APE_URL = $"{await GetPaymentURLAsync(this.BID)}ape";

            GetOptionValueResponse isEnabled = await _optionService.Get(7021, "Integration.CreditCard.Enabled", BID, null, null, null, null, null);
            var provider = await GetOptionValueAsync(7022, "Integration.CreditCard.Provider");
            int providerID = 0;
            if (!int.TryParse(provider, out providerID))
            {
                //APE now accepts providerID of 0...
                //throw new InvalidOperationException("APE provider not found");
            }
            var merchantID = await GetOptionValueAsync(7023, "Integration.CreditCard.MerchantID");
            var username = await GetOptionValueAsync(7024, "Integration.CreditCard.UserName");
            var password = await GetOptionValueAsync(7025, "Integration.CreditCard.Password");

            var timeZone = await GetTimeZoneIDAsync(ctx,this.BID);

            var paymentMethods = this.ctx.PaymentMethod.Where(p => p.BID == this.BID && p.IsActive == true);
            var apePaymentMethods = new List<APEPaymentMethods>();
            // used to contain credit cards as submethods
            var creditCardPaymentMethod = new APEPaymentMethods
            {
                ID = -1, // used to determine Credit Card (External), check sub method id
                Name = "Credit Card (External)",
                PaymentScreenType = 2, // APE - Credit Card (External)
                SubMethods = new List<APEPaymentSubMethods>()
            };

            foreach (var paymentMethod in paymentMethods)
            {
                switch (paymentMethod.PaymentMethodType)
                {
                    case PaymentMethodType.Custom:
                    case PaymentMethodType.Check:
                    case PaymentMethodType.WireTransfer:
                        apePaymentMethods.Add(new APEPaymentMethods
                        {
                            ID = paymentMethod.ID,
                            Name = paymentMethod.Name,
                            PaymentScreenType = 1 // APE - custom
                        });
                        break;
                    case PaymentMethodType.Cash:
                        apePaymentMethods.Add(new APEPaymentMethods
                        {
                            ID = paymentMethod.ID,
                            Name = paymentMethod.Name,
                            PaymentScreenType = 4 // APE - cash
                        });
                        break;
                    case (PaymentMethodType)3: // TODO: no PaymentMethodType.ACH:
                        apePaymentMethods.Add(new APEPaymentMethods
                        {
                            ID = paymentMethod.ID,
                            Name = paymentMethod.Name,
                            PaymentScreenType = 11 // APE - echeck ACH
                        });
                        break;
                    case PaymentMethodType.CreditCard:
                        apePaymentMethods.Add(new APEPaymentMethods
                        {
                            ID = paymentMethod.ID,
                            Name = paymentMethod.Name,
                            PaymentScreenType = 12 // APE - Credit Card (Processed)
                        });
                        break;
                    case PaymentMethodType.RefundableCredit:
                    case PaymentMethodType.NonRefundableCredit:
                        apePaymentMethods.Add(new APEPaymentMethods
                        {
                            ID = paymentMethod.ID,
                            Name = paymentMethod.Name,
                            PaymentScreenType = 3 // APE - Customer Credit
                        });
                        break;
                    case PaymentMethodType.AmEx:
                    case PaymentMethodType.MasterCard:
                    case PaymentMethodType.Visa:
                    case PaymentMethodType.Discover: // NOTE: added as submethods to creditCardPaymentMethod 
                        creditCardPaymentMethod.SubMethods.Add(new APEPaymentSubMethods
                        {
                            ID = paymentMethod.ID,
                            Name = paymentMethod.Name
                        });
                        break;
                }
            }

            // NOTE: if credit card has values in submethods, add to first
            if (creditCardPaymentMethod.SubMethods.Count > 0)
            {
                apePaymentMethods.Insert(0, creditCardPaymentMethod);
            }

            var registration = new APERegistrationRequest
            {
                AppType = "Endor",
                BusinessKey = businessKey.ToString(),
                Currency = currency,
                IsSandboxed = true,
                Name = "Registration",
                ProcessorType = providerID,
                TimeZone = timeZone,
                PaymentMethods = apePaymentMethods
            };

            if (isEnabled.ValueAsBoolean(false) && providerID == CardConnectProviderID)
            {
                registration.MerchantID = merchantID;
                registration.Credentials = new Dictionary<string, string>
                {
                    { "username", username },
                    { "password", password }
                };
            }

            APERegistrationResponse registrationResponse = null;
            using (var client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, APE_URL + "/registration");
                var registrationJSONString = JsonConvert.SerializeObject(registration);
                request.Content = new StringContent(registrationJSONString, Encoding.UTF8, "application/json");
                var response = await client.SendAsync(request);
                string responseString = await response.Content.ReadAsStringAsync();

                switch (response.StatusCode)
                {
                    case HttpStatusCode.OK:
                        registrationResponse = JsonConvert.DeserializeObject<APERegistrationResponse>(responseString);
                        break;
                    case HttpStatusCode.NotFound:
                        await DeleteBusinessKey();
                        throw new InvalidOperationException("business key not found");
                    case HttpStatusCode.InternalServerError:
                        throw new InvalidOperationException("APE error!");
                }
            }

            registrationResponse.ApeURL = APE_URL;
            registrationResponse.Expiration = DateTime.Now.AddMinutes(expiryMinutes);

            var cardReaderEnabled = await _optionService.Get(7027, "Integration.CreditCard.Reader.Enabled", BID, null, null, null, null, null);
            var vaultingEnabled = await _optionService.Get(7026, "Integration.CreditCard.Vaulting.Enabled", BID, null, null, null, null, null);
            
            if (providerID != 0)
            {
                registrationResponse.ProviderTest = await TestProviderRegistration(registrationResponse.RegistrationID);
            }

            registrationResponse.Provider = new APEProviderInfo
            {
                IntegrationEnabled = isEnabled.ValueAsBoolean(false),
                Name = providerID != 0 ? "Card Connect" : "",
                EnableCardReader = cardReaderEnabled.ValueAsBoolean(false),
                EnableVault = vaultingEnabled.ValueAsBoolean(false),
            };


            return registrationResponse;
        }

        private async Task<APEProviderTest> TestProviderRegistration(string registrationId)
        {
            var providerTest = new APEProviderTest();

            try
            {
                string TEST_URL = $"{await GetPaymentURLAsync(this.BID)}ape/test";

                using (var client = new HttpClient())
                {
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, TEST_URL + "/" + registrationId);

                    var response = await client.SendAsync(request);
                    string jsonString = await response.Content.ReadAsStringAsync();

                    dynamic json = JValue.Parse(jsonString);
                    providerTest.ResponseCode = (int)response.StatusCode;
                    providerTest.Message = json.messageText;
                    providerTest.Success = json.wasSuccess;

                }
            }
            catch (Exception ex)
            {
                await _logger.Error(BID, "TestProviderRegistration Error >>> " + ex.Message, ex);
            }

            return providerTest;
        }

        private async Task<string> GetOptionValueAsync(int optionID, string optionName)
        {
            try
            {
                var option = await _optionService.Get(optionID, optionName, BID, null, null, null, null, null);
                return option.Value.Value;
            }
            catch (Exception ex)
            {
                await _logger.Error(BID, ex.Message, ex);
                return null;
            }
        }

        public static async Task<short> GetTimeZoneIDAsync(ApiContext ctx, short bid)
        {
            var location = await GetBusinessDefaultLocation(ctx, bid);

            if (location.TimeZoneID != null)
            {
                short? timezoneId = location.TimeZoneID.Value;

                return timezoneId.GetValueOrDefault();
            }
            else
            {
                return (short)TimeZoneUtils.NametoID("GMT Standard Time");
            }
        }

        public static async Task<LocationData> GetBusinessDefaultLocation(ApiContext ctx, short BID)
        {
            BusinessData business = await ctx.BusinessData
                .Include(b => b.Locations)
                .FirstOrDefaultAsync(b => b.BID == BID);

            LocationData defaultLocation = business.Locations.FirstOrDefault(l => l.IsDefault);
            if (defaultLocation == null)
            {
                defaultLocation = ctx.LocationData.FirstOrDefault(x => x.BID == BID);
            }

            return defaultLocation;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task DeleteBusinessKey()
        {
            var businessKey = await GetOptionValueAsync(OptionId, OptionName);

            if (string.IsNullOrWhiteSpace(businessKey))
            {
                throw new Exception($"Unable to get BusinessKey from the BID {this.BID}");
            }

            await _optionService.Delete(OptionId, OptionName, null, this.BID, null, null, null, null, null);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(await GetPaymentURLAsync(this.BID));
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
                await client.DeleteAsync($"ape/businesskey/{businessKey}");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageRequest"></param>
        /// <returns></returns>
        public async Task<JObject> GetPaymentPage(APIPaymentPageRequest pageRequest)
        {
            List<APEPaymentPageRequestOrders> paymentPageRequestOrders = new List<APEPaymentPageRequestOrders>();

            var paymentPageRequest = new APEPaymentPageRequest
            {
                TransactionType = "auth",
                Amount = 0,
                AmountToCredit = pageRequest.AmountToCredit.HasValue ? pageRequest.AmountToCredit : 0,
                CanAlterAmount = pageRequest.CanAlterAmount.HasValue ? pageRequest.CanAlterAmount : false,
                MinAmount = pageRequest.MinAmount.HasValue ? pageRequest.MinAmount : 0,
                MaxAmount = pageRequest.MaxAmount.HasValue ? pageRequest.MaxAmount : 0,
                CanUseCompanyCredit = pageRequest.CanUseCompanyCredit.HasValue ? pageRequest.CanUseCompanyCredit : false,
                CompanyCreditBalance = pageRequest.CompanyCreditBalance.HasValue ? pageRequest.CompanyCreditBalance : 0,
                ShowCardReader = true,
                ShowPaymentTypes = true,
                ShowVault = true,
                CanAddToVault = true,
                Orders = new APEPaymentPageRequestOrders[] { }
            };

            if (pageRequest.Orders != null && pageRequest.Orders.Length > 0)
            {
                foreach (APIPaymentPageRequestOrders pageRequestOrder in pageRequest.Orders)
                {
                    if (!pageRequestOrder.Amount.HasValue)
                        pageRequestOrder.Amount = 0;
                    if (!paymentPageRequest.Amount.HasValue)
                        paymentPageRequest.Amount = 0;

                    var order = await this.ctx.OrderData
                    .Include(o => o.Company)
                        .ThenInclude(c => c.Location)
                    .Include(o => o.ContactRoles)
                    .Include("ContactRoles.Contact")
                    .Include("ContactRoles.Contact.CompanyContactLinks")
                    .Include(o => o.Dates)
                    .Include(o => o.Items)
                    .Include(o => o.Location)
                        .ThenInclude(l => l.LocationLocators)
                    .WherePrimary(BID, pageRequestOrder.OrderID).FirstOrDefaultAsync();

                    if (order == null)
                        throw new KeyNotFoundException("Invalid Order");

                    var creditLimit = order.Company.RefundableCredit.GetValueOrDefault() + order.Company.NonRefundableCredit.GetValueOrDefault();

                    // load contact
                    OrderContactRole primaryContact = null;
                    OrderContactRole billingContact = null;
                    if (order.ContactRoles != null && order.ContactRoles.Count > 0)
                    {
                        primaryContact = order.ContactRoles.FirstOrDefault(r => r.RoleType == OrderContactRoleType.Primary);
                        billingContact = order.ContactRoles.FirstOrDefault(r => r.RoleType == OrderContactRoleType.Billing);
                    }

                    if (pageRequest.Amount == 0)
                    {
                        paymentPageRequest.Amount += order.PaymentBalanceDue;
                    }
                    else
                    {
                        paymentPageRequest.Amount = pageRequest.Amount;
                    }

                    paymentPageRequest.CanUseCompanyCredit = creditLimit > 0;
                    paymentPageRequest.CompanyCreditBalance = creditLimit;
                    paymentPageRequest.CompanyID = order.Company.ID;
                    paymentPageRequest.CompanyName = order.Company.Name;
                    paymentPageRequest.LocationID = order.Company.Location.ID;
                    paymentPageRequest.LocationName = order.Company.Location.Name;
                    paymentPageRequest.ContactID = billingContact?.ContactID ?? (primaryContact?.ContactID ?? 0);

                    // NOTE: populate contacts to paymentPageRequest.Contacts
                    if (order.ContactRoles != null && order.ContactRoles.Count > 0)
                    {
                        var contacts = new List<APEPaymentPageRequestContacts>();
                        foreach (var orderContact in order.ContactRoles)
                        {
                            if (!orderContact.Contact.CompanyContactLinks.Any(l => l.CompanyID == order.CompanyID && l.IsActive == true))
                                continue;

                            contacts.Add(new APEPaymentPageRequestContacts
                            {
                                ContactID = orderContact.ContactID.GetValueOrDefault(0).ToString(),
                                Name = orderContact.ContactName
                            });
                        }
                        paymentPageRequest.Contacts = contacts.ToArray();
                    }

                    if (creditLimit > 0)
                    {
                        paymentPageRequest.CompanyCreditBalance = creditLimit;
                    }

                    // NOTE: load date created from order.Dates
                    OrderKeyDate orderDate = null;
                    if (order.Dates != null && order.Dates.Count > 0)
                    {
                        orderDate = order.Dates.FirstOrDefault(d => d.KeyDateType == OrderKeyDateType.Created);
                    }

                    var locator = order.Location.LocationLocators.FirstOrDefault(l => l.LocatorType == (byte)LocatorType.Address);
                    AddressMetaData address = null;
                    var paymentAddress = new APEPaymentAddress
                    {
                        StreetAddress = "",
                        City = "",
                        State = "",
                        Country = "",
                        PostalCode = ""
                    };
                    if (locator != null)
                    {
                        address = (AddressMetaData)locator.MetaDataObject;
                        paymentAddress.StreetAddress = address.Street1 + (!String.IsNullOrEmpty(address.Street2) ? " " + address.Street2 : "");
                        paymentAddress.City = address.City;
                        paymentAddress.State = address.State;
                        paymentAddress.Country = address.Country;
                        paymentAddress.PostalCode = address.PostalCode;
                    }

                    var apeOrder = new APEPaymentPageRequestOrders
                    {
                        OrderID = order.FormattedNumber,
                        Amount = pageRequestOrder.Amount > 0 ? pageRequestOrder.Amount : order.PriceNet,
                        PONumber = order.OrderPONumber,
                        TaxAmount = order.PriceTax,
                        FreightAmount = order.PriceDestinationTotal,
                        OrderDate = orderDate != null ? orderDate.KeyDT : DateTime.Now, // NOTE: default is current DT
                        ShipToAddress = paymentAddress, // NOTE: we will need to look up the ShipTo Address on the Order when Destinations is implemented.
                        ShipFromAddress = paymentAddress
                    };

                    var apeOrderItems = new List<APEPaymentLineItem>();
                    foreach (var lineItem in order.Items)
                    {
                        apeOrderItems.Add(new APEPaymentLineItem
                        {
                            LineNumber = lineItem.ItemCount.ToString(),
                            Product = lineItem.Name,
                            Description = lineItem.Description,
                            Quantity = (int)lineItem.Quantity,
                            UnitPrice = lineItem.PriceUnitPreTax.GetValueOrDefault(),
                            SubtotalPrice = lineItem.PricePreTax.GetValueOrDefault(),
                            Discount = lineItem.PriceDiscount.GetValueOrDefault(),
                            TaxAmount = lineItem.PriceTax.GetValueOrDefault()
                        });
                    }
                    apeOrder.Items = apeOrderItems.ToArray();
                    paymentPageRequestOrders.Add(apeOrder);
                }
            }
            else
            {
                var location = await this.ctx.LocationData.Where(l => l.BID == BID && l.ID == pageRequest.LocationID.GetValueOrDefault()).FirstOrDefaultAsync();
                var company = await this.ctx.CompanyData.WherePrimary(BID, pageRequest.CompanyID.GetValueOrDefault()).FirstOrDefaultAsync();

                var creditLimit = company.RefundableCredit.GetValueOrDefault() + company.NonRefundableCredit.GetValueOrDefault();

                paymentPageRequest.Amount = pageRequest.Amount;
                paymentPageRequest.CanUseCompanyCredit = creditLimit > 0;
                paymentPageRequest.CompanyCreditBalance = creditLimit;
                paymentPageRequest.CompanyID = company.ID;
                paymentPageRequest.CompanyName = company.Name;
                paymentPageRequest.LocationID = location.ID;
                paymentPageRequest.LocationName = location.Name;
                // paymentPageRequest.ContactID = billingContact?.ContactID ?? (primaryContact?.ContactID ?? 0);

                if (creditLimit > 0)
                {
                    paymentPageRequest.CompanyCreditBalance = creditLimit;
                }
            }

            paymentPageRequest.Orders = paymentPageRequestOrders.ToArray();

            // NOTE: populate apeOrder to paymentPageRequest.Orders after populating level 2 & 3 data
            //paymentPageRequest.Orders = new APEPaymentPageRequestOrders[] { apeOrder };

            var responseString = await GetIFrameElementFromPaymentPageRequest(paymentPageRequest, pageRequest.RegistrationID);
            var result = new JObject
            {
                ["htmlCode"] = responseString.Replace("00000000-0000-0000-0000-000000000000", pageRequest.RegistrationID),
                ["paymentData"] = JToken.FromObject(paymentPageRequest)
            };
            return result;
        }

        private async Task<string> GetIFrameElementFromPaymentPageRequest(APEPaymentPageRequest paymentPageRequest, string registrationID)
        {
            string BASE_URL = await GetPaymentURLAsync(this.BID);
            string APE_URL = $"{BASE_URL}ape";

            using (var client = new HttpClient())
            {

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, APE_URL + "/ui/paymentpage");
                request.Headers.Authorization = new AuthenticationHeaderValue("Registration", registrationID);
                request.Content = new StringContent(JsonConvert.SerializeObject(paymentPageRequest), Encoding.UTF8, "application/json");
                var response = await client.SendAsync(request);
                string responseString = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.OK)
                    return responseString.Replace("src=\"/", "src=\"" + BASE_URL);

                switch (response.StatusCode)
                {
                    case HttpStatusCode.Unauthorized:
                        throw new KeyNotFoundException("Invalid Token");

                    case HttpStatusCode.NotFound:
                        throw new InvalidOperationException("Business key not found");

                    default:
                        throw new InvalidOperationException(responseString);
                }
            }
        }
    }
}
