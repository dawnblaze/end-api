﻿using Endor.EF;
using Endor.Models;
using Endor.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using System.Data;
using Endor.Tenant;
using Endor.Logging.Client;
using Endor.RTM;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Filter Service
    /// </summary>
    public class FilterService : AtomCRUDService<SystemListFilter, int>
    {
        /// <summary>
        /// Filter Service Contructor
        /// </summary>
        public FilterService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// Get Child Service Associations for Filter 
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<SystemListFilter, int>[] GetChildAssociations()
        {
            return null;
        }

        /// <summary>
        /// Read Simple List for User
        /// </summary>
        public async Task<List<SimpleSystemListFilter>> ReadSimpleListForUser(short BID, int CTID, int userID)
        {
            return await ReadUserToEmployee(BID, CTID, userID, ReadSimpleListForEmployee);
        }

        /// <summary>
        /// Read 
        /// </summary>
        public async Task<List<SystemListFilter>> ReadMyListsForUser(short BID, int CTID, int userID)
        {
            return await ReadUserToEmployee(BID, CTID, userID, ReadMyListsForEmployee);
        }

        private async Task<List<T>> ReadUserToEmployee<T>(short BID, int CTID, int userID, Func<short, int, int, Task<List<T>>> reader)
        {
            int? employeeID = (await ctx.UserLink.Where(x => x.BID == BID && x.AuthUserID == userID).Select(x => x.EmployeeID).FirstOrDefaultAsync());

            if (employeeID.HasValue)
                return await reader(BID, CTID, employeeID.Value);
            else
                return new List<T>();
        }

        /// <summary>
        /// Retrieve Simple Lists for employee
        /// </summary>
        public async Task<List<SimpleSystemListFilter>> ReadSimpleListForEmployee(short BID, int CTID, int userLinkID)
        {
            List<SimpleSystemListFilter> results = new List<SimpleSystemListFilter>();

            using (var cmd = ctx.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "SELECT * FROM [System.List.Filter] WHERE TargetClassTypeID = @TargetClassTypeID";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddRange(new SqlParameter[]
                {
                    new SqlParameter("TargetClassTypeID", CTID)
                });
                if (cmd.Connection.State != ConnectionState.Open)
                    await cmd.Connection.OpenAsync();
                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        results.Add(new SimpleSystemListFilter()
                        {
                            ID = /*****************/reader.GetInt32(0),
                            ClassTypeID = /********/reader.GetInt32(1),
                            IsActive = /*********/reader.GetBoolean(2),
                            DisplayName = /*******/reader.GetString(3),
                            TargetClassTypeID = /**/reader.GetInt32(4)
                        });
                    }
                }

            }

            return results;
        }

        /// <summary>
        /// Get MyLists for specified Employee
        /// </summary>
        public async Task<List<SystemListFilter>> ReadMyListsForEmployee(short BID, int CTID, int userLinkID)
        {
            return await ctx.ListFilter.FromSqlRaw("SELECT * FROM [System.List.Filter] WHERE TargetClassTypeID = @TargetClassTypeID",
                new SqlParameter("TargetClassTypeID", CTID))
                .ToListAsync();
        }

        internal override void DoBeforeValidate(SystemListFilter newModel)
        {
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<SystemListFilter> WherePrimary(IQueryable<SystemListFilter> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }
    }
}
