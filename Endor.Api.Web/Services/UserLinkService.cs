﻿using Endor.Api.Web.Classes;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.Tasks;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Service for working with user link
    /// </summary>
    public class UserLinkService
    {
        private readonly ApiContext _ctx;
        private readonly IRTMPushClient _rtmClient;
        private readonly ITaskQueuer _taskQueuer;

        /// <summary>
        /// Business ID
        /// </summary>
        public short BID { get; }

        /// <summary>
        /// Constructs a UserLink service
        /// </summary>
        /// <param name="context">Api context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public UserLinkService(ApiContext context, short bid, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, IMigrationHelper migrationHelper)
        {
            BID = bid;
            _ctx = context;
            migrationHelper.MigrateDb(_ctx);
            _rtmClient = rtmClient;
            _taskQueuer = taskQueuer;
        }

        /// <summary>
        /// Force Delete
        /// </summary>
        /// <param name="requestingUserLinkID"></param>
        /// <param name="userLinkID">UserLink.ID</param>
        public async Task<EntityActionChangeResponse> ForceDelete(short requestingUserLinkID, short userLinkID)
        {
            try
            {
                UserLink requestingUser = await _ctx.UserLink.Where(ul => ul.BID == this.BID && ul.ID == requestingUserLinkID).FirstOrDefaultAsync();

                if (requestingUser == null || requestingUser.UserAccessType < UserAccessType.SupportManager)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Access denied"
                    };
                }

                UserLink userLinkToDelete = await _ctx.UserLink.Where(ul => ul.BID == this.BID && ul.ID == userLinkID).FirstOrDefaultAsync();

                if (userLinkToDelete == null)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "UserLink not found"
                    };
                }


                List<RefreshEntity> refreshes = new List<RefreshEntity>()
                {
                    new RefreshEntity()
                    {
                        BID = BID,
                        ID = userLinkToDelete.ID,
                        ClasstypeID = (int)ClassType.UserLink,
                        RefreshMessageType = RTM.Enums.RefreshMessageType.Delete
                    }
                };

                _ctx.UserLink.Remove(userLinkToDelete);

                var save = _ctx.SaveChanges();

                if (save > 0)
                {
                    await _rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshes });
                    await _taskQueuer.IndexModel(refreshes);
                    return new EntityActionChangeResponse()
                    {
                        Success = true,
                        Message = "Successfully Deleted"
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Nothing to delete"
                    };
                }
            }
            catch (DbUpdateException exc)
            {
                return new EntityActionChangeResponse()
                {
                    Success = false,
                    ErrorMessage = exc.InnerException.Message
                };
            }
            catch (Exception exc)
            {
                return new EntityActionChangeResponse()
                {
                    Success = false,
                    ErrorMessage = exc.ToString()
                };
            }
        }

        /// <summary>
        /// Get Document Creator Employee Name
        /// </summary>
        /// <param name="documents"></param>
        public async Task<List<DMItem>> GetDocumentCreatorEmployeeName(List<DMItem> documents)
        {
            var userLinks = await this._ctx.UserLink.Where(x => x.BID == this.BID && x.EmployeeID != null).Include(x => x.Employee).AsNoTracking().ToListAsync();

            if (userLinks != null && userLinks.Count() > 0)
            {
                documents = this.GetCreatorNameByUserLinkInfo(documents, userLinks);
            }

            return documents;
        }

        /// <summary>
        /// Get Creator Name By UserLink Info
        /// </summary>
        /// <param name="documents"></param>
        /// <param name="userLinks"></param>
        private List<DMItem> GetCreatorNameByUserLinkInfo(List<DMItem> documents, List<UserLink> userLinks)
        {
            foreach (var doc in documents)
            {
                if (doc.CreatedByID != null)
                {
                    var userLink = userLinks.Where(x => x.ID == doc.CreatedByID && x.EmployeeID != null).FirstOrDefault();

                    if (userLink != null && userLink.EmployeeID != null && userLink.Employee != null)
                    {
                        doc.CreatedBy = userLink?.Employee?.ShortName;
                    }
                }

                if (doc.Contents != null && doc.Contents.Count() > 0)
                {
                    doc.Contents = this.GetCreatorNameByUserLinkInfo(doc.Contents, userLinks);
                }
            }

            return documents;
        }
    }
}
