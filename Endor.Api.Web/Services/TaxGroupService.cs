﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Includes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Endor.RTM;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Service that wraps utilizes data storage layer and is accessed primarily from Controller layer
    /// </summary>
    public class TaxGroupService : AtomCRUDService<TaxGroup, short>, ISimpleListableViewService<SimpleTaxGroup, short>
    {
        /// <summary>
        /// Constructs a new TaxGroupService with a number of injeced parameters
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public TaxGroupService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) :
            base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// SimpleTaxGroup list
        /// </summary>
        public DbSet<SimpleTaxGroup> SimpleListSet => ctx.SimpleTaxGroup;

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<TaxGroup, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<TaxGroup, short>[]
            {
                //CreateChildAssociation<TaxGroupService, TaxGroup, short>((a)=>a.SubGroups)
            };
        }

        /// <summary>
        /// Links/Unlink a TaxItem into TaxGroup
        /// </summary>
        /// <param name="taxGroupId">TaxGroup ID</param>
        /// <param name="taxItemId">TaxItem ID</param>
        /// <param name="isLinked">Flag to set the link</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkTaxItem(short taxGroupId, short taxItemId, bool isLinked)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                object[] myParams = {
                    new SqlParameter("@BID", this.BID),
                    new SqlParameter("@TaxGroupID", taxGroupId),
                    new SqlParameter("@TaxItemID", taxItemId),
                    new SqlParameter("@IsLinked", isLinked),
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Accounting.Tax.Group.Action.LinkTaxItem] @BID, @TaxGroupID, @TaxItemID, @IsLinked;", parameters: myParams);
                if (rowResult > 0)
                {
                    await RecomputeTaxRate(taxGroupId);
                    await QueueIndexForModel(taxGroupId);

                    string verb = isLinked ? "linked" : "unlinked";
                    return new EntityActionChangeResponse()
                    {
                        Id = taxGroupId,
                        Message = $"Successfully {verb} the TaxItem to the TaxGroup.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = taxGroupId,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = taxGroupId,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Links/Unlink a Location into TaxGroup
        /// </summary>
        /// <param name="taxGroupId">TaxGroup ID</param>
        /// <param name="LocationId">Location ID</param>
        /// <param name="isLinked">Flag to set the link</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkLocation(short taxGroupId, short LocationId, bool isLinked)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                object[] myParams = {
                    new SqlParameter("@BID", this.BID),
                    new SqlParameter("@TaxGroupID", taxGroupId),
                    new SqlParameter("@LocationID", LocationId),
                    new SqlParameter("@IsLinked", isLinked),
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Accounting.Tax.Group.Action.LinkLocation] @BID, @TaxGroupID, @LocationID, @IsLinked;", parameters: myParams);
                if (rowResult > 0)
                {
                    await QueueIndexForModel(taxGroupId);

                    string verb = isLinked ? "linked" : "unlinked";
                    return new EntityActionChangeResponse()
                    {
                        Id = taxGroupId,
                        Message = $"Successfully { verb } the Location to the TaxGroup.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = taxGroupId,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = taxGroupId,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Recomputes the TaxRate of a TaxGroup
        /// </summary>
        /// <param name="id">TaxGroup ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> RecomputeTaxRate(short id)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                object[] myParams = {
                    new SqlParameter("@bid", this.BID),
                    new SqlParameter("@TaxGroupID", id),
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Accounting.Tax.Group.Action.RecomputeTaxRate] @bid, @TaxGroupID;", parameters: myParams);
                if (rowResult > 0)
                {
                    await QueueIndexForModel(id);

                    return new EntityActionChangeResponse()
                    {
                        Id = id,
                        Message = "Successfully recomputed Tax Rate.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = id,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = id,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }


        internal async Task<Dictionary<byte, short?>> DefaultsForLocationsAsync()
        {
            return await this.ctx.LocationData
                .Where(x => x.BID == this.BID)
                .ToDictionaryAsync(x => x.ID, x => x.DefaultTaxGroupID);
        }

        internal override void DoBeforeValidate(TaxGroup newModel)
        {
        }

        /// <summary>
        /// Appends a string to before cloning a TaxGroup
        /// </summary>
        /// <param name="clone">TaxGroup to clone</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(TaxGroup clone)
        {
            var cloneSource = await this.ctx.TaxGroup
                                .AsNoTracking()
                                .Include(tg => tg.TaxGroupItemLinks)
                                .Include(tg => tg.TaxGroupLocationLinks)
                                .FirstOrDefaultAsync(
                                    tg => tg.BID == this.BID &&
                                    tg.ID == clone.ID
                                );

            clone.TaxGroupItemLinks = cloneSource.TaxGroupItemLinks
                                        .Select(tiLink => new TaxGroupItemLink()
                                        {
                                            BID = tiLink.BID,
                                            GroupID = clone.ID,
                                            ItemID = tiLink.ItemID
                                        }).ToList();

            clone.TaxGroupLocationLinks = cloneSource.TaxGroupLocationLinks
                                        .Select(locLink => new TaxGroupLocationLink()
                                        {
                                            BID = locLink.BID,
                                            GroupID = clone.ID,
                                            LocationID = locLink.LocationID
                                        }).ToList();

            clone.Name = clone.Name + " (Clone)";
            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Executes after a successful TaxGroup creation
        /// </summary>
        /// <param name="model">TaxGroup</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(TaxGroup model, Guid? tempGuid)
        {
            if (model?.ID > 0)
            {
                await RecomputeTaxRate(model.ID);
            }
            await base.DoAfterAddAsync(model, tempGuid);
        }

        /// <summary>
        /// Executes after a successful TaxGroup update
        /// </summary>
        /// <param name="oldModel">old TaxGroup</param>
        /// <param name="model">new TaxGroup</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(TaxGroup oldModel, TaxGroup model, string connectionID)
        {
            if (model?.ID > 0)
            {
                await RecomputeTaxRate(model.ID);
            }
            await base.DoAfterUpdateAsync(oldModel, model, connectionID);
        }

        /// <summary>
        /// Called for each TaxGroup to transform fields
        /// </summary>
        /// <param name="item">TaxGroup</param>
        /// <param name="includeExpander">Child/Parent includes</param>
        /// <returns></returns>
        public override Task MapItem(TaxGroup item, IExpandIncludes includeExpander = null)
        {
            var includes = includeExpander!=null? includeExpander.GetIncludes():null;
            if (includes != null)
            {
                if (item.TaxGroupItemLinks?.Count > 0)
                {
                    switch (includes[TaxGroupIncludes.TaxItemsKey].Level)
                    {
                        case IncludesLevel.Simple:
                            item.SimpleTaxItems = new HashSet<SimpleTaxItem>();
                            foreach (var link in item.TaxGroupItemLinks)
                            {
                                item.SimpleTaxItems.Add(new SimpleTaxItem()
                                {
                                    BID = link.TaxItem.BID,
                                    ID = link.TaxItem.ID,
                                    ClassTypeID = link.TaxItem.ClassTypeID,
                                    IsActive = link.TaxItem.IsActive,
                                    IsDefault = false,
                                    DisplayName = link.TaxItem.Name
                                });
                            }
                            break;
                        case IncludesLevel.Full:
                            foreach (var link in item.TaxGroupItemLinks)
                            {
                                item.TaxItems.Add(link.TaxItem);
                            }
                            break;
                    }
                }

                if (item.TaxGroupLocationLinks?.Count > 0)
                {
                    switch (includes[TaxGroupIncludes.LocationsKey].Level)
                    {
                        case IncludesLevel.Simple:
                            item.SimpleLocations = new HashSet<SimpleLocationData>();

                            foreach (var link in item.TaxGroupLocationLinks)
                            {
                                if (link.Location != null)
                                {
                                    item.SimpleLocations.Add(new SimpleLocationData()
                                    {
                                        BID = link.Location.BID,
                                        ID = link.Location.ID,
                                        ClassTypeID = link.Location.ClassTypeID,
                                        IsActive = (bool)link.Location.IsActive,
                                        IsDefault = link.Location.IsDefault,
                                        DisplayName = link.Location.Name
                                    });
                                }
                            }
                            break;
                        case IncludesLevel.Full:
                            break;
                    }
                }
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// Occurs before the create hits the database
        /// </summary>
        /// <param name="newModel">New Tax Group to be created</param>
        /// <param name="tempGuid">temporary guid to aid in saving related objects</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(TaxGroup newModel, Guid? tempGuid = null)
        {
            await base.DoBeforeCreateAsync(newModel, tempGuid);

            if (newModel.SimpleLocations != null)
            {
                foreach (var simpleLocation in newModel.SimpleLocations)
                {
                    newModel.TaxGroupLocationLinks.Add(new TaxGroupLocationLink() { BID = this.BID, GroupID = newModel.ID, LocationID = simpleLocation.ID });
                }
            }

            if (newModel.SimpleTaxItems != null)
            {
                foreach (var simpleTaxItem in newModel.SimpleTaxItems)
                {
                    newModel.TaxGroupItemLinks.Add(new TaxGroupItemLink() { BID = this.BID, GroupID = newModel.ID, ItemID = simpleTaxItem.ID });
                }
            }
        }

        /// <summary>
        /// Builds a query with primary filters
        /// </summary>
        /// <param name="query">TaxGroup query</param>
        /// <param name="ID">TaxGroup ID</param>
        /// <returns></returns>
        public override IQueryable<TaxGroup> WherePrimary(IQueryable<TaxGroup> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Gets a Filtered SimpleList of TaxGroup
        /// </summary>
        /// <param name="filters">Filters</param>
        /// <returns></returns>
        public async Task<SimpleTaxGroup[]> GetSimpleList(TaxGroupFilter filters)
        {
            return await ctx.Set<TaxGroup>().Where(taxGroup => taxGroup.BID == BID).WhereAll(filters.WherePredicates())
                .Select(taxGroup => new SimpleTaxGroup()
                {
                    TaxRate = taxGroup.TaxRate,
                    ID = taxGroup.ID,
                    BID = taxGroup.BID,
                    ClassTypeID = taxGroup.ClassTypeID,
                    IsActive = taxGroup.IsActive,
                    DisplayName = taxGroup.Name
                }
                ).ToArrayAsync();
        }

        internal async Task<List<TaxGroup>> GetWithFiltersAsync(TaxGroupFilter filters, TaxGroupIncludes includes)
        {
            if (filters != null)
            {
                var results = await this.Where(filters.WherePredicates(), false, includes).ToListAsync();
                return await this.MapCollection(results, includes);
            }

            return await this.GetAsync(includes);
        }
    }
}
