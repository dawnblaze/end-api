﻿using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// System alert service
    /// </summary>
    public class SystemAlertService
    {
        private readonly ApiContext _ctx;

        /// <summary>
        /// Business ID
        /// </summary>
        public short BID { get; }

        /// <summary>
        /// Constructs a System Alert service with injected params
        /// </summary>
        /// <param name="context">Api context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public SystemAlertService(ApiContext context, short bid, IMigrationHelper migrationHelper)
        {
            this._ctx = context;
            migrationHelper.MigrateDb(_ctx);
            this.BID = bid;
        }

        internal async Task<SystemAutomationActionDefinition[]> GetActionsAsync(DataType dataType, Expression<Func<SystemAutomationActionDefinition, bool>> predicate)
        {
            IQueryable<SystemAutomationActionDefinition> query = _ctx.SystemAutomationActionDefinition.Include("DataTypeLinks").Where(predicate);

            if (dataType != DataType.None)
            {
                query = query.Where(x => x.AppliesToAllDataTypes || x.DataTypeLinks.FirstOrDefault(y => y.DataType == dataType) != null);
            }
            else
            {
                query = query.Where(x => x.AppliesToAllDataTypes);
            }

            return await query.ToArrayAsync();
        }

        internal async Task<SystemAutomationTriggerDefinition[]> GetTriggersAsync(DataType dataType, Expression<Func<SystemAutomationTriggerDefinition, bool>> predicate)
        {
            IQueryable<SystemAutomationTriggerDefinition> query = _ctx.SystemAutomationTriggerDefinition.Where(predicate);

            if (dataType != DataType.None)
            {
                query = query.Where(x => x.DataType == dataType);
            }

            return await query.ToArrayAsync();
        }
    }
}
