using System;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Collections.Generic;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Custom Field Layout Element Service
    /// </summary>
    public class CustomFieldLayoutElementService : AtomCRUDService<CustomFieldLayoutElement, int>,
        IDoBeforeCreateUpdateWithParent<CustomFieldLayoutElement, CustomFieldLayoutElement>,
        IDoBeforeCreateUpdateWithParent<CustomFieldLayoutElement, CustomFieldLayoutDefinition>
    {
        /// <summary>
        /// /// Constructs a CustomFieldLayoutContainerService service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CustomFieldLayoutElementService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "Children" };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">CustomFieldLayoutDefinition ID</param>
        /// <returns></returns>
        public override IQueryable<CustomFieldLayoutElement> WherePrimary(IQueryable<CustomFieldLayoutElement> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<CustomFieldLayoutElement, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<CustomFieldLayoutElement, int>[]{
                CreateChildAssociation<CustomFieldLayoutElementService, CustomFieldLayoutElement, int>(e => e.Elements),
            };
        }

        internal override void DoBeforeValidate(CustomFieldLayoutElement newModel)
        {
            
        }

        public override Task DoBeforeDeleteAsync(CustomFieldLayoutElement model)
        {
            if (model.Definition != null)
            {
                if (ctx.Entry(model.Definition).State != EntityState.Detached)
                    ctx.Entry(model.Definition).State = EntityState.Detached;
                model.Definition = null;
            }

            /* END-11708: Throws exception when deleting parent without deleting child first.
             * The DELETE statement conflicted with the SAME TABLE REFERENCE constraint 
             * "FK_CustomField.Layout.Element_CustomField.Layout.Element". 
             * The conflict occurred in database "Dev.Endor.Business.DB1", 
             * table "dbo.CustomField.Layout.Element". */
            elements = new List<CustomFieldLayoutElement>();
            AddElementRecursive(model);
            elements.ForEach(element => ctx.CustomFieldLayoutElement.Remove(element));

            return base.DoBeforeDeleteAsync(model);
        }

        List<CustomFieldLayoutElement> elements;
        private void AddElementRecursive(CustomFieldLayoutElement parent)
        {
            if (parent.ParentID != null)
            {
                var children = GetChildren(parent);
                AddChildren(children);
                parent.ParentID = null;
                AddElementRecursive(parent);
            }
            else
            {
                var element = elements.FirstOrDefault(p => p.ParentID != null);
                if (element != null)
                {
                    var children = GetChildren(element);
                    AddChildren(children);
                    elements.Remove(element);
                    element.ParentID = null;
                    elements.Add(element);
                    AddElementRecursive(element); 
                }
            }
        }

        private List<CustomFieldLayoutElement> GetChildren(CustomFieldLayoutElement parent)
        {
            return ctx.CustomFieldLayoutElement.Where(cf => cf.BID == parent.BID && cf.ParentID == parent.ID).ToList();
        }

        private void AddChildren(List<CustomFieldLayoutElement> children)
        {
            foreach (var child in children)
            {
                if (child.ParentID == null)
                    elements.Prepend(child);
                else
                    elements.Add(child);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public void DoBeforeCreateWithParent(CustomFieldLayoutElement child, CustomFieldLayoutElement parent)
        {
            child.BID = this.BID;
            child.LayoutID = parent.LayoutID;
            child.ParentID = parent.ID;
        }

        public override async Task DoBeforeCreateAsync(CustomFieldLayoutElement newModel, Guid? tempGuid = null)
        {
            if (newModel.Definition != null)
            {
                newModel.Definition.BID = this.BID;
                if (newModel.Definition.DataType == DataType.None)
                    newModel.Definition.DataType = ctx.EnumDataType.FirstOrDefault(x => x.ID == newModel.Definition.DataType).ID;

                var svc = new CustomFieldDefinitionService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
                CustomFieldDefinition oldDefinition = this.ctx.CustomFieldDefinition.WherePrimary(BID, x => x.ID == newModel.DefinitionID).AsNoTracking().FirstOrDefault();
                if (oldDefinition == null)
                {
                    // add a new definition
                    await svc.DoBeforeCreateAsync(newModel.Definition);
                    newModel.DefinitionID = newModel.Definition.ID;
                }
                else
                {
                    // update an existing definition
                    // don't add a duplicate newModel.Definition if there is an existing Definition
                    await svc.DoBeforeUpdateAsync(oldDefinition, newModel.Definition);
                    newModel.DefinitionID = newModel.Definition.ID;
                }
            }

            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// CustomFieldLayoutElementService Override of DoBeforeUpdateAsync
        /// </summary>
        /// <param name="oldModel">Old CustomFieldLayoutElement</param>
        /// <param name="newModel">Updated CustomFieldLayoutElement</param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(CustomFieldLayoutElement oldModel, CustomFieldLayoutElement newModel)
        {
            if (newModel != null && (oldModel == null || oldModel.Definition == null) && newModel.Definition != null)
            {
                //add new definition
                var svc = new CustomFieldDefinitionService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
                await svc.DoBeforeCreateAsync(newModel.Definition);
                newModel.DefinitionID = newModel.Definition.ID;
            }
            else if (newModel != null && newModel.DefinitionID != null && newModel.Definition != null)
            {
                //update old definition
                CustomFieldDefinition oldDefinition = this.ctx.CustomFieldDefinition.WherePrimary(BID, x => x.ID == newModel.DefinitionID).AsNoTracking().FirstOrDefault();
                var svc = new CustomFieldDefinitionService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
                await svc.DoBeforeUpdateAsync(oldDefinition, newModel.Definition);
                newModel.DefinitionID = newModel.Definition.ID;
            }

            await base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        public void DoBeforeUpdateWithParent(CustomFieldLayoutElement child, CustomFieldLayoutElement oldParent, CustomFieldLayoutElement newParent)
        {
            child.BID = this.BID;
            child.LayoutID = newParent.LayoutID;
            child.ParentID = newParent.ID;
        }

        /// <summary>
        /// Gets the old model
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task<CustomFieldLayoutElement> GetOldModelAsync(CustomFieldLayoutElement newModel)
        {
            var oldModel = await WherePrimary(ctx.CustomFieldLayoutElement.Include(x => x.Definition).Include(x => x.Elements).AsNoTracking(), newModel.ID).FirstOrDefaultAsync();

            return oldModel;
        }

        public void DoBeforeCreateWithParent(CustomFieldLayoutElement child, CustomFieldLayoutDefinition parent)
        {
            child.LayoutID = parent.ID;
        }

        public void DoBeforeUpdateWithParent(CustomFieldLayoutElement child, CustomFieldLayoutDefinition oldParent, CustomFieldLayoutDefinition newParent)
        {
            child.LayoutID = newParent.ID;
        }
    }
}