﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Endor.RTM;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Alert Definition Service
    /// </summary>
    public class AlertDefinitionService : AtomCRUDService<AlertDefinition, short>, ISimpleListableViewService<SimpleAlertDefinition, short>
    {
        /// <summary>
        /// gets simple list set
        /// </summary>
        public DbSet<SimpleAlertDefinition> SimpleListSet => ctx.SimpleAlertDefinition;

        /// <summary>
        /// Constructs a alert definition service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public AlertDefinitionService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }


        /// <summary>
        /// Returns Alert Definition based on filters supplied in
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<List<AlertDefinition>> GetWithFiltersAsync(AlertDefinitionFilter filters)
        {
            if (filters != null && filters.HasFilters)
            {
                return await this.GetWhere(filters.WherePredicates());
            }

            return await this.GetAsync();
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "Actions" };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// AlertDefinition Service Override of DoBeforeCreate
        /// </summary>
        /// <param name="newModel">AlertDefinition</param>
        /// <param name="tempGuid">Optionala Temp Guid</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(AlertDefinition newModel, Guid? tempGuid = null)
        {
            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// AlertDefinition Service Override of DoBeforeUpdateAsync
        /// </summary>
        /// <param name="oldModel">Old AlertDefinition</param>
        /// <param name="updatedModel">Updated AlertDefinition</param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(AlertDefinition oldModel, AlertDefinition updatedModel)
        {
            await base.DoBeforeUpdateAsync(oldModel, updatedModel);
        }

        /// <summary>
        /// AlertDefinition Service Override of DoBeforeCloneAsync
        /// </summary>
        /// <param name="clone">Cloned AlertDefinition</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(AlertDefinition clone)
        {
            clone.Name = clone.Name + " (Clone)";
            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<AlertDefinition, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<AlertDefinition, short>[]{
                CreateChildAssociation<AlertDefinitionActionService, AlertDefinitionAction, short>((emp) => emp.Actions),
            };
        }

        internal override void DoBeforeValidate(AlertDefinition newModel) { }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<AlertDefinition> WherePrimary(IQueryable<AlertDefinition> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Updates the LastRunDT and CummRunCount fields for the Alert Definition.  This is called when the Alert is fired to record that is was run.
        /// </summary>
        /// <param name="Id">Alert Definition ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> PostResults(short Id)
        {
            AlertDefinition existing = this.ctx.AlertDefinition.WherePrimary(this.BID, Id).FirstOrDefault();
            if (existing == null)
            {

                return new EntityActionChangeResponse
                {
                    Id = Id,
                    ErrorMessage = "Alert Definition does not exist",
                    Success = false
                };
            }
            else
            {
                try
                {
                    existing.LastRunDT = DateTime.UtcNow;
                    existing.CummRunCount = (existing.CummRunCount ?? 0) + 1;
                    await this.ctx.SaveChangesAsync();

                    return new EntityActionChangeResponse
                    {
                        Id = Id,
                        Message = "Successfully posted results.",
                        Success = true
                    };
                }
                catch (Exception ex)
                {
                    return new EntityActionChangeResponse
                    {
                        Id = Id,
                        Message = ex.Message,
                        Success = false,
                        ErrorMessage = ex.Message
                    };
                }
            }
        }

        /// <summary>
        /// Resets the LastRunDT and CummRunCount fields to NULL.
        /// </summary>
        /// <param name="Id">Alert Definition ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> ResetTimer(short Id)
        {
            AlertDefinition existing = this.ctx.AlertDefinition.WherePrimary(this.BID, Id).FirstOrDefault();
            if (existing == null)
            {

                return new EntityActionChangeResponse
                {
                    Id = Id,
                    ErrorMessage = "Alert Definition does not exist",
                    Success = false
                };
            }
            else
            {
                try
                {
                    existing.LastRunDT = null;
                    existing.CummRunCount = null;
                    await this.ctx.SaveChangesAsync();

                    return new EntityActionChangeResponse
                    {
                        Id = Id,
                        Message = "Successfully reset timer.",
                        Success = true
                    };
                }
                catch (Exception ex)
                {
                    return new EntityActionChangeResponse
                    {
                        Id = Id,
                        Message = ex.Message,
                        Success = false,
                        ErrorMessage = ex.Message
                    };
                }
            }
        }

        /// <summary>
        /// Resets the LastRunDT and CummRunCount fields to NULL for all Alerts for the given BID. 
        /// </summary>
        /// <returns></returns>
        public async Task<EntitiesActionChangeResponse> ResetAllTimers()
        {
            var allExisting = await this.ctx.AlertDefinition.Where(x => x.BID == this.BID).ToListAsync();
            if (allExisting == null || allExisting.Count < 1)
            {
                return new EntitiesActionChangeResponse
                {
                    ErrorMessage = "Alert Definitions do not exist",
                    Success = false
                };
            }
            else
            {
                try
                {
                    allExisting.ForEach((existing) =>
                    {
                        existing.LastRunDT = null;
                        existing.CummRunCount = null;
                    });
                    await this.ctx.SaveChangesAsync();

                    return new EntitiesActionChangeResponse
                    {
                        Message = "Successfully reset timers.",
                        Success = true
                    };
                }
                catch (Exception ex)
                {
                    return new EntitiesActionChangeResponse
                    {
                        Message = ex.Message,
                        Success = false,
                        ErrorMessage = ex.Message
                    };
                }
            }
        }

    }

}
