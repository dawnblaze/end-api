﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.RTM;
using Endor.Api.Web.Classes;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Threading;
using Microsoft.Data.SqlClient;
using System.Text;
using Endor.DocumentStorage.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Endor.RTM.Models;
using Endor.RTM.Enums;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Business Service
    /// </summary>
    public class BusinessService : AtomCRUDService<BusinessData, short>, ILocatorParentService<BusinessData, short>
    {

        private EndorOptions _endorOptions = null;

        /// <summary>
        /// Business Service Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">ITaskQueuer</param>
        /// <param name="cache">ITenantDataCache</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public BusinessService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// Get Child Service Associations for BusinessData 
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<BusinessData, short>[] GetChildAssociations()
        {
            return null;
        }

        /// <summary>
        /// Returns a collection of a BusinessData's BaseLocator
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IEnumerable<BaseLocator<short, BusinessData>> GetLocators(BusinessData model) => model.BusinessLocators;

        /// <summary>
        /// Assigns a collection of locators to a BusinessData Model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="value"></param>
        public void SetLocators(BusinessData model, IEnumerable<BaseLocator<short, BusinessData>> value) => model.BusinessLocators = value as ICollection<BusinessLocator>;

        internal override void DoBeforeValidate(BusinessData newModel)
        {
            this.StripOutTemporaryLocators(newModel);
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<BusinessData> WherePrimary(IQueryable<BusinessData> query, short ID)
        {
            // the ID is actually the BID and all Business.Data has ID of 1
            return query.WherePrimary(ID, b => b.ID == 1);
        }

        /// <summary>
        /// Updates Association Type of a business
        /// </summary>
        /// <param name="newAssociationType"></param>
        /// <param name="BID"></param>
        /// <returns></returns>
        public async Task<GenericResponse<BusinessData>> UpdateAssociationType(byte? newAssociationType, short BID)
        {
            var response = new GenericResponse<BusinessData>();
            var business = this.ctx.BusinessData.Where(B => B.BID == BID).FirstOrDefault();
            if (business == null)
            {
                response.Success = false;
                response.Data = business;
                response.ErrorMessage = "unable to get Business data from the BID:" + BID;
            }
            else
            {
                try
                {
                    business.AssociationType = newAssociationType;
                    await this.ctx.SaveChangesAsync();
                    response.Success = true;
                    response.Message = "Sucessfully set new Association type";
                }
                catch (Exception ex)
                {
                    response.ErrorMessage = ex.Message;
                }
                response.Data = business;
            }

            return response;
        }

        /// <summary>
        /// Delete a business with an optional query testonly param
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="options"></param>
        /// <param name="cache"></param>
        /// <param name="TestOnly"></param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse<short>> DeleteAsync(short ID, EndorOptions options, ITenantDataCache cache, bool TestOnly = false)
        {
            try
            {
                await cache.Get(ID); // Assures that the cache has the business tenant data loaded before deletion.

                //delete business from endor.business    
                await ctx.Database.ExecuteSqlRawAsync2("EXEC [Util.Business.Delete] @BID = @BID, @TestOnly = @TestOnly", CancellationToken.None, new SqlParameter("@BID", ID), new SqlParameter("@TestOnly", (bool)TestOnly ? 1 : 0));

                //delete business from endor.master                    
                using (var client = new HttpClient())
                {
                    HttpResponseMessage response = null;
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, options.AuthOrigin + "/api/business/" + ID.ToString() + "?TestOnly=" + TestOnly);
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToBase64String(Encoding.UTF8.GetBytes(options.TenantSecret)));
                    response = await client.SendAsync(request);

                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            //delete business from blob storage (including lucene indexes)
                            if (!TestOnly)
                            {
                                await DeleteBlobStorage(ID, cache);
                                cache.InvalidateCache(ID);
                            }
                            return new EntityActionChangeResponse<short>()
                            {
                                Id = ID,
                                Success = true,
                                Message = "Sucessfully deleted business data." + (TestOnly ? " This was only a test DELETE, changes are not saved." : "")
                            };
                        default:
                            return new EntityActionChangeResponse<short>()
                            {
                                Success = response.StatusCode == HttpStatusCode.OK ? true : false,
                                ErrorMessage = response.ReasonPhrase
                            };
                    }
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        private EntityActionChangeResponse GetNoChangeRequiredResponse(short id)
        {
            return new EntityActionChangeResponse()
            {
                Id = id,
                Success = true,
                Message = "No Change Required"
            };
        }

        private RefreshEntity GetRefreshMessage(int ID, int classTypeID, RefreshMessageType refreshType)
        {
            return new RefreshEntity()
            {
                BID = this.BID,
                ClasstypeID = classTypeID,
                ID = ID,
                DateTime = DateTime.UtcNow,
                RefreshMessageType = refreshType,
            };
        }

        /// <summary>
        /// Unlinks the specified Time Zone of the Business
        /// </summary>
        /// <param name="BID"></param>
        /// <param name="timeZoneId"></param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkTimeZoneToBusiness(short BID, short timeZoneId)
        {
            //check if link exists
            var link = await ctx.BusinessTimeZoneLink.FirstOrDefaultAsync(b => b.BID == BID && b.TimeZoneID == timeZoneId);
            if (link == null)
            {
                //add a link
                BusinessTimeZoneLink tl = new BusinessTimeZoneLink
                {
                    BID = this.BID,
                    TimeZoneID = timeZoneId
                };
                ctx.BusinessTimeZoneLink.Add(tl);

                try
                {
                    await ctx.SaveChangesAsync();
                    return new EntityActionChangeResponse()
                    {
                        Id = timeZoneId,
                        Success = true,
                        Message = "Successfully linked Time Zone"
                    };
                }
                catch (Exception e)
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = timeZoneId,
                        Success = false,
                        Message = e.Message
                    };
                }
            } else
            {
                return GetNoChangeRequiredResponse(timeZoneId);
            }
        }

        /// <summary>
        /// Unlinks the specified Time Zone of the Business
        /// </summary>
        /// <param name="BID"></param>
        /// <param name="timeZoneId"></param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> UnlinkTimeZoneToBusiness(short BID, short timeZoneId)
        {
            var timeZoneLink = await ctx.BusinessTimeZoneLink.FirstOrDefaultAsync(b => b.BID == BID && b.TimeZoneID == timeZoneId);

            if (timeZoneLink != null)
            {
                ctx.BusinessTimeZoneLink.Remove(timeZoneLink);
                try
                {
                    await ctx.SaveChangesAsync();
                    return new EntityActionChangeResponse()
                    {
                        Id = timeZoneId,
                        Success = true,
                        Message = "Successfully unlinked Time Zone"
                    };
                }
                catch (Exception e)
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = timeZoneId,
                        Success = false,
                        Message = e.Message
                    };
                }
            }
            else
            {
                return GetNoChangeRequiredResponse(timeZoneId);
            }

        }

        /// <summary>
        /// Deletes a blobstorage folder for a given business ID
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        private async Task<bool> DeleteBlobStorage(short ID, ITenantDataCache cache)
        {
            DMID dmid = new DMID();
            var ctx = new StorageContext(ID, BucketRequest.All, dmid);
            var results = await new DocumentManager(cache, ctx).DeleteBusinessBlobStorage();

            return results;
        }

        /// <summary>
        /// Creates a new Business Object, Location Object, Employee Object, and User Object. 
        /// </summary>
        /// <param name="TemplateBID"></param>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="EmailAddress"></param>
        /// <param name="LocationName"></param>
        /// <param name="NewBusinessID"></param>
        /// <param name="options"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse<short>> CreateBusinessAsync(short TemplateBID, string FirstName, string LastName, string EmailAddress, string LocationName, short? NewBusinessID, EndorOptions options, ITenantDataCache cache, IMigrationHelper migrationHelper)
        {
            try
            {
                this._endorOptions = options;

                //Call CreateBusiness on AUTH              
                using (var client = new HttpClient())
                {
                    HttpResponseMessage response = null;
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, options.AuthOrigin + "/api/business" + "?TemplateBID=" + TemplateBID + "&FirstName=" + FirstName + "&LastName=" + LastName + "&EmailAddress=" + EmailAddress + "&LocationName=" + LocationName + "&NewBusinessID=" + NewBusinessID);
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToBase64String(Encoding.UTF8.GetBytes(options.TenantSecret)));
                    response = await client.SendAsync(request);
                    string responseString = await response.Content.ReadAsStringAsync();
                    CreateBusinessResponse createResponse = JsonConvert.DeserializeObject<CreateBusinessResponse>(responseString);

                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            NewBusinessID = createResponse.BID;
                            break;
                        default:
                            return new EntityActionChangeResponse<short>()
                            {
                                Id = createResponse.BID,
                                Success = createResponse.Success,
                                ErrorMessage = createResponse.Response
                            };
                    }
                }

                //Create Business Object
                var business = await this.CreateBusinessDataAsync(TemplateBID, NewBusinessID.Value);
                if (!business.Success)
                {
                    await DeleteAsync(NewBusinessID.Value, options, cache, false);
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        Id = NewBusinessID.GetValueOrDefault(-1),
                        ErrorMessage = business.ErrorMessage
                    };
                }

                //Create Location Object
                var location = await this.CreateLocationDataAsync(business.Id, LocationName);
                if (!location.Success)
                {
                    await DeleteAsync(NewBusinessID.Value, options, cache, false);
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        ErrorMessage = location.ErrorMessage
                    };
                }

                //Clone Tables from Template BID
                var cloneTables = await this.CloneTemplateTables(TemplateBID, NewBusinessID.Value, location.Id);
                if (!cloneTables.Success)
                {
                    await DeleteAsync(NewBusinessID.Value, options, cache, false);
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        Id = NewBusinessID.GetValueOrDefault(-1),
                        ErrorMessage = cloneTables.ErrorMessage
                    };
                }

                //Create TaxGroup.LocationLink on all Active TaxGroup with the new BusIness ID and Location ID
                var locationLink = await this.CreateTaxGroupLocationLinkAsync(business.Id, location.Id);
                if (!locationLink.Success)
                {
                    await DeleteAsync(NewBusinessID.Value, options, cache, false);
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        ErrorMessage = locationLink.ErrorMessage
                    };
                }

                //Clone Employee Roles to new Business
                var employeeRole = await CloneEmployeeRoles(business.Id);
                if (!employeeRole.Success)
                {
                    await DeleteAsync(NewBusinessID.Value, options, cache, false);
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        ErrorMessage = employeeRole.ErrorMessage
                    };
                }

                //Reset the IDs on the GetNextID Counter
                var idResetAll = await this.IdResetAllAsync(business.Id, false);
                if (!idResetAll.Success)
                {
                    await DeleteAsync(NewBusinessID.Value, options, cache, false);
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        Id = NewBusinessID.GetValueOrDefault(-1),
                        ErrorMessage = business.ErrorMessage
                    };
                }

                //Create Employee Object
                if (location.Id > 0)
                {
                    var employee = await this.CreateEmployeeDataAsync(business.Id, location.Id, LastName, FirstName);
                    if (!employee.Success)
                    {
                        await DeleteAsync(NewBusinessID.Value, options, cache, false);
                        return new EntityActionChangeResponse<short>()
                        {
                            Success = false,
                            ErrorMessage = employee.ErrorMessage
                        };
                    }

                    ////Create User Object / Call CreateUser on AUTH
                    if (!string.IsNullOrEmpty(EmailAddress))
                    {
                        //Create EmployeeLocator Object
                        var locator = await this.CreateEmployeeLocatorAsync(business.Id, employee.Id, EmailAddress);
                        if (!locator.Success)
                        {
                            await DeleteAsync(NewBusinessID.Value, options, cache, false);
                            return new EntityActionChangeResponse<short>()
                            {
                                Success = false,
                                ErrorMessage = locator.ErrorMessage
                            };
                        }

                        if (EmailAddress.ToLower() != "test@corebridge.net") //Don't run when executing unit tests...
                        {
                            var userLink = await this.CreateUserLinkAsync(business.Id, employee.Id, EmailAddress);
                            if (!userLink.Success)
                            {
                                await DeleteAsync(NewBusinessID.Value, options, cache, false);
                                return new EntityActionChangeResponse<short>()
                                {
                                    Success = false,
                                    ErrorMessage = userLink.ErrorMessage
                                };
                            }
                        }
                    }
                }

                //Create Blob and Lucene Index
                Array values = Enum.GetValues(typeof(ClassType));
                foreach (ClassType ct in values)
                {
                    await this.taskQueuer.IndexClasstype(NewBusinessID.Value, ct.ID());
                }

                return new EntityActionChangeResponse<short>()
                {
                    Success = true,
                    Id = NewBusinessID.GetValueOrDefault(-1),
                    Message = $"Successfully created Business BID {NewBusinessID}"
                };
            }
            catch (Exception ex)
            {
                await DeleteAsync(NewBusinessID.Value, options, cache, false);
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    Id = NewBusinessID.GetValueOrDefault(-1),
                    ErrorMessage = ex.Message
                };
            }
        }

        internal async Task<List<SimpleEnumTimeZone>> GetTimezoneSimpleListAsync(short bid)
        {
            
            var joinQuery = from businessTimeZoneLink in ctx.BusinessTimeZoneLink
                            join timeZone in ctx.SimpleEnumTimeZone on businessTimeZoneLink.TimeZoneID equals timeZone.ID
                            where businessTimeZoneLink.BID == bid
                            select new SimpleEnumTimeZone()
                            {
                                ID = timeZone.ID,
                                DisplayName = timeZone.DisplayName,
                                HasImage = false,
                                IsActive = true

                            };
            return await joinQuery.ToListAsync<SimpleEnumTimeZone>();
        }

        /// <summary>
        /// Clones an existing Business to a new Business
        /// </summary>
        /// <param name="BID"></param>
        /// <param name="options"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse<short>> CloneAsync(short BID, EndorOptions options, ITenantDataCache cache)
        {
            return await CreateBusinessAsync(BID, null, null, null, null, null, options, cache, migrationHelper);
        }


        /// <summary>
        /// Changes the BusinessData's IsActive status
        /// </summary>
        /// <param name="ID">Business ID</param>
        /// <param name="active">Whether to set to Active or Inactive</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public override async Task<EntityActionChangeResponse<short>> SetActive(short ID, bool active, string connectionID)
        {
            EntityActionChangeResponse<short> resp = null;
            try
            {
                var business = await ctx.BusinessData.FirstOrDefaultAsync(b => b.BID == ID);
                if (business == null)
                    return new EntityActionChangeResponse<short>()
                    {
                        Message = "Business not found",
                        ErrorMessage = "Business not found",
                        Success = false,
                        Id = ID
                    };

                business.IsActive = active;
                var upResp = await base.UpdateAsync(business, connectionID);
                if (upResp.IsActive == active)
                {
                    var result = await this.SetActiveAuthDB((active ? "setactive" : "setinactive"));
                    if (result)
                    {
                        resp = new EntityActionChangeResponse<short>()
                        {
                            Message = $"{upResp.Name} status updated to {(active ? "active" : "inactive")}.",
                            Success = true,
                            Id = ID
                        };
                    }
                    else
                    {
                        // rollback changes
                        business.IsActive = !active;
                        await base.UpdateAsync(business, connectionID);
                        resp = new EntityActionChangeResponse<short>()
                        {
                            Message = "Unable to update active status.",
                            ErrorMessage = "Unable to update active status.",
                            Success = false,
                            Id = ID
                        };
                    }
                }
                else
                {
                    resp = new EntityActionChangeResponse<short>()
                    {
                        Message = "Unable to update active status.",
                        ErrorMessage = "Unable to update active status.",
                        Success = false,
                        Id = ID
                    };
                }
                    
            }
            catch (Exception ex)
            {
                resp = new EntityActionChangeResponse<short>()
                {
                    Message = ex.Message,
                    ErrorMessage = ex.Message,
                    Success = false,
                    Id = ID
                };
            }

            return resp;
        }

        /// <summary>
        /// Retrieves a Business by its BID
        /// </summary>
        /// <param name="BID">Business ID</param>
        /// <returns></returns>
        public async Task<IActionResult> RetrieveByBID(short BID)
        {
            var model = await ctx.BusinessData.FirstOrDefaultAsync(b => b.BID == BID);
            if (model == null)
                return new NotFoundResult();

            return new OkObjectResult(model);
        }

        /// <summary>
        /// Updates a BusinessData by its BID
        /// </summary>
        /// <param name="BID">Business ID</param>
        /// <param name="newModel">Updated BusinessData object</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public async Task<GenericResponse<BusinessData>> DoUpdate(short BID, BusinessData newModel, string connectionID)
        {
            if (newModel == null)
                return new GenericResponse<BusinessData>()
                {
                    Success = false,
                    ErrorMessage = "Update body must not be empty."
                };

            try
            {
                SetBIDAndID(BID, newModel);
                var oldModel = await GetOldModelAsync(newModel);

                //Nothing to update
                if (oldModel == null)
                    return new GenericResponse<BusinessData>()
                    {
                        Success = false,
                        ErrorMessage = $"Business with BID {BID} not found."
                    };

                await DoBeforeUpdateAsync(oldModel, newModel);

                await ctx.SaveChangesAsync();

                await DoAfterUpdateAsync(oldModel, newModel, connectionID);
                return new GenericResponse<BusinessData>()
                {
                    Success = true,
                    Message = "Successfully updated Business",
                    Data = newModel
                };
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Update failed", e);
                return null;
            }

        }

        /// <summary>
        /// Sets the BID and ID to the correct values
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="business">Business model</param>
        private void SetBIDAndID(short bid, BusinessData business)
        {
            business.BID = bid;
            business.ID = 1;
        }

        /// <summary>
        /// Create Business
        /// </summary>
        /// <param name="TemplateBID"></param>
        /// <param name="NewBusinessID"></param>
        private async Task<EntityActionChangeResponse<short>> CreateBusinessDataAsync(short TemplateBID, short NewBusinessID)
        {
            try
            {
                //check for duplicate BID
                bool businessExists = await ctx.Set<BusinessData>().FirstOrDefaultAsync(d => d.BID == TemplateBID) != null;
                if (businessExists)
                {
                    BusinessData templateBusiness = new BusinessData();
                    templateBusiness = ctx.BusinessData.FirstOrDefault(i => i.BID == TemplateBID);
                    templateBusiness.BID = NewBusinessID;
                    templateBusiness.CreatedDate = DateTime.UtcNow;
                    templateBusiness.ModifiedDT = DateTime.UtcNow;

                    bool businessInDatabase = await ctx.Set<BusinessData>().FirstOrDefaultAsync(d => d.BID == NewBusinessID) != null;
                    if (businessInDatabase)
                    {
                        return new EntityActionChangeResponse<short>()
                        {
                            Id = NewBusinessID,
                            Success = false,
                            ErrorMessage = "Business already found in the BUSINESS Database."
                        };
                    }
                    else
                    {
                        ctx.BusinessData.Add(templateBusiness);
                    }

                    await ctx.SaveChangesAsync();

                    // Insert Common US TimeZone for newly added business.
                    await ctx.Database.ExecuteSqlRawAsync("EXEC dbo.[Util.Table.CopyDefaultRecords] 'Business.TimeZone.Link', @IDFieldName = 'TimeZoneID'");
                    NewBusinessID = templateBusiness.BID;
                }
                else
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        ErrorMessage = "Template business ID not found."
                    };
                }

                return new EntityActionChangeResponse<short>()
                {
                    Id = NewBusinessID,
                    Success = true,
                    Message = "Successfully created business."
                };

            }
            catch (Exception e)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    ErrorMessage = "Create business failed - " + e.Message
                };
            }

        }

        private async Task<EntityActionChangeResponse<short>> CloneEmployeeRoles(short BID)
        {
            try
            {
                await ctx.Database.ExecuteSqlRawAsync("EXEC [Util.Table.CopyDefaultRecords] 'Employee.Role';");
                return new EntityActionChangeResponse<short>()
                {
                    Id = -1,
                    Success = true,
                    Message = $"Successfully cloned Employee Roles to Business."
                };
            }
            catch (Exception e)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    ErrorMessage = "Cloning Employee Roles failed - " + e.Message
                };
            }
        }

        /// <summary>
        /// Create Location
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="name">Location name</param>
        private async Task<EntityActionChangeResponse<byte>> CreateLocationDataAsync(short bid, string name)
        {
            try
            {
                if (string.IsNullOrEmpty(name))
                {
                    name = "Main";
                }
                LocationData newModel = new LocationData()
                {
                    BID = bid,
                    IsActive = true,
                    IsDefault = true,
                    LegalName = name,
                    Name = name,
                };

                Guid tempGuid = Guid.NewGuid();
                var result = await new LocationService(ctx, bid, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper).CreateAsync(newModel, tempGuid);
                if (result == null)
                {
                    return new EntityActionChangeResponse<byte>()
                    {
                        Success = false,
                        ErrorMessage = "CreateLocationDataAsync returned null"
                    };
                }

                return new EntityActionChangeResponse<byte>()
                {
                    Id = result.ID,
                    Success = true,
                    Message = $"Successfully created location."
                };
            }
            catch (Exception e)
            {
                return new EntityActionChangeResponse<byte>()
                {
                    Success = false,
                    ErrorMessage = "Create location failed - " + e.Message
                };
            }
        }

        /// <summary>
        /// Create TaxGroupLocationLink
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="locationId">Location ID</param>
        private async Task<EntityActionChangeResponse<byte>> CreateTaxGroupLocationLinkAsync(short bid, byte locationId)
        {
            try
            {

                //get all active taxgroup and create location link for each...
                short[] taxGroupIDs = await ctx.TaxGroup.Where(x => x.IsActive == true && x.BID == bid).Select(i => i.ID).ToArrayAsync();

                foreach (short taxGroupID in taxGroupIDs)
                {
                    ctx.TaxGroupLocationLink.Add(new TaxGroupLocationLink()
                    {
                        BID = bid,
                        GroupID = taxGroupID,
                        LocationID = locationId
                    });
                }
                ctx.SaveChanges();

                return new EntityActionChangeResponse<byte>()
                {
                    Id = locationId,
                    Success = true,
                    Message = $"Successfully created taxgroup location link."
                };
            }
            catch (Exception e)
            {
                return new EntityActionChangeResponse<byte>()
                {
                    Success = false,
                    ErrorMessage = "Create taxgroup location link failed - " + e.Message
                };
            }
        }


        /// <summary>
        /// Create Employee
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="locationId">Location ID</param>
        /// <param name="lastName">The lat name of the employee</param>
        /// <param name="firstName">The first name of the employee</param>
        private async Task<EntityActionChangeResponse<short>> CreateEmployeeDataAsync(short bid, byte locationId, string lastName, string firstName)
        {
            try
            {
                if (string.IsNullOrEmpty(firstName))
                {
                    firstName = "Default";
                }
                if (string.IsNullOrEmpty(lastName))
                {
                    lastName = "Default";
                }

                EmployeeData newModel = new EmployeeData()
                {
                    BID = bid,
                    IsActive = true,
                    First = firstName,
                    Last = lastName,
                    LocationID = locationId
                };


                Guid tempGuid = Guid.NewGuid();
                var employee = await new EmployeeService(ctx, bid, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper).CreateAsync(newModel, tempGuid);
                if (employee == null)
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        ErrorMessage = "EmployeeService.CreateAsync returned null"
                    };
                }

                return new EntityActionChangeResponse<short>()
                {
                    Id = employee.ID,
                    Success = true,
                    Message = $"Successfully created employee."
                };
            }
            catch (Exception e)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    ErrorMessage = "Create employee failed - " + e.Message
                };
            }
        }

        /// <summary>
        /// Create Employee
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="employeeId">Employee ID</param>
        /// <param name="emailAddress">The email of the employee</param>
        private async Task<EntityActionChangeResponse<int>> CreateEmployeeLocatorAsync(short bid, short employeeId, string emailAddress)
        {
            try
            {
                //create employee locator
                EmployeeLocator newModel = new EmployeeLocator()
                {
                    BID = bid,
                    ParentID = employeeId,
                    LocatorType = (byte)LocatorType.Email,
                    Locator = emailAddress,
                    RawInput = emailAddress,
                    SortIndex = 1,
                    IsVerified = false,
                    IsValid = true
                };
                var locator = await new EmployeeLocatorService(ctx, bid, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper).CreateAsync(newModel);
                if (locator == null)
                {
                    return new EntityActionChangeResponse<int>()
                    {
                        Success = false,
                        ErrorMessage = "EmployeeLocatorService.CreateAsync returned null"
                    };
                }

                return new EntityActionChangeResponse<int>()
                {
                    Id = locator.ID,
                    Success = true,
                    Message = $"Successfully created employee locator."
                };
            }
            catch (Exception e)
            {
                return new EntityActionChangeResponse<int>()
                {
                    Success = false,
                    ErrorMessage = "Create employee locator failed - " + e.Message
                };
            }
        }

        /// <summary>
        /// Create Employee
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="employeeID">Employee ID</param>
        /// /// <param name="emailAddress">Employees email</param>
        private async Task<EntityActionChangeResponse<short>> CreateUserLinkAsync(short bid, short employeeID, string emailAddress)
        {
            try
            {

                var inviteBody = new SendInviteRequest()
                {
                    Email = emailAddress,
                    UserAccessType = UserAccessType.EmployeeAdministrator
                };

                return await new EmployeeService(ctx, bid, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper).SendInvite(employeeID, inviteBody, this._endorOptions);
            }
            catch (Exception e)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    ErrorMessage = "Create userlink failed - " + e.Message
                };
            }
        }

        /// <summary>
        /// Create Employee
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="testOnly">When false, changes are not saved.</param>
        private async Task<EntityActionChangeResponse<short>> IdResetAllAsync(short bid, bool testOnly)
        {
            try
            {
                object[] myParams = {
                    new SqlParameter("@BID", bid),
                    new SqlParameter("@TestOnly",  (bool)testOnly ? 1 : 0),
                };

                //Resetting the IDs on the GetNextID counter for the Business 
                await ctx.Database.ExecuteSqlRawAsync("EXEC dbo.[Util.ID.Reset.All] @BID, @TestOnly;", parameters: myParams);

                return new EntityActionChangeResponse<short>()
                {
                    Id = bid,
                    Message = "Sucessfully reset all IDs on the GetNextID counter for the Business." + (testOnly ? " This was only a test ID reset, changes are not saved." : ""),
                    Success = true
                };
            }
            catch (Exception e)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    ErrorMessage = "Failed resetting the IDs on the GetNextID counter for the Business  - " + e.Message
                };
            }
        }

        private string CloneObject<T>(short newBusinessID, byte newLocationID, DbSet<T> db, System.Linq.Expressions.Expression<Func<T, bool>> whereClause, bool resetIDENTITY = false)
            where T : class
        {
            try
            {
                var results = db.Where(whereClause).AsNoTracking();
                if (results != null)
                {
                    foreach (var i in results)
                    {
                        i.GetType().GetProperty("BID").SetValue(i, newBusinessID, null);

                        System.Reflection.PropertyInfo prop = null;
                        if (resetIDENTITY)
                        {
                            prop = i.GetType().GetProperty("ID");
                            if (prop != null)
                            {
                                prop.SetValue(i, null, null);
                            }
                        }
                        prop = i.GetType().GetProperty("LocationID");
                        if (prop != null)
                        {
                            if (prop.PropertyType == typeof(byte?))
                                prop.SetValue(i, (byte)newLocationID, null);
                            else
                                prop.SetValue(i, newLocationID, null);
                        }
                        prop = i.GetType().GetProperty("EmployeeID");
                        if (prop != null)
                        {
                            prop.SetValue(i, null, null);
                        }
                        prop = i.GetType().GetProperty("CompanyID");
                        if (prop != null)
                        {
                            prop.SetValue(i, null, null);
                        }
                        prop = i.GetType().GetProperty("ValidToDT");
                        if (prop != null)
                        {
                            prop.SetValue(i, default(DateTime), null);
                        }
                        prop = i.GetType().GetProperty("ModifiedDT");
                        if (prop != null)
                        {
                            prop.SetValue(i, default(DateTime), null);
                        }
                        db.Add(i);
                    }
                    ctx.SaveChanges();
                }
                return null;
            }
            catch (Exception e)
            {
                return e.Message + " - ";
            }
        }

        /// <summary>
        /// Clone Template Tables
        /// </summary>
        /// <param name="templateBID">Template Business ID</param>
        /// <param name="newBusinessID">New Business ID</param>
        /// <param name="newLocationID">New Location ID</param>
        private async Task<EntityActionChangeResponse<short>> CloneTemplateTables(short templateBID, short newBusinessID, byte newLocationID)
        {
            string errorMessages = null;
            try
            {
                
                errorMessages = CloneObject<CustomFieldOtherData>(newBusinessID, newLocationID, ctx.CustomFieldOtherData, i => i.BID == templateBID);
             
                errorMessages = CloneObject<CustomFieldLayoutDefinition>(newBusinessID, newLocationID, ctx.CustomFieldLayoutDefinition, i => i.BID == templateBID);
                
                errorMessages = CloneObject<PaymentTerm>(newBusinessID, newLocationID, ctx.PaymentTerm, i => i.BID == templateBID);
              
                errorMessages = CloneObject<GLAccount>(newBusinessID, newLocationID, ctx.GLAccount, i => i.BID == templateBID);
               
                errorMessages = CloneObject<TaxabilityCode>(newBusinessID, newLocationID, ctx.TaxabilityCodes, i => i.BID == templateBID);
               
                errorMessages = CloneObject<AssemblyData>(newBusinessID, newLocationID, ctx.AssemblyData, i => i.BID == templateBID);
             
                errorMessages = CloneObject<MaterialData>(newBusinessID, newLocationID, ctx.MaterialData, i => i.BID == templateBID);
              
                errorMessages = CloneObject<MachineData>(newBusinessID, newLocationID, ctx.MachineData, i => i.BID == templateBID);
             
                errorMessages = CloneObject<LaborData>(newBusinessID, newLocationID, ctx.LaborData, i => i.BID == templateBID);
               
                errorMessages = CloneObject<PaymentMethod>(newBusinessID, newLocationID, ctx.PaymentMethod, i => i.BID == templateBID);
               
                errorMessages = CloneObject<AssemblyVariable>(newBusinessID, newLocationID, ctx.AssemblyVariable, i => i.BID == templateBID);
                
                errorMessages = CloneObject<SurchargeDef>(newBusinessID, newLocationID, ctx.SurchargeDef, i => i.BID == templateBID);
              
                errorMessages = CloneObject<AssemblyLayout>(newBusinessID, newLocationID, ctx.AssemblyLayout, i => i.BID == templateBID);
              
                errorMessages = CloneObject<CustomFieldDefinition>(newBusinessID, newLocationID, ctx.CustomFieldDefinition, i => i.BID == templateBID);
             
                errorMessages = CloneObject<MachineCategory>(newBusinessID, newLocationID, ctx.MachineCategory, i => i.BID == templateBID);
              
                errorMessages = CloneObject<AssemblyCategory>(newBusinessID, newLocationID, ctx.AssemblyCategory, i => i.BID == templateBID);
               
                errorMessages = CloneObject<QuickItemData>(newBusinessID, newLocationID, ctx.QuickItemData, i => i.BID == templateBID);
              
                errorMessages = CloneObject<AssemblyTable>(newBusinessID, newLocationID, ctx.AssemblyTable, i => i.BID == templateBID);
                
                errorMessages = CloneObject<MachineProfile>(newBusinessID, newLocationID, ctx.MachineProfile, i => i.BID == templateBID);
               
                errorMessages = CloneObject<BoardDefinitionData>(newBusinessID, newLocationID, ctx.BoardDefinitionData, i => i.BID == templateBID);
                
                errorMessages = CloneObject<LaborCategory>(newBusinessID, newLocationID, ctx.LaborCategory, i => i.BID == templateBID);
              
                errorMessages = CloneObject<MaterialCategory>(newBusinessID, newLocationID, ctx.MaterialCategory, i => i.BID == templateBID);
              
                errorMessages = CloneObject<MachineInstance>(newBusinessID, newLocationID, ctx.MachineInstance, i => i.BID == templateBID);
              
                errorMessages = CloneObject<AssemblyVariableFormula>(newBusinessID, newLocationID, ctx.AssemblyVariableFormula, i => i.BID == templateBID);
               
                errorMessages = CloneObject<OpportunityCustomData>(newBusinessID, newLocationID, ctx.OpportunityCustomData, i => i.BID == templateBID);
                
                errorMessages = CloneObject<MaterialCategoryLink>(newBusinessID, newLocationID, ctx.MaterialCategoryLink, i => i.BID == templateBID);
                
                errorMessages = CloneObject<MachineCategoryLink>(newBusinessID, newLocationID, ctx.MachineCategoryLink, i => i.BID == templateBID);
                
                errorMessages = CloneObject<LaborCategoryLink>(newBusinessID, newLocationID, ctx.LaborCategoryLink, i => i.BID == templateBID);
               
                errorMessages = CloneObject<BoardRoleLink>(newBusinessID, newLocationID, ctx.BoardRoleLink, i => i.BID == templateBID);
               
                errorMessages = CloneObject<EmployeeRole>(newBusinessID, newLocationID, ctx.EmployeeRole, i => i.BID == templateBID);
                errorMessages = CloneObject<BoardModuleLink>(newBusinessID, newLocationID, ctx.BoardModuleLink, i => i.BID == templateBID);
               
                errorMessages = CloneObject<BoardView>(newBusinessID, newLocationID, ctx.BoardView, i => i.BID == templateBID);
                errorMessages = CloneObject<BoardViewLink>(newBusinessID, newLocationID, ctx.BoardViewLink, i => i.BID == templateBID);
              
                errorMessages = CloneObject<MachineProfileTable>(newBusinessID, newLocationID, ctx.MachineProfileTable, i => i.BID == templateBID);
                
                errorMessages = CloneObject<ReportMenu>(newBusinessID, newLocationID, ctx.ReportMenu, i => i.BID == templateBID);
               
                errorMessages = CloneObject<AssemblyCategoryLink>(newBusinessID, newLocationID, ctx.AssemblyCategoryLink, i => i.BID == templateBID);
               
                errorMessages = CloneObject<AssemblyElement>(newBusinessID, newLocationID, ctx.AssemblyElement, i => i.BID == templateBID);
               
                errorMessages = CloneObject<CustomFieldLayoutElement>(newBusinessID, newLocationID, ctx.CustomFieldLayoutElement, i => i.BID == templateBID);
               
                errorMessages = CloneObject<RightsGroupList>(newBusinessID, newLocationID, ctx.RightsGroupList, i => i.BID == templateBID);
                errorMessages = CloneObject<RightsGroupListRightsGroupLink>(newBusinessID, newLocationID, ctx.RightsGroupListRightsGroupLink, i => i.BID == templateBID);
                
                errorMessages = CloneObject<ListTagOtherLink>(newBusinessID, newLocationID, ctx.ListTagOtherLink, i => i.BID == templateBID);
              
                errorMessages = CloneObject<ListTag>(newBusinessID, newLocationID, ctx.ListTag, i => i.BID == templateBID);
                
                errorMessages = CloneObject<FlatListItem>(newBusinessID, newLocationID, ctx.FlatListItem, i => ((i.BID == templateBID) && (i.IsAdHoc == false)));
                
                errorMessages = CloneObject<ReportMenu>(newBusinessID, newLocationID, ctx.ReportMenu, i => i.BID == templateBID);
                
                errorMessages = CloneObject<CrmOrigin>(newBusinessID, newLocationID, ctx.CrmOrigin, i => i.BID == templateBID);
                
                errorMessages = CloneObject<CrmIndustry>(newBusinessID, newLocationID, ctx.CrmIndustry, i => i.BID == templateBID);
                
                errorMessages = CloneObject<OrderItemStatus>(newBusinessID, newLocationID, ctx.OrderItemStatus, i => i.BID == templateBID);
                
                errorMessages = CloneObject<TaxGroup>(newBusinessID, newLocationID, ctx.TaxGroup, i => i.BID == templateBID);

                var resultsFlatList = ctx.FlatListItem.Where(i => ((i.BID == templateBID) && (i.IsAdHoc == false))).AsNoTracking().ToList();
                var results = ctx.QuickItemCategoryLink.Where(i => i.BID == templateBID).AsNoTracking().ToList();
                if (results != null)
                {
                    foreach (var i in results)
                    {
                        bool isInFlatList = false;
                        var categoryID = i.CategoryID; //i.GetType().GetProperty("CategoryID").GetValue(i);
                        foreach (var fl in resultsFlatList)
                        {
                            var flatlistID = fl.ID; //fl.GetType().GetProperty("ID").GetValue(fl);
                            if (categoryID.ToString() == flatlistID.ToString())
                            {
                                isInFlatList = true;
                                break;
                            }
                        }
                        if (isInFlatList)
                        {
                            i.BID = newBusinessID;
                            //i.GetType().GetProperty("BID").SetValue(i, newBusinessID, null);
                            ctx.QuickItemCategoryLink.Add(i);
                        }
                    }
                }

                ctx.SaveChanges();

                return new EntityActionChangeResponse<short>()
                {
                    Success = true,
                    ErrorMessage = "Done cloning template tables to new business ID."
                };
            }
            catch (Exception e)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    ErrorMessage = "Failed cloning template tables to new business ID  - " + e.Message + " - " + errorMessages
                };
            }
        }

        public void setOptions(EndorOptions options)
        {
            this._endorOptions = options;
        }

        private async Task<bool> SetActiveAuthDB(string action)
        {
            string endpoint = $"{_endorOptions.AuthOrigin}/api/business/{this.BID}/action/{action}";

            using (var client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, endpoint);
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToBase64String(Encoding.UTF8.GetBytes(_endorOptions.TenantSecret)));
                request.Content = new StringContent("", Encoding.UTF8, "application/json");
                var response = await client.SendAsync(request);
                string responseString = await response.Content.ReadAsStringAsync();

                return response.StatusCode == HttpStatusCode.OK;
            }
        }
    }
}
