﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// A service for Objects
    /// </summary>
    public class ObjectService
    {
        /// <summary>
        /// object to queue tasks with
        /// </summary>
        protected readonly ITaskQueuer _taskQueuer;

        /// <summary>
        /// RTM Client
        /// </summary>
        protected readonly IRTMPushClient _rtmClient;

        /// <summary>
        /// API Context
        /// </summary>
        ApiContext _context;

        /// <summary>
        /// Constructor to inject an ApiContext
        /// </summary>
        /// <param name="context">Api context</param>
        /// <param name="taskQueuer"></param>
        /// <param name="rtmClient"></param>
        public ObjectService(ApiContext context, ITaskQueuer taskQueuer, IRTMPushClient rtmClient)
        {
            _context = context;
            this._taskQueuer = taskQueuer;
            this._rtmClient = rtmClient;
        }

        /// <summary>
        /// Gets supported features for an object
        /// </summary>
        /// <param name="CTID">ClassType ID</param>
        /// <returns></returns>
#pragma warning disable CS1998 // async lacking await operator
        public async Task<GenericResponse<string[]>> GetSupportedFeatures(int CTID)
        {
            try
            {
                var response = new List<string>();

                if (CTID == (int)ClassType.Business)
                    return new GenericResponse<string[]>()
                    {
                        Message = "",
                        Data = response.ToArray(),
                        Success = true
                    };

                var entity = ModelToClassType.FromClassTypeID(CTID);
                if (entity == null)
                    return null;

                if (entity.GetProperty("IsActive") != null)
                {
                    response.Add(("SetActive"));
                    response.Add(("SetInactive"));
                }

                if (entity.GetProperty("IsUrgent") != null)
                    response.Add(("SetUrgency"));

                if (entity.GetProperty("Priority") != null)
                    response.Add(("SetPriority"));

                switch (CTID)
                {
                    case (int)ClassType.OrderItem:
                        response.Add(("ChangeItemStatus"));//https://corebridge.atlassian.net/browse/END-5743
                        break;
                    default:
                        //other things
                        break;
                }

                response.Add(("AddTag"));
                response.Add(("RemoveTag"));

                return new GenericResponse<string[]>()
                {
                    Message = "",
                    Data = response.ToArray(),
                    Success = true
                };
            }
            catch (Exception e)
            {
                return new GenericResponse<string[]>()
                {
                    Message = e.Message,
                    Success = false
                };
            }

        }
#pragma warning restore CS1998 // async lacking await operator

        /// <summary>
        /// Sets Priority for an object
        /// </summary>
        /// <param name="BID">BID</param>
        /// <param name="CTID">ClassType ID</param>
        /// <param name="ID">Object ID</param>
        /// <param name="urgency">Urgency Value - must be either hi or normal</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> SetUrgency(short BID, int CTID, int ID, string urgency)
        {
            try
            {
                var entityName = Enum.Parse<ClassType>(CTID.ToString()).ToString();

                if ((urgency != "hi") && (urgency != "normal"))
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "Urgency need to be one of (hi, normal).",
                        Success = false
                    };

                bool urgentVal = urgency == "hi" ? true : false;

                var entityType = ModelToClassType.FromClassTypeID(CTID);

                if (!(await IsSupported(CTID, "SetUrgency")))
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = $"{entityName} does not support SetUrgency.",
                        Success = false
                    };

                object entity = (await this._context.FindAsync(entityType, BID, ID));
                if (entity == null)
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = $"{entityName} with ID {ID} Not Found.",
                        Success = false
                    };

                entity.GetType().GetProperty("IsUrgent").SetValue(entity, urgentVal, null);
                await this._context.SaveChangesAsync();
                await this.SendRefreshMessage(BID,CTID,ID);

                return new EntityActionChangeResponse
                {
                    Id = ID,
                    Message = "Urgency updated.",
                    Success = true
                };
            }
            catch (Exception e)
            {
                return new EntityActionChangeResponse
                {
                    Id = ID,
                    Message = $"Exception: {e.Message}",
                    Success = false
                };
            }
        }

        /// <summary>
        /// Sets Priority for an object
        /// </summary>
        /// <param name="BID">BID</param>
        /// <param name="CTID">ClassType ID</param>
        /// <param name="ID">Object ID</param>
        /// <param name="priority">Integer Value between 1 and 32767</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> SetPriority(short BID, int CTID, int ID, int priority)
        {
            try
            {
                var entityName = Enum.Parse<ClassType>(CTID.ToString()).ToString();

                if (priority < 1 || priority > 32767)
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "Priority must be between 1 and 32767.",
                        Success = false
                    };

                Type entityType = ModelToClassType.FromClassTypeID(CTID);
                if (!(await IsSupported(CTID, "SetPriority")))
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = $"{entityName} does not support SetPriority.",
                        Success = false
                    };

                object entity = (await this._context.FindAsync(entityType, BID, ID));
                if (entity == null)
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = $"{entityName} with ID {ID} Not Found.",
                        Success = false
                    };

                entity.GetType().GetProperty("Priority").SetValue(entity, (short)priority, null);
                await this._context.SaveChangesAsync();
                await this.SendRefreshMessage(BID, CTID, ID);

                return new EntityActionChangeResponse
                {
                    Id = ID,
                    Message = "Priority updated.",
                    Success = true
                };
            }
            catch (Exception e)
            {
                return new EntityActionChangeResponse
                {
                    Id = ID,
                    Message = $"Exception: {e.Message}",
                    Success = false
                };
            }
        }

        private async Task SendRefreshMessage(short BID, int CTID, int ID)
        {
            if (this._taskQueuer == null || this._rtmClient == null)
            {
                return;
            }

            var refreshEntity = new RefreshEntity()
            {
                BID = BID,
                ClasstypeID = CTID,
                ID = ID,
                DateTime = DateTime.UtcNow,
                RefreshMessageType = RefreshMessageType.Change
            };
            var refreshMsg = new RefreshMessage()
            {
                RefreshEntities = new List<RefreshEntity>() { refreshEntity }
            };
            await this._taskQueuer.IndexModel(refreshEntity.BID, refreshEntity.ClasstypeID, refreshEntity.ID);
            await this._rtmClient.SendRefreshMessage(refreshMsg);
        }

        /// <summary>
        /// Checks if the provided feature is supported
        /// </summary>
        /// <param name="CTID">ClassTypeID</param>
        /// <param name="feature">Feature to check for support of</param>
        /// <returns></returns>
        private async Task<bool> IsSupported(int CTID, string feature)
        {
            bool result = false;

            var checkSupported = await GetSupportedFeatures(CTID);
            if (checkSupported.Success && checkSupported.Data != null && checkSupported.Data.Length > 0 && checkSupported.Data.Contains(feature))
                    result = true;

            return result;
        }
    }
}
