﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Includes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Employee Team Service
    /// </summary>
    public class EmployeeTeamService : AtomCRUDService<EmployeeTeam, int>, ISimpleListableViewService<SimpleEmployeeTeam, int>
    {
        private EmployeeService _employeeService;

        /// <summary>
        /// Property to get  SimpleEmployeeTeam from ApiContext
        /// </summary>
        public DbSet<SimpleEmployeeTeam> SimpleListSet => ctx.SimpleEmployeeTeam;

        /// <summary>
        /// Constructor for Employee Team Service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public EmployeeTeamService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) 
        {
            this._employeeService = new EmployeeService(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper);
        }

        /// <summary>
        /// Default Child Properties to include from Database Context
        /// </summary>
        public override string[] IncludeDefaults => new string[] 
        {
            "EmployeeTeamLinks",
            "EmployeeTeamLocationLinks"
        };

        /// <summary>
        /// Get the names of navigation properties to include on Get
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// Get child associations (services that handle business logic for child collections). May return null.
        /// </summary>
        /// <returns>IChildServiceAssociation or null</returns>
        protected override IChildServiceAssociation<EmployeeTeam, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<EmployeeTeam, int>[0];
        }

        internal override void DoBeforeValidate(EmployeeTeam newModel)
        {
        }

        /// <summary>
        /// Gets a list of EmployeeTeam with includes
        /// </summary>
        /// <param name="ID">EmployeeTeam Id</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        public async Task<EmployeeTeam> GetWithIncludes(int ID, EmployeeTeamIncludes includes = null)
        {
            var result = (await this.FilteredGetAsync(t => t.ID == ID, includes)).FirstOrDefault();
            if (result != null && includes != null)
            {
                if (includes.EmailAccountsLevel == IncludesLevel.Full || includes.EmailAccountsLevel == IncludesLevel.Simple)
                {
                    var links = await ctx.EmailAccountTeamLink.Include(x => x.EmailAccount)
                        .Where(x => x.BID == BID && x.TeamID == ID).ToListAsync();
                    if (includes.EmailAccountsLevel == IncludesLevel.Simple)
                    {
                        foreach (var link in links)
                        {
                            link.Team = null; // clear out so as to not serialize, and prevent recursion
                            link.EmailAccount = null; // clear out so as to not serialize, and prevent recursion
                        }
                        result.EmailAccountTeamLinks = links;
                    }
                    else
                    {
                        if (links != null && links.Count > 0)
                        {
                            result.EmailAccounts = new List<EmailAccountData>();
                            foreach (var link in links)
                            {
                                var emailAccount = link.EmailAccount;
                                emailAccount.EmailAccountTeamLinks = null; // prevent recursion
                                result.EmailAccounts.Add(emailAccount);
                            }
                            result.EmailAccountTeamLinks = null; // clear out so as to not serialize
                        }
                    }
                }

            }
            return result;
        }

        /// <summary>
        /// Maps a specific item.
        /// </summary>
        /// <param name="item">Item to map</param>
        /// <param name="includes">Child/Parent includes</param>
        public override Task MapItem(EmployeeTeam item, IExpandIncludes includes = null)
        {
            if (!(includes is EmployeeTeamIncludes employeeTeamIncludes) || employeeTeamIncludes.LocationLevel != IncludesLevel.Simple)
                return base.MapItem(item, includes);

            EmployeeTeam team = this.ctx.EmployeeTeam.IncludeAll(new[] { "EmployeeTeamLocationLinks", "EmployeeTeamLocationLinks.Location" })
                .FirstOrDefault(t => t.ID == item.ID && t.BID == this.BID);

            List<SimpleLocationData> simpleLocations = (from location in team?.EmployeeTeamLocationLinks.Select(l => l.Location)
                    select new SimpleLocationData()
                    {
                        ID = location.ID,
                        BID = location.BID,
                        ClassTypeID = location.ClassTypeID,
                        DisplayName = location.Name,
                        IsActive = location.IsActive,
                        IsDefault = location.IsDefault
                    }).ToList();

            item.SimpleLocations = simpleLocations;

            return base.MapItem(item, includes);
        }

        /// <summary>
        /// Maps Simple Locations from CheckBox Picker on front end (SimpleLocations) to EmployeeTeamLocationLinks
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(EmployeeTeam newModel, Guid? tempGuid = null)
        {
            await base.DoBeforeCreateAsync(newModel, tempGuid);

            if (newModel.SimpleLocations != null)
            {
                if (newModel.EmployeeTeamLocationLinks == null)
                    newModel.EmployeeTeamLocationLinks = new HashSet<EmployeeTeamLocationLink>();

                foreach (SimpleLocationData simpleLocation in newModel.SimpleLocations)
                {
                    newModel.EmployeeTeamLocationLinks.Add(new EmployeeTeamLocationLink()
                    {
                        BID = this.BID,
                        TeamID = newModel.ID,
                        LocationID = simpleLocation.ID
                    });
                }
            }
        }

        /// <summary>
        /// 1. if no company directly references the team, the team's links (EmployeeTeamLinks and EmployeeTeamLocationLinks) 
        /// should be deleted if they exist
        /// <para>2. If a company directly references the team, throw a 400 error</para>
        /// </summary>
        /// <param name="model">EmployeeTeam</param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(EmployeeTeam model)
        {
            var canDelete = await this.CanDelete(model.ID);
            if (canDelete.Value.GetValueOrDefault())
            {
                //check if EmployeeTeamLinks exist
                var teamLinks = model.EmployeeTeamLinks;
                teamLinks.ToList().ForEach(tl =>
                {
                    model.EmployeeTeamLinks.Remove(tl);
                });
                var teamLocationLinks = model.EmployeeTeamLocationLinks;
                teamLocationLinks.ToList().ForEach(tl =>
                {
                    model.EmployeeTeamLocationLinks.Remove(tl);
                });

                await base.DoBeforeDeleteAsync(model);
            }
            else
            {
                throw new InvalidOperationException(canDelete.Message);
            }
        }

        /// <summary>
        /// Links or Unlinks a team with a location
        /// </summary>
        /// <param name="teamId"></param>
        /// <param name="locationId"></param>
        /// <param name="link"></param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkLocation(int teamId, byte locationId, bool link)
        {
            try
            {
                EmployeeTeamLocationLink existingLink = this.ctx.EmployeeTeamLocationLink.FirstOrDefault(l => l.BID == this.BID && l.TeamID == teamId && l.LocationID == locationId);

                bool linkExists = existingLink != null;

                if (link)
                {
                    if (linkExists) return GetNoChangeRequiredResponse(teamId);

                    this.ctx.EmployeeTeamLocationLink.Add(new EmployeeTeamLocationLink() { BID = this.BID, LocationID = locationId, TeamID = teamId });
                }
                else
                {//unlink
                    if (!linkExists) return GetNoChangeRequiredResponse(teamId);

                    this.ctx.EmployeeTeamLocationLink.Remove(existingLink);
                }

                await this.ctx.SaveChangesAsync();

                await this.rtmClient.SendRefreshMessage
                (
                    new RefreshMessage()
                    {
                        RefreshEntities = new List<RefreshEntity>()
                        {
                            GetRefreshMessage(teamId, (int) ClassType.EmployeeTeam, RefreshMessageType.Change),
                            GetRefreshMessage(locationId, (int) ClassType.Location, RefreshMessageType.Change)
                        }
                    }
                );

                await this.taskQueuer.IndexModel(this.BID, ClassType.EmployeeTeam.ID(), teamId);

                if (link)
                    return new EntityActionChangeResponse()
                    {
                        Id = teamId,
                        Message = $"Successfully linked the Location to the Employee Team.",
                        Success = true
                    };
                else
                    return new EntityActionChangeResponse()
                    {
                        Id = teamId,
                        Message = $"Successfully un-linked the Location from the Employee Team.",
                        Success = true
                    };
            }
            catch (Exception e)
            {
                return new EntityActionChangeResponse()
                {
                    Id = teamId,
                    ErrorMessage = e.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Links or Unlinks a team with a employee by a role ID
        /// </summary>
        /// <param name="teamId"></param>
        /// <param name="employeeId"></param>
        /// <param name="roleID"></param>
        /// <param name="link"></param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkEmployeeRole(int teamId, short employeeId, short roleID, bool link)
        {
            try
            {
                EmployeeTeamLink existingLink = this.ctx.EmployeeTeamLink.FirstOrDefault(l => l.BID == this.BID && l.TeamID == teamId && l.EmployeeID == employeeId && l.RoleID == roleID);

                bool linkExists = existingLink != null;

                if (link)
                {
                    if (linkExists) return GetNoChangeRequiredResponse(teamId);

                    this.ctx.EmployeeTeamLink.Add(new EmployeeTeamLink() { BID = this.BID, EmployeeID = employeeId, TeamID = teamId, RoleID = roleID });
                }
                else
                {//unlink
                    if (!linkExists) return GetNoChangeRequiredResponse(teamId);

                    this.ctx.EmployeeTeamLink.Remove(existingLink);
                }

                await this.ctx.SaveChangesAsync();

                await this.rtmClient.SendRefreshMessage
                (
                    new RefreshMessage()
                    {
                        RefreshEntities = new List<RefreshEntity>()
                        {
                            GetRefreshMessage(teamId, (int) ClassType.EmployeeTeam, RefreshMessageType.Change),
                            GetRefreshMessage(employeeId, (int) ClassType.Location, RefreshMessageType.Change)
                        }
                    }
                );

                await this.taskQueuer.IndexModel(this.BID, ClassType.EmployeeTeam.ID(), teamId);

                if (link)
                    return new EntityActionChangeResponse()
                    {
                        Id = teamId,
                        Message = $"Successfully linked the Employee to the Employee Team Role.",
                        Success = true
                    };
                else
                    return new EntityActionChangeResponse()
                    {
                        Id = teamId,
                        Message = $"Successfully un-linked the Employee from the Employee Team Role.",
                        Success = true
                    };
            }
            catch (Exception e)
            {
                return new EntityActionChangeResponse()
                {
                    Id = teamId,
                    ErrorMessage = e.Message,
                    Success = false
                };
            }
        }

        private EntityActionChangeResponse GetNoChangeRequiredResponse(int id)
        {
            return new EntityActionChangeResponse()
            {
                Id = id,
                Success = true,
                ErrorMessage = "No Change Required"
            };
        }

        private RefreshEntity GetRefreshMessage(int ID, int classTypeID, RefreshMessageType refreshType)
        {
            return new RefreshEntity()
            {
                BID = this.BID,
                ClasstypeID = classTypeID,
                ID = ID,
                DateTime = DateTime.UtcNow,
                RefreshMessageType = refreshType,
            };
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<EmployeeTeam> WherePrimary(IQueryable<EmployeeTeam> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Checks if the Team can be deleted
        /// </summary>
        /// <param name="id">Team ID to check for</param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(int id)
        {
            var includes = new string[]{ "Companies", "Campaigns", "Opportunities" };
            var team = await this.ctx.EmployeeTeam.IncludeAll(includes).Where(t => t.BID == BID && t.ID == id).FirstOrDefaultAsync();
            if (team == null)
                return new BooleanResponse()
                {
                    Success = false,
                    Value = false,
                    Message = "Team not found.",
                    ErrorMessage = "Team not found."
                };
            if (team.Companies != null && team.Companies.Count > 0)
                return new BooleanResponse()
                {
                    Success = true,
                    Value = false,
                    Message = "Team is associated with one or more Companies."
                };
            if (team.Campaigns != null && team.Campaigns.Count > 0)
                return new BooleanResponse()
                {
                    Success = true,
                    Value = false,
                    Message = "Team is associated with one or more Campaigns."
                };
            if (team.Opportunities != null && team.Opportunities.Count > 0)
                return new BooleanResponse()
                {
                    Success = true,
                    Value = false,
                    Message = "Team is associated with one or more Opportunities."
                };

            return new BooleanResponse()
            {
                Success = true,
                Value = true,
                Message = "Team can be deleted."
            };
        }

        /// <summary>
        /// Get Employee LocationID
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public async Task<byte> GetLocationID(short employeeID)
        {
            return await this._employeeService.GetLocationID(employeeID);
        }
    }
}
