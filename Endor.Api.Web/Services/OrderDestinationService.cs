﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// 
    /// </summary>
    
    /// <summary>
    /// An order destination child service
    /// </summary>
    public class OrderDestinationService : AtomCRUDService<OrderDestinationData, int>
    {
        /// <summary>
        /// Constructs an order destination service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderDestinationService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) 
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Returns a an array of includes to expand
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {

            return null;
        }

        
        /// <summary>
        /// Returns the query filtered by its primary key
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<OrderDestinationData> WherePrimary(IQueryable<OrderDestinationData> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Returns the collection of ChildServiceAssociations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<OrderDestinationData, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<OrderDestinationData, int>[]
            {
                CreateChildAssociation<OrderNoteService, OrderNote, int>((order)=> order.Notes),
                CreateChildAssociation<OrderKeyDateService, OrderKeyDate, int>((order)=> order.Dates),
                CreateChildAssociation<OrderDestinationContactRoleService, OrderContactRole, int>((order)=> order.ContactRoles),
                CreateChildAssociation<OrderEmployeeRoleService, OrderEmployeeRole, int>((order)=> order.EmployeeRoles),
            };
        }

        internal override void DoBeforeValidate(OrderDestinationData newModel)
        {
            ctx.Entry(newModel).Property(x => x.ModifiedDT).IsModified = false;
        }

        /// <summary>
        /// Sets the ModifiedDT to unmodified since OrderDestination is a temporal table
        /// </summary>
        /// <param name="newModel"></param>
        protected override void SetModifiedDT(OrderDestinationData newModel)
        {
            if (newModel.ID == 0)
            {
                newModel.ModifiedDT = default(DateTime);
            }
            ctx.Entry(newModel).Property(t => t.ModifiedDT).IsModified = false;
        }

        /// <summary>
        /// Gets a list of Order Destinations associated with the OrderID
        /// </summary>
        /// <param name="orderID">OrderID</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        public async Task<List<OrderDestinationData>> GetOrderDestinationsForOrder(int orderID, OrderDestinationIncludes includes = null)
        {
            var order = await ctx.Set<TransactionHeaderData>().Where(o => o.BID == this.BID && o.ID == orderID).FirstOrDefaultAsync();
            if (order == null)
                return null;

            var destinations = await ctx.Set<OrderDestinationData>().IncludeAll(GetIncludes(includes))
                .Where(o => o.BID == this.BID && o.OrderID == orderID).ToListAsync();

            foreach (var destination in destinations)
                destination.Order = null; // prevent error on serialization

            return destinations;
        }

        /// <summary>
        /// Gets a SimpleList of OrderItems for the given Order by ID
        /// </summary>
        /// <param name="orderID">Order's ID</param>
        /// <returns></returns>
        public async Task<List<SimpleOrderDestinationData>> GetSimpleOrderDestinationDataForOrder(int orderID)
        {
            List<SimpleOrderDestinationData> simpleOrderDestinations = new List<SimpleOrderDestinationData>();
            var order = await ctx.Set<TransactionHeaderData>().Where(o => o.BID == this.BID && o.ID == orderID).FirstOrDefaultAsync();
            if (order == null)
                return null;

            var orderItems = await ctx.Set<OrderDestinationData>().Where(o => o.BID == this.BID && o.OrderID == orderID).ToListAsync();
            if (orderItems != null && orderItems.Count > 0)
            {
                simpleOrderDestinations = orderItems.Select(link => new SimpleOrderDestinationData()
                {
                    BID = link.BID,
                    ID = link.ID,
                    ClassTypeID = link.ClassTypeID,
                    IsActive = true,
                    IsDefault = false,
                    DisplayName = link.Name
                }).ToList();
            }

            return simpleOrderDestinations;
        }
    }
}
