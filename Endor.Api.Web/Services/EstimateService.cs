﻿using System;
using System.Collections.Generic;
using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Classes.Clones;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Microsoft.AspNetCore.Mvc;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Estimate Service Layer
    /// </summary>
    public class EstimateService : BaseTransactionService<EstimateData>
    {
        private EstimateCloneOption _cloneOption;

        /// <summary>
        /// Estimate Service Layer
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public EstimateService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
            this.OptionKeyCopyDocuments = "Estimate.Clone.CopyDocuments";
        }

        /// <summary>
        /// Defines this service as for Estimates
        /// </summary>
        protected override OrderTransactionType TransactionType => OrderTransactionType.Estimate;

        /// <summary>
        /// Checks if the Estimate can be deleted
        /// </summary>
        /// <param name="id">Team ID to check for</param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(int id)
        {
            var order = await this.ctx.TransactionHeaderData.Where(t => t.BID == BID
                                                                    && t.ID == id
                                                                    && t.TransactionType == (byte)OrderTransactionType.Estimate).FirstOrDefaultAsync();
            if (order == null)
                return new BooleanResponse()
                {
                    Success = false,
                    Value = false,
                    Message = "Estimate not found.",
                    ErrorMessage = "Estimate not found."
                };
            if (order.OrderStatusID != OrderOrderStatus.EstimateApproved)
                return new BooleanResponse()
                {
                    Success = false,
                    Value = false,
                    Message = "Estimate needs to be approved before it can be deleted.",
                    ErrorMessage = "Estimate needs to be approved before it can be deleted."
                };

            return new BooleanResponse()
            {
                Success = true,
                Value = true,
                Message = "Estimate can be deleted."
            };
        }

        public async Task<IActionResult> CloneTransactionWithOptionAsync(int ID, EstimateCloneOption cloneOption, short? userLinkId, OrderTransactionType? newTransactionType = null)
        {
            _cloneOption = cloneOption;
            var result = await base.CloneTransactionAsync(ID, cloneOption.KeepFiles == true, newTransactionType, userLinkId);
            if (result == null)
            {
                return new BadRequestObjectResult("Clone failed");
            }
            else
            {
                return new OkObjectResult(result);
            }
        }


        protected override async Task DoAfterCloneAsync(int clonedFromID, EstimateData clone, bool? CopyFiles, OrderOrderStatus? newStatus, short? newItemStatus, short? userLinkID)
        {

            if (clone.Links == null || clone.Links.Count < 0)
            {
                var clonedFrom = await this.ctx.Set<EstimateData>().AsNoTracking().FirstOrDefaultAsync(x => x.BID == this.BID && x.ID == clonedFromID);
                ctx.OrderOrderLink.Add(
                    new OrderOrderLink()
                    {
                        BID = this.BID,
                        OrderID = clone.ID,
                        LinkType = OrderOrderLinkType.ClonedFrom,
                        LinkedOrderID = clonedFromID,
                        LinkedFormattedNumber = clonedFrom.FormattedNumber,
                        Description = "",
                    }
                );
                ctx.OrderOrderLink.Add(
                    new OrderOrderLink()
                    {
                        BID = this.BID,
                        OrderID = clonedFromID,
                        LinkType = OrderOrderLinkType.ClonedTo,
                        LinkedOrderID = clone.ID,
                        LinkedFormattedNumber = clone.FormattedNumber,
                        Description = "",
                    }
                );

                try
                {
                    await ctx.SaveChangesAsync();
                    var UserLink = ctx.UserLink.First(x => x.BID == this.BID && x.ID == userLinkID);
                    await _taskQueuer.RecomputeGL(this.BID, (byte)GLEngine.Models.EnumGLType.Order, clone.ID, "Order Cloned", "Order Cloned", UserLink.EmployeeID, null, (byte)GLEntryType.Order_New);
                }
                catch (Exception)
                {

                }

            }

            await base.DoAfterCloneAsync(clonedFromID, clone);


            if (_cloneOption != null)
            {
                if (_cloneOption.OrderStatus != null)
                {
                    clone.OrderStatusID = _cloneOption.OrderStatus.Value;
                }
                if (_cloneOption.KeepFiles == true)
                {
                    CopyFiles = true;
                }
            }


            if (!CopyFiles.HasValue)
            {
                //read option from db
                string key = (((ClassType)clone.ClassTypeID) == ClassType.Order) ? OptionKeyCopyDocuments : OptionEstimateKeyCopyDocuments;
                CopyFiles = (await base.GetUserOptionValueByName(key, userLinkID)).ValueAsBoolean();
            }
            if (CopyFiles.GetValueOrDefault())
            {
                // Get clone from object for reference of the copy files
                var clonedFrom = await this.ctx.Set<EstimateData>().AsNoTracking().FirstOrDefaultAsync(x => x.BID == this.BID && x.ID == clonedFromID);

                //Copy All Files
                DocumentManager client = base.GetDocumentManager(clonedFromID, (ClassType)clonedFrom.ClassTypeID, BucketRequest.All);
                await client.CloneAllDocumentsToClassTypeAsync(clone.ID, clone.ClassTypeID, includeSpecialFolderBlobName: true);
            }

            //Need to clone Contact and Employee roles...
            await CloneContactAndEmployeeRole(clonedFromID, null, clone.ID, null);

            //Need to update the primary contact
            await UpdateOrderContactToPrimary(clone.ID, clone.CompanyID);

            //Update Entered By to User
            OrderEmployeeRoleService oerservice = new OrderEmployeeRoleService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
            var employeeRoleId = ctx.EmployeeRole.FirstOrDefault(er => er.Name == "Entered By").ID;
            await oerservice.RemoveEmployeeRole(clone.ID, employeeRoleId);
            short? employeeID = ctx.UserLink.First(ul => ul.BID == BID && ul.ID == userLinkID).EmployeeID;
            if (employeeID.HasValue)
                await oerservice.AddEmployeeRole(clone.ID, employeeRoleId, employeeID.Value);

            //Clone notes
            if (_cloneOption == null || _cloneOption.KeepNotes == true)
            {
                await CloneNotes(clonedFromID, null, clone.ID, null);
            }

            await CreateKeyDateBasedOnOrderStatus(clone.ID, clone.OrderStatusID, true);

            if (clone.TransactionType == (byte)OrderTransactionType.Order)
            {
                await SetCloneDueDate(clone.ID, clone.TransactionType, _cloneOption?.DueDate ?? DateTime.UtcNow.AddDays(1));
            }
            else
            {
                await SetCloneDueDate(clone.ID, clone.TransactionType, _cloneOption?.FollowupDate ?? DateTime.UtcNow.AddDays(1));
            }

            //Need to clone order items...
            //Note: CopyFiles value is guaranteed a value on the code above.            
            await CloneOrderItemData(clonedFromID, clone.ID, newStatus.Value, CopyFiles.GetValueOrDefault(), (OrderTransactionType)clone.TransactionType, newItemStatus);

            //update default line items status
            await UpdateLineItemStatuses(clone.ID, Convert.ToInt16(clone.OrderStatusID), Convert.ToInt16(clone.TransactionType));

            await CloneCustomData(clonedFromID, clone.ID);
            await UpdateCompanyStatus(clone);

            //
            await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(clone.ClassTypeID), Convert.ToInt32(clone.ID));

            if (_cloneOption != null && _cloneOption.UpdatePricing == true)
            {
                await taskQueuer.OrderPricingCompute(BID, clone.ID);
            }

            await rtmClient.SendRefreshMessage(new RefreshMessage()
            {
                RefreshEntities = new List<RefreshEntity>()
                    {
                        new RefreshEntity()
                        {
                            ClasstypeID = clone.ClassTypeID,
                            DateTime = clone.ModifiedDT,
                            RefreshMessageType = RefreshMessageType.New,
                            BID = clone.BID,
                        }
                    }
            });
        }

    }
}