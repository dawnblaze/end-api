﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Includes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Dashboard Service
    /// </summary>
    public class DashboardService : AtomCRUDService<DashboardData, short>
    {
        private DashboardWidgetService _widgetService;


        /// <summary>
        /// Constructor for Dashboard Service
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">Push Client</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DashboardService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) 
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
            _widgetService = new DashboardWidgetService(context,bid,taskQueuer,cache,logger, rtmClient, migrationHelper);
        }

        /// <summary>
        /// Default Child Properties to include from Database Context
        /// </summary>
        public override string[] IncludeDefaults => new string[]
        {
            
        };

        /// <summary>
        /// Get the names of navigation properties to include on Get
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<DashboardData> WherePrimary(IQueryable<DashboardData> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Get child associations (services that handle business logic for child collections). May return null.
        /// </summary>
        /// <returns>IChildServiceAssociation or null</returns>
        protected override IChildServiceAssociation<DashboardData, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<DashboardData, short>[0];
        }

        internal override void DoBeforeValidate(DashboardData newModel)
        {
            
        }

        /// <summary>
        /// check whether a different model exists for that user and module
        /// </summary>
        public bool CheckExistingDashboardConflict(DashboardData newModel)
        {
            return ctx.DashboardData.Where(x =>
                x.UserLinkID == newModel.UserLinkID && x.Module == newModel.Module && newModel.ID != x.ID).Any();
        }

        /// <summary>
        /// Returns the Dashboards for the given filters
        /// </summary>
        /// <param name="filters">Dashboard Filters</param>
        /// <param name="includes">Dashboard Includes</param>
        /// <param name="userLinkID">The calling User's ID</param>
        /// <returns></returns>
        public async Task<List<DashboardData>> GetWithFilters(DashboardFilters filters, DashboardIncludes includes, int? userLinkID)
        {
            var dashboardData = await ctx.Set<DashboardData>().IncludeAll(GetIncludes(includes))
                    .Where(d => d.BID == BID)
                    .WhereAll(filters.WherePredicates(userLinkID))
                    .ToListAsync();

            if (dashboardData.Count > 0) return dashboardData;
            else
            {
                var module = (short)Enum.Parse(typeof(Module), filters.Module.Value.ToString(), true);

                DashboardData clonedData = this.GetByID(module).Result;

                if (clonedData == null)
                {
                    clonedData = new DashboardData()
                    {
                        BID = this.BID,
                        ClassTypeID = ClassType.Dashboard.ID(),
                        Cols = 4,
                        Description = "Default Dashboard for the " + filters.Module.Value.ToString() + " module.",
                        IsActive = true,
                        ModifiedDT = DateTime.UtcNow,
                        Module = (Module)module,
                        Name = filters.Module.Value.ToString() + " Dashboard",
                        Rows = 10
                    };
                }

                clonedData.UserLinkID = userLinkID.HasValue ? (short)userLinkID : Convert.ToInt16(0);
                await this.CreateAsync(clonedData);

                var userDashboard = await ctx.Set<DashboardData>().IncludeAll(GetIncludes(includes))
                    .Where(d => d.BID == BID)
                    .WhereAll(filters.WherePredicates(userLinkID))
                    .ToListAsync();

                return userDashboard;
            }
        }

        /// <summary>
        /// Returns a Dashboard by its ID
        /// </summary>
        /// <param name="id">Dashboard ID</param>
        /// <param name="includes">Dashboard includes</param>
        /// <returns></returns>
        public async Task<DashboardData> GetByID(short id, DashboardIncludes includes = null)
        {
            return await ctx.Set<DashboardData>().IncludeAll(GetIncludes(includes))
                .Where(d => d.BID == BID && d.ID == id)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Gets a simple list of Dashboards for the given filters
        /// </summary>
        /// <param name="filters">Simple Dashboard Filters</param>
        /// <param name="userID">The calling User's ID</param>
        /// <returns></returns>
        public async Task<List<SimpleListItem<short>>> GetSimpleList(SimpleDashboardFilters filters, int? userID)
        {
            return await ctx.Set<DashboardData>().Where(d => d.BID == BID).WhereAll(filters.WherePredicates(userID))
                .Select(d => new SimpleListItem<short>()
                    {
                        BID = d.BID,
                        ID = d.ID,
                        ClassTypeID = d.ClassTypeID,
                        DisplayName = d.Name,
                        IsActive = d.IsActive,
                    }
                ).ToListAsync();
        }

        /// <summary>
        /// Checks if the Dashboard can be deleted
        /// </summary>
        /// <param name="ID">Dashboard's ID</param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(short ID)
        {
            var model = await this.GetByID(ID);
            if (model == null)
            {
                return new BooleanResponse()
                {
                    Message = "Entity not found",
                    ErrorMessage = "Entity not found",
                    Success = false,
                    Value = false
                };
            }

            return new BooleanResponse()
            {
                Message = $"Entity can be deleted",
                Success = true,
                Value = true
            };
        }

        /// <summary>
        /// Potentially Copy Widgets after Dashboard Clone
        /// </summary>
        /// <param name="clonedFromID"></param>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(short clonedFromID, DashboardData clone)
        {

            await CopyDashboardWidgetValues(clonedFromID, clone.ID);

            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        /// <summary>
        /// Copy all Widgets from one Dashboard to another Dashboard
        /// </summary>
        /// <param name="clonedFromDashboardID"></param>
        /// <param name="newDashboardID"></param>
        /// <returns></returns>
        private async Task CopyDashboardWidgetValues(short clonedFromDashboardID, short newDashboardID)
        {
            List<DashboardWidgetData> values = await _widgetService.GetWithFilters(
                new DashboardWidgetFilter()
                {
                    DashboardID = clonedFromDashboardID
                });

            foreach(var value in values)
            {
                value.DashboardID = newDashboardID;
                await _widgetService.CreateAsync(value);
            }
        }

    }
}
