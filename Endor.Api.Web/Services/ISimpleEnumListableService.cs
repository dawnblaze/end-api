﻿using Endor.Models;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Interface for Simple Enum List Set Service
    /// </summary>
    public interface ISimpleEnumListableService<SLI, I> where SLI : SimpleEnumListItem<I>
    {
        /// <summary>
        /// Collection of Simple Enums
        /// </summary>
        DbSet<SLI> SimpleEnumListSet { get; }
    }
}
