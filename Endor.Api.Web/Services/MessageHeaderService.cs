using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{

    /// <summary>
    /// MessageHeaderService
    /// </summary>
    public class MessageHeaderService : AtomCRUDService<MessageHeader, int>
    {

        /// <summary>
        /// constructor
        /// </summary>
        public MessageHeaderService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) 
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
            
        }

        /// <summary>
        /// GetIncludes
        /// </summary>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return new string[] {};
        }

        /// <summary>
        /// WherePrimary
        /// </summary>
        public override IQueryable<MessageHeader> WherePrimary(IQueryable<MessageHeader> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// GetChildAssociations
        /// </summary>
        protected override IChildServiceAssociation<MessageHeader, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<MessageHeader, int>[] {};
        }

        /// <summary>
        /// DoBeforeValidate
        /// </summary>
        internal override void DoBeforeValidate(MessageHeader newModel)
        {
            
        }

        /// <summary>
        /// GetFilteredMessages
        /// </summary>
        /// <param name="includes"></param>
        /// <param name="filters"></param>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <param name="channelID">required</param>
        /// <returns></returns>
        public async Task<ICollection<Message>> GetFilteredMessages(MessageChannelType channelID, MessageIncludes includes,MessageFilters filters, int skip, int take){
            IQueryable<MessageHeader> headersQuery = this.ctx.MessageHeader
                .Include(header => header.MessageBody).ThenInclude(body => body.MessageParticipantInfos);

            headersQuery = includes.AssignIncludedProps(headersQuery);

            var headers = await headersQuery
                .Where(filters.WherePredicates()[0])
                .Where(h => (channelID==MessageChannelType.None&&h.Channels==MessageChannelType.None) || (channelID!=MessageChannelType.None&&h.Channels.HasFlag(channelID)))
                .Skip(skip)
                .Take(take)
                .ToListAsync();

            var msgBreakdowns = headers.Select(h => new MessageBreakdown()
            {
                MessageBody = h.MessageBody,
                MessageHeader = h,
                MessageParticipantInfos = h.MessageBody.MessageParticipantInfos,
                MessageObjectLinks = h.MessageBody.MessageObjectLinks,
                MessageDeliveryRecords = h.MessageBody.MessageParticipantInfos
                                            .Where(p => p.MessageDeliveryRecords!=null)
                                            .SelectMany(p => p.MessageDeliveryRecords)
                                            .ToList()
            }).ToList();

            var tskAssignBodyString = new List<Task>();
            foreach (var msgBreakdown in msgBreakdowns)
            {
                tskAssignBodyString.Add(Task.Run(async ()=>{
                    //Console.WriteLine($"getting bodystring for: {msgBreakdown.MessageBody.ID}");
                    msgBreakdown.BodyString = await this.GetHtmlMessageBody(msgBreakdown.MessageBody.ID);
                    //Console.WriteLine($"got bodystring for: {msgBreakdown.MessageBody.ID}");
                }));
            }

            await Task.WhenAll(tskAssignBodyString);
            return msgBreakdowns.Select(m => m.ToMessage()).ToList();
        }

        /// <summary>
        /// GetSingleMessage
        /// </summary>
        /// <param name="headerID"></param>
        /// <param name="includes"></param>
        public async Task<Message> GetSingleMessage(int headerID, MessageIncludes includes){
            IQueryable<MessageHeader> headerQuery = this.ctx.MessageHeader
                .Include("MessageBody")
                .Include("MessageBody.MessageParticipantInfos")
                .Include("MessageBody.MessageParticipantInfos.EmployeeData")
                .Include("MessageBody.MessageParticipantInfos.EmployeeTeam.EmployeeTeamLinks.Employee");

            headerQuery = includes.AssignIncludedProps(headerQuery);

            var header = await headerQuery.FirstOrDefaultAsync(h => h.ID == headerID);
            var msgBreakdown = new MessageBreakdown(){
                MessageBody = header.MessageBody,
                MessageHeader = header,
                MessageParticipantInfos = header.MessageBody.MessageParticipantInfos,
                MessageObjectLinks = header.MessageBody.MessageObjectLinks,
                MessageDeliveryRecords = header.MessageBody.MessageParticipantInfos
                                            .Where(p => p.MessageDeliveryRecords!=null)
                                            .SelectMany(p => p.MessageDeliveryRecords)
                                            .ToList(),
                BodyString = await this.GetHtmlMessageBody(header.MessageBody.ID)
            };
            return msgBreakdown.ToMessage();
        }

        /// <summary>
        /// CreateMessage
        /// </summary>
        /// <param name="msg">Endor.Models.Message</param>
        /// <param name="tempGUID">System.Guid</param>
        /// <returns></returns>
        public async Task<Message> CreateMessageAsync(Message msg, Guid? tempGUID = null){
            //validation start----(custom validation for MessageObject since Endor.Models.Message is not a DB entity)
            if(msg.ObjectLinks!=null){
                var duplicateObjLinks = msg.ObjectLinks.GroupBy(obj => new {obj.LinkedObjectClassTypeID, obj.LinkedObjectID}).ToList();
                if(duplicateObjLinks.Count < msg.ObjectLinks.Count){
                    var ex = new Exception("ObjectLinks must have a unique set of (obj.LinkedObjectClassTypeID, obj.LinkedObjectID)");
                    await this.logger.Error(msg.BID, ex.Message, ex);
                    return null;
                }
            }

            msg.Participants = msg.Participants ?? new List<MessageParticipantInfo>();
            var participantFROM = msg.Participants.FirstOrDefault(p => p.ParticipantRoleType==MessageParticipantRoleType.From);
            var participantTO = msg.Participants.FirstOrDefault(p => p.ParticipantRoleType==MessageParticipantRoleType.To);
            if(participantFROM==null){
                var ex = new Exception("Message requires a FROM participant");
                await this.logger.Error(msg.BID, ex.Message, ex);
                return null;
            }

            if(participantTO==null && !msg.Channels.HasFlag(MessageChannelType.Alert) && !msg.Channels.HasFlag(MessageChannelType.SystemNotification)){
                var ex = new Exception("At least one TO is required unless the Channel Type is Alerts (2) or System Notifications (128).");
                await this.logger.Error(msg.BID, ex.Message, ex);
                return null;
            }
            //validation end----

        
            
            //assign proper references
            var msgBreakdown = new MessageBreakdown(msg);
            msgBreakdown.MessageBody.MessageParticipantInfos = msgBreakdown.MessageParticipantInfos;
            msgBreakdown.MessageBody.MessageObjectLinks = msgBreakdown.MessageObjectLinks;
            //create the body
            //--strip html tags and get 50 substring
            msgBreakdown.MessageBody.BodyFirstLine = Regex.Replace(msg.Body??"", "<.*?>", String.Empty);
            if(msgBreakdown.MessageBody.BodyFirstLine.Length>50){
                msgBreakdown.MessageBody.BodyFirstLine = msgBreakdown.MessageBody.BodyFirstLine.Substring(0,50);
            }
            //--save body
            MessageBodyService msgBodySvc = MessageBodyService
                                .CreateService<MessageBodyService>(this.ctx, this.taskQueuer, this.cache, this.logger, this.BID, this.rtmClient, migrationHelper).Value;

            await msgBodySvc.DoBeforeCreateAsync(msgBreakdown.MessageBody, tempGUID);
            var newBody = await msgBodySvc.CreateAsync(msgBreakdown.MessageBody, tempGUID);

            foreach (var participant in msg.Participants)
            {
                participant.BID = this.BID;
                participant.BodyID = newBody.ID;
            }

            //set the body.html blob
            var tskSetHtmlBody = this.SetHtmlMessageBody(msg.Body, msgBreakdown.MessageBody);

            var teamMessageParticipants = msgBreakdown.MessageBody.MessageParticipantInfos.Where(p => p.TeamID.HasValue && p.ParticipantRoleType == MessageParticipantRoleType.To).ToList();

            var employeeMessageParticipants = msgBreakdown.MessageBody.MessageParticipantInfos.Where(p => p.EmployeeID.HasValue && p.ParticipantRoleType == MessageParticipantRoleType.To).ToList();

            //create headers filtered by EmployeeID
            //handled differently instead of using child services
            //already tried using headerservice as child link of participantinfoservice but bodyID is still `0` prior to parent's creation
            var employeeHeaders = msgBreakdown.MessageBody.MessageParticipantInfos.Where(p => p.EmployeeID.HasValue).Select(p => new MessageHeader
            {
                BID = this.BID,
                ClassTypeID = (int)ClassType.MessageHeader,
                EmployeeID = (short)p.EmployeeID,
                Channels = p.Channel,
                InSentFolder = p.ParticipantRoleType == MessageParticipantRoleType.From,
                IsRead = false,
                ParticipantID = p.ID,
                ModifiedDT = DateTime.UtcNow,
                ReceivedDT = DateTime.UtcNow,
                BodyID = newBody.ID
            }).ToList();

            // get team participants
            var teamParticipants = msgBreakdown.MessageBody.MessageParticipantInfos.Where(p => p.TeamID.HasValue);

            // get team participants IDs
            var teamParticipantIDs = teamParticipants.Select(p => p.TeamID ).ToList();

            // get EmployeeTeam Data filtered by the team ID on the team Participants
            var teamParticipantData = ctx.EmployeeTeam.Where(et => teamParticipantIDs.Contains(et.ID)).Include("EmployeeTeamLinks").ToList();

            var teamHeaders = new List<MessageHeader>();

            foreach (var team in teamParticipants)
            {
                // get employeeTeam Data filtered by teamID
                var employeeTeam = teamParticipantData.Where(tpd => tpd.ID == team.TeamID).FirstOrDefault();

                // check if employeeTeam is not null and if employeeTeam has employee links
                if (employeeTeam != null && employeeTeam.EmployeeTeamLinks.Count > 0)
                {
                    foreach (var employee in employeeTeam.EmployeeTeamLinks)
                    {
                        teamHeaders.Add(new MessageHeader
                        {
                            BID = this.BID,
                            ClassTypeID = (int)ClassType.MessageHeader,
                            EmployeeID = (short)employee.EmployeeID,
                            Channels = team.Channel,
                            InSentFolder = team.ParticipantRoleType == MessageParticipantRoleType.From,
                            IsRead = false,
                            ParticipantID = team.ID,
                            ModifiedDT = DateTime.UtcNow,
                            ReceivedDT = DateTime.UtcNow,
                            BodyID = newBody.ID
                        });
                    }
                }

            }

            var allHeaders = new List<MessageHeader>();

            // get the MessageParticipantRoleType.From participant
            var headerFrom = employeeHeaders.Where(h => h.InSentFolder).FirstOrDefault();

            // get all the MessageParticipantRoleType.To participants
            var headersTo = employeeHeaders.Where(h => !h.InSentFolder).ToList();

            // add headersTo and teamHeaders to allHeaders
            allHeaders.AddRange(headersTo);
            allHeaders.AddRange(teamHeaders);

            // remove duplicate Employees on allHeaders
            allHeaders = allHeaders.GroupBy(h => h.EmployeeID).Select(h => h.First()).ToList();

            // Insert headerFrom to the list of headers
            allHeaders.Add(headerFrom);

            //--save allHeaders
            foreach (var header in allHeaders)
            {
                await this.DoBeforeCreateAsync(header);
                await this.CreateAsync(header);
            }

            await tskSetHtmlBody;//do in parallel of header saving since this is not an EF task
            msgBreakdown.MessageHeader = participantFROM.MessageHeaders.FirstOrDefault();
            var messageToReturn = msgBreakdown.ToMessage();

            await QueueIndexForModel(messageToReturn.ID);

            if (msg.Channels == MessageChannelType.Email)
            {
                //Sending message is handled seperately in Email Account Service
            }
            else
            {
                foreach (var header in headersTo)
                {
                    await this.rtmClient.SendEmployeeMessage(new RTM.Models.EmployeeMessage()
                    {
                        BID = this.BID,
                        EmployeeID = header.EmployeeID,
                        Message = "You've got mail!",
                        Data = JsonConvert.SerializeObject(new
                        {
                            Subject = header.MessageBody?.Subject
                        })
                    });
                }
            }

            await this.SendMessageHeaderRefresh((int)ClassType.MessageHeader, messageToReturn.ID);
            return messageToReturn;
        }

        /// <summary>
        /// GetHtmlMessageBody
        /// </summary>
        /// <param name="messageBodyID"></param>
        /// <returns></returns>
        public async Task<string> GetHtmlMessageBody(int messageBodyID){
            var docman = this.GetDocumentManager(messageBodyID, ClassType.MessageBody, BucketRequest.Data);
            var bodyHtml = (await docman.GetDocumentsAsync()).Where(doc => doc.Name.EndsWith("body.html")).FirstOrDefault();
            if(bodyHtml!=null){
                var str = await docman.ReadTextAsync(bodyHtml);
                return str;
            }

            return "";
        }

        /// <summary>
        /// SetHtmlMessageBody
        /// </summary>
        /// <param name="htmlString"></param>
        /// <param name="msgBody"></param>
        /// <returns></returns>
        public async Task SetHtmlMessageBody(string htmlString, MessageBody msgBody){
            var docman = this.GetDocumentManager(msgBody.ID, ClassType.MessageBody, BucketRequest.Data);
            await docman.UploadTextAsync("body.html", htmlString, msgBody.ClassTypeID, msgBody.ID, "text/html");
        }

        /// <summary>
        /// MarkAsRead MessageHeader
        /// </summary>
        /// <param name="msgHeaderID">int</param>
        /// <param name="isRead">bool</param>
        /// <param name="connectionID">string</param>
        public async Task<EntityActionChangeResponse> MarkAsRead(int msgHeaderID, bool isRead, string connectionID){
            var msgHeaderOld = await this.ctx.MessageHeader.AsNoTracking().FirstOrDefaultAsync(header => header.ID==msgHeaderID && header.BID==this.BID);
            var msgHeader = await this.ctx.MessageHeader.FirstOrDefaultAsync(header => header.ID==msgHeaderID && header.BID==this.BID);
            if (msgHeader == null){
                return new EntityActionChangeResponse()
                {
                    Message = "Entity not found",
                    ErrorMessage = "Entity not found",
                    Success = false,
                    Id = Convert.ToInt32(msgHeaderID)
                };
            }

            msgHeader.IsRead = isRead;
            msgHeader.ReadDT = isRead? DateTime.UtcNow : (DateTime?)null;
            await ctx.SaveChangesAsync();
            await this.DoAfterUpdateAsync(msgHeaderOld, msgHeader, connectionID);
            return new EntityActionChangeResponse(){
                Message = $"messageHeader marked as {(isRead? "read":"unread")}",
                ErrorMessage = null,
                Success = true,
                Id = Convert.ToInt32(msgHeaderID)                
            };
        }


        /// <summary>
        /// Same to MarkAsDeleted by multiple IDs. Needed as a workaround for group actions to prevent multiple individual index request
        /// </summary>
        /// <param name="msgHeaderIDs"></param>
        /// <param name="isDeleted"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> MarkAsDeletedMany(ICollection<int> msgHeaderIDs, bool isDeleted, string connectionID)
        {
            foreach(var id in msgHeaderIDs)
            {
                await this.MarkAsDeleted(id, isDeleted, connectionID, false);
            }

            await this.QueueIndexForModels(msgHeaderIDs);
            await this.SendMessageHeaderRefresh((int)ClassType.MessageHeader);

            return new EntityActionChangeResponse()
            {
                Message = $"multiple messageHeaders {(isDeleted ? "deleted" : "undeleted")}",
                ErrorMessage = null,
                Success = true,
                Id = 0
            };
        }

        /// <summary>
        /// UndeleteMsgHeader
        /// </summary>
        /// <param name="msgHeaderID">int</param>
        /// <param name="isDeleted">bool</param>
        /// <param name="connectionID">string</param>
        /// <param name="doIndex">bool</param>
        public async Task<EntityActionChangeResponse> MarkAsDeleted(int msgHeaderID, bool isDeleted, string connectionID, bool doIndex = true){
            var msgHeaderOld = await this.ctx.MessageHeader.AsNoTracking().FirstOrDefaultAsync(header => header.ID==msgHeaderID && header.BID==this.BID);
            var msgHeader = await this.ctx.MessageHeader.FirstOrDefaultAsync(header => header.ID==msgHeaderID && header.BID==this.BID);
            if (msgHeader == null){
                return new EntityActionChangeResponse()
                {
                    Message = "Entity not found",
                    ErrorMessage = "Entity not found",
                    Success = false,
                    Id = Convert.ToInt32(msgHeaderID)
                };
            }else if(msgHeader.IsDeleted && isDeleted) {
                return new EntityActionChangeResponse(){
                    Message = $"messageHeader already deleted",
                    ErrorMessage = null,
                    Success = true,
                    Id = Convert.ToInt32(msgHeaderID)                
                };                  
            }

            msgHeader.IsDeleted = isDeleted;
            msgHeader.DeletedOrExpiredDT = isDeleted? DateTime.UtcNow:(DateTime?)null;
            await ctx.SaveChangesAsync();
            if (doIndex)
            {
                await this.DoAfterUpdateAsync(msgHeaderOld, msgHeader, connectionID);
            }
            return new EntityActionChangeResponse(){
                Message = $"messageHeader {(isDeleted? "deleted":"undeleted")}",
                ErrorMessage = null,
                Success = true,
                Id = Convert.ToInt32(msgHeaderID)                
            };            
        }

        /// <summary>
        /// MarkAllRead
        /// </summary>
        /// <param name="channel">MessageChannelType</param>
        /// <param name="employeeID">short?</param>
        /// <param name="isRead">bool</param>
        /// <param name="connectionID">string</param>
        public async Task<EntityActionChangeResponse> MarkAllRead(MessageChannelType channel, short? employeeID, bool isRead, string connectionID){
            if(!employeeID.HasValue){
                return new EntityActionChangeResponse(){
                    Message = "invalid employeeID",
                    ErrorMessage = "invalid employeeID",
                    Success = false,
                    Id = 0
                };                                        
            }
            
            var msgHeaders = await this.ctx.MessageHeader
                .Where(h=>h.EmployeeID == (short)employeeID && employeeID.HasValue)
                .Where(h => (channel==MessageChannelType.None&&h.Channels==MessageChannelType.None) || (channel!=MessageChannelType.None&&h.Channels.HasFlag(channel)))
                .ToListAsync();

            foreach (var header in msgHeaders)
            {
                await this.MarkAsRead(header.ID, true, connectionID);
            }

            return new EntityActionChangeResponse(){
                Message = $"employeeID({employeeID.Value}) msgHeaders on channel `{channel.ToString()}` all marked as read",
                ErrorMessage = null,
                Success = true,
                Id = Convert.ToInt32(employeeID.Value)
            };                        
        }

        /// <summary>
        /// MarkAllDeleted
        /// </summary>
        /// <param name="channel">Endor.Models.MessageChannelType</param>
        /// <param name="employeeID">short?</param>
        /// <param name="isDeleted">bool</param>
        /// <param name="connectionID">string</param>
        public async Task<EntityActionChangeResponse> MarkAllDeleted(MessageChannelType channel, short? employeeID, bool isDeleted, string connectionID){
            if(!employeeID.HasValue){
                return new EntityActionChangeResponse(){
                    Message = "invalid employeeID",
                    ErrorMessage = "invalid employeeID",
                    Success = false,
                    Id = 0
                };                                        
            }

            var msgHeaders = await this.ctx.MessageHeader
                .Where(h=>h.EmployeeID == (short)employeeID && employeeID.HasValue)
                .Where(h => (channel==MessageChannelType.None&&h.Channels==MessageChannelType.None) || (channel!=MessageChannelType.None&&h.Channels.HasFlag(channel)))
                .ToListAsync();

            foreach (var header in msgHeaders)
            {
                await this.MarkAsDeleted(header.ID, true, connectionID);
            }

            return new EntityActionChangeResponse(){
                Message = $"employeeID({employeeID.Value}) msgHeaders on channel `{channel.ToString()}` all marked as deleted",
                ErrorMessage = null,
                Success = true,
                Id = Convert.ToInt32(employeeID.Value)
            };
        }

        /// <summary>
        /// MessagesByChannel
        /// </summary>
        /// <param name="channel">Endor.Models.MessageChannelType</param>
        /// <param name="employeeID">short?</param>
        public async Task<IActionResult> MessagesByChannel(MessageChannelType channel, short employeeID)
        {
  
            var msgHeaders = await this.ctx.MessageHeader
                .Where(h => h.Channels.HasFlag(channel) && h.EmployeeID == (short)employeeID)
                .ToListAsync();

            return new OkObjectResult(msgHeaders);
        }

        /// <summary>
        /// Find Email Address BY ID
        /// </summary>
        /// <param name="type">ClassType</param>
        /// <param name="id"></param>
        public string GetEmailsForClassType(ClassType type, int id)
        {
            string emailAddress = "";
            switch (type)
            {
                case ClassType.Contact:
                    var result = ctx.ContactData.Where(c => c.ID == id).Include(emp => emp.ContactLocators).First()
                        .ContactLocators.Where(l => l.eLocatorType == LocatorType.Email);
                    if (result.Any()) emailAddress = result.First().Locator;
                    break;
                case ClassType.Employee:
                    var resultEmp = ctx.EmployeeData.Where(c => c.ID == id).Include(emp => emp.EmployeeLocators).First();
                    if (resultEmp.DefaultEmailAccountID != null)
                    {
                        emailAddress = ctx.EmailAccountData.Where(add => add.ID == resultEmp.DefaultEmailAccountID).First().EmailAddress;
                    }
                    else
                    {
                        var results = resultEmp.EmployeeLocators.Where(l => l.eLocatorType == LocatorType.Email);
                        if (results.Any()) emailAddress = results.First().Locator;
                    }
                    break;
            }
            return emailAddress;
        }

        /// <summary>
        /// Send MessageHeader Refresh
        /// </summary>
        /// <param name="classTypeID"></param>
        /// <param name="entityID"></param>
        /// <returns></returns>
        public async Task SendMessageHeaderRefresh(int classTypeID, int entityID = 0)
        {
            RefreshEntity refreshEntity;
            if (entityID == 0)
            {
                refreshEntity = new RefreshEntity()
                {
                    BID = this.BID,
                    ClasstypeID = classTypeID,
                    Data = "messageChange",
                    RefreshMessageType = RefreshMessageType.Change
                };
            }
            else
            {
                refreshEntity = new RefreshEntity()
                {
                    BID = this.BID,
                    ClasstypeID = classTypeID,
                    ID = entityID,
                    Data = "messageChange",
                    RefreshMessageType = RefreshMessageType.Change
                };
            }
            await this.rtmClient.SendRefreshMessage(
                new RefreshMessage()
                {//create a new refresh message
                    RefreshEntities = new List<RefreshEntity>(){//refresh message contains list of refresh entity
                        refreshEntity//list of refresh entity contains a refresh entity
                    }
                }
            );
        }

    }
}