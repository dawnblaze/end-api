﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// interface for locator parents
    /// like Employee or Location
    /// </summary>
    /// <typeparam name="M"></typeparam>
    /// <typeparam name="I"></typeparam>
    public interface ILocatorParentService<M,I>
        where M : IAtom<I>
        where I : struct, IConvertible
    {
        /// <summary>
        /// gets locators from model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        IEnumerable<BaseLocator<I, M>> GetLocators(M model);
        /// <summary>
        /// sets locators
        /// </summary>
        /// <param name="model"></param>
        /// <param name="value"></param>
        void SetLocators(M model, IEnumerable<BaseLocator<I, M>> value);
    }

    internal static class LocatorParentServiceExtensions
    {
        internal static void StripOutTemporaryLocators<M, I>(this ILocatorParentService<M, I> service, M newModel)
            where M : IAtom<I>
            where I : struct, IConvertible
        {
            IEnumerable<BaseLocator<I, M>> locators = service.GetLocators(newModel);
            //prune out locators with negative IDs
            if (locators != null && locators.Count() > 0)
            {
                service.SetLocators(newModel, locators.Where(l => l.ID >= 0));
            }
        }
    }

}
