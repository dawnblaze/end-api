﻿using Endor.Api.Web.Classes;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <inheritdoc />
    /// <summary>
    /// UserDraftService
    /// </summary>
    public class UserDraftService : AtomCRUDService<UserDraft, int>
    {

        /// <inheritdoc />
        /// <summary>
        /// constructor
        /// </summary>
        public UserDraftService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<UserDraft, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<UserDraft, int>[]
            {

            };
        }

        internal override void DoBeforeValidate(UserDraft newModel)
        {

        }

        /// <inheritdoc />
        /// <summary>
        /// GetIncludes
        /// </summary>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return new string[] { };
        }

        /// <inheritdoc />
        /// <summary>
        /// WherePrimary
        /// </summary>
        public override IQueryable<UserDraft> WherePrimary(IQueryable<UserDraft> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// DoAfterAddAsync
        /// </summary>
        /// <param name="model"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(UserDraft model, Guid? tempGuid)
        {
            model = await this.InitializeUserDraftObjectJSONData(model);
            
            var docman = this.GetDocumentManager(model.ID, ClassType.UserDraft, BucketRequest.Data);
            await docman.UploadTextAsync("draft.json", model.ObjectJSON, model.ClassTypeID, model.ID, "text/json");
            await base.DoAfterAddAsync(model, tempGuid);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public async Task DeletePendingDrafts(int ct, int ID)
        {
            if (ct != (int)ClassType.UserDraft)
            {
                List<UserDraft> allPending = await this.ctx.UserDraft.Where(x => x.BID == this.BID && x.ObjectCTID == ct && x.ObjectID == ID).ToListAsync();
                foreach(UserDraft d in allPending)
                {
                    var docman = this.GetDocumentManager(d.ID, ClassType.UserDraft, BucketRequest.Data);
                    await docman.DeleteDocumentAsync("draft.json");
                    await this.DeleteAsync(d);
                }
            }
        }

        /// <summary>
        /// Returns the UserDraft for the given filters
        /// </summary>
        /// <param name="filters">UserDraft Filters</param>
        /// <param name="userID">The calling User's ID</param>
        /// <returns></returns>
        public async Task<List<UserDraft>> GetWithFiltersAsync(UserDraftFilters filters, int? userID)
        {
            if (filters != null)
                return await this.GetWhere(filters.WherePredicates(userID));
            else
                return await this.GetAsync(null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public async override Task DoBeforeUpdateAsync(UserDraft oldModel, UserDraft newModel)
        {
            await base.DoBeforeUpdateAsync(oldModel, newModel);
            //reset ExpiryDT to 30 days ahead from ModifiedDT
            if (newModel != null && newModel.ModifiedDT != null)
            {
                newModel.ExpirationDT = newModel.ModifiedDT.AddDays(30);
            }
        }

        /// <summary>
        /// Business logic for after having updated a model
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel">Model object to update</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(UserDraft oldModel, UserDraft newModel, string connectionID)
        {
            newModel = await this.InitializeUserDraftObjectJSONData(newModel);

            var docman = this.GetDocumentManager(newModel.ID, ClassType.UserDraft, BucketRequest.Data);
            // First, Delete the existing json file
            await docman.DeleteDocumentAsync("draft.json");
            // Second, Upload a new one
            await docman.UploadTextAsync("draft.json", newModel.ObjectJSON, newModel.ClassTypeID, newModel.ID, "text/json");

            await base.DoAfterUpdateAsync(oldModel, newModel, connectionID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public async Task<string> GetObjectJSON(int ID)
        {
            var docman = this.GetDocumentManager(ID, ClassType.UserDraft, BucketRequest.Data);
            var draftJson = (await docman.GetDocumentsAsync()).Where(doc => doc.Name.EndsWith("draft.json")).FirstOrDefault();
            if (draftJson != null)
            {
                var str = await docman.ReadTextAsync(draftJson);
                return str;
            }

            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public async Task<IActionResult> GetUserDraftByID(int ID)
        {
            UserDraft model = await ctx.UserDraft.Where(udd => udd.BID == BID && udd.ID == ID).FirstOrDefaultAsync();

            if (model == null)
            {
                return new NotFoundResult();
            }

            model.ObjectJSON = await GetObjectJSON(ID);

            return new OkObjectResult(model);
        }

        /// <summary>
        /// Initialize UserDraft ObjectJSON Data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task<UserDraft> InitializeUserDraftObjectJSONData(UserDraft model)
        {
            if (model != null && model?.ObjectJSON != null)
            {
                if (model?.ObjectCTID == (int)ClassType.Order || model?.ObjectCTID == (int)ClassType.Estimate)
                {
                    TransactionHeaderData transactionHeaderData = null;

                    if (model?.ObjectCTID == (int)ClassType.Order)
                    {
                        transactionHeaderData = JsonConvert.DeserializeObject<OrderData>(model?.ObjectJSON);
                    }

                    if (model?.ObjectCTID == (int)ClassType.Estimate)
                    {
                        transactionHeaderData = JsonConvert.DeserializeObject<EstimateData>(model?.ObjectJSON);
                    }

                    if (transactionHeaderData != null)
                    {
                        transactionHeaderData.Company = await ctx.CompanyData.Where(cd => cd.BID == model.BID && cd.ID == transactionHeaderData.CompanyID).FirstOrDefaultAsync();

                        if (transactionHeaderData.ContactRoles != null)
                        {
                            var primary = transactionHeaderData.ContactRoles.FirstOrDefault(x => x.RoleType == OrderContactRoleType.Primary);

                            if (primary != null)
                            {
                                transactionHeaderData.ContactRoles.Remove(primary);
                                primary.Contact = await ctx.ContactData.Where(cd => cd.BID == model.BID && cd.ID == primary.ContactID).FirstOrDefaultAsync();
                                transactionHeaderData.ContactRoles.Add(primary);
                            }
                        }
                    }

                    model.Object = transactionHeaderData;
                }

                if (model?.Object != null)
                {
                    model.ObjectJSON = JsonConvert.SerializeObject(model?.Object);
                }

            }

            return model;
        }

        /// <summary>
        /// UserDraft Service Override of DoAfterDeleteAsync
        /// </summary>
        /// <param name="model">UserDraft Model</param>
        /// <returns></returns>
        protected async override Task DoAfterDeleteAsync(UserDraft model)
        {
            var docman = this.GetDocumentManager(model.ID, ClassType.UserDraft, BucketRequest.Data);
            //  Delete the existing json file
            await docman.DeleteDocumentAsync("draft.json");

            //Call base - This will re-index the UserDraft itself
            await base.DoAfterDeleteAsync(model);
        }

        /// <summary>
        /// UserDraft Service Override of DoBeforeCloneAsync
        /// </summary>
        /// <param name="clone">Cloned AlertDefinitionAction</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(UserDraft clone)
        {
            clone.ExpirationDT = DateTime.UtcNow.AddDays(30);
            clone.ObjectJSON = await GetObjectJSON(clone.ID);
            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// UserDraft Service Override of DoAfterCloneAsync
        /// </summary>
        /// <param name="clonedFromID">Cloned UserDraft ID</param>
        /// <param name="clone">Cloned UserDraft</param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(int clonedFromID, UserDraft clone)
        {
            var docman = this.GetDocumentManager(clonedFromID, ClassType.UserDraft, BucketRequest.Data);
            //Copy Documents Bucket
            await docman.CloneAllBucketBlobsAsync(clone.ID, Bucket.Data, includeSpecialFolderBlobName: true);

            //Call base - This will re-index the UserDraft itself
            await base.DoAfterCloneAsync(clonedFromID, clone);
        }
    }
}
