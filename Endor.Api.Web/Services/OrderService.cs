﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Clones;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Order Service Layer
    /// </summary>
    public class OrderService : BaseTransactionService<OrderData>
    {
        private OrderCloneOption _cloneOption;

        /// <summary>
        /// Defines this controller as for Estimates
        /// </summary>
        protected override OrderTransactionType TransactionType => OrderTransactionType.Order;

        /// <summary>
        /// Order Service Layer
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache,
            RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid,
            taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        public async Task<IActionResult> CloneTransactionWithOptionAsync(int ID, OrderCloneOption cloneOption, short? userLinkId, OrderTransactionType? newTransactionType = null)
        {
            _cloneOption = cloneOption;
            var result = await base.CloneTransactionAsync(ID, cloneOption.KeepFiles == true, newTransactionType, userLinkId);
            if (result == null)
            {
                return new BadRequestObjectResult("Clone failed");
            }
            else
            {
                return new OkObjectResult(result);
            }
        }

        /// <summary>
        /// Get all Locations without Open Order
        /// </summary>
        /// <returns></returns>
        public async Task<List<byte>> GetLocationWithoutOpenOrder()
        {
            var locationOpenOrders = await this.ctx.OrderData
                    .Where(o => o.BID == this.BID && o.PaymentBalanceDue > 0)
                    .Select(o => o.LocationID)
                    .Distinct()
                    .ToListAsync();
            var locations = await this.ctx.LocationData.Where(o => o.BID == this.BID)
                    .Select(o => o.ID)
                    .ToListAsync();
            return locations.Where(c => !locationOpenOrders.Any(c2 => c2 == c)).ToList();
        }

        /// <summary>
        /// Get all Companies without Open Order
        /// </summary>
        /// <returns></returns>
        public async Task<List<int>> GetCompanyWithoutOpenOrder()
        {
            var companyOpenOrders = await this.ctx.OrderData
                    .Where(o => o.BID == this.BID && o.PaymentBalanceDue > 0)
                    .Select(o => o.CompanyID)
                    .Distinct()
                    .ToListAsync();
            var companies = await this.ctx.CompanyData.Where(o => o.BID == this.BID)
                    .Select(o => o.ID)
                    .ToListAsync();
            return companies.Where(c => !companyOpenOrders.Any(c2 => c2 == c)).ToList();
        }


        protected override async Task DoAfterCloneAsync(int clonedFromID, OrderData clone, bool? CopyFiles, OrderOrderStatus? newStatus, short? newItemStatus, short? userLinkID)
        {

            if (clone.Links == null || clone.Links.Count < 0)
            {
                var clonedFrom = await this.ctx.Set<OrderData>().AsNoTracking().FirstOrDefaultAsync(x => x.BID == this.BID && x.ID == clonedFromID);
                ctx.OrderOrderLink.Add(
                    new OrderOrderLink()
                    {
                        BID = this.BID,
                        OrderID = clone.ID,
                        LinkType = OrderOrderLinkType.ClonedFrom,
                        LinkedOrderID = clonedFromID,
                        LinkedFormattedNumber = clonedFrom.FormattedNumber,
                        Description = "",
                    }
                );
                ctx.OrderOrderLink.Add(
                    new OrderOrderLink()
                    {
                        BID = this.BID,
                        OrderID = clonedFromID,
                        LinkType = OrderOrderLinkType.ClonedTo,
                        LinkedOrderID = clone.ID,
                        LinkedFormattedNumber = clone.FormattedNumber,
                        Description = "",
                    }
                );

                try
                {
                    await ctx.SaveChangesAsync();
                    var UserLink = ctx.UserLink.First(x => x.BID == this.BID && x.ID == userLinkID);
                    await _taskQueuer.RecomputeGL(this.BID, (byte)GLEngine.Models.EnumGLType.Order, clone.ID, "Order Cloned", "Order Cloned", UserLink.EmployeeID, null, (byte)GLEntryType.Order_New);
                }
                catch (Exception)
                {

                }

            }

            await base.DoAfterCloneAsync(clonedFromID, clone);

            
            if (_cloneOption != null )
            {
                if (_cloneOption.OrderStatus != null)
                {
                    clone.OrderStatusID = _cloneOption.OrderStatus.Value;
                }
                if (_cloneOption.KeepFiles == true)
                {
                    CopyFiles = true;
                }
            }


            if (!CopyFiles.HasValue)
            {
                //read option from db
                string key = (((ClassType)clone.ClassTypeID) == ClassType.Order) ? OptionKeyCopyDocuments : OptionEstimateKeyCopyDocuments;
                CopyFiles = (await base.GetUserOptionValueByName(key, userLinkID)).ValueAsBoolean();
            }
            if (CopyFiles.GetValueOrDefault())
            {
                // Get clone from object for reference of the copy files
                var clonedFrom = await this.ctx.Set<OrderData>().AsNoTracking().FirstOrDefaultAsync(x => x.BID == this.BID && x.ID == clonedFromID);

                //Copy All Files
                DocumentManager client = base.GetDocumentManager(clonedFromID, (ClassType)clonedFrom.ClassTypeID, BucketRequest.All);
                await client.CloneAllDocumentsToClassTypeAsync(clone.ID, clone.ClassTypeID, includeSpecialFolderBlobName: true);
            }

            //Need to clone Contact and Employee roles...
            await CloneContactAndEmployeeRole(clonedFromID, null, clone.ID, null);

            //Need to update the primary contact
            await UpdateOrderContactToPrimary(clone.ID, clone.CompanyID);

            //Update Entered By to User
            OrderEmployeeRoleService oerservice = new OrderEmployeeRoleService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
            var employeeRoleId = ctx.EmployeeRole.FirstOrDefault(er => er.Name == "Entered By").ID;
            await oerservice.RemoveEmployeeRole(clone.ID, employeeRoleId);
            short? employeeID = ctx.UserLink.First(ul => ul.BID == BID && ul.ID == userLinkID).EmployeeID;
            if (employeeID.HasValue)
                await oerservice.AddEmployeeRole(clone.ID, employeeRoleId, employeeID.Value);

            //Clone notes
            if (_cloneOption == null || _cloneOption.KeepNotes == true)
            {
                await CloneNotes(clonedFromID, null, clone.ID, null);
            }

            await CreateKeyDateBasedOnOrderStatus(clone.ID, clone.OrderStatusID, true);
            if (clone.TransactionType == (byte)OrderTransactionType.Estimate)
            {
                await SetCloneDueDate(clone.ID, clone.TransactionType, _cloneOption?.FollowupDate ?? DateTime.UtcNow.AddDays(1));
            }
            else
            {
                await SetCloneDueDate(clone.ID, clone.TransactionType, _cloneOption?.DueDate ?? DateTime.UtcNow.AddDays(1));
            }

            //Need to clone order items...
            //Note: CopyFiles value is guaranteed a value on the code above.            
            await CloneOrderItemData(clonedFromID, clone.ID, newStatus.Value, CopyFiles.GetValueOrDefault(), (OrderTransactionType)clone.TransactionType, newItemStatus);

            //update default line items status
            await UpdateLineItemStatuses(clone.ID, Convert.ToInt16(clone.OrderStatusID), Convert.ToInt16(clone.TransactionType));

            await CloneCustomData(clonedFromID, clone.ID);
            await UpdateCompanyStatus(clone);

            //
            

            if (_cloneOption != null && _cloneOption.UpdatePricing == true)
            {
                await taskQueuer.OrderPricingComputeTax(BID, clone.ID);
                await taskQueuer.OrderPricingCompute(BID, clone.ID);
            }

            await taskQueuer.IndexModel(this.BID, Convert.ToInt32(clone.ClassTypeID), Convert.ToInt32(clone.ID));

            await rtmClient.SendRefreshMessage(new RefreshMessage()
            {
                RefreshEntities = new List<RefreshEntity>()
                    {
                        new RefreshEntity()
                        {
                            ClasstypeID = clone.ClassTypeID,
                            DateTime = clone.ModifiedDT,
                            RefreshMessageType = RefreshMessageType.New,
                            BID = clone.BID,
                        }
                    }
            });

 
        }

        public async Task<int?> GetPreviewNumber(ClassType? appliesToCTID){
            int? previewNum = null;
            switch (appliesToCTID)
            {
                case ClassType.Order:
                    previewNum = await this.ctx.OrderData
                                    .WherePrimary(this.BID, x => true)
                                    .MaxAsync(x => (int?)x.Number);
                    break;
                case ClassType.Estimate:
                    previewNum = await this.ctx.EstimateData
                                    .WherePrimary(this.BID, x => true)
                                    .MaxAsync(x => (int?)x.Number);
                    break;
                case ClassType.PurchaseOrder:
                    previewNum = await this.ctx.PurchaseOrderData
                                    .WherePrimary(this.BID, x => true)
                                    .MaxAsync(x => (int?)x.Number);
                    break;
                case ClassType.Opportunity:
                    previewNum = await this.ctx.OpportunityData
                                    .Where(x => x.BID == this.BID)//OpportunityData is not IAtom
                                    .MaxAsync(x => (int?)x.ID);
                    break;
                case ClassType.Company:
                    previewNum = (await this.ctx.CompanyData
                                    .WherePrimary(this.BID, x => x.IsActive && !x.IsAdHoc)
                                    .OrderBy(x => x.Name)
                                    .FirstOrDefaultAsync())?.ID;
                    break;
                case ClassType.Contact:
                    previewNum = (await this.ctx.ContactData
                                    .WherePrimary(this.BID, x => true)
                                    .OrderBy(x => x.LongName)
                                    .FirstOrDefaultAsync())?.ID;
                    break;
                case ClassType.Employee:
                    previewNum = (await this.ctx.EmployeeData
                                    .WherePrimary(this.BID, x => x.IsActive)
                                    .OrderBy(x => x.LongName)
                                    .FirstOrDefaultAsync())?.ID;
                    break;
            }

            return previewNum;
        }
    }

}