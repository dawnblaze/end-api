﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Order Note Service Layer
    /// </summary>
    public class OrderItemNoteService : AtomCRUDService<OrderNote, int>, ICloneableChildCRUDService<OrderData, OrderNote>
    {
        /// <summary>
        /// Order Note Service Layer
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderItemNoteService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default children to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] {
            "Employee",
            "Contact"
        };

        /// <summary>
        /// gets Child associations (none)
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<OrderNote, int>[] GetChildAssociations()
        {
            return null;
        }

        internal override void DoBeforeValidate(OrderNote newModel)
        {

        }

        /// <summary>
        /// Returns the includes
        /// </summary>
        /// <param name="includes">Children to include</param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Creates or Updates an OrderNote
        /// </summary>
        /// <param name="orderItemId"></param>
        /// <param name="userlinkID"></param>
        /// <param name="noteID"></param>
        /// <param name="request"></param>
        /// <param name="connectionID"></param>
        /// <returns>Task&lt;OrderNote&gt;<see cref="Task&lt;OrderNote&gt;"/></returns>
        public async Task<OrderNote> UpdateNote(int orderItemId, short userlinkID, int noteID, OrderNoteRequest request, string connectionID)
        {
            if (string.IsNullOrWhiteSpace(request?.Note))
                return null;

            OrderItemData orderItem = ctx.OrderItemData.FirstOrDefault(t => t.ID == orderItemId);

            if (orderItem == null)
                return null;

            //Try and locate an existing note
            var note = await this.FirstOrDefaultAsync(n => n.OrderItemID == orderItemId && n.IsOrderItemNote && n.ID == noteID);

            if (note == null)
                return null;

            if (string.IsNullOrWhiteSpace(note.Note))
                note.Note = request.Note;
            else
                note.Note = note.Note + "\r\n\r\n" + request.Note;

            await this.UpdateAsync(note, connectionID);
            return note;
        }

        /// <summary>
        /// Creates or Updates an OrderNote
        /// </summary>
        /// <param name="orderItemId"></param>
        /// <param name="noteType"></param>
        /// <param name="orderNote"></param>
        /// <param name="connectionID"></param>
        /// <returns>Task&lt;OrderNote&gt;<see cref="Task&lt;OrderNote&gt;"/></returns>
        public async Task<OrderNote> CreateOrUpdate(int orderItemId, OrderNoteType noteType, OrderNote orderNote, string connectionID)
        {
            OrderItemData orderItem = ctx.OrderItemData.FirstOrDefault(t => t.ID == orderItemId);

            if (orderItem == null)
                return null;

            //Try and locate an existing note
            OrderNote note = await this.FirstOrDefaultAsync(n => n.OrderItemID == orderItemId && n.IsOrderItemNote && n.NoteType == noteType);

            if (note != null)
            {//Update        
                note.Note = orderNote.Note;
                note.NoteType = noteType;

                //SetUpdateModelProperties(note.ID, orderNote);

                await this.UpdateAsync(note, connectionID);
                return note;
            }
            else
            {//Create         
                SetCreateModelProperties(orderNote);
                orderNote.OrderID = orderItem.OrderID;
                orderNote.OrderItemID = orderItemId;
                orderNote.NoteType = noteType;
                await this.CreateAsync(orderNote);

                return await this.FirstOrDefaultAsync(n => n.OrderItemID == orderItemId && n.IsOrderItemNote && n.NoteType == noteType);
            }

        }

        /// <summary>
        /// Attempts to Delete an OrderNote by noteType
        /// </summary>
        /// <param name="orderItemId"></param>
        /// <param name="noteType"></param>
        /// <returns>Task&lt;OrderNote&gt;<see cref="Task&lt;OrderNote&gt;"/></returns>
        public async Task<EntityActionChangeResponse> DeleteAsync(int orderItemId, OrderNoteType noteType)
        {
            //Locate note to be deleted
            try
            {
                var note = await this.FirstOrDefaultAsync(n => n.OrderItemID == orderItemId && n.IsOrderItemNote && n.NoteType == noteType, asNoTracking: true);

                if (note == null)
                {
                    return new EntityActionChangeResponse
                    {
                        Id = orderItemId,
                        Message = "No rows affected.",
                        Success = false
                    };
                }

                BooleanResponse canDelete = await CanDelete(note.ID);

                if (canDelete.Success)
                {
                    bool result = await DeleteAsync(note);

                    if (result)
                    {
                        await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnDelete(note) });

                        return new EntityActionChangeResponse
                        {
                            Id = orderItemId,
                            Message = "Successfully deleted order note.",
                            Success = true
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = orderItemId,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }

            return new EntityActionChangeResponse
            {
                Id = orderItemId,
                Message = "No rows affected.",
                Success = false
            };
        }

        /// <summary>
        /// query filter for where primary key of id
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<OrderNote> WherePrimary(IQueryable<OrderNote> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Sets the ID of childModel to a new ID and the ParentID of childModel to parentModel.ID
        /// </summary>
        /// <param name="parentModel"></param>
        /// <param name="childModel"></param>
        /// <returns></returns>
        public Task DoBeforeCloneForParent(OrderData parentModel, OrderNote childModel)
        {
            childModel.OrderID = parentModel.ID;
            return Task.CompletedTask;
        }

        /// <summary>
        /// Before create
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(OrderNote newModel, Guid? tempGuid = null)
        {
            newModel.CreatedDT = DateTime.UtcNow;

            OrderItemData orderItemData = ctx.OrderItemData.Where(oid => oid.BID == BID && oid.ID == newModel.OrderItemID).FirstOrDefault();

            if (orderItemData != null)
            {
                newModel.OrderID = orderItemData.OrderID;
            }

            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }
    }
}
