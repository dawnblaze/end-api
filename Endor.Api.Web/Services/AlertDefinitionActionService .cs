﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// AlertDefinitionAction Service
    /// </summary>
    public class AlertDefinitionActionService : AtomCRUDService<AlertDefinitionAction, short>
    {
        /// <summary>
        /// Constructs a AlertDefinitionAction service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public AlertDefinitionActionService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Gets a list of AlertDefinitionAction for the specified filters
        /// </summary>
        /// <param name="filters">AlertDefinitionAction Filters</param>
        /// <returns></returns>
        public async Task<List<AlertDefinitionAction>> GetAllWithFilters(AlertDefinitionActionFilter filters)
        {
            return await ctx.Set<AlertDefinitionAction>().IncludeAll(GetIncludes())
                .Where(ada => ada.BID == BID)
                .WhereAll(filters.WherePredicates())
                .ToListAsync();
        }


        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// AlertDefinitionAction Service Override of DoBeforeCreate
        /// </summary>
        /// <param name="newModel">AlertDefinitionAction</param>
        /// <param name="tempGuid">Optionala Temp Guid</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(AlertDefinitionAction newModel, Guid? tempGuid = null)
        {
            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// AlertDefinitionAction Service Override of DoBeforeUpdateAsync
        /// </summary>
        /// <param name="oldModel">Old AlertDefinitionAction</param>
        /// <param name="updatedModel">Updated AlertDefinitionAction</param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(AlertDefinitionAction oldModel, AlertDefinitionAction updatedModel)
        {
            await base.DoBeforeUpdateAsync(oldModel, updatedModel);
        }

        /// <summary>
        /// AlertDefinitionAction Service Override of DoBeforeCloneAsync
        /// </summary>
        /// <param name="clone">Cloned AlertDefinitionAction</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(AlertDefinitionAction clone)
        {
            clone.Subject = clone.Subject + " (Clone)";
            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<AlertDefinitionAction, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<AlertDefinitionAction, short>[0];
        }

        internal override void DoBeforeValidate(AlertDefinitionAction newModel) { }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<AlertDefinitionAction> WherePrimary(IQueryable<AlertDefinitionAction> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public override List<RefreshEntity> DoGetRefreshMessagesOnDelete(AlertDefinitionAction deleted)
        {
            return base.DoGetRefreshMessagesOnDelete(deleted).Append(new RefreshEntity()
            {
                BID = deleted.BID,
                ClasstypeID = Convert.ToInt32(ClassType.AlertDefinition),
                ID = Convert.ToInt32(deleted.AlertDefinitionID),
                DateTime = DateTime.UtcNow,
                RefreshMessageType = RefreshMessageType.Change,
            }).ToList();
        }

    }

}
