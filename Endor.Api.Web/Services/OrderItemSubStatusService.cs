﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Includes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Endor.RTM;
using System.Data;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// order item substatus service
    /// </summary>
    public class OrderItemSubStatusService : AtomCRUDService<OrderItemSubStatus, short>
    {
        /// <summary>
        /// Constructs a new OrderItemSubStatusService with a number of injected parameters
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderItemSubStatusService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) :
            base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            /*if (includes == null)
                return null;

            var tempIncludes = (IExpandIncludes)Activator.CreateInstance(includes.GetType());

            foreach (var prop in includes.GetType().GetProperties())
            {
                prop.SetValue(tempIncludes, prop.GetValue(includes));
            }

            if (tempIncludes != null)
            {
                if (((OrderItemSubStatusIncludes)tempIncludes).OrderItemStatuses == IncludesLevel.None)
                {
                    // Removing all Simple collections that are implemented as select in C# 
                    // but NOT implemented in EF and SQL
                    ((OrderItemSubStatusIncludes)tempIncludes).OrderItemStatuses = IncludesLevel.Full;
                }
            }
            return tempIncludes.ToIncludesArray(this.IncludeDefaults);*/
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// fills SimpleOrderItemStatuses
        /// </summary>
        /// <param name="item"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override Task MapItem(OrderItemSubStatus item, IExpandIncludes includes = null)
        {
            if (includes != null)
            {
                if (((OrderItemSubStatusIncludes)includes).OrderItemStatuses == IncludesLevel.Simple)
                {
                    if (item.OrderItemStatusSubStatusLinks != null)
                    {
                        item.SimpleOrderItemStatuses = item.OrderItemStatusSubStatusLinks
                                            .Select(link => new SimpleOrderItemStatus()
                                            {
                                                BID = link.BID,
                                                ID = link.StatusID,
                                                ClassTypeID = link.OrderItemStatus.ClassTypeID,
                                                DisplayName = link.OrderItemStatus.Name,
                                                IsActive = link.OrderItemStatus.IsActive,
                                                IsDefault = link.OrderItemStatus.IsDefault,
                                                OrderStatusID = (link.OrderItemStatus.OrderStatusID.HasValue ? link.OrderItemStatus.OrderStatusID.Value : 0),
                                                TransactionType = link.OrderItemStatus.TransactionType
                                            }).ToList();

                        item.OrderItemStatusSubStatusLinks = null;
                    }

                    else
                        item.SimpleOrderItemStatuses = null;
                }
                if (((OrderItemSubStatusIncludes)includes).OrderItemStatuses == IncludesLevel.Full)
                {
                    if (item.OrderItemStatusSubStatusLinks != null)
                    {
                        item.OrderItemStatuses = item.OrderItemStatusSubStatusLinks.Select(link => link.OrderItemStatus).ToList();
                        item.OrderItemStatusSubStatusLinks = null;
                    }

                    else
                        item.OrderItemStatuses = null;
                }
            }

            return Task.CompletedTask;
        }
        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<OrderItemSubStatus, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<OrderItemSubStatus, short>[]
            {
                //CreateChildAssociation<OrderItemSubStatusService, OrderItemSubStatus, int>((a)=>a.SubGroups)
            };
        }

        internal IEnumerable<SimpleOrderItemSubStatus> GetSimpleList(short value, SimpleOrderItemSubStatusFilters filters)
        {
            return this.SelectWhereAll(filters.WherePredicates(), filters);
        }

        /// <summary>
        /// Links/Unlinks an OrderItemSubStatus by its ID to an OrderItemStatus by its ID or Name
        /// </summary>
        /// <param name="orderItemSubStatusID">OrderItemSubStatus's ID</param>
        /// <param name="linkStatus">OrderItemStatus's ID or Name</param>
        /// <param name="isLinked">Is this to Link the SubStatus with the Status</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkStatus(short orderItemSubStatusID, string linkStatus, bool isLinked)
        {
            int statusID = 0;
            if (!Int32.TryParse(linkStatus, out statusID))
            {
                var orderItemStatus = await ctx.OrderItemStatus.Where(o => o.Name == linkStatus).FirstOrDefaultAsync();
                if (orderItemStatus != null)
                {
                    statusID = orderItemStatus.ID;
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = orderItemSubStatusID,
                        Message = $"OrderItemStatus '{linkStatus}' not found.",
                        Success = false
                    };
                }
            }

            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", SqlDbType.Int) { Direction = ParameterDirection.Output };

                object[] myParams =
                {
                    new SqlParameter("@BID", this.BID),
                    new SqlParameter("@SubStatusID", orderItemSubStatusID),
                    new SqlParameter("@OrderStatusID", statusID),
                    new SqlParameter("@IsLinked", isLinked),
                    resultParam
                };

                await this.ctx.Database.ExecuteSqlRawAsync(
                    "EXEC [dbo].[Order.Item.Status.Action.LinkSubStatus] @BID, @SubStatusID, @OrderStatusID, @IsLinked, @Result = @Result Output;",
                    myParams);

                int resultParamValue = (int)resultParam.Value;

                if (resultParamValue > 0)
                {
                    var linkStr = isLinked ? "Linked" : "Unlinked";
                    return new EntityActionChangeResponse()
                    {
                        Id = orderItemSubStatusID,
                        Message = $"Successfully {linkStr} OrderItemSubStatus (ID: {orderItemSubStatusID}) with OrderItemStatus (ID: {statusID}) .",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = orderItemSubStatusID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = orderItemSubStatusID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        internal override void DoBeforeValidate(OrderItemSubStatus newModel)
        {
            
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<OrderItemSubStatus> WherePrimary(IQueryable<OrderItemSubStatus> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }
    }
}
