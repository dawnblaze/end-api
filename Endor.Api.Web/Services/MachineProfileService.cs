﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// assembly Table service
    /// </summary>
    public class MachineProfileService : AtomCRUDService<MachineProfile, int>, IDoBeforeCreateUpdateWithParent<MachineProfile, MachineData>
    {
        /// <summary>
        /// Constructs a AssemblyTableService service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper"></param>
        public MachineProfileService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { nameof(MachineProfile.MachineProfileTables), nameof(MachineProfile.MachineProfileVariables) };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(IncludeDefaults);
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">AssemblyTable ID</param>
        /// <returns></returns>
        public override IQueryable<MachineProfile> WherePrimary(IQueryable<MachineProfile> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// get old model
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task<MachineProfile> GetOldModelAsync(MachineProfile newModel)
        {
            IQueryable<MachineProfile> oldModelQry = ctx.MachineProfile
                                                    .Include(p => p.MachineProfileTables)
                                                    .Include(p => p.MachineProfileVariables)
                                                    .AsNoTracking();
            var oldModel = await WherePrimary(oldModelQry, newModel.ID).FirstOrDefaultAsync();

            return oldModel;
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<MachineProfile, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<MachineProfile, int>[]{
                CreateChildAssociation<MachineProfileTableService, MachineProfileTable, short>((a) => a.MachineProfileTables),
                CreateChildAssociation<MachineProfileVariableService, MachineProfileVariable, short>((a) => a.MachineProfileVariables)
                // CreateChildAssociation<AssemblyService, AssemblyTable, int>(e => e.AssemblyID),
            };
        }

        internal override void DoBeforeValidate(MachineProfile newModel)
        {

        }

        /// <summary>
        /// set machine ID and BID of child
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public void DoBeforeCreateWithParent(MachineProfile child, MachineData parent)
        {
            child.MachineID = parent.ID;
            child.MachineTemplateID = parent.MachineTemplateID.GetValueOrDefault(0);
            child.BID = parent.BID;
        }

        /// <summary>
        /// set machine ID and BID of child
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        public void DoBeforeUpdateWithParent(MachineProfile child, MachineData oldParent, MachineData newParent)
        {
            child.MachineID = newParent.ID;
            child.MachineTemplateID = newParent.MachineTemplateID.GetValueOrDefault(0);
            child.BID = newParent.BID;
        }

    }
}
