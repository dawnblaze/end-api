﻿using Endor.EF;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Endor.Tasks;
using Endor.Api.Web.Classes;
using Endor.Tenant;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Models;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Compnay Service
    /// </summary>
    public class CompanyService : AtomCRUDService<CompanyData, int>, ISimpleListableViewService<SimpleCompanyData, int>, ILocatorParentService<CompanyData, int>
    {
        /// <summary>
        /// Property to get  SimpleCompanyData from ApiContext
        /// </summary>
        public DbSet<SimpleCompanyData> SimpleListSet => ctx.SimpleCompanyData;

        /// <summary>
        /// Company Service Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">ITaskQueuer</param>
        /// <param name="cache">ITenantDataCache</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CompanyService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Default Child Properties to include from Database Context
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "CompanyLocators" };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            if (includes == null)
                return null;

            var tempIncludes = (IExpandIncludes)Activator.CreateInstance(includes.GetType());

            foreach (var prop in includes.GetType().GetProperties())
            {
                prop.SetValue(tempIncludes, prop.GetValue(includes));
            }

            if (tempIncludes != null)
            {
                // Removing CustomFields prevents errors because CFValues is not a navigation property
                ((CompanyIncludes)tempIncludes).CustomFields = IncludesLevel.None;
            }

            return tempIncludes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Maps the Includes for custom fields for a single Company
        /// </summary>
        /// <param name="item">Order to map</param>
        /// <param name="includes">Includes to map</param>
        /// <returns></returns>
        public override async Task MapItem(CompanyData item, IExpandIncludes includes = null)
        {
            if (includes != null)
            {
                if (((CompanyIncludes)includes).CustomFields != IncludesLevel.None)
                {
                    item.LoadCFValues(ctx);
                }
            }

            await Task.Yield();
        }

        /// <summary>
        /// Actions to perform before creating the Contact
        /// </summary>
        /// <param name="newModel">Contact</param>
        /// <returns></returns>
        internal async Task PrepareModelForCreateAsync(CompanyData newModel)
        {
            if (newModel == null)
                return;

            if (newModel.LocationID == 0)
            {
                LocationData defaultLocation = await GetDefaultLocation(this.BID);
                newModel.LocationID = defaultLocation.ID;
            }

            if (newModel.CompanyContactLinks != null && newModel.CompanyContactLinks.Count > 0)
            {
                await VerifyCompanyContactLinksHaveValidContactIDs(newModel);
            }
        }

        /// <summary>
        /// Creates any new Contacts for the Company
        /// </summary>
        /// <param name="company">Company</param>
        /// <returns></returns>
        private async Task VerifyCompanyContactLinksHaveValidContactIDs(CompanyData company)
        {
            foreach (var link in company.CompanyContactLinks.ToList())
            {
                var contactToCheck = await this.ctx.ContactData.FirstOrDefaultAsync(x => x.BID == BID && x.ID == link.ContactID);
                if (contactToCheck == null)
                {
                    throw new InvalidOperationException("Company is trying to create a link to a contact that doesn't exist.");
                }
            }
        }

        /// <summary>
        /// Gets the Default Location for the Business
        /// </summary>
        /// <param name="BID">Business ID</param>
        /// <returns></returns>
        private async Task<LocationData> GetDefaultLocation(short BID)
        {
            BusinessData business = await this.ctx.BusinessData
                                                        .Include(b => b.Locations)
                                                        .FirstOrDefaultAsync(b => b.BID == BID);

            LocationData defaultLocation = business.Locations.FirstOrDefault(l => l.IsDefault);
            if (defaultLocation == null)
            {
                defaultLocation = this.ctx.LocationData.FirstOrDefault(x => x.BID == this.BID);
            }

            return defaultLocation;
        }

        /// <summary>
        /// Comapany Service Override of DoBeforeCreate
        /// </summary>
        /// <param name="newModel">CompanyData</param>
        /// <param name="tempGuid">Optionala Temp Guid</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(CompanyData newModel, Guid? tempGuid = null)
        {
            this.SetPricingTierIfNull(newModel);
            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// ensures that the company has a pricing tier set
        /// </summary>
        /// <param name="company"></param>
        protected void SetPricingTierIfNull(CompanyData company)
        {
            if ((company?.PricingTierID ?? 0) == 0)
            {
                company.PricingTierID = 100;//https://corebridge.atlassian.net/browse/END-2910
            }
        }

        /// <summary>
        /// CompanyService Override of DoBeforeUpdateAsync
        /// </summary>
        /// <param name="oldModel">Old CompanyData</param>
        /// <param name="newModel">Updated CompanyData</param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(CompanyData oldModel, CompanyData newModel)
        {
            if (oldModel.NonRefundableCredit != newModel.NonRefundableCredit || oldModel.RefundableCredit != newModel.RefundableCredit)
                throw new InvalidOperationException("Non refundable and refundable credit must match the value already present in the company");

            short? defaultTaxGroupIDFromLocation = this.ctx.LocationData.AsNoTracking().Where(x => x.BID == this.BID && x.ID == newModel.LocationID).Select(x => x.DefaultTaxGroupID).FirstOrDefault();

            if (defaultTaxGroupIDFromLocation.HasValue && newModel.TaxGroupID == defaultTaxGroupIDFromLocation.Value)
            {
                newModel.TaxGroupID = null;
            }

            this.SetPricingTierIfNull(newModel);

            await this.UpdateContactCompanyLink(newModel);
            await base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        /// <summary>
        /// Company Service Override of DoAfterUpdateAsync
        /// </summary>
        /// <param name="oldModel">Old CompanyData</param>
        /// <param name="updatedModel">Updated CompanyData</param>
        /// <param name="connectionID"></param>
        protected override async Task DoAfterUpdateAsync(CompanyData oldModel, CompanyData updatedModel, string connectionID)
        {
            await base.DoAfterUpdateAsync(oldModel, updatedModel, connectionID);

            //END - 12681
            //Need to reindex Orders after Company update.
            try
            {
                await this.taskQueuer.IndexClasstype(this.BID, Convert.ToInt32(ClassType.Order));
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Indexing failed after UPDATE", ex);
            }

        }

        /// <summary>
        /// Creates any new Companies for the Company
        /// </summary>
        /// <param name="updatedModel">Company</param>
        /// <returns></returns>
        private async Task UpdateContactCompanyLink(CompanyData updatedModel)
        {
            var oldLinks = await ctx.CompanyContactLink.AsNoTracking()
                .Where(x => x.BID == BID && x.CompanyID == updatedModel.ID).ToListAsync();

            if (oldLinks.Count() == 0 && (updatedModel.CompanyContactLinks == null || updatedModel.CompanyContactLinks.Count == 0))
                return;

            // if oldlinks has a contactid that updatedlinks does not have, it should be removed
            var companyContactLinksToRemove = new List<CompanyContactLink>();
            foreach (var link in oldLinks)
            {
                if (!updatedModel.CompanyContactLinks.Any(l => l.ContactID == link.ContactID))
                {
                    companyContactLinksToRemove.Add(link);
                }
            }

            // if updatedLinks has a contactid that oldLinks does not have, it should be created
            var companyContactLinksToAdd = new List<CompanyContactLink>();
            var badContactIDLinks = new List<CompanyContactLink>();
            foreach (var link in updatedModel.CompanyContactLinks)
            {
                if (!oldLinks.Any(l => l.ContactID == link.ContactID))
                {
                    var contact = await ctx.ContactData.FirstOrDefaultAsync(x => x.BID == BID && x.ID == link.ContactID);
                    if (contact != null)
                        companyContactLinksToAdd.Add(link);
                    else
                        badContactIDLinks.Add(link);
                }
            }

            foreach (var link in badContactIDLinks)
                updatedModel.CompanyContactLinks.Remove(link);

            if (updatedModel.CompanyContactLinks != null &&
                 updatedModel.CompanyContactLinks.Count != 0 &&
                (!updatedModel.CompanyContactLinks.Any(x => (x.Roles & ContactRole.Billing) == ContactRole.Billing) ||
                !updatedModel.CompanyContactLinks.Any(x => (x.Roles & ContactRole.Primary) == ContactRole.Primary)))
            {
                throw new InvalidOperationException("Cannot remove Billing or Primary Contact for a Company.");
            }

            ctx.CompanyContactLink.RemoveRange(companyContactLinksToRemove);
            ctx.CompanyContactLink.AddRange(companyContactLinksToAdd);
            await ctx.SaveChangesAsync();

            foreach(var link in companyContactLinksToRemove)
            {
                await UpdateOrDeleteAssociatedContact(link);
            }

            foreach (var link in companyContactLinksToAdd)
            {
                await taskQueuer.IndexModel(BID, ClassType.Contact.ID(), link.ContactID);
            }
        }

        /// <summary>
        /// Company Service Override of DoBeforeCloneAsync
        /// </summary>
        /// <param name="clone">Cloned CompanyData</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(CompanyData clone)
        {
            clone.Name = GetNextClonedName(ctx.Set<CompanyData>().AsNoTracking().Where(t => t.BID == BID && t.Name.StartsWith(t.Name)).ToList(), clone.Name, t => t.Name);
            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Actions to perform after Cloning a Company
        /// </summary>
        /// <param name="clonedFromID">Company ID we Cloned from</param>
        /// <param name="clone">Cloned Company</param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(int clonedFromID, CompanyData clone)
        {
            // Clone CompanyContactLinks
            var clonedFromLinks = await ctx.CompanyContactLink.Include(l => l.Contact).AsNoTracking()
                .Where(l => l.BID == BID && l.CompanyID == clonedFromID).ToListAsync();

            if (clonedFromLinks != null && clonedFromLinks.Count > 0)
            {
                List<CompanyContactLink> toClone = new List<CompanyContactLink>();
                foreach (var link in clonedFromLinks)
                {
                    toClone.Add(new CompanyContactLink()
                    {
                        BID = BID,
                        ContactID = link.ContactID,
                        CompanyID = clone.ID,
                    });
                }
                ctx.CompanyContactLink.AddRange(toClone);
                await ctx.SaveChangesAsync();
            }

            // Copy All Files
            DocumentManager client = base.GetDocumentManager(clonedFromID, (ClassType)clone.ClassTypeID, BucketRequest.All);
            await client.CloneAllDocumentsToClassTypeAsync(clone.ID, clone.ClassTypeID, includeSpecialFolderBlobName: true);

            // Clone Custom Fields
            await CloneCustomFields(clonedFromID, clone.ID);

            // Clone Locators
            await CloneLocators(clonedFromID, clone.ID);

            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        private async Task<bool> CloneCustomFields(int oldID, int newID)
        {
            try
            {
                CustomFieldService customFieldService = new CustomFieldService(this.ctx, this.logger, this.rtmClient);
                var customFieldList = customFieldService.ReadCustomFields(this.BID, (int)ClassType.Company, oldID, new CustomFieldValueFilters());
                customFieldService.UpdateCustomFields(this.BID, (int)ClassType.Company, newID, customFieldList.ToArray());
                return true;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Clone of Company Custom Fields failed!", e);
                return false;
            }
        }

        private async Task<bool> CloneLocators(int oldID, int newID)
        {
            try
            {
                List<CompanyLocator> clData = await this.ctx.CompanyLocator.Where(t => t.BID == BID && t.ParentID == oldID).ToListAsync();
                if (clData != null)
                {
                    CompanyLocatorService service = new CompanyLocatorService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
                    foreach (CompanyLocator locator in clData)
                    {
                        CompanyLocator clone = (await service.GetClone(locator.ID));
                        await service.DoBeforeCloneAsync(clone);
                        clone.ParentID = newID;
                        clone.ModifiedDT = DateTime.MinValue;
                        clone.ClassTypeID = 0;
                        await service.CreateAsync(clone, null);
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Clone of Company Locators failed!", e);
                return false;
            }
        }

        /// <summary>
        /// Get Child Service Associations for CompanyData - Includes CompanyLocators
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<CompanyData, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<CompanyData, int>[]{
                CreateChildAssociation<CompanyLocatorService, CompanyLocator, int>((a) => a.CompanyLocators),

                CreateChildAssociation<PaymentTermService, PaymentTerm, int>((a) => a.DefaultPaymentTerms == null ? null : new List<PaymentTerm>() { a.DefaultPaymentTerms }),
            };
        }

        /// <summary>
        /// Sets a group of Companies to Active or Inactive
        /// </summary>
        /// <param name="ids">Company Ids</param>
        /// <param name="active">If the Companies should be set to Active</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public async Task<EntitiesActionChangeResponse> SetMultipleActive(int[] ids, bool active, string connectionID)
        {
            try
            {
                foreach (var id in ids)
                {
                    var resp = await base.SetActive(id, active, connectionID);
                    if (resp.HasError)
                        throw new Exception($"Unable to set Company {id}'s active status to {active}. Multiple set active aborted.");
                }
                return new EntitiesActionChangeResponse
                {
                    Ids = ids,
                    Message = "Successfully updated Company(s) active status.",
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new EntitiesActionChangeResponse
                {
                    Ids = ids,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        internal async Task<EntityActionChangeResponse> SetProspect(int id, bool booleanState)
        {
            CompanyData model = await this.GetAsync(id);
            SetStatusFlag(booleanState, model, 2);
            return await _SetStatusViaSPROC(id, "SetStatus", "@status", model.StatusID, "Successfully set Company's prospect status");
        }

        private static void SetStatusFlag(bool booleanState, CompanyData model, byte flag)
        {
            if (booleanState)
            {
                model.StatusID |= flag; //set flag
            }
            else
            {
                model.StatusID &= flag; //clear flag
            }
        }

        internal async Task<EntityActionChangeResponse> SetClient(int id, bool booleanState)
        {
            CompanyData model = await this.GetAsync(id);
            SetStatusFlag(booleanState, model, 2);
            return await _SetStatusViaSPROC(id, "SetStatus", "@status", model.StatusID, "Successfully set Company's client status");
        }

        internal async Task<EntityActionChangeResponse> SetLead(int id, bool booleanState)
        {
            CompanyData model = await this.GetAsync(id);
            SetStatusFlag(booleanState, model, 2);
            return await _SetStatusViaSPROC(id, "SetStatus", "@status", model.StatusID, "Successfully set Company's lead status");
        }

        /// <summary>
        /// Executes a SPROC that takes a BID, ID, and single byte value
        /// the SPROC name must be [Company.Action.<paramref name="sprocActionName"/>]
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sprocActionName"></param>
        /// <param name="sprocValueParamName">The name of the parameter that boolean is</param>
        /// <param name="status"></param>
        /// <param name="successMessage"></param>
        /// <returns></returns>
        private async Task<EntityActionChangeResponse> _SetStatusViaSPROC(int id, string sprocActionName, string sprocValueParamName, byte status, string successMessage)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                if (!sprocValueParamName.StartsWith("@"))
                    sprocValueParamName = '@' + sprocValueParamName;

                object[] myParams = {
                    new SqlParameter("@bid", BID),
                    new SqlParameter("@CompanyId", id),
                    new SqlParameter(sprocValueParamName, status),
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync(String.Format("EXEC @Result = dbo.[Company.Action.{0}] @bid, @CompanyId, {1};", sprocActionName, sprocValueParamName), parameters: myParams);
                if (rowResult > 0)
                {
                    await QueueIndexForModel(id);

                    return new EntityActionChangeResponse()
                    {
                        Id = id,
                        Message = successMessage,
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = id,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = id,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }


        /// <summary>
        /// Returns a collection of a CompanyData's CompanyLocators
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IEnumerable<BaseLocator<int, CompanyData>> GetLocators(CompanyData model) => model.CompanyLocators;

        /// <summary>
        /// Assigns a collection of locators to a CompanyData Model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="value"></param>
        public void SetLocators(CompanyData model, IEnumerable<BaseLocator<int, CompanyData>> value) => model.CompanyLocators = value.Select(x => (CompanyLocator)x).ToList();

        internal override void DoBeforeValidate(CompanyData newModel)
        {
            if (newModel.StatusID == 0)
            {
                newModel.StatusID = 1;//lead
            }
            if (String.IsNullOrEmpty(newModel.StatusText))
            {
                newModel.StatusText = "default";//db computed
            }
            if (newModel.LocationID == 0)
            {
                var business = this.ctx.BusinessData
                    .Include(b => b.Locations)
                    .Single(b => b.BID == newModel.BID);
                var defaultLocation = business.Locations.Where(l => l.IsDefault).FirstOrDefault();
                if (defaultLocation != null)
                {
                    newModel.LocationID = defaultLocation.ID;
                }
            }
            this.StripOutTemporaryLocators(newModel);
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<CompanyData> WherePrimary(IQueryable<CompanyData> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Actions to perform after adding a new Company
        /// </summary>
        /// <param name="model">New Company</param>
        /// <param name="tempGuid">Temporary identifier</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(CompanyData model, Guid? tempGuid)
        {
            await base.InitializeCustomerSharedFolderAsync(model);
            await base.DoAfterAddAsync(model, tempGuid);
        }

        /// <summary>
        /// Force Delete
        /// </summary>
        /// <param name="CompanyID">ID of the Company to Delete</param>
        /// <param name="userLinkID">UserLink.ID</param>
        public async Task<EntityActionChangeResponse> ForceDeleteCompany(short CompanyID, short userLinkID)
        {
            try
            {
                UserLink userLinkData = await ctx.UserLink.Where(ul => ul.BID == this.BID && ul.ID == userLinkID).FirstOrDefaultAsync();

                if (userLinkData == null)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Company not found"
                    };
                }

                if (userLinkData.UserAccessType < UserAccessType.SupportManager)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Access denied"
                    };
                }

                CompanyData toDelete = await this.GetAsync(CompanyID);

                if (toDelete == null)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Company not found"
                    };
                }

                List<RefreshEntity> refreshes = DoGetRefreshMessagesOnDelete(toDelete);

                refreshes.AddRange(await ctx.CompanyCustomData.RemoveRangeWhereAtomAsync(this.BID, x => x.ID == CompanyID));
                refreshes.AddRange(await ctx.CompanyLocator.RemoveRangeWhereAtomAsync(this.BID, x => x.ParentID == CompanyID));
                var companyContactLinks = await ctx.CompanyContactLink.AsNoTracking()
                    .Where(x => x.BID == BID && x.CompanyID == CompanyID).ToListAsync();
                ctx.CompanyContactLink.RemoveRange(ctx.CompanyContactLink.Where(x => x.BID == BID && x.CompanyID == CompanyID));

                ctx.CompanyTagLink.RemoveRange(await ctx.CompanyTagLink.Where(x => x.BID == BID && x.CompanyID == CompanyID).ToArrayAsync());

                ctx.CompanyData.Remove(toDelete);

                var save = ctx.SaveChanges();

                // check if contact(s) needs to go away as well
                foreach (var link in companyContactLinks)
                    await UpdateOrDeleteAssociatedContact(link);

                if (save > 0)
                {
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshes });
                    await taskQueuer.IndexModel(refreshes);
                    return new EntityActionChangeResponse()
                    {
                        Success = true,
                        Message = "Successfully Deleted"
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Nothing to delete"
                    };
                }
            }
            catch (DbUpdateException exc)
            {
                return new EntityActionChangeResponse()
                {
                    Success = false,
                    ErrorMessage = exc.InnerException.Message
                };
            }
            catch (Exception exc)
            {
                return new EntityActionChangeResponse()
                {
                    Success = false,
                    ErrorMessage = exc.ToString()
                };
            }
        }

        public override async Task DoBeforeDeleteAsync(CompanyData model)
        {
            var companyContactLinks = await ctx.CompanyContactLink.AsNoTracking()
                .Where(x => x.BID == BID && x.CompanyID == model.ID).ToListAsync();
            ctx.CompanyContactLink.RemoveRange(ctx.CompanyContactLink.Where(x => x.BID == BID && x.CompanyID == model.ID));

            // check if contact(s) needs to go away as well
            foreach (var link in companyContactLinks)
                await UpdateOrDeleteAssociatedContact(link);

            await base.DoBeforeDeleteAsync(model);
        }

        /// <summary>
        /// Check if Location is valid
        /// </summary>
        /// <param name="locationID">Location ID</param>
        /// <returns></returns>
        public bool IsValidLocation(int locationID)
        {
            var locationData = ctx.LocationData.Where(t => t.ID == locationID).FirstOrDefault();
            return locationData != null;
        }

        /// <summary>
        /// Gets the Old Model Async
        /// </summary>
        /// <param name="newModel">New Model</param>
        /// <returns></returns>
        public override Task<CompanyData> GetOldModelAsync(CompanyData newModel)
        {
            return base.GetOldModelAsync(newModel);
        }

        /// <summary>
        /// Gets a list of Companies associated with a Contact by its ID
        /// </summary>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        public async Task<List<CompanyContactLink>> GetCompanyContactLinks(int contactID)
        {
            var contact = await ctx.ContactData.AsNoTracking().FirstOrDefaultAsync(c => c.BID == BID && c.ID == contactID);
            if (contact == null)
                return null;

            return await ctx.CompanyContactLink.Include("Company").AsNoTracking()
                .Where(l => l.BID == BID && l.ContactID == contactID).ToListAsync();
        }

        /// <summary>
        /// Updates the Contact associated in the Link
        /// </summary>
        /// <param name="link">Company Contact Link</param>
        /// <returns>True if the Contact is deleted; false if not</returns>
        private async Task UpdateOrDeleteAssociatedContact(CompanyContactLink link)
        {
            // check that the contact has no other associated companies
            var contactsLinks = ctx.CompanyContactLink.AsNoTracking()
                .Where(x => x.BID == BID && x.ContactID == link.ContactID && x.CompanyID != link.CompanyID);
            if (contactsLinks == null || contactsLinks.Count() == 0)
            {
                var contact = await ctx.ContactData.FirstOrDefaultAsync(x => x.BID == BID && x.ID == link.ContactID);
                ContactService contactService = new ContactService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
                await contactService.DeleteAsync(contact);
            }
            else
            {
                await taskQueuer.IndexModel(BID, ClassType.Contact.ID(), link.ContactID);
            }
        }

    }
}
