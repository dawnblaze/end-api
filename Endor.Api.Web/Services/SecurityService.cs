﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.RTM;
using Endor.Api.Web.Classes;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Threading;
using Microsoft.Data.SqlClient;
using System.Text;
using Endor.DocumentStorage.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Endor.RTM.Models;
using Endor.RTM.Enums;
using System.Data;
using Endor.Api.Web.Classes.Enums;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Business Service
    /// </summary>
    public class SecurityService
    {
        private readonly ApiContext _ctx;

        /// <summary>
        /// Constructs a Security service
        /// </summary>
        /// <param name="context">Api context</param> 
        public SecurityService(ApiContext context)
        {
            _ctx = context;
        }

        /// <summary>
        /// Return the user rights based on userLinkID
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="bid"></param>
        /// <param name="userLinkID"></param>
        /// <returns></returns>
        public async Task<ServiceResponse> Rights(SecurityRightFilter filter, short? bid, int userLinkID)
        {
            try
            {
                if (!bid.HasValue)
                    return new ServiceResponse
                    {
                        Success = false,
                        Message = $"The business id is required.",
                    };

                var parameters = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@BID", SqlDbType = SqlDbType.SmallInt, Value = bid.Value },
                    new SqlParameter() { ParameterName = "@UserLinkID", SqlDbType = SqlDbType.Int, Value = userLinkID },
                };

                filter.Format ??= EnumSecurityRightFormat.JSON;

                if (filter.Format == EnumSecurityRightFormat.JSON)
                {
                    var rights = await _ctx.Database.GetModelFromQueryAsync<SecurityRightsProceduresResults.RightsForArray>(@"SELECT RightID FROM [User.Security.Rights] (@BID, @UserLinkID);", parameters: parameters.ToArray());

                    return new ServiceResponse
                    {
                        Value = new SecurityRightsResponses.JSONRights
                        {
                            BID = bid.Value,
                            UserLinkID = userLinkID,
                            Rights = rights.Any() ? rights.Select(a => a.RightID).ToArray() : new int[0]
                        },
                        Success = true
                    };
                }

                if (filter.Format == EnumSecurityRightFormat.Base64)
                {
                    var rightsBase64 = await _ctx.Database.GetModelFromQueryAsync<SecurityRightsProceduresResults.RightsForString>(@"SELECT dbo.[User.Security.Rights.String] (@BID, @UserLinkID) as 'RightString';", parameters: parameters.ToArray());

                    return new ServiceResponse
                    {
                        Value = Convert.ToBase64String(rightsBase64.FirstOrDefault()?.RightString ?? new byte[0]),
                        Success = true
                    };
                }

                return new ServiceResponse
                {
                    Success = false,
                    Message = "Invalid request"
                };
            }
            catch (Exception ex) 
            {
                return new ServiceResponse
                {
                    Success = false,
                    Message = ex.Message
                };
            }            
        }

        /// <summary>
        /// Return the user rights with source based on userLinkID
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="userLinkID"></param>
        /// <returns></returns>
        public async Task<ServiceResponse> RightsWithSource(short? bid, int userLinkID)
        {
            if (!bid.HasValue)
                return new ServiceResponse
                {
                    Success = false,
                    Message = $"The business id is required.",
                };

            var parameters = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@BID", SqlDbType = SqlDbType.SmallInt, Value = bid.Value },
                    new SqlParameter() { ParameterName = "@UserLinkID", SqlDbType = SqlDbType.Int, Value = userLinkID },
                };

            var rights = await _ctx.Database.GetModelFromQueryAsync<SecurityRightsProceduresResults.RightsForSource>(
                @"SELECT * FROM [dbo].[User.Security.RightsBySource] (@BID, @UserLinkID);", parameters: parameters.ToArray());

            return new ServiceResponse
            {
                Value = rights.Any() ? rights.ToArray() : new SecurityRightsProceduresResults.RightsForSource[0] { },
                Success = true
            };
        }
    }
}
