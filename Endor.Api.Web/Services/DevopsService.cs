﻿using Endor.EF;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Payment APE Service
    /// </summary>
    public class DevopsService : BaseGenericService
    {
        /// <summary>
        /// Business ID
        /// </summary>
        public readonly short BID;
        /// <summary>
        /// object to get cached tenant data
        /// </summary>
        protected readonly ITenantDataCache cache;

        private readonly ITaskQueuer _taskQueuer;
        private readonly ApiContext _ctx;
        private readonly List<ClassTypeEntity> classTypeEntities = new List<ClassTypeEntity>();
        private readonly IRTMPushClient _rtmClient;
        private readonly IMigrationHelper _migrationHelper;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="ctx"></param>
        public DevopsService(ApiContext context, short bid, ITenantDataCache cache, IMigrationHelper migrationHelper, ITaskQueuer taskQueuer, ApiContext ctx, IRTMPushClient rtmClient = null) : base(context, migrationHelper)
        {
            this.BID = bid;
            this.cache = cache;
            this._ctx = ctx;
            this._taskQueuer = taskQueuer;
            this._rtmClient = rtmClient;
            this._migrationHelper = migrationHelper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctid"></param>
        /// <returns></returns>
        public async Task<bool> ReindexAllBID(int ctid)
        {
            try
            {
                List<BusinessData> Businesses = _ctx.BusinessData.ToList();

                if (ctid == 0)
                {
                    List<ClassTypeEntity> entities = await GetIndexableEntityTypes();
                    foreach (BusinessData bd in _ctx.BusinessData)
                    {
                        foreach (ClassTypeEntity entity in entities)
                        {
                            await this._taskQueuer.IndexClasstype(bd.ID, entity.Id);
                        }
                    }
                }
                else
                {
                    foreach (BusinessData bd in _ctx.BusinessData)
                    {
                        await this._taskQueuer.IndexClasstype(bd.ID, ctid);
                    }
                        
                }

            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctid"></param>
        /// <returns></returns>
        public async Task<bool> Reindex(int ctid)
        {
            try
            {
                if (ctid==0)
                {
                    List<ClassTypeEntity> entities = await GetIndexableEntityTypes();
                    foreach (ClassTypeEntity entity in entities)
                    {
                        await this._taskQueuer.IndexClasstype(this.BID, entity.Id);
                    }
                }
                else
                {
                    await this._taskQueuer.IndexClasstype(this.BID, ctid);
                }
                
            }
            catch
            {
                return false;
            }
            return true;
        }

        private static int GetClassTypeIDFromClassTypeName(string EnumClassTypeName)
        {
            try
            {
                EnumClassTypeName = EnumClassTypeName.Replace("SearchModel","");
                int hasDataInName = EnumClassTypeName.ToLower().LastIndexOf("data");
                if (hasDataInName > 0)
                {
                    EnumClassTypeName = EnumClassTypeName.Substring(0, hasDataInName);
                }

                return (int)Enum.Parse(typeof(ClassType), EnumClassTypeName);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private async Task<List<string>> GetIndexableEntities()
        {
            List<string> entities = new List<string>();
            try
            {
                string SEARCH_URL = $"{await GetSearchURLAsync(this.BID)}api/search/entities";

                using (var client = new HttpClient())
                {
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, SEARCH_URL);
                    var response = await client.SendAsync(request);
                    string responseString = await response.Content.ReadAsStringAsync();

                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            entities = JsonConvert.DeserializeObject<List<string>>(responseString);
                            break;
                        case HttpStatusCode.InternalServerError:
                            throw new InvalidOperationException("SEARCH error!");
                    }
                }

            }
            catch
            {
                return null;
            }
            return entities;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<ClassTypeEntity>> GetIndexableEntityTypes()
        {
            List<string> entities = await GetIndexableEntities();
            foreach (string entity in entities)
            {
                int classTypeID = GetClassTypeIDFromClassTypeName(entity);
                if (classTypeID != 0)
                {
                    classTypeEntities.Add(new ClassTypeEntity
                    {
                        Name = entity,
                        Id = classTypeID
                    });
                }
            }
            return classTypeEntities;
        }

        private async Task<string> GetSearchURLAsync(short bid)
        {
            return (await cache.Get(bid)).SearchURL;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool RunCopyAllDefaults()
        {
            try
            {
                this._migrationHelper.RunCopyAllDefaults(_ctx);
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool ResetCopyAllDefaults()
        {
            try
            {
                this._migrationHelper.ResetCopyAllDefaults(_ctx);
            }
            catch
            {
                return false;
            }
            return true;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ClassTypeEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
    }
}
