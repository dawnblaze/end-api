﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// location service
    /// </summary>
    public class LocationService : AtomCRUDService<LocationData, byte>, ISimpleListableViewService<SimpleLocationData, byte>, ILocatorParentService<LocationData, byte>
    {
        private int? _userLinkId = null;
        private const string OptionKeyCopySettings = "Location.Clone.CopySettings";
        private const string OptionKeyCopyDocuments = "Location.Clone.CopyDocuments";

        /// <summary>
        /// gets simple list set
        /// </summary>
        public DbSet<SimpleLocationData> SimpleListSet => ctx.SimpleLocationData;

        /// <summary>
        /// location service ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public LocationService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// gets a list of default include strings
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "LocationLocators" };

        /// <summary>
        /// gets include strings
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// do before create hook
        /// makes sure that tax group location link exists
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(LocationData newModel, Guid? tempGuid = null)
        {
            //this gets our LocationID
            await base.DoBeforeCreateAsync(newModel, tempGuid);
            //then once we have our ID we can make a link to it
            await EnsureTaxGroupLocationLinkExists(newModel);
        }

        /// <summary>
        /// before update hook
        /// makes sure that tax group location link exists
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(LocationData oldModel, LocationData newModel)
        {
            await EnsureTaxGroupLocationLinkExists(newModel);
            var userAccessType = UserAccessType.None;
            if (_userLinkId != null)
            {
                userAccessType = ctx.UserLink.FirstOrDefault(ul => ul.ID == this._userLinkId).UserAccessType;
            }
            if (userAccessType < UserAccessType.SupportStaff)
            {
                //check that Location number is not changing
                if ((oldModel.LocationNumber != null) && (oldModel.LocationNumber != newModel.LocationNumber))
                {
                    throw new Exception("This user does not have permission to change the Location Number field");
                }
            }
            await base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        private async Task EnsureTaxGroupLocationLinkExists(LocationData newModel)
        {
            if (newModel.DefaultTaxGroupID.HasValue)
            {
                TaxGroupLocationLink existingLink = await this.ctx.TaxGroupLocationLink
                                                                .FirstOrDefaultAsync(x =>
                                                                    x.BID == this.BID &&
                                                                    x.LocationID == newModel.ID &&
                                                                    x.GroupID == newModel.DefaultTaxGroupID
                                                                );
                if (existingLink == null)
                {
                    ctx.TaxGroupLocationLink.Add(new TaxGroupLocationLink()
                    {
                        BID = this.BID,
                        GroupID = newModel.DefaultTaxGroupID.Value,
                        LocationID = newModel.ID
                    });
                }
            }
        }

        /// <summary>
        /// before clone hook
        /// makes sure name has cloned appended
        /// and doesn't conflict with an existing record
        /// </summary>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(LocationData clone)
        {
            var existing = await this.ctx.LocationData.Where(l => l.BID == this.BID && l.Name.ToLower() == clone.Name.ToLower()).FirstOrDefaultAsync();
            if (existing != null)
            {
                var originalName = clone.Name;
                const string cloneIdentifier = " (Clone)";
                string currentCloneIdentifier = cloneIdentifier;

                //Ensure record doesn't already exist
                while (await this.ctx.LocationData.WherePrimary(this.BID, x => x.Name.ToLowerInvariant() == $"{originalName}{currentCloneIdentifier}".ToLowerInvariant()).FirstOrDefaultAsync() != null)
                {
                    //Record already exists, append clone identifier until it doesn't
                    currentCloneIdentifier += cloneIdentifier;
                }

                //Modify Values
                clone.Name += currentCloneIdentifier;
                clone.LegalName += currentCloneIdentifier;
            }

            var newName = GetNextClonedName(ctx.Set<LocationData>().AsNoTracking().Where(t => t.BID == BID && t.Name.StartsWith(clone.Name)).ToList(), clone.Name, t => t.Name);
         
            //Don't carry IsDefault value from clone source
            clone.IsDefault = false;

            clone.Abbreviation = "L" + (clone.ID % 99);
            clone.Name = newName;
            clone.LegalName = newName;

            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Potentially Copy Settings and Documents after Location Clone
        /// </summary>
        /// <param name="clonedFromID"></param>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(byte clonedFromID, LocationData clone)
        {

            OptionValue copySettings = (await base.GetUserOptionValueByName(OptionKeyCopySettings, this._userLinkId)).Value;
            OptionValue copyDocuments = (await base.GetUserOptionValueByName(OptionKeyCopyDocuments, this._userLinkId)).Value;

            if (copySettings == null || bool.TryParse(copySettings.Value, out bool copySettingsValue) && copySettingsValue)
            {//Copy if setting is null
                await CopyOptionValues(clonedFromID, clone.ID);
            }

            if (copyDocuments != null && bool.TryParse(copyDocuments.Value, out bool copyDocumentsValue) && copyDocumentsValue)
            {//Don't copy if setting is null
                DocumentManager client = base.GetDocumentManager(clonedFromID, ClassType.Location, BucketRequest.Documents);
                await client.CloneAllDocumentsAsync(clone.ID);
            }

            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        /// <summary>
        /// Copy all Options from one Location to another Location
        /// </summary>
        /// <param name="clonedFromLocationID"></param>
        /// <param name="newLocationID"></param>
        /// <returns></returns>
        private async Task<PutOptionValuesResponse> CopyOptionValues(byte clonedFromLocationID, byte newLocationID)
        {
            GetOptionValuesResponse values = await _optionService.Get(BID, clonedFromLocationID, null, null, null, null, 5 /*Operations*/);

            var optionValues = values.Values
                .Select(o => new Options
                {
                    OptionID = o.OptionID,
                    OptionName = o.Name,
                    Value = o.Value
                }).ToList();

            PutOptionValuesResponse response = await _optionService.Put(optionValues, null, this.BID, newLocationID, null, null, null, null);

            return response;
        }

        /// <summary>
        /// Clones a Location from a given ID
        /// </summary>
        /// <param name="ID">ID of the Location to clone</param>
        /// <param name="newName">New Location Name</param>
        /// <param name="userLinkId"></param>
        /// <returns></returns>
        public async Task<LocationData> CloneAsync(byte ID, string newName, int? userLinkId)
        {
            try
            {
                this._userLinkId = userLinkId;
                var clone = await this.GetClone(ID);

                clone.Name = newName;
                clone.LegalName = newName;

                await DoBeforeCloneAsync(clone);

                var cloneAfterSave = await this.CreateAsync(clone, null);

                await DoAfterCloneAsync(ID, cloneAfterSave);

                return cloneAfterSave;
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Clone failed", ex);
                return null;
            }
        }

        /// <summary>
        /// after add hook
        /// persists uploaded data from temp to new ID
        /// but doesn't use common code
        /// </summary>
        /// <param name="model"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(LocationData model, Guid? tempGuid)
        {
            if (tempGuid.HasValue)
            {
                IStorageContext dataStorage = new StorageContext(this.BID, BucketRequest.Data, new DocumentStorage.Models.DMID() { guid = tempGuid });
                IStorageContext documentStorage = new StorageContext(this.BID, BucketRequest.Documents, new DocumentStorage.Models.DMID() { guid = tempGuid });
                IStorageContext reportStorage = new StorageContext(this.BID, BucketRequest.Reports, new DocumentStorage.Models.DMID() { guid = tempGuid });
                DocumentManager dataDM = new DocumentManager(cache, dataStorage);
                await dataDM.PersistDocumentsAsync(tempGuid.Value, model.ID, model.ClassTypeID);

                DocumentManager documentDataDM = new DocumentManager(cache, documentStorage);
                await documentDataDM.PersistDocumentsAsync(tempGuid.Value, model.ID, model.ClassTypeID);

                DocumentManager reportDM = new DocumentManager(cache, reportStorage);
                await reportDM.PersistDocumentsAsync(tempGuid.Value, model.ID, model.ClassTypeID);

                IStorageContext permanentDataStorage = new StorageContext(this.BID, BucketRequest.Data, new DocumentStorage.Models.DMID() { id = model.ID, ctid = model.ClassTypeID });
                DocumentManager permanentDataDM = new DocumentManager(cache, permanentDataStorage);
                bool defaultImageSaved = await permanentDataDM.HasDefaultImage();

                if (defaultImageSaved)
                {
                    model.HasImage = true;
                    await this.ctx.SaveChangesAsync();
                }
            }

            await base.DoAfterAddAsync(model, tempGuid);
        }

        /// <summary>
        /// gets child associations list
        /// which is only locators
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<LocationData, byte>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<LocationData, byte>[]{
                CreateChildAssociation<LocationLocatorService, LocationLocator, int>((a) => a.LocationLocators),
            };
        }

        /// <summary>
        /// Sets a Business Location as the Default location
        /// </summary>
        /// <param name="bid">Business Id</param>
        /// <param name="id">Location Id</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> SetDefault(short bid, int id)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                object[] myParams = {
                    new SqlParameter("@bid", bid),
                    new SqlParameter("@locationId", id),
                    resultParam
                };



                List<byte> idsToReindex = new List<byte>();

                //For END-11224
                //Check and fetch other LocationData entities that were previously set to IsDefault = true and store in a collection.
                if (ctx.LocationData.Where(l => l.IsDefault == true).Any())
                {
                     var res = ctx.LocationData.Where(l => l.IsDefault == true).Select(ld => ld.ID);
    
                    foreach(var i in res)
                    {
                        idsToReindex.Add(i);

                    }
                }

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Location.Action.SetDefault] @bid, @locationId;", parameters: myParams);
                if (rowResult > 0)
                {
                    //Add the id to our collection for reindexing.
                    idsToReindex.Add((byte)id);

                    if(idsToReindex.Count() > 0)
                    {
                        
                        await QueueIndexForModels(idsToReindex.ToArray());

                    }
                    else
                    {
                        await QueueIndexForModel((byte)id);
                    }


                    return new EntityActionChangeResponse
                    {
                        Id = id,
                        Message = "Successfully set Location to Default location.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = id,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = id,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Attempts to change the IsActive Status of the Location
        /// </summary>
        /// <param name="ID">Location's ID</param>
        /// <param name="active">Active status to set to</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public override async Task<EntityActionChangeResponse<byte>> SetActive(byte ID, bool active, string connectionID)
        {
            EntityActionChangeResponse<byte> resp = null;
            var location = await ctx.LocationData.WherePrimary(this.BID, ID).FirstOrDefaultAsync();

            if (location == null)
            {
                resp = new EntityActionChangeResponse<byte>()
                {
                    Id = ID,
                    ErrorMessage = "Location Not Found.",
                    Message = "Location Not Found.",
                    Success = false
                };
            }
            else
            {
                if (active)
                {
                    resp = await base.SetActive(ID, true, connectionID);
                }
                else
                {
                    if (location.IsDefault)
                    {
                        const string respMsg = "Attempting to set the Default Location to Inactive. Set another Location as the Default first before setting this one to Inactive.";
                        resp = new EntityActionChangeResponse<byte>()
                        {
                            Id = ID,
                            ErrorMessage = respMsg,
                            Message = respMsg,
                            Success = false
                        };
                    }
                    else
                    {
                        var otherActive = await ctx.LocationData.Where(l => l.ID != ID && l.BID == this.BID && l.IsActive).FirstOrDefaultAsync();

                        if (otherActive == null)
                        {
                            const string respMsg = "Attempting to set the only Active Location Inactive. Set another Location active first before setting this one inactive.";
                            resp = new EntityActionChangeResponse<byte>()
                            {
                                Id = ID,
                                ErrorMessage = respMsg,
                                Message = respMsg,
                                Success = false
                            };
                        }
                        else
                        {
                            resp = await base.SetActive(ID, false, connectionID);
                        }
                    }
                }
            }

            return resp;
        }

        /// <summary>
        /// Sets a group of Business Locations to Active
        /// </summary>
        /// <param name="ids">Location Ids</param>
        /// <param name="active">If the locations should be set to Active</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public async Task<EntitiesActionChangeResponse> SetMultipleActive(byte[] ids, bool active, string connectionID)
        {
            try
            {
                foreach (var id in ids)
                {
                    var resp = await this.SetActive(id, active, connectionID);

                    if (resp.HasError)
                        throw new Exception($"Unable to set Location {id}'s active status to {active}. Multiple set active aborted.");
                }
                return new EntitiesActionChangeResponse
                {
                    Ids = ids.Select(Convert.ToInt32).ToArray(),
                    Message = "Successfully updated Locations(s) active status.",
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new EntitiesActionChangeResponse
                {
                    Ids = ids.Select(Convert.ToInt32).ToArray(),
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Sets an Location's default Email account
        /// </summary>
        /// <param name="locationID">Location's ID</param>
        /// <param name="emailAccountID">Email Account's ID</param>
        /// <returns></returns>
        public async Task<BooleanResponse> SetDefaultEmailAccount(byte locationID, short emailAccountID)
        {
            var location = await GetAsync(locationID);
            if (location == null)
            {
                return new BooleanResponse()
                {
                    Success = false,
                    ErrorMessage = $"Location with ID `{locationID} not found.",
                    Message = $"Location with ID `{locationID} not found."
                };
            }

            var emailAccount = await ctx.EmailAccountData.FirstOrDefaultAsync(a => a.BID == BID && a.ID == emailAccountID);
            if (emailAccount == null)
            {
                return new BooleanResponse()
                {
                    Success = false,
                    ErrorMessage = $"EmailAccount with ID `{emailAccountID} not found.",
                    Message = $"EmailAccount with ID `{emailAccountID} not found."
                };
            }

            if (!emailAccount.IsActive)
            {
                return new BooleanResponse()
                {
                    Success = false,
                    ErrorMessage = $"Forbidden: EmailAccount with ID `{emailAccountID} is not active.",
                    Message = $"Forbidden: EmailAccount with ID `{emailAccountID} is not active."
                };
            }
            else
            {
                location.DefaultEmailAccountID = emailAccountID;
                ctx.LocationData.Update(location);
                await ctx.SaveChangesAsync();

                return new BooleanResponse()
                {
                    Success = true,
                    Message = $"Successfully set Default Email Account."
                };
            }
        }

        /// <summary>
        /// Clears an Location's default Email account
        /// </summary>
        /// <param name="locationID">Location's ID</param>
        /// <returns></returns>
        public async Task<BooleanResponse> ClearDefaultEmailAccount(byte locationID)
        {
            var location = await GetAsync(locationID);
            if (location == null)
            {
                return new BooleanResponse()
                {
                    Success = false,
                    ErrorMessage = $"Location with ID `{locationID} not found.",
                    Message = $"Location with ID `{locationID} not found."
                };
            }

            location.DefaultEmailAccountID = null;
            ctx.LocationData.Update(location);
            await ctx.SaveChangesAsync();

            return new BooleanResponse()
            {
                Success = true,
                Message = $"Successfully cleared Default Email Account."
            };
        }

        /// <summary>
        /// gets Locators
        /// for ILocatorParentService
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IEnumerable<BaseLocator<byte, LocationData>> GetLocators(LocationData model) => model.LocationLocators;
        /// <summary>
        /// sets locators
        /// for ILocatorParentService
        /// </summary>
        /// <param name="model"></param>
        /// <param name="value"></param>
        public void SetLocators(LocationData model, IEnumerable<BaseLocator<byte, LocationData>> value) {
            var locators = value.ToList();
            ICollection<LocationLocator> loc = new List<LocationLocator>();
            foreach (LocationLocator locator in locators)
            {
                loc.Add(locator);
            }
            model.LocationLocators = loc;
        }

        internal override void DoBeforeValidate(LocationData newModel)
        {
            this.StripOutTemporaryLocators(newModel);
        }

        /// <summary>
        /// Returns a list of `LocationData` that shares the same name as the `model`
        /// </summary>
        /// <param name="model">`LocationData` model to check Name from</param>
        public List<LocationData> ExistingNameOwners(LocationData model)
        {
            string cmpValue = String.IsNullOrWhiteSpace(model.Name) ? "" : model.Name.Trim();
            return this.ctx.LocationData
                .Where(location =>
                        location.BID == this.BID
                        && location.Name.Trim().Equals(cmpValue)
                        && location.ID != model.ID)
                .ToList();
        }

        /// <summary>
        /// gets a model given the primary ID of type byte
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<LocationData> WherePrimary(IQueryable<LocationData> query, byte ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Updates a record
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="connectionID"></param>
        /// <param name="userLink">User Link ID</param>
        /// <returns></returns>
        public async Task<LocationData> UpdateAsync(LocationData newModel, string connectionID, int? userLink)
        {
            try
            {
                this._userLinkId = userLink;

                LocationData oldModel = await GetOldModelAsync(newModel);

                //Nothing to update
                if (oldModel == null) return null;

                await DoBeforeUpdateAsync(oldModel, newModel);

                await ctx.SaveChangesAsync();

                await DoAfterUpdateAsync(oldModel, newModel, connectionID);
                return newModel;
            }
            catch (Exception e)
            {
                return await HandleUpdateException(e);
            }
        }

        /// <summary>
        /// Updates a record
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        [Obsolete]
        public override async Task<LocationData> UpdateAsync(LocationData newModel, string connectionID)
        {
            return await UpdateAsync(newModel, connectionID, this._userLinkId);
            //await Task.CompletedTask;
            //throw new Exception("This function should never be called");
        }

        /// <summary>
        /// Force Delete
        /// </summary>
        /// <param name="LocationID">ID of the Location to Delete</param>
        /// <param name="userLinkID">UserLink.ID</param>
        public async Task<EntityActionChangeResponse> ForceDelete(byte LocationID, short userLinkID)
        {
            try
            {
                UserLink userLinkData = await ctx.UserLink.Where(ul => ul.BID == this.BID && ul.ID == userLinkID).FirstOrDefaultAsync();

                if (userLinkData == null)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "User not found"
                    };
                }

                if (userLinkData.UserAccessType < UserAccessType.SupportManager)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Access denied."
                    };
                }

                LocationData toDelete = await this.GetAsync(LocationID);

                if (toDelete == null)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Location not found"
                    };
                }

                List<RefreshEntity> refreshes = DoGetRefreshMessagesOnDelete(toDelete);

                refreshes.AddRange(await ctx.DomainData.RemoveRangeWhereAtomAsync(this.BID, x => x.LocationID == LocationID));

                ctx.TaxGroupLocationLink.RemoveRange(await ctx.TaxGroupLocationLink.Where(x => x.BID == BID && x.LocationID == LocationID).ToArrayAsync());
                ctx.DomainEmailLocationLink.RemoveRange(await ctx.DomainEmailLocationLink.Where(x => x.BID == BID && x.LocationID == LocationID).ToArrayAsync());
                ctx.EmployeeTeamLocationLink.RemoveRange(await ctx.EmployeeTeamLocationLink.Where(x => x.BID == BID && x.LocationID == LocationID).ToArrayAsync());

                refreshes.AddRange(await ctx.LocationGoal.RemoveRangeWhereAtomAsync(this.BID, x => x.LocationID == LocationID));
                refreshes.AddRange(await ctx.LocationLocator.RemoveRangeWhereAtomAsync(this.BID, x => x.ParentID == LocationID));

                ctx.LocationData.Remove(toDelete);

                var save = ctx.SaveChanges();

                if (save > 0)
                {
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshes });
                    await taskQueuer.IndexModel(refreshes);
                    return new EntityActionChangeResponse()
                    {
                        Success = true,
                        Message = "Successfully Deleted"
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Nothing to delete"
                    };
                }
            }
            catch (DbUpdateException exc)
            {
                return new EntityActionChangeResponse()
                {
                    Success = false,
                    ErrorMessage = exc.InnerException.Message
                };
            }
            catch (Exception exc)
            {
                return new EntityActionChangeResponse()
                {
                    Success = false,
                    ErrorMessage = exc.ToString()
                };
            }
        }

        /// <summary>
        /// checks if LocationData.AbbreviationExists
        /// </summary>
        /// <param name="abbreviation"></param>
        /// <param name="locationID"></param>
        /// <returns></returns>
        public Boolean AbbreviationExists(string abbreviation, byte locationID)
        {
            abbreviation = abbreviation.Trim().ToLower();
            var dupCount = this.ctx.LocationData
                        .Where(loc => loc.BID == this.BID && loc.ID != locationID && loc.Abbreviation.Trim().ToLower() == abbreviation)
                        .Count();

            return dupCount > 0;
        }
    }
}
