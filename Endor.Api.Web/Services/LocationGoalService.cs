﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// location service 
    /// </summary>
    public class LocationGoalService : AtomCRUDService<LocationGoal, short>
    {

        /// <summary>
        /// Lazy Loaded OptionCategory Service
        /// </summary>
        protected readonly Lazy<OptionCategoryService> _optionCategoryService;

        private List<byte> months = new List<byte>();

        /// <summary>
        /// location service ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public LocationGoalService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
            this._optionCategoryService = new Lazy<OptionCategoryService>(() => new OptionCategoryService(context, migrationHelper));
        }

        /// <summary>
        /// gets a list of default include strings
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// gets include strings
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// returns child associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<LocationGoal, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<LocationGoal, short>[0];
        }

        internal override void DoBeforeValidate(LocationGoal newModel)
        {
            //noop
        }

        /// <summary>
        /// returns clause for filtering on primary ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns> 
        public override IQueryable<LocationGoal> WherePrimary(IQueryable<LocationGoal> query, short ID)
        {
            return query.WherePrimary(BID, ID);
        }

        /// <summary>
        /// Returns LocationGoal based on filters supplied in
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<GenericResponse<List<LocationGoal>>> GetWithFiltersAsync(LocationGoalFilter filters)
        {
            if (!filters.Year.Any())
            {
                return new GenericResponse<List<LocationGoal>>()
                {
                    Message = "Year must be defined",
                    ErrorMessage = "Year must be defined",
                    Success = false
                };
            }
            if (filters != null)
            {
                Expression<Func<BusinessGoal, bool>>[] predicates = filters.WherePredicates();
                var businessGoalresults = await ctx.Set<BusinessGoal>().IncludeAll(GetIncludes()).Where(m => m.BID == BID).WhereAll(predicates).OrderBy(x => x.FiscalYear).ThenBy(x => x.FiscalMonth).AsNoTracking().ToListAsync();
                var results = new List<LocationGoal>();

                // Set dummy month indexes for empty Fiscal Year
                this.months = await GetFiscalMonthsIndexes();

                //First get all locations that need an entry
                List<byte> locationsToAdd = new List<byte>();
                if (filters.LocationID != null && filters.LocationID.Any())
                {
                    locationsToAdd.AddRange(filters.LocationID);
                }
                else
                {
                    locationsToAdd.AddRange(ctx.Set<LocationData>().Select(l => l.ID));
                }

                foreach (var year in filters.Year)
                {
                    //for every location add the relevant entries
                    foreach (var locationID in locationsToAdd)
                    {
                        //check if we have an entry 
                        var yearlyLocationBusinessGoal = businessGoalresults.Where(bg => bg.FiscalYear == year && bg.LocationID == locationID);
                        if (yearlyLocationBusinessGoal.Any())
                        {
                            foreach (var goal in yearlyLocationBusinessGoal)
                            {
                                results.Add(GetLocationGoalFromBusinessGoal(goal));
                            }
                        }
                        else
                        {
                            results.AddRange(addAllGoalsForYear(locationID, year,results.Count()));
                        }
                    }
                    //check if we need business goals
                    if (filters.IncludeBusiness.GetValueOrDefault())
                    {
                        var yearlyBusinessBusinessGoal = businessGoalresults.Where(bg => bg.FiscalYear == year && bg.LocationID == null);
                        if (yearlyBusinessBusinessGoal.Any())
                        {
                            foreach (var goal in yearlyBusinessBusinessGoal)
                            {
                                results.Add(GetLocationGoalFromBusinessGoal(goal));
                            }
                        }
                        else
                            //add zero business goals if requested
                            results.AddRange(addAllGoalsForYear(null, year, results.Count()));
                    }
                }
                return new GenericResponse<List<LocationGoal>>()
                {
                    Message = "Success",
                    ErrorMessage = "",
                    Success = true,
                    Data = results
                };
            }

            return new GenericResponse<List<LocationGoal>>()
            {
                Message = "Filters not properly defined",
                ErrorMessage = "Filters not properly defined",
                Success = false
            };
        }

        private async Task<SystemOptionDefinitionWithValue[]> GetSystemOptionDefinitionByCategoryID(int categoryId)
        {
            return await this._optionCategoryService.Value.GetSystemOptionDefinitionsWithValueByCategory(categoryId, null, BID, null, null, null, null, null);
        }

        private async Task<SystemOptionDefinitionWithValue> GetSystemOptionByID(int optionID)
        {
            SystemOptionDefinitionWithValue selectedFiscal = null;
            SystemOptionDefinitionWithValue[] option = await GetSystemOptionDefinitionByCategoryID(301);

            if (option != null)
            {
                selectedFiscal = option.FirstOrDefault(item => item.OptionID == optionID);


            }

            return selectedFiscal;
        }

        private async Task<List<byte>> GetFiscalMonthsIndexes()
        {

            SystemOptionDefinitionWithValue fiscalOption = await GetSystemOptionByID(3011);
            List<byte> months = new List<byte>();

            if (fiscalOption != null)
            {
                byte monthIndex = 1;
                byte currentMonthIndex = !string.IsNullOrEmpty(fiscalOption.Value) ? byte.Parse(fiscalOption.Value) : (byte)1;

                while (monthIndex <= 12)
                {
                    if (currentMonthIndex > 12)
                    {
                        currentMonthIndex = 1;
                    }

                    months.Add(currentMonthIndex);

                    currentMonthIndex++;
                    monthIndex++;
                }
            }

            return months;
        }


        private List<LocationGoal> addAllGoalsForYear(byte? locationID, short year, int startIndex)
        {
            var results = new List<LocationGoal>();
            //add an entry for every month
            for (byte month = 1; month <= 12; month++)
            {
                results.Add(GenerateZeroLocationGoal(-(results.Count() + 1) - startIndex, locationID, year, month));
            }
            //add yearly total
            results.Add(GenerateZeroLocationGoal(-(results.Count() + 1) - startIndex, locationID, year, null));
            return results;
        }

        /// <summary>
        /// Generate "O" LocationGoal
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="LocationID"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        private LocationGoal GenerateZeroLocationGoal(int ID, byte? LocationID, short year, byte? month)
        {
            return new LocationGoal
            {
                BID = BID,
                ID = (short)ID,
                ClassTypeID = (int)ClassType.LocationGoal,
                ModifiedDT = new DateTime(),
                LocationID = LocationID,
                Year = year,
                Month = month,
                IsYearlyTotal = (month == null),
                IsBusinessTotal = (LocationID==null),
            };
        }

        /// <summary>
        /// Returns LocationGoal based on month
        /// </summary>
        /// <param name="locationId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public async Task<LocationGoal> GetLocationGoalByMonth(byte locationId, short year, byte month)
        {
            var result = ctx.BusinessGoal.Where(lg => (lg.LocationID.HasValue && lg.CalendarMonth.HasValue)
                            && (lg.LocationID == locationId)
                            && (lg.FiscalYear == year)
                            && (lg.CalendarMonth == month));
            return await Task.FromResult(GetLocationGoalFromBusinessGoal(result.FirstOrDefault()));
        }

        /// <summary>
        /// Updates LocationGoal based on month
        /// </summary>
        /// <param name="locationID">Location ID</param>
        /// <param name="year">Year</param>
        /// <param name="month">Month</param>
        /// <param name="budgeted">Optional.  If supplied, the Budgeted amount is updated to this value. If not supplied, the existing Budgeted number retains its current value.</param>
        /// <param name="actual">Optional.  If supplied, the Actual amount is updated to this value. If not supplied, the existing Actual number retains its current value.</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> UpdateLocationGoalByMonth(byte locationID, short year, byte month, float? budgeted = null, float? actual = null)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                var myParams = new List<SqlParameter>() {
                    new SqlParameter("@BID", BID),
                    new SqlParameter("@LocationID", locationID),
                    new SqlParameter("@Year", year),
                    new SqlParameter("@Month", month)
                };

                if (budgeted.HasValue)
                    myParams.Add(new SqlParameter("@Budgeted", budgeted));
                else
                    myParams.Add(new SqlParameter("@Budgeted", DBNull.Value));

                if (actual.HasValue)
                    myParams.Add(new SqlParameter("@Actual", actual));
                else
                    myParams.Add(new SqlParameter("@Actual", DBNull.Value));

                myParams.Add(resultParam);

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Location.Goal.CreateOrUpdate] @BID, @LocationID, @Year, @Month, @Budgeted, @Actual;", parameters: myParams);
                if (rowResult > 0)
                {
                    return new EntityActionChangeResponse
                    {
                        Message = "Successfully updated location goal values.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        private LocationGoal GetLocationGoalFromBusinessGoal(BusinessGoal goal)
        {
            if (goal == null) return null;
            else return new LocationGoal
            {
                BID = goal.BID,
                ID = goal.ID,
                ClassTypeID = goal.ClassTypeID,
                ModifiedDT = goal.ModifiedDT,
                LocationID = goal.LocationID,
                Year = goal.CalendarYear ?? goal.FiscalYear,
                Month = goal.CalendarMonth,
                Budgeted = goal.Budgeted,
                Actual = goal.Actual,
                PercentOfGoal = goal.PercentOfGoal,
                IsYearlyTotal = goal.IsYearlyTotal,
                IsBusinessTotal = goal.IsBusinessTotal,
            };
        }
    }
}
