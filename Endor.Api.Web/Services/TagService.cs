﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Service that wraps utilizes data storage layer and is accessed primarily from Controller layer
    /// </summary>
    public class TagService : AtomCRUDService<ListTag, short>
    {
        /// <summary>
        /// Constructs a new TagService with a number of injeced parameters
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public TagService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) :
            base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {

        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        internal async Task<List<ListTag>> GetWithFiltersAsync(TagFilter filters)
        {
            //start with filters.wherePredicates in case we need a Name filter
            Expression<Func<ListTag, bool>>[] predicates = new Expression<Func<ListTag, bool>>[] { t => !t.IsDeleted };
            if (filters != null)
                predicates = predicates.Concat( filters?.WherePredicates() ).ToArray();

            if ((filters != null)&&(filters.AssociatedClassTypeID!=null))
            {
                List<short> ids = new List<short>();
                switch (filters.AssociatedClassTypeID)
                {
                    case (int)ClassType.Contact:
                        ids = ctx.ContactTagLink.Where(tl => tl.ContactID == filters.AssociatedID).Select(tl => tl.TagID).ToList();
                        break;
                    case (int)ClassType.Company:
                        ids = ctx.CompanyTagLink.Where(tl => tl.CompanyID == filters.AssociatedID).Select(tl => tl.TagID).ToList();
                        break;
                    case (int)ClassType.OrderDestination:
                        ids = ctx.OrderDestinationTagLink.Where(tl => tl.OrderDestinationID == filters.AssociatedID).Select(tl => tl.TagID).ToList();
                        break;
                    case (int)ClassType.OrderItem:
                        ids = ctx.OrderItemTagLink.Where(tl => tl.OrderItemID == filters.AssociatedID).Select(tl => tl.TagID).ToList();
                        break;
                    case (int)ClassType.Order:
                        ids = ctx.OrderTagLink.Where(tl => tl.OrderID == filters.AssociatedID).Select(tl => tl.TagID).ToList();
                        break;
                    default:
                        ids = ctx.ListTagOtherLink.Where(tl => tl.AppliesToID == filters.AssociatedID && tl.AppliesToClassTypeID == filters.AssociatedClassTypeID).Select(tl => tl.TagID).ToList();
                        break;
                }
                Expression<Func<ListTag, bool>>[] idsClause = new Expression<Func<ListTag, bool>>[] { t => ids.Contains(t.ID) };

                predicates = predicates.Concat(idsClause).ToArray();
            }

            return await this.GetWhere(predicates);

        }

        /// <summary>
        /// Links a Tag with a record
        /// </summary>
        /// <param name="tagId">Tag ID</param>
        /// <param name="classTypeId">ClassType ID that you're linking to</param>
        /// <param name="objectId">ID of the object you're linking to</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkTag(short tagId, int classTypeId, int objectId)
        {
            switch (classTypeId)
            {
                case (int)ClassType.Contact:
                    //check if tag exists
                    if (!ctx.ContactTagLink.Where(tl => tl.TagID == tagId && tl.ContactID == objectId).Any())
                    {
                        //if not add the tag
                        ContactTagLink tl = new ContactTagLink
                        {
                            BID = this.BID,
                            TagID = tagId,
                            ContactID = objectId
                        };
                        ctx.ContactTagLink.Add(tl);
                    }
                    break;
                case (int)ClassType.Company:
                    //check if tag exists
                    if (!ctx.CompanyTagLink.Where(tl => tl.TagID == tagId && tl.CompanyID == objectId).Any())
                    {
                        //if not add the tag
                        CompanyTagLink tl = new CompanyTagLink
                        {
                            BID = this.BID,
                            TagID = tagId,
                            CompanyID = objectId
                        };
                        ctx.CompanyTagLink.Add(tl);
                    }
                    break;
                case (int)ClassType.OrderDestination:
                    //check if tag exists
                    if (!ctx.OrderDestinationTagLink.Where(tl => tl.TagID == tagId && tl.OrderDestinationID == objectId).Any())
                    {
                        //if not add the tag
                        OrderDestinationTagLink tl = new OrderDestinationTagLink
                        {
                            BID = this.BID,
                            TagID = tagId,
                            OrderDestinationID = objectId
                        };
                        ctx.OrderDestinationTagLink.Add(tl);
                    }
                    break;
                case (int)ClassType.OrderItem:
                    //check if tag exists
                    if (!ctx.OrderItemTagLink.Where(tl => tl.TagID == tagId && tl.OrderItemID == objectId).Any())
                    {
                        //if not add the tag
                        OrderItemTagLink tl = new OrderItemTagLink
                        {
                            BID = this.BID,
                            TagID = tagId,
                            OrderItemID = objectId
                        };
                        ctx.OrderItemTagLink.Add(tl);
                    }
                    break;
                case (int)ClassType.Order:
                    //check if tag exists
                    if (!ctx.OrderTagLink.Where(tl => tl.TagID == tagId && tl.OrderID == objectId).Any())
                    {
                        //if not add the tag
                        OrderTagLink tl = new OrderTagLink
                        {
                            BID = this.BID,
                            TagID = tagId,
                            OrderID = objectId
                        };
                        ctx.OrderTagLink.Add(tl);
                    }
                    break;
                default:
                    //check if tag exists
                    if (!ctx.ListTagOtherLink.Where(tl => tl.TagID == tagId && tl.AppliesToID == objectId && tl.AppliesToClassTypeID==classTypeId).Any())
                    {
                        //if not add the tag
                        ListTagOtherLink tl = new ListTagOtherLink
                        {
                            BID = this.BID,
                            TagID = tagId,
                            AppliesToID = objectId,
                            AppliesToClassTypeID = classTypeId
                        };
                        ctx.ListTagOtherLink.Add(tl);
                    }
                    break;
            }
            await ctx.SaveChangesAsync();
            await this.SendTagRefresh(classTypeId, objectId);
            return new EntityActionChangeResponse()
            {
                Id = tagId,
                Message = "Successful",
                Success = true,
                ErrorMessage = ""
            };
        }

        /// <summary>
        /// Links a Tag with a record
        /// </summary>
        /// <param name="tagId">Tag ID</param>
        /// <param name="classTypeId">ClassType ID that you're linking to</param>
        /// <param name="objectId">ID of the object you're linking to</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> UnLinkTag(short tagId, int classTypeId, int objectId)
        {
            switch (classTypeId)
            {
                case (int)ClassType.Contact:
                    //check if tag exists
                    var contactResults = ctx.ContactTagLink.Where(tl => tl.TagID == tagId && tl.ContactID == objectId);
                    if (contactResults.Any()) ctx.ContactTagLink.RemoveRange(contactResults);
                    break;
                case (int)ClassType.Company:
                    //check if tag exists
                    var companyResults = ctx.CompanyTagLink.Where(tl => tl.TagID == tagId && tl.CompanyID == objectId);
                    if (companyResults.Any()) ctx.CompanyTagLink.RemoveRange(companyResults);
                    break;
                case (int)ClassType.OrderDestination:
                    //check if tag exists
                    var orderDestinationResults = ctx.OrderDestinationTagLink.Where(tl => tl.TagID == tagId && tl.OrderDestinationID == objectId);
                    if (orderDestinationResults.Any()) ctx.OrderDestinationTagLink.RemoveRange(orderDestinationResults);
                    break;
                case (int)ClassType.OrderItem:
                    //check if tag exists
                    var orderItemResults = ctx.OrderItemTagLink.Where(tl => tl.TagID == tagId && tl.OrderItemID == objectId);
                    if (orderItemResults.Any()) ctx.OrderItemTagLink.RemoveRange(orderItemResults);
                    break;
                case (int)ClassType.Order:
                    //check if tag exists
                    var orderResults = ctx.OrderTagLink.Where(tl => tl.TagID == tagId && tl.OrderID == objectId);
                    if (orderResults.Any()) ctx.OrderTagLink.RemoveRange(orderResults);
                    break;
                default:
                    //check if tag exists
                    var otherResults = ctx.ListTagOtherLink.Where(tl => tl.TagID == tagId && tl.AppliesToID == objectId && tl.AppliesToClassTypeID == classTypeId);
                    if (otherResults.Any()) ctx.ListTagOtherLink.RemoveRange(otherResults);
                    break;
            }
            await ctx.SaveChangesAsync();
            await this.SendTagRefresh(classTypeId, objectId);
            return new EntityActionChangeResponse()
            {
                Id = tagId,
                Message = "Successful",
                Success = true,
                ErrorMessage = ""
            };
        }

        /// <summary>
        /// SendTagRefresh
        /// </summary>
        /// <param name="classTypeID"></param>
        /// <param name="entityID"></param>
        /// <returns></returns>
        public async Task SendTagRefresh(int classTypeID, int entityID){
            await this.rtmClient.SendRefreshMessage(
                new RefreshMessage(){//create a new refresh message
                    RefreshEntities = new List<RefreshEntity>(){//refresh message contains list of refresh entity
                        new RefreshEntity(){
                            BID = this.BID,
                            ClasstypeID = classTypeID,
                            ID = entityID,
                            Data = "tagChange",
                            RefreshMessageType = RefreshMessageType.Change
                        }//list of refresh entity contains a refresh entity
                    }
                }
            );
        }

        /// <summary>
        /// Additional logic to before before validation
        /// </summary>
        /// <param name="newModel">BoardDefinitionData model</param>
        internal override void DoBeforeValidate(ListTag newModel)
        {
            newModel.Name = newModel.Name.Trim();
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<ListTag, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<ListTag, short>[]
            {
                
            };
        }

        /// <summary>
        /// Builds a query with primary filters
        /// </summary>
        /// <param name="query">Tag query</param>
        /// <param name="ID">Tag ID</param>
        /// <returns></returns>
        public override IQueryable<ListTag> WherePrimary(IQueryable<ListTag> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Check that name is unique
        /// if ID is passed, ignores that ID
        /// so that you can checkName on an existing entity to change its RGB without this unique name check causing an error
        /// </summary>
        /// <param name="id">ID of existing entity</param>
        /// <param name="name">Name to check against</param>
        /// <returns></returns>
        public bool CheckName(short? id, string name)
        {
            var iqueryable = ctx.ListTag.Where(d => d.Name == name && !(d.IsDeleted));
            if (id.HasValue)
            {
                iqueryable = iqueryable.Where(d => d.ID != id.Value);
            }
            if (iqueryable.Any())
            {
                return false;
            }
            return true;
        }

        /*/// <summary>
        /// Check that value is a system value
        /// </summary>
        /// <param name="ID">ID to check against</param>
        /// <returns></returns>
        public bool IsSystem(short ID)
        {
            var value = ctx.ListTag.Where(d => d.ID == ID).FirstOrDefault();
            if (value == null) return false;
            if (value.IsSystem)
            {
                return true;
            }
            return false;
        }*/

        /// <summary>
        /// before update hook
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async override Task DoBeforeUpdateAsync(ListTag oldModel, ListTag model)
        {
            if (oldModel.IsSystem)
                throw new Exception("Cannot modify system value");
            await base.DoBeforeUpdateAsync(oldModel, model);
        }

        /// <summary>
        /// DoAfterUpdateAsync
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(ListTag oldModel, ListTag newModel, string connectionID)
        {
            await base.DoAfterUpdateAsync(oldModel, newModel, connectionID);
            
            //https://corebridge.atlassian.net/browse/END-5972
            if(newModel.IsDeleted)
            {
                var orderItemTagLinks = await this.ctx.OrderItemTagLink.Where(tl => tl.TagID==newModel.ID).ToListAsync();
                foreach (var taglink in orderItemTagLinks)
                {
                    await this.UnLinkTag(taglink.TagID, (int)ClassType.OrderItem, taglink.OrderItemID);
                }
            }
        }
    }
}
