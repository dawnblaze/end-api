﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Responses;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// TimeCardDetail Service
    /// </summary>
    public class TimeCardDetailService : AtomCRUDService<TimeCardDetail, short>
    {
        /// <summary>
        /// Constructs a TimeCardDetail service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public TimeCardDetailService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Returns TimeCardDetail based on filters supplied in
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<List<TimeCardDetail>> GetWithFiltersAsync(TimeCardDetailFilter filters)
        {
            if (filters != null && filters.HasFilters)
            {
                return await this.GetWhere(filters.WherePredicates());
            }

            return await this.GetAsync();
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] {
            "TimeClockActivity",
            "OrderItemStatus",
            "TimeClockBreak",
        };

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// Maps a specific item.
        /// </summary>
        /// <param name="item">Item to map</param>
        /// <param name="includes">Child/Parent includes</param>
        public override Task MapItem(TimeCardDetail item, IExpandIncludes includes = null)
        {

            return Task.CompletedTask;
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<TimeCardDetail, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<TimeCardDetail, short>[0];
        }

        internal override void DoBeforeValidate(TimeCardDetail newModel) { }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<TimeCardDetail> WherePrimary(IQueryable<TimeCardDetail> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Executes after a successful TimeCardDetail creation
        /// </summary>
        /// <param name="newModel">TimeCardDetail</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(TimeCardDetail newModel, Guid? tempGuid = null)
        {

            await ctx.SaveChangesAsync();
            await base.DoAfterAddAsync(newModel, tempGuid);
        }

        internal async Task<TimeCardDetailStatus[]> GetStatusesAndActivities()
        {
            var lookup = await TimeCardStatusOptionsLookup.Lookup(BID, ctx, migrationHelper);

            List<TimeCardDetailStatus> results = new List<TimeCardDetailStatus>();

            if (lookup.AllowBreaks){
                results.AddRange(await this.ctx.FlatListItem.Where(x => x.BID == this.BID && x.FlatListType == FlatListType.TimeCardBreaks).Select(y => new TimeCardDetailStatus()
                {
                    Name = y.Name,
                    ID = y.ID,
                    Type = "Break",
                    IsPaid = false
                }).ToArrayAsync());
            }
            if (lookup.AllowItemStatus){
                results.AddRange(await this.ctx.OrderItemStatus.Where(x => x.BID == this.BID).Select(y => new TimeCardDetailStatus()
                {
                    Name = y.Name,
                    ID = y.ID,
                    Type = "OrderItemStatus",
                    IsPaid = true
                }).ToArrayAsync());
            }
            if (lookup.AllowCustomStatus)
            {
                results.AddRange(await this.ctx.FlatListItem.Where(x => x.BID == this.BID && x.FlatListType == FlatListType.TimeCardActivities).Select(y => new TimeCardDetailStatus()
                {
                    Name = y.Name,
                    ID = y.ID,
                    Type = "Activity",
                    IsPaid = true
                }).ToArrayAsync());
            }

            return results.ToArray();
        }
    }

    internal class TimeCardStatusOptionsLookup
    {
        internal bool AllowBreaks { get; private set; }
        internal bool AllowCustomStatus { get; private set; }
        internal bool AllowItemStatus { get; private set; }

        public async static Task<TimeCardStatusOptionsLookup> Lookup(short BID, ApiContext ctx, IMigrationHelper migrationHelper)
        {
            OptionService svc = new OptionService(ctx, migrationHelper);

            var breakOption = await svc.Get(
                optionName: "Timeclock.BreakTracking.Enabled", bid: BID,
                optionID: null, locationId: null, storeFrontID: null, userLinkID: null, companyID: null, contactID: null
                );
            var itemStatusOption = await svc.Get(
                optionName: "Timeclock.ActivityTracking.UseItemStatus", bid: BID,
                optionID: null, locationId: null, storeFrontID: null, userLinkID: null, companyID: null, contactID: null
                );
            var customOption = await svc.Get(
                optionName: "Timeclock.ActivityTracking.UseCustomList", bid: BID,
                optionID: null, locationId: null, storeFrontID: null, userLinkID: null, companyID: null, contactID: null
                );

            return new TimeCardStatusOptionsLookup()
            {
                AllowBreaks = breakOption?.ValueAsBoolean() ?? false,
                AllowItemStatus = itemStatusOption?.ValueAsBoolean() ?? false,
                AllowCustomStatus = customOption?.ValueAsBoolean() ?? false,
            };
        }
    }

    internal static class TimeCardDetailExtensions
    {
        public static TimeCardDetailStatus ToStatus(this TimeCardDetail detail, TimeCardStatusOptionsLookup optionLookup)
        {
            if (detail.TimeClockBreakID.HasValue &&  detail.TimeClockBreak != null)
            {
                return new TimeCardDetailStatus()
                {
                    ID = detail.TimeClockBreakID.Value,
                    IsPaid = false,
                    Name = detail.TimeClockBreak.Name,
                    Type = "Break"
                };
            }
            else if (detail.TimeClockActivityID.HasValue && detail.TimeClockActivity != null)
            {
                return new TimeCardDetailStatus()
                {
                    ID = detail.TimeClockActivityID.Value,
                    IsPaid = true,
                    Name = detail.TimeClockActivity.Name,
                    Type = "Activity"
                };
            }
            else if (detail.OrderItemStatusID.HasValue && detail.OrderItemStatus != null)
            {
                return new TimeCardDetailStatus()
                {
                    ID = detail.OrderItemStatusID.Value,
                    IsPaid = true,
                    Name = detail.OrderItemStatus.Name,
                    Type = "OrderItemStatus"
                };
            }
            else
            {
                return null;
            }
        }
    }
}
