﻿using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// A service for <see cref=" AssemblyCategory"/>
    /// </summary>
    public class AssemblyCategoryService : AtomCRUDService<AssemblyCategory, short>, ISimpleListableViewService<SimpleAssemblyCategory, short>
    {
        /// <summary>
        /// Constructs a Assembly Category service with injected params
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public AssemblyCategoryService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Returns a list of `AssemblyCategory` that shares the same name as the `model`
        /// </summary>
        /// <param name="model">`AssemblyCategory` model to check Name from</param>
        public List<AssemblyCategory> ExistingNameOwners(AssemblyCategory model)
        {
            return this.ctx.AssemblyCategory
                .Where(category =>
                        category.BID == this.BID
                        && category.Name.Trim().Equals(model.Name.Trim())
                        && category.ID != model.ID)
                .ToList();
        }

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<AssemblyCategory, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<AssemblyCategory, short>[0];
        }

        internal override void DoBeforeValidate(AssemblyCategory newModel) { }

        /// <summary>
        /// Changes clone name
        /// </summary>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override Task DoBeforeCloneAsync(AssemblyCategory clone)
        {
            //clone.Name = GetNextClonedName(clone.Name, t => t.Name);
            clone.Name = GetNextClonedName(ctx.Set<AssemblyCategory>().AsNoTracking().Where(t => t.BID == BID && t.Name.StartsWith(t.Name)).ToList(), clone.Name, t => t.Name);
            return base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Clone links as well
        /// </summary>
        /// <param name="clonedFromID">Assmebly Category ID we Cloned from</param>
        /// <param name="clone">Cloned Assembly Category</param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(short clonedFromID, AssemblyCategory clone)
        {
            var cloneFromLinks = await ctx.AssemblyCategoryLink.Where(l => l.BID == this.BID && l.CategoryID == clonedFromID).ToListAsync();
            if (cloneFromLinks != null && cloneFromLinks.Count > 0)
            {
                List<AssemblyCategoryLink> newLinks = new List<AssemblyCategoryLink>();
                foreach(var link in cloneFromLinks)
                {
                    newLinks.Add(new AssemblyCategoryLink()
                    {
                        BID = this.BID,
                        CategoryID = clone.ID,
                        PartID = link.PartID
                    });
                }
                ctx.AssemblyCategoryLink.AddRange(newLinks);
                await ctx.SaveChangesAsync();
            }
            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        /// <summary>
        /// Business logic to do after adding the new model.
        /// </summary>
        /// <param name="model">Model that was added</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(AssemblyCategory model, Guid? tempGuid)
        {
           await base.DoAfterAddAsync(model, tempGuid);
           await this.LinkOrUnlinkParts(model);
        }        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(AssemblyCategory oldModel, AssemblyCategory newModel, string connectionID)
        {
            await base.DoAfterUpdateAsync(oldModel, newModel, connectionID);
            await this.LinkOrUnlinkParts(newModel);
        }          

        internal async Task<List<AssemblyCategory>> GetWithFiltersAsync(AssemblyCategoryFilter filters)
        {
            if (filters != null && filters.IsActive.HasValue)
            {
                return await this.GetWhere(filters.WherePredicates());
            }
            else
                return await this.GetAsync(null);
        }

        /// <summary>
        /// Link/Unlink Labor Category with Labor Data
        /// </summary>
        /// <param name="assemblyCategoryID">Assembly Category ID</param>
        /// <param name="assemblyDataID">Assembly Data ID</param>
        /// <param name="isLinked">If it is to link or unlink</param>
        /// <param name="indexModel"></param>
        public async Task<EntityActionChangeResponse> LinkAssembly(short assemblyCategoryID, int assemblyDataID, bool isLinked, bool indexModel = true)
        {
            /*          
                @BID
                @AssemblyCategoryID
                @AssemblyDataID
                @IsLinked
                @Result Output
            */
            try
            {
                SqlParameter Result = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                Result.Direction = System.Data.ParameterDirection.Output;

                object[] myParams = {
                    new SqlParameter("@BID", this.BID),
                    new SqlParameter("@SubassemblyCategoryID", assemblyCategoryID),
                    new SqlParameter("@SubassemblyDataID", assemblyDataID),
                    new SqlParameter("@IsLinked", isLinked),
                    Result
                };

                int affectedRows = await ctx.Database.ExecuteSqlRawAsync(
                    "EXEC dbo.[Part.Subassembly.Category.Action.LinkSubassembly] @BID, @SubassemblyCategoryID, @SubassemblyDataID, @IsLinked, @Result Output;",
                    parameters: myParams);

                if (affectedRows > 0)
                {
                    if (indexModel)
                    {
                        await QueueIndexForModel(assemblyCategoryID);
                    }

                    return new EntityActionChangeResponse()
                    {
                        Id = assemblyCategoryID,
                        Message = $"Successfully {(isLinked ? "linked" : "unlinked")} AssemblyCategory:{assemblyCategoryID} to AssemblyData:{assemblyDataID}",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = assemblyCategoryID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }

            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        /// <summary>
        /// Remove all Assembly Category Links
        /// </summary>
        public async Task unlinkAllAsync(short assemblyCategoryID)
        {
            var existingLinks = this.ctx.AssemblyCategoryLink.Where(link => link.CategoryID == assemblyCategoryID);
            if (existingLinks.Count() > 0)
            {
                foreach (var link in existingLinks)
                {
                    this.ctx.AssemblyCategoryLink.Remove(link);
                }
                await ctx.SaveChangesAsync();
            }
        }

        //-------------unmapped items----------------

        /// <summary>
        /// Maps a specific item.
        /// </summary>
        /// <param name="item">Item to map</param>
        /// <param name="includeExpander">Child/Parent includes</param>
        public override async Task MapItem(AssemblyCategory item, IExpandIncludes includeExpander = null)
        {
            var includes = includeExpander != null ? includeExpander.GetIncludes() : null;
            if (includes != null)
            {
                if (item.AssemblyCategoryLinks?.Count > 0)
                {
                    switch (includes[AssemblyCategoryIncludes.AssemblyPartsKey].Level)
                    {
                        case IncludesLevel.Full:
                            item.Assemblies = await Task.FromResult(item.AssemblyCategoryLinks.Select(link => link.Assembly).ToList());
                            break;
                        case IncludesLevel.Simple:
                            item.SimpleAssemblies = await Task.FromResult(item.AssemblyCategoryLinks
                                                    .Select(link => new SimpleAssemblyData()
                                                    {
                                                        BID = link.Assembly.BID,
                                                        ID = link.Assembly.ID,
                                                        ClassTypeID = link.Assembly.ClassTypeID,
                                                        IsActive = link.Assembly.IsActive,
                                                        IsDefault = false,
                                                        DisplayName = link.Assembly.Name
                                                    }).ToList());
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<AssemblyCategory> WherePrimary(IQueryable<AssemblyCategory> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cat"></param>
        /// <returns></returns>
        public async Task LinkOrUnlinkParts(AssemblyCategory cat)
        {
            await PartsToCategory.LinkOrUnlink(
                this.ctx.AssemblyCategoryLink.Where(link => link.CategoryID == cat.ID && link.BID == this.BID).ToList(),
                cat.SimpleAssemblies,
                (link, simplePart)=>link.PartID == simplePart.ID,
                async (link)=>await this.LinkAssembly(cat.ID, link.PartID, false),
                async (simplePart)=>await this.LinkAssembly(cat.ID, simplePart.ID, true)
            );
        }  

        /// <summary>
        /// Gets simple list set
        /// </summary>
        public DbSet<SimpleAssemblyCategory> SimpleListSet => this.ctx.SimpleAssemblyCategory;
    }
}
