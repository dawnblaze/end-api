﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// CompanyContactLink Service
    /// </summary>
    public class CompanyContactLinkService : LinkCRUDService<CompanyContactLink>, ICloneableChildCRUDService<ContactData, CompanyContactLink>
    {
        /// <summary>
        /// EmployeeTeamLink Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="bid">Business ID</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CompanyContactLinkService(ApiContext context, RemoteLogger logger, short bid, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, logger, bid, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Sets the ID of childModel to a new ID 
        /// </summary>
        /// <param name="parentModel"></param>
        /// <param name="childModel"></param>
        /// <returns></returns>
        public Task DoBeforeCloneForParent(ContactData parentModel, CompanyContactLink childModel)
        {
            childModel.ContactID = parentModel.ID;
            return Task.CompletedTask;
        }

        /// <summary>
        /// Gets an untracked snapshot of the EmployeeTeamLink before new updates are made
        /// </summary>
        /// <param name="newModel">Updated version of the EmployeeTeamLink</param>
        /// <returns></returns>
        public override async Task<CompanyContactLink> GetOldModelAsync(CompanyContactLink newModel)
        {
            return await ctx.CompanyContactLink.AsNoTracking()
                .FirstOrDefaultAsync(x => x.BID == newModel.BID
                    && x.ContactID == newModel.ContactID && x.CompanyID == newModel.CompanyID);
        }
    }
}
