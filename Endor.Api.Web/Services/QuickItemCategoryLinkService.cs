﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    public class QuickItemCategoryLinkService : LinkCRUDService<QuickItemCategoryLink>, ICloneableChildCRUDService<QuickItemData, QuickItemCategoryLink>
    {
        /// <summary>
        /// QuickItemCategoryLinkService Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="bid">Business ID</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public QuickItemCategoryLinkService(ApiContext context, RemoteLogger logger, short bid, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, logger, bid, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Sets the ID of childModel to a new ID 
        /// </summary>
        /// <param name="parentModel"></param>
        /// <param name="childModel"></param>
        /// <returns></returns>
        public Task DoBeforeCloneForParent(QuickItemData parentModel, QuickItemCategoryLink childModel)
        {
            // Make this dynamic ID like RequestIDAsync
            childModel.Category = null;
            childModel.QuickItem = null;
            childModel.QuickItemID = parentModel.ID;
            return Task.CompletedTask;
        }

        /// <summary>
        /// Gets an untracked snapshot of the QuickItemCategoryLink before new updates are made
        /// </summary>
        /// <param name="newModel">Updated version of the QuickItemCategoryLink</param>
        /// <returns></returns>
        public override async Task<QuickItemCategoryLink> GetOldModelAsync(QuickItemCategoryLink newModel)
        {
            return await ctx.QuickItemCategoryLink.AsNoTracking()
                .FirstOrDefaultAsync(x => x.BID == newModel.BID
                    && x.QuickItemID == newModel.QuickItemID);
        }
    }
}
