﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.EF;
using Endor.Api.Web.Classes;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// subclass to include value w/ option definition
    /// </summary>
    public class SystemOptionDefinitionWithValue: SystemOptionDefinition
    {
        /// <summary>
        /// current value of option
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// option id
        /// </summary>
        public int OptionID { get; set; }
    }

    /// <summary>
    /// name and parent tuple
    /// </summary>
    public class SystemOptionSectionBreadcrumbNode
    {
        /// <summary>
        /// name of the section
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// ID of the section
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// parent of this section
        /// </summary>
        public SystemOptionSectionBreadcrumbNode Parent { get; set; }

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="leafSection"></param>
        public SystemOptionSectionBreadcrumbNode(SystemOptionSection leafSection)
        {
            Name = leafSection.Name;
            ID = leafSection.ID;
            if (leafSection.Parent != null)
                Parent = new SystemOptionSectionBreadcrumbNode(leafSection.Parent);
        }
    }

    /// <summary>
    /// utility class to return section breadcrumb info
    /// </summary>
    public class SystemOptionCategoryWithSectionBreadcrumbs: SystemOptionCategory
    {
        /// <summary>
        /// section breadcrumb info
        /// </summary>
        public SystemOptionSectionBreadcrumbNode SectionBreadcrumb { get; set; }

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="from"></param>
        public SystemOptionCategoryWithSectionBreadcrumbs(SystemOptionCategory from)
        {
            IsContactOption     = from.IsContactOption    ;
            IsCompanyOption     = from.IsCompanyOption    ;
            IsUserOption        = from.IsUserOption;
            IsStorefrontOption  = from.IsStorefrontOption ;
            IsLocationOption    = from.IsLocationOption   ;
            IsBusinessOption    = from.IsBusinessOption   ;
            IsAssociationOption = from.IsAssociationOption;
            IsSystemOption      = from.IsSystemOption     ;
            SearchTerms         = from.SearchTerms        ;
            IsHidden            = from.IsHidden           ;
            OptionLevels        = from.OptionLevels       ;
            Description         = from.Description        ;
            SectionID           = from.SectionID          ;
            Name                = from.Name               ;
            ID                  = from.ID                 ;
            Section             = from.Section            ;
            SystemOptionDefinitions = from.SystemOptionDefinitions;

            if (Section != null)
                SectionBreadcrumb = new SystemOptionSectionBreadcrumbNode(Section);

            Section = null;
        }
    }

    /// <summary>
    /// A service for <see cref=" OptionCategory"/>
    /// </summary>
    public class OptionCategoryService : AtomGenericService<OptionCategory, int>
    {
        /// <summary>
        /// Constructor to inject an ApiContext
        /// </summary>
        /// <param name="context">Api context</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OptionCategoryService(ApiContext context, IMigrationHelper migrationHelper) : base(context, migrationHelper)
        {
        }

        /// <summary>
        /// Retrieves Option Categories by ID
        /// </summary>
        /// <param name="id">Option Category ID</param>
        /// <param name="includeSectionBreadcrumbs"></param>
        public async Task<GenericResponse<SystemOptionCategory>> Get(int id, bool includeSectionBreadcrumbs = false)
        {
            try
            {
                var query = ctx.SystemOptionCategory
                                    .Include(a => a.SystemOptionDefinitions)
                                    .Where(a => a.ID == id);

                if (includeSectionBreadcrumbs)
                    query = query.Include(a => a.Section).Include(a => a.Section.Parent).Include(a => a.Section.Parent.Parent).Include(a => a.Section.Parent.Parent.Parent);

                SystemOptionCategory result = await query.FirstOrDefaultAsync();

                if (result == null)
                {
                    return new GenericResponse<SystemOptionCategory>()
                    {
                        Data = null,
                        Message = "Results not found.",
                        Success = true
                    };
                }
                else
                {
                    if (includeSectionBreadcrumbs)
                    {
                        result = new SystemOptionCategoryWithSectionBreadcrumbs(result);
                    }
                    return new GenericResponse<SystemOptionCategory>()
                    {
                        Data = result,
                        Message = "Successfully retrieved Option Categories.",
                        Success = true
                    };
                }
            }
            catch (Exception ex)
            {
                return new GenericResponse<SystemOptionCategory>()
                {
                    Data = null,
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Retrieves Option Categories by pageIndex and pageSize
        /// </summary>
        /// <param name="pageIndex">Option Category page index</param>
        /// <param name="pageSize">Option Category page size</param>
        public async Task<GenericResponse<List<SystemOptionCategory>>> Get(int pageIndex = 0, int pageSize = 10)
        {
            try
            {
                var result = await ctx.SystemOptionCategory.Skip(pageIndex * pageSize).Take(pageSize).ToListAsync();

                if (result == null || result.Count() == 0)
                {
                    return new GenericResponse<List<SystemOptionCategory>>()
                    {
                        Data = null,
                        Message = "Results not found.",
                        Success = true
                    };
                }
                else
                {
                    return new GenericResponse<List<SystemOptionCategory>>()
                    {
                        Data = result,
                        Message = "Successfully retrieved Option Categories.",
                        Success = true
                    };
                }
            }
            catch (Exception ex)
            {
                return new GenericResponse<List<SystemOptionCategory>>()
                {
                    Data = null,
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Retrieves Option Categories by level
        /// </summary>
        /// <param name="levels"></param>
        /// <returns></returns>
        public async Task<OptionCategoryResponse> GetByLevel(params byte[] levels)
        {
            try
            {
                List<SystemOptionCategory> categories = new List<SystemOptionCategory>();
                foreach (var level in levels.Distinct())
                {
                    List<SqlParameter> sp = new List<SqlParameter>()
                    {
                        new SqlParameter() { ParameterName = "@Level", SqlDbType = System.Data.SqlDbType.TinyInt, Value = level }
                    };
                    var rowResult = ctx.SystemOptionCategory.FromSqlRaw<SystemOptionCategory>(@"SELECT * FROM [dbo].[Option.Categories.ByLevel] ( @Level )",
                                        parameters: sp.ToArray())
                                        .Include("Section")
                                        .Include("Section.Parent")
                                        .Include("SystemOptionDefinitions")
                                        .Include("SystemOptionDefinitions.Options");
                    categories.AddRange(await rowResult.ToListAsync());
                }

                if (categories.Count == 0)
                {
                    return new OptionCategoryResponse
                    {
                        Message = "No result found.",
                        Success = true
                    };
                }
                else
                {
                    // extract sections from results
                    List<SystemOptionSection> sections = categories.Select(a => a.Section).Distinct().ToList();
                    List<SystemOptionSection> hierarchySections = new List<SystemOptionSection>();

                    foreach(var section in sections)
                    {
                        GetHierarchialSections(section, hierarchySections);
                    }
                    sections.AddRange(hierarchySections.ToList());
                    sections.ForEach(x => x.Parent = null);

                    categories.ForEach(x => x.Section = null);
                    categories.ForEach(x => {
                        foreach(var y in x.SystemOptionDefinitions)
                        {
                            y.Category = null;
                        }
                    });
                    return new OptionCategoryResponse
                    {
                        OptionSections = sections
                                            .Distinct()
                                            .Where(a => a.ParentID == null)
                                            .OrderBy(a => a.ParentID)
                                            .ThenBy(a => a.ID).ToList(),
                        //OptionCategories = categories,
                        Message = "Successfully retrieved Option Categories.",
                        Success = true
                    };
                }
            }
            catch (Exception ex)
            {
                return new OptionCategoryResponse
                {
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Get includes for model
        /// </summary>
        /// <returns></returns>
        public override string[] GetIncludes()
        {
            return new string[] 
            {
                "Section",
                "Section.Parent",
                //"OptionSection.Children",
                "SystemOptionDefinitions"
            };
        }

        internal async Task<SystemOptionDefinitionWithValue[]> GetSystemOptionDefinitionsWithValueByCategory(int categoryID, byte? associationID, short? bid, short? locationID, short? storeFrontID, short? employeeID, int? companyID, int? contactID)
        {
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@CategoryID", SqlDbType = System.Data.SqlDbType.SmallInt, Value = categoryID },
                new SqlParameter() { ParameterName = "@BID", SqlDbType = System.Data.SqlDbType.SmallInt, Value = (object)bid ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@LocationID", SqlDbType = System.Data.SqlDbType.SmallInt, Value = (object)locationID ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@StoreFrontID", SqlDbType = System.Data.SqlDbType.SmallInt, Value = (object)storeFrontID ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@EmployeeID", SqlDbType = System.Data.SqlDbType.SmallInt, Value = (object)employeeID ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@CompanyID", SqlDbType = System.Data.SqlDbType.Int, Value = (object)companyID ?? DBNull.Value },
                new SqlParameter() { ParameterName = "@ContactID", SqlDbType = System.Data.SqlDbType.Int, Value = (object)contactID ?? DBNull.Value },
            };

            var rowResult = await ctx.Database.GetModelFromQueryAsync<SystemOptionDefinitionWithValue>(@"EXEC dbo.[Option.GetValues] 
                @CategoryID, @BID, @LocationID, @StoreFrontID, @EmployeeID, @CompanyID, @ContactID;", parameters: sp.ToArray());

            return rowResult.ToArray();
        }

        private void GetHierarchialSections(SystemOptionSection node, List<SystemOptionSection> sections)
        {
            if (node.Parent != null)
            {
                sections.Add(node.Parent);
                GetHierarchialSections(node.Parent, sections);
            }
        }
    }
}
