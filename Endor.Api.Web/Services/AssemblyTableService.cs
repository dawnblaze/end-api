﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using System;
using System.Linq;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// assembly Table service
    /// </summary>
    public class AssemblyTableService : AtomCRUDService<AssemblyTable, short>, IDoBeforeCreateUpdateWithParent<AssemblyTable, AssemblyData>
    {
        /// <summary>
        /// Constructs a AssemblyTableService service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public AssemblyTableService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return new string[] { };
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">AssemblyTable ID</param>
        /// <returns></returns>
        public override IQueryable<AssemblyTable> WherePrimary(IQueryable<AssemblyTable> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<AssemblyTable, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<AssemblyTable, short>[]{
                // CreateChildAssociation<AssemblyService, AssemblyTable, int>(e => e.AssemblyID),
            };
        }

        internal override void DoBeforeValidate(AssemblyTable newModel)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public void DoBeforeCreateWithParent(AssemblyTable child, AssemblyData parent)
        {
            child.AssemblyID = parent.ID;
            FixUpColumnAndRowIDs(child, parent);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        public void DoBeforeUpdateWithParent(AssemblyTable child, AssemblyData oldParent, AssemblyData newParent)
        {
            child.AssemblyID = newParent.ID;
            FixUpColumnAndRowIDs(child, newParent);
        }

        private static void FixUpColumnAndRowIDs(AssemblyTable child, AssemblyData parent)
        {
            if (!String.IsNullOrWhiteSpace(child.ColumnVariableTempID) && child.ColumnVariableID < 1)
            {
                AssemblyVariable matchingVar = parent.Variables.FirstOrDefault((x) => { return x.TempID == Guid.Parse(child.ColumnVariableTempID); });
                if (matchingVar != null)
                    child.ColumnVariableID = matchingVar.ID;
            }

            if (!String.IsNullOrWhiteSpace(child.RowVariableTempID) && child.RowVariableID < 1)
            {
                AssemblyVariable matchingVar = parent.Variables.FirstOrDefault((x) => { return x.TempID == Guid.Parse(child.RowVariableTempID); });
                if (matchingVar != null)
                    child.RowVariableID = matchingVar.ID;
            }
        }
    }
}
