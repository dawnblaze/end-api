﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Order Note Service Layer
    /// </summary>
    public class OrderNoteService : AtomCRUDService<OrderNote, int>
    {
        private IRTMPushClient _rtmPushClient;

        /// <summary>
        /// Order Note Service Layer
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmPushClient">RTM Push Client</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderNoteService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmPushClient, IMigrationHelper migrationHelper) 
            : base(context, bid, taskQueuer, cache, logger, rtmPushClient, migrationHelper)
        {
            this._rtmPushClient = rtmPushClient;
        }

        /// <summary>
        /// Default children to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] {
            "Employee",
            "Contact"
        };

        /// <summary>
        /// Get child associations (services that handle business logic for child collections). May return null.
        /// </summary>
        /// <returns>IChildServiceAssociation or null</returns>
        protected override IChildServiceAssociation<OrderNote, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<OrderNote, int>[0];
        }

        internal override void DoBeforeValidate(OrderNote newModel)
        {

        }

        /// <summary>
        /// Returns the includes
        /// </summary>
        /// <param name="includes">Children to include</param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }


        /// <summary>
        /// Creates or Updates an OrderNote
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="noteType"></param>
        /// <param name="orderNote"></param>
        /// <param name="connectionID"></param>
        /// <returns>Task&lt;OrderNote&gt;<see cref="Task&lt;OrderNote&gt;"/></returns>
        public async Task<OrderNote> CreateOrUpdate(int orderId, OrderNoteType noteType, OrderNote orderNote, string connectionID)
        {
            //Try and locate an existing note
            OrderNote note = await this.FirstOrDefaultAsync(n => n.OrderID == orderId && n.IsOrderNote && n.NoteType == noteType);

            if (note != null)
            {//Update        
                note.Note = orderNote.Note;
                note.NoteType = orderNote.NoteType;

                //SetUpdateModelProperties(note.ID, orderNote);

                await this.UpdateAsync(note, connectionID);
                return note;
            }
            else
            {//Create         
                SetCreateModelProperties(orderNote);
                orderNote.OrderID = orderId;
                await this.CreateAsync(orderNote);

                return await this.FirstOrDefaultAsync(n => n.OrderID == orderId && n.IsOrderNote && n.NoteType == noteType);
            }

        }

        /// <summary>
        /// Attempts to Delete an OrderNote
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="noteType"></param>
        /// <returns>Task&lt;OrderNote&gt;<see cref="Task&lt;OrderNote&gt;"/></returns>
        public async Task<EntityActionChangeResponse> DeleteAsync(int orderId, OrderNoteType noteType)
        {
            //Locate note to be deleted
            try
            {
                var note = await this.FirstOrDefaultAsync(n => n.OrderID == orderId && n.IsOrderNote && n.NoteType == noteType, asNoTracking: true);

                if (note == null)
                {
                    return new EntityActionChangeResponse
                    {
                        Id = orderId,
                        Message = "No rows affected.",
                        Success = false
                    };
                }

                BooleanResponse canDelete = await CanDelete(note.ID);

                if (canDelete.Success)
                {
                    bool result = await DeleteAsync(note);

                    if (result)
                    {
                        await _rtmPushClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnDelete(note) });

                        return new EntityActionChangeResponse
                        {
                            Id = orderId,
                            Message = "Successfully deleted order note.",
                            Success = true
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = orderId,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }

            return new EntityActionChangeResponse
            {
                Id = orderId,
                Message = "No rows affected.",
                Success = false
            };
        }

        /// <summary>
        /// gets clause for filtering by primary key
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<OrderNote> WherePrimary(IQueryable<OrderNote> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Before create
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(OrderNote newModel, Guid? tempGuid = null)
        {
            newModel.CreatedDT = DateTime.UtcNow;
            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }
    }
}
