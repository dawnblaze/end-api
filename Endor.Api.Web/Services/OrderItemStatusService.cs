﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// order item status service
    /// </summary>
    public class OrderItemStatusService : AtomCRUDService<OrderItemStatus, short>
    {
        private Lazy<OrderItemSubStatusService> _orderItemSubStatusService;
        internal OrderItemSubStatusService _OrderItemSubStatusService => _orderItemSubStatusService.Value;

        /// <summary>
        /// Constructs a new OrderItemStatusService with a number of injeced parameters
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderItemStatusService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache,
            RemoteLogger logger, IRTMPushClient rtmClient,IMigrationHelper migrationHelper) :
            base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
            this._orderItemSubStatusService = new Lazy<OrderItemSubStatusService>(() =>
                new OrderItemSubStatusService(ctx, this.BID, taskQueuer, cache, logger, rtmClient, migrationHelper));
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// override for set modified dt for temporal table
        /// </summary>
        /// <param name="newModel"></param>
        protected override void SetModifiedDT(OrderItemStatus newModel)
        {
            ctx.Entry(newModel).Property(x => x.ModifiedDT).IsModified = false;
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<OrderItemStatus, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<OrderItemStatus, short>[]
            {
                //CreateChildAssociation<OrderItemStatusService, OrderItemStatus, int>((a)=>a.SubGroups)
            };
        }

        internal IEnumerable<SimpleOrderItemStatus> GetSimpleList(short value, SimpleOrderItemStatusFilters filters)
        {
            return this.SelectWhereAll(filters.WherePredicates(), filters);
        }

        /// <summary>
        /// Sets a Business OrderItemStatus as the Default
        /// </summary>
        /// <param name="id">OrderItemStatus Id</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> SetDefault(short id)
        {
            try
            {
                var status = await ctx.OrderItemStatus.AsNoTracking().Where(x => x.BID == this.BID && x.ID == id).FirstOrDefaultAsync();
                
                if (status != null)
                {
                    SqlParameter resultParam = new SqlParameter("@Result", SqlDbType.Int)
                    { Direction = ParameterDirection.Output };

                    object[] myParams =
                    {
                        new SqlParameter("@BID", this.BID),
                        new SqlParameter("@ID", id),
                        new SqlParameter("@OrderStatusID", status.OrderStatusID),
                        new SqlParameter("@TransactionType", status.TransactionType),
                        resultParam
                    };

                    await this.ctx.Database.ExecuteSqlRawAsync(
                        "EXEC [dbo].[Order.Item.Status.Action.SetDefault] @BID, @ID, @OrderStatusID, @TransactionType, @Result = @Result Output;",
                        myParams);

                    await this.SetActive(id, true, null);

                    int resultParamValue = (int)resultParam.Value;

                    if (resultParamValue > 0)
                    {
                        await QueueIndexForModel(id);

                        base.DoGetRefreshMessagesOnUpdate(new OrderItemStatus()
                        {
                            ClassTypeID = Convert.ToInt32(ClassType.OrderItemStatus),
                            ID = Convert.ToInt16(id),
                            BID = this.BID,
                        });

                        return new EntityActionChangeResponse()
                        {
                            Id = id,
                            Message = $"Successfully set OrderItemStatus (ID: {id}) to Default.",
                            Success = true
                        };
                    }
                    else
                    {
                        return new EntityActionChangeResponse()
                        {
                            Id = id,
                            Message = "No rows affected.",
                            Success = false
                        };
                    }
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = id,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = id,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        private async Task<EntityActionChangeResponse> Move(short id, short targetID, bool moveAfter)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", SqlDbType.Int)
                    {Direction = ParameterDirection.Output};

                object[] myParams =
                {
                    new SqlParameter("@BID", this.BID),
                    new SqlParameter("@ID", id),
                    new SqlParameter("@TargetID", targetID),
                    new SqlParameter("@MoveAfter", moveAfter == true ? 1 : 0),
                    resultParam
                };

                await this.ctx.Database.ExecuteSqlRawAsync(
                    "EXEC [dbo].[Order.Item.Status.Action.Move] @BID, @ID, @TargetID, @MoveAfter, @Result = @Result Output;",
                    myParams);

                int resultParamValue = (int) resultParam.Value;

                if (resultParamValue > 0)
                {
                    var beforeOrAfter = moveAfter ? "after" : "before";
                    return new EntityActionChangeResponse()
                    {
                        Id = id,
                        Message = $"Successfully Moved OrderItemStatus (ID: {id}) {beforeOrAfter} (ID: {targetID}) .",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = id,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = id,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }


        /// <summary>
        /// Move before an OrderItemStatus
        /// </summary>
        /// <param name="id">OrderItemStatus ID</param>
        /// /// <param name="targetID">Target ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> MoveBefore(short id, short targetID)
        {
            return await this.Move(id, targetID, false);
        }

        /// <summary>
        /// Move before an OrderItemStatus
        /// </summary>
        /// <param name="id">OrderItemStatus ID</param>
        /// /// <param name="targetID">Target ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> MoveAfter(short id, short targetID)
        {
            return await this.Move(id, targetID, true);
        }

        /// <summary>
        /// Links an OrderItemStatus by its ID to an OrderItemSubStatus by its ID or Name
        /// </summary>
        /// <param name="orderItemStatusID">OrderItemStatus's ID</param>
        /// <param name="orderItemSubStatus">OrderItemSubStatus ID or Name</param>
        /// <param name="isLinked"></param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkSubStatus(short orderItemStatusID, string orderItemSubStatus,
            bool isLinked)
        {
            try
            {
                //int orderItemSubStatusID = -1;
                if (!int.TryParse(orderItemSubStatus, out int orderItemSubStatusID))
                {
                    //must be the name so we will try to find that
                    orderItemSubStatusID = await _OrderItemSubStatusService.Where((t) => t.Name == orderItemSubStatus)
                        .Select(t => t.ID).FirstOrDefaultAsync();
                }

                if (orderItemSubStatusID == default(int))
                    throw new Exception("Could not find OrderItemSubStatus Id or Name for: " + orderItemSubStatus);

                SqlParameter Result = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                Result.Direction = System.Data.ParameterDirection.Output;

                object[] myParams =
                {
                    new SqlParameter("@BID", this.BID),
                    new SqlParameter("@SubStatusID", orderItemSubStatusID),
                    new SqlParameter("@OrderStatusID", orderItemStatusID),
                    new SqlParameter("@IsLinked", isLinked),
                    Result
                };

                int affectedRows = await ctx.Database.ExecuteSqlRawAsync(
                    "EXEC dbo.[Order.Item.Status.Action.LinkSubStatus] @BID, @SubStatusID, @OrderStatusID, @IsLinked, @Result Output;",
                    parameters: myParams);

                if (affectedRows > 0)
                {
                    await QueueIndexForModel(orderItemStatusID);

                    base.DoGetRefreshMessagesOnUpdate(new OrderItemStatus()
                    {
                        ClassTypeID = Convert.ToInt32(ClassType.OrderItemStatus),
                        ID = Convert.ToInt16(orderItemSubStatusID),
                        BID = this.BID,
                    });

                    return new EntityActionChangeResponse()
                    {
                        Id = orderItemSubStatusID,
                        Message =
                            $"Successfully {(isLinked ? "linked" : "unlinked")} OrderItemStatusID:{orderItemStatusID} to OrderItemSubStatusID:{orderItemSubStatus}",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        //Id = orderItemSubStatusID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }

            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        /// <summary>
        /// Unlinks an OrderItemStatus by its ID to an OrderItemSubStatus by its ID
        /// </summary>
        /// <param name="orderItemStatusID">OrderItemStatus's ID</param>
        /// <param name="unLinkSubStatusID">OrderItemSubStatus's ID</param>
        /// <returns></returns>
        public EntityActionChangeResponse UnLinkSubStatus(short orderItemStatusID, short unLinkSubStatusID)
        {
            EntityActionChangeResponse resp = new EntityActionChangeResponse()
            {
                Success = false,
                Message = "Not implemented.",
                ErrorMessage = "Not implemented."
            };

            return resp;
        }

        internal override void DoBeforeValidate(OrderItemStatus newModel)
        {
        }

        /// <summary>
        /// OrderItemStatus Service Override of DoBeforeCreate
        /// </summary>
        /// <param name="newModel">OrderItemStatus</param>
        /// <param name="tempGuid">Optionala Temp Guid</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(OrderItemStatus newModel, Guid? tempGuid = null)
        {
            var existingStatuses = await ctx.OrderItemStatus.AsNoTracking().Where(x => x.BID == this.BID && x.OrderStatusID == newModel.OrderStatusID && x.TransactionType == newModel.TransactionType).ToListAsync();

            // If No existing status IsDefault=true
            if (existingStatuses == null)
            {
                newModel.IsDefault = true;
            }

            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// fills SimpleOrderItemSubStatuses
        /// </summary>
        /// <param name="item"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override Task MapItem(OrderItemStatus item, IExpandIncludes includes = null)
        {
            if (includes != null)
            {
                if (((OrderItemStatusIncludes) includes).SubStatuses == IncludesLevel.Simple)
                {
                    if (item.OrderItemStatusSubStatusLinks != null)
                    {
                        item.SimpleOrderItemSubStatuses = item.OrderItemStatusSubStatusLinks
                            .Select(link => new SimpleOrderItemSubStatus()
                            {
                                BID = link.BID,
                                ID = link.SubStatusID,
                                ClassTypeID = link.OrderItemSubStatus.ClassTypeID,
                                DisplayName = link.OrderItemSubStatus.Name
                            }).ToList();

                        item.OrderItemStatusSubStatusLinks = null;
                    }

                    else
                        item.SimpleOrderItemSubStatuses = null;
                }

                if (((OrderItemStatusIncludes) includes).SubStatuses == IncludesLevel.Full)
                {
                    if (item.OrderItemStatusSubStatusLinks != null)
                    {
                        item.OrderItemSubStatuses = item.OrderItemStatusSubStatusLinks
                            .Select(link => link.OrderItemSubStatus).ToList();
                        item.OrderItemStatusSubStatusLinks = null;
                    }
                    else
                        item.OrderItemSubStatuses = null;
                }
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<OrderItemStatus> WherePrimary(IQueryable<OrderItemStatus> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(short s)
        {
            var result = await base.CanDelete(s);
            if (result?.Success == true && (result.Value.HasValue && result.Value.Value))
            {
                //var orderItemStatus = await ctx.OrderItemStatus.Where(ois => ois.ID == s && ois.Name.FirstOrDefault().Equals($"x".ToCharArray()[0])).FirstOrDefaultAsync();
                var orderItemStatus = await ctx.OrderItemStatus.Where(ois => ois.ID == s && ois.IsDefault).FirstOrDefaultAsync();
                if (orderItemStatus == null)
                {
                    return result;
                }
                return new BooleanResponse()
                {
                    Message = $"Forbidden - The Default ItemStatus can't be deleted: {orderItemStatus.Name}",
                    Success = false,
                    Value = false
                };
            }
            else
            {
                return result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(OrderItemStatus model)
        {
            //cleanup boardviews
            /* 
                sample value:
                {"IncludeAll":false,"HideEmpty":true,"PropertyName":"ItemStatusID","Columns":[{"ID":1087,"Label":"Limbo","BoardColumnType":0,"OrderStatusID":23,"isActive":true},{"ID":1078,"Label":"Desi","BoardColumnType":0,"OrderStatusID":22,"isActive":true}]} 
            */
            var allBoardViews = await this.ctx.BoardView
                                .Where(b => b.BID == this.BID && 
                                    (b.GroupBy ?? "").Contains("\"PropertyName\":\"ItemStatusID\"") &&
                                    (b.GroupBy ?? "").Contains("\"Columns\"") &&
                                    (b.GroupBy ?? "").Contains("\"ID\":"+model.ID.ToString())
                                ).ToListAsync();

            foreach (var bView in allBoardViews)
            {
                JObject groupBy = JsonConvert.DeserializeObject<JObject>(bView.GroupBy);
                groupBy["Columns"] = new JArray(
                                        (groupBy["Columns"] as JArray)
                                            .Where(col => !col["ID"].ToString().Equals(model.ID.ToString()))
                                            .ToArray()
                                    );
                bView.GroupBy = JsonConvert.SerializeObject(groupBy);
            }
          
            await base.DoBeforeDeleteAsync(model);
        }
    }
}
