﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;

namespace Endor.Api.Web.Services
{

    /// <inheritdoc />
    /// <summary>
    /// MessageBodyService
    /// </summary>
    public class MessageBodyService : AtomCRUDService<MessageBody, int>
    {

        /// <inheritdoc />
        /// <summary>
        /// constructor
        /// </summary>
        public MessageBodyService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <inheritdoc />
        /// <summary>
        /// GetChildAssociations
        /// </summary>
        protected override IChildServiceAssociation<MessageBody, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<MessageBody, int>[]{
                CreateChildAssociation<MessageParticipantInfoService, MessageParticipantInfo, int>((e) => e.MessageParticipantInfos)
            };
        }

        internal override void DoBeforeValidate(MessageBody newModel)
        {
           
        }

        /// <inheritdoc />
        /// <summary>
        /// GetIncludes
        /// </summary>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return new string[] { };
        }

        /// <inheritdoc />
        /// <summary>
        /// WherePrimary
        /// </summary>
        public override IQueryable<MessageBody> WherePrimary(IQueryable<MessageBody> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// DoAfterAddAsync
        /// </summary>
        /// <param name="model"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(MessageBody model, Guid? tempGuid){
            await base.DoAfterAddAsync(model, tempGuid);
        }
    }
}
