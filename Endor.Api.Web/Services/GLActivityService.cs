﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Service that wraps utilizes data storage layer and is accessed primarily from Controller layer
    /// </summary>
    public class GLActivityService : AtomCRUDService<ActivityGlactivity, int>
    {
        /// <summary>
        /// Constructs a new GLActivityService with a number of injeced parameters
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public GLActivityService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) :
            base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "GL" };

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<ActivityGlactivity, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<ActivityGlactivity, int>[]
            {
                CreateChildAssociation<GLDataService, GLData, int>((a)=>a.GL)
            };
        }

        internal override void DoBeforeValidate(ActivityGlactivity newModel)
        {
        }

        /// <summary>
        /// Finds the GLActivity by primary key
        /// </summary>
        /// <param name="query">GLActivity query</param>
        /// <param name="ID">ID of GlActivity to find</param>
        /// <returns></returns>
        public override IQueryable<ActivityGlactivity> WherePrimary(IQueryable<ActivityGlactivity> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Maps the Includes for GLActivity
        /// </summary>
        /// <param name="item">GLActivity to map</param>
        /// <param name="includes">Includes to map</param>
        /// <returns></returns>
        public override async Task MapItem(ActivityGlactivity item, IExpandIncludes includes = null)
        {
            if (includes != null)
            {
                if (includes.GetIncludes().ContainsKey("GL"))
                {
                    
                }
            }

            await Task.Yield();
        }

        /// <summary>
        /// Inserts the GLActivity
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="type">"final","current" or "difference"</param>
        /// <param name="user"></param>
        /// <param name="completedDate"></param>
        /// <returns></returns>
        public IActionResult GenerateGLActivity(int orderId, string type, System.Security.Claims.ClaimsPrincipal user, DateTime? completedDate)
        {
            List<GLData> glData = new List<GLData>();
            Endor.GLEngine.GLEngine gLEngine = new Endor.GLEngine.GLEngine(this.BID,this.ctx);
            switch(type)
            {
                case "final":
                    glData = gLEngine.CalculateGL(GLEngine.Models.EnumGLType.Order, orderId);
                    break;
                case "current":
                    glData = gLEngine.LoadHistoricalGLData(GLEngine.Models.EnumGLType.Order, orderId);
                    break;
                case "difference":
                    glData = gLEngine.ComputeGLDiff(GLEngine.Models.EnumGLType.Order, orderId);
                    break;
                default:
                    return new BadRequestObjectResult("Return type specified must be one of (final,current,difference)");
            }
            var glActivity = new ActivityGlactivity()
            {
                IsActive = true,
                Name = type + " Computation",
                ActivityType = (byte)ActivityType.Accounting_Entry,
                OrderID = orderId,
                CompanyID = glData.Any() ? glData.First().CompanyID : null,
                Subject = type + " Computation",
                CompletedByID = user.EmployeeID(),
                CompletedByContactID = null,
                CompletedDT = completedDate,
                GLEntryType = (byte)GLEntryType.Order_Maintenance_Recompute,

            };
            glActivity.GL = glData;
            return new OkObjectResult(glActivity);
        }
    }
}
