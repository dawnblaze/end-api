﻿using Endor.EF;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Endor.Tasks;
using Endor.Api.Web.Classes;
using Endor.Tenant;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.DocumentStorage.Models;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Destination Service
    /// </summary>
    public class DestinationService : AtomCRUDService<DestinationData, short>, ISimpleListableViewService<SimpleDestinationData, short>
    {
        /// <summary>
        /// Property to get  SimpleContactData from ApiContext
        /// </summary>
        public DbSet<SimpleDestinationData> SimpleListSet => ctx.SimpleDestinationData;

        /// <summary>
        /// Destination Service Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">ITaskQueuer</param>
        /// <param name="cache">ITenantDataCache</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DestinationService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Default Child Properties to include from Database Context
        /// </summary>
        public override string[] IncludeDefaults => new string[] {
              "Profiles"
            , "Profiles.DestinationProfileTables"
            , "Profiles.DestinationProfileVariables"
        };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Gets the Child Associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<DestinationData, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<DestinationData, short>[]
            {
                CreateChildAssociation<DestinationProfileService, DestinationProfile, int>((a) => a.Profiles)
            };
        }

        /// <summary>
        /// Actions to perform before Base class validation
        /// </summary>
        /// <param name="newModel"></param>
        internal override void DoBeforeValidate(DestinationData newModel)
        {
            
        }

        /// <summary>
        /// Destination Service Override of DoBeforeCloneAsync
        /// </summary>
        /// <param name="clone">Cloned DestinationData</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(DestinationData clone)
        {
            clone.Name = GetNextClonedName(ctx.Set<DestinationData>().AsNoTracking().Where(t => t.BID == BID && t.Name.StartsWith(t.Name)).ToList(), clone.Name, t => t.Name);
            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Lookup DestinationData by BID and ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">Destination ID</param>
        /// <returns></returns>
        public override IQueryable<DestinationData> WherePrimary(IQueryable<DestinationData> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Gets an array of Destinations for the give Template
        /// </summary>
        /// <param name="templateID">Template's ID</param>
        /// <param name="includeInactive">If Inactive Destinations should be included</param>
        /// <returns></returns>
        public async Task<DestinationData[]> GetDestinationsForTemplate(int templateID, bool includeInactive)
        {
            if (!includeInactive)
            {
                return await ctx.DestinationData
                    .Where(d => d.BID == BID && d.TemplateID == templateID && d.IsActive == true).ToArrayAsync();
            }
            else
            {
                return await ctx.DestinationData
                    .Where(d => d.BID == BID && d.TemplateID == templateID).ToArrayAsync();
            }
        }

        /// <summary>
        /// Checks if a Destination can be deleted
        /// </summary>
        /// <param name="id">Destination's ID</param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(short id)
        {
            var destination = await ctx.DestinationData.FirstOrDefaultAsync(d => d.BID == BID && d.ID == id);
            if (destination == null)
                return new BooleanResponse()
                {
                    Success = false,
                    Value = false,
                    ErrorMessage = $"Destination with ID `{id}` not found."
                };

            return new BooleanResponse()
            {
                Success = true,
                Value = true,
                Message = "Destination can be deleted."
            };
        }

        /// <summary>
        /// Actions to perform before Deleting
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override Task DoBeforeDeleteAsync(DestinationData model)
        {
            return base.DoBeforeDeleteAsync(model);
        }

        /// <summary>
        /// Actions to clean up additional related items
        /// </summary>
        /// <param name="model">Destination that's been deleted</param>
        /// <returns></returns>
        protected override async Task DoAfterDeleteAsync(DestinationData model)
        {
            // cleanup custom field values
            var customFieldValues = ctx.CustomFieldOtherData.Where(x => x.BID == BID && x.ID == model.ID && x.AppliesToClassTypeID == ClassType.Destination.ID());
            if (customFieldValues != null)
            {
                ctx.RemoveRange(customFieldValues);
                await ctx.SaveChangesAsync();
            }

            // cleanup documents
            DocumentManager client = GetDocumentManager(model.ID, ClassType.Destination, BucketRequest.Documents);
            await client.DeleteAllDocumentsAsync();

            await base.DoAfterDeleteAsync(model);
        }

        /// <summary>
        /// Machine Service Override of DoAfterCloneAsync
        /// </summary>
        /// <param name="clonedFromID">Cloned MachineData ID</param>
        /// <param name="clone">Cloned MachineData</param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(short clonedFromID, DestinationData clone)
        {
            //Copy Documents Bucket
            DocumentManager client = base.GetDocumentManager(clonedFromID, ClassType.Destination, BucketRequest.Documents);
            await client.CloneAllBucketBlobsAsync(clone.ID, Bucket.Documents, includeSpecialFolderBlobName: true);

            await CloneCustomFields(clonedFromID, clone.ID);

            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        private async Task<bool> CloneCustomFields(int oldID, int newID)
        {
            try
            {
                CustomFieldService customFieldService = new CustomFieldService(this.ctx, this.logger, this.rtmClient);
                var customFieldList = customFieldService.ReadCustomFields(this.BID, (int)ClassType.Destination, oldID, new CustomFieldValueFilters());

                if (customFieldList != null)
                    customFieldService.UpdateCustomFields(this.BID, (int)ClassType.Destination, newID, customFieldList.ToArray());

                return true;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Clone of Destination Custom Fields failed", e);
                return false;
            }
        }

        /// <summary>
        /// Set Destination to Active/Inactive
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="active"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public override async Task<EntityActionChangeResponse<short>> SetActive(short ID, bool active, string connectionID)
        {
            var destination = await GetAsync(ID);
            if (destination == null)
            {
                return new EntityActionChangeResponse<short>
                {
                    ErrorMessage = $"Destination with ID `{ID}` not found.",
                    Success = false
                };
            }

            try
            {
                destination.IsActive = active;
                destination.ModifiedDT = DateTime.UtcNow;

                ctx.Update(destination);
                await ctx.SaveChangesAsync();
                await QueueIndexForModel(ID);

                string activeStr = active ? "Active" : "Inactive";
                return new EntityActionChangeResponse<short>
                {
                    Message = $"Destination successfully set to {activeStr}.",
                    Id = ID,
                    Success = true
                };
            }
            catch (Exception ex)
            {
                await logger.Error(BID, "Error changing Destination's Active status.", ex);
                return new EntityActionChangeResponse<short>
                {
                    ErrorMessage = "Error changing Destination's Active status.",
                    Id = ID,
                    Success = false
                };
            }
        }
    }
}
