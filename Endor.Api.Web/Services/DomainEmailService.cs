﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Endor.RTM;

namespace Endor.Api.Web.Services
{
    /*
    /// <summary>
    /// Domain Email Service
    /// </summary>
    public class DomainEmailService : AtomCRUDService<DomainEmail, short>, ISimpleListableViewService<SimpleDomainEmail, short>
    {
        /// <summary>
        /// Constructs a domain email service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DomainEmailService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Gets simple list set
        /// </summary>
        public DbSet<SimpleDomainEmail> SimpleListSet => ctx.SimpleDomainEmail;


        /// <summary>
        /// Returns DomainEmails based on filters supplied in
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<List<DomainEmail>> GetWithFiltersAsync(DomainEmailFilter filters)
        {
            if (filters != null && filters.HasFilters)
            {
                return await this.GetWhere(filters.WherePredicates());
            }

            return await this.GetAsync();
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "LocationLinks", "LocationLinks.Location" };

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// Maps a specific item.
        /// </summary>
        /// <param name="item">Item to map</param>
        /// <param name="includes">Child/Parent includes</param>
        public override Task MapItem(DomainEmail item, IExpandIncludes includes = null)
        {
            if (item.LocationLinks != null)
            {
                item.SimpleLocations = item.LocationLinks
                                    .Select(link => new SimpleLocationData()
                                    {
                                        BID = link.BID,
                                        ID = link.LocationID,
                                        ClassTypeID = link.Location?.ClassTypeID ?? -1,
                                        DisplayName = link.Location?.Name
                                    }).ToList();
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<DomainEmail, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<DomainEmail, short>[0];
        }

        internal override void DoBeforeValidate(DomainEmail newModel) { }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<DomainEmail> WherePrimary(IQueryable<DomainEmail> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        internal async Task<EntityActionChangeResponse> Test(short domainEmailID, DomainEmailTestCredentials creds)
        {
            var domainEmail = await ctx.DomainEmail.FirstOrDefaultPrimaryAsync(this.BID, domainEmailID);

            if (domainEmail == null)
            {
                return new EntityActionChangeResponse
                {
                    Message = "Domain Not Found",
                    Success = false
                };
            }

            domainEmail.LastVerificationAttemptDT = DateTime.UtcNow;

            var result = await DoTest(domainEmail, creds);

            domainEmail.LastVerificationSuccess = result.Success;
            domainEmail.LastVerificationResult = result.Success ? "Success" : result.Message;
            if (result.Success)
                domainEmail.LastVerifiedDT = domainEmail.LastVerificationAttemptDT;

            ctx.SaveChanges();

            if (result.Success)
            {
                return new EntityActionChangeResponse
                {
                    Message = "Test Successful",
                    Success = true
                };
            }
            else
            {
                return new EntityActionChangeResponse
                {
                    Message = "Email Credentials or Password Not Supplied or Valid",
                    Success = false
                };
            }
        }

        private Task<DomainEmailTestResult> DoTest(DomainEmail domainEmail, DomainEmailTestCredentials creds)
        {
            return Task.FromResult(new DomainEmailTestResult() {
                Success = false,
                Message = "Not Implemented"
            });
        }

        /// <summary>
        /// links or unlinks domain email and location
        /// </summary>
        /// <param name="domainEmailID"></param>
        /// <param name="locationID"></param>
        /// <param name="makeLink">pass true to create a link, pass false to unlink</param>
        /// <returns></returns>
        internal async Task<EntityActionChangeResponse> LinkLocation(short domainEmailID, byte locationID, bool makeLink)
        {
            bool undoLink = !makeLink;

            var domainEmail = await ctx.DomainEmail.FirstOrDefaultPrimaryAsync(this.BID, domainEmailID);

            if (domainEmail == null)
            {
                return new EntityActionChangeResponse
                {
                    Message = "Domain Not Found",
                    Success = false
                };
            }

            if (domainEmail.IsForAllLocations)
            {
                return new EntityActionChangeResponse
                {
                    Id = domainEmailID,
                    Message = "This email domain is currently enabled for all Locations and may not be enabled or disabled by Location.",
                    Success = false
                };
            }

            var existingLocation = await ctx.LocationData.FirstOrDefaultPrimaryAsync(this.BID, locationID);

            if (makeLink && existingLocation == null)
            {
                return new EntityActionChangeResponse
                {
                    Message = "Location Not Found",
                    Success = false
                };
            }

            //check if it already exists
            var existingLinks = await ctx.DomainEmailLocationLink.Where(x => x.BID == this.BID && x.DomainID == domainEmailID && x.LocationID == locationID).ToArrayAsync();
            bool hasExistingLink = existingLinks != null && existingLinks.Length > 0;

            if (makeLink && hasExistingLink)
            {
                return new EntityActionChangeResponse
                {
                    Id = domainEmail.ID,
                    Message = "Domain and location already linked",
                    Success = true
                };
            }
            else if (undoLink && !hasExistingLink)
            {
                return new EntityActionChangeResponse
                {
                    Id = domainEmail.ID,
                    Message = "Domain and location already unlinked",
                    Success = true
                };
            }
            else if (makeLink && !hasExistingLink)
            {
                ctx.Add(new DomainEmailLocationLink()
                {
                    BID = domainEmail.BID,
                    DomainID = domainEmail.ID,
                    LocationID = existingLocation.ID
                });
                await ctx.SaveChangesAsync();
                await base.QueueIndexForModel(domainEmail.ID);
                return new EntityActionChangeResponse
                {
                    Id = domainEmail.ID,
                    Message = $"Successfully {(makeLink ? "linked" : "unlinked")} domain email and location",
                    Success = true
                };
            }
            else if (undoLink && hasExistingLink)
            {
                ctx.RemoveRange(existingLinks);
                await ctx.SaveChangesAsync();
                await base.QueueIndexForModel(domainEmailID);
                return new EntityActionChangeResponse
                {
                    Id = domainEmail.ID,
                    Message = $"Successfully {(makeLink ? "linked" : "unlinked")} domain email and location",
                    Success = true
                };
            }
            else
            {
                return new EntityActionChangeResponse
                {
                    Id = domainEmail.ID,
                    Message = $"Cannot {(makeLink ? "link" : "unlink")} because there is {(hasExistingLink ? "an existing":"no existing")} link",
                    Success = false
                };
            }
        }

        internal async Task<SystemEmailSMTPConfigurationType[]> GetSMTPConfigurationTypes()
        {
            return await this.ctx.SystemEmailSMTPConfigurationType.ToArrayAsync();
        }

        /// <summary>
        /// Executes after a successful Domain Email creation
        /// </summary>
        /// <param name="newModel">DomainEmail</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(DomainEmail newModel, Guid? tempGuid = null)
        {
            if (newModel.SimpleLocations != null)
            {
                foreach (var simpleLocation in newModel.SimpleLocations)
                {
                    ctx.Add(new DomainEmailLocationLink() { BID = this.BID, DomainID = newModel.ID, LocationID = simpleLocation.ID });
                }
            }

            await ctx.SaveChangesAsync();
            await base.DoAfterAddAsync(newModel, tempGuid);
        }
    }

    /// <summary>
    /// used for POST domainemail/{id}/action/test
    /// </summary>
    public class DomainEmailTestCredentials
    {
        /// <summary>
        /// email address for test
        /// </summary>
        public string emailaddress { get; set; }
        /// <summary>
        /// password for test
        /// </summary>
        public string password { get; set; }
    }

    internal class DomainEmailTestResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }*/
}
