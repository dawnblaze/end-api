﻿using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Service for retrieving System Colors
    /// </summary>
    public class SystemColorService
    {
        private readonly ApiContext ctx;

        /// <summary>
        /// Business ID
        /// </summary>
        public short BID { get; }

        /// <summary>
        /// Constructs a System Alert service with injected params
        /// </summary>
        /// <param name="context">Api context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public SystemColorService(ApiContext context, short bid, IMigrationHelper migrationHelper)
        {
            this.ctx = context;
            migrationHelper.MigrateDb(ctx);
            this.BID = bid;
        }

        /// <summary>
        /// Retrieves and returns the System Colors
        /// </summary>
        /// <returns></returns>
        public async Task<SystemColor[]> GetSystemColorsAsync()
        {
            return await ctx.SystemColor.ToArrayAsync();
        }
    }
}
