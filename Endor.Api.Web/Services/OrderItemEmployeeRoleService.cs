﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// A child service for OrderItemEmployeeRole
    /// </summary>
    public class OrderItemEmployeeRoleService : AtomCRUDChildService<OrderEmployeeRole, int, int>
    {
        /// <summary>
        /// Constructs an OrderItemEmployeeRoleService
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderItemEmployeeRoleService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// This happens before delete
        /// </summary>
        /// <param name="model"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(OrderEmployeeRole model, int parentID)
        {
            SetParentID(model, parentID);
            await base.DoBeforeDeleteAsync(model);
        }

        /// <summary>
        /// This happens before update
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="parentID"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(OrderEmployeeRole oldModel, int parentID, OrderEmployeeRole newModel)
        {
            SetParentID(newModel, parentID);
            await base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        /// <summary>
        /// Returns an array of items to expand
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return null;
        }

        /// <summary>
        /// Gets the EmployeeRoles for an OrderItem
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="includeDefaults">If includeDefaults is true then default roles from the order will be substituted in</param>
        /// <returns></returns>
        internal async Task<List<OrderEmployeeRole>> GetByParentIDWithDefaultsAsync(int parentID, bool includeDefaults)
        {
            if (!await ctx.OrderItemData.AnyAsync(t => t.BID == this.BID && t.ID == parentID))
                return null;

            var orderItemEmployeeRoles = await ctx.OrderEmployeeRole.Where(t => t.OrderItemID == parentID && t.BID == this.BID && t.IsOrderItemRole).ToListAsync();

            if (includeDefaults)
            {
                var orderEmployeeRolesNotInOrderItemEmployeeRoles =
                    await ctx.OrderItemData.Where(t => t.ID == parentID && t.BID == this.BID).Include(t => t.Order)
                    .Select(t => t.Order).Include(t => t.EmployeeRoles)
                    .SelectMany(t => t.EmployeeRoles)
                    .Where(t => t.IsOrderRole).Where(t => !orderItemEmployeeRoles.Select(r => r.RoleID).Contains(t.RoleID)).ToListAsync();

                orderItemEmployeeRoles.AddRange(orderEmployeeRolesNotInOrderItemEmployeeRoles);
            }

            return orderItemEmployeeRoles;
        }

        /// <summary>
        /// Returns the parentID
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override int GetParentID(OrderEmployeeRole model)
        {
            return model.OrderItemID.GetValueOrDefault();
        }

        /// <summary>
        /// Validates the parentID on a model against an arg
        /// </summary>
        /// <param name="model"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public override bool IsParentIDValid(OrderEmployeeRole model, int parentID)
        {
            return model.OrderItemID == parentID;
        }

        /// <summary>
        /// Sets the parentID on the model;
        /// </summary>
        /// <param name="model"></param>
        /// <param name="parentID"></param>
        public override void SetParentID(OrderEmployeeRole model, int parentID)
        {
            model.OrderItemID = parentID;
        }

        /// <summary>
        /// returns the query filtered by parentID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public override IQueryable<OrderEmployeeRole> WhereParent(IQueryable<OrderEmployeeRole> query, int parentID)
        {
            return query.Where(t => t.OrderItemID == parentID);
        }

        /// <summary>
        /// returns the query filtered by primary key
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<OrderEmployeeRole> WherePrimary(IQueryable<OrderEmployeeRole> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Returns a collection of Child Service associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<OrderEmployeeRole, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<OrderEmployeeRole, int>[0];
        }

        internal override void DoBeforeValidate(OrderEmployeeRole newModel)
        {

        }

        /// <summary>
        /// This is done before create
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="parentID"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(OrderEmployeeRole newModel, int parentID, Guid? tempGuid = null)
        {
            SetParentID(newModel, parentID);

            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }
        
        /// <summary>
        /// Sets the modified DT field to not modified since this is a temporal table
        /// </summary>
        /// <param name="newModel"></param>
        protected override void SetModifiedDT(OrderEmployeeRole newModel)
        {
            ctx.Entry(newModel).Property(x => x.ModifiedDT).IsModified = false;
        }
    }
}
