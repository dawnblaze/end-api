﻿using Endor.EF;
using System.Threading.Tasks;
using Endor.CBEL.Metadata;
using Endor.CBEL.Operators;
using Endor.CBEL.Autocomplete;

namespace Endor.Api.Web.Services
{
    public class SystemDefinitionService
    {
        private readonly ApiContext ctx;

        /// <summary>
        /// Business ID
        /// </summary>
        public short BID { get; }

        private readonly IAutocompleteEngine _autocompleteEngine;
        /// <summary>
        /// Constructs a System Alert service with injected params
        /// </summary>
        /// <param name="context">Api context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <param name="autocompleteEngine">Autocomplete Engine</param>
        public SystemDefinitionService(ApiContext context, short bid, IMigrationHelper migrationHelper, IAutocompleteEngine autocompleteEngine)
        {
            this.ctx = context;
            migrationHelper.MigrateDb(ctx);
            this.BID = bid;
            this._autocompleteEngine = autocompleteEngine;
        }

        /// <summary>
        /// returns common definitions
        /// with custom fields for current BID
        /// </summary>
        /// <returns></returns>
        public async Task<string> GetCommonDefinition()
        {
            return await this._autocompleteEngine.CommonDefinition();
        }

        /// <summary>
        /// returns common definitions of the given classtypeid
        /// </summary>
        /// <param name="classtypeid"></param>
        /// <returns></returns>
        public async Task<string> GetCommonDefinition(int classtypeid)
        {
            return await this._autocompleteEngine.CommonDefinition(classtypeid);
        }

        /// <summary>
        /// returns the hint from the given name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<string> GetHint(string name)
        {
            return await this._autocompleteEngine.Hint(name);
        }

        public async Task<CBELFunctionInfo[]> GetFunctions()
        {
            return await this._autocompleteEngine.Functions();
        }

        public async Task<CBELOperatorInfo[]> GetOperators()
        {
            return await this._autocompleteEngine.Operators();
        }
    }
}
