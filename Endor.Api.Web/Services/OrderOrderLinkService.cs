﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Endor.Api.Web.Classes;
using Endor.RTM.Models;
using Endor.RTM.Enums;
using System.Linq.Expressions;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Order Link Service Layer
    /// </summary>
    public class OrderOrderLinkService<M>: LinkCRUDService<OrderOrderLink>, ICloneableChildCRUDService<M, OrderOrderLink>, IDoBeforeCreateUpdateWithParent<OrderOrderLink, M>
        where M : TransactionHeaderData
    {
        /// <summary>
        /// Constructor for type
        /// </summary>
        public OrderOrderLinkService(ApiContext ctx, RemoteLogger logger, short bid, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(ctx, logger, bid, rtmClient, migrationHelper)
        {

        }

        /// <summary>
        /// Gets a collection of models for a given predicate
        /// </summary>
        /// <param name="predicate">Predicate to search against</param>
        /// <returns></returns>
        public async Task<List<OrderOrderLink>> GetWhere(Expression<Func<OrderOrderLink, bool>> predicate)
        {
            return await ctx.OrderOrderLink.Where(m => m.BID == BID).Where(predicate).ToListAsync();
        }

        /// <summary>
        /// logic to execute before the Create is called
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(OrderOrderLink newModel, Guid? tempGuid = null)
        {
            var order = await ctx.Set<TransactionHeaderData>().AsNoTracking().Where(t => t.BID == this.BID && t.ID == newModel.OrderID).Select(t => new { BID = t.BID, ID = t.ID, OrderNumber = t.FormattedNumber }).FirstOrDefaultAsync();
            var linkedOrder = await ctx.Set<TransactionHeaderData>().AsNoTracking().Where(t => t.BID == this.BID && t.ID == newModel.LinkedOrderID).Select(t => new { BID = t.BID, LinkedOrderID = t.ID, LinkedOrderNumber = t.FormattedNumber }).FirstOrDefaultAsync();

            if (order == null)
                throw new InvalidOperationException("Order ID was not valid");

            if (linkedOrder == null)
                throw new InvalidOperationException("Linked Order ID was not valid");

            if (String.IsNullOrWhiteSpace(newModel.Description))
                newModel.Description = GetDefaultDescription(newModel, order.OrderNumber, linkedOrder.LinkedOrderNumber);

            newModel.LinkedFormattedNumber = linkedOrder.LinkedOrderNumber;

            ctx.OrderOrderLink.Add(newModel);
            OrderOrderLink inverseLink = CreateInverseLink(newModel, linkedOrder.LinkedOrderNumber, order.OrderNumber);
            inverseLink.LinkedFormattedNumber = order.OrderNumber;

            ctx.OrderOrderLink.Add(inverseLink);

            await Task.Yield();
        }

        private string GetDefaultDescription(OrderOrderLink newModel, string orderNumber, string linkedOrderNumber)
        {
            return $"{orderNumber} is {newModel.LinkType.ToString().PascalCaseStringSpacer()} {linkedOrderNumber}";
        }

        /// <summary>
        /// Business logic on delete.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(OrderOrderLink model)
        {
            var orderLink = await this.ctx.OrderOrderLink.FirstOrDefaultAsync(t => t.BID == model.BID && t.OrderID == model.OrderID && t.LinkedOrderID == model.LinkedOrderID && t.LinkType == model.LinkType);
            this.ctx.OrderOrderLink.Remove(orderLink);
            var inverseLinkType = GetInverseLinkType(model.LinkType);
            var inverseOrderLink = await this.ctx.OrderOrderLink.FirstOrDefaultAsync(t => t.BID == model.BID && t.OrderID == model.LinkedOrderID && t.LinkedOrderID == model.OrderID && t.LinkType == inverseLinkType);
            this.ctx.OrderOrderLink.Remove(inverseOrderLink);
        }

        /// <summary>
        /// Business logic on update.
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        public override async Task DoBeforeUpdateAsync(OrderOrderLink oldModel, OrderOrderLink newModel)
        {

            if (String.IsNullOrWhiteSpace(newModel.Description))
            {
                var order = ctx.Set<M>().AsNoTracking().Where(t => t.BID == this.BID && t.ID == newModel.OrderID).Select(t => new { BID = t.BID, ID = t.ID, OrderNumber = t.FormattedNumber }).FirstOrDefault();
                newModel.Description = GetDefaultDescription(newModel, order.OrderNumber, newModel.LinkedFormattedNumber);
            }

            oldModel.Description = newModel.Description;
            ctx.Entry(oldModel).State = EntityState.Modified;
            ctx.Entry(oldModel).Property(x => x.ModifiedDT).IsModified = false;

            newModel.Description = oldModel.Description;
            newModel.EmployeeID = oldModel.EmployeeID;
            newModel.Amount = oldModel.Amount;
            newModel.BID = oldModel.BID;
            newModel.LinkedFormattedNumber = oldModel.LinkedFormattedNumber;

            await Task.Yield();
        }

        /// <summary>
        /// Gets an untracked snapshot of the model before new updates are made
        /// </summary>
        /// <param name="newModel">Updated version of the model</param>
        /// <returns></returns>
        public override async Task<OrderOrderLink> GetOldModelAsync(OrderOrderLink newModel)
        {
            var orderLink = this.ctx.OrderOrderLink.FirstOrDefault(t => t.BID == newModel.BID && t.OrderID == newModel.OrderID && t.LinkedOrderID == newModel.LinkedOrderID && t.LinkType == newModel.LinkType);
            return await Task.FromResult(orderLink);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="orderLinkInput"></param>
        /// <param name="linkType"></param>
        /// <param name="linkID"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public async Task<OrderOrderLink> UpdateAsync(int id, OrderOrderLinkInput orderLinkInput, OrderOrderLinkType linkType, int linkID, string connectionID)
        {
            var newModel = new OrderOrderLink()
            {
                OrderID = id,
                LinkedOrderID = linkID,
                LinkType = linkType,
                Description = orderLinkInput?.Description,
            };
            
            return await UpdateAsync(newModel, connectionID);
        }

        /// <summary>
        /// Sends a refresh message after updating
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public override async Task<OrderOrderLink> UpdateAsync(OrderOrderLink newModel, string connectionID)
        {
            newModel.BID = this.BID;

            var result = await base.UpdateAsync(newModel, connectionID);
            if (result != null)
            {
                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnUpdate(newModel.OrderID, newModel.LinkedOrderID) });
            }

            return result;
        }

        /// <summary>
        /// Creates an OrderLink
        /// </summary>
        /// <param name="orderID"></param>
        /// <param name="newModel"></param>
        /// <param name="linkType"></param>
        /// <param name="linkedOrderID"></param>
        /// <returns></returns>
        public async Task<OrderOrderLink> CreateAsync(int orderID, OrderOrderLinkInput newModel, OrderOrderLinkType linkType, int linkedOrderID)
        {
            OrderOrderLink tempOrderOrderLink = new OrderOrderLink()
            {
                BID = this.BID,
                OrderID = orderID,
                LinkedOrderID = linkedOrderID,
                LinkType = linkType,
                Description = newModel?.Description,
            };

            var result = await CreateAsync(tempOrderOrderLink);
            if (result != null)
            {
                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnCreate(result.OrderID, result.LinkedOrderID) });
            }

            return result;
        }

        /// <summary>
        /// Deletes orderlinks between two listed orders by type
        /// </summary>
        /// <param name="orderID"></param>
        /// <param name="linkType"></param>
        /// <param name="linkedOrderID"></param>
        /// <returns></returns>
        public async Task<BooleanResponse> DeleteAsync(int orderID, OrderOrderLinkType linkType, int linkedOrderID)
        {
            OrderOrderLink tempOrderOrderLink = new OrderOrderLink()
            {
                BID = this.BID,
                OrderID = orderID,
                LinkedOrderID = linkedOrderID,
                LinkType = linkType
            };

            bool result = await DeleteAsync(tempOrderOrderLink);

            if (result)
            {
                await this.rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnDelete(orderID, linkedOrderID) });

                return new BooleanResponse
                {
                    Value = true,
                    Success = true,
                    Message = "Successfully deleted order link.",
                };
            }

            return new BooleanResponse() { Success = true, Value = false };
        }

        private OrderOrderLink CreateInverseLink(OrderOrderLink newModel, string orderNumber, string linkedOrderNumber)
        {
            OrderOrderLink inverted = new OrderOrderLink()
            {
                BID = this.BID,
                Amount = newModel.Amount,
                EmployeeID = newModel.EmployeeID,
                LinkedOrderID = newModel.OrderID,
                OrderID = newModel.LinkedOrderID,
                LinkType = GetInverseLinkType(newModel.LinkType),
            };

            inverted.Description = GetDefaultDescription(inverted, orderNumber, linkedOrderNumber);

            return inverted;
        }

        private OrderOrderLinkType GetInverseLinkType(OrderOrderLinkType linkType)
        {
            switch (linkType)
            {
                case OrderOrderLinkType.ClonedFrom:
                    return OrderOrderLinkType.ClonedTo;
                case OrderOrderLinkType.ClonedTo:
                    return OrderOrderLinkType.ClonedFrom;
                case OrderOrderLinkType.ConvertedFrom:
                    return OrderOrderLinkType.ConvertedTo;
                case OrderOrderLinkType.ConvertedTo:
                    return OrderOrderLinkType.ConvertedFrom;
                case OrderOrderLinkType.MasterOrder:
                    return OrderOrderLinkType.ChildOrder;
                case OrderOrderLinkType.ChildOrder:
                    return OrderOrderLinkType.MasterOrder;
                case OrderOrderLinkType.CreditMemoAppliedFrom:
                    return OrderOrderLinkType.CreditMemoAppliedTo;
                case OrderOrderLinkType.CreditMemoAppliedTo:
                    return OrderOrderLinkType.CreditMemoAppliedFrom;
                case OrderOrderLinkType.PrimaryVariationFor:
                    return OrderOrderLinkType.AlternateVariationOf;
                case OrderOrderLinkType.AlternateVariationOf:
                    return OrderOrderLinkType.PrimaryVariationFor;
                default:
                    return default(OrderOrderLinkType);
            }
        }
        /// <summary>
        /// Gets a list of delete refresh messages
        /// </summary>
        /// <param name="orderID"></param>
        /// <param name="linkedOrderID"></param>
        /// <returns></returns>
        public List<RefreshEntity> DoGetRefreshMessagesOnDelete(int orderID, int linkedOrderID)
        {
            return new List<RefreshEntity>()
            {
                GetRefreshMessage(orderID, RefreshMessageType.Delete),
                GetRefreshMessage(linkedOrderID, RefreshMessageType.Delete),
            };
        }
        /// <summary>
        /// Gets a list of update refresh messages
        /// </summary>
        /// <param name="orderID"></param>
        /// <param name="linkedOrderID"></param>
        /// <returns></returns>
        public List<RefreshEntity> DoGetRefreshMessagesOnUpdate(int orderID, int linkedOrderID)
        {
            return new List<RefreshEntity>()
            {
               GetRefreshMessage(orderID, RefreshMessageType.Change),
                GetRefreshMessage(linkedOrderID, RefreshMessageType.Change),

            };
        }

        /// <summary>
        /// Gets a list of new refresh messages
        /// </summary>
        /// <param name="orderID"></param>
        /// <param name="linkedOrderID"></param>
        /// <returns></returns>
        public List<RefreshEntity> DoGetRefreshMessagesOnCreate(int orderID, int linkedOrderID)
        {
            return new List<RefreshEntity>()
            {
                GetRefreshMessage(orderID, RefreshMessageType.New),
                GetRefreshMessage(linkedOrderID, RefreshMessageType.New),
            };
        }

        private RefreshEntity GetRefreshMessage(int orderID, RefreshMessageType refreshType)
        {
            return new RefreshEntity()
            {
                BID = this.BID,
                ClasstypeID = ClassType.Order.ID(),
                ID = orderID,
                DateTime = DateTime.UtcNow,
                RefreshMessageType = refreshType,
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentModel"></param>
        /// <param name="childModel"></param>
        /// <returns></returns>
        public Task DoBeforeCloneForParent(M parentModel, OrderOrderLink childModel)
        {
            return Task.FromResult<OrderOrderLink>(null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public void DoBeforeCreateWithParent(OrderOrderLink child, M parent)
        {
            child.OrderID = parent.ID;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        public void DoBeforeUpdateWithParent(OrderOrderLink child, M oldParent, M newParent)
        {
            child.OrderID = newParent.ID;
        }
    }

    /// <summary>
    /// OrderOrderLinkInput
    /// </summary>
    public class OrderOrderLinkInput
    {
        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }
    }
}
