﻿using Endor.EF;
using Endor.Models;
// using Microsoft.EntityFrameworkCore.Extensions.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class EnumMaterialCostingMethodService : BaseGenericService
    {
        /// <summary>
        /// Constructor to inject an ApiContext
        /// </summary>
        /// <param name="context"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public EnumMaterialCostingMethodService(ApiContext context, IMigrationHelper migrationHelper) : base(context, migrationHelper)
        {
        }

        /// <summary>
        /// Gets a list of EnumMaterialCostingMethods
        /// </summary>
        /// <returns></returns>
        public async Task<List<EnumMaterialCostingMethod>> Get()
        {
            return await Task.FromResult(this.ctx.EnumMaterialCostingMethod.ToList());
        }
    }
}
