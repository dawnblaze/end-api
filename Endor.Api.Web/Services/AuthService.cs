﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.RTM;
using Endor.Api.Web.Classes;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Threading;
using Microsoft.Data.SqlClient;
using System.Text;
using Endor.DocumentStorage.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Endor.RTM.Models;
using Endor.RTM.Enums;


namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Business Service
    /// </summary>
    public class AuthService
    {

        private readonly ApiContext _ctx;
        private readonly IRTMPushClient _rtmClient;
        private readonly ITaskQueuer _taskQueuer;

        /// <summary>
        /// Business ID
        /// </summary>
        public short BID { get; }

        /// <summary>
        /// Constructs a UserLink service
        /// </summary>
        /// <param name="context">Api context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public AuthService(ApiContext context, short bid, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, IMigrationHelper migrationHelper)
        {
            BID = bid;
            _ctx = context;
            migrationHelper.MigrateDb(_ctx);
            _rtmClient = rtmClient;
            _taskQueuer = taskQueuer;
        }
        public async Task<List<UserLink>> GetAllSupportUsers()
        {
            List<UserLink> lstUsers = await this._ctx.UserLink.Where(x => x.BID == this.BID && x.EmployeeID == null && x.ContactID == null).AsNoTracking().ToListAsync();
            return lstUsers;
        }

        public async Task<EntityActionChangeResponse<short>> AddNewSupportUser(string UserName, string DisplayName, byte AccessType, EndorOptions options)
        {
            var inviteBody = new GenerateInviteRequest()
            {
                BID = this.BID,
                IsEmployee = true,
                SuggestedEmail = UserName,
                DisplayName = DisplayName,
                HasImage = false,
                ImageLocation = string.Empty
            };

            GenerateInviteResponse inviteResp = null;

            using (var client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, options.AuthOrigin + "/User/GenerateInviteForExistingUser");
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToBase64String(Encoding.UTF8.GetBytes(options.TenantSecret)));
                request.Content = new StringContent(JsonConvert.SerializeObject(inviteBody), Encoding.UTF8, "application/json");

                HttpResponseMessage response = await client.SendAsync(request);
                string responseString = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        Id = -1,
                        ErrorMessage = "Unauthorized",
                    };

                if (response.StatusCode != HttpStatusCode.OK)
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        Id = -1,
                        ErrorMessage = responseString,
                    };

                inviteResp = JsonConvert.DeserializeObject<GenerateInviteResponse>(responseString);
            }

            var oldLinks = _ctx.UserLink.Where(l => l.BID == this.BID && l.UserName == UserName && l.EmployeeID == null && l.ContactID == null);
            _ctx.UserLink.RemoveRange(oldLinks);

            UserLink newlink = new UserLink();

            newlink.ID = await RequestIDIntegerAsync(this.BID, (int)ClassType.UserLink);
            newlink.BID = this.BID;
            newlink.EmployeeID = null;
            newlink.TempUserID = inviteResp.ID;
            newlink.UserAccessType = (UserAccessType)AccessType;
            newlink.RightsGroupListID = 1000; //Default for Full Access Admin - Requires END-10658 migration which add the defaults to the BIDs.
            newlink.UserName = UserName;
            newlink.DisplayName = DisplayName;

            _ctx.UserLink.Add(newlink);
            await _ctx.SaveChangesAsync();

            return new EntityActionChangeResponse<short>()
            {
                Success = true,
                Id = newlink.ID,
                Message = inviteResp.ID.ToString()
            };
        }

        private async Task<short> RequestIDIntegerAsync(short bid, int classtypeID)
        {
            string sqlExecute = "EXEC @Result = dbo.[Util.ID.GetID] @BID, @ClassTypeID, @Count";

            List<object> myParams = new List<object>(){
                new SqlParameter("@BID", bid),
                new SqlParameter("@ClassTypeID", classtypeID),
                new SqlParameter("@Count", 1)
            };

            SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
            {
                Direction = System.Data.ParameterDirection.Output
            };
            myParams.Add(resultParam);

            await _ctx.Database.ExecuteSqlRawAsync(sqlExecute, parameters: myParams);

            return (short)Convert.ToInt32(resultParam.Value);
        }
    }
}
