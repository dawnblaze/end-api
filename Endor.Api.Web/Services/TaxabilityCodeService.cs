﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Service that wraps utilizes data storage layer and is accessed primarily from Controller layer
    /// </summary>
    public class TaxabilityCodeService : AtomCRUDService<TaxabilityCode, short>, ISimpleListableViewService<SimpleTaxabilityCode, short>
    {
        /// <summary>
        /// Constructs a new TaxabilityCodeService with a number of injeced parameters
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public TaxabilityCodeService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) :
            base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "TaxabilityCodeItemExemptionLinks", "TaxabilityCodeItemExemptionLinks.TaxItem" };

        /// <summary>
        /// SimpleTaxabilityCode list
        /// </summary>
        public DbSet<SimpleTaxabilityCode> SimpleListSet => ctx.SimpleTaxabilityCodes;

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<TaxabilityCode, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<TaxabilityCode, short>[]
            {
                //CreateChildAssociation<TaxabilityCodeService, TaxabilityCode, short>((a)=>a.SubGroups)
            };
        }

        internal override void DoBeforeValidate(TaxabilityCode newModel)
        {
        }

        /// <summary>
        /// Overridden CanDelete for Taxability Code
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(short id)
        {
            var target = await ctx.TaxabilityCodes.FirstOrDefaultAsync(t => t.BID == this.BID && t.ID == id);
            if (target == null)
                return new BooleanResponse() { Success = true, Value = true, Message = "Not found" };

            if (target.IsSystem)
                return new BooleanResponse() { Success = true, Value = false };

            return await base.CanDelete(id);
        }

        /// <summary>
        /// Adds a new GLAccount to a newly created TaxabilityCode
        /// </summary>
        /// <param name="newModel">TaxabilityCode</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(TaxabilityCode newModel, Guid? tempGuid = null)
        {
            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// Sets the TaxabilityCode's GLAccount status and Name to equal to update TaxabilityCode 
        /// </summary>
        /// <param name="oldModel">old TaxabilityCode</param>
        /// <param name="newModel">new TaxabilityCode</param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(TaxabilityCode oldModel, TaxabilityCode newModel)
        {
            await base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        /// <summary>
        /// Removes the tied GLAccount before deleting the TaxabilityCode 
        /// </summary>
        /// <param name="newModel">new TaxabilityCode</param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(TaxabilityCode newModel)
        {
            List<short> linkedIds = new List<short>();

            if (newModel.TaxabilityCodeItemExemptionLinks == null)
                newModel.TaxabilityCodeItemExemptionLinks = await ctx.TaxabilityCodeItemExemptionLinks.Where(l => l.BID == BID && l.TaxCodeID == newModel.ID).ToListAsync();

            if (newModel.TaxabilityCodeItemExemptionLinks == null && newModel.TaxabilityCodeItemExemptionLinks.Count > 0)
                linkedIds = newModel.TaxabilityCodeItemExemptionLinks.Select(l => l.TaxItemID).ToList();

            foreach (var linkedId in linkedIds)
            {
                var delLink = await this.UnlinkTaxItem(newModel.ID, linkedId);
                if (delLink.HasError)
                    throw new Exception(delLink.ErrorMessage);
            }

            newModel.TaxabilityCodeItemExemptionLinks = null; // these are gone, don't let EF get confused about it being otherwise :)
            await base.DoBeforeDeleteAsync(newModel);
        }

        /// <summary>
        /// Appends a string before cloning a TaxGroup
        /// </summary>
        /// <param name="clone">new TaxabilityCode</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(TaxabilityCode clone)
        {
            clone.Name = clone.Name + " (Clone)";
            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Recomputes the TaxRate after TaxabilityCode creation
        /// </summary>
        /// <param name="model">TaxabilityCode</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(TaxabilityCode model, Guid? tempGuid)
        {
            await base.DoAfterAddAsync(model, tempGuid);
        }

        /// <summary>
        /// Recomputes the TaxRate before applying TaxabilityCode change
        /// </summary>
        /// <param name="oldModel">old TaxabilityCode</param>
        /// <param name="model">new TaxabilityCode</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(TaxabilityCode oldModel, TaxabilityCode model, string connectionID)
        {
            await base.DoAfterUpdateAsync(oldModel, model, connectionID);
        }

        /// <summary>
        /// Builds a query with primary filters
        /// </summary>
        /// <param name="query">TaxabilityCode query</param>
        /// <param name="ID">TaxabilityCode ID</param>
        /// <returns></returns>
        public override IQueryable<TaxabilityCode> WherePrimary(IQueryable<TaxabilityCode> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Links a TaxItem into Taxability Code
        /// </summary>
        /// <param name="taxCodeId">Taxability Code ID</param>
        /// <param name="taxItemId">Tax Item ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkTaxItem(short taxCodeId, short taxItemId)
        {
            var taxCode = await ctx.TaxabilityCodes.FirstOrDefaultAsync(x => x.BID == this.BID && x.ID == taxCodeId);
            if (taxCode == null)
                return new EntityActionChangeResponse()
                {
                    ErrorMessage = "Taxability Code not found.",
                    Success = false
                };

            if (!await ctx.TaxItem.AnyAsync(x => x.BID == this.BID && x.ID == taxItemId))
                return new EntityActionChangeResponse()
                {
                    ErrorMessage = "Tax Item not found.",
                    Success = false
                };

            bool hasLink = await ctx.TaxabilityCodeItemExemptionLinks.AnyAsync(x => x.BID == this.BID && x.TaxCodeID == taxCodeId && x.TaxItemID == taxItemId);

            if (!hasLink)
            {
                TaxabilityCodeItemExemptionLink newLink = new TaxabilityCodeItemExemptionLink()
                {
                    BID = this.BID,
                    TaxCodeID = taxCodeId,
                    TaxItemID = taxItemId,
                };

                ctx.TaxabilityCodeItemExemptionLinks.Add(newLink);
                await ctx.SaveChangesAsync();

                await QueueIndexForModel(taxCodeId);
                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnUpdate(taxCode) });
            }

            return new EntityActionChangeResponse()
            {
                Id = taxCodeId,
                Message = "Successfully linked the Taxability Code to the Tax Item.",
                Success = true
            };
        }

        /// <summary>
        /// Unlinks a TaxItem into Taxability Code
        /// </summary>
        /// <param name="taxCodeId">Taxability Code ID</param>
        /// <param name="taxItemId">Tax Item ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> UnlinkTaxItem(short taxCodeId, short taxItemId)
        {
            var taxCode = await ctx.TaxabilityCodes.FirstOrDefaultAsync(x => x.BID == this.BID && x.ID == taxCodeId);
            if (taxCode == null)
                return new EntityActionChangeResponse()
                {
                    ErrorMessage = "Taxability Code not found.",
                    Success = false
                };

            if (!await ctx.TaxItem.AnyAsync(x => x.BID == this.BID && x.ID == taxItemId))
                return new EntityActionChangeResponse()
                {
                    ErrorMessage = "Tax Item not found.",
                    Success = false
                };

            TaxabilityCodeItemExemptionLink foundLink = await ctx.TaxabilityCodeItemExemptionLinks.FirstOrDefaultAsync(x => x.BID == this.BID && x.TaxCodeID == taxCodeId && x.TaxItemID == taxItemId);

            if (foundLink != null)
            {
                ctx.TaxabilityCodeItemExemptionLinks.Remove(foundLink);
                await ctx.SaveChangesAsync();

                await QueueIndexForModel(taxCodeId);
                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnUpdate(taxCode) });
            }

            return new EntityActionChangeResponse()
            {
                Id = taxCodeId,
                Message = "Successfully unlinked the Taxability Code from the Tax Item.",
                Success = true
            };
        }

        /// <summary>
        /// Maps properties in a collection of TaxabilityCodes
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override Task<List<TaxabilityCode>> MapCollection(List<TaxabilityCode> collection, IExpandIncludes includes = null)
        {
            foreach (var tc in collection)
                MapItem(tc);

            return base.MapCollection(collection, includes);
        }

        /// <summary>
        /// Maps properties in a single TaxabilityCode
        /// </summary>
        /// <param name="item"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override Task MapItem(TaxabilityCode item, IExpandIncludes includes = null)
        {
            item.TaxItems = item.TaxabilityCodeItemExemptionLinks.Select(l => l.TaxItem).ToList();
            return base.MapItem(item, includes);
        }
    }
}
