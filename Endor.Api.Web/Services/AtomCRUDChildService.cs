﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Base class for services that have a parentID that is used to request
    /// </summary>
    /// <typeparam name="M"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="PI"></typeparam>
    public abstract class AtomCRUDChildService<M, I, PI> : AtomCRUDService<M, I>
        where M : class, IAtom<I>, IAtom
        where I : struct, IConvertible
        where PI : struct, IConvertible
    {

        /// <summary>
        /// Constructor for AtomCRUDChildService
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public AtomCRUDChildService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Determine if Parent ID is Valid
        /// </summary>
        public abstract bool IsParentIDValid(M model, PI parentID);

        /// <summary>
        /// Called before create
        /// </summary>
        public abstract Task DoBeforeCreateAsync(M newModel, PI parentID, Guid? tempGuid = null);

        /// <summary>
        /// Called before update
        /// </summary>
        public abstract Task DoBeforeUpdateAsync(M oldModel, PI parentID, M newModel);

        /// <summary>
        /// Called before delete
        /// </summary>
        public abstract Task DoBeforeDeleteAsync(M model, PI parentID);

        /// <summary>
        /// Filter for objects with a given parent
        /// </summary>
        public abstract IQueryable<M> WhereParent(IQueryable<M> query, PI parentID);
        /// <summary>
        /// Get Parent ID
        /// </summary>
        public abstract PI GetParentID(M model);
        /// <summary>
        /// Set Parent ID
        /// </summary>
        public abstract void SetParentID(M model, PI parentID);



        /// <summary>
        /// Get objects by ParentID
        /// </summary>
        public async Task<List<M>> GetByParentIDAsync(PI parentID, IExpandIncludes includes = null)
        {
            var includedQ = ctx.Set<M>().IncludeAll(GetIncludes(includes)).Where(m => m.BID == BID);
            List<M> results = await WhereParent(includedQ, parentID).ToListAsync();

            return await this.MapCollection(results, includes);
        }

        /// <summary>
        /// Update
        /// </summary>
        public override async Task<M> UpdateAsync(M newModel, string connectionID)
        {
            try
            {
                PI parentID = GetParentID(newModel);

                M oldModel = await GetOldModelAsync(newModel);

                await DoBeforeUpdateAsync(oldModel, parentID, newModel);

                await ctx.SaveChangesAsync();

                await DoAfterUpdateAsync(oldModel, newModel, connectionID);

                return newModel;
            }
            catch (Exception e)
            {
                return await HandleUpdateException(e);
            }
        }

        /// <summary>
        /// Create
        /// </summary>
        public override async Task<M> CreateAsync(M newModel, Guid? tempGuid = null)
        {
            try
            {
                PI parentID = GetParentID(newModel);

                await DoBeforeCreateAsync(newModel, parentID, tempGuid);

                await ctx.SaveChangesAsync();

                await DoAfterAddAsync(newModel, tempGuid);

                return newModel;
            }
            catch (Exception e)
            {
                return await HandleCreateException(e);
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        public override async Task<bool> DeleteAsync(M model)
        {
            try
            {
                PI parentID = GetParentID(model);

                await DoBeforeDeleteAsync(model, parentID);

                bool result = (await ctx.SaveChangesAsync()) > 0;

                await DoAfterDeleteAsync(model);

                return result;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Delete failed", e);
                return false;
            }
        }

        #region static factory functions
        /// <summary>
        /// Sets up a lazy service from the types in this service given static values
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="ctx"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="logger"></param>
        /// <param name="cache"></param>
        /// <param name="rtmClient"></param>
        /// <returns></returns>
        public static Lazy<S> CreateService<S>(ApiContext ctx, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, short bid, IRTMPushClient rtmClient)
            where S : AtomCRUDChildService<M, I, PI>
        {
            //if you change this, change the function version too
            return new Lazy<S>(() => Activator.CreateInstance(typeof(S), ctx, bid, taskQueuer, cache, logger, rtmClient) as S);
        }

        /// <summary>
        /// Creates a lazy service from the types in this service, given a value and a BID function
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="ctx"></param>
        /// <param name="bidFunc"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="rtmClient"></param>
        /// <returns></returns>
        public static Lazy<S> CreateService<S>(ApiContext ctx, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, Func<short> bidFunc, IRTMPushClient rtmClient)
            where S : AtomCRUDChildService<M, I, PI>
        {
            //if you change this, change the non-function version too
            return new Lazy<S>(() => Activator.CreateInstance(typeof(S), ctx, bidFunc(), taskQueuer, cache, logger, rtmClient) as S);
        }
        #endregion
    }
}
