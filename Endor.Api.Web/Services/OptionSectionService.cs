﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Endor.Api.Web.Classes;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// A service for <see cref="SystemOptionSection"/>
    /// </summary>
    public class OptionSectionService : AtomGenericService<SystemOptionSection, int>
    {
        /// <summary>
        /// ctor for OptionSectionService
        /// </summary>
        /// <param name="context"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <returns></returns>
        public OptionSectionService(ApiContext context, IMigrationHelper migrationHelper) : base(context, migrationHelper)
        {
        }

        /// <summary>
        /// Get includes for model
        /// </summary>
        /// <returns></returns>
        public override string[] GetIncludes()
        {
            return new string[]
            {
                "Parent",
                "Children"
            };
        }

        /// <summary>
        /// Gets SystemOptionSection by ID
        /// </summary>
        /// <param name="id">ID</param>
        public Task<SystemOptionSection> Get(int id)
        {
            return ctx.SystemOptionSection.IncludeAll<SystemOptionSection>(GetIncludes()).Where(a => a.ID == id).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Gets Lists of SystemOptionSection
        /// </summary>
        public Task<List<SystemOptionSection>> Get()
        {
            return ctx.SystemOptionSection.IncludeAll<SystemOptionSection>(GetIncludes()).ToListAsync();
        }

        /// <summary>
        /// Gets Section Value by Section ID and Section Name
        /// </summary>
        /// <param name="sectionID">Section ID</param>
        /// <param name="sectionName">Section Name</param>
        public async Task<GetSectionValueResponse> Get(
            int? sectionID,
            string sectionName
            )
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@SectionID", SqlDbType = System.Data.SqlDbType.Int, Value = (object)sectionID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@SectionName", SqlDbType = System.Data.SqlDbType.NVarChar, Value = (String.IsNullOrWhiteSpace(sectionName) ? DBNull.Value : (object)sectionName) },
                };

                var rowResult = await ctx.Database.GetModelFromQueryAsync<OptionSectionReturnValue>(@"EXEC dbo.[Option.GetSection] 
                    @SectionID,@SectionName;", parameters: sp.ToArray());

                if (rowResult.Count() == 0)
                {
                    return new GetSectionValueResponse
                    {
                        Value = null,
                        Message = "No result found.",
                        Success = true
                    };
                }
                else
                {
                    //Need to convert results to heirarchy list...
                    SystemOptionSection ReturnValue = new SystemOptionSection();
                    List<OptionSectionReturnValue> ReturnValueList = rowResult.ToList();
                    foreach (OptionSectionReturnValue osrv in ReturnValueList)
                    {
                        if (osrv.Level == 0)
                        {
                            ReturnValue.ID = osrv.ID;
                            ReturnValue.Name = osrv.Name;
                            ReturnValue = BuildOptionSectionTree(ReturnValue, 1, osrv.ID, ReturnValueList);
                            continue;
                        }
                    }

                    return new GetSectionValueResponse
                    {
                        Value = ReturnValue,
                        Message = "Successfully retrieved Option Sections.",
                        Success = true
                    };
                }
            }
            catch (Exception ex)
            {
                return new GetSectionValueResponse
                {
                    Value = null,
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        private SystemOptionSection BuildOptionSectionTree(SystemOptionSection OptionSectionValue, int Level, int parentID, List<OptionSectionReturnValue> ReturnValueList)
        {
            foreach (OptionSectionReturnValue osrv in ReturnValueList)
            {
                if ((osrv.Level == Level) && (osrv.ParentID == parentID))
                {
                    if (osrv.LevelType.ToUpper() == "SECTION")
                    {
                        SystemOptionSection newSection = new SystemOptionSection();
                        newSection.ID = osrv.ID;
                        newSection.Name = osrv.Name;
                        newSection = BuildOptionSectionTree(newSection, Level + 1, osrv.ID, ReturnValueList);

                        OptionSectionValue.ChildSections.Add(newSection);
                    }

                    if (osrv.LevelType.ToUpper() == "CATEGORY")
                    {
                        SystemOptionCategory newCategory = new SystemOptionCategory();
                        newCategory.ID = osrv.ID;
                        newCategory.Name = osrv.Name;
                        newCategory.Description = osrv.Description;

                        OptionSectionValue.SystemOptionCategories.Add(newCategory);
                    }
                }
            }

            return OptionSectionValue;
        }

        private class OptionSectionReturnValue
        {
            //LevelType, ID, Level, Name, Description, ParentID, ImageName
            public string LevelType { get; set; }
            public short ID { get; set; }
            public byte Level { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public short ParentID { get; set; }
            public string ImageName { get; set; }
        }
    }
}
