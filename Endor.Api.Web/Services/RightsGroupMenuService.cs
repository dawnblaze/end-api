﻿using Endor.EF;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
// using Microsoft.EntityFrameworkCore.Extensions.Internal;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class RightsGroupMenuService : BaseGenericService
    {
        /// <summary>
        /// Constructor to inject an ApiContext
        /// </summary>
        /// <param name="context"></param>
        /// <param name="migrationHelper">MigrationHelper</param>
        public RightsGroupMenuService(ApiContext context, IMigrationHelper migrationHelper) 
            : base(context, migrationHelper)
        {
        }

        /// <summary>
        /// Gets a nested JSON array of RightsGroupMenuTree 
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Get(short BID, int userID)
        {
            var userLink = await ctx.UserLink.FirstOrDefaultAsync(x => x.BID == BID && x.AuthUserID == userID);
            bool allowIsInternal = userLink.UserAccessType > UserAccessType.SupportStaff;

            List<RightsGroupMenuTree> results = new List<RightsGroupMenuTree>();

            //pull all the rights
            var dict = await ctx.RightsGroupMenuTree.ToDictionaryAsync(x => x.ID);
            foreach(RightsGroupMenuTree node in dict.Values)
            {
                if (node.IsInternal && !allowIsInternal)
                {
                    continue;
                }
                else if (node.ParentID.HasValue)
                {
                    if (dict.ContainsKey(node.ParentID.Value))
                    {
                        var parent = dict[node.ParentID.Value];
                        if (parent.Children == null)
                            parent.Children = new List<RightsGroupMenuTree>();
                        parent.Children.Add(node);
                    }
                }
                else
                {
                    results.Add(node);
                }
            }

            if (results == null)
                return new NotFoundResult();
            else
                return new OkObjectResult(results);
        }

        /// <summary>
        /// Gets a list of Module Types
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> GetRelatedModules(short menuID)
        {
            List<short> results = new List<short>();

            using (var conn = ctx.Database.GetDbConnection())
            {
                try
                {
                    var command = conn.CreateCommand();
                    DbParameter menuIDParam = command.CreateParameter();
                    menuIDParam.DbType = System.Data.DbType.Int16;
                    menuIDParam.ParameterName = "MenuID";
                    menuIDParam.Value = menuID;

                    List<DbParameter> dbParams = new List<DbParameter>();
                    dbParams.Add(menuIDParam);

                    command.CommandText = $"SELECT DISTINCT [Module] from [System.Rights.Group.Menu.Tree] where EnabledGroupID = @MenuID";
                    command.Parameters.AddRange(dbParams.ToArray());

                    await conn.OpenAsync();
                    using (DbDataReader reader = await command.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            results.Add(reader.GetInt16(0));
                        }
                    }
                }
                catch (Exception ex)
                {
                    return new BadRequestObjectResult(ex.ToString());
                }
            }

            if (results == null)
                return new NotFoundResult();
            else
                return new OkObjectResult(results);
        }
    }
}
