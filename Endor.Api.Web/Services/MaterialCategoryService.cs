﻿using Endor.EF;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Endor.Tasks;
using Endor.Api.Web.Classes;
using Endor.Tenant;
using Endor.Logging.Client;
using Endor.RTM;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// A service for <see cref=" MaterialCategory"/>
    /// </summary>
    public class MaterialCategoryService : AtomCRUDService<MaterialCategory, short>, ISimpleListableViewService<SimpleMaterialCategory, short>
    {

        /// <summary>
        /// Constructs a Material Category service with injected params
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public MaterialCategoryService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Gets simple list set
        /// </summary>
        public DbSet<SimpleMaterialCategory> SimpleListSet => ctx.SimpleMaterialCategory;

        //public override string[] IncludeDefaults { get; } = { "MaterialCategoryLinks" };

        /// <summary>
        /// Get includes for model
        /// </summary>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Gets the Child Associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<MaterialCategory, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<MaterialCategory, short>[]
            {
                CreateChildAssociation<MaterialService, MaterialData, int>(lc=>lc.Materials)
            };
        }

        internal override void DoBeforeValidate(MaterialCategory model)
        {

        }

        /// <summary>
        /// Business logic to do after adding the new model.
        /// </summary>
        /// <param name="model">Model that was added</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(MaterialCategory model, Guid? tempGuid)
        {
           await base.DoAfterAddAsync(model, tempGuid);
           await this.LinkOrUnlinkParts(model);
        }        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(MaterialCategory oldModel, MaterialCategory newModel, string connectionID)
        {
            await base.DoAfterUpdateAsync(oldModel, newModel, connectionID);
            await this.LinkOrUnlinkParts(newModel);
        }        

        /// <summary>
        /// Changes clone name
        /// </summary>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override Task DoBeforeCloneAsync(MaterialCategory clone)
        {
            //clone.Name = GetNextClonedName(clone.Name, t => t.Name);
            clone.Name = GetNextClonedName(ctx.Set<MaterialCategory>().AsNoTracking().Where(t => t.BID == BID && t.Name.StartsWith(t.Name)).ToList(), clone.Name, t => t.Name);
            return base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Clone links as well
        /// </summary>
        /// <param name="clonedFromID">Material Category ID we Cloned from</param>
        /// <param name="clone">Cloned Material Category</param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(short clonedFromID, MaterialCategory clone)
        {
            var cloneFromLinks = await ctx.MaterialCategoryLink.Where(l => l.BID == this.BID && l.CategoryID == clonedFromID).ToListAsync();
            if (cloneFromLinks != null && cloneFromLinks.Count > 0)
            {
                List<MaterialCategoryLink> newLinks = new List<MaterialCategoryLink>();
                foreach (var link in cloneFromLinks)
                {
                    newLinks.Add(new MaterialCategoryLink()
                    {
                        BID = this.BID,
                        CategoryID = clone.ID,
                        PartID = link.PartID
                    });
                }
                ctx.MaterialCategoryLink.AddRange(newLinks);
                await ctx.SaveChangesAsync();
            }
            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        /// <summary>
        /// Returns a list of `MaterialCategory` that shares the same name as the `model`
        /// </summary>
        /// <param name="model">`MaterialCategory` model to check Name from</param>
        public List<MaterialCategory> ExistingNameOwners(MaterialCategory model)
        {
            return this.ctx.MaterialCategory
                .Where(category=> 
                        category.BID==this.BID 
                        && category.Name.Trim().Equals(model.Name.Trim())
                        && category.ID!=model.ID)
                .ToList();
        }

        /// <summary>
        /// Link Material by Category ID and Material ID
        /// </summary>
        /// <param name="materialCategoryID">Material Category ID</param>
        /// <param name="materialDataID">Material ID</param>
        /// <param name="isLinked">Is Linked</param>
        /// <param name="indexModel"></param>
        public async Task<EntityActionChangeResponse> LinkMaterial(short materialCategoryID, int materialDataID, bool isLinked, bool indexModel = true)
        {
            /*          
                @BID
                @MaterialCategoryID
                @MaterialDataID
                @IsLinked
                @Result Output
            */
            try
            {
                SqlParameter Result = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                Result.Direction = System.Data.ParameterDirection.Output;

                object[] myParams = {
                    new SqlParameter("@BID", this.BID),
                    new SqlParameter("@MaterialCategoryID", materialCategoryID),
                    new SqlParameter("@MaterialDataID", materialDataID),
                    new SqlParameter("@IsLinked", isLinked),
                    Result
                };

                int affectedRows = await ctx.Database.ExecuteSqlRawAsync(
                    "EXEC dbo.[Part.Material.Category.Action.LinkMaterial] @BID, @MaterialCategoryID, @MaterialDataID, @IsLinked, @Result Output;",
                    parameters: myParams);

                if (affectedRows > 0)
                {
                    // NOTE: prevent entity category from indexing on entity update
                    if (indexModel)
                    {
                        await QueueIndexForModel(materialCategoryID);
                    }

                    return new EntityActionChangeResponse()
                    {
                        Id = materialCategoryID,
                        Message = $"Successfully {(isLinked ? "linked" : "unlinked")} MaterialCategory:{materialCategoryID} to MaterialData:{materialDataID}",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = materialCategoryID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }

            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        /// <summary>
        /// UnlinkAll by Material Category ID
        /// </summary>
        /// <param name="materialCategoryID">Material Category ID</param>
        public async Task unlinkAllAsync(short materialCategoryID)
        {
            var existingLinks = this.ctx.MaterialCategoryLink.Where(link => link.CategoryID == materialCategoryID);
            if (existingLinks.Count() > 0)
            {
                foreach (var link in existingLinks)
                {
                    this.ctx.MaterialCategoryLink.Remove(link);
                }
                await ctx.SaveChangesAsync();
            }
        }

        //-------------unmapped items----------------

        /// <summary>
        /// Maps the Includes for material category
        /// </summary>
        /// <param name="item">Material Category</param>
        /// <param name="includeExpander">Child/Parent includes</param>
        /// <returns></returns>
        public override async Task MapItem(MaterialCategory item, IExpandIncludes includeExpander = null)
        {
            var includes = includeExpander != null ? includeExpander.GetIncludes() : null;
            if (includes != null)
            {
                //var materialDataIDs = item.MaterialCategoryLinks.Select(link=>link.PartID).ToArray();
                if (item.MaterialCategoryLinks?.Count > 0)
                {
                    switch (includes[MaterialCategoryIncludes.MaterialPartsKey].Level)
                    {
                        case IncludesLevel.Full:
                            item.Materials = await Task.FromResult(item.MaterialCategoryLinks.Select(link => link.Material).ToList());
                            break;
                        case IncludesLevel.Simple:
                            item.SimpleMaterials = await Task.FromResult(item.MaterialCategoryLinks
                                                    .Select(link => new SimpleMaterialData()
                                                    {
                                                        BID = link.Material.BID,
                                                        ID = link.Material.ID,
                                                        ClassTypeID = link.Material.ClassTypeID,
                                                        IsActive = link.Material.IsActive,
                                                        IsDefault = false,
                                                        DisplayName = link.Material.Name
                                                    }).ToList());
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<MaterialCategory> WherePrimary(IQueryable<MaterialCategory> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cat"></param>
        /// <returns></returns>
        public async Task LinkOrUnlinkParts(MaterialCategory cat)
        {
            await PartsToCategory.LinkOrUnlink(
                this.ctx.MaterialCategoryLink.Where(link => link.CategoryID == cat.ID && link.BID == this.BID).ToList(),
                cat.SimpleMaterials,
                (link, simpleMaterial)=>link.PartID == simpleMaterial.ID,
                async (link)=>await this.LinkMaterial(cat.ID, link.PartID, false),
                async (simpleMaterial)=>await this.LinkMaterial(cat.ID, simpleMaterial.ID, true)
            );
        }
    }
}
