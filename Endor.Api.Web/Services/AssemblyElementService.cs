﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Assembly Element Service
    /// </summary>
    public class AssemblyElementService : AtomCRUDService<AssemblyElement, int>, 
        IDoBeforeCreateUpdateWithParent<AssemblyElement, AssemblyLayout>, 
        IDoBeforeCreateUpdateWithParent<AssemblyElement, AssemblyElement>,
        ICloneableChildCRUDService<AssemblyLayout, AssemblyElement>
    {
        /// <summary>
        /// Assembly Element Service
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">Push Client</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public AssemblyElementService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "Children" };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        } 

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">CustomFieldLayoutDefinition ID</param>
        /// <returns></returns>
        public override IQueryable<AssemblyElement> WherePrimary(IQueryable<AssemblyElement> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<AssemblyElement, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<AssemblyElement, int>[]{
                CreateChildAssociation<AssemblyElementService, AssemblyElement, int>(e => e.Elements),
            };
        }

        /// <summary>
        /// Actions to perform before model validation
        /// </summary>
        /// <param name="newModel"></param>
        internal override void DoBeforeValidate(AssemblyElement newModel)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public void DoBeforeCreateWithParent(AssemblyElement child, AssemblyLayout parent)
        {
            child.LayoutID = parent.ID;
            child.ParentID = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        public void DoBeforeUpdateWithParent(AssemblyElement child, AssemblyLayout oldParent, AssemblyLayout newParent)
        {
            child.LayoutID = newParent.ID;
            child.ParentID = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public void DoBeforeCreateWithParent(AssemblyElement child, AssemblyElement parent)
        {
            child.LayoutID = parent.LayoutID;
            child.ParentID = parent.ID;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        public void DoBeforeUpdateWithParent(AssemblyElement child, AssemblyElement oldParent, AssemblyElement newParent)
        {
            child.LayoutID = newParent.LayoutID;
            child.ParentID = newParent.ID;
        }

        /// <summary>
        /// Gets the old model
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task<AssemblyElement> GetOldModelAsync(AssemblyElement newModel)
        {
            // because AssemblyElement is self-referential, loading an element and all of its deeply nested children is hard
            var oldModel = await WherePrimary(
                ctx.AssemblyElement
                .Include(x => x.Elements)
                .ThenInclude(x => x.Elements) // we need lots of these because at time of writing EF is bad at recursive includes + "AsNoTracking"
                .ThenInclude(x => x.Elements) // if you don't include this deep you will see an error "DELETE statement conflicted with the SAME TABLE REFERENCE constraint "FK_Subassembly.Element.Data_Subassembly.Element.Data""
                .ThenInclude(x => x.Elements) // when trying to delete AssemblyElements
                .ThenInclude(x => x.Elements) // so if a user has Elements deeper than the number of includes listed here
                .ThenInclude(x => x.Elements) // you might see that error again and need to add another ThenInclude
                .AsNoTracking(), 
                newModel.ID)
                .FirstOrDefaultAsync();

            return oldModel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentModel"></param>
        /// <param name="childModel"></param>
        /// <returns></returns>
        public Task DoBeforeCloneForParent(AssemblyLayout parentModel, AssemblyElement childModel)
        {
            childModel.AssemblyID = parentModel.AssemblyID;
            return Task.CompletedTask;
        }
    }
}