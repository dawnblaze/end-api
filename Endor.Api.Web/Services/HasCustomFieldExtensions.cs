﻿using Endor.EF;
using Endor.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Result class
    /// </summary>
    public class CustomFieldValidationResult
    {
        /// <summary>
        /// Validation was successful or not
        /// </summary>
        [JsonProperty("result")]
        public bool Result { get; set; }

        /// <summary>
        /// Validation failure message
        /// </summary>
        [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        /// <summary>
        /// Returns a successful validation result
        /// </summary>
        /// <returns></returns>
        public static CustomFieldValidationResult Success()
        {
            return new CustomFieldValidationResult()
            {
                Result = true,
            };
        }

        /// <summary>
        /// Returns a failed validation result
        /// </summary>
        /// <returns></returns>
        public static CustomFieldValidationResult Fail(string message)
        {
            return new CustomFieldValidationResult()
            {
                Result = false,
                Message = message,
            };
        }
    }

    /// <summary>
    /// Extension Methods for the IHasCustomField interface
    /// </summary>
    public static class HasCustomFieldExtensions
    {
        /// <summary>
        /// Loads the Custom Field Values from the database
        /// </summary>
        /// <typeparam name="I"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="ctx"></param>
        public static void LoadCFValues<I, T>(this IHasCustomField<I,T> obj, ApiContext ctx)
            where I : struct, IConvertible
            where T : class, ICustomFieldRecord<I>
        {
            T result = ctx.Set<T>().FirstOrDefault(cf => cf.BID == obj.BID && cf.ID.Equals(obj.ID) && cf.AppliesToClassTypeID == obj.ClassTypeID);

            if (result == null)
                obj.CFValuesJSON = null;
            else
                obj.CFValuesJSON = CustomFieldService.ConvertCustomFieldXMLtoJSON(result.DataXML);
        }

        /// <summary>
        /// Validates custom field values
        /// </summary>
        /// <typeparam name="I"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="ctx"></param>
        /// <returns></returns>
        public static CustomFieldValidationResult ValidateCFValues<I, T>(this IHasCustomField<I, T> obj, ApiContext ctx)
            where I : struct, IConvertible
            where T : class, ICustomFieldRecord<I>
        {
            if (obj.CFValues == null)
                return CustomFieldValidationResult.Success();

            // To Do
            return CustomFieldValidationResult.Success();
        }

        /// <summary>
        /// Saves the Custom Field Values from the database
        /// </summary>
        /// <typeparam name="I"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="ctx"></param>
        public static void SaveCFValues<I, T>(this IHasCustomField<I, T> obj, ApiContext ctx)
            where I : struct, IConvertible
            where T : class, ICustomFieldRecord<I>, new()
        {
            T result = ctx.Set<T>().FirstOrDefault(cf => cf.BID == obj.BID && cf.ID.Equals(obj.ID) && cf.AppliesToClassTypeID == obj.ClassTypeID);

            if (result == null)
            {
                result = new T()
                {
                    BID = obj.BID,
                    ID = obj.ID, 
                    ClassTypeID = obj.ClassTypeID+1,
                    AppliesToClassTypeID = obj.ClassTypeID,
                };
                ctx.Set<T>().Add(result);
            }
            //result.DataJSON = obj.CFValuesJSON;
            result.DataXML = CustomFieldService.ConvertCustomFieldJSONtoXML(obj.CFValuesJSON);
            result.ModifiedDT = DateTime.UtcNow;

            ctx.SaveChanges();
        }

        /// <summary>
        /// Returns a List of CustomFieldDefinitions for the class
        /// </summary>
        /// <typeparam name="I"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="ctx"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public static List<CustomFieldDefinition> GetCFDefinitions<I, T>(this IHasCustomField<I, T> obj, ApiContext ctx, bool isActive = true)
            where I : struct, IConvertible
            where T : class, ICustomFieldRecord<I>
        {
            var result = ctx.Set<CustomFieldDefinition>().Where(cfd => cfd.BID == obj.BID && cfd.AppliesToClassTypeID == obj.ClassTypeID);

            if (isActive)
                result = result.Where(cfd => cfd.IsActive);

            return result.ToList();
        }

        /// <summary>
        /// Results a List of CustomFieldValues on the class that includes NULL entries for all CFDs that have no value.  
        /// </summary>
        /// <typeparam name="I"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="ctx"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public static List<CustomFieldValue> GetAllCFValues<I, T>(this IHasCustomField<I, T> obj, ApiContext ctx, bool isActive = true)
            where I : struct, IConvertible
            where T : class, ICustomFieldRecord<I>
        {
            List<CustomFieldValue> result;

            if (string.IsNullOrWhiteSpace(obj.CFValuesJSON))
                result = new List<CustomFieldValue>();
            else
            {
                try
                {
                    result = JsonConvert.DeserializeObject<List<CustomFieldValue>>(obj.CFValuesJSON);
                }
                catch
                {
                    result = new List<CustomFieldValue>();
                }
            }

            List<CustomFieldDefinition> cfds = obj.GetCFDefinitions(ctx, isActive);

            foreach (var cfd in cfds)
            {
                if (!result.Exists(cfv => cfv.ID == cfd.ID))
                    result.Add(new CustomFieldValue()
                    {
                        ID = cfd.ID,
                        V = null,
                    });
            }

            return result;
        }
    }
}
