﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Base CRUD Service
    /// </summary>
    /// <typeparam name="M"></typeparam>
    public abstract class BaseCRUDService<M> : ICRUDService<M>
        where M : class
    {
        /// <summary>
        /// Unsaved Entity ID
        /// </summary>
        public const int UnsavedEntityID = 0;
        /// <summary>
        /// API Context
        /// </summary>
        protected readonly ApiContext ctx;
        /// <summary>
        /// Business ID
        /// </summary>
        public readonly short BID;
        /// <summary>
        /// Logger
        /// </summary>
        protected readonly RemoteLogger logger;
        /// <summary>
        /// RTM Client
        /// </summary>
        protected readonly IRTMPushClient rtmClient;
        /// <summary>
        /// Migration Helper
        /// </summary>
        protected readonly IMigrationHelper migrationHelper;

        /// <summary>
        /// List of error exceptions
        /// </summary>
        public List<Exception> exceptions;

        /// <summary>
        /// Base CRUD Service Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="bid"></param>
        /// <param name="rtmClient"></param>
        /// <param name="exceptions"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public BaseCRUDService(ApiContext context, RemoteLogger logger, short bid, IRTMPushClient rtmClient, IMigrationHelper migrationHelper, List<Exception> exceptions = null)
        {
            this.ctx = context;
            this.migrationHelper = migrationHelper;
            this.migrationHelper.MigrateDb(ctx);
            this.BID = bid;
            this.logger = logger;
            this.rtmClient = rtmClient;
            this.exceptions = exceptions;
        }

        /// <summary>
        /// Adds a new record
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public async virtual Task<M> CreateAsync(M newModel, Guid? tempGuid = null)
        {
            try
            {
                await DoBeforeCreateAsync(newModel, tempGuid);

                await ctx.SaveChangesAsync();

                await DoAfterAddAsync(newModel, tempGuid);

                return newModel;
            }
            catch (Exception e)
            {
                return await HandleCreateException(e);
            }
        }

        /// <summary>
        /// Virtual method to handle exceptions that occur in CreateAsync
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        protected async virtual Task<M> HandleCreateException(Exception e)
        {
            await logger.Error(this.BID, "Create failed", e);
            if (this.exceptions == null)
                this.exceptions = new List<Exception>();
            this.exceptions.Add(e);
            return null;
        }

        /// <summary>
        /// Base abstract method called before adding a new record
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public abstract Task DoBeforeCreateAsync(M newModel, Guid? tempGuid = null);

        /// <summary>
        /// Base abstract method called after adding a new record
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        protected abstract Task DoAfterAddAsync(M newModel, Guid? tempGuid = null);

        /// <summary>
        /// Updates a record
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public virtual async Task<M> UpdateAsync(M newModel, string connectionID)
        {
            try
            {
                M oldModel = await GetOldModelAsync(newModel);

                //Nothing to update
                if (oldModel == null) return null;

                await DoBeforeUpdateAsync(oldModel, newModel);

                await ctx.SaveChangesAsync();

                await DoAfterUpdateAsync(oldModel, newModel, connectionID);
                return newModel;
            }
            catch (Exception e)
            {
                return await HandleUpdateException(e);
            }
        }

        /// <summary>
        /// Virtual method to handle exceptions that occur in CreateAsync
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        protected async virtual Task<M> HandleUpdateException(Exception e)
        {
            await logger.Error(this.BID, "Update failed", e);
            if (this.exceptions == null)
                this.exceptions = new List<Exception>();
            this.exceptions.Add(e);
            return null;
        }

        /// <summary>
        /// Base abstract method to get an old version of a record
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public abstract Task<M> GetOldModelAsync(M newModel);

        /// <summary>
        /// Base abstract method called before updating a record
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public abstract Task DoBeforeUpdateAsync(M oldModel, M newModel);

        /// <summary>
        /// Base abstract method called after updating a record
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected abstract Task DoAfterUpdateAsync(M oldModel, M newModel, string connectionID);

        /// <summary>
        /// Deletes a record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteAsync(M model)
        {
            try
            {
                await DoBeforeDeleteAsync(model);

                bool result = (await ctx.SaveChangesAsync()) > 0;

                await DoAfterDeleteAsync(model);

                return result;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Delete failed", e);
                await HandleUpdateException(e);
                return false;
            }
        }

        /// <summary>
        /// Base abstract method called before deleting a record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public abstract Task DoBeforeDeleteAsync(M model);

        /// <summary>
        /// Base abstract method called after deleting a record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected abstract Task DoAfterDeleteAsync(M model);

    }
}
