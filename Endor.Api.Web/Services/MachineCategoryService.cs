﻿using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// A service for <see cref=" MachineCategory"/>
    /// </summary>
    public class MachineCategoryService : AtomCRUDService<MachineCategory, short>, ISimpleListableViewService<SimpleMachineCategory, short>
    {
        /// <summary>
        /// Constructs a Machine Category service with injected params
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public MachineCategoryService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Returns a list of `MachineCategory` that shares the same name as the `model`
        /// </summary>
        /// <param name="model">`MachineCategory` model to check Name from</param>
        public List<MachineCategory> ExistingNameOwners(MachineCategory model)
        {
            return this.ctx.MachineCategory
                .Where(category =>
                        category.BID == this.BID
                        && category.Name.Trim().Equals(model.Name.Trim())
                        && category.ID != model.ID)
                .ToList();
        }

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<MachineCategory, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<MachineCategory, short>[0];
        }

        internal override void DoBeforeValidate(MachineCategory newModel) { }

        /// <summary>
        /// Changes clone name
        /// </summary>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override Task DoBeforeCloneAsync(MachineCategory clone)
        {
            //clone.Name = GetNextClonedName(clone.Name, t => t.Name);
            clone.Name = GetNextClonedName(ctx.Set<MachineCategory>().AsNoTracking().Where(t => t.BID == BID && t.Name.StartsWith(t.Name)).ToList(), clone.Name, t => t.Name);
            return base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Clone links as well
        /// </summary>
        /// <param name="clonedFromID">Machine Category ID we Cloned from</param>
        /// <param name="clone">Cloned Machine Category</param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(short clonedFromID, MachineCategory clone)
        {
            var cloneFromLinks = await ctx.MachineCategoryLink.Where(l => l.BID == this.BID && l.CategoryID == clonedFromID).ToListAsync();
            if (cloneFromLinks != null && cloneFromLinks.Count > 0)
            {
                List<MachineCategoryLink> newLinks = new List<MachineCategoryLink>();
                foreach (var link in cloneFromLinks)
                {
                    newLinks.Add(new MachineCategoryLink()
                    {
                        BID = this.BID,
                        CategoryID = clone.ID,
                        PartID = link.PartID
                    });
                }
                ctx.MachineCategoryLink.AddRange(newLinks);
                await ctx.SaveChangesAsync();
            }
            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        internal async Task<List<MachineCategory>> GetWithFiltersAsync(MachineCategoryFilter filters)
        {
            if (filters != null && filters.IsActive.HasValue)
            {
                return await this.GetWhere(filters.WherePredicates());
            }
            else
                return await this.GetAsync(null);
        }

        /// <summary>
        /// Business logic to do after adding the new model.
        /// </summary>
        /// <param name="model">Model that was added</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(MachineCategory model, Guid? tempGuid)
        {
           await base.DoAfterAddAsync(model, tempGuid);
           await this.LinkOrUnlinkParts(model);
        }        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(MachineCategory oldModel, MachineCategory newModel, string connectionID)
        {
            await base.DoAfterUpdateAsync(oldModel, newModel, connectionID);
            await this.LinkOrUnlinkParts(newModel);
        }  

        /// <summary>
        /// Link/Unlink Labor Category with Labor Data
        /// </summary>
        /// <param name="machineCategoryID">Machine Category ID</param>
        /// <param name="machineDataID">Machine Data ID</param>
        /// <param name="isLinked">If it is to link or unlink</param>
        public async Task<EntityActionChangeResponse> LinkMachine(short machineCategoryID, int machineDataID, bool isLinked)
        {
            /*          
                @BID
                @MachineCategoryID
                @MachineDataID
                @IsLinked
                @Result Output
            */
            try
            {
                SqlParameter Result = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                Result.Direction = System.Data.ParameterDirection.Output;

                object[] myParams = {
                    new SqlParameter("@BID", this.BID),
                    new SqlParameter("@MachineCategoryID", machineCategoryID),
                    new SqlParameter("@MachineDataID", machineDataID),
                    new SqlParameter("@IsLinked", isLinked),
                    Result
                };

                int affectedRows = await ctx.Database.ExecuteSqlRawAsync(
                    "EXEC dbo.[Part.Machine.Category.Action.LinkMachine] @BID, @MachineCategoryID, @MachineDataID, @IsLinked, @Result Output;",
                    parameters: myParams);

                if (affectedRows > 0)
                {
                    await QueueIndexForModel(machineCategoryID);

                    return new EntityActionChangeResponse()
                    {
                        Id = machineCategoryID,
                        Message = $"Successfully {(isLinked ? "linked" : "unlinked")} MachineCategory:{machineCategoryID} to MachineData:{machineDataID}",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = machineCategoryID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }

            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        /// <summary>
        /// Remove all Machine Category Links
        /// </summary>
        public async Task unlinkAllAsync(short machineCategoryID)
        {
            var existingLinks = this.ctx.MachineCategoryLink.Where(link => link.CategoryID == machineCategoryID);
            if (existingLinks.Count() > 0)
            {
                foreach (var link in existingLinks)
                {
                    this.ctx.MachineCategoryLink.Remove(link);
                }
                await ctx.SaveChangesAsync();
            }
        }

        //-------------unmapped items----------------

        /// <summary>
        /// Maps a specific item.
        /// </summary>
        /// <param name="item">Item to map</param>
        /// <param name="includeExpander">Child/Parent includes</param>
        public override async Task MapItem(MachineCategory item, IExpandIncludes includeExpander = null)
        {
            var includes = includeExpander != null ? includeExpander.GetIncludes() : null;
            if (includes != null)
            {
                if (item.MachineCategoryLinks?.Count > 0)
                {
                    switch (includes[MachineCategoryIncludes.MachinePartsKey].Level)
                    {
                        case IncludesLevel.Full:
                            item.Machines = await Task.FromResult(item.MachineCategoryLinks.Select(link => link.Machine).ToList());
                            break;
                        case IncludesLevel.Simple:
                            item.SimpleMachines = await Task.FromResult(item.MachineCategoryLinks
                                                    .Select(link => new SimpleMachineData()
                                                    {
                                                        BID = link.Machine.BID,
                                                        ID = link.Machine.ID,
                                                        ClassTypeID = link.Machine.ClassTypeID,
                                                        IsActive = link.Machine.IsActive,
                                                        IsDefault = false,
                                                        DisplayName = link.Machine.Name
                                                    }).ToList());
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<MachineCategory> WherePrimary(IQueryable<MachineCategory> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cat"></param>
        /// <returns></returns>
        public async Task LinkOrUnlinkParts(MachineCategory cat)
        {
            await PartsToCategory.LinkOrUnlink(
                this.ctx.MachineCategoryLink.Where(link => link.CategoryID == cat.ID && link.BID == this.BID).ToList(),
                cat.SimpleMachines,
                (link, simplePart)=>link.PartID == simplePart.ID,
                async (link)=>await this.LinkMachine(cat.ID, link.PartID, false),
                async (simplePart)=>await this.LinkMachine(cat.ID, simplePart.ID, true)
            );
        }        

        /// <summary>
        /// Gets simple list set
        /// </summary>
        public DbSet<SimpleMachineCategory> SimpleListSet => this.ctx.SimpleMachineCategory;
    }
}
