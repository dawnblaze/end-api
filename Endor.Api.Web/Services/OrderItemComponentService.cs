﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Endor.Units;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// order item component service
    /// </summary>
    public class OrderItemComponentService : AtomCRUDService<OrderItemComponent, int>, IDoBeforeCreateUpdateWithParent<OrderItemComponent, OrderItemData>
    {
        /// <summary>
        /// OrderItem Service Layer
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderItemComponentService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default children to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        protected readonly Dictionary<AssemblyElementType, ClassType> ElemTypeToClassType = new Dictionary<AssemblyElementType, ClassType>(){
            [AssemblyElementType.LinkedAssembly] = ClassType.Assembly,
            [AssemblyElementType.LayoutVisualizer] = ClassType.Assembly,
            [AssemblyElementType.LinkedLabor] = ClassType.Labor,
            [AssemblyElementType.LinkedMachine] = ClassType.Machine,
            [AssemblyElementType.LinkedMaterial] = ClassType.Material
        };

        /// <summary>
        /// Returns the includes
        /// </summary>
        /// <param name="includes">Children to include</param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// GetChildAssociations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<OrderItemComponent, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<OrderItemComponent, int>[]
            {
                CreateChildAssociation<OrderItemComponentService, OrderItemComponent, int>((y) => y.ChildComponents),
            };
        }

        internal override void DoBeforeValidate(OrderItemComponent newModel)
        {
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">OrderItemComponent ID</param>
        /// <returns></returns>
        public override IQueryable<OrderItemComponent> WherePrimary(IQueryable<OrderItemComponent> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }
        
        /// <summary>
        /// Gets the old model
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task<OrderItemComponent> GetOldModelAsync(OrderItemComponent newModel)
        {
            var oldModel = await WherePrimary(ctx.OrderItemComponent.Include(x => x.ChildComponents).AsNoTracking(), newModel.ID).FirstOrDefaultAsync();

            return oldModel;
        }

        /// <summary>
        /// override for set modified DT for temporal table
        /// we have to set these to not modified because they are computed
        /// we can't save computed values to the db
        /// </summary>
        /// <param name="newModel"></param>
        protected override void SetModifiedDT(OrderItemComponent newModel)
        {
            if (newModel.ID == 0)
            {
                /*
                fix for "Cannot insert an explicit value into a GENERATED ALWAYS column in table 'Dev.Endor.Business.DB1.dbo.Order.Item.Component'. 
                Use INSERT with a column list to exclude the GENERATED ALWAYS column, or insert a DEFAULT into GENERATED ALWAYS column."
                */
                newModel.ModifiedDT = default(DateTime);

                newModel.CostNet = 0;
                newModel.PricePreTax = 0;
                newModel.CostPerItem = 0;
                newModel.PricePerItem = 0;
            }

            ctx.Entry(newModel).Property(x => x.ModifiedDT).IsModified = false;

            ctx.Entry(newModel).Property(x => x.PricePreTax).IsModified = false;
            ctx.Entry(newModel).Property(x => x.PricePerItem).IsModified = false;
            ctx.Entry(newModel).Property(x => x.CostNet).IsModified = false;
            ctx.Entry(newModel).Property(x => x.CostPerItem).IsModified = false;
                       
        }

        /// <summary>
        /// set OrderID from parent
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public void DoBeforeCreateWithParent(OrderItemComponent child, OrderItemData parent)
        {
            child.OrderID = parent.OrderID;
        }

        public async static Task AddOrderTaxItemAssessmentForComponent(OrderTaxItemAssessmentService orderTaxItemAssessmentService, OrderItemComponent newModel)
        {
            //Populate OrderTaxItemAssessment
            if (newModel.TaxInfoList != null)
            {
                foreach (var TaxAssessment in newModel.TaxInfoList)
                {
                    foreach (var TaxItemAssessmentResult in TaxAssessment.Items)
                    {
                        OrderItemTaxAssessment newEntry = new OrderItemTaxAssessment()
                        {
                            TaxGroupID = TaxAssessment.TaxGroupID,
                            ItemComponentID = newModel.ID,
                            TaxAmount = TaxItemAssessmentResult.TaxAmount,
                            TaxRate = TaxItemAssessmentResult.TaxRate,
                            TaxItemID = TaxItemAssessmentResult.TaxItemID,
                            TaxCodeID = TaxAssessment.TaxCodeID.GetValueOrDefault(), //defaulting to the default for now
                            OrderItemID = newModel.OrderItemID,
                            OrderID = newModel.OrderID,
                            TaxableAmount = TaxAssessment.PriceTaxable,
                            BID = newModel.BID
                        };
                        await orderTaxItemAssessmentService.DoBeforeCreateAsync(newEntry, null);
                    }
                }
            }
        }

        /// <summary>
        /// Business logic to do after adding the new model.
        /// </summary>
        /// <param name="newModel">Model that was added</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(OrderItemComponent newModel, Guid? tempGuid)
        {
            await base.DoAfterAddAsync(newModel, tempGuid);

            OrderTaxItemAssessmentService orderTaxItemAssessmentService = new OrderTaxItemAssessmentService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);

            await AddOrderTaxItemAssessmentForComponent(orderTaxItemAssessmentService, newModel);

            await ctx.SaveChangesAsync(); //Save changes async
        }

        /// <summary>
        /// Business logic to do after updating the new model.
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(OrderItemComponent oldModel, OrderItemComponent newModel)
        {
            await base.DoBeforeUpdateAsync(oldModel, newModel);

            //Populate OrderTaxItemAssessment
            OrderTaxItemAssessmentService orderTaxItemAssessmentService = new OrderTaxItemAssessmentService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
            var existingEntries = ctx.OrderItemTaxAssessment.AsNoTracking().Where(otia => otia.BID == BID && otia.ItemComponentID == newModel.ID).ToList();

            if (newModel.TaxInfoList != null)
            {
                foreach (var TaxAssessment in newModel.TaxInfoList)
                {
                    foreach (var TaxItemAssessmentResult in TaxAssessment.Items)
                    {
                        var existingEntry = existingEntries.Where(e => e.TaxGroupID == TaxAssessment.TaxGroupID && e.TaxItemID == TaxItemAssessmentResult.TaxItemID).FirstOrDefault();
                        if (existingEntry != null)
                        {
                            var clone = await orderTaxItemAssessmentService.GetClone(existingEntry.ID);
                            clone.TaxAmount = TaxItemAssessmentResult.TaxAmount;
                            clone.TaxRate = TaxItemAssessmentResult.TaxRate;
                            clone.TaxableAmount = TaxAssessment.PriceTaxable;
                            clone.TaxCodeID = TaxAssessment.TaxCodeID.GetValueOrDefault(0);
                            await orderTaxItemAssessmentService.DoBeforeUpdateAsync(existingEntry, clone);
                            ctx.Entry(clone).Property("BID").IsModified = false;
                            existingEntries.Remove(existingEntry);
                        }
                        else
                        {
                            //create new Entries
                            OrderItemTaxAssessment newEntry = new OrderItemTaxAssessment()
                            {
                                TaxGroupID = TaxAssessment.TaxGroupID,
                                ItemComponentID = newModel.ID,
                                TaxAmount = TaxItemAssessmentResult.TaxAmount,
                                TaxRate = TaxItemAssessmentResult.TaxRate,
                                TaxItemID = TaxItemAssessmentResult.TaxItemID,
                                TaxCodeID = TaxAssessment.TaxCodeID.GetValueOrDefault(0), //defaulting to the default for now
                                OrderItemID = newModel.OrderItemID,
                                OrderID = newModel.OrderID,
                                TaxableAmount = TaxAssessment.PriceTaxable,
                                BID = newModel.BID
                            };
                            await orderTaxItemAssessmentService.DoBeforeCreateAsync(newEntry);
                        }
                    }
                }
            }
            foreach (var entry in existingEntries) //remove the remaining entries
            {
                await orderTaxItemAssessmentService.DeleteAsync(entry);
            }
        }

        /// <summary>
        /// Overrides the mapping of the whole collection result
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override async Task<List<OrderItemComponent>> MapCollection(List<OrderItemComponent> collection, IExpandIncludes includes = null)
        {
            foreach (var item in collection)
                await this.MapItem(item, includes);

            return collection;
        }

        /// <summary>
        /// Map the relevent data into the given Map Item
        /// </summary>
        public override Task MapItem(OrderItemComponent item, IExpandIncludes includeExpander = null)
        {
            var taxEntries = ctx.OrderItemTaxAssessment.AsNoTracking().Where(x => x.BID == BID && x.ItemComponentID.GetValueOrDefault() == (int)item.ID);
            if (taxEntries.Any())
            {
                item.TaxInfoList = new List<TaxAssessmentResult>();
                var taxGroupIDs = taxEntries.Select(e => e.TaxGroupID).Distinct().ToList();
                foreach(var taxGroupID in taxGroupIDs)
                {
                    var taxItemResults = new List<TaxAssessmentItemResult>();
                    var itemEntries = taxEntries.Where(x => x.TaxGroupID == taxGroupID).ToList();
                    foreach (var itemEntry in itemEntries)
                    {
                        var taxItem = ctx.TaxItem.Where(x => x.BID == BID && x.ID == itemEntry.TaxItemID).First();
                        TaxAssessmentItemResult itemResult = new TaxAssessmentItemResult()
                        {
                            TaxItemID = itemEntry.TaxItemID,
                            InvoiceText = taxItem.InvoiceText,
                            TaxRate = itemEntry.TaxRate,
                            TaxAmount = itemEntry.TaxAmount,
                            GLAccountID = taxItem.GLAccountID
                        };
                        taxItemResults.Add(itemResult);
                    }
                    var firstItem = itemEntries.First();
                    TaxAssessmentResult result = new TaxAssessmentResult()
                    {
                        OrderItemID = firstItem.OrderItemID.GetValueOrDefault(),
                        TaxGroupID = firstItem.TaxGroupID,
                        TaxAmount = item.PriceTax.GetValueOrDefault(),
                        PriceTaxable = item.PriceTaxable.GetValueOrDefault(),
                        IsTaxExempt = ctx.OrderItemData.First(x => x.BID == BID && x.ID == item.OrderItemID).IsTaxExempt,
                        TaxRate = itemEntries.Sum(x => x.TaxRate),
                        ItemComponentID = item.ID,
                        InvoiceText = ctx.TaxGroup.FirstOrDefault(x => x.BID == BID && x.ID == taxGroupID)?.Name,
                        Items = taxItemResults
                    };
                    item.TaxInfoList.Add(result);
                }
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// set orderID from parent
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        public void DoBeforeUpdateWithParent(OrderItemComponent child, OrderItemData oldParent, OrderItemData newParent)
        {
            child.OrderID = newParent.OrderID;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public override async Task DoBeforeDeleteAsync(OrderItemComponent model)
        {
            
            //Delete OrderTaxItemAssessment
            OrderTaxItemAssessmentService orderTaxItemAssessmentService = new OrderTaxItemAssessmentService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
            var existingEntries = ctx.OrderItemTaxAssessment.AsNoTracking().Where(otia => otia.BID == BID && otia.ItemComponentID == model.ID).ToList();
            foreach (var entry in existingEntries)
            {
                await orderTaxItemAssessmentService.DoBeforeDeleteAsync(entry);
            }
            await base.DoBeforeDeleteAsync(model);

            model.OrderItem = null;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="componentClassType"></param>
        /// <param name="componentID"></param>
        /// <param name="componentName"></param>
        /// <param name="partName">Labor or Material Name, this will override the `componentID` param</param>
        /// <returns></returns>
        public async Task<OrderItemComponent> GenerateComponent(ClassType componentClassType, int componentID, string componentName = null, string partName = null)
        {
            if(!String.IsNullOrWhiteSpace(partName))
            {
                componentID = 0;
            }

            var linkedTypes = new AssemblyElementType[]{
                AssemblyElementType.LinkedAssembly,
                AssemblyElementType.LinkedLabor,
                AssemblyElementType.LinkedMachine,
                AssemblyElementType.LinkedMaterial,
                AssemblyElementType.LayoutVisualizer
            };

            var newComponent = new OrderItemComponent{
                BID = this.BID,
                ChildComponents = new List<OrderItemComponent>(),
                ClassTypeID = (int)ClassType.OrderItemComponent,
                TotalQuantity = 1
            };


            switch (componentClassType)
            {
                case ClassType.Assembly:
                    AssemblyData assembly;
                    if(componentID == 1)
                    {
                        assembly = EmbeddedAssemblySingleton.GetInstance().GetEmbeddedAssembly(EmbeddedAssemblyType.LayoutComponent);
                    }else{                        
                        assembly = await this.ctx.AssemblyData
                                            .Include(x => x.Variables)
                                            .AsNoTracking()
                                            .FirstOrDefaultPrimaryAsync(this.BID, componentID);
                    }
                    newComponent.ComponentID = assembly.ID;
                    newComponent.ComponentType = OrderItemComponentType.Assembly;
                    newComponent.Name = componentName ?? assembly.Name;
                    newComponent.IncomeAccountID = assembly.IncomeAccountID;
                    newComponent.Description = assembly.Description;
                    newComponent.QuantityUnit = assembly.Variables
                                                    .FirstOrDefault(x => x.Name == "TotalQuantity")
                                                    ?.UnitID;
                    break;
                case ClassType.Labor:
                    var laborData = await this.ctx.LaborData
                                        .AsNoTracking()
                                        .FirstOrDefaultAsync(x => 
                                            x.BID==this.BID &&
                                            (x.Name == partName || x.ID == componentID)
                                        );
                                        
                    newComponent.Name = componentName ?? laborData.Name;
                    newComponent.ComponentID = laborData.ID;
                    newComponent.ComponentType = OrderItemComponentType.Labor;
                    newComponent.ExpenseAccountID = laborData.ExpenseAccountID;
                    newComponent.IncomeAccountID = laborData.IncomeAccountID;
                    newComponent.Description = laborData.Description;
                    newComponent.QuantityUnit = Unit.Hour;

                    newComponent.CostNet = laborData.EstimatingCostFixed + (laborData.EstimatingCostPerHour * newComponent.TotalQuantity);
                    newComponent.CostUnit = newComponent.CostNet/newComponent.TotalQuantity;
                    
                    newComponent.PricePreTax = laborData.EstimatingPriceFixed + (laborData.EstimatingPricePerHour * newComponent.TotalQuantity);
                    newComponent.PriceUnit = newComponent.PricePreTax/newComponent.TotalQuantity;                         
                    break;
                case ClassType.Machine:
                    var machineData = await this.ctx.MachineData
                                        .AsNoTracking()
                                        .FirstOrDefaultPrimaryAsync(this.BID, (short)componentID);
                    newComponent.ComponentID = machineData.ID;
                    newComponent.ComponentType = OrderItemComponentType.Machine;
                    newComponent.Name = componentName ?? machineData.Name;
                    newComponent.ExpenseAccountID = machineData.ExpenseAccountID;
                    newComponent.IncomeAccountID = machineData.IncomeAccountID;
                    newComponent.Description = machineData.Description;
                    break;
                case ClassType.Material:
                    var materialData = await this.ctx.MaterialData
                                        .AsNoTracking()
                                        .FirstOrDefaultAsync(x => 
                                            x.BID==this.BID &&
                                            (x.Name == partName || x.ID == componentID)
                                        );
                    newComponent.ComponentID = materialData.ID;
                    newComponent.ComponentType = OrderItemComponentType.Material;
                    newComponent.Name = componentName ?? materialData.Name;
                    newComponent.ExpenseAccountID = materialData.ExpenseAccountID;
                    newComponent.IncomeAccountID = materialData.IncomeAccountID;
                    newComponent.Description = materialData.Description;
                    newComponent.QuantityUnit = materialData.ConsumptionUnit;

                    newComponent.CostUnit = materialData.EstimatingCost;
                    newComponent.CostNet = newComponent.CostUnit * newComponent.TotalQuantity;

                    newComponent.PricePreTax = materialData.EstimatingPrice * newComponent.TotalQuantity;
                    newComponent.PriceUnit = newComponent.PricePreTax/newComponent.TotalQuantity;
                    break;                                              
            }

            return newComponent;
        }
    }
}
