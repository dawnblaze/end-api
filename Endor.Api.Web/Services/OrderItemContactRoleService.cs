﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// order item contact role service
    /// </summary>
    public class OrderItemContactRoleService : AtomCRUDChildService<OrderContactRole, int, int>
    {
        /// <summary>
        /// order item contact role service ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderItemContactRoleService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }
        
        /// <summary>
        /// before delete hook
        /// sets parent id
        /// </summary>
        /// <param name="model"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public override Task DoBeforeDeleteAsync(OrderContactRole model, int parentID)
        {
            SetParentID(model, parentID);
            return base.DoBeforeDeleteAsync(model);
        }

        /// <summary>
        /// before update hook
        /// sets parent id
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="parentID"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(OrderContactRole oldModel, int parentID, OrderContactRole newModel)
        {
            SetParentID(newModel, parentID);
            await base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        /// <summary>
        /// returns includes array (null for this service)
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return null;
        }

        /// <summary>
        /// Gets the EmployeeRoles for an OrderItem
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="includeDefaults">If includeDefaults is true then default roles from the order will be substituted in</param>
        /// <returns></returns>
        internal async Task<List<OrderContactRole>> GetByParentIDWithDefaultsAsync(int parentID, bool includeDefaults)
        {
            if (!await ctx.OrderItemData.AnyAsync(t => t.BID == this.BID && t.ID == parentID))
                return null;

            var orderItemContactRoles = await ctx.OrderContactRole.Where(t => t.OrderItemID == parentID && t.BID == this.BID && t.IsOrderItemRole).ToListAsync();

            if (includeDefaults)
            {
                var OrderContactRolesNotInOrderItemContactRoles = await ctx.OrderItemData.Where(t => t.ID == parentID && t.BID == this.BID).Include(t => t.Order)
                    .Select(t => t.Order).Include(t => t.ContactRoles)
                    .SelectMany(t => t.ContactRoles).Where(t => t.IsOrderRole).Where(t => !orderItemContactRoles.Select(r => r.RoleType).Contains(t.RoleType)).ToListAsync();

                orderItemContactRoles.AddRange(OrderContactRolesNotInOrderItemContactRoles);
            }

            return orderItemContactRoles;
        }

        /// <summary>
        /// gets parent id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override int GetParentID(OrderContactRole model)
        {
            return model.OrderItemID.GetValueOrDefault();
        }

        /// <summary>
        /// returns whether the parent ID is valid
        /// </summary>
        /// <param name="model"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public override bool IsParentIDValid(OrderContactRole model, int parentID)
        {
            return model.OrderItemID == parentID;
        }

        /// <summary>
        /// sets the parent ID
        /// </summary>
        /// <param name="model"></param>
        /// <param name="parentID"></param>
        public override void SetParentID(OrderContactRole model, int parentID)
        {
            model.OrderItemID = parentID;
        }

        /// <summary>
        /// query filter for where parent is of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public override IQueryable<OrderContactRole> WhereParent(IQueryable<OrderContactRole> query, int parentID)
        {
            return query.Where(t => t.OrderItemID == parentID);
        }

        /// <summary>
        /// query filter for where has primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<OrderContactRole> WherePrimary(IQueryable<OrderContactRole> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// returns child associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<OrderContactRole, int>[] GetChildAssociations()
        {
            // needs locator association
            return null;
        }

        internal override void DoBeforeValidate(OrderContactRole newModel)
        {
            
        }

        /// <summary>
        /// before create hook
        /// sets parent id
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="parentID"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(OrderContactRole newModel, int parentID, Guid? tempGuid = null)
        {
            SetParentID(newModel, parentID);
            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }
        
        /// <summary>
        /// override for set modified DT for temporal table
        /// </summary>
        /// <param name="newModel"></param>
        protected override void SetModifiedDT(OrderContactRole newModel)
        {
            ctx.Entry(newModel).Property(x => x.ModifiedDT).IsModified = false;
        }
    }
}
