using Endor.EF;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Endor.Tasks;
using Endor.Api.Web.Classes;
using Endor.Tenant;
using Endor.Logging.Client;
using Endor.RTM;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Labor Category Service
    /// </summary>
    public class LaborCategoryService : AtomCRUDService<LaborCategory, short>, ISimpleListableViewService<SimpleLaborCategory, short>
    {

        /// <summary>
        /// Labor Category Service Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">ITaskQueuer</param>
        /// <param name="cache">ITenantDataCache</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public LaborCategoryService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) 
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Property to get  SimpleLaborCategory from ApiContext
        /// </summary>
        public DbSet<SimpleLaborCategory> SimpleListSet => ctx.SimpleLaborCategory;

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        //public override string[] IncludeDefaults { get; } = { "LaborCategoryLinks" };
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Get Child Service Associations for LaborCategory - Includes PartLabor
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<LaborCategory, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<LaborCategory, short>[]
            {
                CreateChildAssociation<PartLaborService, LaborData, int>(lc=>lc.Labors)
            };
        }

        internal override void DoBeforeValidate(LaborCategory model)
        {

        }

        /// <summary>
        /// Business logic to do after adding the new model.
        /// </summary>
        /// <param name="model">Model that was added</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(LaborCategory model, Guid? tempGuid)
        {
           await base.DoAfterAddAsync(model, tempGuid);
           await this.LinkOrUnlinkParts(model);
        }        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(LaborCategory oldModel, LaborCategory newModel, string connectionID)
        {
            await base.DoAfterUpdateAsync(oldModel, newModel, connectionID);
            await this.LinkOrUnlinkParts(newModel);
        }           

        /// <summary>
        /// Changes clone name
        /// </summary>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override Task DoBeforeCloneAsync(LaborCategory clone)
        {
            //clone.Name = GetNextClonedName(clone.Name, t => t.Name);
            clone.Name = GetNextClonedName(ctx.Set<LaborCategory>().AsNoTracking().Where(t => t.BID == BID && t.Name.StartsWith(t.Name)).ToList(), clone.Name, t => t.Name);
            return base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Clone links as well
        /// </summary>
        /// <param name="clonedFromID">Labor Category ID we Cloned from</param>
        /// <param name="clone">Cloned Labor Category</param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(short clonedFromID, LaborCategory clone)
        {
            var cloneFromLinks = await ctx.LaborCategoryLink.Where(l => l.BID == this.BID && l.CategoryID == clonedFromID).ToListAsync();
            if (cloneFromLinks != null && cloneFromLinks.Count > 0)
            {
                List<LaborCategoryLink> newLinks = new List<LaborCategoryLink>();
                foreach (var link in cloneFromLinks)
                {
                    newLinks.Add(new LaborCategoryLink()
                    {
                        BID = this.BID,
                        CategoryID = clone.ID,
                        PartID = link.PartID
                    });
                }
                ctx.LaborCategoryLink.AddRange(newLinks);
                await ctx.SaveChangesAsync();
            }
            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        /// <summary>
        /// Returns a list of `LaborCategory` that shares the same name as the `model`
        /// </summary>
        /// <param name="model">`LaborCategory` model to check Name from</param>
        public List<LaborCategory> ExistingNameOwners(LaborCategory model)
        {
            return this.ctx.LaborCategory
                .Where(category=> 
                        category.BID==this.BID 
                        && category.Name.Trim().Equals(model.Name.Trim())
                        && category.ID!=model.ID)
                .ToList();
        }

        /// <summary>
        /// Link/Unlink Labor Category with Labor Data
        /// </summary>
        /// <param name="laborCategoryID">ID</param>
        /// <param name="laborDataID">ID</param>
        /// <param name="isLinked">ID</param>
        /// <param name="indexModel">If the model should be Indexed</param>
        public async Task<EntityActionChangeResponse> LinkLabor(short laborCategoryID, int laborDataID, bool isLinked, bool indexModel = true)
        {
            /*          
                @BID
                @LaborCategoryID
                @LaborDataID
                @IsLinked
                @Result Output
            */
            try{
                SqlParameter Result = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                Result.Direction = System.Data.ParameterDirection.Output;

                object[] myParams = {
                    new SqlParameter("@BID", this.BID),
                    new SqlParameter("@LaborCategoryID", laborCategoryID),
                    new SqlParameter("@LaborDataID", laborDataID),
                    new SqlParameter("@IsLinked", isLinked),
                    Result
                };

                int affectedRows = await ctx.Database.ExecuteSqlRawAsync(
                    "EXEC dbo.[Part.Labor.Category.Action.LinkLabor] @BID, @LaborCategoryID, @LaborDataID, @IsLinked, @Result Output;",
                    parameters: myParams);

                if (affectedRows > 0)
                {
                    // NOTE: prevent entity category from indexing on entity update
                    if (indexModel)
                    {
                        await QueueIndexForModel(laborCategoryID);
                    }

                    base.DoGetRefreshMessagesOnUpdate(new LaborCategory()
                    {
                        ClassTypeID = Convert.ToInt32(ClassType.Labor),
                        ID = Convert.ToInt16(laborCategoryID),
                        BID = this.BID,
                    });

                    return new EntityActionChangeResponse()
                    {
                        Id = laborCategoryID,
                        Message = $"Successfully {(isLinked? "linked":"unlinked")} LaborCategory:{laborCategoryID} to LaborData:{laborDataID}",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = laborCategoryID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }

            }catch(Exception ex){
                return new EntityActionChangeResponse()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        /// <summary>
        /// Remove all Labor Category Links
        /// </summary>
        public async Task unlinkAllAsync(short laborCategoryID){
            var existingLinks = this.ctx.LaborCategoryLink.Where(link=>link.CategoryID==laborCategoryID);
            if(existingLinks.Count()>0){
                foreach (var link in existingLinks)
                {
                    this.ctx.LaborCategoryLink.Remove(link);
                }
                await ctx.SaveChangesAsync();
            }
        }

        //-------------unmapped items----------------
        /// <summary>
        /// Map the relevent data into the Labor Category item
        /// </summary>
        public override async Task MapItem(LaborCategory item, IExpandIncludes includeExpander = null)
        {
            var includes = includeExpander!=null? includeExpander.GetIncludes():null;
            if(includes!=null){
                //var laborDataIDs = item.LaborCategoryLinks.Select(link=>link.PartID).ToArray();
                if (item.LaborCategoryLinks?.Count > 0)
                {
                    switch (includes[LaborCategoryIncludes.LaborPartsKey].Level)
                    {
                        case IncludesLevel.Full:
                            item.Labors = item.LaborCategoryLinks.Select(link=>link.Labor).ToList();
                            break;                        
                        case IncludesLevel.Simple:
                            item.SimpleLabors = item.LaborCategoryLinks
                                                    .Select(link=> new SimpleLaborData(){
                                                        BID = link.Labor.BID,
                                                        ID = link.Labor.ID,
                                                        ClassTypeID = link.Labor.ClassTypeID,
                                                        IsActive = link.Labor.IsActive,
                                                        IsDefault = false,
                                                        DisplayName = link.Labor.Name
                                                    }).ToList();
                            break;                                              
                    }
                }
            }
            await Task.Yield();
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<LaborCategory> WherePrimary(IQueryable<LaborCategory> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cat"></param>
        /// <returns></returns>
        public async Task LinkOrUnlinkParts(LaborCategory cat)
        {
            await PartsToCategory.LinkOrUnlink(
                this.ctx.LaborCategoryLink.Where(link => link.CategoryID == cat.ID && link.BID == this.BID).ToList(),
                cat.SimpleLabors,
                (link, simpleLabor)=>link.PartID == simpleLabor.ID,
                async (link)=>await this.LinkLabor(cat.ID, link.PartID, false),
                async (simpleLabor)=>await this.LinkLabor(cat.ID, simpleLabor.ID, true)
            );
        }        
    }
}
