﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using System.Linq;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// DestinationProfileVariableService
    /// </summary>
    public class DestinationProfileVariableService : AtomCRUDService<DestinationProfileVariable, short>, IDoBeforeCreateUpdateWithParent<DestinationProfileVariable, DestinationProfile>
    {

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper"></param>
        /// <returns></returns>
        public DestinationProfileVariableService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return new string[] { };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<DestinationProfileVariable> WherePrimary(IQueryable<DestinationProfileVariable> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<DestinationProfileVariable, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<DestinationProfileVariable, short>[] { };
        }

        internal override void DoBeforeValidate(DestinationProfileVariable newModel)
        {

        }

        void IDoBeforeCreateUpdateWithParent<DestinationProfileVariable, DestinationProfile>.DoBeforeCreateWithParent(DestinationProfileVariable child, DestinationProfile parent)
        {
            child.ProfileID = parent.ID;
            child.AssemblyID = parent.TemplateID;
        }

        void IDoBeforeCreateUpdateWithParent<DestinationProfileVariable, DestinationProfile>.DoBeforeUpdateWithParent(DestinationProfileVariable child, DestinationProfile oldParent, DestinationProfile newParent)
        {
            child.ProfileID = newParent.ID;
            child.AssemblyID = newParent.TemplateID;
        }
    }
}
