﻿using Endor.EF;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Endor.Tasks;
using Endor.Api.Web.Classes;
using Endor.Tenant;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Models;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Linq.Expressions;

namespace Endor.Api.Web.Services
{
    /*/// <summary>
    /// MessageBodyTemplate Service
    /// </summary>
    public class MessageBodyTemplateService : AtomCRUDService<MessageBodyTemplate, short>
    {
        /// <summary>
        /// MessageBodyTemplate Service Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">ITaskQueuer</param>
        /// <param name="cache">ITenantDataCache</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public MessageBodyTemplateService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) {
            
        }

        ///// <summary>
        ///// Default Child Properties to include from Database Context
        ///// </summary>
        //public override string[] IncludeDefaults => new string[] { "" };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// The MessageBodyTemplate does not have any child associations, so this returns null;
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<MessageBodyTemplate, short>[] GetChildAssociations()
        {
            return null;
        }

        /// <summary>
        /// MessageBodyTemplate Service Override of DoBeforeCreate
        /// </summary>
        /// <param name="newModel">MessageBodyTemplate</param>
        /// <param name="tempGuid">Optionala Temp Guid</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(MessageBodyTemplate newModel, Guid? tempGuid = null)
        {
            if (newModel.Participants != null && newModel.Participants?.Count > 0)
            {
                await ValidateParticipantIds(newModel.Participants);
                newModel.ParticipantInfoJSON = JsonConvert.SerializeObject(newModel.Participants
                    , Newtonsoft.Json.Formatting.None,
                    new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
            }

            this.SetSortIndex(newModel);
            this.SetMergeFieldList(newModel);
            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// Checks that the all of the FK IDs in the MessageParticipantInfo list are valid
        /// </summary>
        /// <param name="messageParticipantInfos"></param>
        /// <returns></returns>
        private async Task ValidateParticipantIds(List<MessageParticipantInfo> messageParticipantInfos)
        {
            List<string> invalidIDMessages = new List<string>();
            foreach (var participant in messageParticipantInfos)
            {
                if (participant.ContactID != null)
                {
                    var contactCheckResult = await ctx.ContactData.FirstOrDefaultAsync(x => x.BID == BID && x.ID == participant.ContactID);
                    if (contactCheckResult == null)
                        invalidIDMessages.Add($"Contact for ID `{participant.ContactID}` not found. ");
                }

                if (participant.EmployeeID != null)
                {
                    var employeeCheckResult = await ctx.EmployeeData.FirstOrDefaultAsync(x => x.BID == BID && x.ID == participant.EmployeeID);
                    if (employeeCheckResult == null)
                        invalidIDMessages.Add($"Employee for ID `{participant.EmployeeID}` not found. ");
                }

                if (participant.TeamID != null)
                {
                    var teamCheckResult = await ctx.EmployeeTeam.FirstOrDefaultAsync(x => x.BID == BID && x.ID == participant.TeamID);
                    if (teamCheckResult == null)
                        invalidIDMessages.Add($"Team for ID `{participant.TeamID}` not found. ");
                }
            }

            if (invalidIDMessages.Count > 0)
            {
                string errors = "";
                foreach (var m in invalidIDMessages)
                    errors += m;

                throw new InvalidOperationException(errors);
            }
        }

        /// <summary>
        /// MessageBodyTemplate Service Override of DoBeforeUpdateAsync
        /// </summary>
        /// <param name="oldModel">Old MessageBodyTemplate</param>
        /// <param name="newModel">Updated MessageBodyTemplate</param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(MessageBodyTemplate oldModel, MessageBodyTemplate newModel)
        {
            if (newModel.Participants != null && newModel.Participants?.Count > 0)
            {
                await ValidateParticipantIds(newModel.Participants);
                newModel.ParticipantInfoJSON = JsonConvert.SerializeObject(newModel.Participants);
            }

            this.SetMergeFieldList(newModel);
            await base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        /// <summary>
        /// MessageBodyTemplate Service Override of DoBeforeCloneAsync
        /// </summary>
        /// <param name="clone">Cloned MessageBodyTemplate</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(MessageBodyTemplate clone)
        {
            clone.Name = GetNextClonedName(ctx.Set<MessageBodyTemplate>().AsNoTracking().Where(t => t.BID == BID && t.Name.StartsWith(t.Name)).ToList(), clone.Name, t => t.Name);
            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newModel"></param>
        internal override void DoBeforeValidate(MessageBodyTemplate newModel)
        {
            
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<MessageBodyTemplate> WherePrimary(IQueryable<MessageBodyTemplate> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        public async Task<MessageBodyTemplate> GetFirstOrDefaultAsync(Expression<Func<MessageBodyTemplate, bool>> predicate)
        {
            var result = await this.FirstOrDefaultAsync(predicate);
            if (result != null && (!String.IsNullOrEmpty(result.ParticipantInfoJSON)  || !String.IsNullOrWhiteSpace(result.ParticipantInfoJSON)))
            {
                var participants = JsonConvert.DeserializeObject<List<MessageParticipantInfo>>(result.ParticipantInfoJSON);
                result.Participants = participants;
            }            
            return result;
        }

        /// <summary>
        /// Returns MessageBodyTemplates based on filters supplied
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<List<MessageBodyTemplate>> GetWithFiltersAsync(MessageBodyTemplateFilter filters)
        {
            if (filters != null && filters.HasFilters)
            {
                return await this.GetWhere(filters.WherePredicates());
            }
            return await this.GetAsync();
        }

        public async Task<List<MessageBodyTemplate>> GetWithFiltersAsyncJunction(MessageBodyTemplateFilter filters)
        {
            var messageBodyTemplates = await GetWithFiltersAsync(filters);
            List<MessageBodyTemplate> result = new List<MessageBodyTemplate>();
            foreach(var messageBodyTemplate in messageBodyTemplates)
            {
                if(!String.IsNullOrEmpty(messageBodyTemplate.ParticipantInfoJSON) || !String.IsNullOrWhiteSpace(messageBodyTemplate.ParticipantInfoJSON))
                {
                    messageBodyTemplate.Participants = JsonConvert.DeserializeObject<List<MessageParticipantInfo>>(messageBodyTemplate.ParticipantInfoJSON);
                }
                result.Add(messageBodyTemplate);
            }
            return result;
        }

        internal async Task<List<SimpleListItem<byte>>> GetSimpleListWithFiltersAsync(MessageBodyTemplateFilter filters)
        {
            List<SimpleListItem<byte>> simpleList = new List<SimpleListItem<byte>>();

            List<MessageBodyTemplate> msgs = await this.GetWithFiltersAsync(filters);
            foreach (var msg in msgs)
            {
                simpleList.Add(new SimpleListItem<byte>
                {
                    BID = this.BID,
                    ID = (byte)msg.ID,
                    ClassTypeID = msg.AppliesToClassTypeID,
                    DisplayName = msg.Name,
                    IsActive = true
                });
            }

            return simpleList;
        }

        /// <summary>
        /// Moves the specified record before the target
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="targetID"></param>
        /// <param name="moveAfter"></param>
        /// <returns></returns>
        public async Task<EntitiesActionChangeResponse> MoveTo(short ID, short targetID, bool moveAfter = false)
        {
            var src = this.ctx.MessageBodyTemplate.FirstOrDefaultPrimary(this.BID, ID);
            var target = this.ctx.MessageBodyTemplate.FirstOrDefaultPrimary(this.BID, targetID);

            if (src == null || target == null)
            {
                return new EntitiesActionChangeResponse
                {
                    Message = "Message Body Template Not Found",
                    Success = false
                };
            }

            short targetSortIndex = target.SortIndex;
            short targetSortIdx = targetSortIndex;
            
            if (src.SortIndex >= targetSortIndex)
            {
                var shifters = await this.ctx.MessageBodyTemplate.Where(x => x.BID == this.BID && x.SortIndex >= targetSortIndex + (moveAfter ? 1 : 0) && x.SortIndex < src.SortIndex).ToListAsync();
                shifters.ForEach(x => x.SortIndex += 1);
                src.SortIndex = (short)(targetSortIndex + (moveAfter && (src.SortIndex != targetSortIndex) ? 1 : 0));
            }
            else
            {
                var shifters = await this.ctx.MessageBodyTemplate.Where(x => x.BID == this.BID && x.SortIndex >= src.SortIndex && x.SortIndex < targetSortIndex + (moveAfter ? 1 : 0)).ToListAsync();
                shifters.ForEach(x => x.SortIndex -= 1);
                src.SortIndex = (short)(targetSortIndex + (moveAfter ? 0 : -1));
            }

            await ctx.SaveChangesAsync();
            await base.QueueIndexForModel(src.ID);

            return new EntitiesActionChangeResponse
            {
                Message = "Successfully moved",
                Success = true
            };
        }

        /// <summary>
        /// Extacts the MergeFields from Subject, Body, and AttachedFileName and sets the MergeFieldList
        /// </summary>
        /// <param name="messageTemplate"></param>
        protected void SetMergeFieldList(MessageBodyTemplate messageTemplate)
        {
            string input = messageTemplate.Subject + messageTemplate.Body + messageTemplate.AttachedFileNames;

            Dictionary<string, string> mf = new Dictionary<string, string>(StringComparer.InvariantCulture);
            var regex = new Regex("{{(.*?)}}");
            var matches = regex.Matches(input);
            foreach(Match match in matches)
            {
                // value with curly braces
                // var val = match.Value;

                // value without curly braces
                var val = match.Groups[1].Value;
                if (!mf.ContainsKey(val.ToUpperInvariant()))
                    mf.Add(val.ToUpperInvariant(), val);
            }
            messageTemplate.MergeFieldList = String.Join(",", mf.Select(x => x.Value).ToArray());
        }

        protected void SetSortIndex(MessageBodyTemplate messageTempate)
        {
            messageTempate.SortIndex = (short)(this.ctx.MessageBodyTemplate.Count() + 1);
        }
        
    }*/
}
