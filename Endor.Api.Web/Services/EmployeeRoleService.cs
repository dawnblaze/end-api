﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Employee Role Service
    /// </summary>
    public class EmployeeRoleService : AtomCRUDService<EmployeeRole, short>
    {
        /// <summary>
        /// Constructs an employee role service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public EmployeeRoleService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Returns EmployeeRoles based on filters supplied in
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<List<EmployeeRole>> GetWithFiltersAsync(EmployeeRoleFilters filters)
        {
            if (filters != null && filters.HasFilters)
            {
                return await this.GetWhere(filters.WherePredicates());
            }

            return await this.GetAsync();
        }

        /// <summary>
        /// default include property strings
        /// </summary>
        public override string[] IncludeDefaults => new string[]
        {
            "BoardLinks"
        };

        /// <summary>
        /// The employee role does not have any includes so this returns null;
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// The employee role does not have any child associations, so this returns null;
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<EmployeeRole, short>[] GetChildAssociations()
        {
            return null;
        }

        internal override void DoBeforeValidate(EmployeeRole newModel)
        {

        }

        /// <summary>
        /// EmployeeRole Override of DoBeforeUpdateAsync
        /// </summary>
        /// <param name="oldModel">Old EmployeeRole</param>
        /// <param name="newModel">Updated EmployeeRole</param>
        /// <returns></returns>
        public override Task DoBeforeUpdateAsync(EmployeeRole oldModel, EmployeeRole newModel)
        {
            if (oldModel.IsSystem)
                throw new InvalidOperationException();

            return base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        internal async Task<List<EmployeeRole>> GetEmployeeRolesAsync(bool? IsActive)
        {
            return await FilteredGetAsync(t => t.BID == this.BID && ((IsActive.HasValue && IsActive.Value) || !IsActive.HasValue));
        }

        internal async Task<SimpleEmployeeRole[]> GetSimpleEmployeeRolesWithFiltersAsync(EmployeeRoleFilters filters)
        {
            return await Where(filters.WherePredicates()).Select(GetSimpleEmployeeRole).ToArrayAsync();
        }

        /// <summary>
        /// Where clause by fixed ID type
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<EmployeeRole> WherePrimary(IQueryable<EmployeeRole> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        private Expression<Func<EmployeeRole, SimpleEmployeeRole>> GetSimpleEmployeeRole =
            (t) => new SimpleEmployeeRole()
            {
                BID = t.BID,
                ClassTypeID = ClassType.EmployeeRole.ID(),
                ID = t.ID,
                DisplayName = t.Name,
                IsActive = t.IsActive,
                IsDefault = false,
            };
    }
}
