﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Option service
    /// </summary>
    public class OptionService : AtomGenericService<OptionData, int>
    {
        /// <summary>
        /// Constructs an Option service
        /// </summary>
        /// <param name="context">Context</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OptionService(ApiContext context, IMigrationHelper migrationHelper) 
            : base(context, migrationHelper)
        {
        }

        /// <summary>
        /// Returns option value by Option ID, Option Name, Association ID, Business ID, Location ID, StoreFront ID, User Link ID, Company ID and Contact ID
        /// </summary>
        /// <param name="optionID">Association ID</param>
        /// <param name="optionName">Association Name</param>
        /// <param name="bid">Business ID</param>
        /// <param name="locationId">Location ID</param>
        /// <param name="storeFrontID">StoreFront ID</param>
        /// <param name="userLinkID">User Link ID</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        public async Task<GetOptionValueResponse> Get(
            int? optionID,
            string optionName,
            short bid,
            short? locationId,
            short? storeFrontID,
            int? userLinkID,
            int? companyID,
            int? contactID    
            )
        {
            try
            {

                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@OptionID", SqlDbType = SqlDbType.Int, Value = (object)optionID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@OptionName", SqlDbType = SqlDbType.NVarChar, Value = (String.IsNullOrWhiteSpace(optionName) ? DBNull.Value : (object)optionName) },
                    new SqlParameter() { ParameterName = "@BID", SqlDbType = SqlDbType.SmallInt, Value = (object)bid },
                    new SqlParameter() { ParameterName = "@LocationID", SqlDbType = SqlDbType.SmallInt, Value = (object)locationId ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@StoreFrontID", SqlDbType = SqlDbType.SmallInt, Value = (object)storeFrontID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@UserLinkID", SqlDbType = SqlDbType.SmallInt, Value = (object)userLinkID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@CompanyID", SqlDbType = SqlDbType.Int, Value = (object)companyID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@ContactID", SqlDbType = SqlDbType.Int, Value = (object)contactID ?? DBNull.Value },
                };

                var rowResult = await ctx.Database.GetModelFromQueryAsync<OptionValue>(@"EXEC dbo.[Option.GetValue] 
                    @OptionID, @OptionName, @BID, @LocationID, @StoreFrontID, @UserLinkID, @CompanyID, @ContactID;"
                    , parameters: sp.ToArray());

                if (rowResult.Count() == 0)
                {
                    return new GetOptionValueResponse
                    {
                        Value = null,
                        Message = "No result found.",
                        Success = true
                    };
                }
                else
                {
                    return new GetOptionValueResponse
                    {
                        Value = rowResult.FirstOrDefault(),
                        Message = "Successfully retrieved Option Value.",
                        Success = true
                    };
                }
            }
            catch (Exception ex)
            {
                return new GetOptionValueResponse
                {
                    Value = null,
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Returns list of option values by Association ID, Business ID, Location ID, Storefront ID, User Link ID, Company ID and Contact ID.
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="locationId">Location ID</param>
        /// <param name="storeFrontID">StoreFront ID</param>
        /// <param name="userLinkID">User Link ID</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="contactID">Contact ID</param>
        /// <param name="categoryID">Category ID</param>
        /// <returns></returns>
        public async Task<GetOptionValuesResponse> Get(
            short bid,
            short? locationId,
            short? storeFrontID,
            int? userLinkID,
            int? companyID,
            int? contactID,
            int? categoryID = null
            )
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@CategoryID", SqlDbType = SqlDbType.SmallInt, Value = (object)categoryID ?? DBNull.Value  },
                    new SqlParameter() { ParameterName = "@BID", SqlDbType = SqlDbType.SmallInt, Value = (object)bid },
                    new SqlParameter() { ParameterName = "@LocationID", SqlDbType = SqlDbType.SmallInt, Value = (object)locationId ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@StoreFrontID", SqlDbType = SqlDbType.SmallInt, Value = (object)storeFrontID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@UserLinkID", SqlDbType = SqlDbType.SmallInt, Value = (object)userLinkID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@CompanyID", SqlDbType = SqlDbType.Int, Value = (object)companyID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@ContactID", SqlDbType = SqlDbType.Int, Value = (object)contactID ?? DBNull.Value },
                };

                var rowResult = await ctx.Database.GetModelFromQueryAsync<OptionValues>(@"EXEC dbo.[Option.GetValues] 
                    @CategoryID, @BID, @LocationID, @StoreFrontID, @UserLinkID, @CompanyID, @ContactID;"
                    , parameters: sp.ToArray());

                if (rowResult.Count() == 0)
                {
                    return new GetOptionValuesResponse
                    {
                        Values = null,
                        Message = "No result found.",
                        Success = true
                    };
                }
                else
                {
                    return new GetOptionValuesResponse
                    {
                        Values = rowResult.ToList(),
                        Message = "Successfully retrieved Option Value.",
                        Success = true
                    };
                }
            }
            catch (Exception ex)
            {
                return new GetOptionValuesResponse
                {
                    Values = null,
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        private async Task AddAssociationAndBIDParameters(byte? associationID, short? bid, List<SqlParameter> sp)
        {
            object bidValue = DBNull.Value;
            object associationValue = DBNull.Value;
            if (!associationID.HasValue && bid.HasValue)
            {
                bidValue = (object)bid;
            }
            else if (associationID.HasValue)
            {
                if (associationID.Value == Byte.MaxValue && bid.HasValue)
                {
                    associationID = (await this.ctx.BusinessData.FirstOrDefaultAsync(x => x.ID == bid.Value))?.AssociationType;
                    if (associationID.HasValue)
                        associationValue = associationID.Value;
                }
                else
                {
                    associationValue = associationID.Value;
                }
            }

            sp.Add(new SqlParameter() { ParameterName = "@BID", SqlDbType = System.Data.SqlDbType.SmallInt, Value = bidValue });
            sp.Add(new SqlParameter() { ParameterName = "@AssociationID", SqlDbType = System.Data.SqlDbType.TinyInt, Value = associationValue });
        }

        /// <summary>
        /// Get valid includes for OptionData model
        /// </summary>
        public override string[] GetIncludes()
        {
            return new string[] 
            {
                "OptionDefinition",
                "EnumOptionLevel"
            };
        }

        /// <summary>
        /// Gets an Option Values by Category ID and Category Name
        /// </summary>
        /// <param name="categoryID">Category ID</param>
        /// <param name="categoryName">Category Name</param>
        /// <returns></returns>
        public async Task<GetOptionValuesResponse> GetByCategory(
            int? categoryID,
            string categoryName
            )
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@CategoryID", SqlDbType = SqlDbType.Int, Value = (object)categoryID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@CategoryName", SqlDbType = SqlDbType.NVarChar, Value = (String.IsNullOrWhiteSpace(categoryName) ? DBNull.Value : (object)categoryName) },
                };

                var rowResult = await ctx.Database.GetModelFromQueryAsync<OptionValues>(@"EXEC dbo.[Option.GetCategoryOptions] 
                    @CategoryID,@CategoryName;", parameters: sp.ToArray());

                if (rowResult.Count() == 0)
                {
                    return new GetOptionValuesResponse
                    {
                        Values = null,
                        Message = "No result found.",
                        Success = true
                    };
                }
                else
                {
                    return new GetOptionValuesResponse
                    {
                        Values = rowResult.ToList(),
                        Message = "Successfully retrieved Options By Category.",
                        Success = true
                    };
                }
            }
            catch (Exception ex)
            {
                return new GetOptionValuesResponse
                {
                    Values = null,
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Updates an Option Value by Option ID, Option Name, Association ID, Business ID, Location ID, Storefront ID, User Link ID, Company ID and Contact ID.
        /// </summary>
        /// <param name="optionID">Option ID</param>
        /// <param name="optionName">Option Name</param>
        /// <param name="value">Option Value</param>
        /// <param name="associationID">Association ID</param>
        /// <param name="bid">Business ID</param>
        /// <param name="locationId">Location ID</param>
        /// <param name="storeFrontID">StoreFront ID</param>
        /// <param name="userLinkID">User Link ID</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        public async Task<PutOptionValueResponse> Put(
            int? optionID,
            string optionName,
            string value,
            byte? associationID,
            short? bid,
            short? locationId,
            short? storeFrontID,
            int? userLinkID,
            int? companyID,
            int? contactID
            )
        {
            try
            {
                if ((optionID.HasValue)&&(ctx.SystemOptionDefinition.Where(opt => opt.ID == optionID.GetValueOrDefault()).Count()==0))
                {
                    return new PutOptionValueResponse
                    {
                        Value = null,
                        ErrorMessage = "Option Definition ID Not Found",
                        Success = false
                    };
                }
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@OptionID", SqlDbType = SqlDbType.Int, Value = (object)optionID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@OptionName", SqlDbType = SqlDbType.NVarChar, Value = (String.IsNullOrWhiteSpace(optionName) ? DBNull.Value : (object)optionName) },
                    new SqlParameter() { ParameterName = "@Value", SqlDbType = SqlDbType.NVarChar, Value = (object)value ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@LocationID", SqlDbType = SqlDbType.SmallInt, Value = (object)locationId ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@UserLinkID", SqlDbType = SqlDbType.SmallInt, Value = (object)userLinkID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@CompanyID", SqlDbType = SqlDbType.Int, Value = (object)companyID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@ContactID", SqlDbType = SqlDbType.Int, Value = (object)contactID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@StoreFrontID", SqlDbType = SqlDbType.SmallInt, Value = (object)storeFrontID ?? DBNull.Value },
                };
                await AddAssociationAndBIDParameters(associationID, bid, sp);

                using (await ctx.Database.ExecuteSqlRawAsync2(@"EXEC dbo.[Option.SaveValue] 
                    @OptionID, @OptionName, @Value, @AssociationID, @BID, @LocationID, @UserLinkID, @CompanyID, @ContactID, @StoreFrontID;"
                    , new System.Threading.CancellationToken(), parameters: sp.ToArray()))
                {
                    
                }
                return new PutOptionValueResponse
                {
                    Message = "Successfully saved Option Value.",
                    Success = true
                };

            }
            catch (Exception ex)
            {
                return new PutOptionValueResponse
                {
                    Value = null,
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Updates List of Option Values by Association ID, Business ID, Location ID, Storefront ID, User Link ID, Company ID and Contact ID.
        /// </summary>
        /// <param name="options">List of options</param>
        /// <param name="associationID">Association ID</param>
        /// <param name="bid">Business ID</param>
        /// <param name="locationId">Location ID</param>
        /// <param name="storeFrontID">StoreFront ID</param>
        /// <param name="userLinkID">User Link ID</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        public async Task<PutOptionValuesResponse> Put(
            List<Options> options,
            byte? associationID,
            short? bid,
            short? locationId,
            short? storeFrontID,
            int? userLinkID,
            int? companyID,
            int? contactID
            )
        {
            DataTable saveData = new DataTable();
            saveData.Columns.Add("OptionID", typeof(System.Data.SqlTypes.SqlInt32)); //0
            saveData.Columns.Add("OptionName", typeof(System.Data.SqlTypes.SqlString)); //1
            saveData.Columns.Add("Value", typeof(System.Data.SqlTypes.SqlString)); //2
            saveData.Columns[0].AllowDBNull = true;
            saveData.Columns[1].MaxLength = 255;
            saveData.Columns[1].AllowDBNull = true;
            saveData.Columns[2].AllowDBNull = true;

            // iterate the options being passed into the method to construct a row for each
            foreach (Options option in options)
            {
                var row = saveData.NewRow();
                if (option.OptionID != null)
                    row[0] = option.OptionID;
                row[1] = (String.IsNullOrWhiteSpace(option.OptionName) ? DBNull.Value : (object)option.OptionName);
                row[2] = (String.IsNullOrWhiteSpace(option.Value)? DBNull.Value : (object)option.Value);
                saveData.Rows.Add(row);
            }

            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@Options_Array", SqlDbType = SqlDbType.Structured, Value = saveData, TypeName = "[dbo].[OptionsArray]" },
                    new SqlParameter() { ParameterName = "@LocationID", SqlDbType = SqlDbType.SmallInt, Value = (object)locationId ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@UserLinkID", SqlDbType = SqlDbType.SmallInt, Value = (object)userLinkID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@CompanyID", SqlDbType = SqlDbType.Int, Value = (object)companyID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@ContactID", SqlDbType = SqlDbType.Int, Value = (object)contactID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@StoreFrontID", SqlDbType = SqlDbType.SmallInt, Value = (object)storeFrontID ?? DBNull.Value },
                };
                await AddAssociationAndBIDParameters(associationID, bid, sp);

                using (await ctx.Database.ExecuteSqlRawAsync2(@"EXEC dbo.[Option.SaveValues] 
                    @Options_Array, @AssociationID, @BID, @LocationID, @UserLinkID, @CompanyID, @ContactID, @StoreFrontID;"
                    , new System.Threading.CancellationToken(), parameters: sp.ToArray()))
                {
                    return new PutOptionValuesResponse
                    {
                        Message = "Successfully saved Option Values.",
                        Success = true
                    };
                }
            }
            catch (Exception ex)
            {
                return new PutOptionValuesResponse
                {
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Deletes an Option Value by Option ID, Option Name, Association ID, Business ID, Location ID, Storefront ID, User Link ID, Company ID and Contact ID.
        /// </summary>
        /// <param name="optionID">Option ID</param>
        /// <param name="optionName">Option Name</param>
        /// <param name="associationID">Association ID</param>
        /// <param name="bid">Business ID</param>
        /// <param name="locationId">Location ID</param>
        /// <param name="storeFrontID">StoreFront ID</param>
        /// <param name="userLinkID">User Link ID</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        public async Task<DeleteOptionValueResponse> Delete(
            int? optionID,
            string optionName,
            byte? associationID,
            short? bid,
            short? locationId,
            short? storeFrontID,
            int? userLinkID,
            int? companyID,
            int? contactID
            )
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@OptionID", SqlDbType = System.Data.SqlDbType.Int, Value = (object)optionID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@OptionName", SqlDbType = System.Data.SqlDbType.NVarChar, Value = (String.IsNullOrWhiteSpace(optionName) ? DBNull.Value : (object)optionName) },
                    new SqlParameter() { ParameterName = "@LocationID", SqlDbType = System.Data.SqlDbType.SmallInt, Value = (object)locationId ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@UserLinkID", SqlDbType = System.Data.SqlDbType.SmallInt, Value = (object)userLinkID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@CompanyID", SqlDbType = System.Data.SqlDbType.Int, Value = (object)companyID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@ContactID", SqlDbType = System.Data.SqlDbType.Int, Value = (object)contactID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@StoreFrontID", SqlDbType = System.Data.SqlDbType.SmallInt, Value = (object)storeFrontID ?? DBNull.Value },
                };
                await AddAssociationAndBIDParameters(associationID, bid, sp);

                using (await ctx.Database.ExecuteSqlRawAsync2(@"EXEC dbo.[Option.DeleteValue] 
                    @OptionID, @OptionName, @AssociationID, @BID,
                    @LocationID,@UserLinkID,@CompanyID,@ContactID,@StoreFrontID;", parameters: sp.ToArray()))
                {
                    return new DeleteOptionValueResponse
                    {
                        Message = "Successfully deleted Option Value.",
                        Success = true
                    };
                }
            }
            catch (Exception ex)
            {
                return new DeleteOptionValueResponse
                {
                    Value = null,
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Deletes list of option values by Association ID, Business ID, Location ID, Storefront ID, User Link ID, Company ID and Contact ID
        /// </summary>
        /// <param name="options">List of options</param>
        /// <param name="associationID">Association ID</param>
        /// <param name="bid">Business ID</param>
        /// <param name="locationId">Location ID</param>
        /// <param name="storeFrontID">StoreFront ID</param>
        /// <param name="userLinkID">User Link ID</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        public async Task<DeleteOptionValuesResponse> Delete(
            List<Options> options,
            byte? associationID,
            short? bid,
            short? locationId,
            short? storeFrontID,
            int? userLinkID,
            int? companyID,
            int? contactID
            )
        {
            DataTable saveData = new DataTable();
            saveData.Columns.Add("OptionID", typeof(System.Data.SqlTypes.SqlInt32)); //0
            saveData.Columns.Add("OptionName", typeof(System.Data.SqlTypes.SqlString)); //1
            saveData.Columns.Add("Value", typeof(System.Data.SqlTypes.SqlString)); //2
            saveData.Columns[0].AllowDBNull = true;
            saveData.Columns[1].MaxLength = 255;
            saveData.Columns[1].AllowDBNull = true;
            saveData.Columns[2].AllowDBNull = true;

            // iterate the options being passed into the method to construct a row for each
            foreach (Options option in options)
            {
                var row = saveData.NewRow();
                row[0] = option.OptionID.HasValue ? option.OptionID : (object)DBNull.Value;  // optionID cannot be set to null
                row[1] = (String.IsNullOrWhiteSpace(option.OptionName) ? DBNull.Value : (object)option.OptionName);
                row[2] = DBNull.Value;
                saveData.Rows.Add(row);
            }

            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@Options_Array", SqlDbType = System.Data.SqlDbType.Structured, Value = saveData, TypeName = "[dbo].[OptionsArray]" },
                    new SqlParameter() { ParameterName = "@LocationID", SqlDbType = System.Data.SqlDbType.SmallInt, Value = (object)locationId ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@UserLinkID", SqlDbType = System.Data.SqlDbType.SmallInt, Value = (object)userLinkID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@CompanyID", SqlDbType = System.Data.SqlDbType.Int, Value = (object)companyID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@ContactID", SqlDbType = System.Data.SqlDbType.Int, Value = (object)contactID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@StoreFrontID", SqlDbType = System.Data.SqlDbType.SmallInt, Value = (object)storeFrontID ?? DBNull.Value },
                };
                await AddAssociationAndBIDParameters(associationID, bid, sp);

                using (await ctx.Database.ExecuteSqlRawAsync2(@"EXEC dbo.[Option.DeleteValues] 
                    @Options_Array, @AssociationID, @BID,
                    @LocationID,@UserLinkID,@CompanyID,@ContactID,@StoreFrontID;", new System.Threading.CancellationToken(), parameters: sp.ToArray()))
                {
                    return new DeleteOptionValuesResponse
                    {
                        Message = "Successfully deleted Option Values.",
                        Success = true
                    };
                }
            }
            catch (Exception ex)
            {
                return new DeleteOptionValuesResponse
                {
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Deletes an Option Value by Category ID and Category Name
        /// </summary>
        /// <param name="categoryID">Category ID</param>
        /// <param name="categoryName">Category Name</param>
        /// <returns></returns>
        public async Task<DeleteOptionValuesResponse> DeleteByCategory(
            int? categoryID,
            string categoryName
            )
        {
            try
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@CategoryID", SqlDbType = System.Data.SqlDbType.Int, Value = (object)categoryID ?? DBNull.Value },
                    new SqlParameter() { ParameterName = "@CategoryName", SqlDbType = System.Data.SqlDbType.NVarChar, Value = (String.IsNullOrWhiteSpace(categoryName) ? DBNull.Value : (object)categoryName) },
                };

                using (await ctx.Database.ExecuteSqlRawAsync2(@"EXEC dbo.[Option.DeleteCategoryOptions] 
                    @CategoryID,@CategoryName;", parameters: sp.ToArray()))
                {
                    return new DeleteOptionValuesResponse
                    {
                        Message = "Successfully deleted Option Values.",
                        Success = true
                    };
                }
            }
            catch (Exception ex)
            {
                return new DeleteOptionValuesResponse
                {
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// lookup category ID by name
        /// </summary>
        /// <param name="CategoryName"></param>
        /// <returns></returns>
        public async Task<int?> GetCategoryIDByName(string CategoryName)
        {
            CategoryName = CategoryName.ToLowerInvariant();
            return (await this.ctx.SystemOptionCategory.Where(x => x.Name.ToLower() == CategoryName).FirstOrDefaultAsync())?.ID;
        }
    }
}
