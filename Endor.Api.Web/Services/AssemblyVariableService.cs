﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// assembly Variable service
    /// </summary>
    public class AssemblyVariableService : AtomCRUDService<AssemblyVariable, int>
        //IDoBeforeCreateUpdateWithParent<AssemblyVariable, AssemblyData>
    {
        /// <summary>
        /// Constructs a AssemblyVariableService service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public AssemblyVariableService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "Formulas" };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(IncludeDefaults);
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">CustomFieldVariableDefinition ID</param>
        /// <returns></returns>
        public override IQueryable<AssemblyVariable> WherePrimary(IQueryable<AssemblyVariable> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<AssemblyVariable, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<AssemblyVariable, int>[]
            {
                CreateChildAssociation<AssemblyVariableFormulaService, AssemblyVariableFormula, int>((a) => a.Formulas)
            };
        }

        internal override void DoBeforeValidate(AssemblyVariable newModel)
        {
            ValidateFormula(newModel);
        }

        /// <summary>
        /// AssymblyVariable Service Override of DoBeforeUpdateAsync
        /// </summary>
        /// <param name="oldModel">Old AssymblyVariable</param>
        /// <param name="updatedModel">Updated AssymblyVariable</param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(AssemblyVariable oldModel, AssemblyVariable updatedModel)
        {
            await base.DoBeforeUpdateAsync(oldModel, updatedModel);
        }

        /// <summary>
        /// Actions to perform before deleting an AssemblyVariable
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(AssemblyVariable model)
        {
            var formulas = await this.ctx.AssemblyVariableFormula.Where(x => x.BID == this.BID && x.VariableID == model.ID).ToListAsync();
            this.ctx.AssemblyVariableFormula.RemoveRange(formulas);

            await base.DoBeforeDeleteAsync(model);
        }

        /// <summary>
        /// Validates formula
        /// </summary>
        /// <param name="variable"></param>
        public static void ValidateFormula(AssemblyVariable variable)
        {
            if (((variable != null && variable.IsFormula && ((String.IsNullOrWhiteSpace(variable.DefaultValue)) || (ContainsOnlyFormulaChar(variable.DefaultValue))))) ||
                ((variable != null && variable.IsConsumptionFormula && ((String.IsNullOrWhiteSpace(variable.ConsumptionDefaultValue)) || (ContainsOnlyFormulaChar(variable.ConsumptionDefaultValue))))))
            {
                throw new InvalidOperationException("Formula is empty");
            }

        }

        /// <summary>
        /// check if it only contains formula char
        /// </summary>
        /// <param name="val">formula string value</param>
        private static bool ContainsOnlyFormulaChar(string val)
        {
            string formulaChar = "=";

            foreach (char c in val)
            {
                if (!formulaChar.Contains(c.ToString()))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Adds a new AssymblyVariable to a newly created Assymbly
        /// </summary>
        /// <param name="newModel">AssymblyVariable</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(AssemblyVariable newModel, Guid? tempGuid = null)
        {
            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// Gets the old model
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task<AssemblyVariable> GetOldModelAsync(AssemblyVariable newModel)
        {
            var model = await WherePrimary(ctx.AssemblyVariable.Include(x => x.Formulas).AsNoTracking(), newModel.ID).FirstOrDefaultAsync();

            //MapLayoutChildrenToTree(oldModel);

            return model;
        }

    }
}
