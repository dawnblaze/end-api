﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.RTM;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Service that wraps utilizes data storage layer and is accessed primarily from Controller layer
    /// </summary>
    public class GLAccountService : AtomCRUDService<GLAccount, int>
    {
        /// <summary>
        /// Constructs a new GLAccountService with a number of injeced parameters
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public GLAccountService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) :
            base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "Subaccounts", "Subaccounts.Subaccounts", "Subaccounts.Subaccounts.Subaccounts" };

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<GLAccount, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<GLAccount, int>[]
            {
                CreateChildAssociation<GLAccountService, GLAccount, int>((a)=>a.Subaccounts)
            };
        }

        internal async Task<List<GLAccount>> GetWithFiltersAsync(GLAccountFilters filters)
        {
            if (filters.HasFilter())
            {
                var result = await this.GetWhere(filters.WherePredicates());
                if (!String.IsNullOrWhiteSpace(filters.NumberedName)){
                    result = this.ApplyRecursiveNameFilter(result, filters.NumberedName);
                }

                if ((!filters.IncludeInActive.HasValue && filters.IsActive.HasValue) || (filters.IncludeInActive.HasValue && filters.IncludeInActive == false && filters.IsActive.HasValue))
                {
                    result = this.ApplyRecursiveActiveStatusFilter(result, filters.IsActive);
                }

                return result;
            }
            else
                return await this.GetAsync(null);
        }

        /// <summary>
        /// https://corebridge.atlassian.net/browse/END-9653
        /// EF core currently does not support filtering of chilren fetched via `.Include()`
        /// https://stackoverflow.com/questions/43618096/filtering-on-include-in-ef-core
        /// </summary>
        /// <param name="GLAccountList">ICollection of GLAccount</param>
        /// <param name="IsActive">boolean</param>
        internal List<GLAccount> ApplyRecursiveActiveStatusFilter(ICollection<GLAccount> GLAccountList, bool? IsActive)
        {
            try
            {
                var matchingGLAccount = GLAccountList.Where(gla =>
                    gla.IsActive == IsActive.Value
                ).ToList();

                foreach (var gla in matchingGLAccount)
                {
                    gla.Subaccounts = ApplyRecursiveActiveStatusFilter(gla.Subaccounts, IsActive);
                }

                return matchingGLAccount;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// https://corebridge.atlassian.net/browse/END-2731
        /// EF core currently does not support filtering of chilren fetched via `.Include()`
        /// https://stackoverflow.com/questions/43618096/filtering-on-include-in-ef-core
        /// </summary>
        /// <param name="GLAccountList">ICollection of GLAccount</param>
        /// <param name="numberedNameSearch">string</param>
        internal List<GLAccount> ApplyRecursiveNameFilter(ICollection<GLAccount> GLAccountList, string numberedNameSearch)
        {
            try{
                var matchingGLAccountNames = GLAccountList.Where(gla=>
                        (gla.NumberedName.ToLower()).Contains(numberedNameSearch.ToLower()) ||
                        gla.Subaccounts.Any(subgla => //sub account level 1
                            (subgla.NumberedName.ToLower()).Contains(numberedNameSearch.ToLower()) ||
                            subgla.Subaccounts.Any(subsubgla => //sub account level 2
                                (subsubgla.NumberedName.ToLower()).Contains(numberedNameSearch.ToLower()) ||
                                subsubgla.Subaccounts.Any(subsubsubgla => //sub account level 3
                                    (subsubsubgla.NumberedName.ToLower()).Contains(numberedNameSearch.ToLower())
                                )
                            )
                        )
                ).ToList();

                foreach (var gla in matchingGLAccountNames)
                {
                    gla.Subaccounts = ApplyRecursiveNameFilter(gla.Subaccounts, numberedNameSearch);
                }
                
                return matchingGLAccountNames;

            }catch(Exception ex){
                Console.WriteLine(ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Attempts to change the IsActive Status of the GlAccount
        /// </summary>
        /// <param name="ID">GlAccount's ID</param>
        /// <param name="active">Active status to set to</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public override async Task<EntityActionChangeResponse<int>> SetActive(int ID, bool active, string connectionID)
        {
            var glAccount = await ctx.GLAccount.Include("Subaccounts").Where(g => g.ID == ID && g.BID == this.BID).FirstOrDefaultAsync();

            if (glAccount != null && glAccount.CanEdit)
            {
                if (!active)
                {
                    foreach (var glAccountSubaccount in glAccount.Subaccounts)
                    {
                        if (glAccountSubaccount.CanEdit)
                        {
                            glAccountSubaccount.IsActive = active;
                            ctx.GLAccount.Update(glAccountSubaccount);
                        }
                    }
                }

                return await base.SetActive(ID, active, connectionID);
            }
                

            return new EntityActionChangeResponse() { Id = ID, ErrorMessage = "Uneditable GLAccount Specified.", Success = false };
        }

        /// <summary>
        /// Appends a string before cloning the GLAccount
        /// </summary>
        /// <param name="clone">GlAccount to clone</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(GLAccount clone)
        {
            clone.Name = clone.Name + " (Clone)";
            await base.DoBeforeCloneAsync(clone);
        }

        internal async Task<IEnumerable<SimpleGLAccount>> SimpleList(SimpleGLAccountFilters filters)
        {
            await filters.DoBeforeSelectAsync(this.BID, this.ctx);

            return this.SelectWhereAll(filters.WherePredicates(), filters);
        }

        /// <summary>
        /// Tests a GLAccountNumber (nullable) for uniqueness within a Business (bid)
        /// </summary>
        /// <param name="glAccount">GLAccount to check</param>
        /// <returns>True if GLAccountNumber is null or is not on a glaccount already</returns>
        public bool IsGLAccountNumberUnique(GLAccount glAccount)
        {
            int? glaccountNumber = glAccount.Number;

            if (!glaccountNumber.HasValue)
                return true;

            var duplicateQuery = ctx.GLAccount.Where(t => t.BID == this.BID && t.Number == glaccountNumber.Value);

            if (glAccount.ID > 0)
                duplicateQuery = duplicateQuery.Where(gla => gla.ID != glAccount.ID);

            var duplicate = duplicateQuery.FirstOrDefault();

            return (duplicate == null);
        }

        /// <summary>
        /// Checks if a Parent is Subaccount
        /// </summary>
        /// <param name="glAccount">GlAccount to check</param>
        /// <returns></returns>
        public bool IsParentIDASubaccount(GLAccount glAccount)
        {
            if (glAccount.ID <= 0 || !glAccount.ParentID.HasValue)
                return false;

            return FindChildByID(glAccount.ID, glAccount.ParentID.Value) != null;
        }

        /// <summary>
        /// Finds the child by ID
        /// </summary>
        /// <param name="parentID">parent GlAccount ID</param>
        /// <param name="targetID">ID of GlAccount to find</param>
        /// <returns></returns>
        public GLAccount FindChildByID(int parentID, int targetID)
        {
            var subaccounts = ctx.GLAccount.Where(t => t.BID == this.BID && t.ParentID == parentID).ToArray();

            if (subaccounts == null || subaccounts.Length < 1)
            {
                return null;
            }
            else
            {
                //go ahead and loop through the subaccounts to see if we found it already
                foreach(GLAccount account in subaccounts)
                {
                    //if we did find it, early exit
                    if (account.ID == targetID)
                    {
                        return account;
                    }
                }
                foreach (GLAccount account in subaccounts)
                {
                    //otherwise look at my grandchildren
                    GLAccount myDescendent = FindChildByID(account.ID, targetID);
                    //and return that if they are one
                    if (myDescendent != null)
                        return myDescendent;
                }
                return null;
            }
        }

        internal override void DoBeforeValidate(GLAccount newModel)
        {
        }

        /// <summary>
        /// Finds the GLAccount by primary key
        /// </summary>
        /// <param name="query">GLAccount query</param>
        /// <param name="ID">ID of GlAccount to find</param>
        /// <returns></returns>
        public override IQueryable<GLAccount> WherePrimary(IQueryable<GLAccount> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }
    }
}
