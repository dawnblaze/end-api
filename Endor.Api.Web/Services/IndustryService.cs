﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Endor.RTM;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Service that wraps utilizes data storage layer and is accessed primarily from Controller layer
    /// </summary>
    public class IndustryService : AtomCRUDService<CrmIndustry, short>, ISimpleListableViewService<SimpleCrmIndustry, short>
    {
        /// <summary>
        /// Constructs a new IndustryService with a number of injected parameters
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public IndustryService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) :
            base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "ChildIndustries" };

        /// <summary>
        /// Simple List of Industries
        /// </summary>
        public DbSet<SimpleCrmIndustry> SimpleListSet => ctx.SimpleCrmIndustry;

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Overrides the mapping of the whole collection result
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override async Task<List<CrmIndustry>> MapCollection(List<CrmIndustry> collection, IExpandIncludes includes = null)
        {
            var optSvc = new OptionService(this.ctx, migrationHelper);
            bool lockFirstLevel = false;
            GetOptionValueResponse govr = await optSvc.Get(null, "Association.Industry.LockFirstLevel", BID, null, null, null, null, null);

            if (govr == null || !govr.Success)
            {
                return collection;
            }

            // Check Option Value, if true and isTopLevel, set IsLock = true
            if (bool.TryParse(govr.Value.Value, out bool result) && result)
            {
                lockFirstLevel = true;
            }
            else if (int.TryParse(govr.Value.Value, out int intResult) && intResult > 0)
            {
                lockFirstLevel = true;
            }
            else
            {
                lockFirstLevel = false;
            }

            collection.ForEach(async (item) => await this.MapIndustry(item, includes, lockFirstLevel));

            return collection;
        }

        private async Task MapIndustry(CrmIndustry item, IExpandIncludes includes = null, bool isFirstLevelLocked = false)
        {
            await base.MapItem(item, includes);

            if (item.IsTopLevel.GetValueOrDefault(false) && isFirstLevelLocked)
            {
                item.IsLocked = true;
            }
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<CrmIndustry, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<CrmIndustry, short>[]
            {
                CreateChildAssociation<IndustryService, CrmIndustry, short>((a)=>a.ChildIndustries)
            };
        }

        /// <summary>
        /// Sets the industry (and any applicable parents/children) to active or inactive.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="active"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public override async Task<EntityActionChangeResponse<short>> SetActive(short id, bool active, string connectionID)
        {
            try
            {
                var result = await base.SetActive(id, active, connectionID);

                if (result.Success)
                {
                    CrmIndustry industry = this.ctx.CrmIndustry
                                               .Include(t => t.ParentIndustry)
                                               .Include(t => t.ChildIndustries)
                                               .FirstOrDefault(t => t.BID == this.BID && t.ID == id);

                    if (active && industry?.ParentIndustry != null && industry?.ParentID != null)
                    {
                        await base.SetActive(industry.ParentID.Value, true, connectionID);
                    }

                    if (!active && industry?.ChildIndustries != null)
                    {
                        foreach (CrmIndustry ind in industry.ChildIndustries)
                            await base.SetActive(ind.ID, false, connectionID);
                    }

                    return new EntityActionChangeResponse<short>()
                    {
                        Id = id,
                        Message = "Successfully set Industry's Active status to " + (active ? "active" : "inactive"),
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        Id = id,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Id = id,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Sets the Industry's parent indutry
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> SetParent(short id, short? parentID)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                object[] myParams = {
                    new SqlParameter("@bid", this.BID),
                    new SqlParameter("@IndustryID", id),
                    parentID.HasValue? new SqlParameter("@ParentID", parentID.Value):new SqlParameter("@ParentID", DBNull.Value),
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Industry.Action.SetParent] @bid, @IndustryID, @ParentID;", parameters: myParams);
                if (rowResult > 0)
                {
                    await QueueIndexForModel(id);

                    base.DoGetRefreshMessagesOnUpdate(new CrmIndustry()
                    {
                        ClassTypeID = Convert.ToInt32(ClassType.CrmIndustry),
                        ID = Convert.ToInt16(id),
                        BID = this.BID,
                    });

                    return new EntityActionChangeResponse()
                    {
                        Id = id,
                        Message = "Successfully set Industry's Parent",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = id,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = id,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        internal override void DoBeforeValidate(CrmIndustry newModel)
        {
        }

        internal async Task<SimpleCrmIndustry[]> SimpleList(bool? isTopLevel, bool? isActive)
        {
            return await this.ctx.CrmIndustry
                .Where(t => t.IsTopLevel == (bool)(isTopLevel ?? t.IsTopLevel))
                .Where(t => t.IsActive == (bool)(isActive ?? t.IsActive))
                .Where(t => t.BID == BID)
                .Select(t => new SimpleCrmIndustry()
                {
                    BID = t.BID,
                    ID = t.ID,
                    ClassTypeID = t.ClassTypeID,
                    IsActive = t.IsActive,
                    IsDefault = false,
                    DisplayName = t.Name,
                })
                .ToArrayAsync();
        }

        /// <summary>
        /// Implements the primary lookup filter
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<CrmIndustry> WherePrimary(IQueryable<CrmIndustry> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }
    }
}
