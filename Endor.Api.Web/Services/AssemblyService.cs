﻿using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.CBEL.Common;
using Endor.CBEL.Exceptions;
using Endor.CBEL.ObjectGeneration;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Endor.CBEL.Metadata;
using Newtonsoft.Json.Linq;
using Endor.CBEL.Autocomplete;
using Endor.Models.Autocomplete;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// A service for <see cref="AssemblyData"/>
    /// </summary>
    public class AssemblyService : AtomCRUDService<AssemblyData, int>, ISimpleListableViewService<SimpleAssemblyData, int>, IPartCategoryLinkableService
    {
        private readonly AssemblyElementType[] inputElementTypes = new AssemblyElementType[]{
                AssemblyElementType.SingleLineText,
                AssemblyElementType.MultiLineString,
                AssemblyElementType.DropDown,
                AssemblyElementType.Number,
                AssemblyElementType.Checkbox
        };

        private readonly Lazy<AssemblyElementService> _elementService;
        private readonly Lazy<AssemblyCategoryService> _assemblyCategoryService;
        private readonly IAutocompleteEngine _autocompleteEngine;
        private ITaskQueuer _taskQueuer;

        /// <summary>
        /// Constructs a Assembly service with injected params
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <param name="autocompleteEngine">Autocomplete Engine</param>
        public AssemblyService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper, IAutocompleteEngine autocompleteEngine)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
            this._elementService = new Lazy<AssemblyElementService>(() => new AssemblyElementService(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper));
            this._assemblyCategoryService = new Lazy<AssemblyCategoryService>(() => new AssemblyCategoryService(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper));
            this._autocompleteEngine = autocompleteEngine;
            this._taskQueuer = taskQueuer;
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<AssemblyData, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<AssemblyData, int>[]
            {
                CreateChildAssociation<AssemblyLayoutService, AssemblyLayout, short>((a) => a.Layouts),
                CreateChildAssociation<AssemblyVariableService, AssemblyVariable, int>((a) => a.Variables),
                CreateChildAssociation<AssemblyTableService, AssemblyTable, short>((a) => a.Tables),
            };
        }

        internal override void DoBeforeValidate(AssemblyData newModel) {
            if (newModel != null && newModel.Variables != null && newModel.Variables.Count > 0)
            {
                foreach (AssemblyVariable variable in newModel.Variables)
                {
                    AssemblyVariableService.ValidateFormula(variable);
                }
            }

            if(newModel != null)
            {
                ValidateDestinationPropertiesAssembly(newModel);
            }

        }

        internal async Task<List<AssemblyData>> GetWithFiltersAsync(AssemblyFilter filters, AssemblyDataIncludes includes)
        {
            if (filters != null)
            {
                var results = await this.Where(filters.WherePredicates(), false, includes).ToListAsync();
                return await this.MapCollection(results, includes);
            }

            return await this.GetAsync(includes);
        }

        /// <summary>
        /// Gets simple list set
        /// </summary>
        public DbSet<SimpleAssemblyData> SimpleListSet => this.ctx.SimpleAssemblyData;

        /// <summary>
        /// Get using list of IDs and CategoryIDs as filter
        /// </summary>
        /// <param name="IDs"></param>
        /// <param name="CategoryIDs"></param>
        /// <returns></returns>
        public async Task<ICollection<SimpleAssemblyData>> GetByIDsAndCategoriesAsync(ICollection<int> IDs, ICollection<int> CategoryIDs)
        {
            var result = await this.ctx.AssemblyData
                            .Include(e => e.AssemblyCategoryLinks)
                            .Select(e => new
                            {
                                data = e,
                                CategoryIDs = e.AssemblyCategoryLinks.Select(catlink => catlink.CategoryID)
                            }).Where(q =>
                                q.data.BID == this.BID &&
                                q.data.IsActive &&
                                (IDs.Contains(q.data.ID) || q.CategoryIDs.Any(catID => CategoryIDs.Contains(catID)))
                            ).Select(q => new SimpleAssemblyData()
                            {
                                BID = q.data.BID,
                                ClassTypeID = q.data.ClassTypeID,
                                DisplayName = q.data.Name,
                                ID = q.data.ID,
                                IsActive = q.data.IsActive,
                                IsDefault = false
                            }).ToListAsync();
            return result;
        }

        /// <summary>
        /// Map the relevent data into the given Map Item
        /// </summary>
        public override Task MapItem(AssemblyData item, IExpandIncludes includeExpander = null)
        {

            var includes = includeExpander?.GetIncludes();
            if (item != null && includes != null &&
                includeExpander is AssemblyDataIncludes)
            {
                if ((includeExpander as AssemblyDataIncludes).IncludeLayouts == IncludesLevel.Full)
                {
                    MapLayoutChildrenToTree(item);
                }
                if ((includeExpander as AssemblyDataIncludes).IncludeCategories == IncludesLevel.Full)
                {
                    item.AssemblyCategories = item.AssemblyCategoryLinks.Select(x => x.AssemblyCategory).ToArray();
                    item.AssemblyCategoryLinks = null;
                }
                else if ((includeExpander as AssemblyDataIncludes).IncludeCategories == IncludesLevel.Simple)
                {
                    item.SimpleAssemblyCategories = item.AssemblyCategoryLinks.Select(link => new SimpleAssemblyCategory()
                    {
                        BID = link.AssemblyCategory.BID,
                        ID = link.AssemblyCategory.ID,
                        ClassTypeID = link.AssemblyCategory.ClassTypeID,
                        IsActive = link.AssemblyCategory.IsActive,
                        IsDefault = false,
                        DisplayName = link.AssemblyCategory.Name
                    }).ToArray();
                    item.AssemblyCategoryLinks = null;
                }
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// turns <see cref="AssemblyData.Layouts"/>.Elements from flat to tree
        /// </summary>
        /// <param name="item"></param>
        private void MapLayoutChildrenToTree(AssemblyData item)
        {
            if (item?.Layouts?.Any() ?? false)
            {
                foreach (AssemblyLayout layout in item.Layouts)
                {
                    AssemblyLayoutService.MapChildTree(layout);
                }
            }
        }

        /// <summary>
        /// Links or Unlinks a Assembly with a Assembly Category
        /// </summary>
        /// <param name="AssemblyDataID">Assembly's ID</param>
        /// <param name="AssemblyCategoryID">Assembly Category's ID</param>
        /// <param name="isLinked">If it is to link or unlink</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkAssemblyCategory(int AssemblyDataID, short AssemblyCategoryID, bool isLinked)
        {
            /*          
                @BID
                @AssemblyCategoryID
                @AssemblyDataID
                @IsLinked
                @Result Output
            */
            try
            {
                SqlParameter Result = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                object[] myParams = {
                    new SqlParameter("@BID", this.BID),
                    new SqlParameter("@SubassemblyCategoryID", AssemblyCategoryID),
                    new SqlParameter("@SubassemblyDataID", AssemblyDataID),
                    new SqlParameter("@IsLinked", isLinked),
                    Result
                };

                int affectedRows = await ctx.Database.ExecuteSqlRawAsync(
                    "EXEC dbo.[Part.Subassembly.Category.Action.LinkSubassembly] @BID, @SubassemblyCategoryID, @SubassemblyDataID, @IsLinked, @Result Output;",
                    parameters: myParams);

                if (affectedRows > 0)
                {
                    await QueueIndexForModel(AssemblyCategoryID);

                    return new EntityActionChangeResponse()
                    {
                        Id = AssemblyCategoryID,
                        Message = $"Successfully {(isLinked ? "linked" : "unlinked")} AssemblyCategory:{AssemblyCategoryID} to AssemblyData:{AssemblyDataID}",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = AssemblyCategoryID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }

            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        /// <summary>
        /// END-8634: Prevent the user from Creating Variables with the following names:
        /// LayoutType, Instance, Profile
        /// and failing with a message "Invalid Variable Name {name}" response message.
        /// The update could also check but it would have to be coded to allow
        /// any variables with an ID to be saved since they aren't new
        /// </summary>
        /// <param name="assemblyVariables"></param>
        /// <returns></returns>
        internal async  Task<VariableValidationResult> ValidateVariables(ICollection<AssemblyVariable> assemblyVariables)
        {
            VariableValidationResult variableValidationResult = new VariableValidationResult() { Valid = true };
            List<string> restrictedNames = new List<string>() { "layouttype", "instance", "profile" };

            if (assemblyVariables != null)
            {
                foreach (AssemblyVariable assemblyVariable in assemblyVariables)
                {
                    if (assemblyVariable.ID <= 0 && !string.IsNullOrWhiteSpace(restrictedNames.FirstOrDefault(name => name == assemblyVariable.Name.ToLowerInvariant())))
                    {
                        variableValidationResult.Valid = false;
                        variableValidationResult.Response = new GenericResponse<AssemblyData>() { ErrorMessage = $"Invalid Variable Name {assemblyVariable.Name}" };
                        return await Task.FromResult(variableValidationResult);
                    }
                }
            }

            return variableValidationResult;
        }

        internal Task<AssemblyLayout[]> GetLayouts(int assemblyID, AssemblyLayoutFilter filter)
        {
            return ctx.AssemblyLayout.Include("Elements").WherePrimary(this.BID, x => x.AssemblyID == assemblyID).WhereAll(filter.WherePredicates()).ToArrayAsync();
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<AssemblyData> WherePrimary(IQueryable<AssemblyData> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public async override Task DoBeforeUpdateAsync(AssemblyData oldModel, AssemblyData newModel)
        {
            CheckAssemblyNameUnique(newModel.Name, newModel.AssemblyType, oldModel.ID);

            if (newModel?.Tables?.Any() ?? false)
            {
                foreach (var assemblyTable in newModel.Tables)
                {
                    if(assemblyTable.IsTierTable)
                    {
                        assemblyTable.RowMatchType = AssemblyTableMatchType.UseHigherValue;
                        if (assemblyTable.VariableName.IndexOf("Tier", StringComparison.Ordinal) == -1)
                            assemblyTable.VariableName = $"Tier{assemblyTable.VariableName}";
                    }
                }
            }

            CBELBaseAssemblyGenerator newModelAssemblyGen;
            if (newModel.AssemblyType == AssemblyType.MachineTemplate)
                newModelAssemblyGen = CBELMachineGenerator.Create(ctx, newModel);
            else
                newModelAssemblyGen = CBELAssemblyGenerator.Create(ctx, newModel);

            var compilationValidation = await newModelAssemblyGen.TestAssemblyCompilation();
            if (!compilationValidation.Success)
                throw new CBELCompilationException("Assembly Validation Failed", compilationValidation);

            await base.DoBeforeUpdateAsync(oldModel, newModel);
            FixupAssemblyIDOnChildElements(newModel, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public async override Task DoBeforeCreateAsync(AssemblyData newModel, Guid? tempGuid = null)
        {
            CheckAssemblyNameUnique(newModel.Name, newModel.AssemblyType);

            if (newModel?.Tables?.Any() ?? false)
            {
                foreach (var assemblyTable in newModel.Tables)
                {
                    if (assemblyTable.TableType != AssemblyTableType.Custom)
                    {
                        if (assemblyTable.IsTierTable)
                        {
                            assemblyTable.RowMatchType = AssemblyTableMatchType.UseHigherValue;

                        }
                    }
                }
            }

            //before we get IDs for our whole tree, look for any existing variables with saved IDs
            List<Tuple<int, AssemblyVariable>> existingVariables = newModel.Variables?.Where(x => x.ID != UnsavedEntityID).Select(x => new Tuple<int, AssemblyVariable>(x.ID, x)).ToList();
            //now go get IDs
            await base.DoBeforeCreateAsync(newModel, tempGuid);
            //now our newModel.Variables have updated IDs, and our existingVariables collection lets us map old IDs to new ones

            FixupAssemblyIDOnChildElements(newModel, existingVariables);

            CBELBaseAssemblyGenerator newModelAssemblyGen;
            if (newModel.AssemblyType == AssemblyType.MachineTemplate)
                newModelAssemblyGen = CBELMachineGenerator.Create(ctx, newModel);
            else
                newModelAssemblyGen = CBELAssemblyGenerator.Create(ctx, newModel);
            var compilationValidation = await newModelAssemblyGen.TestAssemblyCompilation();

            if (newModel?.Tables?.Any() ?? false)
            {
                foreach (var assemblyTable in newModel.Tables)
                {
                    if (assemblyTable.IsTierTable)
                    {
                        if (assemblyTable.VariableName.IndexOf("Tier", StringComparison.Ordinal) == -1)
                        {
                            assemblyTable.VariableName = $"Tier{assemblyTable.VariableName}";
                        }
                    }
                }
            }


            if (!compilationValidation.Success)
                throw new CBELCompilationException("Assembly Validation Failed", compilationValidation);
        }
        
        /// <summary>
        /// Checks assembly name if its unique on BID. Throws InvalidOperationException error 
        /// </summary>
        /// <param name="assemblyName">name to checck</param>
        /// <param name="assemblyType">assembly type to checck</param>
        /// <param name="assemblyID">(Optional) use if updating assembly</param>
        private void CheckAssemblyNameUnique(string assemblyName, AssemblyType assemblyType, int assemblyID = 0)
        {
            var checkSameName = ctx.AssemblyData.FirstOrDefault(a => a.BID == BID
                && a.Name.ToLower() == assemblyName.ToLower()
                && a.AssemblyType == assemblyType
                && a.ID != assemblyID);
            if (checkSameName != null)
            {
                throw new InvalidOperationException("Assembly has a similar name");
            }
        }

        /// <summary>
        /// Fixes up AssemblyElement.AssemblyID and AssemblyElement.VariableID because EF isn't managing those
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="clonedFromVariables">a list of tuples (oldVariableID, variableObject) that came from the cloned-from AssemblyData, only pass this during a clone operation</param>
        private static void FixupAssemblyIDOnChildElements(AssemblyData newModel, List<Tuple<int, AssemblyVariable>> clonedFromVariables = null)
        {
            Dictionary<Guid, int> VariableTempIDToID = newModel.Variables?.Where(x => x.TempID.HasValue).ToDictionary(x => x.TempID.Value, y => y.ID) ?? new Dictionary<Guid, int>();

            //grandchild fixup
            //EF sets the Layout's AssemblyID because it knows about it
            //but it doesn't fix up the individual Elements
            if (newModel.Layouts != null && newModel.Layouts.Count > 0)
            {
                foreach (var layout in newModel.Layouts)
                {
                    if (layout.Elements != null && layout.Elements.Count > 0)
                    {
                        RecurseFixupAssemblyID(layout.Elements, newModel.ID, VariableTempIDToID, clonedFromVariables);
                    }
                }
            }
        }

        private static void RecurseFixupAssemblyID(
            ICollection<AssemblyElement> collection,
            int assemblyID,
            Dictionary<Guid, int> variableTempIDToID,
            List<Tuple<int, AssemblyVariable>> clonedFromVariables = null)
        {
            if (collection != null && collection.Count > 0)
            {
                foreach (var element in collection)
                {
                    element.AssemblyID = assemblyID;
                    if (element.TempVariableID.HasValue)
                    {
                        if (variableTempIDToID.ContainsKey(element.TempVariableID.Value))
                        {
                            element.VariableID = variableTempIDToID[element.TempVariableID.Value];
                        }
                    }

                    //CLONE CODE
                    //when cloning, our elements have these VariableID references that need new IDs
                    if (clonedFromVariables != null && element.VariableID != null)
                    {
                        //if our element referenced an existing variable
                        AssemblyVariable linkedVariable = clonedFromVariables.FirstOrDefault(x => x.Item1 == element.VariableID)?.Item2;
                        //and our current linkedVariable object ID doesn't match what we currently have
                        if (linkedVariable != null && element.VariableID != linkedVariable.ID)
                        {
                            //replace it with the updated variable ID
                            element.VariableID = linkedVariable.ID;
                        }
                    }
                    RecurseFixupAssemblyID(element.Elements, assemblyID, variableTempIDToID, clonedFromVariables);
                }
            }
        }

        /// <summary>
        /// Actions to perform before deleting a Assembly and its Elements
        /// </summary>
        /// <param name="model">Assembly to delete</param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(AssemblyData model)
        {
            // we must load Layouts and their Elements
            // because the IncludeDefaults no longer include them
            await ctx.Entry(model).Collection(x => x.Layouts).LoadAsync();
            if (model.Layouts?.Any() ?? false)
            {
                foreach(var layout in model.Layouts)
                {
                    await ctx.Entry(layout).Collection(x => x.Elements).LoadAsync();
                }
            }

            await ctx.Entry(model).Collection(x => x.Tables).LoadAsync();

            // go ahead and delete these because they don't have a childAssociation
            var existingLinks = await this.ctx.AssemblyCategoryLink.Where(link => link.BID == this.BID && link.PartID == model.ID).ToListAsync();
            this.ctx.AssemblyCategoryLink.RemoveRange(existingLinks);

            await ctx.Entry(model).Collection(x => x.Variables).LoadAsync();
            if (model.Variables?.Any() ?? false)
            {
                foreach (var variable in model.Variables)
                {
                    await ctx.Entry(variable).Collection(x => x.Formulas).LoadAsync();
                }
            }

            await base.DoBeforeDeleteAsync(model);
        }

        /// <summary>        
        /// turns <see cref="AssemblyData.Layouts"/>.Elements from flat to tree
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(AssemblyData oldModel, AssemblyData newModel, string connectionID)
        {
            if (newModel != null)
            {
                MapLayoutChildrenToTree(newModel);
            }

            await this.CreateAndSaveCSandDLL(newModel);
            if (newModel.AssemblyType == AssemblyType.MachineTemplate)
                this.UpdateMachinesWithTemplate(newModel);

            await base.DoAfterUpdateAsync(oldModel, newModel, connectionID);
            await this.LinkOrUnlinkCategories(newModel);

            if (newModel?.Tables?.Any() ?? false)
            {
                foreach (var assemblyTable in newModel.Tables)
                {
                    if (assemblyTable.IsTierTable)
                    {
                        if (assemblyTable.VariableName.IndexOf("Tier", StringComparison.Ordinal) == -1)
                        {
                            assemblyTable.VariableName = $"Tier{assemblyTable.VariableName}";
                        }
                    }
                }
            }
        }

        /// <summary>
        /// LinkOrUnlinkCategories
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        protected async Task LinkOrUnlinkCategories(AssemblyData assembly)
        {
            if(assembly.SimpleAssemblyCategories!=null)
            {
                var catSvc = this._assemblyCategoryService.Value;
                var oldCategoryIDs = await this.ctx.AssemblyCategoryLink
                                    .AsNoTracking()
                                    .Where(cat => cat.BID==this.BID &&
                                        cat.PartID == assembly.ID
                                    )
                                    .Select(x => x.CategoryID)
                                    .ToArrayAsync();

                await this.LinkOrUnlinkCategories(catSvc.LinkAssembly, assembly.ID, oldCategoryIDs, assembly.SimpleAssemblyCategories.Select(y => y.ID).ToArray(), false);
            }
        }

        /// <summary>
        /// turns <see cref="AssemblyData.Layouts"/>.Elements from flat to tree
        /// </summary>
        /// <param name="model"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(AssemblyData model, Guid? tempGuid)
        {
            if (model != null)
            {
                MapLayoutChildrenToTree(model);

                await this.CreateAndSaveCSandDLL(model);

                if (model.AssemblyType == AssemblyType.MachineTemplate)
                    UpdateMachinesWithTemplate(model);
            }

            await base.DoAfterAddAsync(model, tempGuid);
        }

        /// <summary>
        /// Create and Save CS and DLL
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> CreateAndSaveCSandDLL(AssemblyData model)
        {
            try
            {
                //Create CBELAssemblyGenerator
                CBELAssemblyGenerator assemblyGenerator = CBELAssemblyGenerator.Create(ctx, model);
                //Save off files    
                var compilationResult = await assemblyGenerator.Save(cache);
                return compilationResult.Success;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Error during creation or save of Assembly: " + e.Message, e);
                return false;
            }
        }

        /// <summary>
        /// Gets the old model
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task<AssemblyData> GetOldModelAsync(AssemblyData newModel)
        {
            IQueryable<AssemblyData> oldModelQry = ctx.AssemblyData
                                                    .Include(x => x.Variables)
                                                    .Include(x => x.Layouts)
                                                        .ThenInclude(l => l.Elements)
                                                    .Include(x => x.Tables)
                                                    .AsNoTracking();
            var oldModel = await WherePrimary(oldModelQry, newModel.ID).FirstOrDefaultAsync();

            MapLayoutChildrenToTree(oldModel);

            return oldModel;
        }


        public override async Task<AssemblyData> GetAsync(int ID, IExpandIncludes includes = null)
        {
            var includedQ = ctx.Set<AssemblyData>().IncludeAll(GetIncludes(includes));
            AssemblyData result = await WherePrimary(includedQ, ID).FirstOrDefaultAsync();
//            if (result?.Tables?.Any() ?? false)
//            {
//                foreach (var assemblyTable in result.Tables)
//                {
//                    if (assemblyTable.TableType != AssemblyTableType.Custom && assemblyTable.VariableName.IndexOf("Tier", StringComparison.Ordinal) == 0)
//                    {
//                        switch (result.PriceFormulaType)
//                        {
//                            case PriceFormulaType.Custom:
//                                switch (result.PricingType)
//                                {
//                                    case AssemblyPricingType.MarketBasedPricing:
//                                        assemblyTable.VariableName = "TierDiscountTable";
//                                        break;
//
//                                    case AssemblyPricingType.MarginBasedPricing:
//                                        assemblyTable.VariableName = "TierMarginTable";
//                                        break;
//
//                                    case AssemblyPricingType.MarkupBasedPricing:
//                                        assemblyTable.VariableName = "TierMarkupTable";
//                                        break;
//                                }
//                                break;
//
//                            case PriceFormulaType.PriceTable:
//                                assemblyTable.VariableName = "TierPriceTable";
//                                break;
//                            case PriceFormulaType.DiscountTable:
//                                assemblyTable.VariableName = "TierDiscountTable";
//                                break;
//                            case PriceFormulaType.MarginTable:
//                                assemblyTable.VariableName = "TierMarginTable";
//                                break;
//                            case PriceFormulaType.MarkupTable:
//                                assemblyTable.VariableName = "TierMarkupTable";
//                                break;
//                        }
//                    }
//                }
//            }

            if (result != null)
            {
                await this.MapItem(result, includes);
            }

            return result;
        }


        /// <summary>
        /// Checks if the Assembly can be deleted
        /// </summary>
        /// <param name="id">Assembly's ID</param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(int id)
        {
            var assembly = await this.GetAsync(id, new AssemblyDataIncludes()
            {
                IncludeCategories = IncludesLevel.Full,
            });
            if (assembly == null)
            {
                await this.QueueIndexForModel(id);
                return new BooleanResponse()
                {
                    Message = "Assembly Not Found",
                    Success = false
                };
            }
            int linkedAssemblyVariables = this.ctx.AssemblyVariable.Where(x => x.BID == this.BID && x.LinkedAssemblyID == id).Count();
            if (linkedAssemblyVariables > 0)
                return new BooleanResponse()
                {
                    Message = "Assembly is associated with one or more Variables.",
                    Success = true,
                    Value = false
                };

            // is linked to an order is determined how?

            return new BooleanResponse()
            {
                Message = "Assembly can be deleted.",
                Success = true,
                Value = true
            };
        }


        /// <summary>
        /// get includes for cloning
        /// </summary>
        /// <returns></returns>
        public override string[] GetIncludesForClone()
        {
            return new string[] {
                nameof(AssemblyData.AssemblyCategoryLinks),
                nameof(AssemblyData.Layouts),
                nameof(AssemblyData.Layouts)+"."+ nameof(AssemblyLayout.Elements),
                nameof(AssemblyData.Variables),
                nameof(AssemblyData.Tables)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(AssemblyData clone)
        {
            //clone.Name = GetNextClonedName(clone.Name, t => t.Name);
            clone.Name = GetNextClonedName(ctx.Set<AssemblyData>().AsNoTracking().Where(t => t.BID == BID && t.Name.StartsWith(t.Name)).ToList(), clone.Name, t => t.Name);
            try
            {
                foreach (AssemblyLayout l in clone.Layouts ?? new List<AssemblyLayout>())
                {
                    AssemblyLayoutService.MapChildTree(l);
                }

                await base.DoBeforeCloneAsync(clone);
            }
            catch (Exception e)
            {
                await this.logger.Error(this.BID, "Assembly Do After Clone Failed: " + e.Message, e);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clonedFromID"></param>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(int clonedFromID, AssemblyData clone)
        {
            //Copy All Files
            DocumentManager client = base.GetDocumentManager(clonedFromID, (ClassType)clone.ClassTypeID, BucketRequest.All);
            await client.CloneAllDocumentsToClassTypeAsync(clone.ID, clone.ClassTypeID, includeSpecialFolderBlobName: true);
            if (clone.AssemblyType == AssemblyType.MachineTemplate)
                this.UpdateMachinesWithTemplate(clone);
            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        /// <summary>
        /// Overridden to rethrow CBELExceptions
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        protected override async Task<AssemblyData> HandleCreateException(Exception e)
        {
            if (e is CBELException)
                throw e;
            if (e is InvalidOperationException && e.Message == "Assembly has a similar name")
                throw e;

            return await base.HandleCreateException(e);
        }

        /// <summary>
        /// Overridden to rethrow CBELExceptions
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        protected override async Task<AssemblyData> HandleUpdateException(Exception e)
        {
            if (e is CBELException)
                throw e;
            if (e is InvalidOperationException && e.Message == "Assembly has a similar name")
                throw e;

            return await base.HandleUpdateException(e);
        }

        private bool IsEmpty(string property)
        {
            if (String.IsNullOrEmpty(property) || String.IsNullOrWhiteSpace(property))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private AssemblyData ValidateDestinationPropertiesAssembly(AssemblyData newModel)
        {
            if (newModel.EnableShipByDate == true && IsEmpty(newModel.ShipByDateLabel))
            {
                newModel.ShipByDateLabel = "Ship By Date";
            }
            if (newModel.EnableArriveByDate == true && IsEmpty(newModel.ArriveByDateLabel))
            {
                newModel.ArriveByDateLabel = "Arrive By Date";
            }
            if (newModel.EnableFromAddress == true && IsEmpty(newModel.FromAddressLabel))
            {
                newModel.FromAddressLabel = "From Address";
            }
            if (newModel.EnableToAddress == true && IsEmpty(newModel.ToAddressLabel))
            {
                newModel.ToAddressLabel = "To Address";
            }
            if (newModel.EnableBlindShipping == true && IsEmpty(newModel.BlindShippingLabel))
            {
                newModel.BlindShippingLabel = "Blind Ship From Address";
            }
            return newModel;
        }

        /// <summary>
        /// Validates the assembly
        /// </summary>
        /// <param name="model">Assembly Data</param>
        /// <returns></returns>
        public async Task<AssemblyCompilationResponse> Validate(AssemblyData model)
        {
            model.BID = BID;
            try
            {
                CBELAssemblyGenerator assemblyGenerator = CBELAssemblyGenerator.Create(ctx, model);
                var compileTestResult = await assemblyGenerator.TestAssemblyCompilation();
                return compileTestResult;
            }
            catch (Exception e)
            {
                return new AssemblyCompilationResponse(model?.Name)
                {
                    Errors = new List<PricingErrorMessage>()
                    {
                        new PricingErrorMessage()
                        {
                            Message = e.Message,
                        }
                    },
                };
            }
        }

        /// <summary>
        /// Remove all Assembly Category Links
        /// </summary>
        public async Task unlinkAllCategoryAsync(int assemblyID)
        {
            var existingLinks = this.ctx.AssemblyCategoryLink.Where(link => link.PartID == assemblyID);
            if (existingLinks.Count() > 0)
            {
                foreach (var link in existingLinks)
                {
                    this.ctx.AssemblyCategoryLink.Remove(link);
                }
                await ctx.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Retrieves the member list of the assembly used for code-complete and case-correction.  If the ID is not specifid, the method returns the list of all Members on a generic assembly (without any variables).
        /// </summary>
        /// <param name="ID">Assembly ID</param>
        public async Task<SortedDictionary<string, ICBELMember>> GetMemberList(int? ID)
        {
            AssemblyData assemblyData = null;

            if (ID.HasValue)
            {
                AssemblyDataIncludes includes = new AssemblyDataIncludes();
                includes.IncludeCategories = IncludesLevel.Full;
                includes.IncludeLayouts = IncludesLevel.Full;
                includes.IncludeLinkedAssemblies = IncludesLevel.Full;
                includes.IncludeLinkedParts = IncludesLevel.Full;
                includes.IncludeTables = IncludesLevel.Full;
                includes.IncludeVariables = IncludesLevel.Full;
                assemblyData = await GetAsync(ID.Value, includes);

                if (assemblyData != null)
                {
                    if (assemblyData.Variables != null)
                    {
                        foreach (AssemblyVariable asv in assemblyData.Variables)
                        {
                            asv.Assembly = null;
                        }
                    }
                }
                else
                {
                    return new SortedDictionary<string, ICBELMember>();
                }
            }

            SortedDictionary<string, ICBELMember> memberList = MetadataHelper.KnownMemberList(assemblyData);

            return memberList;
        }

        /// <summary>
        /// returns list of errors
        /// </summary>
        /// <param name="json"></param>
        /// <param name="assemblyID"></param>
        /// <param name="layoutType"></param>
        /// <returns></returns>
        public async Task<ICollection<string>> ValidateAssemblyDataJSON(string json, int assemblyID, AssemblyLayoutType? layoutType = null){
            var err = new HashSet<string>();
            var parsedData = JsonConvert.DeserializeObject<JObject>(String.IsNullOrWhiteSpace(json)? "{}":json);
            var variableData = (JArray)parsedData["VariableData"] ?? new JArray();
            var assembly = await this.ctx.AssemblyData
                                .WherePrimary(this.BID, assemblyID)
                                .Include(a => a.Layouts)
                                .ThenInclude(l => l.Elements)
                                .ThenInclude(el => el.Variable)
                                .FirstOrDefaultAsync();
            
            var inputElements = assembly.Layouts
                                .Where(l => layoutType == null || l.LayoutType == layoutType)
                                .SelectMany(l => l.Elements)
                                .Where(l => this.inputElementTypes.Contains(l.ElementType));
            
            foreach (var elem in inputElements)
            {   
                var data = variableData.FirstOrDefault(v => v["Name"].ToString() == elem.VariableName);
                if(elem.Variable.IsRequired && (data?["Value"]?.ToString()?.Length ?? 0)==0)
                {
                    err.Add($"{elem.Variable.Label} is required");
                }
            }

            return err;
        }

        /// <summary>
        /// returns EmbeddedAssembly specified by ID
        /// </summary>
        /// <param name="embeddedAssemblySingleton"></param>
        /// <param name="ID"></param>
        public AssemblyData GetEmbeddedAssembly(EmbeddedAssemblySingleton embeddedAssemblySingleton, int ID)
        {
            return embeddedAssemblySingleton.GetEmbeddedAssembly((EmbeddedAssemblyType)ID);
        }

        /// <summary>
        /// Clones a model from a given ID
        /// </summary>
        /// <param name="ID">ID of the model to clone</param>
        /// <returns></returns>
        public async Task<AssemblyData> CloneAssembly(int ID)
        {
            try
            {
                AssemblyData clone = await this.GetClone(ID);

                await DoBeforeCloneAsync(clone);
                
                // Clone Custom Fields
                // Putting this in here since I can't save Custom Field 
                // Check screenshot on ticket END-8210 for the error it's returning when I tried to do ctx.SaveChanges() on DoAfterCloneAsync Function
                CustomFieldService customFieldService = new CustomFieldService(this.ctx, this.logger, this.rtmClient);
                var customFieldList = customFieldService.ReadCustomFields(this.BID, (int)ClassType.Assembly, ID, new CustomFieldValueFilters());

                if (customFieldList != null)
                {
                    // Im pretty sure that the DoBeforeCloneAsync() function above it calling the `RequestIDAsync` which gets the latest ID.
                    // Don't know why after save the cloneAfterSave.ID changes to +1 (example: 1300 becomes 1301)
                    int cloneID = clone.ID;
                    customFieldService.UpdateCustomFields(this.BID, (int)ClassType.Assembly, cloneID + 1, customFieldList.ToArray(), true);
                }

                AssemblyData cloneAfterSave = await this.CreateAsync(clone, null);

                await DoAfterCloneAsync(ID, cloneAfterSave);

                return cloneAfterSave;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Clone failed", e);
                //return a more meaningful message...
                throw new Exception($"Clone failed - {e.Message}", e);
            }
        }

        private void UpdateMachinesWithTemplate(AssemblyData model)
        {
            //update any machines with this template
            var machineList = ctx.MachineData.Where(m => m.BID == this.BID && m.MachineTemplateID == model.ID);
            foreach (var machine in machineList.ToList())
            {
                _ = this.taskQueuer.RegenerateCBELForMachine(this.BID, machine.ID);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="variableID"></param>
        /// <param name="machineID"></param>
        /// <param name="profileName"></param>
        /// <returns></returns>
        public async Task<ICollection<NamedQuark>> GetByVariableAndProfile(int variableID, int? machineID, string profileName)
        {
            return await AssemblyHelper.GetListValues<ICollection<NamedQuark>>(ctx, logger, BID, variableID, machineID, profileName);
        }

        /// <summary>
        /// returns the class definition
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<string> GetDefinition_Class(int id)
        {
            return await _autocompleteEngine.ClassDefinition(id, ClassType.Assembly.ID());
        }

        /// <summary>
        /// returns the local definition
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<string> GetDefinition_Local(int id)
        {
            return await _autocompleteEngine.LocalDefinition(id, ClassType.Assembly.ID());
        }

        /// <summary>
        /// returns the local definition from path given
        /// </summary>
        /// <param name="id"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public async Task<string> GetDefinition_Local_Path(int id, string path)
        {
            return await _autocompleteEngine.GetAssemblyByID(id, path);
        }

        /// <summary>
        /// returns the local definition from path given
        /// </summary>
        /// <param name="assemblyVariables"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public async Task<string> PostDefinition_Local_Path(List<AssemblyVariableAutocompleteBrief> assemblyVariables, string path)
        {
            return await _autocompleteEngine.AssemblyVariables(assemblyVariables, path);
        }

        /// <summary>
        /// returns the server definition
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<string> GetDefinition_Server(int id)
        {
            return await _autocompleteEngine.ServerDefinition(id, ClassType.Assembly.ID());
        }

        /// <summary>
        /// returns the variables' local definition
        /// </summary>
        /// <param name="assemblyVariableAutocompleteBrief"></param>
        /// <returns></returns>
        public async Task<string> GetVariables_Local(List<AssemblyVariableAutocompleteBrief> assemblyVariableAutocompleteBrief)
        {
            return await _autocompleteEngine.LocalVariableDefinitions(assemblyVariableAutocompleteBrief);
        }

        /// <summary>
        /// deletes a specific DLL
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="ctid"></param>
        /// <param name="id"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public async Task<string> DeleteDLL(short bid, int ctid, int id, int? userID)
        {
            await this.logger.Information(bid, $"Deletion of DLL triggered by User: {userID} for CTID: {ctid}, ID: {id}");
            return await _taskQueuer.ClearAssembly(bid, ctid, id);
        }
        /// <summary>
        /// deletes all dlls
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public async Task<string> DeleteAllDLL(short bid, int? userID)
        {
            await this.logger.Information(bid, $"Deletion of all DLLs triggered by User: {userID}");
            return await _taskQueuer.ClearAllAssemblies(bid);
        }
    }
}
