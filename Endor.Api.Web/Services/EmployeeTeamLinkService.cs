﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// EmployeeTeamLink Service
    /// </summary>
    public class EmployeeTeamLinkService : LinkCRUDService<EmployeeTeamLink>, ICloneableChildCRUDService<EmployeeData, EmployeeTeamLink>
    {
        /// <summary>
        /// EmployeeTeamLink Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="bid">Business ID</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public EmployeeTeamLinkService(ApiContext context, RemoteLogger logger, short bid, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, logger, bid, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Sets the ID of childModel to a new ID 
        /// </summary>
        /// <param name="parentModel"></param>
        /// <param name="childModel"></param>
        /// <returns></returns>
        public Task DoBeforeCloneForParent(EmployeeData parentModel, EmployeeTeamLink childModel)
        {
            childModel.EmployeeID = parentModel.ID;

            return Task.CompletedTask;
        }

        /// <summary>
        /// Gets an untracked snapshot of the EmployeeTeamLink before new updates are made
        /// </summary>
        /// <param name="newModel">Updated version of the EmployeeTeamLink</param>
        /// <returns></returns>
        public override async Task<EmployeeTeamLink> GetOldModelAsync(EmployeeTeamLink newModel)
        {
            return await ctx.EmployeeTeamLink.AsNoTracking().FirstOrDefaultAsync(x => x.BID == newModel.BID && x.TeamID == newModel.TeamID && x.EmployeeID == newModel.EmployeeID);
        }
    }
}
