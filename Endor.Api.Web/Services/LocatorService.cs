﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Models;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Base controller for Locator types
    /// </summary>
    /// <typeparam name="L">Locator type</typeparam>
    /// <typeparam name="LPI">Locator Parent ID type</typeparam>
    /// <typeparam name="LP">Locator Parent type</typeparam>
    public abstract class BaseLocatorService<L, LPI, LP> : AtomCRUDService<L, int>, ICloneableChildCRUDService<LP, L>
        where L : BaseLocator<LPI, LP>, IAtom<int>,IImageCandidate
        where LPI : struct, IConvertible 
        where LP : class, IAtom<LPI>
    {
        /// <summary>
        /// base locator service ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public BaseLocatorService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// the parent classtype for this locator
        /// </summary>
        protected abstract ClassType ParentClassType { get; }

        /// <summary>
        /// returns a list of include strings
        /// including defaults
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// expression builder to get a list of locators given a parent ID
        /// this takes care of building the ParentID clause
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<List<L>> GetForParent(LPI id)
        {
            ParameterExpression locatorTypeExp = Expression.Parameter(typeof(L));
            Expression<Func<L, bool>> lambda = 
                Expression.Lambda<Func<L, bool>>(
                    Expression.Equal(
                        Expression.Property(locatorTypeExp, "ParentID"), 
                        Expression.Constant(id, typeof(LPI))), 
                    locatorTypeExp);
            
            return ctx.Set<L>().IncludeAll(GetIncludes()).Where(m => m.BID == BID).Where(lambda).ToListAsync();
        }

        /// <summary>
        /// returns child associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<L, int>[] GetChildAssociations()
        {
            return null;
        }

        /// <summary>
        /// throws an error if email already exists in this locator type table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected async virtual Task CheckForDuplicateEmail(L model)
        {
            await Task.Yield();
            /*
            if (model.eLocatorType == LocatorType.Email)
            {
                var dupe = await ctx.Set<L>().Where(
                    l =>
                    l.RawInput == model.RawInput &&
                    l.ID != model.ID &&
                    l.BID == model.BID).FirstOrDefaultAsync();

                if (dupe != null)
                    throw new InvalidOperationException("Email already exists");
            }
            */
        }

        /// <summary>
        /// before update hook
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async override Task DoBeforeUpdateAsync(L oldModel, L model)
        {
            await CheckForDuplicateEmail(model);
            await base.DoBeforeUpdateAsync(oldModel, model);
        }

        /// <summary>
        /// before create hook
        /// </summary>
        /// <param name="model"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(L model, Guid? tempGuid = null)
        {
            await CheckForDuplicateEmail(model);
            await base.DoBeforeCreateAsync(model, tempGuid);
        }

        /// <summary>
        /// Sets the ID of childModel to a new ID and the ParentID of childModel to parentModel.ID
        /// </summary>
        /// <param name="parentModel"></param>
        /// <param name="childModel"></param>
        /// <returns></returns>
        public async Task DoBeforeCloneForParent(LP parentModel, L childModel)
        {
            childModel.ID = await this.RequestIDAsync(childModel.BID, childModel.ClassTypeID);
            childModel.ParentID = parentModel.ID;
        }

        internal override void DoBeforeValidate(L newModel)
        {
        }

        /// <summary>
        /// Locator Service Override of DoAfterAddAsync
        /// </summary>
        /// <param name="newModel">Model that was added</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected async override Task DoAfterAddAsync(L newModel, Guid? tempGuid)
        {
            await this.taskQueuer.IndexModel(newModel.BID, Convert.ToInt32(ParentClassType), Convert.ToInt32(newModel.ParentID));
            await base.DoAfterAddAsync(newModel, tempGuid);
        }

        /// <summary>
        /// Locator Service Override of DoAfterUpdateAsync
        /// </summary>
        /// <param name="oldModel">Old Locator</param>
        /// <param name="newModel">Updated Locator</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected async override Task DoAfterUpdateAsync(L oldModel, L newModel, string connectionID)
        {
            await this.taskQueuer.IndexModel(newModel.BID, Convert.ToInt32(ParentClassType), Convert.ToInt32(newModel.ParentID));

            if (!oldModel.ParentID.Equals(newModel.ParentID))
                await this.taskQueuer.IndexModel(oldModel.BID, Convert.ToInt32(ParentClassType), Convert.ToInt32(oldModel.ParentID));
            await base.DoAfterUpdateAsync(oldModel, newModel, connectionID);
        }

        /// <summary>
        /// Locator Service Override of DoAfterDeleteAsync
        /// </summary>
        /// <param name="model">Locator Model</param>
        /// <returns></returns>
        protected async override Task DoAfterDeleteAsync(L model)
        {
            //Re-Index Locator Parent
            await this.taskQueuer.IndexModel(model.BID, Convert.ToInt32(ParentClassType), Convert.ToInt32(model.ParentID));

            //Call base - This will re-index the Locator itself
            await base.DoAfterDeleteAsync(model);

        }

        /// <summary>
        /// helper method to get refresh messages after create
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override List<RefreshEntity> DoGetRefreshMessagesOnCreate(L newModel)
        {
            var baseMessages = base.DoGetRefreshMessagesOnCreate(newModel);

            baseMessages.Add(GetParentRefreshEntity(newModel));

            return baseMessages;
        }

        /// <summary>
        /// helper method to get refresh messages after update
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override List<RefreshEntity> DoGetRefreshMessagesOnUpdate(L newModel)
        {
            var baseMessages = base.DoGetRefreshMessagesOnUpdate(newModel);

            baseMessages.Add(GetParentRefreshEntity(newModel));

            return baseMessages;
        }

        /// <summary>
        /// helper method to get refresh messages after delete
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override List<RefreshEntity> DoGetRefreshMessagesOnDelete(L newModel)
        {
            var baseMessages = base.DoGetRefreshMessagesOnDelete(newModel);

            baseMessages.Add(GetParentRefreshEntity(newModel));

            return baseMessages;
        }

        private RefreshEntity GetParentRefreshEntity(L newModel)
        {
            return new RefreshEntity()
            {
                BID = newModel.BID,
                ClasstypeID = Convert.ToInt32(ParentClassType),
                ID = Convert.ToInt32(newModel.ParentID)
            };
        }

        /// <summary>
        /// filter builder for Where Entity has Primary Key
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<L> WherePrimary(IQueryable<L> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }
    }

    /// <summary>
    /// employee locator service
    /// </summary>
    public class EmployeeLocatorService : BaseLocatorService<EmployeeLocator, short, EmployeeData>
    {
        /// <summary>
        /// employee locator service ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public EmployeeLocatorService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// parent classtype for employee locator is employee
        /// </summary>
        protected override ClassType ParentClassType { get { return ClassType.Employee; } }

        /// <summary>
        /// throws an error if email already exists in this locator type table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected async override Task CheckForDuplicateEmail(EmployeeLocator model)
        {
            if (model.eLocatorType == LocatorType.Email)
            {
                bool hasDupe = await ctx.Set<EmployeeLocator>().AnyAsync(
                    l =>
                    l.RawInput == model.RawInput &&
                    l.ID != model.ID &&
                    l.BID == model.BID &&
                    l.ParentID == model.ParentID);

                if (hasDupe)
                    throw new InvalidOperationException("Email already exists");
            }
        }
    }

    /// <summary>
    /// company locator service 
    /// </summary>
    public class CompanyLocatorService : BaseLocatorService<CompanyLocator, int, CompanyData>
    {
        /// <summary>
        /// company locator service ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CompanyLocatorService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// company locator service has parent type of company
        /// </summary>
        protected override ClassType ParentClassType { get { return ClassType.Company; } }
    }

    /// <summary>
    /// contact locator service 
    /// </summary>
    public class ContactLocatorService : BaseLocatorService<ContactLocator, int, ContactData>
    {
        /// <summary>
        /// contact locator service ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public ContactLocatorService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// contact locator service has parent type of contact
        /// </summary>
        protected override ClassType ParentClassType { get { return ClassType.Contact; } }
    }

    /// <summary>
    /// business locator service 
    /// </summary>
    public class BusinessLocatorService : BaseLocatorService<BusinessLocator, short, BusinessData>
    {
        /// <summary>
        /// business locator service ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public BusinessLocatorService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// business locator service has parent type of business
        /// </summary>
        protected override ClassType ParentClassType { get { return ClassType.Business; } }
    }

    /// <summary>
    /// location locator service 
    /// </summary>
    public class LocationLocatorService : BaseLocatorService<LocationLocator, byte, LocationData>
    {
        /// <summary>
        /// location locator service ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public LocationLocatorService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// location locator service has parent type of location
        /// </summary>
        protected override ClassType ParentClassType { get { return ClassType.Location; } }
    }

    /// <summary>
    /// order contact role  locator service 
    /// </summary>
    public class OrderContactRoleLocatorService : BaseLocatorService<OrderContactRoleLocator, int, OrderContactRole>
    {
        /// <summary>
        /// order contact role locator service ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderContactRoleLocatorService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// order contact role locator service has parent type of order contact role
        /// </summary>
        protected override ClassType ParentClassType => ClassType.OrderContactRole;
    }
}
