﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Link CRUD service
    /// </summary>
    public abstract class LinkCRUDService<M> : BaseCRUDService<M> where M : class
    {
        /// <summary>
        /// Constructs a Link CRUD service with injected params
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="bid"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration helper</param>
        public LinkCRUDService(ApiContext context, RemoteLogger logger, short bid, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, logger, bid, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Called before create
        /// </summary>
        /// <param name="newModel"></param>
        /// /// <param name="tempGuid"></param>
        public override async Task DoBeforeCreateAsync(M newModel, Guid? tempGuid = null)
        {
            await Task.FromResult(ctx.Set<M>().Add(newModel));
        }

        /// <summary>
        /// Business logic on delete
        /// </summary>
        /// <param name="model"></param>
        public override async Task DoBeforeDeleteAsync(M model)
        {
            await Task.FromResult(ctx.Set<M>().Remove(model));
        }

        /// <summary>
        /// Business logic on update. Base class handles ModifiedDT
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        public override async Task DoBeforeUpdateAsync(M oldModel, M newModel)
        {
            ctx.Entry(newModel).State = await Task.FromResult(EntityState.Modified);
        }

        /// <summary>
        /// Sets up a lazy service from the types in this service given static values
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="ctx"></param>
        /// <param name="logger"></param>
        /// <param name="bid"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <returns></returns>
        public static Lazy<S> CreateService<S>(ApiContext ctx, RemoteLogger logger, short bid, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            where S : LinkCRUDService<M>
        {
            //if you change this, change the function version too
            return new Lazy<S>(() => Activator.CreateInstance(typeof(S), ctx, logger, bid, rtmClient, migrationHelper) as S);
        }

        /// <summary>
        /// Creates a lazy service from the types in this service, given a value and a BID function
        /// </summary>
        /// <typeparam name="S">Service to create</typeparam>
        /// <param name="ctx">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="bid">Business ID</param>
        /// <param name="rtmClient">Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <returns></returns>
        public static Lazy<S> CreateService<S>(ApiContext ctx, RemoteLogger logger, short bid, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            where S : LinkCRUDService<M>
        {
            //if you change this, change the function version too
            return new Lazy<S>(() => Activator.CreateInstance(typeof(S), ctx, logger, bid, rtmClient, taskQueuer, cache, migrationHelper) as S);
        }

        /// <summary>
        /// Creates a lazy service from the types in this service, given a value and a BID function
        /// </summary>
        /// <typeparam name="S">Service to create</typeparam>
        /// <param name="ctx">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="bidFunc">BID Function</param>
        /// <returns></returns>
        public static Lazy<S> CreateService<S>(ApiContext ctx, RemoteLogger logger, Func<short> bidFunc)
            where S : LinkCRUDService<M>
        {
            //if you change this, change the non-function version too
            return new Lazy<S>(() => Activator.CreateInstance(typeof(S), ctx, logger, bidFunc()) as S);
        }

        /// <summary>
        /// Business logic to do after adding the new model.
        /// </summary>
        /// <param name="newModel">Model that was added</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override Task DoAfterAddAsync(M newModel, Guid? tempGuid)
        {
            return Task.CompletedTask;
        }

        /// <summary>
        /// Link CRUD Service Override of DoAfterUpdateAsync
        /// </summary>
        /// <param name="oldModel">Old mode</param>
        /// <param name="newModel">New model</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override Task DoAfterUpdateAsync(M oldModel, M newModel, string connectionID)
        {
            return Task.CompletedTask;
        }

        /// <summary>
        /// Called after deleting a record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected override Task DoAfterDeleteAsync(M model)
        {
            return Task.CompletedTask;
        }
    }
}
