﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Clones;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Web.Services
{
    public class CreditMemoService : BaseTransactionService<CreditMemoData>
    {
        /// <summary>
        /// Defines this controller as for Credit Memos
        /// </summary>
        protected override OrderTransactionType TransactionType => OrderTransactionType.Memo;

        /// <summary>
        /// Order Service Layer
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CreditMemoService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache,
            RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid,
            taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

    }
}
