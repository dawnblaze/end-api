﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// assembly Table service
    /// </summary>
    public class DestinationProfileService : AtomCRUDService<DestinationProfile, int>, IDoBeforeCreateUpdateWithParent<DestinationProfile, DestinationData>
    {
        /// <summary>
        /// Constructs a AssemblyTableService service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper"></param>
        public DestinationProfileService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { nameof(DestinationProfile.DestinationProfileTables), nameof(DestinationProfile.DestinationProfileVariables) };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(IncludeDefaults);
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">AssemblyTable ID</param>
        /// <returns></returns>
        public override IQueryable<DestinationProfile> WherePrimary(IQueryable<DestinationProfile> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// get old model
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task<DestinationProfile> GetOldModelAsync(DestinationProfile newModel)
        {
            IQueryable<DestinationProfile> oldModelQry = ctx.DestinationProfile
                                                    .Include(p => p.DestinationProfileTables)
                                                    .Include(p => p.DestinationProfileVariables)
                                                    .AsNoTracking();
            var oldModel = await WherePrimary(oldModelQry, newModel.ID).FirstOrDefaultAsync();

            return oldModel;
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<DestinationProfile, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<DestinationProfile, int>[]{
                CreateChildAssociation<DestinationProfileTableService, DestinationProfileTable, short>((a) => a.DestinationProfileTables),
                CreateChildAssociation<DestinationProfileVariableService, DestinationProfileVariable, short>((a) => a.DestinationProfileVariables)
            };
        }

        internal override void DoBeforeValidate(DestinationProfile newModel)
        {

        }

        /// <summary>
        /// set machine ID and BID of child
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public void DoBeforeCreateWithParent(DestinationProfile child, DestinationData parent)
        {
            child.DestinationID = parent.ID;
            child.TemplateID = parent.TemplateID.GetValueOrDefault(0);
            child.BID = parent.BID;
        }

        /// <summary>
        /// set machine ID and BID of child
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        public void DoBeforeUpdateWithParent(DestinationProfile child, DestinationData oldParent, DestinationData newParent)
        {
            child.DestinationID = newParent.ID;
            child.TemplateID = newParent.TemplateID.GetValueOrDefault(0);
            child.BID = newParent.BID;
        }

    }
}
