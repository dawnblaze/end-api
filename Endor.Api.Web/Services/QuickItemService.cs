﻿using Endor.Api.Web.Classes;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Service that wraps utilizes data storage layer and is accessed primarily from Controller layer
    /// </summary>
    public class QuickItemService : AtomCRUDService<QuickItemData, int>, ISimpleListableViewService<SimpleQuickItemData, int>
    {
        /// <summary>
        /// Constructs a new QuickItemService with a number of injeced parameters
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public QuickItemService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) :
            base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
            flatlistService = new FlatListItemService(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper);
        }

        /// <summary>
        /// Flatlist service for saving of quickitem categories
        /// </summary>
        public FlatListItemService flatlistService = null;

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "CategoryLinks", "CategoryLinks.Category" };

        /// <summary>
        /// SimpleQuickItemData list
        /// </summary>
        public DbSet<SimpleQuickItemData> SimpleListSet => ctx.SimpleQuickItemData;

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<QuickItemData> WherePrimary(IQueryable<QuickItemData> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Gets child associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<QuickItemData, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<QuickItemData, int>[]
            {
                CreateChildLinkAssociation<QuickItemCategoryLinkService, QuickItemCategoryLink>((a) => a.CategoryLinks)
            };
        }

        internal override void DoBeforeValidate(QuickItemData newModel)
        {
        }

        private void CleanUpDataJSON(QuickItemData quickItem)
        {
            if (!string.IsNullOrWhiteSpace(quickItem.DataJSON))
            {
                string dataJSON = quickItem.DataJSON;

                //dataJSON = RenumberLineItem(dataJSON); - NOTE: Moved to the controller before stripping away the line items ID since it is needed in CopyLineItemFiles()
                JObject JSONObj = JsonConvert.DeserializeObject<JObject>(dataJSON);
                this.RemoveIDs(JSONObj);

                quickItem.DataJSON = JSONObj.ToString();
            }
        }

        /// <summary>
        /// Logic to renumber the lineitems.  NOTE: Used in the controller before stripping away the line items ID since it is needed in CopyLineItemFiles()
        /// </summary>
        /// <param name="DataJSON"></param>
        /// <returns></returns>
        public string RenumberLineItem(string DataJSON)
        {
            if (!string.IsNullOrWhiteSpace(DataJSON))
            {
                var quickItemSchema = JsonConvert.DeserializeObject<JObject>(DataJSON);
                if (quickItemSchema["Items"] != null)
                {
                    List<OrderItemData> lineItems = quickItemSchema["Items"].ToObject<List<OrderItemData>>();
                    if (lineItems != null && lineItems.Count > 0)
                    {
                        short lineItemNumber = 0;

                        foreach (var lineItem in lineItems)
                        {
                            lineItemNumber++;
                            lineItem.ItemNumber = lineItemNumber;
                        }

                        quickItemSchema["Items"] = JToken.FromObject(lineItems);

                        DataJSON = JsonConvert.SerializeObject(quickItemSchema);
                    }
                }
            }
            return DataJSON;
        }

        public override Task DoBeforeCloneAsync(QuickItemData clone)
        {
            clone.Name = GetNextClonedName(ctx.Set<QuickItemData>().AsNoTracking().Where(t => t.BID == BID && t.Name.StartsWith(t.Name)).ToList(), clone.Name, t => t.Name);
            return base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Business logic on add. Base class handles ID and ClassTypeID 
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(QuickItemData newModel, Guid? tempGuid = null)
        {
            if (!(AreCategoriesValid(newModel.CategoryLinks)))
                throw new ArgumentException("Categories contains invalid or missing data.");

            //CleanUpDataJSON(newModel);
            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// Business logic on update. Base class handles ModifiedDT
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(QuickItemData oldModel, QuickItemData newModel)
        {
            if (!(AreCategoriesValid(newModel.CategoryLinks)))
                throw new ArgumentException("Categories contains invalid or missing data.");

            CleanUpDataJSON(newModel);

            await base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        /// <summary>
        /// Business logic for CanDelete.
        /// </summary>
        /// <param name="id">QuickItem's ID</param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(int id)
        {
            var resp = await base.CanDelete(id);
            // allowed to proceed despite Part.QuickItem.CategoryLinks
            if ((resp.Value.HasValue && resp.Value.Value == false) && (resp.Message.Equals("Unable to Delete Record - Reference by the following Tables: [Part.QuickItem.CategoryLink]")))
            {
                resp.Value = true;
                resp.Message = "Can Delete QuickItem";
            }
            return resp;
        }

        /// <summary>
        /// Business logic to perform before Deleting
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(QuickItemData model)
        {
            if (model.Categories != null && model.Categories.Count > 0)
            {
                var categories = model.Categories;
                foreach (var cat in categories)
                    await LinkCategory(model.ID, cat.ID, false);

                model.Categories.Clear();
                model.CategoryLinks.Clear();
            }

            await base.DoBeforeDeleteAsync(model);
        }

        /// <summary>
        /// Links or Unlinks a QuickItem with a QuickItem Category
        /// </summary>
        /// <param name="ID">QuickItem.ID</param>
        /// <param name="CategoryID">QuickItemCategory's ID</param>
        /// <param name="isLinked">If it is to link or unlink</param>
        /// <param name="CategoryName">QuickItemCategory's Name</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkCategory(int ID, short CategoryID, bool isLinked, string CategoryName = "")
        {
            try
            {
                var quickItem = await ctx.QuickItemData.Include("CategoryLinks").Where(i => i.BID == BID && i.ID == ID).FirstOrDefaultAsync();
                if (quickItem == null)
                    return new EntityActionChangeResponse()
                    {
                        Message = $"QuickItem with ID of {ID} not found.",
                        Success = false,
                        ErrorMessage = $"QuickItem with ID of {ID} not found.",
                        Id = ID
                    };

                var category = await ctx.FlatListItem.FirstOrDefaultAsync(i => i.BID == BID && i.ID == CategoryID && i.FlatListType == FlatListType.QuickItemCategories);
                if (category == null)
                {
                    if ((isLinked) && (!string.IsNullOrEmpty(CategoryName)))
                    {
                        FlatListItem quickItemCategory = new FlatListItem()
                        {
                            BID = BID,
                            FlatListType = FlatListType.QuickItemCategories,
                            ModifiedDT = DateTime.UtcNow,
                            IsActive = true,
                            IsSystem = false,
                            Name = CategoryName
                        };

                        await flatlistService.CreateAsync(quickItemCategory);
                        CategoryID = quickItemCategory.ID;
                    }
                    else
                    {
                        return new EntityActionChangeResponse()
                        {
                            Message = String.Format(LinkCategoryMissingFormatString, CategoryID),
                            Success = false,
                            ErrorMessage = String.Format(LinkCategoryMissingFormatString, CategoryID),
                            Id = CategoryID
                        };
                    }
                }

                if (isLinked)
                {
                    if (quickItem.CategoryLinks != null && quickItem.CategoryLinks.Count > 0 && quickItem.CategoryLinks.Any(l => l.CategoryID == CategoryID))
                        return new EntityActionChangeResponse()
                        {
                            Message = $"QuickItem already linked with Category.",
                            ErrorMessage = $"QuickItem already linked with Category.",
                            Success = false,
                            Id = ID
                        };

                    var quickItemCategoryLink = new QuickItemCategoryLink()
                    {
                        BID = BID,
                        QuickItemID = ID,
                        CategoryID = CategoryID
                    };

                    await ctx.QuickItemCategoryLink.AddAsync(quickItemCategoryLink);
                    await ctx.SaveChangesAsync();

                    return new EntityActionChangeResponse()
                    {
                        Message = $"QuickItem successfully linked with Category.",
                        Success = true,
                        Id = ID
                    };
                }
                else
                {
                    if (quickItem.CategoryLinks == null || quickItem.CategoryLinks.Count < 1 || !quickItem.CategoryLinks.Any(l => l.CategoryID == CategoryID))
                        return new EntityActionChangeResponse()
                        {
                            Message = $"QuickItem already unlinked from Category.",
                            ErrorMessage = $"QuickItem already unlinked from Category.",
                            Success = false,
                            Id = ID
                        };

                    var quickItemCategoryLink = quickItem.CategoryLinks.FirstOrDefault(l => l.CategoryID == CategoryID);
                    ctx.QuickItemCategoryLink.Remove(quickItemCategoryLink);
                    await ctx.SaveChangesAsync();

                    return new EntityActionChangeResponse()
                    {
                        Message = $"QuickItem successfully unlinked from Category.",
                        Success = true,
                        Id = ID
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        /// <summary>
        /// Gets a Filtered SimpleList of QuickItemData
        /// </summary>
        /// <param name="filters">Filters</param>
        /// <returns></returns>
        public async Task<SimpleQuickItemData[]> GetSimpleList(SimpleQuickItemFilters filters)
        {
            var filteredIds = await ctx.QuickItemData.Where(i => i.BID == BID)
                .WhereAll(filters.WherePredicates()).Select(i => i.ID).ToListAsync();

            return await ctx.SimpleQuickItemData.Where(i => i.BID == this.BID && filteredIds.Contains(i.ID)).ToArrayAsync();
        }

        /// <summary>
        /// Maps the QuickItemData.CategoryLinks.Category to QuickItemData.Categories
        /// </summary>
        /// <param name="item"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override Task MapItem(QuickItemData item, IExpandIncludes includes = null)
        {
            if (item != null)
            {
                List<FlatListItem> categories = new List<FlatListItem>();
                foreach (var cl in item.CategoryLinks)
                    categories.Add(cl.Category);

                if (categories.Count > 0)
                    item.Categories = categories;
                else
                    item.Categories = new FlatListItem[] { };
            }

            return base.MapItem(item, includes);
        }

        /// <summary>
        /// Actions to perform after updating a new QuickItem
        /// </summary>
        /// <param name="oldModel">Old QuickItem</param>
        /// <param name="updatedModel">New QuickItem</param>
        /// <param name="connectionID">Temporary identifier</param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(QuickItemData oldModel, QuickItemData updatedModel, string connectionID)
        {
            await ProcessQuickItemCategoriesLink(updatedModel, false);
            await ProcessQuickItemCategoriesLink(updatedModel, true);
            //await CleanUpOrphanedCategoriesInFlatlist(); -- Do not cleanup since we do not create Categories.
            await base.DoAfterUpdateAsync(oldModel, updatedModel, connectionID);
        }

        /// <summary>
        /// Actions to perform after adding a new QuickItem
        /// </summary>
        /// <param name="model">New QuickItem</param>
        /// <param name="tempGuid">Temporary identifier</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(QuickItemData model, Guid? tempGuid)
        {
            await ProcessQuickItemCategoriesLink(model, true);
            //await CleanUpOrphanedCategoriesInFlatlist(); -- Do not cleanup since we do not create Categories.
            await base.DoAfterAddAsync(model, tempGuid);
        }

        private static DMID GetLineItemDMID(LineItemInformationSchema lineItem)
        {
            return new DMID()
            {
                ctid = (int)ClassType.OrderItem,
                id = lineItem.ID > 0 ? lineItem.ID : (int?)null,
                guid = String.IsNullOrWhiteSpace(lineItem.TempID) ? null : (Guid?)Guid.Parse(lineItem.TempID)
            };
        }

        private async Task ProcessQuickItemCategoriesLink(QuickItemData model, bool link = false)
        {
            if (link)
            {
                var badCategories = new List<FlatListItem>();
                var goodCategories = new List<FlatListItem>();

                if (model.Categories != null)
                {
                    foreach (FlatListItem fl in model.Categories)
                    {
                        var resp = await LinkCategory(model.ID, fl.ID, link, fl.Name);
                        if (!resp.Success && resp.Message.Equals(String.Format(LinkCategoryMissingFormatString, fl.ID)))
                            badCategories.Add(fl);
                        if (resp.Success)
                            goodCategories.Add(fl);
                    }
                }

                // handle bad ID mappings
                if (badCategories.Count > 0)
                    foreach(var cat in badCategories)
                        model.Categories.Remove(cat);

                if (goodCategories.Count > 0)
                {
                    var fullCategories = new List<FlatListItem>();
                    foreach (var cat in goodCategories)
                        fullCategories.Add(await ctx.FlatListItem.FirstOrDefaultAsync(i => i.BID == BID && i.ID == cat.ID));

                    model.Categories = fullCategories;
                }
            }
            else
            {
                List<QuickItemCategoryLink> category = await ctx.QuickItemCategoryLink.Where(i => i.BID == this.BID && i.QuickItemID == model.ID).ToListAsync();
                if (category != null)
                {
                    foreach (QuickItemCategoryLink qicl in category)
                    {
                        await LinkCategory(model.ID, qicl.CategoryID, link);
                    }
                }
            }
        }

        private async Task CleanUpOrphanedCategoriesInFlatlist()
        {
            List<FlatListItem> quickItemCategories = await ctx.FlatListItem.Where(i => i.BID == BID && i.FlatListType == FlatListType.QuickItemCategories).ToListAsync();
            if (quickItemCategories != null)
            {
                foreach (FlatListItem fli in quickItemCategories)
                {
                    if (await CanDeleteQuickItemCategory(fli.ID))
                    {
                        if (!await flatlistService.DeleteAsync(fli))
                        {
                            await flatlistService.ForceDelete(fli.ID);
                        }
                    }
                }

            }
        }

        private async Task<bool> CanDeleteQuickItemCategory(int QuickItemCategoryId)
        {
            var category = await ctx.QuickItemCategoryLink.FirstOrDefaultAsync(i => i.CategoryID == QuickItemCategoryId);
            if (category == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Verifies if the Categories are indeed valid
        /// </summary>
        /// <param name="categories">List of Categories to validate</param>
        /// <returns></returns>
        private bool AreCategoriesValid(ICollection<QuickItemCategoryLink> categories)
        {
            if (categories != null)
            {
                foreach (var cat in categories)
                if (cat.BID != BID)
                        return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newQuickItemID"></param>
        /// <param name="oldModelDataJSON"></param>
        /// <returns></returns>
        public async Task CopyDefaultImage(int newQuickItemID, string oldModelDataJSON)
        {
            if (!string.IsNullOrWhiteSpace(oldModelDataJSON))
            {
                List<LineItemInformationSchema> lineItems = JsonConvert.DeserializeObject<QuickItemDef>(oldModelDataJSON)?.Items;

                if (lineItems != null)
                {
                    DocumentManager firstLineItemDocMan = null;
                    foreach (var lineItem in lineItems)
                    {
                        DMID lineItemDMID = GetLineItemDMID(lineItem);
                        var candidate = this.GetDocumentManager(lineItemDMID, BucketRequest.Data);
                        if (await candidate.HasDefaultImage())
                        {
                            firstLineItemDocMan = candidate;
                            break;
                        }
                    }

                    if (firstLineItemDocMan != null)
                    {
                        await firstLineItemDocMan.CopyDefaultImageAsync(newQuickItemID, ClassType.QuickItem.ID());
                    }
                }
            }
        }

        /// <summary>
        /// Copies the line item files into the quickitem folder
        /// </summary>
        /// <param name="quickItem">The quickitem object (with the lineItem.ItemNumber)</param>
        /// <param name="oldModelDataJSON">The quickitem object before saving (with the lineItem.ID)</param>
        /// <returns></returns>
        public async Task CopyLineItemFiles(QuickItemData quickItem, string oldModelDataJSON)
        {
            if (!string.IsNullOrWhiteSpace(oldModelDataJSON))
            {
                var quickItemSchema = JsonConvert.DeserializeObject<QuickItemDef>(oldModelDataJSON);
                List<LineItemInformationSchema> lineItems = quickItemSchema.Items;

                foreach (var lineItem in lineItems)
                {
                    DMID lineItemDMID = GetLineItemDMID(lineItem);
                    DocumentManager lineItemDocman = this.GetDocumentManager(lineItemDMID, BucketRequest.Documents);
                    await lineItemDocman.CopyExistingDocsAsync("", "/" + lineItem.ItemNumber + "/", quickItem.ID, StorageBin.Permanent, (int?)ClassType.QuickItem);
                }
            }
        }

        /// <summary>
        /// Removes a ID property from a JObject Object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private void RemoveIDs(JObject obj)
        {
            foreach (JToken tok in obj.Children())
            {
                ReplaceProperty(tok, "ID", null);
            }
        }

        /// <summary>
        /// Replace the value of a property from a JObject Object
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <param name="propertyValue"></param>
        /// <returns></returns>
        private void ReplaceProperty(JObject obj, string propertyName, string propertyValue)
        {
            foreach (JToken tok in obj.Children())
            {
                ReplaceProperty(tok, propertyName, propertyValue);
            }
        }

        /// <summary>
        /// Replace the value of a property from a JToken Object
        /// </summary>
        /// <param name="tok"></param>
        /// <param name="propertyName"></param>
        /// <param name="propertyValue"></param>
        /// <returns></returns>
        private void ReplaceProperty(JToken tok, string propertyName, string propertyValue)
        {
            if (tok.Type == JTokenType.Array)
                ReplaceProperty(tok.Value<JArray>(), propertyName, propertyValue);

            else if (tok.Type == JTokenType.Object)
                ReplaceProperty(tok.Value<JObject>(), propertyName, propertyValue);

            else if (tok.Type == JTokenType.Property)
                ReplaceProperty(tok.Value<JProperty>(), propertyName, propertyValue);
        }

        /// <summary>
        /// Replace the value of a property from a JProperty Object
        /// </summary>
        /// <param name="prop"></param>
        /// <param name="propertyName"></param>
        /// <param name="propertyValue"></param>
        /// <returns></returns>
        private void ReplaceProperty(JProperty prop, string propertyName, string propertyValue)
        {
            if (prop.Name == propertyName)
                prop.Value = propertyValue;

            else foreach (JToken tok in prop.Children())
                {
                    ReplaceProperty(tok, propertyName, propertyValue);
                }

        }

        /// <summary>
        /// Replace the value of a property from a JArray of Objects
        /// </summary>
        /// <param name="jArray"></param>
        /// <param name="propertyName"></param>
        /// <param name="propertyValue"></param>
        /// <returns></returns>
        private void ReplaceProperty(JArray jArray, string propertyName, string propertyValue)
        {
            foreach (JObject jo in jArray)
            {
                ReplaceProperty(jo, propertyName, propertyValue);
            }
        }

        private const string LinkCategoryMissingFormatString = "QuickItem Category with ID of %d not found.";
    }
}
