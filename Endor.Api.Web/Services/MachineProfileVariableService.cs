using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using System.Linq;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// MachineProfileVariableService
    /// </summary>
    public class MachineProfileVariableService : AtomCRUDService<MachineProfileVariable, short>, IDoBeforeCreateUpdateWithParent<MachineProfileVariable, MachineProfile>
    {

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper"></param>
        /// <returns></returns>
        public MachineProfileVariableService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return new string[] { };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<MachineProfileVariable> WherePrimary(IQueryable<MachineProfileVariable> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<MachineProfileVariable, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<MachineProfileVariable, short>[] { };
        }

        internal override void DoBeforeValidate(MachineProfileVariable newModel)
        {

        }

        void IDoBeforeCreateUpdateWithParent<MachineProfileVariable, MachineProfile>.DoBeforeCreateWithParent(MachineProfileVariable child, MachineProfile parent)
        {
            child.ProfileID = parent.ID;
            child.AssemblyID = parent.MachineTemplateID;
        }

        void IDoBeforeCreateUpdateWithParent<MachineProfileVariable, MachineProfile>.DoBeforeUpdateWithParent(MachineProfileVariable child, MachineProfile oldParent, MachineProfile newParent)
        {
            child.ProfileID = newParent.ID;
            child.AssemblyID = newParent.MachineTemplateID;
        }
    }
}
