﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// order destination contact role service
    /// </summary>
    public class OrderDestinationContactRoleService : AtomCRUDChildService<OrderContactRole, int, int>
    {
        /// <summary>
        /// order destination contact role service ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderDestinationContactRoleService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// before delete hook
        /// sets parent id
        /// </summary>
        /// <param name="model"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public override Task DoBeforeDeleteAsync(OrderContactRole model, int parentID)
        {
            SetParentID(model, parentID);
            return base.DoBeforeDeleteAsync(model);
        }

        /// <summary>
        /// before update hook
        /// sets parent id
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="parentID"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(OrderContactRole oldModel, int parentID, OrderContactRole newModel)
        {
            SetParentID(newModel, parentID);
            await base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        /// <summary>
        /// returns includes array (null for this service)
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return null;
        }

        /// <summary>
        /// Gets the ContactRoles for an OrderDestination
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="includeDefaults">If includeDefaults is true then default roles from the order will be substituted in</param>
        /// <returns></returns>
        internal async Task<List<OrderContactRole>> GetByParentIDWithDefaultsAsync(int parentID, bool includeDefaults)
        {
            if (!await ctx.OrderDestinationData.AnyAsync(t => t.BID == this.BID && t.ID == parentID))
                return null;

            var orderDestinationContactRoles = await ctx.OrderContactRole.Where(t => t.DestinationID == parentID && t.BID == this.BID && t.IsDestinationRole).ToListAsync();

            if (includeDefaults)
            {
                var OrderContactRolesNotInOrderDestinationContactRoles = await ctx.OrderDestinationData.Where(t => t.ID == parentID && t.BID == this.BID).Include(t => t.Order)
                    .Select(t => t.Order).Include(t => t.ContactRoles)
                    .SelectMany(t => t.ContactRoles).Where(t => t.IsOrderRole).Where(t => !orderDestinationContactRoles.Select(r => r.RoleType).Contains(t.RoleType)).ToListAsync();

                orderDestinationContactRoles.AddRange(OrderContactRolesNotInOrderDestinationContactRoles);
            }

            return orderDestinationContactRoles;
        }

        /// <summary>
        /// gets parent id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override int GetParentID(OrderContactRole model)
        {
            return model.DestinationID.GetValueOrDefault();
        }

        /// <summary>
        /// returns whether the parent ID is valid
        /// </summary>
        /// <param name="model"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public override bool IsParentIDValid(OrderContactRole model, int parentID)
        {
            return model.DestinationID == parentID;
        }

        /// <summary>
        /// sets the parent ID
        /// </summary>
        /// <param name="model"></param>
        /// <param name="parentID"></param>
        public override void SetParentID(OrderContactRole model, int parentID)
        {
            model.DestinationID = parentID;
        }

        /// <summary>
        /// query filter for where parent is of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public override IQueryable<OrderContactRole> WhereParent(IQueryable<OrderContactRole> query, int parentID)
        {
            return query.Where(t => t.DestinationID == parentID);
        }

        /// <summary>
        /// query filter for where has primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<OrderContactRole> WherePrimary(IQueryable<OrderContactRole> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// returns child associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<OrderContactRole, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<OrderContactRole, int>[]{
                CreateChildAssociation<OrderContactRoleLocatorService, OrderContactRoleLocator, int>((a) => a.OrderContactRoleLocators),
            };
        }

        internal override void DoBeforeValidate(OrderContactRole newModel)
        {

        }

        /// <summary>
        /// before create hook
        /// sets parent id
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="parentID"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(OrderContactRole newModel, int parentID, Guid? tempGuid = null)
        {
            SetParentID(newModel, parentID);
            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// override for set modified DT for temporal table
        /// </summary>
        /// <param name="newModel"></param>
        protected override void SetModifiedDT(OrderContactRole newModel)
        {
            if (newModel.ID == 0)
            {
                newModel.ModifiedDT = default(DateTime);
            }
            ctx.Entry(newModel).Property(x => x.ModifiedDT).IsModified = false;
        }
    }
}
