﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Order Contact Role Service Layer
    /// </summary>
    public class OrderContactRoleService : AtomCRUDChildService<OrderContactRole,int, int>
    {
        #region Input Classes
        /// <summary>
        /// Order Contact Role Contact Name Input Class
        /// </summary>
        public class ContactNameInput
        {
            /// <summary>
            /// Contact Name
            /// </summary>
            public string ContactName { get; set; }
        }
        /// <summary>
        /// Order Contact Role Input Class
        /// </summary>
        public class RoleInput
        {
            /// <summary>
            /// Contact Name
            /// </summary>
            public string ContactName { get; set; }
            /// <summary>
            /// Contact ID
            /// </summary>
            public int? ContactID { get; set; }
            /// <summary>
            /// Role Type
            /// </summary>
            public OrderContactRoleType RoleType { get; set; }
        }
        #endregion

        /// <summary>
        /// Order Service Layer
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderContactRoleService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default children to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Returns the includes
        /// </summary>
        /// <param name="includes">Children to include</param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// GetChildAssociations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<OrderContactRole, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<OrderContactRole, int>[]{
                CreateChildAssociation<OrderContactRoleLocatorService, OrderContactRoleLocator, int>((a) => a.OrderContactRoleLocators),
            };
        }

        internal override void DoBeforeValidate(OrderContactRole newModel)
        {
        }

        /// <summary>
        /// Adds a contact role to an order.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="contactRoleType">The contact role type of the link</param>
        /// <param name="contactID">Contact ID</param>
        /// <param name="input">Contact info</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> AddContactRole(int ID, OrderContactRoleType contactRoleType, int? contactID = null, ContactNameInput input = null)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                SqlParameter contactIdParam;
                SqlParameter contactNameParam;

                if (contactID.HasValue)
                {
                    contactIdParam = new SqlParameter("@contactID", contactID.Value);
                    contactNameParam = new SqlParameter("@contactName", DBNull.Value);
                }
                else if (string.IsNullOrWhiteSpace(input?.ContactName))
                {
                    contactIdParam = new SqlParameter("@contactID", DBNull.Value);
                    contactNameParam = new SqlParameter("@contactName", DBNull.Value);
                }
                else
                {
                    contactIdParam = new SqlParameter("@contactID", DBNull.Value);
                    contactNameParam = new SqlParameter("@contactName", input.ContactName);
                }

                object[] myParams = {
                    new SqlParameter("@bid", BID),
                    new SqlParameter("@orderId", ID),
                    new SqlParameter("@contactRoleType", contactRoleType),
                    contactIdParam,
                    contactNameParam,
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Order.Action.AddContactRole] @bid, @orderId, @contactRoleType, @contactID, @contactName;", parameters: myParams);
                if (rowResult > 0)
                {
                    await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.Order), ID);
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(ID) });

                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "Successfully added a contact role.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Adds a Contact role to an OrderItem.
        /// </summary>
        /// <param name="orderItem">OrderItem to add the role to</param>
        /// <param name="contactRoleType">The contact role type of the link</param>
        /// <param name="contactID">Contact ID</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> AddOrderItemContactRole(OrderItemData orderItem, OrderContactRoleType contactRoleType, int contactID, string connectionID)
        {
            try
            {
                var role = (await this.GetWhere(r => r.OrderID == orderItem.OrderID && r.OrderItemID == orderItem.ID && r.RoleType == contactRoleType)).FirstOrDefault();
                if (role == null)
                {
                    role = new OrderContactRole()
                    {
                        OrderID = orderItem.OrderID,
                        OrderItemID = orderItem.ID,
                        ContactID = contactID,
                        RoleType = contactRoleType
                    };

                    var created = await this.CreateAsync(role);
                }
                else if (role.ContactID != contactID)
                {
                    role.ContactID = contactID;
                    var updated = await this.UpdateAsync(role, connectionID);
                }

                return new EntityActionChangeResponse
                {
                    Id = orderItem.ID,
                    Message = "Successfully linked Contact Role to OrderItem",
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = orderItem.ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Removes a Contact Role from an OrderItem.
        /// </summary>
        /// <param name="orderItem">OrderItem to remove the role from</param>
        /// <param name="contactRoleType">The contact role type of the link</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> RemoveOrderItemContactRole(OrderItemData orderItem, OrderContactRoleType contactRoleType, int contactID)
        {
            try
            {
                var role = (await this.GetWhere(r => r.OrderID == orderItem.OrderID && r.OrderItemID == orderItem.ID && r.RoleType == contactRoleType && r.ContactID == contactID)).FirstOrDefault();
                if (role != null)
                    await this.DeleteAsync(role);

                return new EntityActionChangeResponse
                {
                    Id = orderItem.ID,
                    Message = "Successfully unlinked Contact Role from OrderItem",
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = orderItem.ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Removes a contact role from an order.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="contactRoleType">The contact role type of the link</param>
        /// <param name="contactID">Contact ID</param>
        /// <param name="input">Contact info</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> RemoveContactRole(int ID, OrderContactRoleType contactRoleType, int? contactID = null, ContactNameInput input = null)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                SqlParameter contactIdParam;
                SqlParameter contactNameParam;

                if (contactID.HasValue)
                {
                    contactIdParam = new SqlParameter("@contactID", contactID.Value);
                    contactNameParam = new SqlParameter("@contactName", DBNull.Value);
                }
                else if (string.IsNullOrWhiteSpace(input?.ContactName))
                {
                    contactIdParam = new SqlParameter("@contactID", DBNull.Value);
                    contactNameParam = new SqlParameter("@contactName", DBNull.Value);
                }
                else
                {
                    contactIdParam = new SqlParameter("@contactID", DBNull.Value);
                    contactNameParam = new SqlParameter("@contactName", input.ContactName);
                }

                object[] myParams = {
                    new SqlParameter("@bid", BID),
                    new SqlParameter("@orderId", ID),
                    new SqlParameter("@roleId", DBNull.Value),
                    new SqlParameter("@contactRoleType", contactRoleType),
                    contactIdParam,
                    contactNameParam,
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Order.Action.RemoveContactRole] @bid = @bid, @orderId = @orderId, @roleId = @roleId, @contactRoleType = @contactRoleType, @contactID = @contactID, @contactName = @contactName;", parameters: myParams);
                if (rowResult > 0)
                {
                    await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.Order), ID);
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(ID) });

                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "Successfully deleted a contact role.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Removes a contact role from an order.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="roleID">Role ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> RemoveContactRole(int ID, int roleID)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                SqlParameter contactIdParam = new SqlParameter("@contactID", DBNull.Value);
                SqlParameter contactNameParam = new SqlParameter("@contactName", DBNull.Value);

                object[] myParams = {
                    new SqlParameter("@bid", BID),
                    new SqlParameter("@orderId", ID),
                    new SqlParameter("@contactRoleType", DBNull.Value),
                    new SqlParameter("@roleID", roleID),
                    contactIdParam,
                    contactNameParam,
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Order.Action.RemoveContactRole] @bid = @bid, @orderId = @orderId, @roleId = @roleId, @contactRoleType = @contactRoleType, @contactID = @contactID, @contactName = @contactName;", parameters: myParams);
                if (rowResult > 0)
                {
                    await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.Order), ID);
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(ID) });

                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "Successfully deleted a contact role.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Updates a contact role of an order.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="contactRoleType">The contact role type of the link</param>
        /// <param name="contactID">Contact ID</param>
        /// <param name="input">Contact info</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> UpdateContactRole(int ID, OrderContactRoleType contactRoleType, int? contactID = null, ContactNameInput input = null)
        {
            return await UpdateContactRole(ID, null, contactRoleType, contactID, input);
        }

        /// <summary>
        /// Updates a contact role of an order.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="roleID">Role ID</param>
        /// <param name="contactRoleType">The contact role type of the link</param>
        /// <param name="contactID">Contact ID</param>
        /// <param name="input">Contact info</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> UpdateContactRole(int ID, int? roleID, OrderContactRoleType contactRoleType, int? contactID = null, ContactNameInput input = null)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                SqlParameter roleIdParam;
                SqlParameter contactIdParam;
                SqlParameter contactNameParam;

                if (roleID.HasValue)
                    roleIdParam = new SqlParameter("@roleID", roleID.Value);
                else
                    roleIdParam = new SqlParameter("@roleID", DBNull.Value);

                if (contactID.HasValue)
                {
                    contactIdParam = new SqlParameter("@contactID", contactID.Value);
                    contactNameParam = new SqlParameter("@contactName", DBNull.Value);
                }
                else if (string.IsNullOrWhiteSpace(input?.ContactName))
                {
                    contactIdParam = new SqlParameter("@contactID", DBNull.Value);
                    contactNameParam = new SqlParameter("@contactName", DBNull.Value);
                }
                else
                {
                    contactIdParam = new SqlParameter("@contactID", DBNull.Value);
                    contactNameParam = new SqlParameter("@contactName", input.ContactName);
                }

                object[] myParams = {
                    new SqlParameter("@bid", BID),
                    new SqlParameter("@orderId", ID),
                    roleIdParam,
                    new SqlParameter("@contactRoleType", contactRoleType),
                    contactIdParam,
                    contactNameParam,
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Order.Action.UpdateContactRole] @bid=@bid, @orderId=@orderId, @roleID=@roleID, @contactRoleType=@contactRoleType, @contactID=@contactID, @contactName=@contactName;", parameters: myParams);
                if (rowResult > 0)
                {
                    await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.Order), ID);
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(ID) });

                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "Successfully updated a contact role.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        private List<RefreshEntity> DoGetRefreshMessages(int orderID)
        {
            return new List<RefreshEntity>()
                        {
                new RefreshEntity()
                {
                    BID = BID,
                    ClasstypeID = (int)ClassType.Order,
                    ID = orderID,
                    DateTime = DateTime.UtcNow,
                    RefreshMessageType = RefreshMessageType.Change,
                }
            };
        }

        /// <summary>
        /// returns if the parent ID is valid
        /// </summary>
        /// <param name="model"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public override bool IsParentIDValid(OrderContactRole model, int parentID)
        {
            return model.OrderID == parentID;
        }

        /// <summary>
        /// before create hook
        /// makes sure parent ID is valid
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="parentID"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(OrderContactRole newModel, int parentID, Guid? tempGuid = null)
        {
            if (!IsParentIDValid(newModel, parentID))
                throw new InvalidOperationException();
                //newModel = null;

            await base.DoBeforeCreateAsync(newModel, tempGuid);

            ctx.Entry(newModel).Property(t => t.ModifiedDT).IsModified = false;
        }
        
        /// <summary>
        /// before update hook
        /// makes sure parent id is valid
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="parentID"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(OrderContactRole oldModel, int parentID, OrderContactRole newModel)
        {
            if (!IsParentIDValid(newModel, parentID) || !IsParentIDValid(oldModel, parentID))
                throw new InvalidOperationException();

            await base.DoBeforeUpdateAsync(oldModel, newModel);

            if (oldModel.ContactID != null && oldModel.ContactID == newModel.ContactID)
            {
                ctx.Entry(newModel).Property(t => t.ContactName).IsModified = false;
            }
        }

        /// <summary>
        /// before delete hook
        /// makes sure parent ID is valid
        /// </summary>
        /// <param name="model"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(OrderContactRole model, int parentID)
        {
            if (!IsParentIDValid(model, parentID))
                throw new InvalidOperationException();

            await base.DoBeforeDeleteAsync(model);
        }

        /// <summary>
        /// override for set parent ID
        /// </summary>
        /// <param name="model"></param>
        /// <param name="parentID"></param>
        public override void SetParentID(OrderContactRole model, int parentID)
        {
            model.OrderID = parentID;
        }

        /// <summary>
        /// override for get parent ID
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override int GetParentID(OrderContactRole model)
        {
            return model.OrderID;
        }

        /// <summary>
        /// before create hook
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override Task DoBeforeCreateAsync(OrderContactRole newModel, Guid? tempGuid = null)
        {
            return base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// set modified DT override for temporal table
        /// </summary>
        /// <param name="newModel"></param>
        protected override void SetModifiedDT(OrderContactRole newModel)
        {
            if (newModel.ID == 0)
            {
                /*
                fix for "Cannot insert an explicit value into a GENERATED ALWAYS column in table 'Dev.Endor.Business.DB1.dbo.Order.Contact.Role'. 
                Use INSERT with a column list to exclude the GENERATED ALWAYS column, or insert a DEFAULT into GENERATED ALWAYS column."
                */
                newModel.ModifiedDT = default(DateTime);
            }
            ctx.Entry(newModel).Property(x => x.ModifiedDT).IsModified = false;
        }

        /// <summary>
        /// filters query by parent ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public override IQueryable<OrderContactRole> WhereParent(IQueryable<OrderContactRole> query, int parentID)
        {
            return query.WherePrimary(this.BID, t => t.OrderID == parentID);
        }

        /// <summary>
        /// filters query by primary key
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<OrderContactRole> WherePrimary(IQueryable<OrderContactRole> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }
    }
}