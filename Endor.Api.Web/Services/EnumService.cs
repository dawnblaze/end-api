﻿using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// EnumTimeZone Service
    /// </summary>
    public class EnumTimeZoneService : BaseGenericService, ISimpleEnumListableService<SimpleEnumTimeZone, short>
    {
        /// <summary>
        /// Property to get  SimpleEnumTimeZone from ApiContext
        /// </summary>
        public DbSet<SimpleEnumTimeZone> SimpleEnumListSet => ctx.SimpleEnumTimeZone;

        /// <summary>
        /// EnumTimeZone Service Constructor
        /// </summary>
        public EnumTimeZoneService(ApiContext context, IMigrationHelper migrationHelper) : base(context, migrationHelper)
        {
        }
    }
}
