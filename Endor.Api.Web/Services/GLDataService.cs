﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Responses;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// GLData Service
    /// </summary>
    public class GLDataService : AtomCRUDService<GLData, int>
    {
        /// <summary>
        /// GLData Service
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">Push Client</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public GLDataService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Actions to perform before model validation
        /// </summary>
        /// <param name="newModel"></param>
        internal override void DoBeforeValidate(GLData newModel)
        {

        }

        /// <summary>
        /// Get GLData with specfic filters
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        internal async Task<ServiceResponse> GetWithFiltersAsync(GLDataFilters filters)
        {
            if (!filters.AreFiltersValid())
                return new ServiceResponse
                {
                    HasError = true,
                    Message = "One or more of the parameters are required. It is not possible to obtain all records."
                };

            if(!filters.IsOneOrZeroORderCompanyGLActivityFilters())
                return new ServiceResponse
                {
                    HasError = true,
                    Message = "Only Zero or One of the OrderID, CompanyID, or GLActivityID can be used."
                };

            if(!filters.Take.HasValue)
                return new ServiceResponse
                {
                    HasError = true,
                    Message = "The Take parameter should be specified, the maximum value is 5000."
                };

            if (!filters.AreAmountOfRecordsValid())
                return new ServiceResponse
                {
                    HasError = true,
                    Message = "The maximum records allowed in one request is 5000."
                };

            var result = await this.Where(filters.WherePredicates()).Take(filters.Take.Value).Skip(filters.Skip ?? 0).ToListAsync();

            return new ServiceResponse
            {
                Success = true,
                Value = result
            };
        }

        /// <summary>
        /// Get GLData Summary with specfic filters
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        internal async Task<ServiceResponse> GetSummaryWithFiltersAsync(GLDataSummaryFilters filters)
        {
            if (!filters.AreFiltersValid())
                return new ServiceResponse
                {
                    HasError = true,
                    Message = "One or more of the parameters are required. It is not possible to obtain all records."
                };

            if (!filters.IsOneOrZeroORderCompanyGLActivityFilters())
                return new ServiceResponse
                {
                    HasError = true,
                    Message = "Only Zero or One of the OrderID, CompanyID, or GLActivityID can be used."
                };

            var result = await this.Where(filters.WherePredicates())
                                   .Include(a => a.GLAccount)
                                   .Select(a => new {
                                        a.GLAccountID,
                                        GLAccountName = a.GLAccount.Name,
                                        a.Amount
                                    })
                                   .GroupBy(a => new { a.GLAccountID, a.GLAccountName })
                                   .Select(glsum => new GLDataSummaryResponse {
                                       GLAccountID = glsum.Key.GLAccountID,
                                       GLAccountName = glsum.Key.GLAccountName, 
                                       Amount = glsum.Sum(z => z.Amount)
                                   }).ToListAsync();

            return new ServiceResponse
            {
                Success = true,
                Value = result
            };
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<GLData, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<GLData, int>[]
            {

            };
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">GLData ID</param>
        /// <returns></returns>
        public override IQueryable<GLData> WherePrimary(IQueryable<GLData> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }
    }
}
