﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Base Service Layer for Orders
    /// </summary>
    public abstract class BaseTransactionService<T> : AtomCRUDService<T, int>, ISimpleListableViewService<SimpleOrderData, int>
        where T : TransactionHeaderData
    {
        private int? _userLinkId = null;

        const int estNumClassTypeID = -10200;
        const int ordNumClassTypeID = -10000;
        const int invNumClassTypeID = -10003;

        /// <summary>
        /// Lazy Loaded OrderKeyDate Service
        /// </summary>
        protected readonly Lazy<OrderKeyDateService> _orderKeyDateService;


        /// <summary>
        /// Key for Copy Documents
        /// </summary>
        protected string OptionKeyCopyDocuments = "Order.Clone.CopyDocuments";
        /// <summary>
        /// Key for Copy EstimateDocuments
        /// </summary>
        protected string OptionEstimateKeyCopyDocuments = "Estimate.Clone.CopyDocuments";

        /// <summary>
        /// Transaction Type
        /// </summary>
        protected abstract OrderTransactionType TransactionType { get; }

        /// <summary>
        /// Simple List
        /// </summary>
        public DbSet<SimpleOrderData> SimpleListSet => ctx.SimpleOrderData;

        /// <summary>
        /// 
        /// </summary>
        public ITaskQueuer _taskQueuer;

        private OrderOrderStatus[] closedStatus = {
                OrderOrderStatus.EstimateApproved,
                OrderOrderStatus.EstimateLost,
                OrderOrderStatus.EstimateVoided,
                OrderOrderStatus.OrderClosed,
                OrderOrderStatus.OrderVoided,
                OrderOrderStatus.CreditMemoPosted,
                OrderOrderStatus.CreditMemoVoided,

             };

        /// <summary>
        /// Order Service Layer
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper"></param>
        public BaseTransactionService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
            this._orderKeyDateService = new Lazy<OrderKeyDateService>(() => new OrderKeyDateService(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper));
            this._taskQueuer = taskQueuer;
        }

        /// <summary>
        /// Default children to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] {
            "ContactRoles.OrderContactRoleLocators",
            "Destinations.ContactRoles.OrderContactRoleLocators",
            "EmployeeRoles.Role"
        };

        /// <summary>
        /// Returns the includes
        /// </summary>
        /// <param name="includes">Children to include</param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            if (includes == null)
                return null;

            var tempIncludes = (IExpandIncludes)Activator.CreateInstance(includes.GetType());

            foreach (var prop in includes.GetType().GetProperties())
            {
                prop.SetValue(tempIncludes, prop.GetValue(includes));
            }

            if (tempIncludes != null)
            {
                if (((OrderIncludes)tempIncludes).ItemLevel == IncludesLevel.Simple)
                {
                    // Removing all Simple collections that are implemented as select in C# 
                    // but NOT implemented in EF and SQL
                    ((OrderIncludes)tempIncludes).ItemLevel = IncludesLevel.Full;
                }
                if (((OrderIncludes)tempIncludes).DestinationLevel == IncludesLevel.Simple)
                {
                    // Removing all Simple collections that are implemented as select in C# 
                    // but NOT implemented in EF and SQL
                    ((OrderIncludes)tempIncludes).DestinationLevel = IncludesLevel.Full;
                }
            }

            return tempIncludes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// sets up cloned from links
        /// </summary>
        /// <param name="clonedFromID"></param>
        /// <param name="clone"></param>
        /// <param name="CopyFiles"></param>
        /// <param name="newStatus"></param>
        /// <param name="newItemStatus"></param>
        /// <param name="userLinkID"></param>
        /// <returns></returns>
        protected virtual async Task DoAfterCloneAsync(int clonedFromID, T clone, bool? CopyFiles, OrderOrderStatus? newStatus, short? newItemStatus, short? userLinkID)
        {

            if (clone.Links == null || clone.Links.Count < 0)
            {
                var clonedFrom = await this.ctx.Set<T>().AsNoTracking().FirstOrDefaultAsync(x => x.BID == this.BID && x.ID == clonedFromID);
                ctx.OrderOrderLink.Add(
                    new OrderOrderLink()
                    {
                        BID = this.BID,
                        OrderID = clone.ID,
                        LinkType = OrderOrderLinkType.ClonedFrom,
                        LinkedOrderID = clonedFromID,
                        LinkedFormattedNumber = clonedFrom.FormattedNumber,
                        Description = "",
                    }
                );
                ctx.OrderOrderLink.Add(
                    new OrderOrderLink()
                    {
                        BID = this.BID,
                        OrderID = clonedFromID,
                        LinkType = OrderOrderLinkType.ClonedTo,
                        LinkedOrderID = clone.ID,
                        LinkedFormattedNumber = clone.FormattedNumber,
                        Description = "",
                    }
                );

                try
                {
                    await ctx.SaveChangesAsync();
                    var UserLink = ctx.UserLink.Where(x => x.BID == this.BID && x.ID == userLinkID).First();
                    await _taskQueuer.RecomputeGL(this.BID, (byte)GLEngine.Models.EnumGLType.Order, clone.ID, "Order Cloned", "Order Cloned", UserLink.EmployeeID, null, (byte)GLEntryType.Order_New);
                }
                catch (Exception)
                {

                }

            }

            await base.DoAfterCloneAsync(clonedFromID, clone);

            if (!CopyFiles.HasValue)
            {
                //read option from db
                string key = (((ClassType)clone.ClassTypeID) == ClassType.Order) ? OptionKeyCopyDocuments : OptionEstimateKeyCopyDocuments;
                CopyFiles = (await base.GetUserOptionValueByName(key, this._userLinkId)).ValueAsBoolean();
            }
            if (CopyFiles.GetValueOrDefault())
            {
                // Get clone from object for reference of the copy files
                var clonedFrom = await this.ctx.Set<T>().AsNoTracking().FirstOrDefaultAsync(x => x.BID == this.BID && x.ID == clonedFromID);

                //Copy All Files
                DocumentManager client = base.GetDocumentManager(clonedFromID, (ClassType)clonedFrom.ClassTypeID, BucketRequest.All);
                await client.CloneAllDocumentsToClassTypeAsync(clone.ID, clone.ClassTypeID, includeSpecialFolderBlobName: true);
            }

            //Need to clone Contact and Employee roles...
            await CloneContactAndEmployeeRole(clonedFromID, null, clone.ID, null);

            //Need to update the primary contact
            await UpdateOrderContactToPrimary(clone.ID, clone.CompanyID);

            //Update Entered By to User
            OrderEmployeeRoleService oerservice = new OrderEmployeeRoleService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
            var employeeRoleId = ctx.EmployeeRole.Where(er => er.Name == "Entered By").FirstOrDefault().ID;
            await oerservice.RemoveEmployeeRole(clone.ID, employeeRoleId);
            short? employeeID = ctx.UserLink.Where(ul => ul.BID == BID && ul.ID == userLinkID).First().EmployeeID;
            if (employeeID.HasValue)
                await oerservice.AddEmployeeRole(clone.ID, employeeRoleId, employeeID.Value);

            //Clone notes
            await CloneNotes(clonedFromID, null, clone.ID, null);

            await CreateKeyDateBasedOnOrderStatus(clone.ID, clone.OrderStatusID, true);
            await SetCloneDueDate(clone.ID, clone.TransactionType, DateTime.UtcNow.AddDays(1));

            //Need to clone order items...
            //Note: CopyFiles value is guaranteed a value on the code above.            
            await CloneOrderItemData(clonedFromID, clone.ID, newStatus.Value, CopyFiles.GetValueOrDefault(), (OrderTransactionType)clone.TransactionType, newItemStatus);

            //update default line items status
            await this.UpdateLineItemStatuses(clone.ID, Convert.ToInt16(clone.OrderStatusID), Convert.ToInt16(clone.TransactionType));

            await CloneCustomData(clonedFromID, clone.ID);
            await UpdateCompanyStatus(clone);

            //
            await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(clone.ClassTypeID), Convert.ToInt32(clone.ID));
            await rtmClient.SendRefreshMessage(new RefreshMessage()
            {
                RefreshEntities = new List<RefreshEntity>()
                    {
                        new RefreshEntity()
                        {
                            ClasstypeID = clone.ClassTypeID,
                            DateTime = clone.ModifiedDT,
                            RefreshMessageType = RefreshMessageType.New,
                            BID = clone.BID,
                        }
                    }
            });
        }

        /// <summary>
        /// For Order, Sets the Default Production Due Date of the Order and Each Line Item to the next Day
        /// For Estimate, Set the Followup Due Date for the Estimate to the next Day
        /// </summary>
        /// <param name="cloneID">Clone ID</param>
        /// <param name="transactionType">TransactionType</param>
        /// <param name="dueDate">DateTime</param>
        /// <returns></returns>
        protected async Task<EntityActionChangeResponse> SetCloneDueDate(int cloneID, byte transactionType, DateTime dueDate)
        {

            EntityActionChangeResponse resp;
            if (transactionType == (byte)OrderTransactionType.Order)
            {
                resp = await this._orderKeyDateService.Value.ChangeKeyDate(cloneID, OrderKeyDateType.ProductionDue, dueDate, null);
            }
            else
            {
                resp = await this._orderKeyDateService.Value.ChangeKeyDate(cloneID, OrderKeyDateType.Followup, dueDate, null);
            }

            return resp;
        }

        private async Task<OrderOrderStatus> GetDefaultOrderStatus()
        {
            string optionDefaultOrderStatus = (await new OptionService(this.ctx, migrationHelper).Get(null, "Order.Default.OrderStatus", BID, null, null, null, null, null))?.Value?.Value;

            OrderOrderStatus defaultOrderStatus = OrderOrderStatus.OrderPreWIP;

            if (!string.IsNullOrWhiteSpace(optionDefaultOrderStatus) && int.TryParse(optionDefaultOrderStatus, out int optionDefaultOrderStatusAsNumber))
            {
                try
                {
                    defaultOrderStatus = (OrderOrderStatus)optionDefaultOrderStatusAsNumber;
                }
                catch { }
            }

            return defaultOrderStatus;
        }

        /// <summary>
        /// Clones a model from a given ID
        /// </summary>
        /// <param name="ID">ID of the model to clone</param>
        /// <param name="CopyFiles"></param>
        /// <param name="newTransactionType"></param>
        /// <param name="userLinkID"></param>
        /// <returns></returns>
        public async Task<T> CloneTransactionAsync(int ID, bool? CopyFiles, OrderTransactionType? newTransactionType, short? userLinkID)
        {
            try
            {
                T clone = await this.GetClone(ID);

                await DoBeforeCloneAsync(clone);

                if (newTransactionType.HasValue)
                    clone.TransactionType = (byte)newTransactionType.Value;

                if (clone.TransactionType == (byte)OrderTransactionType.Order)
                    clone.OrderStatusID = await GetDefaultOrderStatus();
                else if (clone.TransactionType == (byte)OrderTransactionType.Estimate)
                    clone.OrderStatusID = OrderOrderStatus.EstimatePending;
                else if (clone.TransactionType == (byte)OrderTransactionType.Memo)
                    clone.OrderStatusID = OrderOrderStatus.CreditMemoUnposted;

                short? newItemStatus = ctx.OrderItemStatus.Where(ois => ois.OrderStatusID == clone.OrderStatusID && ois.BID == BID)
                                                          .OrderBy(x => x.StatusIndex)
                                                          .Select(x => x.ID)
                                                          .FirstOrDefault();

                //default balance due to 0
                clone.PaymentBalanceDue = 0;
                clone.PaymentPaid = 0;
                clone.PaymentTotal = 0;
                clone.PaymentWriteOff = 0;
                clone.PaymentAuthorized = 0;

                T cloneAfterSave = await this.CreateAsync(clone, null);

                await DoAfterCloneAsync(ID, cloneAfterSave, CopyFiles, clone.OrderStatusID, newItemStatus, userLinkID);

                return cloneAfterSave;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Clone failed", e);
                return null;
            }
        }
        private async Task CreateConvertLinks(int estimateID, string estimateFormattedNumber, int orderID, string orderFormattedNumber)
        {
            ctx.OrderOrderLink.Add(
                new OrderOrderLink()
                {
                    BID = this.BID,
                    OrderID = orderID,
                    LinkType = OrderOrderLinkType.ConvertedFrom,
                    LinkedOrderID = estimateID,
                    LinkedFormattedNumber = estimateFormattedNumber,
                    Description = "",
                }
            );
            ctx.OrderOrderLink.Add(
                new OrderOrderLink()
                {
                    BID = this.BID,
                    OrderID = estimateID,
                    LinkType = OrderOrderLinkType.ConvertedTo,
                    LinkedOrderID = orderID,
                    LinkedFormattedNumber = orderFormattedNumber,
                    Description = "",
                }
            );

            try
            {
                await ctx.SaveChangesAsync();
            }
            catch (Exception)
            {

            }

        }

        /// <summary>
        /// Actions to perform on the Order before updating
        /// </summary>
        /// <param name="oldModel">Previous Order model</param>
        /// <param name="newModel">Updated Order model</param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(T oldModel, T newModel)
        {
            if (oldModel.TransactionType != newModel.TransactionType)
                throw new Exception("Cannot modify Transaction Type");

            if (newModel?.Links != null)
            {
                foreach (OrderOrderLink link in newModel.Links)
                {
                    link.BID = newModel.BID;
                }
            }

            if (newModel?.Dates != null)
            {
                newModel.Dates = null;
            }

            await this.SetOrderNumberAndFormattedNumber(newModel, oldModel);

            //Employee roles are saved separately on the UI, this will cause an error -> 'cannot be tracked because another instance of this type with the same key is already being tracked'
            if (newModel.EmployeeRoles != null)
                newModel.EmployeeRoles.Clear();

            //UpdateLineItemStatuses(newModel);
            await base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        /// <summary>
        /// Business logic for after having updated a model
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel">Model object to update</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(T oldModel, T newModel, string connectionID)
        {
            await this.UpdateLineItemStatuses(newModel.ID, (short)newModel.OrderStatusID, newModel.TransactionType);

            await base.DoAfterUpdateAsync(oldModel, newModel, connectionID);
        }

        /// <summary>
        /// update line item order status based from line item's item status id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private void UpdateLineItemStatuses(T model)
        {

            if (model != null && model.Items != null)
            {
                //return closedStatus.Contains(trans.OrderStatusID);

                //update line items order status based from line item status id
                foreach (OrderItemData item in model.Items)
                {
                    if (!closedStatus.Contains(item.OrderStatusID))
                    {
                        var orderItemStatus = ctx.OrderItemStatus.Where(x => x.BID == model.BID && x.ID == item.ItemStatusID).FirstOrDefault();
                        if (orderItemStatus != null)
                        {
                            item.OrderStatusID = (OrderOrderStatus)orderItemStatus.OrderStatusID;
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Sets number and formatted number
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(T newModel, Guid? tempGuid = null)
        {
            CompanyData company = await this.ctx.CompanyData.FirstOrDefaultAsync(c => c.ID == newModel.CompanyID && c.BID == this.BID);
            LocationData companyLocation = await this.ctx.LocationData.FirstOrDefaultAsync(l => l.ID == company.LocationID && l.BID == this.BID);
            if (company != null)
            {
                if (newModel.LocationID == 0)
                {
                    newModel.LocationID = company.LocationID;
                }
                if (newModel.PickupLocationID == 0)
                {
                    newModel.PickupLocationID = company.LocationID;
                }
                if (newModel.ProductionLocationID == 0)
                {
                    newModel.ProductionLocationID = company.LocationID;
                }

                if (newModel.TaxGroupID < 1)
                {
                    short? companyLocationTaxGroupID = companyLocation != null ? companyLocation.DefaultTaxGroupID : null;
                    newModel.TaxGroupID = company.TaxGroupID ?? companyLocationTaxGroupID ?? 0;
                    if (newModel.TaxGroupID < 1)
                    {
                        var activeTaxGroup = await this.ctx.TaxGroup.Where(tg => tg.IsActive == true && tg.BID == this.BID).FirstOrDefaultAsync();
                        newModel.TaxGroupID = activeTaxGroup.ID;
                    }
                }

            }

            if (newModel.ContactRoles != null)
            {
                foreach (var crole in newModel.ContactRoles)
                {
                    crole.ModifiedDT = default(DateTime);
                }
            }
            if (newModel.EmployeeRoles != null)
            {
                foreach (var erole in newModel.EmployeeRoles)
                {
                    erole.ModifiedDT = default(DateTime);
                }
            }

            if (newModel.Items != null)
            {
                foreach (var item in newModel.Items)
                {
                    if (item.TaxGroupID < 1)
                    {
                        item.TaxGroupID = newModel.TaxGroupID;
                    }

                    if (item.ProductionLocationID < 1)
                    {
                        item.ProductionLocationID = newModel.LocationID;
                    }

                    item.ModifiedDT = default(DateTime);
                }
            }

            await this.SetOrderNumberAndFormattedNumber(newModel, null);

            await base.DoBeforeCreateAsync(newModel, tempGuid);

            if (newModel.Items != null)
            {

                foreach (var item in newModel.Items)
                {
                    item.OrderID = newModel.ID;
                    TransactionServiceHelper.SetComponentsIDs(item.Components, newModel.ID, item.ID);

                    if (item?.Notes != null)
                    {
                        foreach (var note in item.Notes)
                        {
                            note.OrderID = newModel.ID;
                            note.OrderItemID = item.ID;
                        }
                    }
                }

                var orderItemEmployeeRoles = newModel.Items
                                                .Where(lineItem => lineItem.EmployeeRoles != null)
                                                .SelectMany(lineItem => lineItem.EmployeeRoles)
                                                .ToList();
                foreach (var eRole in orderItemEmployeeRoles)
                {
                    eRole.OrderID = newModel.ID;
                }

                var orderItemKeyDates = newModel.Items
                                            .Where(lineItem => lineItem.Dates != null)
                                            .SelectMany(lineItem => lineItem.Dates)
                                            .ToList();
                foreach (var keydate in orderItemKeyDates)
                {
                    keydate.OrderID = newModel.ID;
                }
            }

            if (newModel.Notes != null)
            {
                foreach (var note in newModel.Notes)
                {
                    note.CreatedDT = DateTime.UtcNow;
                    note.OrderID = newModel.ID;
                }
            }
            //this.UpdateLineItemStatuses(newModel);
        }

        /// <summary>
        /// Overrides the requests an ID given the BID to always use Order as the ClassTypeID
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="t">Object Type</param>
        /// <returns></returns>
        protected override async Task<int> RequestIDAsync(short bid, Type t)
        {
            return await base.RequestIDAsync(bid, (int)ClassType.Order);
        }

        /// <summary>
        /// Maps the Includes for Simple Items and Simple Destinations for a single Order
        /// </summary>
        /// <param name="item">Order to map</param>
        /// <param name="includes">Includes to map</param>
        /// <returns></returns>
        public override Task MapItem(T item, IExpandIncludes includes = null)
        {
            //END-4853 do not include line item key dates in order
            if (item.Dates != null)
            {
                item.Dates = item.Dates.Where(x => x.OrderItemID == null).ToList();

                foreach (var itemDate in item.Dates)
                {
                    itemDate.KeyDT = itemDate.KeyDT.ToUniversalTime();
                }
            }

            if (includes != null)
            {
                if (((OrderIncludes)includes).ItemLevel == IncludesLevel.Simple)
                {
                    if (item.Items != null)
                    {
                        item.SimpleItems = item.Items
                                            .Select(link => new SimpleOrderItemData()
                                            {
                                                BID = link.BID,
                                                ID = link.ID,
                                                ClassTypeID = link.ClassTypeID,
                                                IsActive = true,
                                                IsDefault = false,
                                                DisplayName = link.Name
                                            }).ToList();

                        item.Items = null;
                    }
                    else
                        item.SimpleItems = null;
                }
                else if (((OrderIncludes)includes).ItemLevel == IncludesLevel.Full)
                {
                    if (item?.Items?.Any() ?? false)
                    {
                        OrderItemComponentService itemComponentService = new OrderItemComponentService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
                        OrderItemSurchargeService itemSurchargeService = new OrderItemSurchargeService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
                        foreach (OrderItemData child in item.Items)
                        {
                            OrderItemService.MapChildTree(child, itemComponentService, itemSurchargeService);
                        }
                    }
                }

                if (((OrderIncludes)includes).DestinationLevel == IncludesLevel.Simple)
                {
                    if (item.Destinations != null)
                    {
                        item.SimpleDestinations = item.Destinations
                                            .Select(link => new SimpleOrderDestinationData()
                                            {
                                                BID = link.BID,
                                                ID = link.ID,
                                                ClassTypeID = link.ClassTypeID,
                                                IsActive = true,
                                                IsDefault = false,
                                                DisplayName = link.Name
                                            }).ToList();

                        item.Destinations = null;
                    }
                    else
                        item.SimpleDestinations = null;
                }
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// Maps the Includes for Simple Items and Simple Destinations for a collection of Orders
        /// </summary>
        /// <param name="collection">Orders to map</param>
        /// <param name="includes">Includes to map</param>
        /// <returns></returns>
        public override async Task<List<T>> MapCollection(List<T> collection, IExpandIncludes includes = null)
        {
            foreach (var item in collection)
                await this.MapItem(item, includes);

            return collection;
        }

        /// <summary>
        /// Child Associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<T, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<T, int>[]
            {
                CreateChildLinkAssociation<OrderOrderLinkService<T>, OrderOrderLink>((order)=> order.Links),
                CreateChildAssociation<OrderKeyDateService, OrderKeyDate, int>((order)=> order.Dates),
                CreateChildAssociation<OrderContactRoleService, OrderContactRole, int>((order)=> order.ContactRoles),
                CreateChildAssociation<OrderEmployeeRoleService, OrderEmployeeRole, int>((order)=> order.EmployeeRoles),
                CreateChildAssociation<OrderItemService, OrderItemData, int>((order)=> order.Items),
                CreateChildAssociation<OrderNoteService, OrderNote, int>((order)=> order.Notes),
                CreateChildAssociation<OrderDestinationService, OrderDestinationData, int>((order)=> order.Destinations),
            };
        }

        override internal void DoBeforeValidate(T newModel)
        {
            if (string.IsNullOrWhiteSpace(newModel.FormattedNumber))
            {
                newModel.FormattedNumber = "XXX-123";
            }
        }

        /// <summary>
        /// Overridden DoBeforeDelete
        /// </summary>
        /// <param name="model"></param>
        public override async Task DoBeforeDeleteAsync(T model)
        {
            this.ctx.Entry(model).Collection(t => t.Dates).Load();
            this.ctx.OrderKeyDate.RemoveRange(model.Dates);

            this.ctx.Entry(model).Collection(t => t.ContactRoles).Load();
            this.ctx.OrderContactRole.RemoveRange(model.ContactRoles);

            this.ctx.Entry(model).Collection(t => t.EmployeeRoles).Load();
            this.ctx.OrderEmployeeRole.RemoveRange(model.EmployeeRoles);

            this.ctx.Entry(model).Collection(t => t.Notes).Load();
            this.ctx.OrderNote.RemoveRange(model.Notes);

            this.ctx.OrderItemTaxAssessment.RemoveRange(this.ctx.OrderItemTaxAssessment.Where(t => t.BID == BID && t.OrderID == model.ID));

            this.ctx.Entry(model).Collection(t => t.Items).Load();
            this.ctx.OrderItemData.RemoveRange(model.Items);

            await base.DoBeforeDeleteAsync(model);
        }

        /// <summary>
        /// Gets a list of Order associated for the given criteria
        /// </summary>
        /// <param name="transactionType">Required. Order, Estimate, PO</param>
        /// <param name="locationID">Location ID for the Orders</param>
        /// <param name="statusID">Status ID for the Orders</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        public async Task<List<T>> GetOrdersForCriteria(OrderTransactionType transactionType, int? locationID, int? statusID, OrderIncludes includes = null)
        {
            var orders = await ctx.Set<T>().IncludeAll(GetIncludes(includes))
                .Where(o => o.BID == this.BID && o.TransactionType == (byte)transactionType
                    && (locationID == null || o.LocationID == locationID)
                    && (statusID == null || o.OrderStatusID == (OrderOrderStatus)statusID)).ToListAsync();

            if (includes != null && (includes.ItemLevel == IncludesLevel.Simple || includes.DestinationLevel == IncludesLevel.Simple))
                await this.MapCollection(orders, includes);

            return orders;
        }

        /// <summary>
        /// Gets a single Order by ID and Version
        /// </summary>
        /// <param name="ID">Order's ID</param>
        /// <param name="version">Order's Version</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        public async Task<T> GetForVersion(int ID, int version, OrderIncludes includes = null)
        {
            return await ctx.Set<T>().IncludeAll(GetIncludes(includes))
                .Where(o => o.BID == this.BID && o.ID == ID).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Changes the Order Status
        /// </summary>
        /// <param name="order"></param>
        /// <param name="newOrderStatus"></param>
        /// <param name="connectionID"></param>
        public async Task<EntityActionChangeResponse> ChangeOrderStatus(T order, OrderOrderStatus newOrderStatus, string connectionID)
        {
            order.OrderStatusID = newOrderStatus;

            await this.UpdateAsync(order, connectionID);

            await this.UpdateLineItemStatuses(order.ID, Convert.ToInt16(order.OrderStatusID), Convert.ToInt16(order.TransactionType));


            return new EntityActionChangeResponse()
            {
                Id = order.ID,
                Message = "Successfully set Order's status to " + GetOrderStatus(newOrderStatus),
                Success = true
            };
        }

        /// <summary>
        /// Return the order status based on the filtered order status for display message only
        /// </summary>
        /// <param name="orderStatus"></param>
        /// <returns></returns>
        private OrderOrderStatus GetOrderStatus(OrderOrderStatus orderStatus)
        {
            OrderOrderStatus status;

            switch(orderStatus)
            {
                case OrderOrderStatus.OrderVoided:
                case OrderOrderStatus.EstimateVoided:
                    status = OrderOrderStatus.Voided;
                    break;
                default:
                    status = orderStatus;
                    break;
                 
            }
                

            return status;
        }

        /// <summary>
        /// Update line items statuses
        /// </summary>
        /// <param name="orderID">Order ID</param>
        /// <param name="orderStatusID">Order Status ID</param>
        /// <param name="transType">Transaction Type</param>
        protected async Task<EntityActionChangeResponse> UpdateLineItemStatuses(int orderID, short orderStatusID, short transType)
        {
            //All of the Line Item statuses that are not at least at that level should be updated to the Default Line Item Status for that Order/ Estimate Status
            List<OrderItemData> orderItemData = await this.ctx.OrderItemData.Where(t =>
                                            (t.BID == BID) &&
                                            (t.OrderID == orderID)
                                            ).ToListAsync();

            foreach (OrderItemData item in orderItemData)
            {
                item.OrderStatusID = (OrderOrderStatus)orderStatusID;
                if (!closedStatus.Contains(item.OrderStatusID))
                {
                    //find the default line item status for order status id
                    OrderItemStatus defaultItemStatus = await this.ctx.OrderItemStatus.Where(i =>
                        i.BID == BID &&
                        i.TransactionType == (byte)transType &&
                        i.OrderStatusID == (OrderOrderStatus)orderStatusID
                    ).OrderByDescending(i => i.IsDefault).FirstOrDefaultAsync();

                    if (defaultItemStatus != null && item.ItemStatusID < defaultItemStatus.ID)
                    {
                        item.ItemStatusID = defaultItemStatus.ID;
                    }
                }
            }

            try
            {
                await ctx.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new EntityActionChangeResponse()
                {
                    Success = false,
                    ErrorMessage = e.ToString()
                };
            }

            return new EntityActionChangeResponse()
            {
                Success = true,
                Message = "Successfully updated default line item statuses"
            };
        }

        /// <summary>
        /// Sets the Order's Number and FormattedNumber
        /// </summary>
        /// <param name="order">Order to set the Number and FormattedNumber on</param>
        /// <param name="oldOrder">The old Order (in case we can re-use its numbers)</param>
        /// <returns></returns>
        protected async Task SetOrderNumberAndFormattedNumber(T order, T oldOrder)
        {
            var location = (await new LocationService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper).GetAsync(order.LocationID));
            string prefix = null;

            if (oldOrder == null)
            {
                if (order.TransactionType == (byte)OrderTransactionType.Estimate)
                {
                    if (await UseOrderNumberForEstimateNumber())
                    {
                        order.Number = await base.GetNextNumber(this.BID, ordNumClassTypeID);
                        await base.SetNextNumber(this.BID, estNumClassTypeID, order.Number + 1);
                    }
                    else
                    {
                        order.Number = await base.GetNextNumber(this.BID, estNumClassTypeID);
                    }
                }
                else if (order.TransactionType == (byte)OrderTransactionType.Order || order.TransactionType == (byte)OrderTransactionType.Memo)
                {
                    order.Number = await base.GetNextNumber(this.BID, ordNumClassTypeID);
                    if (await UseOrderNumberForEstimateNumber())
                        await base.SetNextNumber(this.BID, estNumClassTypeID, order.Number + 1);
                }
            }

            else
                order.Number = order.Number = oldOrder.Number;


            if (order.TransactionType == (byte)OrderTransactionType.Estimate)
                prefix = location?.EstimatePrefix;
            else if (order.TransactionType == (byte)OrderTransactionType.Order)
            {
                if (order.OrderStatusID == OrderOrderStatus.OrderInvoicing
                    || order.OrderStatusID == OrderOrderStatus.OrderInvoiced
                    || order.OrderStatusID == OrderOrderStatus.OrderClosed)
                {
                    prefix = location?.InvoicePrefix;

                    if (oldOrder == null || !oldOrder.InvoiceNumber.HasValue)
                    {
                        string optionUseOrderNumberForInvoice = (await new OptionService(this.ctx, migrationHelper).Get(null, "Order.Invoice.Number.UseOrderNumber", BID, null, null, null, null, null))?.Value?.Value;                        
                        var useOrderNumberAsInvoiceNumber = Convert.ToBoolean(Convert.ToByte(optionUseOrderNumberForInvoice));
                        if (useOrderNumberAsInvoiceNumber)
                        {
                            order.InvoiceNumber = order.Number;
                            await base.SetNextNumber(this.BID, invNumClassTypeID, order.Number + 1, true);
                        }
                        else
                            order.InvoiceNumber = await base.GetNextNumber(this.BID, invNumClassTypeID);
                    }
                }

                else
                    order.InvoiceNumber = null;
                prefix = location?.OrderNumberPrefix;
            }

            order.FormattedNumber = (String.IsNullOrWhiteSpace(prefix)) ? $"{order.Number}" : $"{prefix}-{order.InvoiceNumber ?? order.Number}";
        }

        /// <summary>
        /// Actions to perform after adding a new Order
        /// </summary>
        /// <param name="model">New Order</param>
        /// <param name="tempGuid">Temporary identifier</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(T model, Guid? tempGuid)
        {
            await this.UpdateLineItemStatuses(model.ID, (short)model.OrderStatusID, model.TransactionType);

            await base.InitializeCustomerSharedFolderAsync(model);

            if (model?.Items?.Any() ?? false)
            {
                foreach (var oid in model?.Items)
                {
                    if (oid.TempID.HasValue())
                    {
                        StorageContext ctx = new StorageContext(BID, BucketRequest.All, new DocumentStorage.Models.DMID() { guid = new Guid(oid.TempID) });
                        var documentManager = new DocumentManager(cache, ctx);
                        await documentManager.PersistDocumentsAsync(new Guid(oid.TempID), Convert.ToInt32(oid.ID), oid.ClassTypeID);
                    }
                }
            }

            await UpdateCompanyStatus(model);
            await UpdateContactStatus(model);
            await base.DoAfterAddAsync(model, tempGuid);
        }

        /// <summary>
        /// Updates the Company's Status
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected async Task<bool> UpdateCompanyStatus(T model)
        {
            if ((model.TransactionType == (byte)OrderTransactionType.Order)
                 || (model.TransactionType == (byte)OrderTransactionType.Memo))
            {
                var company = ctx.CompanyData.Where(c => c.BID == model.BID && c.ID == model.CompanyID).First();
                if ((company.StatusID & (byte)CompanyStatus.Customer) == 0) //Check if a prospect or customer
                {
                    company.StatusID = (byte)((company.StatusID | (byte)CompanyStatus.Customer) & (byte)CompanyStatus.CustomerAndVendorAndPersonal);  // Set Customer and Clear Prospect and Lead Flag
                    await ctx.SaveChangesAsync();
                }

            }
            else if (model.TransactionType == (byte)OrderTransactionType.Estimate)
            {
                var company = ctx.CompanyData.Where(c => c.BID == model.BID && c.ID == model.CompanyID).First();
                if ((company.StatusID & ((byte)CompanyStatus.Prospect | (byte)CompanyStatus.Customer)) == 0) //Check if a prospect or customer
                {
                    company.StatusID = (byte)((company.StatusID | (byte)CompanyStatus.Prospect) & (byte)CompanyStatus.ProspectAndVendorAndPersonal);  // Set Prospect and Clear Lead Flag
                    await ctx.SaveChangesAsync();
                }

            }
            return true;
        }

        /// <summary>
        /// Updates the Primary Contact's Status
        /// </summary>
        /// <param name="model"></param>
        /// <param name="estimateToOrderCovert"></param>
        /// <returns></returns>
        protected async Task<bool> UpdateContactStatus(T model, bool estimateToOrderCovert = false)
        {
            if (model.ContactRoles != null)
            {
                var contactRole = model.ContactRoles.FirstOrDefault(r => r.RoleType == OrderContactRoleType.Primary);
                if (contactRole != null)
                {
                    if (model.TransactionType == (byte)OrderTransactionType.Order)
                    {
                        var contact = ctx.ContactData.Where(c => c.BID == model.BID && c.ID == contactRole.ContactID).First();
                        if ((contact.StatusID & (byte)CompanyStatus.Customer) == 0) //Check if a prospect or customer
                        {
                            contact.StatusID = (byte)((contact.StatusID | (byte)CompanyStatus.Customer) & (byte)CompanyStatus.CustomerAndVendorAndPersonal);  // Set Customer and Clear Prospect and Lead Flag
                            await ctx.SaveChangesAsync();
                            await _taskQueuer.IndexModel(BID, ClassType.Contact.ID(), contact.ID);
                        }
                    }
                    else if (model.TransactionType == (byte)OrderTransactionType.Estimate)
                    {
                        var contact = ctx.ContactData.Where(c => c.BID == model.BID && c.ID == contactRole.ContactID).First();
                        if ((contact.StatusID & ((byte)CompanyStatus.Prospect | (byte)CompanyStatus.Customer)) == 0) //Check if a prospect or customer
                        {
                            contact.StatusID = (byte)((contact.StatusID | (byte)CompanyStatus.Prospect) & (byte)CompanyStatus.ProspectAndVendorAndPersonal);  // Set Prospect and Clear Lead Flag
                            await ctx.SaveChangesAsync();
                            await _taskQueuer.IndexModel(BID, ClassType.Contact.ID(), contact.ID);
                        }

                        if (estimateToOrderCovert)
                        {
                            contact.StatusID = (byte)CompanyStatus.Customer;  // Set Prospect and Clear Lead Flag
                            await ctx.SaveChangesAsync();
                            await _taskQueuer.IndexModel(BID, ClassType.Contact.ID(), contact.ID);
                        }
                    }
                }

            }

            return true;
        }

        /// <summary>
        /// Gets an Order by ID for the given query
        /// </summary>
        /// <param name="query">Search Query</param>
        /// <param name="ID">Order's ID</param>
        /// <returns></returns>
        public override IQueryable<T> WherePrimary(IQueryable<T> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Checks if the Order can be deleted
        /// </summary>
        /// <param name="id">Team ID to check for</param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(int id)
        {
            var order = await this.ctx.TransactionHeaderData.Where(t => t.BID == BID
                                                                    && t.ID == id
                                                                    && t.TransactionType == (byte)TransactionType).FirstOrDefaultAsync();
            if (order == null)
                return new BooleanResponse()
                {
                    Success = false,
                    Value = false,
                    Message = "Order not found.",
                    ErrorMessage = "Order not found."
                };

            return new BooleanResponse()
            {
                Success = true,
                Value = true,
                Message = "Order can be deleted."
            };
        }

        /// <summary>
        /// Force Delete
        /// </summary>
        /// <param name="TransactionID">ID of the Order/Estimate to Delete</param>
        /// <param name="userLinkID">UserLink.ID</param>
        public async Task<EntityActionChangeResponse> ForceDelete(short TransactionID, short userLinkID)
        {
            try
            {
                UserLink userLinkData = await ctx.UserLink.Where(ul => ul.BID == this.BID && ul.ID == userLinkID).FirstOrDefaultAsync();

                if (userLinkData == null)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Transaction not found"
                    };
                }

                if (userLinkData.UserAccessType < UserAccessType.SupportManager)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Access denied"
                    };
                }

                TransactionHeaderData toDelete = await this.GetAsync(TransactionID);

                if (toDelete == null)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Transaction not found"
                    };
                }
                List<RefreshEntity> refreshes = DoGetRefreshMessagesOnDelete(toDelete as T);

                // Remove OrderItem's Surcharges
                var items = await ctx.OrderItemData.Where(t => t.BID == this.BID && t.OrderID == TransactionID).Select(t => t.ID).ToListAsync();
                refreshes.AddRange(await ctx.OrderItemSurcharge.RemoveRangeWhereAtomAsync(this.BID, x => items.Contains(x.OrderItemID)));

                // Remove OrderItems
                refreshes.AddRange(await ctx.OrderItemData.RemoveRangeWhereAtomAsync(this.BID, x => x.OrderID == TransactionID));
                refreshes.AddRange(await ctx.OrderItemComponent.RemoveRangeWhereAtomAsync(this.BID, x => x.OrderID == TransactionID));

                var query = ctx.OrderItemTagLink.Where(x => x.BID == BID && x.OrderItem.Order.ID == TransactionID);
                var stuff = await query.ToArrayAsync();
                ctx.OrderItemTagLink.RemoveRange(stuff);

                refreshes.AddRange(await ctx.OrderDestinationData.RemoveRangeWhereAtomAsync(this.BID, x => x.OrderID == TransactionID));

                ctx.OrderDestinationTagLink.RemoveRange(await ctx.OrderDestinationTagLink.Where(x => x.BID == BID && x.Destination.Order.ID == TransactionID).ToArrayAsync());


                refreshes.AddRange(await ctx.OrderCustomData.RemoveRangeWhereAtomAsync(this.BID, x => x.ID == TransactionID));

                refreshes.AddRange(await ctx.OrderEmployeeRole.RemoveRangeWhereAtomAsync(this.BID, x => x.OrderID == TransactionID));
                // Order.Status.Log
                ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == BID && x.Order.ID == TransactionID).ToArrayAsync());

                refreshes.AddRange(await ctx.OrderNote.RemoveRangeWhereAtomAsync(this.BID, x => x.OrderID == TransactionID));

                refreshes.AddRange(await ctx.OrderKeyDate.RemoveRangeWhereAtomAsync(this.BID, x => x.OrderID == TransactionID));

                refreshes.AddRange(await ctx.OrderContactRole.RemoveRangeWhereAtomAsync(this.BID, x => x.OrderID == TransactionID));

                refreshes.AddRange(await ctx.OrderContactRoleLocator.RemoveRangeWhereAtomAsync(this.BID, x => x.ParentID == TransactionID));

                refreshes.AddRange(await ctx.OrderItemTaxAssessment.RemoveRangeWhereAtomAsync(this.BID, x => x.OrderID == TransactionID));
                ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == BID && x.Parent.Order.ID == TransactionID).ToArrayAsync());
                // Order.Version.log
                ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == BID && x.Order.ID == TransactionID).ToArrayAsync());
                // Order.Tax.Item.Assessment
                var paymentMasters = ctx.PaymentMaster
                    .Include(t => t.PaymentApplications)
                    .Where(t => t.BID == BID && t.PaymentApplications.Any(p => p.OrderID == TransactionID))
                    .ToList();
                foreach (var p in paymentMasters)
                {
                    ctx.PaymentApplication.RemoveRange(p.PaymentApplications);
                    ctx.PaymentMaster.Remove(p);
                }

                ctx.TransactionHeaderData.Remove(toDelete);

                var save = ctx.SaveChanges();

                if (save > 0)
                {
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshes });
                    await taskQueuer.IndexModel(refreshes);

                    return new EntityActionChangeResponse()
                    {
                        Success = true,
                        Message = "Successfully Deleted"
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Nothing to delete"
                    };
                }

            }
            catch (DbUpdateException exc)
            {
                return new EntityActionChangeResponse()
                {
                    Success = false,
                    ErrorMessage = exc.InnerException.Message
                };
            }
            catch (Exception exc)
            {
                return new EntityActionChangeResponse()
                {
                    Success = false,
                    ErrorMessage = exc.ToString()
                };
            }
        }

        /// <summary>
        /// Get next order/invoice id from the DB Util
        /// </summary>
        /// <param name="transactionType">Estimate, Order, or Invoice</param>
        /// <returns></returns>
        public async Task<int> GetNextTransactionNumber(ApiTransactionType transactionType)
        {
            var useOrderNumberForEstimateNumber = await UseOrderNumberForEstimateNumber();
            using (var conn = ctx.Database.GetDbConnection())
            {
                try
                {
                    DbCommand command = conn.CreateCommand();
                    DbParameter BIDParam = command.CreateParameter();
                    BIDParam.DbType = System.Data.DbType.Int16;
                    BIDParam.ParameterName = "BID";
                    BIDParam.Value = BID;

                    DbParameter classTypeParam = command.CreateParameter();
                    classTypeParam.DbType = System.Data.DbType.Int32;
                    classTypeParam.ParameterName = "ClassTypeID";
                    switch (transactionType)
                    {
                        case ApiTransactionType.Estimate:
                            if (useOrderNumberForEstimateNumber)
                                classTypeParam.Value = ordNumClassTypeID;
                            else
                                classTypeParam.Value = estNumClassTypeID;
                            break;
                        case ApiTransactionType.Order:
                            classTypeParam.Value = ordNumClassTypeID;
                            break;
                        case ApiTransactionType.Invoice:
                            classTypeParam.Value = invNumClassTypeID;
                            break;
                    }

                    List<DbParameter> dbParams = new List<DbParameter>();
                    dbParams.Add(BIDParam);
                    dbParams.Add(classTypeParam);

                    command.CommandText = $"SELECT TOP 1 [NextID] FROM [Util.NextID] WHERE BID = @BID AND ClassTypeID = @ClassTypeID";
                    command.Parameters.AddRange(dbParams.ToArray());

                    if (conn != null && conn.State == System.Data.ConnectionState.Closed)
                        await conn.OpenAsync();

                    using (DbDataReader reader = await command.ExecuteReaderAsync())
                    {
                        await reader.ReadAsync();
                        return reader.GetInt32(0);
                    }
                }
                catch (Exception ex)
                {
                    return await GetCurrentTransactionNumber(transactionType);
                }
            }
        }

        /// <summary>
        /// Get the biggest order/invoice id from the DB Util
        /// </summary>
        /// <param name="transactionType">Estimate, Order, or Invoice</param>
        /// <returns></returns>
        public async Task<int> GetCurrentTransactionNumber(ApiTransactionType transactionType)
        {
            int currentNum = 0;
            switch (transactionType)
            {
                case ApiTransactionType.Estimate:
                    if (await UseOrderNumberForEstimateNumber())
                        currentNum = await ctx.OrderData.Where(x => x.BID == BID).OrderByDescending(x => x.Number).Select(x => x.Number).FirstOrDefaultAsync();
                    else
                        currentNum = await ctx.OrderData.Where(x => x.BID == BID && x.TransactionType == (byte)OrderTransactionType.Estimate).OrderByDescending(x => x.Number).Select(x => x.Number).FirstOrDefaultAsync();
                    break;
                case ApiTransactionType.Order:
                    if (await UseOrderNumberForEstimateNumber())
                        currentNum = await ctx.OrderData.Where(x => x.BID == BID).OrderByDescending(x => x.Number).Select(x => x.Number).FirstOrDefaultAsync();
                    else
                        currentNum = await ctx.OrderData.Where(x => x.BID == BID && x.TransactionType == (byte)OrderTransactionType.Order).OrderByDescending(x => x.Number).Select(x => x.Number).FirstOrDefaultAsync();
                    break;
                case ApiTransactionType.Invoice:
                    currentNum = await ctx.OrderData.Where(x => x.BID == BID).OrderByDescending(x => x.InvoiceNumber).Select(x => (int)x.InvoiceNumber).FirstOrDefaultAsync();
                    break;
            }
            return currentNum < 1000 ? 1000 : currentNum;
        }

        /// <summary>
        /// Set next order/invoice id from the DB Util
        /// Logic: Check for the max OrderID in the system
        /// </summary>
        /// <param name="transactionType">Estimate, Order, or Invoice</param>
        /// <param name="nextId"></param>
        /// <param name="avoidLowering"></param>
        /// <returns></returns>
        public async Task<BooleanResponse> SetNextTransactionNumber(ApiTransactionType transactionType, int nextId, bool avoidLowering = false)
        {
            try
            {
                var currentNumber = await GetCurrentTransactionNumber(transactionType);
                if (currentNumber >= nextId)
                {
                    currentNumber++;
                    return new BooleanResponse()
                    {
                        ErrorMessage = $"Cannot set {transactionType.ToString()} Number lower than {currentNumber}.",
                        Message = $"Cannot set {transactionType.ToString()} Number lower than {currentNumber}.",
                        Success = false,
                        Value = false
                    };
                }
                int classTypeToUse = 0;
                switch (transactionType)
                {
                    case ApiTransactionType.Estimate:
                        if (await UseOrderNumberForEstimateNumber())
                            classTypeToUse = ordNumClassTypeID;
                        else
                            classTypeToUse = estNumClassTypeID;

                        break;
                    case ApiTransactionType.Order:
                        classTypeToUse = ordNumClassTypeID;
                        break;
                    case ApiTransactionType.Invoice:
                        classTypeToUse = invNumClassTypeID;
                        break;
                }
                await base.SetNextNumber(this.BID, classTypeToUse, nextId, avoidLowering);
                return new BooleanResponse()
                {
                    Message = $"Successfully set {transactionType.ToString()} Number to {nextId}.",
                    Success = true
                };
            }
            catch (Exception ex)
            {
                await logger.Error(BID, $"Error setting {transactionType.ToString()} Number.", ex);
                return new BooleanResponse()
                {
                    ErrorMessage = $"Error setting {transactionType.ToString()} Number.",
                    Message = $"Error setting {transactionType.ToString()} Number.",
                    Success = false,
                    Value = false
                };
            }
        }

        /// <summary>
        /// Checks if we should use the same Number for both Estimates and Orders
        /// </summary>
        /// <returns></returns>
        private async Task<bool> UseOrderNumberForEstimateNumber()
        {
            bool useOrderNumberForEstimateNumber = false;

            string ptionUseOrderNumberForEstimateNumber = (await new OptionService(this.ctx, migrationHelper).Get(null, "Order.Estimate.Number.UseEstimateNumber", BID, null, null, null, null, null))?.Value?.Value;
            if (ptionUseOrderNumberForEstimateNumber != null)
                useOrderNumberForEstimateNumber = Convert.ToBoolean(Convert.ToByte(ptionUseOrderNumberForEstimateNumber));
            
            return useOrderNumberForEstimateNumber;
        }

        /// <summary>
        /// Clone Estimate To Order
        /// </summary>
        /// <param name="ID">ID of the TransactionHeaderData to clone</param>
        /// <param name="newStatus">New Order Status</param>
        /// <param name="CopyFiles">Copy Files</param>
        /// <param name="CopyNotes">Copy Notes</param>
        /// <returns></returns>
        public async Task<TransactionHeaderData> CloneEstimateToOrderAsync(int ID, OrderOrderStatus? newStatus, bool? CopyFiles, bool? CopyNotes)
        {
            try
            {
                EstimateService eservice = new EstimateService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
                EstimateData estimate = (await eservice.GetAsync(ID));
                EstimateData esclone = (await eservice.GetClone(ID));

                await eservice.DoBeforeCloneAsync(esclone);
                esclone.TransactionType = (byte)OrderTransactionType.Order;
                esclone.OrderStatusID = newStatus ?? await GetDefaultOrderStatus();
                EstimateData escloneAfterSave = await eservice.CreateAsync(esclone, null);

                await this.CreateConvertLinks(estimate.ID, estimate.FormattedNumber, escloneAfterSave.ID, escloneAfterSave.FormattedNumber);

                if (CopyFiles.Value)
                {
                    //Copy All Files
                    DocumentManager client = base.GetDocumentManager(ID, ClassType.Estimate, BucketRequest.All);
                    await client.CloneAllDocumentsToClassTypeAsync(escloneAfterSave.ID, (int)ClassType.Order, includeSpecialFolderBlobName: true);
                }

                if (!CopyNotes.HasValue || CopyNotes.Value == true)
                {
                    await CloneNotes(ID, null, escloneAfterSave.ID, null);
                }

                //Need to clone Contact and Employee roles...
                await CloneContactAndEmployeeRole(ID, null, escloneAfterSave.ID, null);

                //Need to clone order items...
                //NOTE: copy files is set to true as order's documents are being copied here
                //NOTE: added a OrderTransactionType parameter as this method is converting from estimate to order

                var OrderItemStatus = ctx.OrderItemStatus.Where(t => t.BID == BID && t.OrderStatusID == esclone.OrderStatusID && t.IsDefault == true).FirstOrDefault();
                short newItemStatus = OrderItemStatus.ID;
                await CloneOrderItemData(ID, escloneAfterSave.ID, esclone.OrderStatusID, true, OrderTransactionType.Order, newItemStatus);

                //update default line item status
                await this.UpdateLineItemStatuses(escloneAfterSave.ID, Convert.ToInt16(escloneAfterSave.OrderStatusID), Convert.ToInt16(escloneAfterSave.TransactionType));

                //Clone Custom Fields
                await CloneCustomFieldsForEstimate(ID, escloneAfterSave.ID);
                await SetCloneDueDate(ID, escloneAfterSave.TransactionType, DateTime.UtcNow.AddDays(1));

                return escloneAfterSave;
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Clone failed", ex);
                return null;
            }
        }

        private async Task<bool> CloneCustomFieldsForEstimate(int oldID, int newID)
        {
            try
            {
                CustomFieldService customFieldService = new CustomFieldService(this.ctx, this.logger, this.rtmClient);
                var customFieldList = customFieldService.ReadCustomFields(this.BID, (int)ClassType.Order, oldID, new CustomFieldValueFilters());
                customFieldService.UpdateCustomFields(this.BID, (int)ClassType.Order, newID, customFieldList?.ToArray());
                return true;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Clone of Estimate Custom Fields failed", e);
                return false;
            }
        }

        protected async Task<bool> CloneContactAndEmployeeRole(int oldOrderID, int? oldOrderItemID, int newOrderID, int? newOrderItemID)
        {
            try
            {
                List<OrderEmployeeRole> oerData = await this.ctx.OrderEmployeeRole.Where(t => t.BID == BID
                                            && t.OrderID == oldOrderID && t.OrderItemID == oldOrderItemID).ToListAsync();
                List<OrderContactRole> ocrData = await this.ctx.OrderContactRole.Where(t => t.BID == BID
                                            && t.OrderID == oldOrderID && t.OrderItemID == oldOrderItemID).ToListAsync();

                if (oerData != null && oerData.Count > 0)
                {
                    OrderEmployeeRoleService oerservice = new OrderEmployeeRoleService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
                    foreach (OrderEmployeeRole oer in oerData)
                    {
                        OrderEmployeeRole oerclone = (await oerservice.GetClone(oer.ID));
                        await oerservice.DoBeforeCloneAsync(oerclone);
                        oerclone.OrderID = newOrderID;
                        oerclone.OrderItemID = newOrderItemID;
                        oerclone.ModifiedDT = DateTime.MinValue;
                        oerclone.Role = null;
                        oerclone.ClassTypeID = 0;
                        OrderEmployeeRole oercloneAfterSave = await oerservice.CreateAsync(oerclone, null);
                    }
                    await ctx.SaveChangesAsync();

                }

                if (ocrData != null && ocrData.Count > 0)
                {
                    OrderContactRoleService ocrservice = new OrderContactRoleService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
                    OrderContactRoleLocatorService oclservice = new OrderContactRoleLocatorService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
                    foreach (OrderContactRole ocr in ocrData)
                    {
                        OrderContactRole ocrclone = (await ocrservice.GetClone(ocr.ID));
                        await ocrservice.DoBeforeCloneAsync(ocrclone);
                        ocrclone.OrderID = newOrderID;
                        ocrclone.OrderItemID = newOrderItemID;
                        ocrclone.ModifiedDT = DateTime.MinValue;
                        ocrclone.ClassTypeID = 0;
                        OrderContactRole ocrcloneAfterSave = await ocrservice.CreateAsync(ocrclone, null);

                        List<OrderContactRoleLocator> oclData = await this.ctx.OrderContactRoleLocator.Where(t => t.BID == BID && t.ParentID == ocr.ID).ToListAsync();
                        foreach (OrderContactRoleLocator ocl in oclData)
                        {
                            OrderContactRoleLocator oclclone = (await oclservice.GetClone(ocl.ID));
                            await oclservice.DoBeforeCloneAsync(oclclone);
                            oclclone.ParentID = ocrcloneAfterSave.ID;
                            oclclone.ModifiedDT = DateTime.MinValue;
                            oclclone.ClassTypeID = 0;
                            OrderContactRoleLocator oclcloneAfterSave = await oclservice.CreateAsync(oclclone, null);
                        }
                            
                    }
                    await ctx.SaveChangesAsync();
                }

                return true;
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Clone failed", ex);
                return false;
            }
        }

        /// <summary>
        /// Update the Order Contact Role into primary contact when cloning an order/estimate.
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        protected async Task<bool> UpdateOrderContactToPrimary(int orderId, int CompanyId)
        {
            try
            {
                CompanyData company = await this.ctx.CompanyData
                    .IncludeAll(new[] { "CompanyContactLinks", "CompanyContactLinks.Contact" })
                    .FirstOrDefaultAsync(c => c.BID == BID && c.ID == CompanyId);

                ContactData primaryContact = company?.CompanyContactLinks?
                    .FirstOrDefault(l => l.IsActive == true && l.IsPrimary == true)?.Contact;

                if (primaryContact != null)
                {
                    OrderContactRole contactRole = await this.ctx.OrderContactRole.Where(t => t.OrderID == orderId).FirstOrDefaultAsync();
                    contactRole.ContactID = primaryContact.ID;
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Updating Order Primary Contact failed", ex);
                return false;
            }

        }

        protected async Task<bool> CloneNotes(int oldOrderID, int? oldOrderItemID, int newOrderID, int? newOrderItemID)
        {
            try
            {
                List<OrderNote> notes = await this.ctx.OrderNote.Where(t => t.BID == BID
                                            && t.OrderID == oldOrderID && t.OrderItemID == oldOrderItemID).ToListAsync();

                if (notes != null)
                {
                    OrderNoteService noteService = new OrderNoteService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
                    foreach (OrderNote note in notes)
                    {
                        OrderNote noteClone = (await noteService.GetClone(note.ID));
                        await noteService.DoBeforeCloneAsync(noteClone);
                        noteClone.OrderID = newOrderID;
                        noteClone.OrderItemID = newOrderItemID;
                        noteClone.ModifiedDT = DateTime.MinValue;
                        noteClone.ClassTypeID = 0;
                        noteClone.Employee = null;
                        await noteService.CreateAsync(noteClone, null);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Clone failed", ex);
                return false;
            }
        }

        protected async Task<bool> CloneCustomData(int oldOrderID, int newOrderID)
        {
            try
            {
                List<OrderCustomData> customItems = await this.ctx.OrderCustomData.AsNoTracking().Where(t => t.BID == BID
                                            && t.ID == oldOrderID).ToListAsync();

                if (customItems != null && customItems.Count > 0)
                {
                    foreach (OrderCustomData data in customItems)
                    {
                        data.ID = newOrderID;
                        data.ModifiedDT = DateTime.MinValue;
                        data.ClassTypeID = 0;
                        ctx.OrderCustomData.Add(data);
                    }
                    return await ctx.SaveChangesAsync() > 0;
                }
                return true;
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Clone failed", ex);
                return false;
            }
        }

        protected async Task<bool> CloneOrderItemData(
            int oldOrderID,
            int newOrderID,
            OrderOrderStatus newStatus,
            bool CopyFiles,
            OrderTransactionType newOrderTransactionType,
            short? newItemStatus = null)
        {
            try
            {
                List<OrderItemData> oiData = await this.ctx.OrderItemData
                                                           .Where(t => t.BID == BID && t.OrderID == oldOrderID && t.OrderStatusID != OrderOrderStatus.EstimateLost && t.OrderStatusID != OrderOrderStatus.EstimateVoided)
                                                           .ToListAsync();
                if (oiData != null)
                {
                    OrderItemService oiservice = new OrderItemService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);

                    foreach (OrderItemData oid in oiData)
                    {
                        OrderItemData oiclone = (await oiservice.GetClone(oid.ID));

                        await oiservice.DoBeforeCloneAsync(oiclone);
                        oiclone.TransactionType = (byte)newOrderTransactionType;
                        oiclone.OrderStatusID = newStatus;
                        oiclone.OrderID = newOrderID;
                        oiclone.Name = oiclone.Name.Replace(" (Clone)", "");
                        if (newItemStatus.HasValue)
                        {
                            oiclone.ItemStatusID = newItemStatus.Value;
                            oiclone.SubStatusID = null;
                        }
                        OrderItemData oicloneAfterSave = await oiservice.CreateAsync(oiclone, null);
                        await oiservice.DoAfterCloneAsync(oid.ID, oiclone);

                        if (oicloneAfterSave != null)
                        {
                            //Need to clone Contact and Employee roles...
                            await CloneContactAndEmployeeRole(oldOrderID, oid.ID, newOrderID, oicloneAfterSave.ID);

                            //Clone notes
                            await CloneNotes(oldOrderID, oid.ID, newOrderID, oicloneAfterSave.ID);

                            //Copy Documents Bucket
                            if (CopyFiles)
                            {
                                DocumentManager oiclient = base.GetDocumentManager(oid.ID, ClassType.OrderItem, BucketRequest.All);
                                await oiclient.CloneAllDocumentsToClassTypeAsync(oiclone.ID, (int)ClassType.OrderItem, includeSpecialFolderBlobName: true);
                            }

                            if (newOrderTransactionType == OrderTransactionType.Order)
                            {
                                await this._orderKeyDateService.Value.ChangeKeyDate(newOrderID, oicloneAfterSave.ID, OrderKeyDateType.ProductionDue, DateTime.UtcNow.AddDays(1), null);
                            }
                        }
                    }
                }

                return await ctx.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Clone failed", ex);
                return false;
            }
        }

        private async Task<bool> ChangeOrderKeyDate(int id, OrderKeyDateType keyDateType, DateTime keyDate)
        {
            OrderKeyDateService orderKeyDateService = new OrderKeyDateService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
            EntityActionChangeResponse keyDateResponse = (await orderKeyDateService.ChangeKeyDate(id, keyDateType, keyDate, null));
            return !keyDateResponse.HasError;
        }

        private async Task<bool> ChangeOrderItemKeyDate(int id, int itemId, OrderKeyDateType keyDateType, DateTime keyDate)
        {
            OrderKeyDateService orderKeyDateService = new OrderKeyDateService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
            EntityActionChangeResponse keyDateResponse = (await orderKeyDateService.ChangeKeyDate(id, itemId, keyDateType, keyDate, null));
            return !keyDateResponse.HasError;
        }

        private async Task<DateTime> GetOrderKeyDate(int id, OrderKeyDateType keyDateType)
        {
            OrderKeyDate okd = (await new OrderKeyDateService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper).GetOrderKeyDate(id, keyDateType));
            if (okd == null)
                return DateTime.MinValue;
            else
                return okd.KeyDT;
        }

        private async Task<DateTime> GetOrderItemKeyDate(int id, int itemId, OrderKeyDateType keyDateType)
        {
            OrderKeyDate okd = (await new OrderKeyDateService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper).GetOrderItemKeyDate(id, itemId, keyDateType));
            if (okd == null)
                return DateTime.MinValue;
            else
                return okd.KeyDT;
        }

        private async Task<bool> CloneOrderKeyDate(int oldOrderID, int newOrderID, OrderKeyDateType keyDateType, DateTime keyDate)
        {
            if ((keyDate == DateTime.MinValue) || (keyDate == DateTime.MaxValue))
            {
                keyDate = await GetOrderKeyDate(oldOrderID, keyDateType);
                if (keyDate == DateTime.MinValue)
                {
                    return true; //skip updating keydate since old order has no keydate value for type...
                }
            }
            if (!await ChangeOrderKeyDate(newOrderID, keyDateType, keyDate))
            {
                return false;
            }

            return true;
        }

        private async Task<bool> CloneOrderItemKeyDate(int oldOrderID, int oldOrderItemID, int newOrderID, int newOrderItemID, OrderKeyDateType keyDateType, DateTime keyDate)
        {
            if ((keyDate == DateTime.MinValue) || (keyDate == DateTime.MaxValue))
            {
                keyDate = await GetOrderItemKeyDate(oldOrderID, oldOrderItemID, keyDateType);
                if (keyDate == DateTime.MinValue)
                {
                    return true; //skip updating keydate since old order item has no keydate value for type...
                }
            }
            if (!await ChangeOrderItemKeyDate(newOrderID, newOrderItemID, keyDateType, keyDate))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Convert Estimate
        /// </summary>
        /// <param name="id">Estimate ID</param>
        /// <param name="convertOption">ConvertEstimateOption</param>
        public async Task<ConvertEstimateResponse> ConvertEstimate(int id, ConvertEstimateOption convertOption)
        {
            TransactionHeaderData estimate = await this.ctx.TransactionHeaderData.Where(t => t.BID == BID
                                                        && t.ID == id
                                                        && t.TransactionType == (byte)OrderTransactionType.Estimate).FirstOrDefaultAsync();

            if (estimate == null)
            {
                return null;
            }

            if (convertOption == null)
            {
                throw new Exception("Cannot convert Estimate without options.");
            }

            if (estimate.OrderStatusID != OrderOrderStatus.EstimatePending)
            {
                throw new Exception("Status has to be PENDING.");
            }

            if (!await CloneOrderKeyDate(id, id, OrderKeyDateType.Converted, DateTime.Now))
            {
                throw new Exception("Cannot change converted date for order " + id.ToString());
            }

            estimate.OrderStatusID = OrderOrderStatus.EstimateApproved;
            await this.UpdateLineItemStatuses(estimate.ID, Convert.ToInt16(estimate.OrderStatusID), Convert.ToInt16(estimate.TransactionType));

            TransactionHeaderData order = await CloneEstimateToOrderAsync(id, convertOption.OrderStatus, convertOption.KeepFiles, convertOption.KeepNotes);

            if (order == null)
            {
                throw new Exception("Clone failed.");
            }

            if (!await CloneOrderKeyDate(estimate.ID, order.ID, OrderKeyDateType.Converted, DateTime.MinValue))
            {
                throw new Exception("Cannot change converted date for order " + order.ID.ToString());
            }
            if (!await CloneOrderKeyDate(estimate.ID, order.ID, OrderKeyDateType.Created, DateTime.Now))
            {
                throw new Exception("Cannot change created date for order " + order.ID.ToString());
            }
            if (!await CloneOrderKeyDate(estimate.ID, order.ID, OrderKeyDateType.ProductionDue, convertOption.DueDate))
            {
                throw new Exception("Cannot clone production due date for order " + order.ID.ToString());
            }

            // Commented for now since the `Convert To Order` Modal on UI only have `Due Date` field which is the ProductionDueDate
            //if (!await CloneOrderKeyDate(estimate.ID, order.ID, OrderKeyDateType.DesignDue, designDueDate))
            //{
            //    throw new Exception("Cannot clone design due date for order " + order.ID.ToString());
            //}
            //if (!await CloneOrderKeyDate(estimate.ID, order.ID, OrderKeyDateType.ShippingDue, shippingDueDate))
            //{
            //    throw new Exception("Cannot clone shipping due date for order " + order.ID.ToString());
            //}
            //if (!await CloneOrderKeyDate(estimate.ID, order.ID, OrderKeyDateType.ArrivalDue, arrivalDueDate))
            //{
            //    throw new Exception("Cannot clone arrival due date for order " + order.ID.ToString());
            //}


            List<OrderItemData> oldOIData = await this.ctx.OrderItemData.Where(t => t.BID == BID
                                                        && t.OrderID == estimate.ID
                                                        && t.TransactionType == (byte)OrderTransactionType.Estimate).ToListAsync();
            List<OrderItemData> newOIData = await this.ctx.OrderItemData.Where(t => t.BID == BID
                                                        && t.OrderID == order.ID
                                                        && t.TransactionType == (byte)OrderTransactionType.Order).ToListAsync();
            //OrderItemStatus orderItemStatus = await this.ctx.OrderItemStatus.Where(t => t.BID == BID
            //                                            && t.TransactionType == (byte)OrderTransactionType.Estimate
            //                                            && t.ClassTypeID == (int)ClassType.OrderItemStatus
            //                                            && t.IsActive == true
            //                                            && t.IsDefault == true
            //                                            && t.OrderStatusID == estimate.OrderStatusID).FirstOrDefaultAsync();

            if (newOIData != null)
            {
                int oldOrderItemIndex = 0;
                foreach (OrderItemData oid in newOIData)
                {
                    if (!await CloneOrderItemKeyDate(estimate.ID, oldOIData.ElementAt(oldOrderItemIndex).ID, order.ID, oid.ID, OrderKeyDateType.ProductionDue, convertOption.DueDate))
                    {
                        throw new Exception("Cannot clone production due date for order " + order.ID.ToString() + " order item " + oid.ID.ToString());
                    }

                    // Commented for now since the `Convert To Order` Modal on UI only have `Due Date` field which is the ProductionDueDate
                    //if (!await CloneOrderItemKeyDate(estimate.ID, oldOIData.ElementAt(oldOrderItemIndex).ID, order.ID, oid.ID, OrderKeyDateType.DesignDue, designDueDate))
                    //{
                    //    throw new Exception("Cannot clone design due date for order " + order.ID.ToString() + " order item " + oid.ID.ToString());
                    //}
                    //if (!await CloneOrderItemKeyDate(estimate.ID, oldOIData.ElementAt(oldOrderItemIndex).ID, order.ID, oid.ID, OrderKeyDateType.ShippingDue, shippingDueDate))
                    //{
                    //    throw new Exception("Cannot clone shipping due date for order " + order.ID.ToString() + " order item " + oid.ID.ToString());
                    //}
                    //if (!await CloneOrderItemKeyDate(estimate.ID, oldOIData.ElementAt(oldOrderItemIndex).ID, order.ID, oid.ID, OrderKeyDateType.ArrivalDue, arrivalDueDate))
                    //{
                    //    throw new Exception("Cannot clone arrival due date for order " + order.ID.ToString() + " order item " + oid.ID.ToString());
                    //}


                    //if (orderItemStatus != null)
                    //{
                    //    oldOIData.ElementAt(oldOrderItemIndex).ItemStatusID = orderItemStatus.ID;
                    //}
                    oldOrderItemIndex++;
                }
            }
            var company = ctx.CompanyData.Where(c => c.BID == order.BID && c.ID == order.CompanyID).First();
            if ((company.StatusID & (byte)CompanyStatus.Customer) == 0) //Check if a prospect or customer
            {
                company.StatusID = (byte)((company.StatusID | (byte)CompanyStatus.Customer) & (byte)CompanyStatus.CustomerAndVendorAndPersonal);  // Set Customer and Clear Prospect and Lead Flag
            }

            if (order.ContactRoles != null)
            {
                var contactRole = order.ContactRoles.FirstOrDefault(r => r.RoleType == OrderContactRoleType.Primary);
                if (contactRole != null)
                {
                    var contact = ctx.ContactData.Where(c => c.BID == order.BID && c.ID == contactRole.ContactID).First();
                    if ((contact.StatusID & (byte)CompanyStatus.Customer) == 0) //Check if a prospect or customer
                    {
                        contact.StatusID = (byte)((contact.StatusID | (byte)CompanyStatus.Customer) & (byte)CompanyStatus.CustomerAndVendorAndPersonal);  // Set Customer and Clear Prospect and Lead Flag
                        await ctx.SaveChangesAsync();
                        await _taskQueuer.IndexModel(BID, ClassType.Contact.ID(), contact.ID);
                    }
                }

            }

            await ctx.SaveChangesAsync();

            try
            {
                this.ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }

            await this.taskQueuer.IndexModel(this.BID, ClassType.Order.ID(), order.ID);
            await this.taskQueuer.IndexModel(this.BID, ClassType.Estimate.ID(), estimate.ID);

            var response = new ConvertEstimateResponse()
            {
                EstimateID = estimate.ID,
                OrderID = order.ID,
                OrderNumber = order.FormattedNumber,
                OldPrice = estimate.PriceTotal ?? 0,
                NewPrice = order.PriceTotal ?? 0,
            };
            response.PriceChange = (response.OldPrice != response.NewPrice);

            if (convertOption.UpdatePricing.Value)
                // recompute order pricing...
                await taskQueuer.OrderPricingCompute(BID, response.OrderID);

            // schedule GL refresh task
            await taskQueuer.RecomputeGL(BID, (byte)GLEngine.Models.EnumGLType.Order, response.OrderID, "Order Status Changed", "Order Status Changed", null, null, (byte)GLEntryType.Order_Status_Change);

            return response;
        }

        /// <summary>
        /// Contact Validation
        /// </summary>
        /// <param name="contactID">Contact ID</param>
        public bool IsValidContact(int? contactID)
        {
            ContactData contact = this.ctx.ContactData.Where(t => t.BID == BID && t.ID == contactID).FirstOrDefault();
            return contact != null;
        }

        /// <summary>
        /// Epmloyee Validation
        /// </summary>
        /// <param name="employeeID">Epmloyee ID</param>
        public bool IsValidEmployee(int? employeeID)
        {
            EmployeeData employee = this.ctx.EmployeeData.Where(t => t.BID == BID && t.ID == employeeID).FirstOrDefault();
            return employee != null;
        }

        /// <summary>
        /// Contact Role Type Validation
        /// </summary>
        /// <param name="roleType">Role type of Contact</param>
        public bool IsValidContactRoleType(OrderContactRoleType roleType)
        {
            var roleContact = this.ctx.EnumOrderContactRoleType.Where(t => t.ID == roleType);

            if (roleContact == null || roleContact.Count() == 0) return false;

            switch (TransactionType)
            {
                case OrderTransactionType.Estimate:
                case OrderTransactionType.PurchaseOrder:
                case OrderTransactionType.Memo:
                    roleContact = roleContact.Where(t => t.AppliesToEstimate == true);
                    break;

                case OrderTransactionType.Order:
                    roleContact = roleContact.Where(t => t.AppliesToOrder == true);
                    break;


                default:
                    return false;
            }
            return roleContact != null;
        }

        /// <summary>
        /// Employee Role ID Validation
        /// </summary>
        /// <param name="roleID">Role ID of Employee</param>
        public bool IsValidEmployeeRoleID(int roleID)
        {
            EmployeeRole roleEmployee = this.ctx.EmployeeRole.Where(t => t.BID == BID && t.ID == roleID).FirstOrDefault();
            return roleEmployee != null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool IsTransactionAlreadyClosed(int ID)
        {
            TransactionHeaderData trans = this.ctx.TransactionHeaderData.Where(t => t.BID == BID && t.ID == ID).AsNoTracking().FirstOrDefault();
            return this.closedStatus.Contains(trans.OrderStatusID);
        }

        /// <summary>
        /// END-8642 Enforce the option "IsPORequired" on the Company record during Order Entry.
        /// Fail Server-Side Save (POST/PUT)
        ///     If the Company's IsPORequired == true
        ///     AND is an Order
        ///     AND the OrderStatus >= WIP
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> IsPORequired(T model)
        {
            CompanyData company = await this.ctx.CompanyData.FirstOrDefaultPrimaryAsync(this.BID, model.CompanyID);

            if (company == null)
                return false;

            return company.IsPORequired && model.TransactionType == (byte)OrderTransactionType.Order && model.OrderStatusID >= OrderOrderStatus.OrderWIP;
        }

        #region Order/Estimate KeyDate Creation
        /// <summary>
        /// Kicks off the RecomputeGL Task on BgEngine
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> CreateKeyDateBasedOnOrderStatus(int OrderID, OrderOrderStatus orderStatus, bool IsNew = false)
        {
            bool UpdateExistingKeyDate = false;
            EntityActionChangeResponse resp = new EntityActionChangeResponse();
            OrderKeyDateType okdt = new OrderKeyDateType();
            resp.Success = true;
            resp.Message = "No OrderKeyDate Created.";

            // Save KeyDate depending on the order status
            // Created        - All      - When order or estimate is created.
            // Voided         - All      - If current Order Status is Voided (19 or 29) and Key Date record does not exist
            // Lost           - Estimate - If current Order Status is Lost (18) and Key Date record does not exist
            // Converted      - Estimate - If current Order Status is Approved (16) and Key Date record does not exist
            // Release to WIP - Order    - If current Order Status is >= WIP(22, 23, 24, 25, or 26) and Key Date record does not exist
            // Built          - Order    - If current Order Status is >= Built(23, 24, 25, or 26) and Key Date record does not exist
            // In Invoicing   - Order    - If current Order Status is >= Invoicing(24, 25, or 26) and Key Date record does not exist
            // Invoiced       - Order    - If current Order Status is >= Invoiced(25 or 26) and Key Date record does not exist
            // Closed         - Order    - If current Order Status is Closed (26) and Key Date record does not exist

            // Sample Scenario: I move an order to built, someone realizes it isn't built and moves it back, 
            //                  when I finish getting it ready, I move it back to built. Date should be updated.

            if (IsNew)
            {
                okdt = OrderKeyDateType.Created;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt);
            }

            if (orderStatus == OrderOrderStatus.EstimatePending)
            {
                okdt = OrderKeyDateType.Pending;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, true);
            }

            if ((orderStatus == OrderOrderStatus.OrderVoided))
            {
                okdt = OrderKeyDateType.Voided;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt);
            }

            if (orderStatus == OrderOrderStatus.EstimateVoided)
            {
                okdt = OrderKeyDateType.Voided;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, true);
            }

            if (orderStatus == OrderOrderStatus.EstimateLost)
            {
                okdt = OrderKeyDateType.Lost;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, true);
            }
            if (orderStatus == OrderOrderStatus.EstimateApproved)
            {
                okdt = OrderKeyDateType.Converted;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt);
            }
            if ((orderStatus == OrderOrderStatus.OrderWIP) || (orderStatus == OrderOrderStatus.OrderBuilt) || (orderStatus == OrderOrderStatus.OrderInvoicing) || (orderStatus == OrderOrderStatus.OrderInvoiced) || (orderStatus == OrderOrderStatus.OrderClosed))
            {
                if (orderStatus == OrderOrderStatus.OrderWIP) // Only update date and replace existing when primary status is selected. See Sample Scenario on comments above.
                    UpdateExistingKeyDate = true;
                else
                    UpdateExistingKeyDate = false;
                okdt = OrderKeyDateType.ReleasedToWIP;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, UpdateExistingKeyDate);
            }
            if ((orderStatus == OrderOrderStatus.OrderBuilt) || (orderStatus == OrderOrderStatus.OrderInvoicing) || (orderStatus == OrderOrderStatus.OrderInvoiced) || (orderStatus == OrderOrderStatus.OrderClosed))
            {
                if (orderStatus == OrderOrderStatus.OrderBuilt) // Only update date and replace existing when primary status is selected. See Sample Scenario on comments above.
                    UpdateExistingKeyDate = true;
                else
                    UpdateExistingKeyDate = false;
                okdt = OrderKeyDateType.Built;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, UpdateExistingKeyDate);
            }
            if ((orderStatus == OrderOrderStatus.OrderInvoicing) || (orderStatus == OrderOrderStatus.OrderInvoiced) || (orderStatus == OrderOrderStatus.OrderClosed))
            {
                if (orderStatus == OrderOrderStatus.OrderInvoicing) // Only update date and replace existing when primary status is selected. See Sample Scenario on comments above.
                    UpdateExistingKeyDate = true;
                else
                    UpdateExistingKeyDate = false;
                okdt = OrderKeyDateType.InInvoicing;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, UpdateExistingKeyDate);
            }
            if ((orderStatus == OrderOrderStatus.OrderInvoiced) || (orderStatus == OrderOrderStatus.OrderClosed))
            {
                if (orderStatus == OrderOrderStatus.OrderInvoiced) // Only update date and replace existing when primary status is selected. See Sample Scenario on comments above.
                    UpdateExistingKeyDate = true;
                else
                    UpdateExistingKeyDate = false;
                okdt = OrderKeyDateType.Invoiced;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, UpdateExistingKeyDate);
            }
            if (orderStatus == OrderOrderStatus.OrderClosed)
            {
                okdt = OrderKeyDateType.Closed;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt);
            }

            return resp.ToResult();
        }

        private async Task<EntityActionChangeResponse> CreateOrderKeyDateIfNotFound(int OrderID, OrderKeyDateType okdt, bool UpdateExisting = false)
        {
            EntityActionChangeResponse resp = new EntityActionChangeResponse();
            resp.Success = true;
            resp.Message = "No OrderKeyDate Created.";

            if (UpdateExisting)
            {
                resp = await this._orderKeyDateService.Value.ChangeKeyDate(OrderID, okdt, null, null);
                await CleanOrderKeyDateRecords(OrderID, okdt);
            }
            else
            {
                OrderKeyDate okd = await this._orderKeyDateService.Value.GetOrderKeyDate(OrderID, okdt);
                if (okd == null)
                {
                    resp = await this._orderKeyDateService.Value.ChangeKeyDate(OrderID, okdt, null, null);
                }
            }

            return resp;
        }

        // Delete all "advanced" OrderKeyDates.
        private async Task<EntityActionChangeResponse> CleanOrderKeyDateRecords(int OrderID, OrderKeyDateType okdt)
        {
            EntityActionChangeResponse resp = new EntityActionChangeResponse();
            resp.Success = true;
            resp.Message = "No Rows Affected.";

            if (okdt == OrderKeyDateType.ReleasedToWIP)
            {
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Built);
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.InInvoicing);
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Invoiced);
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Closed);
            }
            if (okdt == OrderKeyDateType.Built)
            {
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.InInvoicing);
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Invoiced);
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Closed);
            }
            if (okdt == OrderKeyDateType.InInvoicing)
            {
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Invoiced);
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Closed);
            }
            if (okdt == OrderKeyDateType.Invoiced)
            {
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Closed);
            }

            return resp;
        }

        #endregion
    }
}
