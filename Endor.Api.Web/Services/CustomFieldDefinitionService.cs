﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Custom Field Definition Service
    /// </summary>
    public class CustomFieldDefinitionService : AtomCRUDService<CustomFieldDefinition, short>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CustomFieldDefinitionService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Include Defaults
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "Elements" };

        /// <summary>
        /// Returns the includes as an array of strings
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// DoBeforeCreateAsync
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(CustomFieldDefinition newModel, Guid? tempGuid = null)
        {
            if (newModel.DataType == DataType.None)
                newModel.DataType = ctx.EnumDataType.FirstOrDefault(x => x.ID == newModel.DataType).ID;

            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// DoBeforeUpdateAsync
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(CustomFieldDefinition oldModel, CustomFieldDefinition newModel)
        {
            if (oldModel.IsSystem)
                throw new InvalidOperationException("Can't Update System Custom Fields");
            if ((oldModel.AppliesToClassTypeID)!=(newModel.AppliesToClassTypeID))
                throw new InvalidOperationException("The AppliesToClassTypeID value cannot be altered");
            if ((oldModel.AppliesToID) != (newModel.AppliesToID))
                throw new InvalidOperationException("The AppliesToID value cannot be altered");
                       
            await base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        /// <summary>
        /// WherePrimary
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<CustomFieldDefinition> WherePrimary(IQueryable<CustomFieldDefinition> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        internal override void DoBeforeValidate(CustomFieldDefinition newModel)
        {
        }

        /// <summary>
        /// Gets the Child Associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<CustomFieldDefinition, short>[] GetChildAssociations() => new IChildServiceAssociation<CustomFieldDefinition, short>[0];

        /// <summary>
        /// Returns EmployeeRoles based on filters supplied in
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<List<CustomFieldDefinition>> GetWithFiltersAsync(CustomFieldDefinitionFilter filters)
        {
            if (filters != null && filters.HasFilters)
            {
                return await this.GetWhere(filters.WherePredicates());
            }

            return await this.GetAsync();
        }

        /// <summary>
        /// Returns a collection of entities that have the same name
        /// </summary>
        /// <param name="entity">CustomFieldDefinition</param>
        /// <returns></returns>
        public ICollection<CustomFieldDefinition> GetSharedName(CustomFieldDefinition entity)
        {
            return this.ctx.CustomFieldDefinition
                            .Where(x=>
                                x.BID == entity.BID &&
                                x.Name.Trim().ToLower().Equals(entity.Name.Trim().ToLower()) &&
                                x.ID != entity.ID
                            ).ToList();
        }

        /// <summary>
        /// Gets the old model
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task<CustomFieldDefinition> GetOldModelAsync(CustomFieldDefinition newModel)
        {
            var oldModel = await WherePrimary(ctx.CustomFieldDefinition.Include(x => x.Elements).AsNoTracking(), newModel.ID).FirstOrDefaultAsync();

            return oldModel;
        }

    }
}
