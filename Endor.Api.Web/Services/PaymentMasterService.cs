﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Payment master service to get PaymentMaster records
    /// </summary>
    public class PaymentMasterService : AtomCRUDService<PaymentMaster, int>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public PaymentMasterService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            if (includes != null)
            {
                var toInclude = new List<string>();
                if (includes.ToIncludesArray(this.IncludeDefaults).Contains("IncludeApplications"))
                {
                    toInclude.Add("PaymentApplications");
                }
                if (includes.ToIncludesArray(this.IncludeDefaults).Contains("IncludeCompany"))
                {
                    toInclude.Add("Company");
                }
                return toInclude.ToArray();
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<PaymentMaster> WherePrimary(IQueryable<PaymentMaster> query, int ID)
        {
            return query.Where(t => t.BID == BID && t.ID == ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<PaymentMaster, int>[] GetChildAssociations()
        {
            return null;
        }

        internal override void DoBeforeValidate(PaymentMaster newModel)
        {
        }

        internal async Task<List<PaymentMaster>> FilteredGetAsync(PaymentFilters filters, PaymentIncludes includes, int count)
        {
            return await this.Where(filters.WherePredicates(), includes: includes).OrderByDescending(p => p.AccountingDT).Take(count).ToListAsync();
        }
    }
}
