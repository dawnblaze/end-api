﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class DashboardWidgetDefinitionService : AtomGenericService<DashboardWidgetDefinition, short>
    {
        /// <summary>
        /// Constructs Dashboard Widget Definition Service
        /// </summary>
        /// <param name="context">Context</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DashboardWidgetDefinitionService(ApiContext context, IMigrationHelper migrationHelper)
            : base(context, migrationHelper)
        {
        }

        /// <summary>
        /// Default Child Properties to include from Database Context
        /// </summary>
        public string[] IncludeDefaults => new string[]
        {
            "WidgetCategoryLinks",
        };

        /// <summary>
        /// Get the names of navigation properties to include on Get
        /// </summary>
        /// <returns></returns>
        public override string[] GetIncludes()
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// Returns the Dashboard Widgets for the given filters
        /// </summary>
        /// <param name="filters">Dashboard Widget Definition Filters</param>
        /// <returns></returns>
        public async Task<List<DashboardWidgetDefinition>> GetWithFilters(DashboardWidgetDefinitionFilters filters)
        {
            return await this.ctx.Set<DashboardWidgetDefinition>().IncludeAll(GetIncludes())
                .WhereAll(filters.WherePredicates()).ToListAsync();
        }

        /// <summary>
        /// Returns the Dashboard Widget Definition for the given ID
        /// </summary>
        /// <param name="ID">Dashboard Widget Definition ID</param>
        /// <returns></returns>
        public async Task<DashboardWidgetDefinition> GetForID(int ID)
        {
            return await this.ctx.Set<DashboardWidgetDefinition>().IncludeAll(GetIncludes())
                .Where(wd => wd.ID == ID).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Returns the Dashboard Widget Definition Categories for the given filters
        /// </summary>
        /// <param name="filters">Dashboard Widget Definition Category filters</param>
        /// <returns></returns>
        public async Task<string[]> GetCategoriesWithFilters(DashboardWidgetDefinitionCategoryFilters filters)
        {
            var categories = await this.ctx.Set<DashboardWidgetDefinition>().IncludeAll(GetIncludes())
                .WhereAll(filters.WherePredicates())
                .SelectMany(wd => wd.WidgetCategoryLinks)
                .Select(wcl => wcl.CategoryType.ToString()).Distinct().ToArrayAsync();

            return categories;
        }
    }
}
