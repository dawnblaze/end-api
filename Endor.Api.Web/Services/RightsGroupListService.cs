﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Includes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Rights Group List Service Layer
    /// </summary>
    public class RightsGroupListService : AtomCRUDService<RightsGroupList, short>
    {
        private int? _userLinkId = null;
        private const string OptionKeyCopySettings = "RightsGroupList.Clone.CopySettings";
        private const string OptionKeyCopyDocuments = "RightsGroupList.Clone.CopyDocuments";

        /// <summary>
        /// Rights Group List Layer constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public RightsGroupListService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Gets an array of SimpleRightsGroup List
        /// </summary>
        public async Task<SimpleRightsGroup[]> GetSimpleList()
        {
            List<SimpleRightsGroup> simpleList = new List<SimpleRightsGroup>();
            var rightsGroupList = await ctx.Set<RightsGroupList>().Where(m => m.BID == BID).ToListAsync();

            foreach (var rightsGroup in rightsGroupList)
            {
                simpleList.Add(new SimpleRightsGroup
                {
                    BID = this.BID,
                    ID = rightsGroup.ID,
                    ClassTypeID = rightsGroup.ClassTypeID,
                    DisplayName = $"{rightsGroup.Name}",
                    IsActive = true
                });
            }

            return simpleList.ToArray();
        }

        /// <summary>
        /// Default children to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        internal override void DoBeforeValidate(RightsGroupList newModel)
        {
            
        }

        /// <summary>
        /// Returns the includes
        /// </summary>
        /// <param name="includes">Children to include</param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Child Associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<RightsGroupList, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<RightsGroupList, short>[]
            {
                //CreateChildAssociation<TaxGroupService, TaxGroup, short>((a)=>a.SubGroups)
            };
        }

        /// <summary>
        /// Gets an RightsGroupList by ID for the given query
        /// </summary>
        /// <param name="query">Search Query</param>
        /// <param name="ID">RightsGroupList's ID</param>
        /// <returns></returns>
        public override IQueryable<RightsGroupList> WherePrimary(IQueryable<RightsGroupList> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Links or Unlinks a RightsGroupList with a RightsGroup
        /// </summary>
        /// <param name="rightsGroupListID">RightsGroupList.ID</param>
        /// <param name="rightsGroupID">RightsGroup.ID</param>
        /// <param name="isLinked">If it is to link or unlink</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkRightsGroup(short rightsGroupListID, short rightsGroupID, bool isLinked)
        {

            try
            {
                var rightsGroupList = await ctx.RightsGroupList.Where(x => x.BID == this.BID && x.ID == rightsGroupListID).FirstOrDefaultAsync();
                var rightsGroup = await ctx.RightsGroup.Where(x => x.ID == rightsGroupID).FirstOrDefaultAsync();

                // check RightsGroupList specified is valid
                if (rightsGroupList == null)
                {
                    throw new Exception($"Invalid RightsGroupList specified. RightsGroupListID={rightsGroupListID}");
                }

                // check RightsGroup specified is valid
                if (rightsGroup == null)
                {
                    throw new Exception($"Invalid RightsGroup specified. RightsGroupID={rightsGroupID}");
                }

                var rightsGroupLink = new RightsGroupListRightsGroupLink()
                {
                    BID = this.BID,
                    ListID = rightsGroupList.ID,
                    GroupID = rightsGroup.ID
                };

                var existingRightsGroupListRightsGroupLink = ctx.RightsGroupListRightsGroupLink.Where(x => x.BID == this.BID && x.ListID == rightsGroupListID && x.GroupID == rightsGroupID).FirstOrDefault();

                if (isLinked)
                {
                    if (existingRightsGroupListRightsGroupLink == null)
                    {
                        ctx.RightsGroupListRightsGroupLink.Add(rightsGroupLink);
                    }
                }
                else
                {
                    ctx.RightsGroupListRightsGroupLink.Remove(existingRightsGroupListRightsGroupLink);
                }

                var affectedRows = await ctx.SaveChangesAsync();
                if (affectedRows > 0)
                {
                    await QueueIndexForModel(rightsGroupListID);

                    base.DoGetRefreshMessagesOnUpdate(new RightsGroupList()
                    {
                        ClassTypeID = Convert.ToInt32(ClassType.RightsGroupList),
                        ID = Convert.ToInt16(rightsGroupListID),
                        BID = this.BID,
                    });

                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnUpdate(rightsGroupList) });

                    return new EntityActionChangeResponse()
                    {
                        Id = rightsGroupListID,
                        Message = $"Successfully {(isLinked ? "linked" : "unlinked")} RightsGroupList:{rightsGroupListID} to RightsGroup:{rightsGroupID}",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = rightsGroupListID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }

            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        /// <summary>
        /// Overridden CanDelete functionality for RightsGroupList
        /// </summary>
        /// <param name="id">RightsGroupList ID</param>
        /// <returns></returns>
        //https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/753107149/RightsGroupList+API
        public override async Task<BooleanResponse> CanDelete(short id)
        {
            var rightsGroupList = await ctx.RightsGroupList
                                        .Where(x => x.BID == this.BID && x.ID == id)
                                        .Include(x => x.UserLinks).ThenInclude(ul=>ul.Contact)
                                        .Include(x => x.UserLinks).ThenInclude(ul=>ul.Employee)
                                        .Include(x => x.RightsGroupRightLinks)
                                        .FirstOrDefaultAsync();
            if(rightsGroupList == null){
                return new BooleanResponse()
                {
                    ErrorMessage = $"RightsGroupList with ID={id} does not exists",
                    Message = $"RightsGroupList with ID={id} does not exists",
                    Success = false,
                    Value = null
                };
            }

            var linkedContacts = rightsGroupList.UserLinks.Select(l=>l.Contact).Where(c=>c!=null).ToArray();
            var linkedEmployees = rightsGroupList.UserLinks.Select(l=>l.Employee).Where(e=>e!=null).ToArray();
            var rlinks = rightsGroupList?.RightsGroupRightLinks.ToArray();

            var linkCount = linkedContacts.Length+linkedEmployees.Length+rlinks.Length;
            return new BooleanResponse()
            {
                Message = $@"Linked Contacts:{linkedContacts.Length}, Linked Employees:{linkedEmployees.Length}, RightsGroupRightLinks:{rlinks.Length}",
                Success = true,
                Value = linkCount<1
            };
        }

        /// <summary>
        /// Performs additional mapping logic
        /// </summary>
        /// <param name="item">RightsGroupList</param>
        /// <param name="includesExpander"></param>
        /// <returns></returns>
        public override Task MapItem(RightsGroupList item, IExpandIncludes includesExpander = null){
            if (item == null) return Task.CompletedTask;
            var includes = includesExpander?.GetIncludes();
            if(includes!=null){
                if(item.UserLinks == null){
                    item.UserLinks = new HashSet<UserLink>();
                }

                if(item.RightsGroupRightLinks == null){
                    item.RightsGroupRightLinks = new HashSet<RightsGroupListRightsGroupLink>();
                }
                
                //Employee-----------------------------------------------------------------------
                switch (includes[RightsGroupListIncludes.EmployeesIncludeKey].Level){
                    case IncludesLevel.Simple:
                        item.SimpleEmployees = item.UserLinks
                                                .Where(ul=>ul.Employee != null)
                                                .Select(ul=>new SimpleEmployeeData(){
                                                    BID = ul.Employee.BID,
                                                    ID = ul.Employee.ID,
                                                    ClassTypeID = ul.Employee.ClassTypeID,
                                                    DisplayName = ul.Employee.LongName,
                                                    IsActive = ul.Employee.IsActive,
                                                    IsDefault = false
                                                })
                                                .ToList();
                        break;
                    case IncludesLevel.Full:
                        item.Employees = item.UserLinks.Where(ul=>ul.Employee != null)
                                                .Select(ul=>ul.Employee)
                                                .ToList();
                        break;                        
                }

                //Contact-----------------------------------------------------------------------
                switch (includes[RightsGroupListIncludes.ContactsIncludeKey].Level){
                    case IncludesLevel.Simple:
                        item.SimpleContacts = item.UserLinks
                                                .Where(ul=>ul.Contact != null)
                                                .Select(ul=>new SimpleContactData(){
                                                    BID = ul.Contact.BID,
                                                    ID = ul.Contact.ID,
                                                    ClassTypeID = ul.Contact.ClassTypeID,
                                                    DisplayName = ul.Contact.NickName,
                                                    IsActive = ul.Contact.CompanyContactLinks?.Any(l => l.IsActive == true) ?? false,
                                                    IsDefault = ul.Contact.CompanyContactLinks?.Any(l => l.IsPrimary == true) ?? false,
                                                })
                                                .ToList();
                        break;
                    case IncludesLevel.Full:
                        item.Contacts = item.UserLinks.Where(ul=>ul.Contact != null)
                                                .Select(ul=>ul.Contact)
                                                .ToList();
                        break;                        
                }

                //RightsGroup---------------------------------------------------------------------
                switch (includes[RightsGroupListIncludes.RightsGroupIncludeKey].Level){
                    case IncludesLevel.Simple:
                        item.SimpleRightsGroup = item.RightsGroupRightLinks
                                                .Where(rl=>rl.RightsGroup != null)
                                                .Select(rl=>new SimpleRightsGroup(){
                                                    ID = rl.RightsGroup.ID,
                                                    DisplayName = rl.RightsGroup.Name,
                                                    IsActive = true,
                                                    IsDefault = false
                                                })
                                                .ToList();
                        break;
                    case IncludesLevel.Full:
                        item.RightsGroups = item.RightsGroupRightLinks
                                                .Where(rl=>rl.RightsGroup != null)
                                                .Select(rl=>rl.RightsGroup)
                                                .ToList();
                        break;                        
                }
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// before clone hook
        /// makes sure name has cloned appended
        /// and doesn't conflict with an existing record
        /// </summary>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(RightsGroupList clone)
        {
            var existing = await this.ctx.RightsGroupList.Where(l => l.BID == this.BID && l.Name.ToLower() == clone.Name.ToLower()).FirstOrDefaultAsync();
            if (existing != null)
            {
                var originalName = clone.Name;
                const string cloneIdentifier = " (Clone)";
                string currentCloneIdentifier = cloneIdentifier;

                //Ensure record doesn't already exist
                while (await this.ctx.RightsGroupList.WherePrimary(this.BID, x => x.Name.ToLower() == $"{originalName}{currentCloneIdentifier}".ToLower()).FirstOrDefaultAsync() != null)
                {
                    //Record already exists, append clone identifier until it doesn't
                    currentCloneIdentifier += cloneIdentifier;
                }

                //Modify Values
                clone.Name += currentCloneIdentifier;
            }

            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Potentially Copy Settings and Documents after RightsGroupList Clone
        /// </summary>
        /// <param name="clonedFromID"></param>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(short clonedFromID, RightsGroupList clone)
        {

            OptionValue copySettings = (await base.GetUserOptionValueByName(OptionKeyCopySettings, this._userLinkId)).Value;
            OptionValue copyDocuments = (await base.GetUserOptionValueByName(OptionKeyCopyDocuments, this._userLinkId)).Value;

            if (copySettings == null || bool.TryParse(copySettings.Value, out bool copySettingsValue) && copySettingsValue)
            {//Copy if setting is null
                await CopyOptionValues(clonedFromID, clone.ID);
            }

            if (copyDocuments != null && bool.TryParse(copyDocuments.Value, out bool copyDocumentsValue) && copyDocumentsValue)
            {//Don't copy if setting is null
                DocumentManager client = base.GetDocumentManager(clonedFromID, ClassType.RightsGroupList, BucketRequest.Documents);
                await client.CloneAllDocumentsAsync(clone.ID);
            }

            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        /// <summary>
        /// Copy all Options from one RightsGroupList to another RightsGroupList
        /// </summary>
        /// <param name="clonedFromRightsGroupListID"></param>
        /// <param name="newRightsGroupListID"></param>
        /// <returns></returns>
        private async Task<PutOptionValuesResponse> CopyOptionValues(short clonedFromRightsGroupListID, short newRightsGroupListID)
        {
            GetOptionValuesResponse values = await _optionService.Get(BID, clonedFromRightsGroupListID, null, null, null, null, 5 /*Operations*/);

            var optionValues = values.Values
                .Select(o => new Options
                {
                    OptionID = o.OptionID,
                    OptionName = o.Name,
                    Value = o.Value
                }).ToList();

            PutOptionValuesResponse response = await _optionService.Put(optionValues, null, this.BID, newRightsGroupListID, null, null, null, null);

            return response;
        }

        /// <summary>
        /// Clones a RightsGroupList from a given ID
        /// </summary>
        /// <param name="ID">ID of the RightsGroupList to clone</param>
        /// <param name="newName">New RightsGroupList Name</param>
        /// <param name="userLinkId"></param>
        /// <returns></returns>
        public async Task<RightsGroupList> CloneAsync(short ID, string newName, int? userLinkId)
        {
            try
            {
                this._userLinkId = userLinkId;
                var clone = await this.GetClone(ID);

                var rightsGroupRightLinks = await ctx.RightsGroupListRightsGroupLink.Where(x => x.BID == this.BID && x.ListID == ID).ToListAsync();
                clone.RightsGroupRightLinks = new List<RightsGroupListRightsGroupLink>();
                rightsGroupRightLinks.ForEach(x =>
                {
                    clone.RightsGroupRightLinks.Add(new RightsGroupListRightsGroupLink
                    {
                        GroupID = x.GroupID
                    });
                });

                clone.Name = newName;

                await DoBeforeCloneAsync(clone);

                var cloneAfterSave = await this.CreateAsync(clone, null);

                await DoAfterCloneAsync(ID, cloneAfterSave);

                return cloneAfterSave;
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Clone failed", ex);
                return null;
            }
        }


        /// <summary>
        /// DeleteExistingRightsGroupLinks
        /// </summary>
        /// <param name="rightsGroupListID">short</param>
        /// <returns></returns>
        public async Task DeleteExistingRightsGroupLinksAsync(short rightsGroupListID){

            var existingRightsGroupLink = await this.ctx.RightsGroupListRightsGroupLink
                                                .Where(rgl => rgl.ListID==rightsGroupListID)
                                                .ToListAsync();

            if(existingRightsGroupLink.Count>0){
                this.ctx.RightsGroupListRightsGroupLink.RemoveRange(existingRightsGroupLink);
                await this.ctx.SaveChangesAsync();
            }
        }

    }
}