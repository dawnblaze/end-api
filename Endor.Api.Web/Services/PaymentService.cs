using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Request;
using Endor.Api.Web.Classes.Responses;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Payment Service
    /// </summary>
    public class PaymentService : BaseGenericService
    {

        private static int RefundablePaymentMethodTypeId = 250;
        private static int NonRefundablePaymentMethodTypeId = 251;

        /// <summary>
        /// object to queue tasks with
        /// </summary>
        protected readonly ITaskQueuer taskQueuer;
        /// <summary>
        /// Logger
        /// </summary>
        protected readonly RemoteLogger logger;
        /// <summary>
        /// Business ID
        /// </summary>
        public readonly short BID;
        /// <summary>
        /// RTM Client
        /// </summary>
        protected readonly IRTMPushClient rtmClient;
        /// <summary>
        /// object to get cached tenant data
        /// </summary>
        protected readonly ITenantDataCache cache;
        /// <summary>
        /// 
        /// </summary>
        public ServiceModelState ServiceModelState;

        /// <summary>
        /// Constructor
        /// </summary>
        public PaymentService(ApiContext context, RemoteLogger logger, short bid, ITaskQueuer taskQueuer, IRTMPushClient rtmClient, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, migrationHelper)
        {
            this.logger = logger;
            this.BID = bid;
            this.taskQueuer = taskQueuer;
            this.rtmClient = rtmClient;
            this.cache = cache;
            this.ServiceModelState = new ServiceModelState();
        }

        /// <summary>
        /// Get Total Payment Due of orders of a company's location
        /// </summary>
        /// <param name="companyID">company ID</param>
        /// <param name="locationID">location ID</param>
        public async Task<decimal?> GetCompanyLocationTotalPaymentDue(int companyID, int locationID)
        {
            var result = from order in ctx.OrderData
                         where this.BID == order.BID
                            && order.CompanyID == companyID
                            && order.LocationID == locationID
                            && (order.OrderStatusID != OrderOrderStatus.OrderVoided
                                || order.OrderStatusID != OrderOrderStatus.OrderClosed)
                         select order.PaymentBalanceDue;
            return await result.SumAsync();
        }

        private async Task<PaymentMaster> GetPaymentMasterFromRequest(PaymentRequest paymentRequest)
        {

            if (paymentRequest == null)
            {
                ServiceModelState.AddError("Payment Request Not Found", "Payment Request Not Found");
                return null;
            }

            PaymentMaster paymentMaster = new PaymentMaster
            {
                BID = BID,
                ID = await RequestIDAsync(BID, (int)ClassType.PaymentMaster),
                //AccountingDT = DateTime.UtcNow, //let the db set this
                PaymentTransactionType = (byte)paymentRequest.PaymentTransactionType,
                LocationID = paymentRequest.LocationID,
                ReceivedLocationID = paymentRequest.ReceivedLocationID ?? paymentRequest.LocationID,
                CompanyID = paymentRequest.CompanyID,
                Amount = paymentRequest.Amount,
                CurrencyType = paymentRequest.CurrencyType,
                DisplayNumber = paymentRequest.DisplayNumber,
                Notes = paymentRequest.Notes,
                RefBalance = paymentRequest.PaymentTransactionType == (int)PaymentTransactionType.Payment ? paymentRequest.Amount : 0,
                NonRefBalance = (paymentRequest.PaymentTransactionType != (int)PaymentTransactionType.Payment) ? (decimal?)paymentRequest.Amount : null,
            };
            return paymentMaster;
        }

        private PaymentResponse GetPaymentResponse(PaymentMaster paymentMaster)
        {
            if (paymentMaster == null)
                return null;

            PaymentResponse paymentResponse = new PaymentResponse
            {
                PaymentTransactionType = (byte)paymentMaster.PaymentTransactionType,
                Amount = paymentMaster.Amount,
                CurrencyType = paymentMaster.CurrencyType,
                TokenID = null,
                DisplayNumber = paymentMaster.DisplayNumber,
                MasterPayments = new List<PaymentMasterResponse>()
                {
                    new PaymentMasterResponse() {
                        ID = paymentMaster.ID,
                        Applications = paymentMaster.PaymentApplications.Select(a => new PaymentApplicationRequest
                        {
                            OrderID = a.OrderID,
                            Amount = a.Amount
                        }).ToList()
                    }
                }
            };

            return paymentResponse;
        }

        /// <summary>
        /// Virtual method to handle exceptions that occur in CreateAsync
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        protected async virtual Task<PaymentResponse> HandleCreateException(Exception e)
        {
            await logger.Error(this.BID, "Create failed", e);
            throw new Exception(e.Message);
        }

        /// <summary>
        /// Requests an ID from SQL via SPROC
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="classtypeID">Class Type ID</param>
        /// <returns></returns>
        protected async Task<int> RequestIDAsync(short bid, int classtypeID)
        {
            int newID = await RequestIDIntegerAsync(bid, classtypeID);
            try
            {
                return (int)Convert.ChangeType(newID, typeof(int));
            }
            catch (Exception e)
            {
                throw new Exception($"[Util.ID.GetID] returned '{newID}', then failed to Convert.ChangeType into {typeof(int).FullName}", e);
            }
        }

        /// <summary>
        /// Requests an ID from SQL via SPROC as an INT
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="classtypeID">Class Type ID</param>
        /// <returns></returns>
        protected async Task<int> RequestIDIntegerAsync(short bid, int classtypeID)
        {
            string sqlExecute = "EXEC @Result = dbo.[Util.ID.GetID] @bid, @ctid, @count";

            List<object> myParams = new List<object>(){
                new SqlParameter("@bid", bid),
                new SqlParameter("@ctid", classtypeID),
                new SqlParameter("@count", 1)
            };

            if (typeof(PaymentMaster).Equals(typeof(byte)))
            {
                myParams.Add(new SqlParameter("@startingID", 10));
                sqlExecute += ", @startingID";
            }

            SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
            {
                Direction = System.Data.ParameterDirection.Output
            };
            myParams.Add(resultParam);

            await ctx.Database.ExecuteSqlRawAsync(sqlExecute, parameters: myParams);

            return Convert.ToInt32(resultParam.Value);
        }

        /// <summary>
        /// Common Payment Validations
        /// </summary>
        /// <param name="master">Master Payment ID</param>
        public void CommonRefundablePaymentValidations(PaymentMaster master)
        {
            if (master.ChangeInNonRefCredit != null)
                ServiceModelState.AddError("Non Refundable credit", "Non Refundable credit is not allowed for refundable payments");
            if (master.NonRefBalance != null)
                ServiceModelState.AddError("Non Refundable balance", "Non Refundable balance must be null for refundable payment");
            if (master.PaymentApplications.Any(app => app.NonRefBalance != null))
                ServiceModelState.AddError("Non Refundable balance is not allowed", "Non Refundable balance is not allowed for applications on a payment");
            if (master.RefBalance != master.PaymentApplications.Sum(app => app.Amount))
                ServiceModelState.AddError("Master refundable balance", "Master refundable balance must equal sum of application amounts.");

        }

        /// <summary>
        /// Common Non RefundablePayment Validations
        /// </summary>
        /// <param name="master">Master Payment ID</param>
        public void CommonNonRefundablePaymentValidations(PaymentMaster master)
        {
            if (master.NonRefBalance != master.PaymentApplications.Sum(app => app.NonRefBalance))
                ServiceModelState.AddError("Master non-refundable balance", "Master non-refundable balance must equal sum of application non-refundable balance");
            if (master.ChangeInRefCredit != null)
                ServiceModelState.AddError("Refundable credit", "Refundable credit is not allowed for credit gifts");
            if (master.RefBalance != 0m)
                ServiceModelState.AddError("Refundable balance", "Refundable balance must be null for credit gifts");
            if (master.PaymentApplications.Any(app => app.RefBalance != 0m))
                ServiceModelState.AddError("Refundable ", "Refundable balance is not allowed for applications on a credit gift");


        }

        /// <summary>
        /// Common Validations for all Payment Transaction Types
        /// </summary>
        /// <param name="master">PaymentMaster</param>
        public void CommonValidationsForAllPaymentTransactionTypes(PaymentMaster master)
        {
            //I'm using <= 0 here but the Jira ticket description says PaymentMaster.LocationID != null. Current LocationID type is not nullable.
            if (master.LocationID <= 0)
                ServiceModelState.AddError("Master LocationID", "Master LocationID must be valid.");
            if (master.PaymentApplications.Any(app => app.LocationID != master.LocationID))
                ServiceModelState.AddError("Location ID", "The LocationID for applications must match parent LocationID");
            //if (master.CompanyID <= 0)
            //    ServiceModelState.AddError("Master CompanyID", "Master CompanyID must be valid.");
            if (master.PaymentApplications.Any(app => app.CompanyID != master.CompanyID))
                ServiceModelState.AddError("Company ID", "The CompanyID for applications must match parent CompanyID");
            if (master.RefBalance != master.PaymentApplications.Sum(app => app.RefBalance))
                ServiceModelState.AddError("Master refundable balance", "Master refundable balance must equal sum of application refundable balance");
            if (master.Amount < 0)
                ServiceModelState.AddError("Payment Amount", "Payment Amount must be greater than or equal to 0");
            if (master.PaymentApplications == null || master.PaymentApplications.Count <= 0)
            {
                ServiceModelState.AddError("Payment Applications", "At least one payment application must be present");
                return;
            }
        }

        /// <summary>
        /// Get Available Credit for a company
        /// </summary>
        /// <param name="companyid">companyid</param>
        public List<LocationGroupByResult> GetAvailableCredit(int companyid)
        {
            var result = from payment in ctx.PaymentMaster.AsEnumerable()
                         where this.BID == payment.BID && payment.CompanyID == companyid
                         group payment by payment.LocationID into lg
                         select new LocationGroupByResult
                         {
                             LocationID = lg.First().LocationID,
                             RefundableCredit = lg.Sum(p => p.ChangeInRefCredit).GetValueOrDefault(0),
                             NonRefundableCredit = lg.Sum(p => p.ChangeInNonRefCredit).GetValueOrDefault(0),
                         };
            result = result.Where(x => x.TotalCredit != 0);
            return result.ToList();
        }

        /// <summary>
        /// Create Relevent Payment Transactions in db
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="masterPaymentID"></param>
        /// <param name="userLinkID"></param>
        /// <param name="connectionID"></param>
        /// <param name="Capture"></param>
        /// <param name="useNewPaymentInfo"></param>
        /// <returns></returns>
        public async virtual Task<PaymentResponse> CreatePaymentTransactionAsync(PaymentRequest newModel, int? masterPaymentID, short? userLinkID, string connectionID, bool Capture, bool useNewPaymentInfo = false)
        {
            try
            {
                PaymentMaster paymentMaster = null;
                //initial validations on amounts
                switch (newModel.PaymentTransactionType)
                {
                    case (int)PaymentTransactionType.Refund:
                        if ((newModel.PaymentMethodID == (int)PaymentMethodType.NonRefundableCredit) || (newModel.PaymentMethodID == (int)PaymentMethodType.NonRefundableCredit))
                            ServiceModelState.AddError("Payment Method Mismatch", "Refunds To Credit must use the transfertocredit endpoint");
                        goto case (int)PaymentTransactionType.Payment;
                    case (int)PaymentTransactionType.Payment:
                    case (int)PaymentTransactionType.Credit_Gift:
                    case (int)PaymentTransactionType.Credit_Adjustment:
                        if (newModel.Amount != newModel.Applications.Sum(app => app.Amount))
                            ServiceModelState.AddError("Application Amount Error", "Sum of application amounts must equal the total amount");
                        break;
                    case (int)PaymentTransactionType.Refund_to_Nonrefundable_Credit:
                        if (newModel.PaymentMethodID != (int)PaymentMethodType.NonRefundableCredit)
                            ServiceModelState.AddError("Payment Method Mismatch", "Refunds To Non-Refundable Credit must use NonRefundableCredit as the payment method");
                        goto case (int)PaymentTransactionType.Payment_from_Nonrefundable_Credit;
                    case (int)PaymentTransactionType.Refund_to_Refundable_Credit:
                        if (newModel.PaymentMethodID != (int)PaymentMethodType.RefundableCredit)
                            ServiceModelState.AddError("Payment Method Mismatch", "Refunds To Refundable Credit must use RefundableCredit as the payment method");
                        goto case (int)PaymentTransactionType.Payment_from_Nonrefundable_Credit;
                    case (int)PaymentTransactionType.Payment_from_Nonrefundable_Credit:
                    case (int)PaymentTransactionType.Payment_from_Refundable_Credit:
                        if (newModel.Applications.Where(x => x.OrderID == null).Sum(app => app.Amount) != -newModel.Applications.Where(x => x.OrderID != null).Sum(app => app.Amount))
                            ServiceModelState.AddError("Application Amount Error", "Credit amount must be -(Sum of applications amounts)");
                        break;
                    default:
                        ServiceModelState.AddError("Invalid Payment TransactionType", "Invalid Payment TransactionType");
                        break;
                }
                switch (newModel.PaymentTransactionType)
                {
                    case (int)PaymentTransactionType.Payment:
                    case (int)PaymentTransactionType.Credit_Gift:
                        if (newModel.Amount <= 0)
                            ServiceModelState.AddError("Amount Error", "Amount must be greater than 0");
                        break;
                    case (int)PaymentTransactionType.Credit_Adjustment:
                        if (newModel.Applications.Any(a => a.OrderID == null && a.Amount >= 0))
                            ServiceModelState.AddError("Amount Error", "Amount must be greater than 0 if application is associated with a credit memo");
                        break;
                    case (int)PaymentTransactionType.Refund:
                        if (newModel.Amount >= 0)
                            ServiceModelState.AddError("Amount Error", "Amount must be less than 0");
                        break;
                    case (int)PaymentTransactionType.Payment_from_Nonrefundable_Credit:
                    case (int)PaymentTransactionType.Payment_from_Refundable_Credit:
                        if (newModel.Applications.Where(x => x.OrderID == null).ToList().Count > 0)
                            if (newModel.Applications.Where(x => x.OrderID == null).FirstOrDefault().Amount >= 0)
                                ServiceModelState.AddError("Application Amount Error", "Credit adjustment amount must be greater than 0");
                        break;
                    case (int)PaymentTransactionType.Refund_to_Nonrefundable_Credit:
                    case (int)PaymentTransactionType.Refund_to_Refundable_Credit:
                        if (newModel.Applications.Where(x => x.OrderID == null).ToList().Count > 0)
                            if (newModel.Applications.Where(x => x.OrderID == null).FirstOrDefault().Amount <= 0)
                                ServiceModelState.AddError("Application Amount Error", "Credit adjustment amount must be less than 0");
                        break;
                }


                //if credit gift or new payment create payment master
                if ((newModel.PaymentTransactionType == (int)PaymentTransactionType.Credit_Gift)
                    || (newModel.PaymentTransactionType == (int)PaymentTransactionType.Payment))
                {
                    paymentMaster = await GetPaymentMasterFromRequest(newModel);
                    ctx.PaymentMaster.Add(paymentMaster);
                }
                else
                { //otherwise get payment master
                    paymentMaster = ctx.PaymentMaster.Include("PaymentApplications").FirstOrDefault(t => t.ID == masterPaymentID && t.BID == BID);
                    if (paymentMaster == null)
                    {
                        ServiceModelState.AddError("Application Error", "Master Payment could not be found");
                        return null;
                    }
                    //adjust payment master
                    if (newModel.PaymentTransactionType == (int)PaymentTransactionType.Refund)
                    {
                        paymentMaster.RefBalance += newModel.Amount;

                    }
                    if (newModel.PaymentTransactionType == (int)PaymentTransactionType.Credit_Adjustment)
                    {
                        paymentMaster.NonRefBalance = (paymentMaster.NonRefBalance ?? 0m) + newModel.Amount;
                    }
                }

                if ((newModel.PaymentTransactionType == (int)PaymentTransactionType.Credit_Adjustment) || (newModel.PaymentTransactionType == (int)PaymentTransactionType.Credit_Gift))
                {
                    var creditMemos = newModel.Applications.Where(x => x.OrderID != null).Select(o => o.OrderID).Distinct();
                    foreach (var orderID in creditMemos)
                    {
                        var order = ctx.Set<TransactionHeaderData>().AsNoTracking().Where(o => o.ID == orderID && o.BID == BID).FirstOrDefault();
                        if (order.TransactionType != (byte)OrderTransactionType.Memo)
                            ServiceModelState.AddError("TransactionType Mismatch Error", "Credit Gifts and adjustments can only apply to Credit Memos if the orderID field is specified");
                    }
                }

                if (!ServiceModelState.IsValid)
                    return null;
                //validate location
                var orderIds = newModel.Applications.Where(x => x.OrderID != null).Select(o => o.OrderID).Distinct();
                var locations = ctx.Set<TransactionHeaderData>().AsNoTracking().Where(o => orderIds.Contains(o.ID) && o.BID == BID).Select(o => o.LocationID).Distinct();
                if (locations.Count() > 1 ||
                    (locations.Count() == 1) && (locations.First() != paymentMaster.LocationID))
                    ServiceModelState.AddError("Location Mismatch Error", "Location for applications must match location of parent location");
                //validate same company
                var companies = ctx.Set<TransactionHeaderData>().AsNoTracking().Where(o => orderIds.Contains(o.ID) && o.BID == BID).Select(o => o.CompanyID).Distinct();
                if (companies.Count() > 1 ||
                    (companies.Count() == 1) && (companies.First() != paymentMaster.CompanyID))
                    ServiceModelState.AddError("Company Mismatch Error", "Company for application on orders must match location of parent company");
                if (!ServiceModelState.IsValid)
                    return null;

                //create new applications on payment master
                await CreateApplicationsOnPaymentMaster(newModel, paymentMaster, userLinkID, useNewPaymentInfo);

                if (!ServiceModelState.IsValid)
                    return null;

                //validate
                CommonValidationsForAllPaymentTransactionTypes(paymentMaster);
                switch (newModel.PaymentTransactionType)
                {
                    case (int)PaymentTransactionType.Payment:
                    case (int)PaymentTransactionType.Refund:
                    case (int)PaymentTransactionType.Payment_from_Refundable_Credit:
                    case (int)PaymentTransactionType.Refund_to_Refundable_Credit:
                        CommonRefundablePaymentValidations(paymentMaster);
                        break;
                    case (int)PaymentTransactionType.Credit_Gift:
                    case (int)PaymentTransactionType.Credit_Adjustment:
                    case (int)PaymentTransactionType.Payment_from_Nonrefundable_Credit:
                    case (int)PaymentTransactionType.Refund_to_Nonrefundable_Credit:
                        CommonNonRefundablePaymentValidations(paymentMaster);
                        break;

                }
                if (!ServiceModelState.IsValid)
                    return null;
                //Save Changes
                await ctx.SaveChangesAsync();

                // adjust orders, company credit - send rtc messages - recompute gl
                await DoAfterPaymentTransactionAsync(paymentMaster, userLinkID, connectionID, newModel);

                //generate payment response - return
                return GetPaymentResponse(paymentMaster);
            }
            catch (Exception e)
            {
                return await HandleCreateException(e);
            }
        }

        private async Task CreateApplicationsOnPaymentMaster(PaymentRequest newModel, PaymentMaster paymentMaster, short? userLinkID, bool useNewPaymentInfo)
        {
            //create new applications on payment master
            var userLink = userLinkID.HasValue ? ctx.UserLink.Where(ul => ul.AuthUserID == userLinkID && ul.BID == BID).FirstOrDefault() : null;
            var applicationGroupID = 0;

            if (paymentMaster == null)
            {
                throw new ArgumentNullException(nameof(paymentMaster));
            }

            foreach (var application in newModel.Applications.OrderBy(a => a.OrderID.HasValue))
            {
                PaymentApplication paymentApplication = new PaymentApplication()
                {
                    BID = BID,
                    ID = await RequestIDAsync(BID, ClassType.PaymentApplication.ID()),
                    MasterID = paymentMaster.ID,
                    PaymentTransactionType = (byte)newModel.PaymentTransactionType,
                    CompanyID = paymentMaster.CompanyID,
                    OrderID = application.OrderID,
                    Amount = application.Amount,
                    LocationID = paymentMaster.LocationID,
                    EnteredByEmployeeID = userLink?.EmployeeID,
                    EnteredByContactID = userLink?.ContactID,
                    PaymentType = (PaymentMethodType)newModel.PaymentType,
                    CurrencyType = paymentMaster.CurrencyType,
                    Notes = paymentMaster.Notes
                };
                //payment method
                switch (newModel.PaymentTransactionType)
                {
                    case (int)PaymentTransactionType.Payment:
                    case (int)PaymentTransactionType.Refund:
                        paymentApplication.PaymentMethodID = newModel.PaymentMethodID;
                        break;
                    case (int)PaymentTransactionType.Credit_Gift:
                    case (int)PaymentTransactionType.Credit_Adjustment:
                    case (int)PaymentTransactionType.Payment_from_Nonrefundable_Credit:
                    case (int)PaymentTransactionType.Refund_to_Nonrefundable_Credit:
                        paymentApplication.PaymentMethodID = NonRefundablePaymentMethodTypeId;
                        break;
                    case (int)PaymentTransactionType.Payment_from_Refundable_Credit:
                    case (int)PaymentTransactionType.Refund_to_Refundable_Credit:
                        paymentApplication.PaymentMethodID = RefundablePaymentMethodTypeId;
                        break;
                }
                //Non/Refundable Payment Balance
                switch (newModel.PaymentTransactionType)
                {
                    case (int)PaymentTransactionType.Payment:
                        paymentApplication.RefBalance = application.Amount;
                        break;
                    case (int)PaymentTransactionType.Refund_to_Refundable_Credit:
                        if (application.OrderID == null) paymentApplication.RefBalance = application.Amount;
                        break;
                    case (int)PaymentTransactionType.Payment_from_Refundable_Credit:
                        if (application.OrderID != null) paymentApplication.RefBalance = application.Amount;
                        break;
                    case (int)PaymentTransactionType.Credit_Adjustment:
                    case (int)PaymentTransactionType.Credit_Gift:
                        if(application.Amount > 0)
                            paymentApplication.NonRefBalance = application.Amount;
                        break;
                    case (int)PaymentTransactionType.Refund_to_Nonrefundable_Credit:
                        if (application.OrderID == null) paymentApplication.NonRefBalance = application.Amount;
                        break;
                    case (int)PaymentTransactionType.Payment_from_Nonrefundable_Credit:
                        if (application.OrderID != null) paymentApplication.NonRefBalance = application.Amount;
                        break;
                }
                if (applicationGroupID == 0)
                    applicationGroupID = paymentApplication.ID;
                paymentApplication.ApplicationGroupID = applicationGroupID;
                ctx.PaymentApplication.Add(paymentApplication);
                paymentMaster.PaymentApplications.Add(paymentApplication);



                //changes to master payment
                switch (newModel.PaymentTransactionType)
                {
                    case (int)PaymentTransactionType.Payment:
                    case (int)PaymentTransactionType.Payment_from_Refundable_Credit:
                    case (int)PaymentTransactionType.Refund_to_Refundable_Credit:
                    case (int)PaymentTransactionType.Refund:
                        paymentMaster.ChangeInRefCredit = (paymentMaster.ChangeInRefCredit ?? 0m) + (application.OrderID == null ? application.Amount : 0m);
                        break;
                    case (int)PaymentTransactionType.Credit_Gift:
                    case (int)PaymentTransactionType.Credit_Adjustment:
                        paymentMaster.ChangeInNonRefCredit = (paymentMaster.ChangeInNonRefCredit ?? 0m) + application.Amount;
                        break;
                    case (int)PaymentTransactionType.Payment_from_Nonrefundable_Credit:
                    case (int)PaymentTransactionType.Refund_to_Nonrefundable_Credit:
                        paymentMaster.ChangeInNonRefCredit = (paymentMaster.ChangeInNonRefCredit ?? 0m) + (application.OrderID == null ? application.Amount : 0m);
                        break;
                }
                //changes to application references
                switch (newModel.PaymentTransactionType)
                {
                    case (int)PaymentTransactionType.Refund_to_Refundable_Credit:
                        if (application.OrderID != null) goto case (int)PaymentTransactionType.Refund;
                        break;
                    case (int)PaymentTransactionType.Payment_from_Refundable_Credit:
                        if (application.OrderID == null) goto case (int)PaymentTransactionType.Refund;
                        break;
                    case (int)PaymentTransactionType.Refund:
                        var applicationsToRefund = paymentMaster.PaymentApplications.Where(app => app.OrderID == application.OrderID && app.RefBalance > 0);

                        if (applicationsToRefund.Count() == 0)
                            ServiceModelState.AddError("Applications", "There are no applications to refund for this master payment on Order with ID of " + application.OrderID);

                        if (Math.Abs(application.Amount) > applicationsToRefund.Sum(a => a.RefBalance))
                            ServiceModelState.AddError("Applications", "There is not enough refundable balance on Order with ID of " + application.OrderID);

                        var amountToAllocate = -application.Amount;
                        foreach (var origPaymentApplication in applicationsToRefund)
                        {
                            if (amountToAllocate > 0)
                            {
                                //what is the max amount to use here
                                var amountToUse = Math.Min(origPaymentApplication.RefBalance, amountToAllocate);
                                origPaymentApplication.RefBalance -= amountToUse;
                                amountToAllocate -= amountToUse;
                                origPaymentApplication.HasAdjustments = true;
                                //origPaymentApplication.ModifiedDT = DateTime.UtcNow;
                                ctx.Entry(origPaymentApplication).Property(t => t.ValidToDT).IsModified = false;
                                if (paymentApplication.AdjustedApplicationID == null) paymentApplication.AdjustedApplicationID = origPaymentApplication.ID;
                                if (!useNewPaymentInfo && (newModel.PaymentTransactionType == (int)PaymentTransactionType.Refund))
                                    paymentApplication.PaymentMethodID = origPaymentApplication.PaymentMethodID;
                            }
                        }

                        break;
                    case (int)PaymentTransactionType.Refund_to_Nonrefundable_Credit:
                        if (application.OrderID != null) goto case (int)PaymentTransactionType.Credit_Adjustment;
                        break;
                    case (int)PaymentTransactionType.Payment_from_Nonrefundable_Credit:
                        if (application.OrderID == null) goto case (int)PaymentTransactionType.Credit_Adjustment;
                        break;
                    case (int)PaymentTransactionType.Credit_Adjustment:
                        applicationsToRefund = paymentMaster.PaymentApplications.Where(app => app.OrderID == application.OrderID && app.NonRefBalance > 0);
                        
                        if ((paymentMaster.PaymentTransactionType == (byte)PaymentTransactionType.Credit_Gift)
                            && (newModel.PaymentTransactionType != (int)PaymentTransactionType.Refund_to_Nonrefundable_Credit)) //if it's a refund of an order, we don't want to pick up all the refundable transactions
                        {
                            applicationsToRefund = paymentMaster.PaymentApplications.Where(app => 
                                ((app.PaymentTransactionType == (byte)PaymentTransactionType.Credit_Gift) || (app.PaymentTransactionType == (byte)PaymentTransactionType.Refund_to_Nonrefundable_Credit))
                                && app.NonRefBalance>0);
                            //Not applicable anymore because you could have refunds back to Nonrefundable credit
                            //if (applicationsToRefund.Count() > 1)
                            //    ServiceModelState.AddError("Applications", "A Credit Gift master should only have one Credit Gift applicaiton. Master Transaction ID:" + paymentMaster.ID); 
                        }
                        if (applicationsToRefund.Count() == 0)
                            ServiceModelState.AddError("Applications", "There are no applications to refund for this master payment on Order with ID of:" + application.OrderID);

                        if (Math.Abs(application.Amount) > applicationsToRefund.Sum(a => a.NonRefBalance))
                            ServiceModelState.AddError("Applications", "There is not enough refundable balance on Order with ID of" + application.OrderID);

                        amountToAllocate = -application.Amount;
                        foreach (var origPaymentApplication in applicationsToRefund)
                        {
                            if (amountToAllocate > 0)
                            {
                                //what is the max amount to use here
                                var amountToUse = Math.Min(origPaymentApplication.NonRefBalance.GetValueOrDefault(), amountToAllocate);
                                origPaymentApplication.NonRefBalance -= amountToUse;
                                amountToAllocate -= amountToUse;
                                origPaymentApplication.HasAdjustments = true;
                                //origPaymentApplication.ModifiedDT = DateTime.UtcNow;
                                ctx.Entry(origPaymentApplication).Property(t => t.ValidToDT).IsModified = false;
                                if (paymentApplication.AdjustedApplicationID == null) paymentApplication.AdjustedApplicationID = origPaymentApplication.ID;
                            }
                        }
                        break;
                }

                if (!ServiceModelState.IsValid)
                    return;

            }
        }

        /// <summary>
        /// Actions to take after 
        /// </summary>
        /// <param name="paymentMaster"></param>
        /// <param name="userLinkID"></param>
        /// <param name="connectionID"></param>
        /// <param name="paymentRequest"></param>
        /// <returns></returns>
        public async Task DoAfterPaymentTransactionAsync(PaymentMaster paymentMaster, short? userLinkID, string connectionID, PaymentRequest paymentRequest = null)
        {
            switch ((PaymentTransactionType)paymentRequest.PaymentTransactionType)
            {
                case PaymentTransactionType.Payment:
                case PaymentTransactionType.Credit_Gift:
                case PaymentTransactionType.Refund:
                case PaymentTransactionType.Credit_Adjustment:
                case PaymentTransactionType.Payment_from_Refundable_Credit:
                case PaymentTransactionType.Payment_from_Nonrefundable_Credit:
                case PaymentTransactionType.Refund_to_Refundable_Credit:
                case PaymentTransactionType.Refund_to_Nonrefundable_Credit:
                    await DoAfterPayment(paymentMaster, connectionID, paymentRequest);
                    break;
                default:
                    break;
            }
        }

        private async Task DoAfterPayment(PaymentMaster paymentMaster, string connectionID, PaymentRequest paymentRequest)
        {
            var newApplicationGroupID = paymentMaster.PaymentApplications.Last().ApplicationGroupID;
            var newPaymentApplications = paymentMaster.PaymentApplications.Where(app => app.ApplicationGroupID == newApplicationGroupID);


            foreach (var app in newPaymentApplications.Where(app => app.OrderID != null))
            {

                var order = ctx.TransactionHeaderData.Where(o => o.ID == app.OrderID && o.BID == BID).First();
                if (order.TransactionType == (byte)OrderTransactionType.Order)
                {
                    // fix orderstatus
                    await FixOrderStatus(order.ID, app.Amount, connectionID);
                }

                if(ctx.Entry(app).State == EntityState.Modified)
                {
                    ctx.Entry(app).Property(pa => pa.BID).IsModified = false;//mark BID as not modified https://corebridge.atlassian.net/browse/END-11196
                }

                await ctx.SaveChangesAsync();

                if (order.TransactionType != (byte)OrderTransactionType.Memo)
                {
                    await this.taskQueuer.RecomputeGL(BID, (byte)GLEngine.Models.EnumGLType.Order, order.ID, "Order Status Changed", "Order Status Changed", null, null, (byte)GLEntryType.Order_Status_Change);
                    await QueueIndexForModel(order);
                    await SendRefreshForChangedEntity(order);
                }
            }

            var creditAmount = newPaymentApplications.Where(app => (app.OrderID == null)).Sum(app => app.Amount);
            // check if an adjustment is for credit memo
            if ((PaymentTransactionType)paymentRequest.PaymentTransactionType == PaymentTransactionType.Credit_Gift
                || (PaymentTransactionType)paymentRequest.PaymentTransactionType == PaymentTransactionType.Credit_Adjustment)
            {
                var orderID = newPaymentApplications.Select(t => t.OrderID).First();
                var order = ctx.TransactionHeaderData.FirstOrDefault(t => t.ID == orderID && t.TransactionType == (byte)OrderTransactionType.Memo);
                if (order != null)
                {
                    creditAmount += newPaymentApplications.Sum(t => t.Amount);
                }
            }

            if (creditAmount != 0m)
            {
                var company = ctx.CompanyData.Where(c => c.ID == paymentMaster.CompanyID).First();
                if (company == null)
                {
                    ServiceModelState.AddError("Company", "Could not find company with ID:" + paymentMaster.CompanyID.ToString());
                    return;
                }

                if (((PaymentTransactionType)paymentRequest.PaymentTransactionType == PaymentTransactionType.Refund_to_Refundable_Credit)
                    || ((PaymentTransactionType)paymentRequest.PaymentTransactionType == PaymentTransactionType.Payment_from_Refundable_Credit)
                    || ((PaymentTransactionType)paymentRequest.PaymentTransactionType == PaymentTransactionType.Refund)
                    || ((PaymentTransactionType)paymentRequest.PaymentTransactionType == PaymentTransactionType.Payment))
                    company.RefundableCredit = (company.RefundableCredit ?? 0m) + creditAmount;
                else
                    company.NonRefundableCredit = (company.NonRefundableCredit ?? 0m) + creditAmount;

                ctx.CompanyData.Update(company);
                await ctx.SaveChangesAsync();
                await QueueIndexForModel(company);
                await SendRefreshForChangedEntity(company);
            }

            try
            {
                foreach (var app in newPaymentApplications)
                {
                    await this.taskQueuer.RecomputeGL(BID, (byte)GLEngine.Models.EnumGLType.Payment, app.ID, "Credit Applied", "Credit Applied", null, null, (byte)GLEntryType.Credit_Applied);
                }

                await QueueIndexForModel(paymentMaster);
                await SendRefreshForChangedEntity(paymentMaster);
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Indexing failed after CREATE", ex);
            }
        }

        private async Task FixOrderStatus(int orderID, decimal appAmount, string connectionID)
        {
            var order = ctx.OrderData.First(t => t.BID == BID && t.ID == orderID);
            order.PaymentPaid = order.PaymentPaid + appAmount;
            ctx.TransactionHeaderData.Update(order);

            var remainingBalanceDue = order.PaymentBalanceDue - appAmount;

            if (order.OrderStatusID == OrderOrderStatus.OrderInvoiced && remainingBalanceDue == 0m)
            {
                var orderService = new OrderService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
                var response = await orderService.ChangeOrderStatus(order, OrderOrderStatus.OrderClosed, connectionID);

                if (response.Success)
                {
                    // call the task service
                    await orderService.CreateKeyDateBasedOnOrderStatus(order.ID, OrderOrderStatus.OrderClosed);
                }
            }
            if (order.OrderStatusID == OrderOrderStatus.OrderClosed && remainingBalanceDue > 0)
            {
                var orderService = new OrderService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
                var response = await orderService.ChangeOrderStatus(order, OrderOrderStatus.OrderInvoiced, connectionID);

                if (response.Success)
                {
                    // call the task service
                    await orderService.CreateKeyDateBasedOnOrderStatus(order.ID, OrderOrderStatus.OrderInvoiced);
                }
            }
        }

        private async Task SendRefreshForChangedEntity(dynamic entity)
        {
            await rtmClient.SendRefreshMessage(new RefreshMessage()
            {
                RefreshEntities = new List<RefreshEntity>()
                {
                    GetRefreshEntityForChangedEntity(entity)
                }
            });
        }

        private static RefreshEntity GetRefreshEntityForChangedEntity(dynamic entity)
        {
            return new RefreshEntity()
            {
                ClasstypeID = entity.ClassTypeID,
                DateTime = entity.ModifiedDT,
                RefreshMessageType = RefreshMessageType.Change,
                BID = entity.BID,
                ID = entity.ID
            };
        }

        /// <summary>
        /// Re-index the the model
        /// </summary>
        /// <param name="entity">Model Id</param>
        /// <returns></returns>
        internal async Task QueueIndexForModel(dynamic entity)
        {
            await this.taskQueuer.IndexModel(this.BID, entity.ClassTypeID, Convert.ToInt32(entity.ID));
        }

        /// <summary>
        /// Create Relevent Payment Transactions in db
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="userLinkID"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public async virtual Task<List<PaymentMasterResponse>> CreatePaymentTransactionAsyncWithoutMasterPayment(PaymentRequest newModel, short? userLinkID, string connectionID)
        {
            //Step 1 - Get available master payments
            //non/refundable credit - search for credits from the customer 
            //(in the case of apply credit this means Ref/NonRef credit)
            //if the case of refund this means refundable amounts applied to order
            //in the case of credit adjustment (unapplied refundable credit)
            //in the case of transfer credit (refundable or non refundable credit - depending on type - and that also matches the order)
            List<PaymentMaster> paymentMasters = null;
            bool isRefundable = true;
            var isPayment = false;
            var isRefundToCredit = false;
            var applicationRequests = newModel.Applications.Where(t => t.OrderID != null).OrderBy(t => t.Amount).ToList();
            if ((newModel.PaymentTransactionType == (int)PaymentTransactionType.Credit_Adjustment)
                || (newModel.PaymentTransactionType == (int)PaymentTransactionType.Refund))
                applicationRequests = newModel.Applications;
            var orderIDs = applicationRequests.Select(t => t.OrderID);
            var filteredMasterPaymentList = ctx.PaymentMaster.AsNoTracking().Include(x => x.PaymentApplications)
                .Where(x => x.BID == BID && x.CompanyID == newModel.CompanyID)
                .OrderBy(y => y.AccountingDT);

            switch (newModel.PaymentTransactionType)
            {
                case (int)PaymentTransactionType.Payment_from_Nonrefundable_Credit:
                case (int)PaymentTransactionType.Payment_from_Refundable_Credit:
                    paymentMasters = filteredMasterPaymentList.Where(t => (t.NonRefBalance > 0 || t.RefBalance > 0)).ToList();
                    isRefundable = true;
                    isPayment = true;
                    break;
                case (int)PaymentTransactionType.Refund_to_Refundable_Credit:
                case (int)PaymentTransactionType.Refund_to_Nonrefundable_Credit:
                    paymentMasters = filteredMasterPaymentList
                        .Where(t => t.PaymentApplications.Any(a => orderIDs.Contains(a.OrderID)))
                        .Where(t => (t.NonRefBalance > 0 || t.RefBalance > 0))
                        .ToList();
                    isRefundable = false;
                    isRefundToCredit = true;
                    break;
                case (int)PaymentTransactionType.Refund:
                    paymentMasters = filteredMasterPaymentList
                        .Where(t => t.PaymentApplications.Any(a => orderIDs.Contains(a.OrderID)))
                        .Where(t => t.RefBalance > 0)
                        .OrderBy(x => x.RefBalance > 0)
                        .ToList();
                    isRefundable = true;
                    isPayment = false;
                    break;
                case (int)PaymentTransactionType.Credit_Adjustment:
                    paymentMasters = filteredMasterPaymentList.Where(x => x.ChangeInNonRefCredit > 0 && x.PaymentTransactionType == (int)PaymentTransactionType.Credit_Gift).ToList();
                    isRefundable = false;
                    isPayment = false;
                    break;
            }
            paymentMasters = paymentMasters.OrderByDescending(x => (x.RefBalance > 0)).ThenBy(y => y.AccountingDT).ToList();
            // make sure we have enough balance before procceeding 
            if (!HasEnoughMasterBalance(applicationRequests, paymentMasters, isRefundable, isPayment, isRefundToCredit))
            {
                ServiceModelState.AddError("Applications", "There is not enough balance to complete the application request");
                return null;
            }

            var needsNullableApplicationTransactionType = new List<PaymentTransactionType>()
            {
                PaymentTransactionType.Payment_from_Refundable_Credit,
                PaymentTransactionType.Payment_from_Nonrefundable_Credit,
                PaymentTransactionType.Refund_to_Nonrefundable_Credit,
                PaymentTransactionType.Refund_to_Refundable_Credit
            };
            // A dictionary of MasterID and PaymentRequests to group Applications together
            Dictionary<int, List<PaymentApplicationRequest>> paymentRequestsToProcess = new Dictionary<int, List<PaymentApplicationRequest>>();
            foreach (var master in paymentMasters)
            {
                paymentRequestsToProcess.Add(master.ID, new List<PaymentApplicationRequest>() { });
            }

            //go through applications to find  the relevant payments
            foreach (var applicationRequest in applicationRequests)
            {
                if (!ServiceModelState.IsValid) break;
                var amountToLookFor = Math.Abs(applicationRequest.Amount);
                var originalAmountToLookFor = amountToLookFor;
                while (amountToLookFor > 0)
                {

                    //loop through payment masters till you get the required amount
                    foreach (var payment in paymentMasters)
                    {
                        // break the loop when the amount is fulfilled
                        if (amountToLookFor <= 0) break;

                        var amountOnThisMaster = isPayment ? payment.ChangeInRefCredit ?? 0m + payment.ChangeInNonRefCredit ?? 0m
                            : isRefundToCredit ? payment.PaymentApplications.Where(app => app.OrderID == applicationRequest.OrderID).Sum(x => x.RefBalance) + (decimal)payment.PaymentApplications.Where(app => app.OrderID == applicationRequest.OrderID).Sum(x => x.NonRefBalance ?? 0m)
                            : isRefundable ? payment.PaymentApplications.Where(app => app.OrderID == applicationRequest.OrderID).Sum(x => x.RefBalance)
                            : (decimal)payment.PaymentApplications.Where(app => app.OrderID == applicationRequest.OrderID).Sum(x => x.NonRefBalance ?? 0m);
                        // skip 0 balance masters
                        if (amountOnThisMaster <= 0) continue;

                        //clone source transaction
                        var newPaymentAmount = Math.Min(amountToLookFor, amountOnThisMaster);
                        newPaymentAmount = isPayment ? newPaymentAmount : -newPaymentAmount;

                        paymentRequestsToProcess[payment.ID].Add(new PaymentApplicationRequest()
                        {
                            OrderID = applicationRequest.OrderID,
                            Amount = newPaymentAmount
                        });

                        // partially apply adjustments to master - masters were loaded as no tracking so this should not adjust the db value
                        if (isPayment)
                        {
                            if ((payment.ChangeInRefCredit ?? 0m) > 0) payment.ChangeInRefCredit -= Math.Abs(newPaymentAmount);
                            else payment.ChangeInNonRefCredit -= Math.Abs(newPaymentAmount);
                        }
                        else
                        {
                            if (isRefundable)
                            {
                                payment.RefBalance -= Math.Abs(newPaymentAmount);
                                //adjust applications
                                var tmpAmount = Math.Abs(newPaymentAmount);
                                foreach (var application in payment.PaymentApplications.Where(app => app.OrderID == applicationRequest.OrderID))
                                {
                                    if (tmpAmount <= 0) continue;
                                    var applicationAmountToAdjust = Math.Max(tmpAmount, application.RefBalance);
                                    application.RefBalance -= Math.Abs(applicationAmountToAdjust);
                                    tmpAmount -= applicationAmountToAdjust;
                                }
                            }
                            else
                            {
                                payment.NonRefBalance -= Math.Abs(newPaymentAmount);
                                //adjust applications
                                var tmpAmount = Math.Abs(newPaymentAmount);
                                foreach (var application in payment.PaymentApplications.Where(app => app.OrderID == applicationRequest.OrderID))
                                {
                                    if (tmpAmount <= 0) continue;
                                    var applicationAmountToAdjust = Math.Min(tmpAmount, application.NonRefBalance ?? 0m);
                                    application.NonRefBalance -= Math.Abs(applicationAmountToAdjust);
                                    tmpAmount -= applicationAmountToAdjust;
                                }
                            }
                        }
                        // decrement the lookup amount
                        amountToLookFor -= Math.Abs(newPaymentAmount);
                    }
                    if (amountToLookFor == originalAmountToLookFor)
                    {
                        //break out of while loop because we can't find amount
                        ServiceModelState.AddError("Applications", "Appropriate Master could not be found");
                        return null;
                    }
                }
            }
            List<PaymentMasterResponse> returnMasterPaymentResponse = new List<PaymentMasterResponse>();
            //run through all the master payments and create the transactions neccessary
            foreach (KeyValuePair<int, List<PaymentApplicationRequest>> requestGroup in paymentRequestsToProcess)
            {
                if (requestGroup.Value.Count() == 0) continue;
                var master = paymentMasters.First(t => t.ID == requestGroup.Key);
                // generate the deduction application for company
                var requestAmount = requestGroup.Value.Sum(t => t.Amount);
                if ((newModel.PaymentTransactionType != (int)PaymentTransactionType.Credit_Adjustment)
                    && (newModel.PaymentTransactionType != (int)PaymentTransactionType.Refund))
                    requestGroup.Value.Add(new PaymentApplicationRequest()
                    {
                        OrderID = null,
                        Amount = -requestAmount
                    });
                // construct the request
                var request = new SubPaymentRequest()
                {
                    PaymentTransactionType = newModel.PaymentTransactionType,
                    masterPaymentId = requestGroup.Key,
                    LocationID = newModel.LocationID,
                    ReceivedLocationID = newModel.ReceivedLocationID,
                    CompanyID = newModel.CompanyID,
                    PaymentMethodID = newModel.PaymentMethodID,
                    PaymentType = newModel.PaymentType,
                    Amount = requestAmount,
                    CurrencyType = newModel.CurrencyType,
                    DisplayNumber = newModel.DisplayNumber,
                    Notes = newModel.Notes,
                    Applications = requestGroup.Value
                };
                //make sure transactiontype is correct
                if (isPayment)
                {
                    if (master.PaymentTransactionType == (int)PaymentTransactionType.Payment)
                    {
                        request.PaymentMethodID = (int)PaymentMethodType.RefundableCredit;
                        request.PaymentTransactionType = (int)PaymentTransactionType.Payment_from_Refundable_Credit;
                    }
                    else
                    {
                        request.PaymentMethodID = (int)PaymentMethodType.NonRefundableCredit;
                        request.PaymentTransactionType = (int)PaymentTransactionType.Payment_from_Nonrefundable_Credit;
                    }
                }
                if (isRefundToCredit)
                {
                    if (master.PaymentTransactionType == (int)PaymentTransactionType.Payment)
                    {
                        request.PaymentMethodID = (int)PaymentMethodType.RefundableCredit;
                        request.PaymentTransactionType = (int)PaymentTransactionType.Refund_to_Refundable_Credit;
                    }
                    else
                    {
                        request.PaymentMethodID = (int)PaymentMethodType.NonRefundableCredit;
                        request.PaymentTransactionType = (int)PaymentTransactionType.Refund_to_Nonrefundable_Credit;
                    }
                }
                // procceed to the actual transaction
                var paymentResponse = await CreatePaymentTransactionAsync(request, request.masterPaymentId, userLinkID, connectionID, false);
                if (paymentResponse == null)
                {
                    ServiceModelState.AddError("Applications", "Sub-Transaction creation failure");
                    return null;
                }
                returnMasterPaymentResponse.AddRange(paymentResponse.MasterPayments);
            }
            //remove duplicates
            var adjustedMasterIds = returnMasterPaymentResponse.Select(x => x.ID).ToList();
            List<PaymentMasterResponse> returnMasterPayments = new List<PaymentMasterResponse>();
            adjustedMasterIds.ForEach(id => returnMasterPayments.Add(
                new PaymentMasterResponse()
                {
                    ID = id,
                    Applications = returnMasterPaymentResponse.ToList().Last(x => x.ID == id).Applications
                }));

            return returnMasterPayments;
        }

        /// <summary>
        /// Checks application requests against the masters balance
        /// </summary>
        private bool HasEnoughMasterBalance(List<PaymentApplicationRequest> applicationRequests, List<PaymentMaster> masters, bool isRefundable, bool isApplyCredit, bool isRefundToCredit)
        {
            if (isApplyCredit)
            {
                var sum = masters.Sum(t => (t.ChangeInRefCredit ?? 0m) + (t.ChangeInNonRefCredit ?? 0m));
                return sum >= applicationRequests.Sum(t => Math.Abs(t.Amount));
            }
            if (isRefundToCredit)
            {
                var sum = masters.Sum(t => (t.RefBalance) + (t.NonRefBalance ?? 0m));
                return sum >= applicationRequests.Sum(t => Math.Abs(t.Amount));
            }
            else
            {
                if (isRefundable)
                {
                    return masters.Sum(t => t.RefBalance) >= applicationRequests.Sum(t => Math.Abs(t.Amount));
                }
                return masters.Sum(t => t.NonRefBalance) >= applicationRequests.Sum(t => Math.Abs(t.Amount));
            }
        }

        private SubPaymentRequest ClonePaymentRequest(PaymentRequest source, PaymentApplicationRequest applicationRequest)
        {
            SubPaymentRequest request = new SubPaymentRequest()
            {
                PaymentTransactionType = source.PaymentTransactionType,
                LocationID = source.LocationID,
                ReceivedLocationID = source.ReceivedLocationID,
                CompanyID = source.CompanyID,
                PaymentMethodID = source.PaymentMethodID,
                PaymentType = source.PaymentType,
                Amount = source.Amount,
                CurrencyType = source.CurrencyType,
                DisplayNumber = source.DisplayNumber,
                Notes = source.Notes,
                Applications = new List<PaymentApplicationRequest>()
                {
                    applicationRequest
                }
            };
            return request;
        }


    }

    /// <summary>
    /// 
    /// </summary>
    public class SubPaymentRequest : PaymentRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public int masterPaymentId;
    }
}
