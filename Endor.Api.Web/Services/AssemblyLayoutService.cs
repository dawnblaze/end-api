﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// assembly layout service
    /// </summary>
    public class AssemblyLayoutService : AtomCRUDService<AssemblyLayout, short>
    {
        /// <summary>
        /// Constructs a AssemblyLayoutService service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public AssemblyLayoutService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return new string[] { };
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">CustomFieldLayoutDefinition ID</param>
        /// <returns></returns>
        public override IQueryable<AssemblyLayout> WherePrimary(IQueryable<AssemblyLayout> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<AssemblyLayout, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<AssemblyLayout, short>[] 
            {
                CreateChildAssociation<AssemblyElementService, AssemblyElement, int>(x => x.Elements)
            };
        }

        internal override void DoBeforeValidate(AssemblyLayout newModel)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task<AssemblyLayout> GetOldModelAsync(AssemblyLayout newModel)
        {
            var layout = await WherePrimary(ctx.AssemblyLayout.Include(x => x.Elements).AsNoTracking(), newModel.ID).FirstOrDefaultAsync();

            MapChildTree(layout);

            return layout;
        }

        internal static void MapChildTree(AssemblyLayout layout)
        {
            if (layout?.Elements?.Any() ?? false)
            {
                var childHash = layout.Elements.ToLookup(x => x.ParentID);

                foreach (AssemblyElement element in layout.Elements)
                {
                    element.Elements = childHash[element.ID].OrderBy(y => y.Row).ToList();
                }
                layout.Elements = layout.Elements.Where(y => y.ParentID == null).OrderBy(y => y.Row).ToList();
            }
        }
    }
}
