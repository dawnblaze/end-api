﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Endor.Tenant;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Generic service for base Atom Model
    /// </summary>
    public abstract class AtomGenericService<M, I> : BaseGenericService
        where M : class
        where I : struct, IConvertible
    {
        /// <summary>
        /// Constructor for generic Atom model
        /// </summary>
        public AtomGenericService(ApiContext context, IMigrationHelper migrationHelper)
            : base(context, migrationHelper)
        {

        }

        /// <summary>
        /// Get valid includes for model
        /// </summary>
        public abstract string[] GetIncludes();

        /// <summary>
        /// Get objects that match the given search funciton
        /// </summary>
        public Task<List<M>> GetWhere(Expression<Func<M, bool>> predicate)
        {
            return ctx.Set<M>().IncludeAll(GetIncludes()).Where(predicate).ToListAsync();
        }

        /// <summary>
        /// Create Service
        /// </summary>
        public static Lazy<S> CreateService<S>(ApiContext ctx, IMigrationHelper migrationHelper)
            where S : BaseGenericService
        {
            return new Lazy<S>(() => Activator.CreateInstance(typeof(S), ctx, migrationHelper) as S);
        }

        /// <summary>
        /// Create Service
        /// </summary>
        public static Lazy<S> CreateService<S>(ApiContext ctx, RemoteLogger logger, Func<short> bidFunc, ITaskQueuer taskQueuer, IRTMPushClient rtmClient, ITenantDataCache cache, IMigrationHelper migrationHelper)
            where S : BaseGenericService
        {
            return new Lazy<S>(() => Activator.CreateInstance(typeof(S), ctx, logger, bidFunc(), taskQueuer, rtmClient, cache, migrationHelper) as S);
        }
    }
}
