using Endor.EF;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Endor.Tasks;
using Endor.Api.Web.Classes;
using Endor.Tenant;
using Endor.Logging.Client;
using Endor.RTM;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Service that handles CRUD ops on SurchargeDef data
    /// </summary>
    public class SurchargeDefService : AtomCRUDService<SurchargeDef, short>, ISimpleListableViewService<SimpleSurchargeDef, short>
    {
        /// <summary>
        /// Property to get  SimpleSurchargeDef from ApiContext
        /// </summary>
        public DbSet<SimpleSurchargeDef> SimpleListSet => ctx.SimpleSurchargeDef;

        /// <summary>
        /// Constructs a new SurchargeDefService with a number of injected parameters
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public SurchargeDefService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<SurchargeDef, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<SurchargeDef, short>[0];
        }

        internal override void DoBeforeValidate(SurchargeDef newModel)
        {
        }

        /// <summary>
        /// Implements the primary lookup filter
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<SurchargeDef> WherePrimary(IQueryable<SurchargeDef> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        internal async Task<List<SurchargeDef>> GetWithFiltersAsync(SurchargeDefFilter filters)
        {
            if (filters?.HasFilters ?? false)
                return await this.GetWhere(filters.WherePredicates());

            return await this.GetAsync(null);
        }

        internal async Task<ServiceResponse> MoveBefore(short id, short targetID)
        {
            var moverAndTarget = await ctx.SurchargeDef.Where(t => t.BID == this.BID && (t.ID == id || t.ID == targetID)).ToListAsync();
            var mover = moverAndTarget.FirstOrDefault(t => t.ID == id);
            var target = moverAndTarget.FirstOrDefault(t => t.ID == targetID);

            if (mover == null)
                return new ServiceResponse() { Success = false, IsNotFound = true, Message = "Entity does not exists or cannot be found" };

            if (target == null)
                return new ServiceResponse() { Success = false, IsNotFound = true, Message = "Target does not exist or cannot be found" };

            if (target.ID == mover.ID)
                return new ServiceResponse() { Success = false, HasError = true, Message = "Invalid Target, targets cannot be the same record" };

            var shifterQuery = ctx.SurchargeDef.Where(t => t.BID == this.BID);

            List<SurchargeDef> shifters = null;

            if (mover.SortIndex > target.SortIndex) // moving up
            {
                shifters = await shifterQuery.Where(t => t.SortIndex > target.SortIndex && t.SortIndex < mover.SortIndex).ToListAsync();
                shifters.ForEach(t => t.SortIndex += 1);
                mover.SortIndex = target.SortIndex;
                target.SortIndex++;
            }
            else // moving down
            {
                shifters = await shifterQuery.Where(t => t.SortIndex < target.SortIndex && t.SortIndex > mover.SortIndex).ToListAsync();
                shifters.ForEach(t => t.SortIndex -= 1);
                mover.SortIndex = (short)(target.SortIndex - 1);
            }

            try
            {
                await ctx.SaveChangesAsync();

                return new ServiceResponse() { Success = true };
            }
            catch (Exception e)
            {
                return new ServiceResponse() { Success = false, HasError = true, Message = e.ToString() };
            }

        }

        internal async Task<ServiceResponse> MoveAfter(short id, short targetID)
        {
            var moverAndTarget = await ctx.SurchargeDef.Where(t => t.BID == this.BID && (t.ID == id || t.ID == targetID)).ToListAsync();
            var mover = moverAndTarget.FirstOrDefault(t => t.ID == id);
            var target = moverAndTarget.FirstOrDefault(t => t.ID == targetID);

            if (mover == null)
                return new ServiceResponse() { Success = false, IsNotFound = true, Message = "Entity does not exists or cannot be found" };

            if (target == null)
                return new ServiceResponse() { Success = false, IsNotFound = true, Message = "Target does not exist or cannot be found" };

            if (target.ID == mover.ID)
                return new ServiceResponse() { Success = false, HasError = true, Message = "Invalid Target, targets cannot be the same record" };

            var shifterQuery = ctx.SurchargeDef.Where(t => t.BID == this.BID);

            List<SurchargeDef> shifters = null;

            if (mover.SortIndex > target.SortIndex) // moving up
            {
                shifters = await shifterQuery.Where(t => t.SortIndex > target.SortIndex && t.SortIndex < mover.SortIndex).ToListAsync();
                shifters.ForEach(t => t.SortIndex += 1);
                mover.SortIndex = (short)(target.SortIndex + 1);

            }
            else // moving down
            {
                shifters = await shifterQuery.Where(t => t.SortIndex < target.SortIndex && t.SortIndex > mover.SortIndex).ToListAsync();
                shifters.ForEach(t => t.SortIndex -= 1);
                mover.SortIndex = (short)(target.SortIndex);
                target.SortIndex--;
            }

            try
            {
                await ctx.SaveChangesAsync();

                return new ServiceResponse() { Success = true };
            }
            catch (Exception e)
            {
                return new ServiceResponse() { Success = false, HasError = true, Message = e.ToString() };
            }
        }

        /// <summary>
        /// Business logic on add. Base class handles ID and ClassTypeID 
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(SurchargeDef newModel, Guid? tempGuid = null)
        {
            if (ctx.SurchargeDef.Any())
            {
                newModel.SortIndex = (short)(ctx.SurchargeDef.Max(x => x.SortIndex) + 1);
            }
            else
                newModel.SortIndex = 0;
            await this.DoBeforeCreateAsync(newModel, tempGuid);
        }
    }
}