﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using System;
using System.Linq;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// assembly Table service
    /// </summary>
    public class MachineInstanceService : AtomCRUDService<MachineInstance, int>, IDoBeforeCreateUpdateWithParent<MachineInstance, MachineData>
    {
        /// <summary>
        /// Constructs a AssemblyTableService service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public MachineInstanceService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return new string[] { };
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">AssemblyTable ID</param>
        /// <returns></returns>
        public override IQueryable<MachineInstance> WherePrimary(IQueryable<MachineInstance> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<MachineInstance, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<MachineInstance, int>[]{
                // CreateChildAssociation<AssemblyService, AssemblyTable, int>(e => e.AssemblyID),
            };
        }

        internal override void DoBeforeValidate(MachineInstance newModel)
        {

        }

        /// <summary>
        /// set machine ID and BID of child
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public void DoBeforeCreateWithParent(MachineInstance child, MachineData parent)
        {
            child.MachineID = parent.ID;
            child.BID = parent.BID;
        }

        /// <summary>
        /// set machine ID and BID of child
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        public void DoBeforeUpdateWithParent(MachineInstance child, MachineData oldParent, MachineData newParent)
        {
            child.MachineID = newParent.ID;
            child.BID = newParent.BID;
        }

    }
}
