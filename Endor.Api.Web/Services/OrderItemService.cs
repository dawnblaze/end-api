﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Request;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// OrderItem Service Layer
    /// </summary>
    public class OrderItemService : AtomCRUDService<OrderItemData, int>, ISimpleListableViewService<SimpleOrderItemData, int>, IDoBeforeCreateUpdateWithParent<OrderItemData, OrderData>, IDoBeforeCreateUpdateWithParent<OrderItemData, CreditMemoData>
    {
        /// <summary>
        /// Simple List
        /// </summary>
        public DbSet<SimpleOrderItemData> SimpleListSet => ctx.SimpleOrderItemData;

        /// <summary>
        /// OrderItem Service Layer
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper"></param>
        public OrderItemService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default children to include
        /// </summary>
        public override string[] IncludeDefaults => new string[]{};
        //public override string[] IncludeDefaults => new string[] { nameof(OrderItemData.Components) };

        /// <summary>
        /// Returns the includes
        /// </summary>
        /// <param name="includes">Children to include</param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Sets the ModifiedDT for an Entity
        /// </summary>
        /// <param name="newModel">Entity to set the ModifiedDT for</param>
        protected override void SetModifiedDT(OrderItemData newModel)
        {
            if(newModel.ID==0)
            {
                /*
                fix for "Cannot insert an explicit value into a GENERATED ALWAYS column in table 'Dev.Endor.Business.DB1.dbo.Order.Item.Data'. 
                Use INSERT with a column list to exclude the GENERATED ALWAYS column, or insert a DEFAULT into GENERATED ALWAYS column."
                */
                newModel.ModifiedDT = default(DateTime);
            }
            ctx.Entry(newModel).Property(x => x.ModifiedDT).IsModified = false;
        }

        /// <summary>
        /// Occurs before the create hits the database
        /// </summary>
        /// <param name="newModel">New Tax Group to be created</param>
        /// <param name="tempGuid">temporary guid to aid in saving related objects</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(OrderItemData newModel, Guid? tempGuid = null)
        {
            if (newModel.TransactionType == 0)
            {
                TransactionHeaderData existingParent = ctx.TransactionHeaderData.FirstOrDefault(t => t.BID == newModel.BID && t.ID == newModel.OrderID);
                if (existingParent != null)
                {
                    newModel.TransactionType = existingParent.TransactionType;
                }
                else
                {
                    switch (newModel.OrderStatusID)
                    {
                        case OrderOrderStatus.EstimateApproved:
                        case OrderOrderStatus.EstimateLost:
                        case OrderOrderStatus.EstimatePending:
                        case OrderOrderStatus.EstimateVoided:
                            newModel.TransactionType = (byte)OrderTransactionType.Estimate;
                            break;
                        case OrderOrderStatus.DestinationCompleted:
                        case OrderOrderStatus.DestinationPending:
                        case OrderOrderStatus.DestinationReady:
                            newModel.TransactionType = (byte)OrderTransactionType.Destination;
                            break;
                        case OrderOrderStatus.POApproved:
                        case OrderOrderStatus.POOrdered:
                        case OrderOrderStatus.POReceived:
                        case OrderOrderStatus.PORequested:
                        case OrderOrderStatus.POVoided:
                            newModel.TransactionType = (byte)OrderTransactionType.PurchaseOrder;
                            break;
                        case OrderOrderStatus.OrderBuilt:
                        case OrderOrderStatus.OrderClosed:
                        case OrderOrderStatus.OrderInvoiced:
                        case OrderOrderStatus.OrderInvoicing:
                        case OrderOrderStatus.OrderPreWIP:
                        case OrderOrderStatus.OrderVoided:
                        case OrderOrderStatus.OrderWIP:
                        default:
                            newModel.TransactionType = (byte)OrderTransactionType.Order;
                            break;
                    }
                }
            }

            if ((newModel?.Surcharges?.Count ?? 0) == 0)
            {
                newModel.Surcharges = this.ctx.SurchargeDef
                                        .AsNoTracking()
                                        .WherePrimary(BID, sd => sd.IsActive && sd.AppliedByDefault)
                                        .Select(sd =>
                                            new OrderItemSurcharge()
                                            {
                                                BID = this.BID,
                                                ClassTypeID = (int)ClassType.OrderItemSurcharge,
                                                DefaultFixedFee = sd.DefaultFixedFee,
                                                DefaultPerUnitFee = sd.DefaultPerUnitFee,
                                                PricePreTax = newModel.Quantity * sd.DefaultPerUnitFee ?? 0 + sd.DefaultFixedFee ?? 0,
                                                IncomeAccountID = sd.IncomeAccountID,
                                                Name = sd.Name,
                                                TaxCodeID = sd.TaxCodeID,
                                                SurchargeDefID = sd.ID
                                            }
                                        ).ToList();
            }

            await base.DoBeforeCreateAsync(newModel, tempGuid);

            if (newModel.EmployeeRoles != null)
            {
                foreach (var eRole in newModel.EmployeeRoles)
                {
                    eRole.OrderItemID = newModel.ID;
                }
            }

            if (newModel.Dates != null)
            {
                foreach (var keydate in newModel.Dates)
                {
                    keydate.OrderID = newModel.OrderID;
                    keydate.OrderItemID = newModel.ID;
                }
            }

            if (newModel.Surcharges != null)
            {
                foreach (var surcharge in newModel.Surcharges)
                {
                    surcharge.OrderItemID = newModel.ID;
                }
            }

            if (newModel.Notes != null)
            {
                foreach (var note in newModel.Notes)
                {
                    note.CreatedDT = DateTime.UtcNow;
                    note.OrderID = newModel.OrderID;
                    note.OrderItemID = newModel.ID;
                }
            }

            if(newModel.Components.Any()) SetOrderItemComponentName(newModel.Components, newModel.FromQuickItemID);

            TransactionServiceHelper.SetComponentsIDs(newModel.Components, newModel.OrderID, newModel.ID);

            OrderTaxItemAssessmentService orderTaxItemAssessmentService = new OrderTaxItemAssessmentService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);

            if (newModel.Components != null)
                foreach (var item in newModel.Components)
                {
                    await OrderItemComponentService.AddOrderTaxItemAssessmentForComponent(orderTaxItemAssessmentService, item);
                }
            if (newModel.Surcharges != null)
                foreach (var item in newModel.Surcharges)
                {
                    await OrderItemSurchargeService.AddOrderTaxItemAssessmentForSurcharge(orderTaxItemAssessmentService, newModel.OrderID, item);
                }
        }

        /// <summary>
        /// Set order item component name
        /// </summary>
        /// <param name="components"></param>
        /// <param name="fromQuickItemID"></param>
        private void SetOrderItemComponentName(ICollection<OrderItemComponent> components, int? fromQuickItemID)
        {
            foreach (OrderItemComponent component in components)
            {
                if (string.IsNullOrWhiteSpace(component.Name))
                {
                    QuickItemData quickItemData = ctx.QuickItemData.FirstOrDefault(q => q.BID == BID && q.ID == fromQuickItemID);
                    if (quickItemData != null && !string.IsNullOrWhiteSpace(quickItemData.DataJSON))
                    {
                        OrderItemComponentCollection componentCollection = JsonConvert.DeserializeObject<OrderItemComponentCollection>(quickItemData.DataJSON);
                        if (componentCollection.Items.Any()) component.Name = componentCollection.Items.FirstOrDefault().Name;
                    }
                }
            }
        }

        /// <summary>
        /// Do Before Clone
        /// </summary>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(OrderItemData clone)
        {
            clone.ID = 0;
            clone.Name += " (Clone)";
            OrderItemData lastOrderItem = await this.ctx.OrderItemData
                .Where(x => x.BID == this.BID && x.OrderID == clone.OrderID).OrderByDescending(x => x.ItemNumber)
                .AsNoTracking().FirstOrDefaultAsync();

            if (lastOrderItem != null)
            {
                clone.ItemNumber = lastOrderItem.ItemNumber;
                clone.ItemNumber += 1;
            }

            foreach (OrderNote note in clone.Notes ?? new List<OrderNote>())
            {
                note.ID = await RequestIDAsync(BID, (int)ClassType.OrderNote);
            }

            this.SetModifiedDT(clone);
            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Do After Clone
        /// </summary>
        /// <param name="clonedFromID"></param>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(int clonedFromID, OrderItemData clone)
        {
            await CloneOrderItemComponentData(clonedFromID, clone.ID);

            //Always Copy Data and Reports Buckets

            //Copy Documents Bucket
            DocumentManager client = base.GetDocumentManager(clonedFromID, ClassType.OrderItem, BucketRequest.Data);
            await client.CloneAllBucketBlobsAsync(clone.ID, Bucket.Data, includeSpecialFolderBlobName: true);

            //Copy Reports Bucket
            client = base.GetDocumentManager(clonedFromID, ClassType.OrderItem, BucketRequest.Reports);
            await client.CloneAllBucketBlobsAsync(clone.ID, Bucket.Reports, includeSpecialFolderBlobName: true);

            await taskQueuer.CheckOrderStatus(clone.BID, clone.OrderID, (int)clone.OrderStatusID);
            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        private async Task<bool> CloneOrderItemComponentData(
            int oldOrderID,
            int newOrderID)
        {
            OrderItemComponentService oicservice = new OrderItemComponentService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
            List<OrderItemComponent> oicList = new List<OrderItemComponent>();

            oicList = await this.ctx.OrderItemComponent
                                        .AsNoTracking()
                                        .Where(com => com.BID == this.BID && com.OrderItem.ID == oldOrderID)
                                        .ToListAsync();

            if (oicList != null)
            {
                foreach (OrderItemComponent oic in oicList)
                {
                    OrderItemComponent oicClone = await oicservice.GetClone(oic.ID);
                    oicClone.ID = await RequestIDAsync(BID, (int)ClassType.OrderItemComponent);
                    oicClone.OrderItemID = newOrderID;
                    oicClone.ModifiedDT = default(DateTime);
                    OrderItemComponent oicCloneAfterSave = await oicservice.CreateAsync(oicClone, null);
                }

            }

            return await ctx.SaveChangesAsync() > 0;
        }

        /// <summary>
        /// do after add
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(OrderItemData newModel, Guid? tempGuid = null)
        {
            var siblingItems = await this.ctx.OrderItemData.Where(x => x.BID == this.BID && x.OrderID == newModel.OrderID && x.ID != newModel.ID).ToListAsync();
            var nextItems = siblingItems.Where(x => x.ItemNumber >= newModel.ItemNumber);
            var previousItems = siblingItems.Where(x => x.ItemNumber < newModel.ItemNumber);
            
            OrderItemData closestNextItem = null;
            OrderItemData closestPreviousItem = null;

            if(nextItems.Count() > 0)
                closestNextItem = siblingItems.FirstOrDefault(x=>x.ItemNumber == nextItems.Min(prev=>prev.ItemNumber));

            if(previousItems.Count() > 0)
                closestPreviousItem = siblingItems.FirstOrDefault(x=>x.ItemNumber == previousItems.Max(prev=>prev.ItemNumber));
            
            if(closestNextItem != null)
                await this.MoveBefore(newModel.ID, closestNextItem.ID);
            else if(closestPreviousItem != null)
                await this.MoveAfter(newModel.ID, closestPreviousItem.ID);

            if (newModel.FromQuickItemID.HasValue)
            {
                var storage = new StorageContext(this.BID, BucketRequest.Documents, new DMID()
                {
                    ctid = (int)ClassType.QuickItem,
                    id = newModel.FromQuickItemID.Value
                });
                var docMan = new DocumentManager(this.cache, storage);
                await docMan.CopyExistingDocsAsync($"/{newModel.FromQuickItemLineItemNumber}/", "", newModel.ID, StorageBin.Permanent, (int?)ClassType.OrderItem);
            }

            await taskQueuer.CheckOrderStatus(newModel.BID, newModel.OrderID, (int)newModel.OrderStatusID);
            await base.DoAfterAddAsync(newModel, tempGuid);
            newModel.Components = newModel.Components?
                                    .Where(x => x.ParentComponentID == null)
                                    .ToList();
        }

        /// <summary>
        /// Do After Update
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="updatedModel"></param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(OrderItemData oldModel, OrderItemData updatedModel)
        {
            if(updatedModel.Dates != null){

                // END-8080 Price total not being saved on line item override of unit price.
                SetCommonModelProperties(updatedModel);

                foreach (OrderKeyDate keyDate in updatedModel.Dates)
                {
                    keyDate.BID = this.BID;
                    keyDate.OrderID = updatedModel.OrderID;
                    keyDate.OrderItemID = updatedModel.ID;

                    //END-4853 do not create multiple keydate objects with the same type, reuse them
                    int? matchingTypeID = oldModel.Dates?.FirstOrDefault(x => x.KeyDateType == keyDate.KeyDateType)?.ID;
                    if (matchingTypeID.HasValue)
                    {
                        keyDate.ID = matchingTypeID.Value;
                        ctx.Entry(keyDate).State = EntityState.Modified;
                    }
                }
            }
            //END-5611, putting base.DoBeforeUpdateAsync to fix bug where KeyDates with same BID,ID are being tracked twice
            await base.DoBeforeUpdateAsync(oldModel, updatedModel);
        }

        /// <summary>
        /// Business logic for after having updated a model
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel">Model object to update</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(OrderItemData oldModel, OrderItemData newModel, string connectionID)
        {
            if (oldModel.ItemStatusID != newModel.ItemStatusID)
                await taskQueuer.CheckOrderStatus(newModel.BID, newModel.OrderID, (int)newModel.OrderStatusID);

            await base.DoAfterUpdateAsync(oldModel, newModel, connectionID);
        }

        /// <summary>
        /// Business logic to perform after deleting a model
        /// </summary>
        /// <param name="model">Deleted model</param>
        /// <returns></returns>
        protected override async Task DoAfterDeleteAsync(OrderItemData model)
        {
            await taskQueuer.CheckOrderStatus(model.BID, model.OrderID, (int)model.OrderStatusID);
            await base.DoAfterDeleteAsync(model);
        }

        /// <summary>
        /// Get Old Model for Do After Update
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task<OrderItemData> GetOldModelAsync(OrderItemData newModel)
        {
            OrderItemData oldModel = await WherePrimary(ctx.Set<OrderItemData>().Include(x => x.Components).Include(x => x.Dates)
                .Include(x => x.Surcharges)
                .AsNoTracking(), newModel.ID).FirstOrDefaultAsync();

            OrderItemComponentService itemComponentService = new OrderItemComponentService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
            OrderItemSurchargeService itemSurchargeService = new OrderItemSurchargeService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
            MapChildTree(oldModel, itemComponentService, itemSurchargeService);

            return oldModel;
        }

        /// <summary>
        /// Do Before Delete
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(OrderItemData model)
        {
            this.ctx.OrderItemTaxAssessment.RemoveRange(this.ctx.OrderItemTaxAssessment.Where(t => t.BID == BID && t.OrderItemID == model.ID));
            
            //Load components
            model.Components = await ctx.Set<OrderItemComponent>().WherePrimary(BID, c => c.OrderID == model.OrderID && c.OrderItemID == model.ID).ToListAsync();
            foreach (var c in model.Components ?? new List<OrderItemComponent>())
            {
                ctx.OrderItemComponent.Remove(c);
            }
            //Load surcharge
            model.Surcharges = await ctx.Set<OrderItemSurcharge>().WherePrimary(BID, c => c.OrderItemID == model.ID).ToListAsync();
            foreach (var c in model.Surcharges ?? new List<OrderItemSurcharge>())
            {
                ctx.OrderItemSurcharge.Remove(c);
            }
            //Load EmployeeRoles
            model.EmployeeRoles = await ctx.OrderEmployeeRole.WherePrimary(BID, erole => erole.OrderItemID == model.ID).ToListAsync();
            ctx.OrderEmployeeRole.RemoveRange(model.EmployeeRoles);

            //Load keydates
            model.Dates = await ctx.OrderKeyDate.WherePrimary(BID, kd => kd.OrderItemID == model.ID).ToListAsync();
            ctx.OrderKeyDate.RemoveRange(model.Dates);

            await base.DoBeforeDeleteAsync(model);
        }

        /// <summary>
        /// GetChildAssociations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<OrderItemData, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<OrderItemData, int>[] {
                CreateChildAssociation<OrderItemComponentService, OrderItemComponent, int>((y) => y.Components),
                CreateChildAssociation<OrderEmployeeRoleService, OrderEmployeeRole, int>((order)=> order.EmployeeRoles),
                CreateChildAssociation<OrderItemSurchargeService, OrderItemSurcharge, int>((orderItem)=> orderItem.Surcharges),
                CreateChildAssociation<OrderKeyDateService, OrderKeyDate, int>((orderItem)=> orderItem.Dates),
                CreateChildAssociation<OrderItemNoteService, OrderNote, int>((orderItem)=> orderItem.Notes)
            };
        }

        internal override void DoBeforeValidate(OrderItemData newModel)
        {
        }

        private List<RefreshEntity> DoGetRefreshMessages(int orderItemID)
        {
            return new List<RefreshEntity>()
                        {
                new RefreshEntity()
                {
                    BID = BID,
                    ClasstypeID = (int)ClassType.OrderItem,
                    ID = orderItemID,
                    DateTime = DateTime.UtcNow,
                    RefreshMessageType = RefreshMessageType.Change,
                }
            };
        }

        /// <summary>
        /// Gets a list of OrderItem
        /// </summary>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        public async Task<List<OrderItemData>> GetOrderItemsWithIncludes(OrderItemIncludes includes = null)
        {
            return await ctx.Set<OrderItemData>().IncludeAll(GetIncludes(includes))
                .Where(o => o.BID == this.BID).ToListAsync();
        }

        /// <summary>
        /// Gets a list of Order Items associated with the OrderID
        /// </summary>
        /// <param name="orderID">OrderID</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        public async Task<List<OrderItemData>> GetOrderItemsForOrder(int orderID, OrderItemIncludes includes = null)
        {
            var order = await ctx.Set<OrderData>().Where(o => o.BID == this.BID && o.ID == orderID).FirstOrDefaultAsync();
            if (order == null)
                return null;

            return await ctx.Set<OrderItemData>().IncludeAll(GetIncludes(includes))
                .Where(o => o.BID == this.BID && o.OrderID == orderID).ToListAsync();
        }

        /// <summary>
        /// Gets a single OrderItem by ID and Order Version
        /// </summary>
        /// <param name="ID">OrderItem's ID</param>
        /// <param name="version">OrderItem's Order's Version</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        public async Task<OrderItemData> GetForVersion(int ID, int version, OrderItemIncludes includes = null)
        {
            return await ctx.Set<OrderItemData>().IncludeAll(GetIncludes(includes))
                .Where(o => o.BID == this.BID && o.ID == ID).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Gets a SimpleList of OrderItems for the given Order by ID
        /// </summary>
        /// <param name="orderID">Order's ID</param>
        /// <returns></returns>
        public async Task<List<SimpleOrderItemData>> GetSimpleOrderItemDataForOrder(int orderID)
        {
            List<SimpleOrderItemData> simpleOrderItems = new List<SimpleOrderItemData>();
            var order = await ctx.Set<OrderData>().Where(o => o.BID == this.BID && o.ID == orderID).FirstOrDefaultAsync();
            if (order == null)
                return null;

            var orderItems = await ctx.Set<OrderItemData>().Where(o => o.BID == this.BID && o.OrderID == orderID).ToListAsync();
            if (orderItems != null && orderItems.Count > 0)
            {
                foreach (var item in orderItems)
                {
                    simpleOrderItems.Add(await ctx.Set<SimpleOrderItemData>()
                        .Where(i => i.BID == this.BID && i.ID == item.ID).FirstOrDefaultAsync());
                }
            }

            return simpleOrderItems;
        }

        private async Task<string> Move(int sourceItemID, int targetItemID, bool moveBefore)
        {
            try
            {
                SqlParameter paramResult = new SqlParameter("@Result", System.Data.SqlDbType.Bit)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                SqlParameter paramFailureReason = new SqlParameter("@FailureReason", System.Data.SqlDbType.VarChar, -1)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                object[] myParams = {
                    new SqlParameter("@BID", BID),
                    new SqlParameter("@SourceItemID", sourceItemID),
                    new SqlParameter("@TargetItemID", targetItemID),
                    new SqlParameter("@MoveBefore", moveBefore),
                    paramResult,
                    paramFailureReason,
                };

                await ctx.Database.ExecuteSqlRawAsync(
@"EXEC dbo.[Order.Item.Action.Move] 
     @BID = @BID
   , @SourceItemID = @SourceItemID
   , @TargetItemID = @TargetItemID
   , @MoveBefore = @MoveBefore
   , @Result = @Result OUTPUT
   , @FailureReason = @FailureReason OUTPUT", parameters: myParams);

                if ((bool)paramResult.Value)
                    return null;

                return (string)paramFailureReason.Value;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        internal async Task<EntityActionChangeResponse> BoardMove(int id, OrderItemData orderItem, BoardMoveRequest boardMove, string connectionID)
        {
            var response = new EntityActionChangeResponse()
            {
                Success = false
            };
            bool doSave = false;
            OrderEmployeeRoleService roleSvc = null;
            switch (boardMove.PropertyName)
            {
                case "ItemSubStatusID":
                case nameof(orderItem.SubStatusID):
                    orderItem.SubStatusID = short.Parse(boardMove.To);
                    orderItem.SubStatusID = orderItem.SubStatusID == 0 ? null : orderItem.SubStatusID;
                    doSave = true;
                    break;
                case "StatusID":
                case nameof(orderItem.ItemStatusID):
                    orderItem.ItemStatusID = short.Parse(boardMove.To);
                    doSave = true;
                    break;
                case nameof(orderItem.OrderStatusID):
                    orderItem.OrderStatusID = (OrderOrderStatus)byte.Parse(boardMove.To);
                    doSave = true;
                    break;
                case "AssignedToID":
                    roleSvc = new OrderEmployeeRoleService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
                    response = await roleSvc.AddOrderItemEmployeeRole(orderItem, 253, short.Parse(boardMove.To), connectionID);//where 253 is roletype of "AssignedTo"
                    doSave = false;
                    break;
                case "DesignerID":
                    roleSvc = new OrderEmployeeRoleService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
                    response = await roleSvc.AddOrderItemEmployeeRole(orderItem, 3, short.Parse(boardMove.To), connectionID);
                    doSave = false;
                    break;                    
                default:
                    string employeeRoleName = boardMove.PropertyName;
                    var role = await this.ctx.EmployeeRole.FirstOrDefaultAsync(x => x.BID == this.BID && x.Name == employeeRoleName);
                    if (role != null)
                    {
                        roleSvc = new OrderEmployeeRoleService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
                        response = await roleSvc.AddOrderItemEmployeeRole(orderItem, role.ID, short.Parse(boardMove.To), connectionID);
                    }
                    else
                    {
                        response = new EntityActionChangeResponse
                        {
                            Message = $"Cannot find property '{boardMove.PropertyName}' to change",
                            Success = false
                        };
                    }
                    break;
            }

            if (doSave)
            {
                await this.UpdateAsync(orderItem, connectionID);
                response.Success = true;

                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(id) });
            }

            return response;
        }

        /// <summary>
        /// Moves the source line item before the target item with in the order
        /// </summary>
        /// <param name="sourceItemID"></param>
        /// <param name="targetItemID"></param>
        /// <returns></returns>
        public async Task<string> MoveBefore(int sourceItemID, int targetItemID)
        {
            return await Move(sourceItemID, targetItemID, true);
        }

        /// <summary>
        /// Moves the source line item after the target item with in the order
        /// </summary>
        /// <param name="sourceItemID"></param>
        /// <param name="targetItemID"></param>
        /// <returns></returns>
        public async Task<string> MoveAfter(int sourceItemID, int targetItemID)
        {
            return await Move(sourceItemID, targetItemID, false);
        }

        /// <summary>
        /// query filter for where primary key of id
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<OrderItemData> WherePrimary(IQueryable<OrderItemData> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Map the relevent data into the given Map Item
        /// </summary>
        public override Task MapItem(OrderItemData item, IExpandIncludes includeExpander = null)
        {
            if (item.Components != null)
            {
                foreach (var component in item.Components)
                {
                    var taxEntries = ctx.OrderItemTaxAssessment.Where(x => x.BID == BID && x.ItemComponentID.GetValueOrDefault() == (int)component.ID);
                    if (taxEntries.Any())
                    {
                        component.TaxInfoList = new List<TaxAssessmentResult>();
                        var taxGroupIDs = taxEntries.Select(e => e.TaxGroupID).Distinct().ToList();
                        foreach (var taxGroupID in taxGroupIDs)
                        {
                            var taxItemResults = new List<TaxAssessmentItemResult>();
                            var itemEntries = taxEntries.Where(x => x.TaxGroupID == taxGroupID).ToList();
                            foreach (var itemEntry in itemEntries)
                            {
                                var taxItem = ctx.TaxItem.Where(x => x.BID == BID && x.ID == itemEntry.TaxItemID).First();
                                TaxAssessmentItemResult itemResult = new TaxAssessmentItemResult()
                                {
                                    TaxItemID = itemEntry.TaxItemID,
                                    InvoiceText = taxItem.InvoiceText,
                                    TaxRate = itemEntry.TaxRate,
                                    TaxAmount = itemEntry.TaxAmount,
                                    GLAccountID = taxItem.GLAccountID
                                };
                                taxItemResults.Add(itemResult);
                            }
                            var firstItem = itemEntries.First();
                            TaxAssessmentResult result = new TaxAssessmentResult()
                            {
                                OrderItemID = firstItem.OrderItemID.GetValueOrDefault(),
                                TaxGroupID = firstItem.TaxGroupID,
                                TaxAmount = component.PriceTax.GetValueOrDefault(),
                                PriceTaxable = component.PriceTaxable.GetValueOrDefault(),
                                IsTaxExempt = ctx.OrderItemData.First(x => x.BID == BID && x.ID == component.OrderItemID).IsTaxExempt,
                                TaxRate = itemEntries.Sum(x => x.TaxRate),
                                ItemComponentID = component.ID,
                                InvoiceText = ctx.TaxGroup.FirstOrDefault(x => x.BID == BID && x.ID == taxGroupID)?.Name,
                                Items = taxItemResults
                            };
                            component.TaxInfoList.Add(result);
                        }
                    }
                }
            }
            if (item.Surcharges != null)
            {
                foreach (var surcharge in item.Surcharges)
                {
                    var taxEntries = ctx.OrderItemTaxAssessment.Where(x => x.BID == BID && x.ItemSurchargeID.GetValueOrDefault() == (int)surcharge.ID);
                    if (taxEntries.Any())
                    {
                        surcharge.TaxInfoList = new List<TaxAssessmentResult>();
                        var taxGroupIDs = taxEntries.Select(e => e.TaxGroupID).Distinct().ToList();
                        foreach (var taxGroupID in taxGroupIDs)
                        {
                            var taxItemResults = new List<TaxAssessmentItemResult>();
                            var itemEntries = taxEntries.Where(x => x.TaxGroupID == taxGroupID).ToList();
                            foreach (var itemEntry in itemEntries)
                            {
                                var taxItem = ctx.TaxItem.Where(x => x.BID == BID && x.ID == itemEntry.TaxItemID).First();
                                TaxAssessmentItemResult itemResult = new TaxAssessmentItemResult()
                                {
                                    TaxItemID = itemEntry.TaxItemID,
                                    InvoiceText = taxItem.InvoiceText,
                                    TaxRate = itemEntry.TaxRate,
                                    TaxAmount = itemEntry.TaxAmount,
                                    GLAccountID = taxItem.GLAccountID
                                };
                                taxItemResults.Add(itemResult);
                            }
                            var firstItem = itemEntries.First();
                            TaxAssessmentResult result = new TaxAssessmentResult()
                            {
                                OrderItemID = firstItem.OrderItemID.GetValueOrDefault(),
                                TaxGroupID = firstItem.TaxGroupID,
                                TaxAmount = surcharge.PriceTax.GetValueOrDefault(),
                                PriceTaxable = surcharge.PriceTaxable.GetValueOrDefault(),
                                IsTaxExempt = ctx.OrderItemData.First(x => x.BID == BID && x.ID == surcharge.OrderItemID).IsTaxExempt,
                                TaxRate = itemEntries.Sum(x => x.TaxRate),
                                ItemSurchargeID = surcharge.ID,
                                InvoiceText = ctx.TaxGroup.FirstOrDefault(x => x.BID == BID && x.ID == taxGroupID)?.Name,
                                Items = taxItemResults
                            };
                            surcharge.TaxInfoList.Add(result);
                        }
                    }
                }
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// Changes the OrderItem's Status by the Status Name or ID
        /// </summary>
        /// <param name="orderItemID">Order Item's ID</param>
        /// <param name="status">Status to change to by Name or ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> ChangeStatus(int orderItemID, string status)
        {
            short statusID = 0;
            OrderItemStatus orderItemStatus = null;

            if (short.TryParse(status, out statusID))
                orderItemStatus = await ctx.OrderItemStatus.Where(s => s.BID == BID && s.ID == statusID).FirstOrDefaultAsync();
            else
                orderItemStatus = await ctx.OrderItemStatus.Where(s => s.BID == BID && s.Name == status).FirstOrDefaultAsync();

            if (orderItemStatus == null && !string.IsNullOrWhiteSpace(status))
            {
                return new EntityActionChangeResponse()
                {
                    Success = false,
                    Message = $"OrderItemStatus '{status}' not found."
                };
            }

            if (string.IsNullOrWhiteSpace(status))
                orderItemStatus = await ctx.OrderItemStatus.Where(s => s.BID == BID && s.IsDefault == true).FirstOrDefaultAsync();

            statusID = orderItemStatus.ID;

            return await this.ChangeStatus(orderItemID, statusID);
        }

        /// <summary>
        /// Calls the stored procedure to change the OrderItem's Status
        /// </summary>
        /// <param name="orderItemID">Order Item's ID</param>
        /// <param name="statusID">Target Status ID</param>
        /// <returns></returns>
        private async Task<EntityActionChangeResponse> ChangeStatus(int orderItemID, short statusID)
        {
            try
            {
                var resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                var bidParam = new SqlParameter("@BID", BID);
                var orderItemIdParam = new SqlParameter("@OrderItemID", orderItemID);
                var statusIdParam = new SqlParameter("@TargetStatusID", statusID);

                object[] myParams = {
                    bidParam,
                    orderItemIdParam,
                    statusIdParam,
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC [dbo].[Order.Item.Action.ChangeStatus] @BID, @OrderItemID, @TargetStatusID, @Result OUTPUT", parameters: myParams);
                if (rowResult > 0)
                {
                    await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.OrderItem), orderItemID);
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(orderItemID) });

                    return new EntityActionChangeResponse
                    {
                        Id = orderItemID,
                        Message = "Successfully changed order item's status",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = orderItemID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = orderItemID,
                    Success = false,
                    ErrorMessage = ex.Message,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="costLookup"></param>
        /// <param name="request"></param>
        /// <param name="componentIDGetter"></param>
        /// <returns></returns>
        internal async Task<OrderItemComponentUnitCost> _getComponentCost(
            Func<int[], Task<Dictionary<int, decimal>>> costLookup,
            OrderItemComponentCostRequest request, 
            Func<OrderItemComponentCostRequestEntry, int?> componentIDGetter)
        {
            OrderItemComponentUnitCost response = new OrderItemComponentUnitCost();

            //get the components for this segment (Labor, Material, etc) 
            var components = request.Components.Where(x => componentIDGetter(x).HasValue && x.Quantity.HasValue);
            //map the IDs to the quantities
            Dictionary<int, decimal> quantityLookupTable = components.ToDictionary(k => componentIDGetter(k).Value, v => v.Quantity.Value);
            //get an ID array for use in EF queries
            int[] IDs = components.Select(x => Convert.ToInt32(componentIDGetter(x))).ToArray();
            //use the Func to get a map of ID to cost
            Dictionary<int, decimal> costLookupTable = await costLookup(IDs);
            //sum the quants * costs
            response.CostNet = IDs.Sum((id) => 
                (quantityLookupTable.ContainsKey(id) && costLookupTable.ContainsKey(id)) ? quantityLookupTable[id] * costLookupTable[id] : 0
                );
            //sum the per unit costs
            response.UnitCost = costLookupTable.Sum(x => x.Value);
            return response;
        }

        internal async Task<OrderItemComponentCostResponse> GetComponentCostAsync(OrderItemComponentCostRequest request)
        {
            OrderItemComponentCostResponse response = new OrderItemComponentCostResponse();

            response.Material = await _getComponentCost(
                (IDs) => ctx.MaterialData.Where(x => x.BID == BID && IDs.Contains(x.ID)).ToDictionaryAsync(k => k.ID, v => v.EstimatingCost ?? 0), 
                request, 
                x => x.MaterialID);
            response.Labor = await _getComponentCost(
                (IDs) => ctx.LaborData.Where(x => x.BID == BID && IDs.Contains(x.ID)).ToDictionaryAsync(k => k.ID, v => v.EstimatingCostFixed ?? 0),
                request,
                x => x.LaborID);
            response.Machine = new OrderItemComponentUnitCost();
            response.Assembly = new OrderItemComponentUnitCost();

            return response;
        }

        /// <summary>
        /// Sets the Key Date for the OrderItem
        /// </summary>
        /// <param name="orderID">Order ID</param>
        /// <param name="orderItemID">Order Item ID</param>
        /// <param name="keyDateType">Key Date type</param>
        /// <param name="isFirmDate">Indicates if there is no flexibility in the date</param>
        /// <param name="date">DateTime to set to</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> SetKeyDate(int orderID, int orderItemID, OrderKeyDateType keyDateType, bool isFirmDate, DateTime? date = null)
        {
            try
            {
                var resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                var bidParam = new SqlParameter("@BID", BID);
                var orderIdParam = new SqlParameter("@OrderID", orderID);
                var orderItemIdParam = new SqlParameter("@OrderItemID", orderItemID);
                var destinationIdParam = new SqlParameter("@DestinationID", DBNull.Value);
                var keyDateTypeParam = new SqlParameter("@KeyDateType", (int)keyDateType);
                var isFirmDateParam = new SqlParameter("@IsFirmDate", isFirmDate);
                SqlParameter dateTimeParam = null;
                if (date.HasValue)
                    dateTimeParam = new SqlParameter("@DateTime", date.Value);
                else
                    dateTimeParam = new SqlParameter("@DateTime", DBNull.Value);

                object[] myParams = {
                    bidParam,
                    orderIdParam,
                    orderItemIdParam,
                    destinationIdParam,
                    keyDateTypeParam,
                    dateTimeParam,
                    isFirmDateParam,
                    resultParam
                };

                await ctx.Database.ExecuteSqlRawAsync("EXEC [dbo].[Order.Action.ChangeKeyDate] @BID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @DateTime, @IsFirmDate, @Result OUTPUT", parameters: myParams);

                await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.OrderItem), orderItemID);
                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(orderItemID) });

                return new EntityActionChangeResponse
                {
                    Id = orderItemID,
                    Message = "Successfully set the key date.",
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = orderItemID,
                    Success = false,
                    ErrorMessage = ex.Message,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Removes the Key Date from the OrderItem
        /// </summary>
        /// <param name="orderID">Order ID</param>
        /// <param name="orderItemID">Order Item ID</param>
        /// <param name="keyDateType">Key Date type</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> RemoveKeyDate(int orderID, int orderItemID, OrderKeyDateType keyDateType)
        {
            try
            {
                var resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                var bidParam = new SqlParameter("@BID", BID);
                var orderIdParam = new SqlParameter("@OrderID", orderID);
                var orderItemIdParam = new SqlParameter("@OrderItemID", orderItemID);
                var destinationIdParam = new SqlParameter("@DestinationID", DBNull.Value);
                var keyDateTypeParam = new SqlParameter("@KeyDateType", (int)keyDateType);

                object[] myParams = {
                    bidParam,
                    orderIdParam,
                    orderItemIdParam,
                    destinationIdParam,
                    keyDateTypeParam,
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC [dbo].[Order.Action.RemoveKeyDate] @BID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @Result OUTPUT", parameters: myParams);
                if (rowResult > 0)
                {
                    await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.OrderItem), orderItemID);
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(orderItemID) });

                    return new EntityActionChangeResponse
                    {
                        Id = orderItemID,
                        Message = "Successfully removed the Key Date.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = orderItemID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = orderItemID,
                    Success = false,
                    ErrorMessage = ex.Message,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Sets the Note for the OrderItem
        /// </summary>
        /// <param name="orderID">Order ID</param>
        /// <param name="orderItemID">Order Item ID</param>
        /// <param name="noteType">Order Note Type</param>
        /// <param name="note">Note value</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> SetNote(int orderID, int orderItemID, OrderNoteType noteType, OrderNote note)
        {
            try
            {
                var resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                var bidParam = new SqlParameter("@BID", BID);
                var orderIdParam = new SqlParameter("@OrderID", orderID);
                var orderItemIdParam = new SqlParameter("@OrderItemID", orderItemID);
                var destinationIdParam = new SqlParameter("@DestinationID", DBNull.Value);
                var noteTypeParam = new SqlParameter("@NoteType", (int)noteType);

                SqlParameter noteParam = null;
                if (string.IsNullOrWhiteSpace(note?.Note))
                    noteParam = new SqlParameter("@Note", DBNull.Value);
                else
                    noteParam = new SqlParameter("@Note", note.Note);

                object[] myParams = {
                    bidParam,
                    orderIdParam,
                    orderItemIdParam,
                    destinationIdParam,
                    noteTypeParam,
                    noteParam,
                    resultParam
                };

                await ctx.Database.ExecuteSqlRawAsync("EXEC [dbo].[Order.Action.ChangeNote] @BID, @OrderID, @OrderItemID, @DestinationID, @NoteType, @Note, @Result OUTPUT", parameters: myParams);

                await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.OrderItem), orderItemID);
                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(orderItemID) });

                return new EntityActionChangeResponse
                {
                    Id = orderItemID,
                    Message = "Successfully set the note.",
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = orderItemID,
                    Success = false,
                    ErrorMessage = ex.Message,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Removes the Note from the OrderItem
        /// </summary>
        /// <param name="orderID">Order ID</param>
        /// <param name="orderItemID">Order Item ID</param>
        /// <param name="noteType">Order Note type</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> RemoveNote(int orderID, int orderItemID, OrderNoteType noteType)
        {
            try
            {
                var resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                var bidParam = new SqlParameter("@BID", BID);
                var orderIdParam = new SqlParameter("@OrderID", orderID);
                var orderItemIdParam = new SqlParameter("@OrderItemID", orderItemID);
                var destinationIdParam = new SqlParameter("@DestinationID", DBNull.Value);
                var noteTypeParam = new SqlParameter("@NoteType", (int)noteType);

                object[] myParams = {
                    bidParam,
                    orderIdParam,
                    orderItemIdParam,
                    destinationIdParam,
                    noteTypeParam,
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC [dbo].[Order.Action.RemoveNote] @BID, @OrderID, @OrderItemID, @DestinationID, @NoteType, @Result OUTPUT", parameters: myParams);
                if (rowResult > 0)
                {
                    await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.OrderItem), orderItemID);
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(orderItemID) });

                    return new EntityActionChangeResponse
                    {
                        Id = orderItemID,
                        Message = "Successfully removed the Note.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = orderItemID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = orderItemID,
                    Success = false,
                    ErrorMessage = ex.Message,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// propagate orderID
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public void DoBeforeCreateWithParent(OrderItemData child, OrderData parent)
        {
            DoBeforeCreateWithParentPrivate(child, parent);
        }

        /// <summary>
        /// propagate orderID
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public void DoBeforeCreateWithParent(OrderItemData child, CreditMemoData parent)
        {
            DoBeforeCreateWithParentPrivate(child, parent);
        }

        /// <summary>
        /// propagate orderID
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        private void DoBeforeCreateWithParentPrivate(OrderItemData child, TransactionHeaderData parent)
        {
            child.OrderID = parent.ID;
        }

        /// <summary>
        /// propagate orderID
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        public void DoBeforeUpdateWithParent(OrderItemData child, OrderData oldParent, OrderData newParent)
        {
            DoBeforeUpdateWithParentPrivate(child, oldParent, newParent);
        }

        /// <summary>
        /// propagate orderID
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        public void DoBeforeUpdateWithParent(OrderItemData child, CreditMemoData oldParent, CreditMemoData newParent)
        {
            DoBeforeUpdateWithParentPrivate(child, oldParent, newParent);
        }

        /// <summary>
        /// propagate orderID
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        private void DoBeforeUpdateWithParentPrivate(OrderItemData child, TransactionHeaderData oldParent, TransactionHeaderData newParent)
        {
            child.OrderID = newParent.ID;
        }

        internal static void MapChildTree(OrderItemData item, OrderItemComponentService componentService, OrderItemSurchargeService itemSurchargeService)
        {
            if (item.Components?.Any() ?? false)
            {
                // Load tax items
                foreach (OrderItemComponent component in item.Components)
                {
                    componentService.MapItem(component);
                }

                var childHash = item.Components.ToLookup(x => x.ParentComponentID);
                foreach (OrderItemComponent childC in item.Components)
                {
                    childC.ChildComponents = childHash[childC.ID].OrderBy(y => y.Number).ToList();
                }
                item.Components = item.Components.Where(y => y.ParentComponentID == null).OrderBy(y => y.Number).ToList();
            }

            if (item.Surcharges?.Any() ?? false)
            {
                // Load tax items
                foreach (OrderItemSurcharge surcharge in item.Surcharges)
                {
                    itemSurchargeService.MapItem(surcharge);
                }
            }
        }
    }

    public class OrderItemComponentCollection
    {
        public ICollection<OrderItemComponent> Items { get; set; }
    }
}
