﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Order Item Surcharge Service
    /// </summary>
    public class OrderItemSurchargeService : AtomCRUDService<OrderItemSurcharge, int>
    {
        /// <summary>
        /// Order Item Surcharge Service Layer
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderItemSurchargeService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default children to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Returns the includes
        /// </summary>
        /// <param name="includes">Children to include</param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// GetChildAssociations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<OrderItemSurcharge, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<OrderItemSurcharge, int>[0];
        }

        internal override void DoBeforeValidate(OrderItemSurcharge newModel)
        {
        }

        public async static Task AddOrderTaxItemAssessmentForSurcharge(OrderTaxItemAssessmentService orderTaxItemAssessmentService, int orderId, OrderItemSurcharge newModel)
        {
            //Populate OrderTaxItemAssessment
            if (newModel.TaxInfoList != null)
            {
                foreach (var TaxAssessment in newModel.TaxInfoList)
                {
                    foreach (var TaxItemAssessmentResult in TaxAssessment.Items)
                    {
                        OrderItemTaxAssessment newEntry = new OrderItemTaxAssessment()
                        {
                            TaxGroupID = TaxAssessment.TaxGroupID,
                            ItemSurchargeID = newModel.ID,
                            TaxAmount = TaxItemAssessmentResult.TaxAmount,
                            TaxRate = TaxItemAssessmentResult.TaxRate,
                            TaxItemID = TaxItemAssessmentResult.TaxItemID,
                            TaxCodeID = TaxAssessment.TaxCodeID.GetValueOrDefault(0), //defaulting to the default for now
                            OrderItemID = newModel.OrderItemID,
                            OrderID = orderId,
                            TaxableAmount = TaxAssessment.PriceTaxable,
                            BID = newModel.BID
                        };
                        await orderTaxItemAssessmentService.DoBeforeCreateAsync(newEntry, null);
                    }
                }
            }
        }

        /// <summary>
        /// Map the relevent data into the given Map Item
        /// </summary>
        public override Task MapItem(OrderItemSurcharge item, IExpandIncludes includeExpander = null)
        {
            var taxEntries = ctx.OrderItemTaxAssessment.AsNoTracking().Where(x => x.BID == BID && x.ItemSurchargeID.GetValueOrDefault() == (int)item.ID);
            if (taxEntries.Any())
            {
                item.TaxInfoList = new List<TaxAssessmentResult>();
                var taxGroupIDs = taxEntries.Select(e => e.TaxGroupID).Distinct().ToList();
                foreach (var taxGroupID in taxGroupIDs)
                {
                    var taxItemResults = new List<TaxAssessmentItemResult>();
                    var itemEntries = taxEntries.Where(x => x.TaxGroupID == taxGroupID).ToList();
                    foreach (var itemEntry in itemEntries)
                    {
                        var taxItem = ctx.TaxItem.Where(x => x.BID == BID && x.ID == itemEntry.TaxItemID).First();
                        TaxAssessmentItemResult itemResult = new TaxAssessmentItemResult()
                        {
                            TaxItemID = itemEntry.TaxItemID,
                            InvoiceText = taxItem.InvoiceText,
                            TaxRate = itemEntry.TaxRate,
                            TaxAmount = itemEntry.TaxAmount,
                            GLAccountID = taxItem.GLAccountID
                        };
                        taxItemResults.Add(itemResult);
                    }
                    var firstItem = itemEntries.First();
                    TaxAssessmentResult result = new TaxAssessmentResult()
                    {
                        OrderItemID = firstItem.OrderItemID.GetValueOrDefault(),
                        TaxGroupID = firstItem.TaxGroupID,
                        TaxAmount = item.PriceTax.GetValueOrDefault(),
                        PriceTaxable = item.PriceTaxable.GetValueOrDefault(),
                        IsTaxExempt = ctx.OrderItemData.First(x => x.BID == BID && x.ID == item.OrderItemID).IsTaxExempt,
                        TaxRate = itemEntries.Sum(x => x.TaxRate),
                        ItemSurchargeID = item.ID,
                        InvoiceText = ctx.TaxGroup.FirstOrDefault(x => x.BID == BID && x.ID == taxGroupID)?.Name,
                        Items = taxItemResults
                    };
                    item.TaxInfoList.Add(result);
                }
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// Business logic to do after updating the new model.
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(OrderItemSurcharge oldModel, OrderItemSurcharge newModel)
        {
            await base.DoBeforeUpdateAsync(oldModel, newModel);

            //Populate OrderTaxItemAssessment
            OrderTaxItemAssessmentService orderTaxItemAssessmentService = new OrderTaxItemAssessmentService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
            var existingEntries = ctx.OrderItemTaxAssessment.AsNoTracking().Where(otia => otia.BID == BID && otia.ItemSurchargeID == newModel.ID).ToList();

            if (newModel.TaxInfoList != null)
            {
                foreach (var TaxAssessment in newModel.TaxInfoList)
                {
                    foreach (var TaxItemAssessmentResult in TaxAssessment.Items)
                    {
                        var existingEntry = existingEntries.Where(e => e.TaxGroupID == TaxAssessment.TaxGroupID && e.TaxItemID == TaxItemAssessmentResult.TaxItemID).FirstOrDefault();
                        if (existingEntry != null)
                        {
                            //update entry in db
                            var clone = await orderTaxItemAssessmentService.GetClone(existingEntry.ID);
                            clone.TaxAmount = TaxItemAssessmentResult.TaxAmount;
                            clone.TaxRate = TaxItemAssessmentResult.TaxRate;
                            clone.TaxableAmount = TaxAssessment.PriceTaxable;
                            await orderTaxItemAssessmentService.DoBeforeUpdateAsync(existingEntry, clone);
                            ctx.Entry(clone).Property("BID").IsModified = false;
                            existingEntries.Remove(existingEntry);
                        }
                        else
                        {
                            //create new Entries
                            var orderItem = ctx.OrderItemData.Where(x => x.BID == BID && x.ID == newModel.OrderItemID).FirstOrDefault();
                            OrderItemTaxAssessment newEntry = new OrderItemTaxAssessment()
                            {
                                TaxGroupID = TaxAssessment.TaxGroupID,
                                ItemSurchargeID = newModel.ID,
                                TaxAmount = TaxItemAssessmentResult.TaxAmount,
                                TaxRate = TaxItemAssessmentResult.TaxRate,
                                TaxItemID = TaxItemAssessmentResult.TaxItemID,
                                TaxCodeID = TaxAssessment.TaxCodeID.GetValueOrDefault(0), //defaulting to the default for now
                                OrderItemID = newModel.OrderItemID,
                                OrderID = orderItem.OrderID,
                                TaxableAmount = TaxAssessment.PriceTaxable,
                                BID = newModel.BID
                            };
                            await orderTaxItemAssessmentService.DoBeforeCreateAsync(newEntry);
                        }
                    }
                }
            }
            foreach (var entry in existingEntries) //remove the remaining entries
            {
                await orderTaxItemAssessmentService.DeleteAsync(entry);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public override async Task DoBeforeDeleteAsync(OrderItemSurcharge model)
        {
            await base.DoBeforeDeleteAsync(model);
            model.OrderItem = null;
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">OrderItemSurcharge ID</param>
        /// <returns></returns>
        public override IQueryable<OrderItemSurcharge> WherePrimary(IQueryable<OrderItemSurcharge> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// override for set modified DT for temporal table
        /// </summary>
        /// <param name="newModel"></param>
        protected override void SetModifiedDT(OrderItemSurcharge newModel)
        {
            if (newModel.ID == 0)
            {
                /*
                fix for "Cannot insert an explicit value into a GENERATED ALWAYS column in table 'Dev.Endor.Business.DB1.dbo.Order.Item.Component'. 
                Use INSERT with a column list to exclude the GENERATED ALWAYS column, or insert a DEFAULT into GENERATED ALWAYS column."
                */
                newModel.ModifiedDT = default(DateTime);
            }

            ctx.Entry(newModel).Property(x => x.ModifiedDT).IsModified = false;

        }
    }
}
