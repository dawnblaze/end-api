﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Includes;
using Endor.EF;
using Endor.ExternalAuthenticator.Authenticator.Providers;
using Endor.ExternalAuthenticator.Authenticator.Response;
using Endor.ExternalAuthenticator.Models;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Email Account Service
    /// </summary>
    public class EmailAccountService : AtomCRUDService<EmailAccountData, short>, ISimpleListableViewService<SimpleEmailAccountData, short>
    {
        /// <summary>
        /// EmailProvider
        /// </summary>
        public IEmailProvider EmailProvider { get; set; }

        /// <summary>
        /// Constructs a email account service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public EmailAccountService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
            //this._emailProvider = emailProvider;
        }

        /// <summary>
        /// Gets simple list set
        /// </summary>
        public DbSet<SimpleEmailAccountData> SimpleListSet => ctx.SimpleEmailAccountData;

        /// <summary>
        /// Returns EmailAccounts based on filters supplied in
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public async Task<List<EmailAccountData>> GetWithFiltersAsync(EmailAccountFilter filters, EmailAccountIncludes includes = null)
        {
            if (filters != null && filters.HasFilters)
            {
                var resultList = this.Where(filters.WherePredicates(), false, includes).ToList();
                var newResultList = new List<EmailAccountData>();
                foreach (var result in resultList)
                {
                    if (result.EmailAccountTeamLinks != null && result.EmailAccountTeamLinks.Count > 0)
                    {
                        if (includes.IncludeTeams == IncludesLevel.Full)
                        {
                            result.Teams = new List<EmployeeTeam>();
                            foreach (var link in result.EmailAccountTeamLinks)
                            {
                                var team = link.Team;
                                team.EmailAccountTeamLinks = null; // prevent recursion
                                result.Teams.Add(team);
                            }
                            result.EmailAccountTeamLinks = null; // clear out so as to not serialize
                        }
                        else if (includes.IncludeTeams == IncludesLevel.None)
                        {
                            result.EmailAccountTeamLinks = null; // clear out so as to not serialize
                        }
                        else
                        {
                            foreach (var link in result.EmailAccountTeamLinks)
                            {
                                link.Team = null; // clear out so as to not serialize, and prevent recursion
                                link.EmailAccount = null; // clear out so as to not serialize, and prevent recursion
                            }
                        }
                    }
                    newResultList.Add(result);
                }

                // NOTE: credentials are nulled
                newResultList.ForEach(e => e.Credentials = null);
                return newResultList;
            }

            return await this.GetAsync();
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "DomainEmail" };

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Maps a specific item.
        /// </summary>
        /// <param name="item">Item to map</param>
        /// <param name="includes">Child/Parent includes</param>
        public override Task MapItem(EmailAccountData item, IExpandIncludes includes = null)
        {
            return base.MapItem(item, includes);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<EmailAccountData, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<EmailAccountData, short>[0];
        }

        internal override void DoBeforeValidate(EmailAccountData newModel) { }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<EmailAccountData> WherePrimary(IQueryable<EmailAccountData> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /*internal async Task<EntityActionChangeResponse> Test(short EmailAccountID, EmailAccountTestCredentials creds)
        {
            var EmailAccount = await ctx.EmailAccountData.FirstOrDefaultPrimaryAsync(this.BID, EmailAccountID);

            if (EmailAccount == null)
            {
                return new EntityActionChangeResponse
                {
                    Message = "Domain Not Found",
                    Success = false
                };
            }

            EmailAccount.LastVerificationAttemptDT = DateTime.UtcNow;

            var result = await DoTest(EmailAccount, creds);

            EmailAccount.LastVerificationSuccess = result.Success;
            EmailAccount.LastVerificationResult = result.Success ? "Success" : result.Message;
            if (result.Success)
                EmailAccount.LastVerifiedDT = EmailAccount.LastVerificationAttemptDT;

            ctx.SaveChanges();

            if (result.Success)
            {
                return new EntityActionChangeResponse
                {
                    Message = "Test Successful",
                    Success = true
                };
            }
            else
            {
                return new EntityActionChangeResponse
                {
                    Message = "Email Credentials or Password Not Supplied or Valid",
                    Success = false
                };
            }
        }

        private Task<EmailAccountTestResult> DoTest(EmailAccountData EmailAccount, EmailAccountTestCredentials creds)
        {
            return Task.FromResult(new EmailAccountTestResult()
            {
                Success = false,
                Message = "Not Implemented"
            });
        }*/

        internal async Task<SystemEmailSMTPConfigurationType[]> GetSMTPConfigurationTypes()
        {
            return await this.ctx.SystemEmailSMTPConfigurationType.ToArrayAsync();
        }

        /// <summary>
        /// Sets the industry (and any applicable parents/children) to active or inactive.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="active"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public override async Task<EntityActionChangeResponse<short>> SetActive(short id, bool active, string connectionID)
        {
            EntityActionChangeResponse<short> resp = null;
            try
            {
                EmailAccountData model = await GetAsync(id);
                if (model == null)
                    return new EntityActionChangeResponse<short>()
                    {
                        Message = "Entity not found",
                        ErrorMessage = "Entity not found",
                        Success = false,
                        Id = id
                    };
                if (active)
                    model.StatusType = EmailAccountStatus.Authorized;
                else
                    model.StatusType = EmailAccountStatus.Inactive;
                await this.UpdateAsync(model, connectionID);
                resp = new EntityActionChangeResponse<short>()
                {
                    Message = $"Status updated to {(active ? "active" : "inactive")}.",
                    Success = true,
                    Id = id
                };
            }
            catch (Exception ex)
            {
                resp = new EntityActionChangeResponse<short>()
                {
                    Message = ex.Message,
                    ErrorMessage = ex.Message,
                    Success = false,
                    Id = id
                };
            }
            return resp;
        }

        /// <summary>
        /// Get All Email Accounts for a location
        /// </summary>
        /// <param name="locationID">Location ID to check for</param>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<List<EmailAccountData>> GetEmailAccountsForLocation(short locationID, EmailAccountFilter filters)
        {
            var domainIDs = ctx.DomainEmailLocationLink
                .Where(l => l.LocationID == locationID)
                .Select(l => l.DomainID).ToList();
            var results = await ctx.EmailAccountData
                .Where(a => domainIDs.Contains(a.DomainEmailID))
                .WhereAll(filters.WherePredicates())
                .ToListAsync();
            // NOTE: credentials are nulled
            results.ForEach(e => e.Credentials = null);
            return results;
        }

        /// <summary>
        /// Get All Email Accounts for a employee
        /// </summary>
        /// <param name="employeeID">Location ID to check for</param>
        /// <param name="IncludeShared"></param>
        /// <param name="orderId"></param>
        /// <param name="estimateID"></param>
        /// <param name="companyID"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<List<EmailAccountData>> GetEmailAccountsForEmployee(short employeeID, bool IncludeShared, int? orderId, int? estimateID, int? companyID, EmailAccountFilter filters)
        {
            var emailAccounts = await ctx.EmailAccountData.Where(x => x.EmployeeID == employeeID)
                                                          .ToListAsync();

            var teamIds = await ctx.EmployeeTeamLink.Where(x => x.EmployeeID == employeeID)
                                                    .Select(x => x.TeamID)
                                                    .ToListAsync();

            var teamEmailAccounts = await ctx.EmailAccountTeamLink.Include(x => x.EmailAccount)
                                                                  .Where(x => teamIds.Contains(x.TeamID))
                                                                  .Select(x => x.EmailAccount)
                                                                  .ToListAsync();
            emailAccounts.AddRange(teamEmailAccounts);

            if (IncludeShared)
            {
                short locationID = 0;
                if (orderId.HasValue)
                {
                    locationID = ctx.OrderData.Where(o => o.ID == orderId).Select(o => o.LocationID).First();    
                } else if (estimateID.HasValue)
                {
                    locationID = ctx.EstimateData.Where(o => o.ID == estimateID).Select(o => o.LocationID).First();
                } else if (companyID.HasValue)
                {
                    locationID = ctx.CompanyData.Where(o => o.ID == companyID).Select(o => o.LocationID).First();
                } else
                {
                    locationID = ctx.EmployeeData.Where(o => o.ID == employeeID).Select(o => o.LocationID).FirstOrDefault();
                }
                emailAccounts.AddRange(await GetEmailAccountsForLocation(locationID, filters));
                // distinct
                emailAccounts = emailAccounts.GroupBy(e => e.ID).Select(e => e.First()).ToList();
            }
            // NOTE: credentials are nulled
            emailAccounts.ForEach(e => e.Credentials = null);
            return emailAccounts;
        }

        /// <summary>
        /// Get All Email Accounts for a employee
        /// </summary>
        /// <param name="employeeID">Location ID to check for</param>
        /// <param name="orderId"></param>
        /// <param name="estimateID"></param>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public async Task<EmailAccountData> GetDefaultEmailAccountForEmployee(short employeeID, int? orderId, int? estimateID, int? companyID)
        {
            var employeeDefault = ctx.EmployeeData.Where(e => e.ID == employeeID).FirstOrDefault();
            if (employeeDefault.DefaultEmailAccountID.HasValue)
            {
                var result = await ctx.EmailAccountData.Where(a => a.ID == employeeDefault.DefaultEmailAccountID.Value).FirstAsync();
                // NOTE: credentials are nulled
                result.Credentials = null;
                return result;
            }
            short locationID = 0;
            if (orderId.HasValue)
            {
                locationID = ctx.OrderData.Where(o => o.ID == orderId).Select(o => o.LocationID).First();
            }
            else if (estimateID.HasValue)
            {
                locationID = ctx.EstimateData.Where(o => o.ID == estimateID).Select(o => o.LocationID).First();
            }
            else if (companyID.HasValue)
            {
                locationID = ctx.CompanyData.Where(o => o.ID == companyID).Select(o => o.LocationID).First();
            }
            else
            {
                locationID = ctx.EmployeeData.Where(o => o.ID == employeeID).Select(o => o.LocationID).First();
            }
            var location = await ctx.LocationData.Where(o => o.ID == locationID).FirstAsync();
            if (location.DefaultEmailAccountID.HasValue)
            {
                var result = await ctx.EmailAccountData.Where(a => a.ID == location.DefaultEmailAccountID.Value).FirstAsync();
                // NOTE: credentials are nulled
                result.Credentials = null;
                return result;
            }
            else
                return null;
        }

        /// <summary>
        /// Checks if the Email Account can be deleted
        /// </summary>
        /// <param name="id">Team ID to check for</param>
        /// <returns></returns>
        public override Task<BooleanResponse> CanDelete(short id)
        {
            var defaultForEmployee = ctx.EmployeeData.Where(x => x.DefaultEmailAccountID == id);
            var defaultForLocation = ctx.LocationData.Where(x => x.DefaultEmailAccountID == id);
            return Task.FromResult(new BooleanResponse
            {
                Success = true,
                Value = !(defaultForEmployee.Any() || defaultForLocation.Any())
            });
        }

        /// <summary>
        /// The email should be sent to the user's email account from the user's email account
        /// </summary>
        /// <param name="emailAccountID"></param>
        /// <param name="passPhrase"></param>
        /// <returns></returns>
        internal async Task<EntityActionChangeResponse> Test(short emailAccountID, string passPhrase)
        {
            var emailAccount = await ctx.EmailAccountData.FirstOrDefaultPrimaryAsync(this.BID, emailAccountID);
            if (emailAccount == null) return ValidationResponse("Email Account Not Found");
            if (string.IsNullOrWhiteSpace(emailAccount.Credentials)) return ValidationResponse("Email Account Credentials are not valid");

            var domainEmail = await ctx.DomainEmail.FirstOrDefaultPrimaryAsync(this.BID, emailAccount.DomainEmailID);
            if (domainEmail == null) return ValidationResponse("Domain Email Not Found");
   
            EmailAccountTestResult result = new EmailAccountTestResult();


            if (domainEmail.ProviderType == EmailProviderType.CustomSMTP)
            {
                result = await AuthenticateAndSendEmail(emailAccount, passPhrase); 
            }
            else if(domainEmail.ProviderType == EmailProviderType.GoogleGSuite || domainEmail.ProviderType == EmailProviderType.Microsoft365)
            {
                ValidationResult validationResult = await ValidateAsync(emailAccount, "");

                if (validationResult.IsValid)
                {
                    Email email = BuildEmail(emailAccount);
                    var sendResult = await this.EmailProvider.GetProvider(domainEmail.ProviderType).SendEmail(validationResult.RefreshToken, email);


                    result = DeserializeSendResult(sendResult);
                }
                else
                {
                    return ValidationResponse("Invalid access token");
                }
            }
            else
            {
                // domainEmail.ProviderType == EmailProviderType.None
            }

            if (result.Success)
            {
                emailAccount.StatusType = EmailAccountStatus.Authorized;
                ctx.SaveChanges();
                return ValidationResponse("Test Successful", true);
            }
            else
            {
                emailAccount.StatusType = EmailAccountStatus.Failed;
                ctx.SaveChanges();
                return ValidationResponse(!string.IsNullOrWhiteSpace(result.Message) 
                    ? result.Message
                    : "Email Credentials or Password Not Supplied or Valid");
            }
        }

        /// <summary>
        /// No exception expected result: "{\"statusCode\":200}"
        /// </summary>
        /// <param name="sendResult"></param>
        /// <returns></returns>
        private EmailAccountTestResult DeserializeSendResult(string sendResult)
        {
            try
            {
                var result = JsonConvert.DeserializeObject<SendResult>(sendResult ?? string.Empty);
                return new EmailAccountTestResult
                {
                    Message = sendResult ?? string.Empty,
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new EmailAccountTestResult
                {
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAccount"></param>
        /// <returns></returns>
        private static Email BuildEmail(EmailAccountData emailAccount)
        {
            return new Email()
            {
                Body = $"This is a test email sent by {emailAccount.EmailAddress}",
                Recipients = new string[] { emailAccount.EmailAddress },
                Sender = emailAccount.EmailAddress,
                Subject = $"Test Email {DateTime.UtcNow}"
            };
        }

        /// <summary>
        /// Sends an Email
        /// </summary>
        /// <param name="msg">Email Message</param>
        /// <param name="passphrase">Passphrase</param>
        /// <param name="employeeId">Employee ID</param>
        /// <param name="orderId">Order ID</param>
        /// <param name="estimateId">Estimate ID</param>
        /// <param name="companyId">Company ID</param>
        /// <param name="overrideEmailAccountID">Override Email Account ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> SendEmail(Message msg, string passphrase, short employeeId, int? orderId, int? estimateId, int? companyId, short? overrideEmailAccountID = null)
        {
            EmailAccountData emailAccount = null;
            if (overrideEmailAccountID.HasValue)
            {
                emailAccount = await this.GetAsync(overrideEmailAccountID.Value);
            } else
            {
                emailAccount = await this.GetDefaultEmailAccountForEmployee(employeeId, orderId, estimateId, companyId);
            }
            var result = await AuthenticateAndSendEmail(emailAccount, passphrase, msg);

            if (result.Success)
            {
                return new EntityActionChangeResponse
                {
                    Message = "Email Sending Successful",
                    Success = true

                };
            }
            else
            {
                emailAccount.StatusType = EmailAccountStatus.Failed;
                return new EntityActionChangeResponse
                {
                    Message = "Problem authorizing account or sending email",
                    Success = false
                };
            }

        }

        internal Task<EmailAccountTestResult> AuthenticateAndSendEmail(EmailAccountData emailAccount, String passphrase, Message msg = null)
        {
            if (string.IsNullOrWhiteSpace(passphrase))
            {
                return Task.FromResult(new EmailAccountTestResult()
                {
                    Success = false,
                    Message = $"Passphrase are required"
                });
            };

            using (var client = new SmtpClient())
            {
                try
                {
                    client.Connect("smtp.gmail.com", 465, SecureSocketOptions.SslOnConnect);
                }
                catch (SmtpCommandException ex)
                {
                    return Task.FromResult(new EmailAccountTestResult()
                    {
                        Success = false,
                        Message = $"Error trying to connect: {ex.Message}. StatusCode: {ex.StatusCode}"
                    });
                }
                catch (SmtpProtocolException ex)
                {
                    return Task.FromResult(new EmailAccountTestResult()
                    {
                        Success = false,
                        Message = $"Protocol error while trying to connect:  {ex.Message}"
                    });
                }

                // Note: Not all SMTP servers support authentication, but GMail does.
                if (client.Capabilities.HasFlag(SmtpCapabilities.Authentication))
                {
                    try
                    {
                        client.Authenticate(emailAccount.EmailAddress, StringEncryptor.Decrypt(emailAccount.Credentials,passphrase));
                    }
                    catch (AuthenticationException)
                    {
                        return Task.FromResult(new EmailAccountTestResult()
                        {
                            Success = false,
                            Message = $"Invalid user name or password."
                        });
                    }
                    catch (SmtpCommandException ex)
                    {
                        return Task.FromResult(new EmailAccountTestResult()
                        {
                            Success = false,
                            Message = $"Error trying to authenticate: {ex.Message}. StatusCode: {ex.StatusCode}"
                        });
                    }
                    catch (SmtpProtocolException ex)
                    {
                        return Task.FromResult(new EmailAccountTestResult()
                        {
                            Success = false,
                            Message = $"Protocol error while trying to connect:  {ex.Message}"
                        });
                    }
                }
                if (msg == null)
                {
                    client.Disconnect(true);
                    return Task.FromResult(new EmailAccountTestResult()
                    {
                        Success = true,
                        Message = "Authenticated"
                    });
                } else
                {

                    try
                    {
                        var message = new MimeMessage();
                        var fromParticipant = msg.Participants.Where(p => p.ParticipantRoleType == MessageParticipantRoleType.From).First();
                        message.From.Add(new MailboxAddress(fromParticipant.UserName));
                        var toParticipants = msg.Participants.Where(p => p.ParticipantRoleType == MessageParticipantRoleType.To).ToList();
                        foreach (var toParticipant in toParticipants)
                        {
                            message.To.Add(new MailboxAddress(toParticipant.UserName));
                        }
                        var ccParticipants = msg.Participants.Where(p => p.ParticipantRoleType == MessageParticipantRoleType.CC).ToList();
                        foreach (var ccParticipant in ccParticipants)
                        {
                            message.To.Add(new MailboxAddress(ccParticipant.UserName));
                        }
                        message.Subject = msg.Subject;
                        message.Body = new TextPart("plain")
                        {
                            Text = msg.Body
                        };
                        client.Send(message);
                        emailAccount.LastEmailSuccessDT = DateTime.UtcNow;
                        
                    }
                    catch (SmtpCommandException ex)
                    {
                        string error = $"Error sending message: {ex.Message}";
                        error += $"\tStatusCode: {ex.StatusCode}";

                        switch (ex.ErrorCode)
                        {
                            case SmtpErrorCode.RecipientNotAccepted:
                                error += $"\tRecipient not accepted: {ex.Mailbox}";
                                break;
                            case SmtpErrorCode.SenderNotAccepted:
                                error += $"\tSender not accepted: {ex.Mailbox}";
                                break;
                            case SmtpErrorCode.MessageNotAccepted:
                                error += $"\tMessage not accepted.";
                                break;
                        }
                        emailAccount.LastEmailFailureDT = DateTime.UtcNow;
                        ctx.SaveChanges();
                        return Task.FromResult(new EmailAccountTestResult()
                        {
                            Success = false,
                            Message = error
                        });
                    }
                    catch (SmtpProtocolException ex)
                    {
                        emailAccount.LastEmailFailureDT = DateTime.UtcNow;
                        ctx.SaveChanges();
                        return Task.FromResult(new EmailAccountTestResult()
                        {
                            Success = false,
                            Message = $"Protocol error while sending message: {ex.Message}"
                        });  
                    }
                    ctx.SaveChanges();
                    client.Disconnect(true);
                }


            }
            return Task.FromResult(new EmailAccountTestResult()
            {
                Success = true,
                Message = "Email Sent"
            });


        }

        /// <summary>
        /// Set the default email for an object
        /// </summary>
        /// <param name="emailAccount"></param>
        /// <param name="objectId"></param>
        /// <param name="classType"></param>
        /// <returns></returns>
        public async Task<bool> SetDefault(EmailAccountData emailAccount, short objectId, ClassType classType)
        {
            if (!emailAccount.IsActive)
            {
                return false;
            } else
            {
                if (classType == ClassType.Location)
                {
                    if (!emailAccount.IsPrivate)
                    {
                        return false;
                    }
                    var location = ctx.LocationData.Where(l => l.ID == objectId).First();
                    location.DefaultEmailAccountID = emailAccount.ID;
                    await ctx.SaveChangesAsync();
                    return true;
                }
                if (classType == ClassType.Employee)
                {
                    var employee = ctx.EmployeeData.Where(l => l.ID == objectId).First();
                    employee.DefaultEmailAccountID = emailAccount.ID;
                    await ctx.SaveChangesAsync();
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Clears the specified type as the default. Only called for an Employee Email Account.
        /// </summary>
        /// <param name="emailAccount"></param>
        /// <returns></returns>
        public async Task<bool> ClearDefault(EmailAccountData emailAccount)
        {
            if (!emailAccount.IsPrivate)
            {
                return false;
            }
            else
            {
                var employee = ctx.EmployeeData.Where(l => l.DefaultEmailAccountID == emailAccount.ID).First();
                employee.DefaultEmailAccountID = null;
                await ctx.SaveChangesAsync();
                return true;
            }
        }

        /// <summary>
        /// When the EmailAccount Object is edited, if credentials are passed in by the client
        /// The credentials are tested using Google or Microsoft's validation routine
        /// The EmailAccount Status is set to :
        ///     a. "Failed" if the test fails; 
        ///     b. "Authorized" if the test passes.
        /// </summary>
        /// <param name="emailAccountData"></param>
        /// <param name="encryptionPassPhrase"></param>
        /// <returns></returns>
        public async Task<ValidationResult> ValidateAsync(EmailAccountData emailAccountData, string encryptionPassPhrase)
        {
            DomainEmail domainEmail = await this.ctx.DomainEmail.FirstOrDefaultPrimaryAsync(this.BID, emailAccountData.DomainEmailID);

            if (domainEmail != null)
            {
                ValidToken validToken = new ValidToken { RequestDT = DateTime.UtcNow };
                EmailAccountCredentials emailAccountCredentials = new EmailAccountCredentials();

                if (domainEmail.ProviderType == EmailProviderType.GoogleGSuite || domainEmail.ProviderType == EmailProviderType.Microsoft365)
                {
                    emailAccountCredentials = JsonConvert.DeserializeObject<EmailAccountCredentials>(emailAccountData.Credentials ?? string.Empty);
                    validToken = this.EmailProvider.GetProvider(domainEmail.ProviderType).Validate(emailAccountCredentials?.AccessToken);
                }
                else if (domainEmail.ProviderType == EmailProviderType.CustomSMTP)
                {
                    EmailAccountTestResult result = await AuthenticateAndSendEmail(emailAccountData, encryptionPassPhrase, null);
                    validToken.IsValid = result.Success;
                }
                else // ProviderType == EmailProviderType.None
                {
                    // by default validToken.IsValid = false
                    // the status will be set to failed
                }

                return new ValidationResult
                {
                    ValidToken = validToken,
                    IsValid = validToken.IsValid,
                    AccessToken = emailAccountCredentials?.AccessToken,
                    RefreshToken = emailAccountCredentials?.RefreshToken
                };
            }
            else
            {
                return new ValidationResult
                {
                    ValidToken = null,
                    IsValid = false,
                    AccessToken = null,
                    RefreshToken = null
                };
            }
        }

        /// <summary>
        /// The DomainName must match the DomainNameID specified if they try to submit otherwise
        /// </summary>
        /// <param name="emailAccountData"></param>
        /// <returns></returns>
        public async Task<ValidationDomainResult> ValidateDomainAsync(EmailAccountData emailAccountData)
        {
            DomainEmail domainEmail = await this.ctx.DomainEmail.FirstOrDefaultPrimaryAsync(this.BID, emailAccountData.DomainEmailID);
            
            var message = "";
            bool IsValid = false;
            if (domainEmail != null)
            {
                IsValid = true;
                if (domainEmail.IsActive == false)
                {
                    IsValid = false;
                    message = "The DomainName supplied is inactive.";                    
                }

                if (emailAccountData.DomainName.ToLowerInvariant() != domainEmail.DomainName.ToLowerInvariant())
                {
                    IsValid = false;
                    message = "Invalid Request - The Domain Name must match the Domain in the Specified DomainEmail setup.";
                }
            }
            else
            {
                message = "DomainEmailID " + emailAccountData.DomainEmailID + " is not found in the [Domain.Email.Data]";
                IsValid = false;
            }

            string userEmail = string.Format("{0}@{1}", emailAccountData.UserName, emailAccountData.DomainName);
            var emailAccount = this.ctx.EmailAccountData.Where(EA => EA.EmailAddress == userEmail).FirstOrDefault();
            if (emailAccount != null)
            {
                IsValid = false;
                message = "Emailaddress is already exist.";
            }

            return new ValidationDomainResult
            {
                IsValid = IsValid,
                Message = message
            };
            
        }

        /// <summary>
        /// Check if valid base64 string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private async Task<bool> IsBase64StringAsync(string input)
        {
            try
            {
                Convert.FromBase64String(input);
                //valid base64 string
                return await Task.FromResult(true);
            }
            catch
            {
                // not valid base64 string
                return await Task.FromResult(false);
            }
        }

        /// <summary>
        /// Updates the status, based on the results of the credentials
        /// </summary>
        /// <param name="model"></param>
        /// <param name="validToken"></param>
        public void UpdateStatus(EmailAccountData model, ValidToken validToken)
        {
            var credentialsObject = JsonConvert.DeserializeObject<EmailAccountCredentials>(model.Credentials ?? string.Empty);
            if (credentialsObject == null)
            {
                model.StatusType = EmailAccountStatus.Pending;
            }
            else
            {
                if (validToken != null)
                {
                    if (validToken.IsValid)
                    {
                        model.StatusType = EmailAccountStatus.Authorized;
                        model.LastEmailSuccessDT = DateTime.UtcNow;
                    }
                    else
                    {
                        model.StatusType = EmailAccountStatus.Failed;
                        model.LastEmailFailureDT = DateTime.UtcNow;
                    }
                }
            }
        }

        /// <summary>
        /// If credentials are not passed on an edit, the existing Credentials should not be overridden nor the status changed.
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override Task DoBeforeUpdateAsync(EmailAccountData oldModel, EmailAccountData newModel)
        {
            if (string.IsNullOrWhiteSpace(newModel.Credentials))
            {
                newModel.Credentials = oldModel.Credentials;
                newModel.StatusType = oldModel.StatusType;
            }
            
            return base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        /// <summary>
        /// overriden GetAsync to remove credentials
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public async override Task<List<EmailAccountData>> GetAsync(IExpandIncludes includes = null)
        {
            var resultList = await base.GetAsync(includes);
            
            // NOTE: credentials are nulled
            resultList.ForEach(e => e.Credentials = null);

            return resultList;
        }

        /// <summary>
        /// overriden GetAsync to remove credentials
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public async override Task<EmailAccountData> GetAsync(short ID, IExpandIncludes includes = null)
        {
            var result = await base.GetAsync(ID, includes);
            if (result != null)
            {
                if (includes != null)
                {
                    EmailAccountIncludes eaIncludes = (EmailAccountIncludes)includes;
                    if (result.EmailAccountTeamLinks != null && result.EmailAccountTeamLinks.Count > 0)
                    {
                        if (eaIncludes != null && eaIncludes.IncludeTeams == IncludesLevel.Full)
                        {
                            result.Teams = new List<EmployeeTeam>();
                            foreach (var link in result.EmailAccountTeamLinks)
                            {
                                var team = link.Team;
                                team.EmailAccountTeamLinks = null; // prevent recursion
                                result.Teams.Add(team);
                            }
                            result.EmailAccountTeamLinks = null; // clear out so as to not serialize
                        }
                        else if (eaIncludes != null && eaIncludes.IncludeTeams == IncludesLevel.None)
                        {
                            result.EmailAccountTeamLinks = null; // clear out so as to not serialize
                        }
                        else // simple
                        {
                            foreach (var link in result.EmailAccountTeamLinks)
                            {
                                link.Team = null; // clear out so as to not serialize, and prevent recursion
                                link.EmailAccount = null; // clear out so as to not serialize, and prevent recursion
                            }
                        }
                    }
                    else
                    {
                        result.EmailAccountTeamLinks = null; // clear out so as to not serialize
                    }
                }

                // NOTE: credentials are nulled
                result.Credentials = null;
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="success"></param>
        /// <returns></returns>
        private static EntityActionChangeResponse ValidationResponse(string message, bool success = false)
        {
            return new EntityActionChangeResponse
            {
                Message = message,
                Success = success
            };
        }

        public async Task<EntityActionChangeResponse> LinkTeam(short id, short teamID, bool link)
        {
            var emailAccount = await ctx.EmailAccountData.Include(x => x.EmailAccountTeamLinks)
                .FirstOrDefaultAsync(x => x.BID == BID && x.ID == id);
            if (emailAccount == null)
            {
                return new EntityActionChangeResponse()
                {
                    Id = id,
                    Message = $"EmailAccount with ID `{id}` not found.",
                    Success = false
                };
            }
            var team = await ctx.EmployeeTeam.FirstOrDefaultAsync(x => x.BID == BID && x.ID == teamID);
            if (team == null)
            {
                return new EntityActionChangeResponse()
                {
                    Id = id,
                    Message = $"EmployeeTeam with ID `{teamID}` not found.",
                    Success = false
                };
            }

            if (link && !emailAccount.EmailAccountTeamLinks.Any(x => x.TeamID == teamID))
            {
                var newLink = new EmailAccountTeamLink()
                {
                    BID = BID,
                    EmailAccountID = id,
                    TeamID = teamID
                };
                ctx.EmailAccountTeamLink.Add(newLink);
                await ctx.SaveChangesAsync();
            }
            else if (!link && emailAccount.EmailAccountTeamLinks.Any(x => x.TeamID == teamID))
            {
                var linkToRemove = emailAccount.EmailAccountTeamLinks.FirstOrDefault(x => x.TeamID == teamID);
                ctx.Remove(linkToRemove);
                await ctx.SaveChangesAsync();
            }

            await QueueIndexForModel(id);
            return new EntityActionChangeResponse()
            {
                Id = id,
                Message = $"EmailAccount successfully {(link?"linked with":"unlinked from")} Team.",
                Success = true
            };
        }
    }

    /// <summary>
    /// Credential model for Gmail and Microsoft provider
    /// </summary>
    public class EmailAccountCredentials
    {
        /// <summary>
        /// access token
        /// </summary>
        public string AccessToken { get; set; }
        /// <summary>
        /// refresh token
        /// </summary>
        public string RefreshToken { get; set; }
        /// <summary>
        /// expiration date time
        /// </summary>
        public DateTime ExpirationDT { get; set; }
    }

    /// <summary>
    /// Credential model for CustomSMTP provider
    /// </summary>
    public class CustomSMTPCredential
    {
        /// <summary>
        /// password
        /// </summary>
        public string password { get; set; }
    }

    /// <summary>
    /// Email Account Test Result
    /// </summary>
    public  class EmailAccountTestResult
    {
        /// <summary>
        /// Whether or not the test result succeeds
        /// </summary>
        public bool Success { get; set; }
        /// <summary>
        /// Result message
        /// </summary>
        public string Message { get; set; }
    }

    /// <summary>
    /// SendResult email model for Gmail and Microsoft provider 
    /// </summary>
    public class SendResult
    {
        /// <summary>
        /// Status code result
        /// </summary>
        public int statusCode { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ValidationResult
    {
        /// <summary>
        /// AccessToken
        /// </summary>
        public string AccessToken { get; set; }
        /// <summary>
        /// Refresh Token
        /// </summary>
        public string RefreshToken { get; set; }
        /// <summary>
        /// IsValid
        /// </summary>
        public bool IsValid { get; set; }
        /// <summary>
        /// Valid Token value
        /// </summary>
        public ValidToken ValidToken { get; set; }
    }

    /// <summary>
    /// Valid Domain Result
    /// </summary>
    public class ValidationDomainResult
    {
        /// <summary>
        /// IsValid
        /// </summary>
        public bool IsValid { get; set; }
        /// <summary>
        /// Valid Token value
        /// </summary>
        public string Message { get; set; }
    }

    /// <summary>
    /// Email Provider Interface
    /// </summary>
    public interface IEmailProvider
    {
        /// <summary>
        /// Gets an Email Provider
        /// </summary>
        /// <param name="emailProviderType"></param>
        /// <returns></returns>
        EmailProviderBase GetProvider(EmailProviderType emailProviderType);
    }

    /// <summary>
    /// Email Provider
    /// </summary>
    public class EmailProvider : IEmailProvider
    {
        /// <summary>
        /// Providers List
        /// </summary>
        public List<EmailProviderBase> Providers = new List<EmailProviderBase>();

        /// <summary>
        /// Gets the Email Provider
        /// </summary>
        /// <param name="emailProviderType">Email Provider Type</param>
        /// <returns></returns>
        public EmailProviderBase GetProvider(EmailProviderType emailProviderType)
        {
            switch (emailProviderType)
            {
                case EmailProviderType.GoogleGSuite:
                    return this.Providers.OfType<GmailProvider>().FirstOrDefault();
                case EmailProviderType.Microsoft365:
                    return this.Providers.OfType<Office365Provider>().FirstOrDefault();
                default:
                    return null;
            }
        }
    }
}

