﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Responses;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Endor.Units;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// A service for <see cref=" MaterialData"/>
    /// </summary>
    public class MaterialService : AtomCRUDService<MaterialData, int>, ISimpleListableViewService<SimpleMaterialData, int>, IPartCategoryLinkableService
    {
        private readonly Lazy<MaterialCategoryService> _matCatSvc;

        /// <summary>
        /// Constructs a Material service with injected params
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper"></param>
        public MaterialService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
            //this.laborCategorySvc = new LaborCategoryService(context, bid, taskQueuer, cache, logger);
            this._matCatSvc = new Lazy<MaterialCategoryService>(() => new MaterialCategoryService(context, this.BID, taskQueuer, cache, logger, rtmClient, migrationHelper));
        }

        /// <summary>
        /// Gets simple list set
        /// </summary>
        public DbSet<SimpleMaterialData> SimpleListSet => this.ctx.Set<SimpleMaterialData>();


        /// <summary>
        /// 
        /// </summary>
        /// <param name="variableID"></param>
        /// <param name="machineID"></param>
        /// <param name="profileName"></param>
        /// <returns></returns>
        public async Task<DropdownResponse<SimpleMaterialData>> GetDropdownByVariable(int variableID, int? machineID, string profileName)
        {
            return await AssemblyHelper.GetDropdownResponse(ctx, logger, BID, variableID, machineID, profileName,
                async (DropDownListValues listValues) =>
                {
                    return await GetByDropDownListValues(listValues);
                });

        }

        private async Task<ICollection<SimpleMaterialData>> GetByDropDownListValues(DropDownListValues listValues)
        {
            return await GetByIDsAndCategoriesAsync(listValues?.Components?.Select(x => x.ID.Value)?.ToList(), listValues?.Categories?.Select(x => x.ID.Value)?.ToList());
        }

        /// <summary>
        /// Get using list of IDs and CategoryIDs as filter
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="categoryIDs"></param>
        /// <param name="IsActive"></param>
        /// <returns></returns>
        public async Task<ICollection<SimpleMaterialData>> GetByIDsAndCategoriesAsync(ICollection<int> ids, ICollection<int> categoryIDs, bool? IsActive = true)
        {
            List<int> IDs = ids?.ToList();
            List<int> CategoryIDs = categoryIDs?.ToList();

            var result = await this.ctx.MaterialData
                            .Include(e => e.MaterialCategoryLinks)
                            .Select(e => new {
                                data = e,
                                CategoryIDs = e.MaterialCategoryLinks.Select(catlink => catlink.CategoryID)
                            }).Where(q =>
                                q.data.BID == this.BID &&
                                (IsActive == null || q.data.IsActive == IsActive) &&
                                (
                                    (IDs == null || IDs.Contains(q.data.ID)) || 
                                    (CategoryIDs == null || q.CategoryIDs.Any(matCatID => CategoryIDs.Contains(matCatID)))
                                )
                            ).Select(q => new SimpleMaterialData()
                            {
                                BID = q.data.BID,
                                ClassTypeID = (int)ClassType.Material,
                                Description = q.data.Description,
                                DisplayName = q.data.Name,
                                HasImage = q.data.HasImage,
                                ID = q.data.ID,
                                UnitType = q.data.UnitType,
                                InvoiceText = q.data.InvoiceText,
                                IsActive = q.data.IsActive,
                                IsDefault = false
                            }).ToListAsync();
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="variableID"></param>
        /// <param name="machineID"></param>
        /// <param name="profileName"></param>
        /// <returns></returns>
        public async Task<ICollection<SimpleMaterialData>> GetByVariableAndProfile(int variableID, int? machineID, string profileName)
        {
            DropDownListValues listValues = await AssemblyHelper.GetListValues<DropDownListValues>(ctx, logger, BID, variableID, machineID, profileName);

            if (listValues == null)
                return new List<SimpleMaterialData>();

            return await GetByIDsAndCategoriesAsync(listValues.Components?.Select(c => c.ID.Value).ToList(), listValues.Categories?.Select(c => c.ID.Value).ToList());
        }

        /// <summary>
        /// Links or Unlinks a Material with a Material Category
        /// </summary>
        /// <param name="materialDataID">Material's ID</param>
        /// <param name="materialCategoryID">Material Category's ID</param>
        /// <param name="isLinked">If it is to link or unlink</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkMaterialCategory(int materialDataID, short materialCategoryID, bool isLinked)
        {
            /*          
                @BID
                @MaterialCategoryID
                @MaterialDataID
                @IsLinked
                @Result Output
            */
            try
            {
                SqlParameter Result = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                object[] myParams = {
                    new SqlParameter("@BID", this.BID),
                    new SqlParameter("@MaterialCategoryID", materialCategoryID),
                    new SqlParameter("@MaterialDataID", materialDataID),
                    new SqlParameter("@IsLinked", isLinked),
                    Result
                };

                int affectedRows = await ctx.Database.ExecuteSqlRawAsync(
                    "EXEC dbo.[Part.Material.Category.Action.LinkMaterial] @BID, @MaterialCategoryID, @MaterialDataID, @IsLinked, @Result Output;",
                    parameters: myParams);

                if (affectedRows > 0)
                {
                    await QueueIndexForModel(materialCategoryID);

                    base.DoGetRefreshMessagesOnUpdate(new MaterialData()
                    {
                        ClassTypeID = Convert.ToInt32(ClassType.Material),
                        ID = Convert.ToInt16(materialDataID),
                        BID = this.BID,
                    });

                    return new EntityActionChangeResponse()
                    {
                        Id = materialCategoryID,
                        Message = $"Successfully {(isLinked ? "linked" : "unlinked")} MaterialCategory:{materialCategoryID} to MaterialData:{materialDataID}",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = materialCategoryID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }

            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        internal async Task<List<MaterialData>> GetWithFiltersAsync(MaterialFilter filters)
        {
            if (filters != null && filters.IsActive.HasValue)
            {
                return await this.GetWhere(filters.WherePredicates());
            }
            else
                return await this.GetAsync(null);
        }

        #region Overrides
        /// <summary>
        /// Gets the Child Associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<MaterialData, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<MaterialData, int>[0];
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Get includes for model
        /// </summary>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        internal override void DoBeforeValidate(MaterialData model)
        {
            var unitInfoList = UnitInfo.InfoListByUnitType(model.UnitType).Select(x => x.Unit).ToList();
            var lengthUnits = UnitInfo.InfoListByUnitType(UnitType.Length).Select(x => x.Unit).ToList();
            var weightUnits = UnitInfo.InfoListByUnitType(UnitType.Weight_Mass).Select(x => x.Unit).ToList();
            var volLiquidUnits = UnitInfo.InfoListByUnitType(UnitType.Volume_Liquid).Select(x => x.Unit).ToList();
            
            if (model.Length?.Unit != null && model.Width?.Unit != null && model.Height?.Unit != null)
            {
                if (lengthUnits.IndexOf(model.Length.Unit) == -1)
                    throw new InvalidOperationException("LengthUnit is not a Unit of UnitType Length.");
                if (lengthUnits.IndexOf(model.Width.Unit) == -1)
                    throw new InvalidOperationException("WidthUnit is not a Unit of UnitType Length.");
                if (lengthUnits.IndexOf(model.Height.Unit) == -1)
                    throw new InvalidOperationException("HeightUnit is not a Unit of UnitType Length.");
            }

            if (model.Weight?.Unit != null && weightUnits.IndexOf(model.Weight.Unit) == -1)
                throw new InvalidOperationException("WeightUnit is not a Unit of UnitType Weight/Mass.");

            if (model.Volume?.Unit != null && volLiquidUnits.IndexOf(model.Volume.Unit) == -1)
                throw new InvalidOperationException("VolumeUnit is not a Unit of UnitType Volume Liquid.");

            switch (model.UnitType)
            {
                case UnitType.Length:
                    if (model.Length == null && model.Length?.Unit == null)
                        throw new InvalidOperationException("Length and LengthUnit are required.");
                    break;
                case UnitType.Area:
                    if (model.Length == null && model.Length?.Unit == null &&
                        model.Width == null && model.Width?.Unit == null)
                        throw new InvalidOperationException("Length, LengthUnit, Width and WidthUnit are required.");
                    break;
                case UnitType.Volume_Solid:
                    if (model.Length == null && model.Length?.Unit == null &&
                        model.Width == null && model.Width?.Unit == null &&
                        model.Height == null && model.Height?.Unit == null)
                        throw new InvalidOperationException("Length, LengthUnit, Width, WidthUnit, Height, and HeightUnit are required.");
                    break;
                case UnitType.Volume_Liquid:
                    if (model.Volume == null && model.Volume?.Unit == null) 
                        throw new InvalidOperationException("Volume and VolumeUnit are required.");
                    break;
                case UnitType.Weight_Mass:
                    if (model.Weight == null && model.Weight?.Unit == null)
                        throw new InvalidOperationException("Weight and WeightUnit are required.");
                    break;
                default: break;
            }
        }

        /// <summary>
        /// Material Service Override of DoBeforeCloneAsync
        /// </summary>
        /// <param name="clone">Cloned MaterialData</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(MaterialData clone)
        {
            //var newName = GetNextClonedName(clone.Name, t => t.Name);
            var newName = GetNextClonedName(ctx.Set<MaterialData>().AsNoTracking().Where(t => t.BID == BID && t.Name.StartsWith(t.Name)).ToList(), clone.Name, t => t.Name);

            if (clone.InvoiceText != null && clone.InvoiceText.Equals(clone.Name))
            {
                clone.InvoiceText = newName;
            }

            clone.Name = newName;

            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>        
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(MaterialData oldModel, MaterialData newModel, string connectionID)
        {
            await base.DoAfterUpdateAsync(oldModel, newModel, connectionID);
            await this.LinkOrUnlinkCategories(newModel, false);
        }

        /// <summary>
        /// Material Service Override of DoAfterCloneAsync
        /// </summary>
        /// <param name="clonedFromID">Cloned MaterialData ID</param>
        /// <param name="clone">Cloned MaterialData</param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(int clonedFromID, MaterialData clone)
        {
            //Copy Documents Bucket
            DocumentManager client = base.GetDocumentManager(clonedFromID, ClassType.Material, BucketRequest.Documents);
            await client.CloneAllBucketBlobsAsync(clone.ID, Bucket.Documents, includeSpecialFolderBlobName: true);

            // Create the category and material data
            var materialCategoryLinks = ctx.MaterialCategoryLink.Where( categoryLink => categoryLink.PartID == clonedFromID).ToList();

            if (materialCategoryLinks != null && materialCategoryLinks.Count() > 0)
            {
                foreach (var link in materialCategoryLinks)
                {
                    link.PartID = clone.ID;
                    ctx.MaterialCategoryLink.Add(link);
                }

                ctx.SaveChanges();
            }

            await CloneCustomFields(clonedFromID, clone.ID);

            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        /// <summary>
        /// Before create, makes sure that InventoryAccountID defaults to an Inventory account
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(MaterialData newModel, Guid? tempGuid = null)
        {
            if (!newModel.TrackInventory)
            {
                //https://corebridge.atlassian.net/browse/END-967
                newModel.InventoryAccountID = 1510;
            }

            if (newModel.SimpleMaterialCategories != null)
            {
                if (newModel.MaterialCategoryLinks == null)
                {
                    newModel.MaterialCategoryLinks = new HashSet<MaterialCategoryLink>();
                }
                foreach (var simpleItem in newModel.SimpleMaterialCategories)
                {
                    newModel.MaterialCategoryLinks.Add(new MaterialCategoryLink() { BID = this.BID, PartID = newModel.ID, CategoryID = simpleItem.ID });
                }
            }

            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }
        #endregion Overrides

        /// <summary>
        /// Returns a list of `MaterialData` that shares the same name as the `model`
        /// </summary>
        /// <param name="model">`MaterialData` model to check Name from</param>
        public List<MaterialData> ExistingNameOwners(MaterialData model)
        {
            return this.ctx.MaterialData
                .Where(material =>
                        material.BID == this.BID
                        && material.Name.Trim().Equals(model.Name.Trim())
                        && material.ID != model.ID)
                .ToList();
        }

        /// <summary>
        /// Maps MaterialData item
        /// </summary>
        /// <param name="item">MaterialData item</param>
        /// <param name="includeExpander">Includes expander</param>
        public override async Task MapItem(MaterialData item, IExpandIncludes includeExpander = null)
        {
            var includes = includeExpander?.GetIncludes();
            if (includes != null)
            {
                if (item.MaterialCategoryLinks!=null)
                {
                    switch (includes[MaterialDataIncludes.MaterialCategoriesKey].Level)
                    {
                        case IncludesLevel.Simple:
                            item.SimpleMaterialCategories = await Task.FromResult(item.MaterialCategoryLinks
                                                            .Select(link=>new SimpleMaterialCategory(){
                                                                BID = link.MaterialCategory.BID,
                                                                ID = link.MaterialCategory.ID,
                                                                ClassTypeID = link.MaterialCategory.ClassTypeID,
                                                                IsActive = link.MaterialCategory.IsActive,
                                                                IsDefault = false,
                                                                DisplayName = link.MaterialCategory.Name                                                        
                                                            }).ToList());
                            break;
                        case IncludesLevel.Full:
                            item.MaterialCategories = await Task.FromResult(item.MaterialCategoryLinks.Select(link=>link.MaterialCategory).ToList());
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<MaterialData> WherePrimary(IQueryable<MaterialData> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// UnlinkAll by Material Category ID
        /// </summary>
        public async Task UnlinkAllCategoryAsync(int ID)
        {
            var existingLinks = this.ctx.MaterialCategoryLink.Where(link => link.PartID == ID);
            if (existingLinks.Count() > 0)
            {
                foreach (var link in existingLinks)
                {
                    this.ctx.MaterialCategoryLink.Remove(link);
                }
                await ctx.SaveChangesAsync();
            }
        }


        /// <summary>
        /// Checks if the Material can be deleted
        /// </summary>
        /// <param name="id">Material ID</param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(int id)
        {
            MaterialData material = await this.GetAsync(id);
            if (material == null)
            {
                return new BooleanResponse()
                {
                    Message = "Material Not Found",
                    Success = false
                };
            }
            int linkedMaterialVariables = this.ctx.AssemblyVariable.Where(x => x.BID == this.BID && x.LinkedMaterialID == id).Count();
            if (linkedMaterialVariables > 0)
                return new BooleanResponse()
                {
                    Message = "Material is associated with one or more Variables.",
                    Success = true,
                    Value = false
                };

            // is linked to an order is determined how?

            return new BooleanResponse()
            {
                Message = "Material can be deleted.",
                Success = true,
                Value = true
            };
        }

        private async Task<bool> CloneCustomFields(int oldID, int newID)
        {
            try
            {
                CustomFieldService customFieldService = new CustomFieldService(this.ctx, this.logger, this.rtmClient);
                var customFieldList = customFieldService.ReadCustomFields(this.BID, (int)ClassType.Material, oldID, new CustomFieldValueFilters());
                customFieldService.UpdateCustomFields(this.BID, (int)ClassType.Material, newID, customFieldList.ToArray());
                return true;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Clone of Material Custom Fields failed", e);
                return false;
            }
        }

        /// <summary>
        /// LinkOrUnlinkCategories
        /// </summary>
        /// <param name="material"></param>
        /// <param name="indexModel"></param>
        /// <returns></returns>
        protected async Task LinkOrUnlinkCategories(MaterialData material, bool indexModel = true)
        {
            if(material.SimpleMaterialCategories != null)
            {
                var catSvc = this._matCatSvc.Value;
                var oldCategoryIDs = await this.ctx.MaterialCategoryLink
                                    .AsNoTracking()
                                    .Where(cat => cat.BID==this.BID &&
                                        cat.PartID == material.ID
                                    )
                                    .Select(x => x.CategoryID)
                                    .ToArrayAsync();

                await this.LinkOrUnlinkCategories(catSvc.LinkMaterial, material.ID, oldCategoryIDs, material.SimpleMaterialCategories.Select(y => y.ID).ToArray(), indexModel);
            }
        }          
    }
}
