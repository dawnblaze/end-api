﻿using Endor.AzureStorage;
using Endor.DNSManagement;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Service that wraps utilizes data storage layer and is accessed primarily from Controller layer
    /// </summary>
    public class SSLCertificateService : AtomCRUDService<SSLCertificateData, short>
    {
        /// <summary>
        /// object to manage dns settings in Azure
        /// </summary>
        public IDNSManager DNSManager;

        /// <summary>
        /// Constructs a new SSLCertificateService with a number of injeced parameters
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public SSLCertificateService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) :
            base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<SSLCertificateData, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<SSLCertificateData, short>[]
            {
            };
        }

        /// <summary>
        /// Occurs before the create hits the database
        /// </summary>
        /// <param name="newModel">New SSL Certificate to be created</param>
        /// <param name="tempGuid">temporary guid to aid in saving related objects</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(SSLCertificateData newModel, Guid? tempGuid = null)
        {
            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// Builds a query with primary filters
        /// </summary>
        /// <param name="query">SSLCertificateData query</param>
        /// <param name="ID">SSLCertificateData ID</param>
        /// <returns></returns>
        public override IQueryable<SSLCertificateData> WherePrimary(IQueryable<SSLCertificateData> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        internal override void DoBeforeValidate(SSLCertificateData newModel)
        {
        }
        
        /// <summary>
        /// Executes after a successful SSLCertificateData creation
        /// </summary>
        /// <param name="model">SSLCertificateData</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(SSLCertificateData model, Guid? tempGuid)
        {
            await base.DoAfterAddAsync(model, tempGuid);
        }
        
        /// <summary>
        /// Returns an array of SimpleList SSLCertificates
        /// </summary>
        /// <returns></returns>
        public async Task<SimpleListItem<short>[]> GetSimpleList()
        {
            List<SimpleListItem<short>> simpleList = new List<SimpleListItem<short>>();
            var sslCertificates = await ctx.Set<SSLCertificateData>().Where(m => m.BID == BID).ToListAsync();

            foreach(var sslCert in sslCertificates)
            {
                var sslThumbprintString = !string.IsNullOrWhiteSpace(sslCert.Thumbprint) ? $" - {sslCert.Thumbprint.ToUpperInvariant()}" : "";
                simpleList.Add(new SimpleListItem<short>
                {
                    BID = this.BID,
                    ID = sslCert.ID,
                    ClassTypeID = sslCert.ClassTypeID,
                    DisplayName = $"{sslCert.CommonName}{sslThumbprintString}",
                    IsActive = sslCert.ValidToDT > DateTime.UtcNow
                });
            }

            return simpleList.ToArray();
        }

        /// <summary>
        /// Downloads certificate from blob storage, validates password,
        /// and populates certificateData object properties
        /// </summary>
        /// <param name="certificateData"></param>
        /// <returns></returns>
        public async Task<IActionResult> ProcessCertificate(SSLCertificateData certificateData)
        {
            const string pwdKey = "metadata1";
            const string incorrectPasswordError = "The specified network password is not correct";

            var certFileName = certificateData.FileName;
            try
            {
                var sClient = new EntityStorageClient((await this.cache.Get(this.BID)).StorageConnectionString, this.BID);
                CloudBlockBlob blob = await sClient.GetBlockBlob(StorageBin.Permanent, Bucket.Documents, new DMID { ctid = (int)ClassType.Business, id = this.BID }, certFileName);

                //Blob must be downloaded before metadata becomes available
                byte[] certificateBytes;
                using (var ms = new MemoryStream())
                {//Download certificate from blob to memory
                    await blob.DownloadToStreamAsync(ms);
                    certificateBytes = ms.ToArray();
                }

                //Check for password in metadata
                if (blob.Metadata.TryGetValue(pwdKey, out string pwd) && !string.IsNullOrWhiteSpace(pwd))
                    pwd = blob.Metadata[pwdKey];
                else
                    throw new Exception("SSL Certificate Password is required");

                SecureString securePassword = new NetworkCredential("", pwd).SecurePassword;

                X509Certificate2 cert;
                try
                {//Attempt to open certificate with password
                    cert = new X509Certificate2(certificateBytes, securePassword);
                }
                catch (CryptographicException e)
                {
                    if (e.Message.Equals(incorrectPasswordError))
                    {//Incorrect Password
                        throw new Exception("Incorrect SSL Password Supplied");
                    }

                    throw new Exception("Error validating certificate using the password supplied");
                }

                if (cert.Subject.Contains('*'))
                    certificateData.CanHaveMultipleDomains = true;

                if(!certificateData.CanHaveMultipleDomains)
                {
                    foreach (var extension in cert.Extensions)
                    {
                        AsnEncodedData asndata = new AsnEncodedData(extension.Oid, extension.RawData);

                        string formattedString = asndata.Format(true);
                        if (!string.IsNullOrWhiteSpace(formattedString))
                        {
                            certificateData.CanHaveMultipleDomains = true;
                            break;
                        }
                    }
                }
                
                Result res = await DNSManager.RegisterSSLAsync(certificateData.CommonName, certificateBytes, pwd, certificateData.FileName);
                if (!res.Success)
                    throw new Exception("Error trying to register SSL. - " + res.Message);

                //Get info from cert
                certificateData.Thumbprint  = cert.Thumbprint;
                certificateData.ValidFromDT = cert.NotBefore;
                certificateData.ValidToDT   = cert.NotAfter;
                certificateData.CommonName  = cert.FriendlyName;
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }

            return new OkObjectResult(certificateData);
        }
    }
}
