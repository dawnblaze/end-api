﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// TimeCard Service
    /// </summary>
    public class TimeCardService : AtomCRUDService<TimeCard, short>
    {
        /// <summary>
        /// Constructs a TimeCard service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public TimeCardService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Returns TimeCard based on filters supplied in
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<List<TimeCard>> GetWithFiltersAsync(TimeCardFilter filters)
        {
            if (filters != null && filters.HasFilters)
            {
                if (filters.IncludeDetails.HasValue && filters.IncludeDetails.Value == true)
                {
                    return await ctx.TimeCard
                        .Where(tc => tc.BID == BID)
                        .WhereAll(filters.WherePredicates())
                        .Include("TimeCardDetails")
                        .Include("TimeCardDetails.TimeClockActivity")
                        .Include("TimeCardDetails.OrderItemStatus")
                        .Include("TimeCardDetails.TimeClockBreak")
                        .ToListAsync();
                }

                return await this.GetWhere(filters.WherePredicates());
            }

            return await this.GetAsync(filters);
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// Maps a specific item.
        /// </summary>
        /// <param name="item">Item to map</param>
        /// <param name="includes">Child/Parent includes</param>
        public override Task MapItem(TimeCard item, IExpandIncludes includes = null)
        {

            return Task.CompletedTask;
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<TimeCard, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<TimeCard, short>[0];
        }

        internal override void DoBeforeValidate(TimeCard newModel) { }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<TimeCard> WherePrimary(IQueryable<TimeCard> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Executes after a successful TimeCard creation
        /// </summary>
        /// <param name="newModel">TimeCard</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(TimeCard newModel, Guid? tempGuid = null)
        {

            await ctx.SaveChangesAsync();
            await base.DoAfterAddAsync(newModel, tempGuid);
        }
    }
}
