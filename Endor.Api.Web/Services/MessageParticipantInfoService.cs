﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;

namespace Endor.Api.Web.Services
{
    /// <inheritdoc />
    /// <summary>
    /// MessageBodyService
    /// </summary>
    public class MessageParticipantInfoService : AtomCRUDService<MessageParticipantInfo, int>
    {
        /// <inheritdoc />
        /// <summary>
        /// constructor
        /// </summary>
        public MessageParticipantInfoService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <inheritdoc />
        /// <summary>
        /// GetChildAssociations
        /// </summary>
        protected override IChildServiceAssociation<MessageParticipantInfo, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<MessageParticipantInfo, int>[] { };
        }

        /// <inheritdoc />
        /// <summary>
        /// DoBeforeValidate
        /// </summary>
        internal override void DoBeforeValidate(MessageParticipantInfo newModel)
        {
            
        }

        /// <inheritdoc />
        /// <summary>
        /// GetIncludes
        /// </summary>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return new string[] { };
        }

        /// <inheritdoc />
        /// <summary>
        /// WherePrimary
        /// </summary>
        public override IQueryable<MessageParticipantInfo> WherePrimary(IQueryable<MessageParticipantInfo> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }
    }
}
