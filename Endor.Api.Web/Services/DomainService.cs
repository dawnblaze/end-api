﻿using Endor.Api.Web.Classes;
using Endor.DNSManagement;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Domain Service
    /// </summary>
    public class DomainService : AtomCRUDService<DomainData, short>
    {
        /// <summary>
        /// object to manage dns settings in Azure
        /// </summary>
        public IDNSManager DNSManager;

        /// <summary>
        /// domain svc ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DomainService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// default include property strings
        /// </summary>
        public override string[] IncludeDefaults => new[] {"SSLCertificate"};

        /// <summary>
        /// returns child associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<DomainData, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<DomainData, short>[0];
        }

        /// <summary>
        /// Deletes the domain 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        async protected override Task DoAfterDeleteAsync(DomainData model)
        {
            await DNSManager.DeleteAsync(model.Domain);
        }

        internal override void DoBeforeValidate(DomainData newModel)
        {
            //noop
        }

        /// <summary>
        /// Adds hostname to Azure app service before saving the database record
        /// </summary>
        /// <param name="model">Custom Domain Model of type DomainData</param>
        /// <returns>BooleanResponse Object, Success and Value Properties Indicate the Result</returns>
        public async Task<BooleanResponse> InstallCustomDomain(DomainData model)
        {
            BooleanResponse result = new BooleanResponse();

            try
            {
                Result addResult = await DNSManager.AddHostNameAsync(model.Domain);

                result.Success = addResult.Success;
                result.Value = addResult.Success;
                string msgPrefix = result.Success ? "Success: " : "Failed: ";

                if(addResult.Message.ToLower().Contains("success") || addResult.Message.ToLower().Contains("fail"))
                    result.Message = $"{addResult.Message}";
                else
                    result.Message = $"{msgPrefix}{addResult.Message}";
            }
            catch (Exception e)
            {
                result.Message = $"Failed: {e.Message}";
                result.Success = false;
                result.Value = false;
            }

            return result;
        }

        /// <summary>
        /// gets default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// returns clause for filtering on primary ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns> 
        public override IQueryable<DomainData> WherePrimary(IQueryable<DomainData> query, short ID)
        {
            return query.WherePrimary(BID, ID);
        }

        /// <summary>
        /// Returns Domains based on filters supplied in
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<List<DomainData>> GetWithFiltersAsync(DomainFilter filters)
        {
            if (filters != null && filters.HasFilters)
            {
                return await this.GetWhere(filters.WherePredicates());
            }

            return await this.GetAsync();
        }

        /// <summary>
        /// Tests the availability of a domain
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        public async Task<BooleanResponse> TestAvailability(string domain)
        {
            BooleanResponse resp;
            try
            {
                // Since the domain must be unique across all businesses, we will not filter on the BID.
                // Per a conversation with Ben, we will need to revisit this later, because it will 
                //  only catch duplicates within the same business database.
                bool domainExists = await ctx.Set<DomainData>().FirstOrDefaultAsync(d => d.Domain.ToLower() == domain.ToLower()) != null;

                if (!domainExists)
                {
                    //We still need to check that it doesn't exist in Azure
                    List<string> domainlist = new List<string>(new string[] { domain });
                    var result = await DNSManager.CheckAvailabilityAsync(domainlist);
                    domainExists = !result[domain];
                }

                if (domainExists)
                    resp = new BooleanResponse()
                    {
                        Success = true,
                        Value = false,
                        Message = $"Domain {domain} is no longer available.",
                    };
                else
                {
                    resp = new BooleanResponse()
                    {
                        Success = true,
                        Value = true,
                        Message = $"Domain {domain} is available.",
                    };
                }
            }
            catch (Exception err)
            {
                resp = new BooleanResponse()
                {
                    Success = false,
                    Value = false,
                    ErrorMessage = err.ToString(),
                };
            }

            return await Task.FromResult(resp);
        }

        /// <summary>
        /// Sets a given domain to be the default.
        /// And sets the current default to no longer be the default
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> SetDefault(short ID, string connectionID)
        {
            EntityActionChangeResponse resp = null;

            try
            {
                DomainData newDefault = await GetAsync(ID);
                if (newDefault == null)
                    return new EntityActionChangeResponse()
                    {
                        Message = "Entity not found",
                        ErrorMessage = "Entity not found",
                        Success = false,
                        Id = Convert.ToInt32(ID)
                    };

                if (newDefault.IsDefault)
                {
                    resp = new EntityActionChangeResponse()
                    {
                        Message = "Unable to update default domain.",
                        ErrorMessage = "Unable to update default domain.",
                        Success = false,
                        Id = Convert.ToInt32(ID)
                    };
                }

                else
                {
                    List<DomainData> currentDefaults = await GetWhere(d => d.IsDefault);

                    foreach (var def in currentDefaults)
                    {
                        def.IsDefault = false;
                        await this.UpdateAsync(def, connectionID);
                    }

                    newDefault.IsDefault = true;
                    var upResp = await this.UpdateAsync(newDefault, connectionID);

                    resp = new EntityActionChangeResponse()
                    {
                        Message = $"{upResp.Domain} updated to be the default.",
                        Success = true,
                        Id = Convert.ToInt32(ID)
                    };
                }
            }
            catch (Exception ex)
            {
                resp = new EntityActionChangeResponse()
                {
                    Message = ex.Message,
                    ErrorMessage = ex.Message,
                    Success = false,
                    Id = Convert.ToInt32(ID)
                };
            }

            return resp;
        }

        /// <summary>
        /// Attempts to change the status of a domain
        /// </summary>
        /// <param name="ID">Domain's ID</param>
        /// <param name="active">Active status to set to</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public override async Task<EntityActionChangeResponse<short>> SetActive(short ID, bool active, string connectionID)
        {
            EntityActionChangeResponse<short> resp = null;

            try
            {
                DomainData domain = await GetAsync(ID);
                if (domain == null)
                    return new EntityActionChangeResponse<short>()
                    {
                        Message = "Entity not found",
                        ErrorMessage = "Entity not found",
                        Success = false,
                        Id = ID
                    };

                bool statusUpdated = false;

                if (active)
                {
                    if (domain.Status == DomainStatus.Inactive)
                    {
                        domain.Status = DomainStatus.Pending;
                        statusUpdated = true;
                    }
                }

                else if (domain.Status != DomainStatus.Inactive)
                {
                    domain.Status = DomainStatus.Inactive;
                    statusUpdated = true;
                }

                if (statusUpdated)
                {
                    DomainData upResp = await this.UpdateAsync(domain, connectionID);

                    resp = new EntityActionChangeResponse<short>()
                    {
                        Message = $"{upResp.Domain} status updated to {upResp.Status}.",
                        Success = true,
                        Id = ID
                    };
                }
                else
                {
                    resp = new EntityActionChangeResponse<short>()
                    {
                        Message = "Unable to update domain status.",
                        ErrorMessage = "Unable to update domain status.",
                        Success = false,
                        Id = ID
                    };
                }
            }
            catch (Exception ex)
            {
                resp = new EntityActionChangeResponse<short>()
                {
                    Message = ex.Message,
                    ErrorMessage = ex.Message,
                    Success = false,
                    Id = ID
                };
            }

            return resp;
        }

        /// <summary>
        /// Refreshes the Domain's related SSL Certificate Validation
        /// </summary>
        /// <param name="ID">DomainData ID</param>
        /// <returns></returns>
        public async Task<GenericResponse<DomainData>> RefreshSslStatus(short ID)
        {
            DomainData domain = await GetAsync(ID);
            if (domain == null)
            {
                return new GenericResponse<DomainData>()
                {
                    ErrorMessage = $"Domain with id `{ID}` not found.",
                    Success = false
                };
            }

           if (domain.SSLCertificate == null)
            {
                return new GenericResponse<DomainData>()
                {
                    ErrorMessage = $"Domain with id `{ID}` has no associated SSL Certiciate.",
                    Success = false
                };
            }

            domain.SSLLastAttemptDT = DateTime.UtcNow;
            domain.SSLLastAttemptResult = "Failed, unable to resolve.";
            var sslCert = domain.SSLCertificate;

            try
            {
                var verifyResp = (await this.DNSManager.VerifySSLAsync(new List<string>() { domain.Domain }))[0];
                var cert = verifyResp.Certificate;

                if (!verifyResp.IsValid || cert == null)
                {
                    this.ctx.DomainData.Update(domain);
                    await this.ctx.SaveChangesAsync();

                    return new GenericResponse<DomainData>()
                    {
                        Data = domain,
                        ErrorMessage = $"Failed, unable to resolve.",
                        Success = false
                    };
                }

                sslCert.ValidFromDT = DateTime.Parse(cert.GetEffectiveDateString());
                sslCert.ValidToDT = DateTime.Parse(cert.GetExpirationDateString());
                sslCert.ModifiedDT = DateTime.UtcNow;

                domain.SSLCertificate = sslCert;
                domain.SSLLastAttemptResult = (sslCert.ValidToDT >= sslCert.ModifiedDT) ? "Success.": "Expired.";

                this.ctx.DomainData.Update(domain);
                this.ctx.SSLCertificateData.Update(sslCert);
                await this.ctx.SaveChangesAsync();

#warning add connection ID in?
                await this.DoAfterUpdateAsync(domain, domain, null);

                return new GenericResponse<DomainData>()
                {
                    Data = domain,
                    Message = $"SSL Certificate validated.",
                    Success = true
                };
            }
            catch (Exception ex)
            {
                this.ctx.DomainData.Update(domain);
                await this.ctx.SaveChangesAsync();

                await logger.Error(BID, ex.Message, ex);

                return new GenericResponse<DomainData>()
                {
                    ErrorMessage = $"Failed, unable to resolve.",
                    Success = false
                };
            }
        }

        /// <summary>
        /// Validates the Dns and updates the Domain record
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public async Task<GenericResponse<DomainRefreshDnsResponse>> RefreshDns(short ID, EndorOptions options)
        {
            bool success;
            DomainRefreshDnsResponse resp;
            string message;

            try
            {
                DomainData domain = await GetAsync(ID);
                if (domain == null)
                {
                    return new GenericResponse<DomainRefreshDnsResponse>()
                    {
                        ErrorMessage = $"Domain not found.",
                        Success = false
                    };
                }

                bool DNSVerified = await DNSManager.VerifyDNSAsync(domain.Domain);

                var verificationAttemptDT = DateTime.UtcNow;

                domain.DNSLastAttemptDT = verificationAttemptDT;
                domain.ModifiedDT = verificationAttemptDT;

                if (DNSVerified)
                {
                    //DNS Verified, Attempt to install Domain Name in Azure App Service
                    BooleanResponse installResult = await this.InstallCustomDomain(domain);
                    if (installResult.Success)
                    {
                        domain.DNSVerifiedDT = verificationAttemptDT;
                        domain.DNSLastAttemptResult = installResult.Message;
                        domain.Status = DomainStatus.Online;
                        success = true;
                        message = null;
                    }
                    else
                    {
                        domain.DNSLastAttemptResult = installResult.Message;
                        domain.Status = DomainStatus.Inactive;
                        success = false;
                        message = "Installation Failed.";
                    }

                    resp = new DomainRefreshDnsResponse()
                    {
                        Domain = domain.Domain,
                        DNSLastAttemptDT = verificationAttemptDT,
                        DNSLastAttemptResult = domain.DNSLastAttemptResult,
                        DNSVerifiedDT = domain.DNSVerifiedDT,
                    };
                }
                else
                {
                    domain.DNSLastAttemptResult = $"Domain failed verification.";
                    domain.Status = DomainStatus.Inactive;

                    success = false;
                    message = "Verification failed.";
                    resp = new DomainRefreshDnsResponse()
                    {
                        Domain = domain.Domain,
                        DNSLastAttemptDT = verificationAttemptDT,
                        DNSLastAttemptResult = domain.DNSLastAttemptResult,
                    };
                }

                await ctx.SaveChangesAsync();

#warning END-2760 do we need connection ID to ignore refreshes here?
                await this.DoAfterUpdateAsync(domain, domain, null);

                return new GenericResponse<DomainRefreshDnsResponse>()
                {
                    Data = resp,
                    ErrorMessage = message,
                    Success = success
                };
            }
            catch (Exception ex)
            {
                return new GenericResponse<DomainRefreshDnsResponse>()
                {
                    ErrorMessage = ex.Message,
                    Success = false
                };
            }
        }
    }
}
