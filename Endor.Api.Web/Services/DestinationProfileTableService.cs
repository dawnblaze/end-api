﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Destination Profile Table Service
    /// </summary>
    public class DestinationProfileTableService : AtomCRUDService<DestinationProfileTable, short>,
        IDoBeforeCreateUpdateWithParent<DestinationProfileTable, DestinationProfile>
    {

        /// <summary>
        /// Destination Profile Table constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DestinationProfileTableService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        internal override void DoBeforeValidate(DestinationProfileTable newModel)
        {

        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "" };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">CustomFieldLayoutDefinition ID</param>
        /// <returns></returns>
        public override IQueryable<DestinationProfileTable> WherePrimary(IQueryable<DestinationProfileTable> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<DestinationProfileTable, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<DestinationProfileTable, short>[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public void DoBeforeCreateWithParent(DestinationProfileTable child, DestinationProfile parent)
        {
            child.ProfileID = parent.ID;
        }

        /// <summary>
        /// Fixes the missing values for IDs when updating specific item. 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        public void DoBeforeUpdateWithParent(DestinationProfileTable child, DestinationProfile oldParent, DestinationProfile newParent)
        {
            child.ProfileID = newParent.ID;

            child.BID = newParent.BID;
        }

        /// <summary>
        /// DoBeforeDeleteAsync
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override Task DoBeforeDeleteAsync(DestinationProfileTable model)
        {
            //model.Profile = null;
            model.Table = null;
            return base.DoBeforeDeleteAsync(model);
        }

    }
}
