using Endor.EF;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Endor.Tasks;
using Endor.Api.Web.Classes;
using Endor.Tenant;
using Endor.Logging.Client;
using Endor.RTM;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Service that handles CRUD ops on Origin data
    /// </summary>
    public class OriginService : AtomCRUDService<CrmOrigin, short>, ISimpleListableViewService<SimpleOriginData, short>
    {
        /// <summary>
        /// Simple List of Industries
        /// </summary>
        public DbSet<SimpleOriginData> SimpleListSet => ctx.SimpleOriginData;

        /// <summary>
        /// Constructs a new OriginService with a number of injected parameters
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OriginService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "ParentOrigin", "Companies", "ChildOrigins" };

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Overrides the mapping of the whole collection result
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override async Task<List<CrmOrigin>> MapCollection(List<CrmOrigin> collection, IExpandIncludes includes = null)
        {
            var optSvc = new OptionService(this.ctx, migrationHelper);
            bool lockFirstLevel = false;
            GetOptionValueResponse govr = await optSvc.Get(null, "Association.Origin.LockFirstLevel", BID, null, null, null, null, null);

            if (govr == null || !govr.Success)
            {
                return collection;
            }

            // Check Option Value, if true and isTopLevel, set IsLock = true
            if (bool.TryParse(govr.Value.Value, out bool result) && result)
            {
                lockFirstLevel = true;
            }
            else if (int.TryParse(govr.Value.Value, out int intResult) && intResult > 0)
            {
                lockFirstLevel = true;
            }
            else
            {
                lockFirstLevel = false;
            }

            collection.ForEach(async (item) => await this.MapOrigin(item, includes, lockFirstLevel));

            return collection;
        }

        private async Task MapOrigin(CrmOrigin item, IExpandIncludes includes = null, bool isFirstLevelLocked = false)
        {
            await base.MapItem(item, includes);

            if (item.IsTopLevel.GetValueOrDefault(false) && isFirstLevelLocked)
            {
                item.IsLocked = true;
            }
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<CrmOrigin, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<CrmOrigin, short>[]
            {
                CreateChildAssociation<OriginService, CrmOrigin, short>((a)=>a.ChildOrigins)
            };
        }

        /// <summary>
        /// Sets the origin (and any applicable parents/children) to active or inactive.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="active"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public override async Task<EntityActionChangeResponse<short>> SetActive(short id, bool active, string connectionID)
        {
            try
            {
                EntityActionChangeResponse<short> result = await base.SetActive(id, active, connectionID);

                if (result.Success)
                {
                    CrmOrigin origin = this.ctx.CrmOrigin
                                               .Include(t => t.ParentOrigin)
                                               .Include(t => t.ChildOrigins)
                                               .FirstOrDefault(t => t.BID == this.BID && t.ID == id);

                    if (active && origin?.ParentOrigin != null && origin.ParentID != null)
                    {
                        await base.SetActive(origin.ParentID.Value, true, connectionID);
                    }

                    if (!active && origin?.ChildOrigins != null)
                    {
                        foreach (CrmOrigin ind in origin.ChildOrigins)
                            await base.SetActive(ind.ID, false, connectionID);
                    }

                    return new EntityActionChangeResponse<short>()
                    {
                        Id = id,
                        Message = "Successfully set Origin's Active status to " + (active ? "active" : "inactive"),
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        Id = id,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Id = id,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        /// <summary>
        /// set's the parent to the specified ID.  Set to NULL to clear.
        /// </summary>
        /// <param name="bid">Business Id</param>
        /// <param name="id">Origin Id</param>
        /// <param name="parentId">parent Origin Id</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> SetParent(short bid, int id, int? parentId)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                object[] myParams = {
                    new SqlParameter("@BID", bid),
                    new SqlParameter("@OriginID", id),
                    parentId.HasValue? new SqlParameter("@ParentID", parentId.Value):new SqlParameter("@ParentID", DBNull.Value),
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Origin.Action.SetParent] @BID, @OriginID, @ParentID;", parameters: myParams);
                if (rowResult > 0)
                {
                    await QueueIndexForModel(Convert.ToInt16(id));

                    return new EntityActionChangeResponse()
                    {
                        Id = id,
                        Message = "Successfully set Origin's Parent.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = id,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = id,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        internal override void DoBeforeValidate(CrmOrigin newModel)
        {
        }

        internal async Task<SimpleOriginData[]> SimpleList(bool? isTopLevel, bool? isActive)
        {
            return await this.ctx.CrmOrigin
                .Where(t => t.IsTopLevel == (bool)(isTopLevel ?? t.IsTopLevel))
                .Where(t => t.IsActive == (bool)(isActive ?? t.IsActive))
                .Where(t => t.BID == BID)
                .Select(t => new SimpleOriginData()
                {
                    BID = t.BID,
                    ID = t.ID,
                    ClassTypeID = t.ClassTypeID,
                    IsActive = t.IsActive,
                    IsDefault = false,
                    DisplayName = t.Name,
                })
                .ToArrayAsync();
        }

        /// <summary>
        /// Implements the primary lookup filter
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<CrmOrigin> WherePrimary(IQueryable<CrmOrigin> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }
    }
}