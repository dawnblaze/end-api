﻿using Endor.EF;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Base Generic Service typed to a Model class, initialized with a ApiContext
    /// </summary>
    public abstract class BaseGenericService
    {
        /// <summary>
        /// ApiContext
        /// </summary>
        protected readonly ApiContext ctx;
        /// <summary>
        /// Migration Helper
        /// </summary>
        protected readonly IMigrationHelper migrationHelper;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="migrationHelper">Migration Helper</param>
        protected BaseGenericService(ApiContext context, IMigrationHelper migrationHelper)
        {
            ctx = context;
            this.migrationHelper = migrationHelper;
            migrationHelper.MigrateDb(ctx);
        }
    }
}
