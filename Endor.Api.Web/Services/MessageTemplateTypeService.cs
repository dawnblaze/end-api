﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{   
    /*public class MessageTemplateTypeService
    {
        private readonly ApiContext _ctx;
        private readonly IMigrationHelper _migrationHelper;

        /// <summary>
        /// Business ID
        /// </summary>
        public short BID { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context">Api context</param>
        /// <param name="bid">BID</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public MessageTemplateTypeService(ApiContext context, short bid, IMigrationHelper migrationHelper)
        {
            this._ctx = context;
            this._migrationHelper = migrationHelper;
            this._migrationHelper.MigrateDb(_ctx);
            this.BID = bid;
        }

        internal async Task<List<SystemMessageTemplateType>> GetWithFiltersAsync(MessageTemplateTypeFilter filters)
        {
            if (filters != null)
                return await this._ctx.SystemMessageTemplateType.WhereAll(filters.WherePredicates()).ToListAsync();
            
            return await this._ctx.SystemMessageTemplateType.ToListAsync();
        }

        internal async Task<List<SimpleListItem<byte>>> GetSimpleListWithFiltersAsync(MessageTemplateTypeFilter filters)
        {
            List<SimpleListItem<byte>> simpleList = new List<SimpleListItem<byte>>();

            List<SystemMessageTemplateType> msgs = await this.GetWithFiltersAsync(filters);
            foreach(var msg in msgs)
            {
                simpleList.Add(new SimpleListItem<byte>
                {
                    BID = this.BID,
                    ID = msg.ID,
                    ClassTypeID = msg.AppliesToClassTypeID,
                    DisplayName = msg.Name,
                    IsActive = true
                });
            }

            return simpleList;
        }
    }*/
}
