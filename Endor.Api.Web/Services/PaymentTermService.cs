﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Pament Term Service
    /// </summary>
    public class PaymentTermService : AtomCRUDService<PaymentTerm, int>, ISimpleListableViewService<SimplePaymentTerm, int>
    {

        /// <summary>
        /// Constructs a Payment Term service with injected params
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public PaymentTermService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        #region Overrides
        /// <summary>
        /// Child Associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<PaymentTerm, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<PaymentTerm, int>[0];
        }

        /// <summary>
        /// Collects the array of includes to expand
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// PaymentTerm Service Override of DoBeforeCloneAsync
        /// </summary>
        /// <param name="clone">Cloned PaymentTerm</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(PaymentTerm clone)
        {
            //            clone.Name = clone.Name + " (Clone)";
            //clone.Name = GetNextClonedName(clone.Name, t => t.Name);
            clone.Name = GetNextClonedName(ctx.Set<PaymentTerm>().AsNoTracking().Where(t => t.BID == BID && t.Name.StartsWith(t.Name)).ToList(), clone.Name, t => t.Name);
            await base.DoBeforeCloneAsync(clone);
        }

        internal override void DoBeforeValidate(PaymentTerm newModel)
        {
        }
        #endregion Overrides

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="activeStatus"></param>
        /// <param name="connectionId"></param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse<int>> SetInactiveIfNotDefault(short id, bool activeStatus, string connectionId)
        {
            var paymentTermDefault =
                ctx.SystemOptionDefinition.FirstOrDefault(option =>
                    option.Name == "Accounting.PaymentTerm.DefaultID" && option.DefaultValue == id.ToString());

            if (paymentTermDefault == null)
            {
                return await SetActive(id, activeStatus, connectionId);
            }

            return new EntityActionChangeResponse()
            {
                Id = id,
                Message = "403 - Forbidden - \"Unable to Delete Default Payment Term",
                Success = false
            };
        }

        /// <summary>
        /// Checks if PaymentTerm ID is the default
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CheckIfDefault(int id)
        {
            var paymentTermDefault =
                ctx.SystemOptionDefinition.FirstOrDefault(option =>
                    option.Name == "Accounting.PaymentTerm.DefaultID" && option.DefaultValue == id.ToString());

            return paymentTermDefault != null;
        }

        /// <summary>
        /// Sets a Payment Term as the Default Payment Term for its Company
        /// </summary>
        /// <param name="id">Payment Term ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> SetDefault(short? id)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", SqlDbType.Int)
                    {Direction = ParameterDirection.Output};

                object[] myParams =
                {
                    new SqlParameter("@BID", this.BID),
                    new SqlParameter("@PaymentTermID", id ?? 0),
                    resultParam
                };

                await this.ctx.Database.ExecuteSqlRawAsync(
                    "EXEC [dbo].[Accounting.Payment.Term.Action.SetDefault] @BID, @PaymentTermID, @Result = @Result Output;",
                    myParams);

                int resultParamValue = (int) resultParam.Value;

                if (resultParamValue > 0)
                {
                    var messageVariable = (id == 0) ? $"ID: {id}" : "NULL";
                    return new EntityActionChangeResponse
                    {
                        Id = id ?? 0,
                        Message = $"Successfully set PaymentTerm ({messageVariable}) to Default.",
                        Success = true
                    };
                }

                return new EntityActionChangeResponse
                {
                    Id = id ?? 0,
                    Message = "No rows affected.",
                    Success = false
                };
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = id ?? 0,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// DbSet of SimplePaymentTerm for <see cref="ISimpleListableViewService{SLI, I}"/>
        /// </summary>
        public DbSet<SimplePaymentTerm> SimpleListSet => this.ctx.SimplePaymentTerm;

        internal async Task<List<PaymentTerm>> GetWithFiltersAsync(PaymentTermFilter filters)
        {
            if (filters != null)
                return await this.GetWhere(filters.WherePredicates());

            return await this.GetAsync(null);
        }

        /// <summary>
        /// Returns the query filtered by its primary key
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<PaymentTerm> WherePrimary(IQueryable<PaymentTerm> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }
    }
}
