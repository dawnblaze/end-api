using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Xml;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Custom Field Layout Definition Service
    /// </summary>
    public class CustomFieldLayoutDefinitionService : AtomCRUDService<CustomFieldLayoutDefinition, short>
    {
        private readonly Lazy<CustomFieldLayoutElementService> _customFieldLayoutElementService;


        /// <summary>
        /// /// Constructs a CustomFieldLayoutDefinition service
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CustomFieldLayoutDefinitionService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
            this._customFieldLayoutElementService = new Lazy<CustomFieldLayoutElementService>(() => new CustomFieldLayoutElementService(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper));
        }

        /// <summary>
        /// Returns JSON Collection of all CustomFieldLayoutDefinition Simplelist
        /// </summary>
        /// <returns></returns>
        public async Task<List<SimpleCustomFieldLayoutDefinition>> GetSimpleListAsync(Expression<Func<CustomFieldLayoutDefinition, bool>> predicate = null)
        {

            var query = this.ctx.CustomFieldLayoutDefinition.Where(x => true);
            if (predicate != null)
            {
                query = this.ctx.CustomFieldLayoutDefinition.Where(predicate);
            }

            return await query
                .Select(x => new SimpleCustomFieldLayoutDefinition()
                {
                    BID = x.BID,
                    ID = x.ID,
                    ClassTypeID = x.ClassTypeID,
                    IsActive = x.IsActive,
                    IsDefault = false,
                    DisplayName = x.Name,
                    AppliesToClassTypeID = x.AppliesToClassTypeID
                })
                .ToListAsync();
        }

        /// <summary>
        /// Gets a list of CustomFieldLayoutDefinition for the specified filters
        /// </summary>
        /// <param name="filters">CustomFieldLayoutDefinition Filters</param>
        /// <returns></returns>
        public async Task<List<CustomFieldLayoutDefinition>> GetAllWithFilters(CustomFieldLayoutDefinitionFilter filters)
        {
            return await this.GetWhere(filters.WherePredicates());
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "Elements", "Elements.Elements", "Elements.Elements.Elements", "Elements.Elements.Definition", "Elements.Elements.Elements.Definition" };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">CustomFieldLayoutDefinition ID</param>
        /// <returns></returns>
        public override IQueryable<CustomFieldLayoutDefinition> WherePrimary(IQueryable<CustomFieldLayoutDefinition> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<CustomFieldLayoutDefinition, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<CustomFieldLayoutDefinition, short>[]
            {
                CreateChildAssociation<CustomFieldLayoutElementService, CustomFieldLayoutElement, int>((d) => d.Elements),
            };
        }

        internal override void DoBeforeValidate(CustomFieldLayoutDefinition newModel)
        {

        }

        /// <summary>
        /// check if candelete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(short id)
        {
            //check first if !.IsSystem and !.IsAllTab
            //https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/763199517/CustomFieldLayout+API
            var layoutDef = await this.ctx.CustomFieldLayoutDefinition.FirstOrDefaultAsync(x => x.BID == this.BID && x.ID == id);

            return new BooleanResponse()
            {
                Message = $"CanDelete: {id}",
                Success = true,//success should only be false when errors are encountered
                Value = true
            };

            //if (layoutDef == null)
            //{
            //    return new BooleanResponse()
            //    {
            //        Message = $"Custom Field Layout Defintion ID = {id} not found",
            //        Success = true,//success should only be false when errors are encountered
            //        Value = false
            //    };
            //}

            //if (layoutDef.IsSystem != true && layoutDef?.IsAllTab != true)
            //{
            //    return new BooleanResponse()
            //    {
            //        Message = $"CanDelete: {id}",
            //        Success = true,//success should only be false when errors are encountered
            //        Value = true
            //    };
            //}
            //else
            //{
            //    return new BooleanResponse()
            //    {
            //        Value = false,
            //        Success = true,//success should only be false when errors are encountered
            //        Message = $"Both IsSystem and IsAllTab must be false. IsSystem: {layoutDef.IsSystem}, IsAllTab: {layoutDef.IsAllTab}."
            //    };
            //}
        }

        /// <summary>
        /// Before create
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(CustomFieldLayoutDefinition newModel, Guid? tempGuid = null)
        {
            if (newModel.IsSystem == true)
            {
                CustomFieldLayoutDefinition existingMainTab = await this.ctx.CustomFieldLayoutDefinition
                    .Where(x => x.BID == this.BID && x.AppliesToClassTypeID == newModel.AppliesToClassTypeID && x.IsSystem && x.IsActive && !x.IsAllTab && !x.IsSubTab)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();

                if (existingMainTab != null)
                {
                    throw new InvalidOperationException("Cannot create more than 1 Quick Custom Field.");
                }
            }

            if (newModel.Elements ==null)
                newModel.Elements = new HashSet<CustomFieldLayoutElement>() { };

            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// After create
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(CustomFieldLayoutDefinition newModel, Guid? tempGuid = null)
        {
            MapChildTree(newModel);
            await base.DoAfterAddAsync(newModel, tempGuid);
        }

        protected override async Task DoAfterUpdateAsync(CustomFieldLayoutDefinition oldModel, CustomFieldLayoutDefinition newModel, string connectionID)
        {
            MapChildTree(newModel);
            await base.DoAfterUpdateAsync(oldModel, newModel, connectionID);
        }

        /// <summary>
        /// Base abstract method called before deleting a record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(CustomFieldLayoutDefinition model)
        {
            await base.DoBeforeDeleteAsync(model);
        }


        /// <summary>
        /// Business logic on update. Base class handles ModifiedDT
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        public override async Task DoBeforeUpdateAsync(CustomFieldLayoutDefinition oldModel, CustomFieldLayoutDefinition newModel)
        {

            if (oldModel.IsAllTab && !newModel.IsAllTab) {
                //prevent AllTab layouts from being updated
                //and prevent existing layouts to be updated with .IsAllTab=true
                throw new Exception("Updating an AllTab layout to .IsAllTab=false is not permitted");
            }

            if (oldModel.IsSystem)
            {
                //prevent IsSystem layouts name and IsSystem bit from being updated
                if (!newModel.IsSystem)
                {
                    throw new Exception("Updating IsSystem bit to false on System layouts is not permitted");
                }
                if (oldModel.Name != newModel.Name)
                {
                    throw new Exception("Updating Name on System layout is not permitted");
                }
                if (oldModel.IsAllTab!=newModel.IsAllTab)
                {
                    throw new Exception("Updating IsAllTab bit on System layouts is not permitted");
                }
            }

            await base.DoBeforeUpdateAsync(oldModel, newModel);
        }
        
        /// <summary>
        /// update sort index of multiple layouts
        /// </summary>
        /// <param name="sortIndexRefs"></param>
        /// <returns></returns>
        public async Task<GenericResponse<ICollection<CustomFieldLayoutDefinition>>> SetSortIndexesAsync(List<IDSortIndexRef<short, short>> sortIndexRefs)
        {
            var IDlist = sortIndexRefs.Select(sref => sref.ID);
            var entitiesToUpdate = await this.ctx.CustomFieldLayoutDefinition
                                        .Where(cfl =>
                                            IDlist.Contains(cfl.ID)
                                            && cfl.BID == this.BID
                                        ).ToListAsync();

            foreach (var sortIndexRef in sortIndexRefs)
            {
                var entityToUpdate = entitiesToUpdate.FirstOrDefault(e => e.ID == sortIndexRef.ID);

                if (entityToUpdate.IsAllTab || entityToUpdate.IsSystem || entityToUpdate.IsSubTab == false)
                {
                    return new GenericResponse<ICollection<CustomFieldLayoutDefinition>>()
                    {
                        Message = "IsAllTab=true, IsSystem=true and IsSubTab=false are reserved for system generated layouts",
                        ErrorMessage = "IsAllTab=true, IsSystem=true and IsSubTab=false are reserved for system generated layouts",
                        Success = false,
                        Data = entitiesToUpdate
                    };
                }
                entityToUpdate.SortIndex = sortIndexRef.SortIndex;
            }
            await this.ctx.SaveChangesAsync();
            //return entitiesToUpdate;
            return new GenericResponse<ICollection<CustomFieldLayoutDefinition>>()
            {
                Message = "Success",
                ErrorMessage = "",
                Success = true,
                Data = entitiesToUpdate
            };
        }

        /// <summary>
        /// map collection, creating All Custom Fields section for all tab
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override async Task<List<CustomFieldLayoutDefinition>> MapCollection(List<CustomFieldLayoutDefinition> collection, IExpandIncludes includes = null)
        {
            var containerTypes = new AssemblyElementType[]{ 
                AssemblyElementType.Group,
                AssemblyElementType.Section,
                AssemblyElementType.ShowHideGroup,
                AssemblyElementType.SplitColumn
            };

            List<CustomFieldLayoutElement> allElements = collection.Where(x => !x.IsAllTab)
                            .SelectMany(x => x.Elements)
                            .Distinct()
                            .ToList();
            //remove nested containers from top level of layouts
            collection.ForEach(layout => layout.Elements = layout.Elements.Where(container => container.ParentID == null).ToList());

            //container for AllTab layouts are rendered only on the UI and not saved to database
            var allTab = collection?.FirstOrDefault(x => x.IsAllTab);

            if (allTab == null && collection.Count > 0)
            {
                allTab = new CustomFieldLayoutDefinition()
                {
                    Name = "All",
                    IsActive = true,
                    IsSubTab = true,
                    IsAllTab = true,
                    IsSystem = true,
                    ID = -1,
                    AppliesToClassTypeID = collection[0].AppliesToClassTypeID
                };
                collection.Add(allTab);
            }

            if (allTab != null)
            {
                List<CustomFieldLayoutElement> allTabGroupChildren = ctx.CustomFieldDefinition.WherePrimary(BID, (x) => 
                    x.AppliesToClassTypeID == allTab.AppliesToClassTypeID && 
                    x.IsActive && 
                    !containerTypes.Contains(x.ElementType)
                ).Select(x => new CustomFieldLayoutElement(){
                        BID = x.BID,
                        ClassTypeID = x.ClassTypeID,
                        ParentID = -2,
                        DataType = (short)x.DataType,
                        Definition = x,
                        DefinitionID = x.ID,
                        ID = x.ID,
                        ModifiedDT = x.ModifiedDT,
                        Name = x.Name,
                        ElementType = x.ElementType
                    }
                ).ToList();
                var allTabSectionChildren = new List<CustomFieldLayoutElement>() {
                    new CustomFieldLayoutElement()
                    {
                        Column = 0,
                        Name = "All Custom Fields",
                        BID = this.BID,
                        LayoutID = allTab.ID,
                        SortIndex = 0,
                        ID = -1,
                        ElementType = AssemblyElementType.Group,
                        ParentID = -2,
                        Elements = allTabGroupChildren
                    }
                };
                allTab.Elements = new CustomFieldLayoutElement[1]
                {
                    new CustomFieldLayoutElement()
                    {
                        Column = 2,
                        Name = "Section",
                        BID = this.BID,
                        LayoutID = allTab.ID,
                        SortIndex = 0,
                        ID = -2,
                        ElementType = AssemblyElementType.Section,
                        Elements = allTabSectionChildren
                    }
                };
            };

            collection = await base.MapCollection(collection, includes);

            return collection;
        }

        /// <summary>
        /// Maps properties in a single CustomFieldLayoutDefinition
        /// </summary>
        /// <param name="item"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override Task MapItem(CustomFieldLayoutDefinition item, IExpandIncludes includes = null)
        {
            if (item?.Elements?.Any() ?? false)
            {
                foreach (var child in item.Elements)
                    MapElement(child);
            }

            return base.MapItem(item, includes);
        }

        /// <summary>
        /// maps elements and child elements recursively
        /// </summary>
        /// <param name="ele"></param>
        protected void MapElement(CustomFieldLayoutElement ele)
        {
            if (ele?.Elements?.Any() ?? false)
            {
                foreach (var child in ele.Elements)
                    MapElement(child);
            }
        }

        /// <summary>
        /// Gets the old model
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task<CustomFieldLayoutDefinition> GetOldModelAsync(CustomFieldLayoutDefinition newModel)
        {
            var layout = await WherePrimary(ctx.CustomFieldLayoutDefinition.Include(x => x.Elements).AsNoTracking(), newModel.ID).FirstOrDefaultAsync();

            MapChildTree(layout);

            return layout;
        }

        internal static void MapChildTree(CustomFieldLayoutDefinition layout)
        {
            if (layout?.Elements?.Any() ?? false)
            {
                var childHash = layout.Elements.ToLookup(x => x.ParentID);

                foreach (CustomFieldLayoutElement element in layout.Elements)
                {
                    element.Elements = childHash[element.ID].OrderBy(y => y.Row).ToList();
                }
                layout.Elements = layout.Elements.Where(y => y.ParentID == null).OrderBy(y => y.Row).ToList();
            }
        }

        internal async Task RemoveElementsAsync(ICollection<CustomFieldLayoutElement> elements)
        {
            if (elements != null && elements.Count > 0)
            {
                foreach (CustomFieldLayoutElement el in elements)
                {
                    if (el.Elements != null && el.Elements.Count > 0)
                    {
                        await RemoveElementsAsync(el.Elements);
                    }
                    var e = await this.ctx.CustomFieldLayoutElement.FirstOrDefaultAsync(b => b.BID == BID && b.ID == el.ID);
                    if (e != null)
                    {
                        ctx.CustomFieldLayoutElement.Remove(e);
                    }
                }
            }
        }

        /// <summary>
        /// Force Delete
        /// </summary>
        /// <param name="ID">ID of the Custom Field Layout to Delete</param>
        public async Task<EntityActionChangeResponse> ForceDeleteLayout(short ID)
        {
            try
            {
                CustomFieldLayoutDefinition toDelete = await this.GetAsync(ID);

                if (toDelete == null)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "CustomFieldLayout not found"
                    };
                }
                List<RefreshEntity> refreshes = DoGetRefreshMessagesOnDelete(toDelete);
                await RemoveElementsAsync(toDelete.Elements);
                await ctx.SaveChangesAsync();
                toDelete = await this.GetAsync(ID);
                ctx.CustomFieldLayoutDefinition.Remove(toDelete);
                var save = ctx.SaveChanges();

                if (save > 0)
                {
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshes });
                    await taskQueuer.IndexModel(refreshes);
                    return new EntityActionChangeResponse()
                    {
                        Success = true,
                        Message = "Successfully Deleted"
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Nothing to delete"
                    };
                }
            }
            catch (DbUpdateException exc)
            {
                return new EntityActionChangeResponse()
                {
                    Success = false,
                    ErrorMessage = exc.InnerException.Message
                };
            }
            catch (Exception exc)
            {
                return new EntityActionChangeResponse()
                {
                    Success = false,
                    ErrorMessage = exc.ToString()
                };
            }
        }


    }
}