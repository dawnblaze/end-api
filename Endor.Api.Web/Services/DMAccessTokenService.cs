﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
//using Endor.Api.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{

    /// <summary>
    /// Create/Read/Update/Delete Service for DMAccessToken
    /// </summary>
    public class DMAccessTokenService : BaseCRUDService<DMAccessToken>
    {

        /// <summary>
        /// Constructs an AtomCRUDService
        /// </summary>
        /// <param name="context">An APIContext injected</param>
        /// <param name="bid">the business ID for this service instance</param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DMAccessTokenService(ApiContext context, short bid, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, logger, bid, rtmClient, migrationHelper)
        {
            
        }

        /// <summary>
        /// Business logic on add. Base class handles ID and ClassTypeID 
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override Task DoBeforeCreateAsync(DMAccessToken newModel, Guid? tempGuid = null)
        {
            newModel.BID = this.BID;
            newModel.ID = Guid.NewGuid();

            //EF knows the SQL column is computed, so it throws errors if it isn't zero
            newModel.ClassTypeID = 0;
            ctx.Set<DMAccessToken>().Add(newModel);
            return Task.CompletedTask;
        }

        /// <summary>
        /// Business logic to do after adding the new model.
        /// </summary>
        /// <param name="model">Model that was added</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override Task DoAfterAddAsync(DMAccessToken model, Guid? tempGuid)
        {
            return Task.CompletedTask;
        }

        /// <summary>
        /// Business logic on update. Base class handles ModifiedDT
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        public override Task DoBeforeUpdateAsync(DMAccessToken oldModel, DMAccessToken newModel)
        {

            //EF knows the SQL column is computed, so it throws errors if it isn't zero
            newModel.ClassTypeID = default(int);
            ctx.Entry(newModel).State = EntityState.Modified;
            return Task.CompletedTask;
        }

        /// <summary>
        /// Business logic for after having updated a model
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel">Model object to update</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override Task DoAfterUpdateAsync(DMAccessToken oldModel, DMAccessToken newModel, string connectionID)
        {
            return Task.CompletedTask;
        }

        /// <summary>
        /// Gets an untracked snapshot of the model before new updates are made
        /// </summary>
        /// <param name="newModel">Updated version of the model</param>
        /// <returns></returns>
        public override async Task<DMAccessToken> GetOldModelAsync(DMAccessToken newModel)
        {
            return await ctx.Set<DMAccessToken>().AsNoTracking().Where(m => m.BID==newModel.BID && m.ID == newModel.ID).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Business logic on delete
        /// </summary>
        /// <param name="model"></param>
        public override Task DoBeforeDeleteAsync(DMAccessToken model)
        {
            var modelClassTypeID = model.ClassTypeID;
            //EF knows the SQL column is computed, so it throws errors if it isn't zero
            model.ClassTypeID = default(int);
            ctx.Set<DMAccessToken>().Remove(model);

            model.ClassTypeID = modelClassTypeID;//reset the original classTypeID since it will be needed by for the refreshMsg

            return Task.CompletedTask;
        }

        /// <summary>
        /// Business logic to perform after deleting a model
        /// </summary>
        /// <param name="model">Deleted model</param>
        /// <returns></returns>
        protected override Task DoAfterDeleteAsync(DMAccessToken model)
        {
            return Task.CompletedTask;
        }

    }
}
