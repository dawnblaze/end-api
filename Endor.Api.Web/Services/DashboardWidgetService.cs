﻿using Endor.EF;
using Endor.Models;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Endor.Tasks;
using Endor.Tenant;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Api.Web.Classes;
using Newtonsoft.Json;
using Endor.Util;
using Newtonsoft.Json.Linq;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// DashboardWidget Service
    /// </summary>
    public class DashboardWidgetService : AtomCRUDService<DashboardWidgetData, int>
    {
        /// <summary>
        /// Constructor for DashboardWidget Service
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">Push Client</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DashboardWidgetService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Default Child Properties to include from Database Context
        /// </summary>
        public override string[] IncludeDefaults => new string[]
        {
            "WidgetDefinition"
        };

        /// <summary>
        /// Get the names of navigation properties to include on Get
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// Gets Includes from Filters
        /// </summary>
        /// <returns></returns>
        public string[] GetIncludes()
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// Overridden CanDelete functionality for DashboardWidget
        /// </summary>
        /// <param name="id">DashboardWidget ID</param>
        /// <param name="UserLink">Current UserLink</param>
        /// <returns></returns>
        //https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/797966385/DashboardWidgetData+API+Endpoints
        public async Task<BooleanResponse> CanDelete(int id, short? UserLink)
        {
            var dashboardWidget = ctx.DashboardWidgetData.Where(dw => dw.ID == id);
            if (!dashboardWidget.Any())
            {
                return new BooleanResponse()
                {
                    ErrorMessage = $"DashboardWidget with ID={id} does not exists",
                    Message = $"DashboardWidget with ID={id} does not exists",
                    Success = false,
                    Value = null
                };
            }
            var linkedDashboard = await ctx.DashboardData.Where(d => d.ID == dashboardWidget.First().DashboardID).FirstAsync();

            bool canDelete = (UserLink.HasValue&&(linkedDashboard.UserLinkID == UserLink));
            return new BooleanResponse()
            {
                Message = $@"Object can be deleted:{canDelete.ToString()}",
                Success = true,
                Value = canDelete
            };
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<DashboardWidgetData> WherePrimary(IQueryable<DashboardWidgetData> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Get child associations (services that handle business logic for child collections). May return null.
        /// </summary>
        /// <returns>IChildServiceAssociation or null</returns>
        protected override IChildServiceAssociation<DashboardWidgetData, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<DashboardWidgetData, int>[0];
        }

        /// <summary>
        /// Called before DashboardWidget is updated
        /// </summary>
        public override async Task DoBeforeUpdateAsync(DashboardWidgetData oldModel, DashboardWidgetData newModel)
        {
            await base.DoBeforeUpdateAsync(oldModel, newModel);
            newModel.LastUpdatedDT = null;
            newModel.LastResults = null;
        }

        internal override void DoBeforeValidate(DashboardWidgetData newModel)
        {
            if (!ctx.DashboardData.Where(d => d.ID == newModel.DashboardID).Any())
            {
                throw new Exception("A valid Dashboard ID must be specified");
            }
            if (!ctx.DashboardWidgetDefinition.Where(d => d.ID == newModel.WidgetDefinitionID).Any())
            {
                throw new Exception("A valid Widget Definition ID must be specified");
            }
        }

        /// <summary>
        /// Returns a DashboardWidget by its ID
        /// </summary>
        /// <param name="id">DashboardWidget ID</param>
        /// <returns></returns>
        public async Task<DashboardWidgetData> GetByID(int id)
        {
            return await ctx.Set<DashboardWidgetData>().IncludeAll(GetIncludes())
                .Where(d => d.BID == BID && d.ID == id)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Returns the DashboardWidgets for the given filters
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<List<DashboardWidgetData>> GetWithFilters(DashboardWidgetFilter filters)
        {
            if (filters != null)
            {
                return await this.GetWhere(filters.WherePredicates());
            }

            return await this.GetAsync();
        }

        private async Task<LocationData> GetBusinessDefaultLocation(short BID)
        {
            BusinessData business = await this.ctx.BusinessData
                .Include(b => b.Locations)
                .FirstOrDefaultAsync(b => b.BID == BID);

            LocationData defaultLocation = business.Locations.FirstOrDefault(l => l.IsDefault);
            if (defaultLocation == null)
            {
                defaultLocation = this.ctx.LocationData.FirstOrDefault(x => x.BID == this.BID);
            }

            return defaultLocation;
        }

        private async Task<LocationData> GetEmployeeDefaultLocaton(long? employeeID)
        {
            var employee = await ctx.EmployeeData.Include("Location").FirstOrDefaultAsync(e => e.ID == employeeID);

            return employee.Location;
        }

        private LocationData GetLocation(DashboardWidgetQueryOptions options)
        {

            if (options?.EmployeeID != null)
            {
                return GetEmployeeDefaultLocaton(options.EmployeeID).Result;
            }
            else
            {
                return GetBusinessDefaultLocation(BID).Result;
            }

        }
        private short GetTimeZoneID(DashboardWidgetQueryOptions options)
        {
            
            var location = GetLocation(options);

            if (location.TimeZoneID != null)
            {
                short? timezoneId = location.TimeZoneID.Value;

                return timezoneId.GetValueOrDefault();
            }
            else
            {
                return (short) TimeZoneUtils.NametoID("GMT Standard Time");
            }
        }
        
        private object QueryCurrentTimeResults(DashboardWidgetQueryOptions defaultOptions, DashboardWidgetQueryOptions options)
        {
            DateTime localTime;
            DateRangeTZ dateHelper;
            DateTime utcTime;

            var timezoneId = 85;


            if (options?.StartDT != null || options?.Timezone != null)
            {
                timezoneId = options?.Timezone ?? GetTimeZoneID(options);

                if (timezoneId == 250) timezoneId = 245;
                if (timezoneId == 255) timezoneId = 260;

                dateHelper = new DateRangeTZ(StandardDateRangeType.TodayToNow, options.StartDT ?? DateTime.Now, timezoneId);
                utcTime = dateHelper.EndDT;
                localTime = dateHelper.LocalTime;

                if (options.StartDT == null)
                {
                    TimeZoneInfo info = TimeZoneUtils.IDToTimeZoneInfo(timezoneId);
                    localTime = TimeZoneInfo.ConvertTime(DateTime.Now, info);
                }
            } 
            else if (defaultOptions?.StartDT != null)
            {
                timezoneId = GetTimeZoneID(options);

                dateHelper = new DateRangeTZ(StandardDateRangeType.TodayToNow, defaultOptions.StartDT.Value, timezoneId);
                utcTime = dateHelper.EndDT;
                localTime = dateHelper.LocalTime;
            }
            else
            {
                utcTime = DateTime.UtcNow;
                localTime = utcTime;
            }


            return new
            {
                TimeZoneID = timezoneId,
                DateTime = localTime,
                UTCDateTime = utcTime
            };
        }

        private async Task<object> QueryOrderTroughputResults(DashboardWidgetQueryOptions defaultOptions,
            DashboardWidgetQueryOptions options)
        {
            var location = GetLocation(options);
            var dateTimeSpan = GetDateTimeSpan(options, StandardDateRangeType.ThisMonthToDate);


            var resultStringFromSQL = "";

            var sqlExecute = "EXEC [dbo].[Dashboard.Widget.Definition.003] @BID, @LocationID, @StartUTCDT, @EndUTCDT";


            var result = await ctx.Database.ExecuteSqlRawAsync2(
                sqlExecute,
                CancellationToken.None,
                new SqlParameter("BID", BID),
                new SqlParameter("LocationID", location.ID),
                new SqlParameter("StartUTCDT", dateTimeSpan.Item1),
                new SqlParameter("EndUTCDT", dateTimeSpan.Item2)
            );
            using (var reader = result.DbDataReader)
            {
                // get one value only
                if (await reader.ReadAsync())
                {
                    resultStringFromSQL = reader.GetString(0);
                }
            }

            return JsonConvert.DeserializeObject(resultStringFromSQL);
        }

        private async Task<object> QueryLineItemStatusCountResults(DashboardWidgetQueryOptions defaultOptions,
            DashboardWidgetQueryOptions options)
        {
            var location = GetLocation(options);
            var resultStringFromSQL = "";
            var statusIds = new List<int>(new[] { 22 });
            if (options != null)
            {
                if (options.StatusIDList != null)
                    foreach (int x in options.StatusIDList.Distinct())
                    {
                        if (x != 22)
                            statusIds.Add(x);
                    }
            }

            var sqlExecute = "EXEC [dbo].[Dashboard.Widget.Definition.005] @BID, @LocationID, @StatusID";
            var result = await ctx.Database.ExecuteSqlRawAsync2(
                sqlExecute,
                CancellationToken.None,
                new SqlParameter("BID", BID),
                new SqlParameter("LocationID", location.ID),
                new SqlParameter("StatusID", string.Join(",", statusIds))
            );
            using (var reader = result.DbDataReader)
            {
                // get one value only
                if (await reader.ReadAsync())
                {
                    resultStringFromSQL = reader.GetString(0);
                }
            }

            return JsonConvert.DeserializeObject(resultStringFromSQL);

        }

        private Tuple<DateTime, DateTime> GetDateTimeSpan(DashboardWidgetQueryOptions options, StandardDateRangeType rangeType)
        {
            var timezoneId = GetTimeZoneID(options);
            var startDt = DateTime.Now;
            var isGmt = timezoneId == 85;

            if (isGmt)
            {
                startDt = DateTime.UtcNow;
            }
            if (timezoneId == 85)
            {
                startDt = DateTime.UtcNow;
            }

            if (options?.StartDT != null)
            {
                startDt = options.StartDT.Value;
            }

            if (isGmt)
            {
                var dateHelper = new DateRange(rangeType, startDt);
                return new Tuple<DateTime, DateTime>(dateHelper.StartDT, dateHelper.EndDT);
            }
            else
            {
                if (options?.RangeType != null)
                {
                    rangeType = (StandardDateRangeType)options.RangeType;
                }
                var dateHelper = new DateRangeTZ((StandardDateRangeType)rangeType, startDt, timezoneId);
                return new Tuple<DateTime, DateTime>(dateHelper.StartDT, dateHelper.EndDT);
            }
        }

        private async Task<object> QueryFinancialSnapshotResults(DashboardWidgetQueryOptions defaultOptions,
            DashboardWidgetQueryOptions options)
        {
            var location = GetLocation(options);
            var dateTimeSpan = GetDateTimeSpan(options, StandardDateRangeType.ThisMonth);

            var resultStringFromSQL = "";

            var sqlExecute = "EXEC [dbo].[Dashboard.Widget.Definition.006] @BID, @LocationID, @StartUTCDT, @EndUTCDT";

            var result = await ctx.Database.ExecuteSqlRawAsync2(
                sqlExecute,
                CancellationToken.None,
                new SqlParameter("BID", BID),
                new SqlParameter("LocationID", location.ID),
                new SqlParameter("StartUTCDT", dateTimeSpan.Item1),
                new SqlParameter("EndUTCDT", dateTimeSpan.Item2)
            );
            using (var reader = result.DbDataReader)
            {
                // get one value only
                if (await reader.ReadAsync())
                {
                    resultStringFromSQL = reader.GetString(0);
                }
            }

            return JsonConvert.DeserializeObject(resultStringFromSQL);

        }

        private async Task<object> QueryCurrentEstimateStatusResults(DashboardWidgetQueryOptions defaultOptions,
            DashboardWidgetQueryOptions options)
        {
            var location = GetLocation(options);
            var dateTimeSpan = GetDateTimeSpan(options, StandardDateRangeType.ThisMonthToDate);

            var resultStringFromSQL = "";

            var sqlExecute = "EXEC [dbo].[Dashboard.Widget.Definition.007] @BID, @LocationID, @StartUTCDT, @EndUTCDT";


            var result = await ctx.Database.ExecuteSqlRawAsync2(
                sqlExecute,
                CancellationToken.None,
                new SqlParameter("BID", BID),
                new SqlParameter("LocationID", location.ID),
                new SqlParameter("StartUTCDT", dateTimeSpan.Item1),
                new SqlParameter("EndUTCDT", dateTimeSpan.Item2)
            );
            using (var reader = result.DbDataReader)
            {
                // get one value only
                if (await reader.ReadAsync())
                {
                    resultStringFromSQL = reader.GetString(0);
                }
            }
            return JsonConvert.DeserializeObject(resultStringFromSQL);
        }

        private async Task<object> QueryCurrentOrderStatusResults(DashboardWidgetQueryOptions defaultOptions,
            DashboardWidgetQueryOptions options)
        {
            var location = GetLocation(options);
            var statusIds = new List<int>();
            if (options != null)
            {
                if (options.StatusIDList != null)
                    foreach (int x in options.StatusIDList.Distinct())
                    {
                        statusIds.Add(x);
                    }
            }

            if (statusIds.Count == 0)
            {
                statusIds = new List<int> {21, 22, 23, 24, 25};
            }

            var resultStringFromSQL = "";

            var sqlExecute = "EXEC [dbo].[Dashboard.Widget.Definition.004] @BID, @LocationID, @StatusID";


            var result = await ctx.Database.ExecuteSqlRawAsync2(
                sqlExecute,
                CancellationToken.None,
                new SqlParameter("BID", BID),
                new SqlParameter("LocationID", location.ID),
                new SqlParameter("StatusID", string.Join(",", statusIds))
            );
            using (var reader = result.DbDataReader)
            {
                // get one value only
                if (await reader.ReadAsync())
                {
                    resultStringFromSQL = reader.GetString(0);
                }
            }
            return JsonConvert.DeserializeObject(resultStringFromSQL);
        }

        private async Task<object> QueryResults(DashboardWidgetDefinition widgetDefinition, DashboardWidgetQueryOptions options)
        {
            DashboardWidgetQueryOptions defaultOptions = DashboardWidgetQueryOptions.Deserialize(widgetDefinition.DefaultOptions);
            
            switch (widgetDefinition.ID)
            {
                case 2:
                    return QueryCurrentTimeResults(defaultOptions, options);

                case 3:
                    return await QueryOrderTroughputResults(defaultOptions, options);

                case 4:
                    return await QueryCurrentOrderStatusResults(defaultOptions, options);

                case 5:
                    return await QueryLineItemStatusCountResults(defaultOptions, options);

                case 6:
                    return await QueryFinancialSnapshotResults(defaultOptions, options);

                case 7:
                    return await QueryCurrentEstimateStatusResults(defaultOptions, options);

                default:
                    return null;
            }
        }

        private async Task<DashboardWidgetQueryResponse> QueryDashboardWidgetData(int widgetID, bool forceRefresh)
        {
            DashboardWidgetData widget = await GetByID(widgetID);

            if (widget == null)
                return null;

            bool mustRefresh = true;
            if (widget.LastUpdatedDT.HasValue)
            {
                double refreshInterval;

                if (forceRefresh)
                    refreshInterval = 1;
                else if (widget.RefreshIntervalinMin.HasValue)
                    refreshInterval = (double)widget.RefreshIntervalinMin.Value / 2;
                else if (widget.WidgetDefinition.DefaultRefreshIntervalInMin.HasValue)
                    refreshInterval = (double)widget.WidgetDefinition.DefaultRefreshIntervalInMin.Value / 2;
                else
                    refreshInterval = 0;

                if (refreshInterval <= 0)
                    mustRefresh = true;
                else
                    mustRefresh = (DateTime.UtcNow - widget.LastUpdatedDT.Value > TimeSpan.FromMinutes(refreshInterval));
            }

            bool fromCache = true;
            object refreshResult;
            if (mustRefresh)
            {
                DashboardWidgetQueryOptions widgetOptions = DashboardWidgetQueryOptions.Deserialize(widget.Options);

                refreshResult = await QueryResults(widget.WidgetDefinition, widgetOptions);

                if (refreshResult == null) refreshResult = "{}";

                widget.LastResults = JsonConvert.SerializeObject(refreshResult);
                widget.LastUpdatedDT = DateTime.UtcNow;
                ctx.DashboardWidgetData.Update(widget);
                await ctx.SaveChangesAsync();

                fromCache = false;
            }

            else if (string.IsNullOrWhiteSpace(widget.LastResults))
                refreshResult = null;

            else
                refreshResult = JsonConvert.DeserializeObject(widget.LastResults);

            return new DashboardWidgetQueryResponse
            {
                Result = refreshResult,
                ResultDT = widget.LastUpdatedDT.Value,
                FromCache = fromCache,
            };
        }

        private async Task<DashboardWidgetQueryResponse> QueryDashboardWidgetDefinition(short widgetDefinitionID, DashboardWidgetQueryOptions options)
        {
            DashboardWidgetDefinition widgetDef = await ctx.DashboardWidgetDefinition.FirstOrDefaultAsync(t => t.ID == widgetDefinitionID);

            if (widgetDef == null)
                return null;

            object result = await QueryResults(widgetDef, options);

            if (result == null) result = "{}";

            return new DashboardWidgetQueryResponse
            {
                Result = result,
                ResultDT = DateTime.UtcNow,
                FromCache = false,
            };
        }

        /// <summary>
        /// Returns the DashboardWidgets for the given query
        /// </summary>
        /// <param name="widgetID">Widget ID</param>
        /// <param name="forceRefresh">Force Refresh</param>
        /// <param name="widgetDefinitionID">Widget Definition ID</param>
        /// <param name="options">Dashboard Widget Options</param>
        /// <returns></returns>
        public async Task<DashboardWidgetQueryResponse> QueryDashboardWidget(int? widgetID, bool? forceRefresh, short? widgetDefinitionID, DashboardWidgetQueryOptions options)
        {
            if (widgetID.HasValue)
                return await QueryDashboardWidgetData(widgetID.Value, forceRefresh.GetValueOrDefault(false));

            if (widgetDefinitionID.HasValue)
            {
                if (options.StatusID == 0 && options.StatusIDList == null)
                {
                    if (widgetDefinitionID == 4)
                        options.StatusIDList = new List<int> { 21, 22, 23, 24, 25 };
                    else if (widgetDefinitionID == 5)
                        options.StatusIDList = new List<int> { 22 };
                }

                return await QueryDashboardWidgetDefinition(widgetDefinitionID.Value, options);
            }

            throw new Exception("Either WidgetID or WidgetDefinitionID must be specified");
        }

        public async Task<DateTime> GetCurrentLocationTime()
        {
            var location = await this.GetBusinessDefaultLocation(this.BID);

            if (location != null)
            {
                TimeZoneInfo info = TimeZoneUtils.IDToTimeZoneInfo((int)location.TimeZoneID);
                var localTime = TimeZoneInfo.ConvertTime(DateTime.Now, info);

                return localTime;
            }

            return DateTime.Now;
        }
    }
}
