﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Includes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Endor.RTM;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Service that wraps utilizes data storage layer and is accessed primarily from Controller layer
    /// </summary>
    public class TaxItemService : AtomCRUDService<TaxItem, short>, ISimpleListableViewService<SimpleTaxItem, short>
    {
        /// <summary>
        /// Gets a paginated list of TaxItems
        /// </summary>
        /// <param name="filters">Query parameters to filter a property</param>
        /// <param name="skip">Number of items to skip</param>
        /// <param name="take">Number of items to take</param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public async Task<PagedList<TaxItem>> GetPagedList(TaxItemFilter filters, int? skip = null, int? take = null, IExpandIncludes includes = null){
            var pagedList = new PagedList<TaxItem>();

            //apply filters to queryable first
            IQueryable<TaxItem> filteredQuery = this.ctx.TaxItem.Where(m => m.BID == BID);

            if (filters != null)
            {
                filteredQuery = this.Where(filters.WherePredicates(), false, includes);
            }
            else if (includes != null)
            {
                filteredQuery = filteredQuery.IncludeAll(GetIncludes(includes));
            }

            //count the unpaged result
            pagedList.TotalCount = await filteredQuery.CountAsync();

            //apply paging if present
            if(skip.HasValue){
                filteredQuery = filteredQuery.Skip(skip.Value);
            }

            if(take.HasValue){
                filteredQuery = filteredQuery.Take(take.Value);
            }

            pagedList.Results = await filteredQuery.ToListAsync();

            if (includes != null)
                pagedList.Results = await this.MapCollection(pagedList.Results.ToList(), includes);

            return pagedList;
        }

        /// <summary>
        /// Constructs a new TaxItemService with a number of injeced parameters
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public TaxItemService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) :
            base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// SimpleTaxItem list
        /// </summary>
        public DbSet<SimpleTaxItem> SimpleListSet => ctx.SimpleTaxItem;

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<TaxItem, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<TaxItem, short>[]
            {
                //CreateChildAssociation<TaxItemService, TaxItem, short>((a)=>a.SubGroups)
            };
        }

        //internal async Task<List<TaxItem>> GetWithFiltersAsync(TaxItemFilters filters)
        //{
        //    if (filters.HasFilter())
        //    {
        //        return await this.GetWhere(filters.WherePredicates());
        //    }
        //    else
        //        return await this.GetAsync(null);
        //}

        /// <summary>
        /// Attempts to change the IsActive Status of the TaxItem
        /// </summary>
        /// <param name="ID">TaxItem's ID</param>
        /// <param name="active">Active status to set to</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public override async Task<EntityActionChangeResponse<short>> SetActive(short ID, bool active, string connectionID)
        {
            EntityActionChangeResponse<short> resp = null;
            var taxItem = await ctx.TaxItem.Where(t => t.ID == ID && t.BID == this.BID).FirstOrDefaultAsync();
            if (taxItem != null)
            {
                if (active)
                {
                    var glAccountSvc = new GLAccountService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
                    var glAccount = await glAccountSvc.GetAsync(taxItem.GLAccountID);

                    // update associated glAccount if it isn't active
                    if (glAccount != null && !glAccount.IsActive)
                    {
                        glAccount.IsActive = true;
                        await glAccountSvc.UpdateAsync(glAccount, connectionID);
                    }

                    resp = await base.SetActive(ID, true, connectionID);
                }
                else
                {
                    resp = await base.SetActive(ID, false, connectionID);
                }
            }
            else
            {
                resp = new EntityActionChangeResponse<short>()
                {
                    Id = ID,
                    ErrorMessage = "TaxItem Not Found.",
                    Message = "TaxItem Not Found.",
                    Success = false
                };
            }
            return resp;
        }

        internal override void DoBeforeValidate(TaxItem newModel)
        {
        }

        /// <summary>
        /// Adds a new GLAccount to a newly created TaxItem
        /// </summary>
        /// <param name="newModel">TaxItem</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(TaxItem newModel, Guid? tempGuid = null)
        {
            newModel.TaxEngineType = TaxEngineType.Internal;
            await base.DoBeforeCreateAsync(newModel, tempGuid);

            int newGLAccountID = 0;

            try
            {
                newGLAccountID = ctx.GLAccount.Where(x => x.ID == 2400 && x.BID == BID).SingleOrDefault().ID;
            }
            catch(Exception e)
            {
                // gl account not found
                await this.logger.Error(BID, e.Message, e);
            }

            newModel.GLAccountID = newGLAccountID;

            if (newModel.SimpleTaxGroups != null)
            {
                foreach (SimpleTaxGroup simpleTaxGroup in newModel.SimpleTaxGroups)
                {
                    newModel.TaxGroupItemLinks.Add(new TaxGroupItemLink() { BID = this.BID, ItemID = newModel.ID, GroupID = simpleTaxGroup.ID });
                }
            }
        }

        /// <summary>
        /// Sets the TaxItem's GLAccount status and Name to equal to update TaxItem 
        /// </summary>
        /// <param name="oldModel">old TaxItem</param>
        /// <param name="newModel">new TaxItem</param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(TaxItem oldModel, TaxItem newModel)
        {
            await base.DoBeforeUpdateAsync(oldModel, newModel);
            GLAccount tiedAccount = await this.ctx.GLAccount.FirstOrDefaultAsync(x => x.BID == this.BID && x.ID == newModel.GLAccountID);
            tiedAccount.IsActive = newModel.IsActive;
            tiedAccount.Name = newModel.Name;
        }

        /// <summary>
        /// Removes the tied GLAccount before deleting the TaxItem 
        /// </summary>
        /// <param name="newModel">new TaxItem</param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(TaxItem newModel)
        {
            await base.DoBeforeDeleteAsync(newModel);
            GLAccount tiedAccount = this.ctx.GLAccount.FirstOrDefault(x => x.BID == this.BID && x.ID == newModel.GLAccountID);
            ctx.Remove(tiedAccount);
        }

        /// <summary>
        /// Appends a string before cloning a TaxGroup
        /// </summary>
        /// <param name="clone">new TaxItem</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(TaxItem clone)
        {
            clone.Name = clone.Name + " (Clone)";
            await base.DoBeforeCloneAsync(clone);
        }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
                              /// <summary>
                              /// Called for each TaxItem to transform fields
                              /// </summary>
                              /// <param name="item">TaxItem</param>
                              /// <param name="includeExpander">Child/Parent includes</param>
                              /// <returns></returns>
        public override async Task MapItem(TaxItem item, IExpandIncludes includeExpander = null)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            var includes = includeExpander?.GetIncludes();
            if (includes != null)
            {
                if (item.TaxGroupItemLinks?.Count > 0)
                {
                    switch (includes[TaxItemIncludes.TaxGroupKey].Level)
                    {
                        case IncludesLevel.Simple:
                            item.SimpleTaxGroups = new HashSet<SimpleTaxGroup>();

                            foreach (var link in item.TaxGroupItemLinks)
                            {
                                if (link.TaxGroup != null)
                                {
                                    item.SimpleTaxGroups.Add(new SimpleTaxGroup()
                                    {
                                        BID = link.TaxGroup.BID,
                                        ID = link.TaxGroup.ID,
                                        ClassTypeID = link.TaxGroup.ClassTypeID,
                                        IsActive = (bool)link.TaxGroup.IsActive,
                                        IsDefault = false,
                                        DisplayName = link.TaxGroup.Name
                                    });
                                }
                            }
                            break;
                        case IncludesLevel.Full:
                            break;
                    }
                }
            }
        }

        internal async Task<EntityActionChangeResponse> LinkTaxGroup(short taxGroupID, short taxItemID, bool isLinked)
        {
            TaxGroupService taxGroupSvc = new TaxGroupService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
            return await taxGroupSvc.LinkTaxItem(taxGroupID, taxItemID, isLinked);
        }

        /// <summary>
        /// Recomputes the TaxRate after TaxItem creation
        /// </summary>
        /// <param name="model">TaxItem</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(TaxItem model, Guid? tempGuid)
        {
            if (model?.ID > 0)
            {
                await RecomputeTaxRate(model);
            }
            await base.DoAfterAddAsync(model, tempGuid);
        }

        /// <summary>
        /// Recomputes the TaxRate before applying TaxItem change
        /// </summary>
        /// <param name="oldModel">old TaxItem</param>
        /// <param name="model">new TaxItem</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(TaxItem oldModel, TaxItem model, string connectionID)
        {
            if (model?.ID > 0)
            {
                await RecomputeTaxRate(model);
            }
            await base.DoAfterUpdateAsync(oldModel, model, connectionID);
        }

        /// <summary>
        /// Recomputes the TaxRate before of a TaxItem
        /// </summary>
        /// <param name="model">TaxItem to compute</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> RecomputeTaxRate(TaxItem model)
        {
            try
            {
                EntityActionChangeResponse response = new EntityActionChangeResponse();

                foreach (TaxGroupItemLink tgil in model.TaxGroupItemLinks)
                {
                    short id = tgil.GroupID;
                    SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                    resultParam.Direction = System.Data.ParameterDirection.Output;

                    object[] myParams = {
                    new SqlParameter("@bid", this.BID),
                    new SqlParameter("@TaxGroupID", id),
                    resultParam
                    };

                    int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Accounting.Tax.Group.Action.RecomputeTaxRate] @bid, @TaxGroupID;", parameters: myParams);
                    if (rowResult > 0)
                    {
                        await QueueIndexForModel(id);

                        response = new EntityActionChangeResponse()
                        {
                            Id = model.ID,
                            Message = "Successfully recomputed Tax Rate.",
                            Success = true
                        };
                    }
                    else
                    {

                        response = new EntityActionChangeResponse()
                        {
                            Id = model.ID,
                            Message = "No rows affected.",
                            Success = false
                        };
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Id = model.ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Builds a query with primary filters
        /// </summary>
        /// <param name="query">TaxItem query</param>
        /// <param name="ID">TaxItem ID</param>
        /// <returns></returns>
        public override IQueryable<TaxItem> WherePrimary(IQueryable<TaxItem> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Gets a Filtered SimpleList of TaxItem
        /// </summary>
        /// <param name="filters">Filters</param>
        /// <returns></returns>
        public async Task<SimpleTaxItem[]> GetSimpleList(TaxItemFilter filters)
        {
            return await ctx.Set<TaxItem>().Where(taxItem => taxItem.BID == BID).WhereAll(filters.WherePredicates())
                .Select(taxItem => new SimpleTaxItem()
                {
                    ID = taxItem.ID,
                    BID = taxItem.BID,
                    ClassTypeID = taxItem.ClassTypeID,
                    IsActive = taxItem.IsActive,
                    DisplayName = taxItem.Name
                }
                ).ToArrayAsync();
        }

        internal async Task<List<TaxItem>> GetWithFiltersAsync(TaxItemFilter filters, TaxItemIncludes includes)
        {
            if (filters != null)
            {
                var results = await this.Where(filters.WherePredicates(), false, includes).ToListAsync();
                return await this.MapCollection(results, includes);
            }

            return await this.GetAsync(includes);
        }
    }
}
