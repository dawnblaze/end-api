﻿using Endor.Models;
using Endor.RTM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    ///   Service Utilities
    /// </summary>
    public static class ServiceUtilities
    {
        /// <summary>
        ///   Returns query based on given includes.
        /// </summary>
        /// <typeparam name="T">Model Type</typeparam>
        /// <param name="query">Query</param>
        /// <param name="includes">Includes</param>
        /// <returns></returns>
        public static IQueryable<T> IncludeAll<T>(this IQueryable<T> query, string[] includes) where T : class
        {
            if (includes == null)
                return query;

            for (int i = 0; i < includes.Length; i++)
            {
                query = query.Include(includes[i]);
            }

            return query;
        }

        /// <summary>
        /// Return query based on given expressions.
        /// </summary>
        /// <typeparam name="T">Model Type</typeparam>
        /// <param name="query">Query</param>
        /// <param name="expressions">Expressions</param>
        /// <returns></returns>
        public static IQueryable<T> WhereAll<T>(this IQueryable<T> query, Expression<Func<T, bool>>[] expressions) where T : class
        {
            if (expressions == null)
                return query;

            for (int i = 0; i < expressions.Length; i++)
            {
                query = query.Where(expressions[i]);
            }

            return query;
        }

        /// <summary>
        /// Return query based on given business ID.  If the model type does not have a BID, the input query is returned
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="BID"></param>
        /// <returns></returns>
        private static IQueryable<T> WhereBID<T>(this IQueryable<T> query, short BID) where T : class, IAtom
        {
            if (typeof(T).GetProperty("BID") != null)
                return query.Where(a => a.BID == BID);

            return query;
        }

        /// <summary>
        /// Return query based on given business ID and model ID of type byte. Includes optional parameter for additional filter predicate.
        /// </summary>
        /// <typeparam name="T">Model Type</typeparam>
        /// <param name="query">Query</param>
        /// <param name="BID">Business ID</param>
        /// <param name="ID">Model ID</param>
        /// <param name="filter">Additional Filter Predicate</param>
        /// <returns></returns>
        public static IQueryable<T> WherePrimary<T>(this IQueryable<T> query, short BID, byte ID, Expression<Func<T, bool>> filter = null) where T : class, IAtom<byte>
        {
            IQueryable<T> internalQuery = query.WhereBID(BID);

            internalQuery = internalQuery.Where(t => t.ID == ID);

            if (filter != null)
                internalQuery = internalQuery.Where(filter);

            return internalQuery;
        }

        /// <summary>
        /// Return query based on given business ID. Includes optional parameter for additional filter predicate.
        /// </summary>
        /// <typeparam name="T">Model Type</typeparam>
        /// <param name="query">Query</param>
        /// <param name="BID">Business ID</param>
        /// <param name="filter">Additional Filter Predicate</param>
        /// <returns></returns>
        public static IQueryable<T> WherePrimary<T>(this IQueryable<T> query, short BID, Expression<Func<T, bool>> filter) where T : class, IAtom
        {
            IQueryable<T> internalQuery = query.WhereBID(BID);

            if (filter != null)
                internalQuery = internalQuery.Where(filter);

            return internalQuery;
        }

        /// <summary>
        /// Return query based on given business ID. Includes optional parameter for additional filter predicate.
        /// </summary>
        /// <typeparam name="T">Model Type</typeparam>
        /// <param name="query">Query</param>
        /// <param name="BID">Business ID</param>
        /// <param name="filter">Additional Filter Predicate</param>
        /// <returns></returns>
        public static IQueryable<T> WherePrimaryAll<T>(this IQueryable<T> query, short BID, Expression<Func<T, bool>>[] filter) where T : class, IAtom
        {
            IQueryable<T> internalQuery = query.WhereBID(BID);

            if (filter != null && filter.Any())
                internalQuery = internalQuery.WhereAll(filter);

            return internalQuery;
        }

        /// <summary>
        /// Return query based on given business ID and model ID of type short. Includes optional parameter for additional filter predicate.
        /// </summary>
        /// <typeparam name="T">Model Type</typeparam>
        /// <param name="query">Query</param>
        /// <param name="BID">Business ID</param>
        /// <param name="ID">Model ID</param>
        /// <param name="filter">Additional Filter Predicate</param>
        /// <returns></returns>
        public static IQueryable<T> WherePrimary<T>(this IQueryable<T> query, short BID, short ID, Expression<Func<T, bool>> filter = null) where T : class, IAtom<short>
        {
            IQueryable<T> internalQuery = query.WhereBID(BID);

            internalQuery = internalQuery.Where(t => t.ID == ID);

            if (filter != null)
                internalQuery = internalQuery.Where(filter);

            return internalQuery;
        }

        /// <summary>
        /// Return query based on given business ID and  model ID of type int. Includes optional parameter for additional filter predicate.
        /// </summary>
        /// <typeparam name="T">Model Type</typeparam>
        /// <param name="query">Query</param>
        /// <param name="BID">Business ID</param>
        /// <param name="ID">Model ID</param>
        /// <param name="filter">Additional Filter Predicate</param>
        /// <returns></returns>
        public static IQueryable<T> WherePrimary<T>(this IQueryable<T> query, short BID, int ID, Expression<Func<T, bool>> filter = null) where T : class, IAtom<int>
        {
            IQueryable<T> internalQuery = query.WhereBID(BID);

            internalQuery = internalQuery.Where(t => t.ID == ID);

            if (filter != null)
                internalQuery = internalQuery.Where(filter);

            return internalQuery;
        }

        /// <summary>
        /// Returns the first database result given a business ID and model ID of type byte
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="BID"></param>
        /// <param name="ID"></param>
        /// <param name="filter">Additional Filter Predicate</param>
        /// <returns></returns>
        public static T FirstOrDefaultPrimary<T>(this IQueryable<T> query, short BID, byte ID, Expression<Func<T, bool>> filter = null) where T : class, IAtom<byte>
        {
            return Task.Run(async () => await FirstOrDefaultPrimaryAsync(query, BID, ID, filter)).Result;
        }

        /// <summary>
        /// Direct Asynchronous call that returns the first database result given a business ID and model ID of type byte
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="BID"></param>
        /// <param name="ID"></param>
        /// <param name="filter">Additional Filter Predicate</param>
        /// <returns></returns>
        public static async Task<T> FirstOrDefaultPrimaryAsync<T>(this IQueryable<T> query, short BID, byte ID, Expression<Func<T, bool>> filter = null) where T : class, IAtom<byte>
        {
            return await query.WherePrimary(BID, ID, filter).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Returns the first database result given a business ID and model ID of type short
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="BID"></param>
        /// <param name="ID"></param>
        /// <param name="filter">Additional Filter Predicate</param>
        /// <returns></returns>
        public static T FirstOrDefaultPrimary<T>(this IQueryable<T> query, short BID, short ID, Expression<Func<T, bool>> filter = null) where T : class, IAtom<short>
        {
            return Task.Run(async () => await FirstOrDefaultPrimaryAsync(query, BID, ID, filter)).Result;
        }

        /// <summary>
        /// Direct Asynchronous call that returns the first database result given a business ID and model ID of type short
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="BID"></param>
        /// <param name="ID"></param>
        /// <param name="filter">Additional Filter Predicate</param>
        /// <returns></returns>
        public static async Task<T> FirstOrDefaultPrimaryAsync<T>(this IQueryable<T> query, short BID, short ID, Expression<Func<T, bool>> filter = null) where T : class, IAtom<short>
        {
            return await query.WherePrimary(BID, ID, filter).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Returns the first database result given a business ID and model ID of type int
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="BID"></param>
        /// <param name="ID"></param>
        /// <param name="filter">Additional Filter Predicate</param>
        /// <returns></returns>
        public static T FirstOrDefaultPrimary<T>(this IQueryable<T> query, short BID, int ID, Expression<Func<T, bool>> filter = null) where T : class, IAtom<int>
        {
            return Task.Run(async () => await FirstOrDefaultPrimaryAsync(query, BID, ID, filter)).Result;
        }

        /// <summary>
        /// Direct Asynchronous call that returns the first database result given a business ID and model ID of type int
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="BID"></param>
        /// <param name="ID"></param>
        /// <param name="filter">Additional Filter Predicate</param>
        /// <returns></returns>
        public static async Task<T> FirstOrDefaultPrimaryAsync<T>(this IQueryable<T> query, short BID, int ID, Expression<Func<T, bool>> filter = null) where T : class, IAtom<int>
        {
            return await query.WherePrimary(BID, ID, filter).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Helper method to iterate over an array of child service associations and invoke Add operations. This method allows null arrays.
        /// </summary>
        /// <typeparam name="pM">Parent Model type</typeparam>
        /// <typeparam name="pI">Parent ID type</typeparam>
        /// <param name="associations">A (possibly null) array of associations</param>
        /// <param name="parentModel">The parent model that is the root of the operation</param>
        /// <returns></returns>
        public static async Task DoBeforeAddAsync<pM, pI>(this IChildServiceAssociation<pM, pI>[] associations, pM parentModel)
            where pM : class, IAtom<pI>
            where pI : struct, IConvertible
        {
            if (associations != null)
            {
                foreach (IChildServiceAssociation<pM, pI> association in associations)
                {
                    await association.ForEachChildDoBeforeAddAsync(parentModel);
                }
            }
        }

        /// <summary>
        /// Helper method to iterate over an array of child service associations and invoke Update operations. This method allows null arrays.
        /// </summary>
        /// <typeparam name="pM">Parent Model type</typeparam>
        /// <typeparam name="pI">Parent ID type</typeparam>
        /// <param name="associations">A (possibly null) array of associations</param>
        /// <param name="parentOldModel">The old parent model that is the root of the operation</param>
        /// <param name="parentNewModel">The new parent model that is the root of the operation</param>
        /// <returns></returns>
        public static async Task DoBeforeUpdateAsync<pM, pI>(this IChildServiceAssociation<pM, pI>[] associations, pM parentOldModel, pM parentNewModel)
            where pM : class, IAtom<pI>
            where pI : struct, IConvertible
        {
            if (associations != null)
            {
                foreach (IChildServiceAssociation<pM, pI> association in associations)
                {
                    await association.ForEachChildDoBeforeUpdateAsync(parentOldModel, parentNewModel);
                }
            }
        }

        /// <summary>
        /// Helper method to iterate over an array of child service associations and invoke Delete operations. This method allows null arrays.
        /// </summary>
        /// <typeparam name="pM">Parent Model type</typeparam>
        /// <typeparam name="pI">Parent ID type</typeparam>
        /// <param name="associations">A (possibly null) array of associations</param>
        /// <param name="parentModel">The parent model that is the root of the operation</param>
        /// <returns></returns>
        public static async Task DoBeforeDeleteAsync<pM, pI>(this IChildServiceAssociation<pM, pI>[] associations, pM parentModel)
            where pM : class, IAtom<pI>
            where pI : struct, IConvertible
        {
            if (associations != null)
            {
                foreach (IChildServiceAssociation<pM, pI> association in associations)
                {
                    await association.ForEachChildDoBeforeDeleteAsync(parentModel);
                }
            }
        }

        /// <summary>
        /// removes Atom from dbset 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="dbSet"></param>
        /// <param name="BID"></param>
        /// <param name="whereClause"></param>
        /// <returns></returns>
        public static async Task<List<RefreshEntity>> RemoveRangeWhereAtomAsync<TEntity>(this DbSet<TEntity> dbSet, short BID, Expression<Func<TEntity, bool>> whereClause)
            where TEntity: class, IAtom
        {
            var result = new List<RefreshEntity>();
            foreach(var entity in await dbSet.WherePrimary(BID, whereClause).ToListAsync())
            {
                dbSet.Remove(entity);
                result.Add(new RefreshEntity()
                {
                    BID = entity.BID,
                    ClasstypeID = entity.ClassTypeID,
                    RefreshMessageType = RTM.Enums.RefreshMessageType.Delete,
                    ID = (entity is IAtom<int>) ? (entity as IAtom<int>).ID : 
                         (entity is IAtom<short>) ? (entity as IAtom<short>).ID : 
                         (entity is IAtom<byte>) ? (entity as IAtom<byte>).ID : default(int)
                });
            }
            
            return result;
        }

        /// <summary>
        /// removes nonAtom from dbset 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="dbSet"></param>
        /// <param name="BID"></param>
        /// <param name="ct"></param>
        /// <param name="whereClause"></param>
        /// <param name="IDGetter"></param>
        /// <returns></returns>
        public static async Task<List<RefreshEntity>> RemoveRangeWhereAsync<TEntity>(this DbSet<TEntity> dbSet, short BID, ClassType ct, Expression<Func<TEntity, bool>> whereClause, Func<TEntity, int> IDGetter = null)
            where TEntity : class
        {
            var result = new List<RefreshEntity>();
            foreach (var entity in await dbSet.Where(whereClause).ToListAsync())
            {
                dbSet.Remove(entity);
                result.Add(new RefreshEntity()
                {
                    BID = BID,
                    ClasstypeID = (int)ct,
                    RefreshMessageType = RTM.Enums.RefreshMessageType.Delete,
                    ID = (IDGetter != null) ? IDGetter(entity) : default(int)
                });
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queuer"></param>
        /// <param name="refreshes"></param>
        /// <returns></returns>
        public static async Task IndexModel(this Tasks.ITaskQueuer queuer, List<RefreshEntity> refreshes)
        {
            foreach(RefreshEntity rE in refreshes)
            {
                await queuer.IndexModel(rE.BID, rE.ClasstypeID, rE.ID);
            }
        }
    }
}
