﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.Data.SqlClient;
using Endor.RTM;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// A service for <see cref="PaymentMethod"/>
    /// </summary>
    public class PaymentMethodService : AtomCRUDService<PaymentMethod, int>, ISimpleListableViewService<SimplePaymentMethod, int>
    {
        /// <summary>
        /// DbSet of SimplepaymentMethod for <see cref="ISimpleListableViewService{SLI, I}"/>
        /// </summary>
        public DbSet<SimplePaymentMethod> SimpleListSet => this.ctx.SimplePaymentMethod;

        /// <summary>
        /// ctor for PaymentMethodService
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public PaymentMethodService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }
        
        #region Overrides
        /// <summary>
        /// Child associations for Payment Method
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<PaymentMethod, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<PaymentMethod, int>[0];
        }

        /// <summary>
        /// Default includes for PaymentMethod
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        internal override void DoBeforeValidate(PaymentMethod newModel)
        {
        }
        #endregion Overrides

        /// <summary>
        /// Returns a boolean response after executing the DB SPROC CanDelete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(int id)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Bit);
                resultParam.Direction = System.Data.ParameterDirection.Output;
                SqlParameter cantDeleteReasonParam = new SqlParameter("@CDR", System.Data.SqlDbType.VarChar, 1024);
                cantDeleteReasonParam.Direction = System.Data.ParameterDirection.Output;

                object[] myParams = {
                    new SqlParameter("@bid", this.BID),
                    new SqlParameter("@ID", id),
                    resultParam,
                    cantDeleteReasonParam
                };

                await ctx.Database.ExecuteSqlRawAsync("EXEC dbo.[Accounting.Payment.Method.Action.CanDelete] @bid, @ID, @Result = @Result Output, @CantDeleteReason = @CDR Output;", parameters: myParams);

                bool? result = (bool?)resultParam.Value;
        
                return new BooleanResponse()
                {
                    Success = result.Value,
                    Value = result,
                    Message = result.GetValueOrDefault() ? $"Payment Type {id} can be deleted" : null,
                    ErrorMessage = result.GetValueOrDefault() ? null : cantDeleteReasonParam.Value.ToString(),
                };
            }
            catch (Exception ex)
            {
                return new BooleanResponse()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        internal async Task<List<PaymentMethod>> GetWithFiltersAsync(PaymentMethodFilter filters)
        {
            if (filters != null && filters.IsActive.HasValue)
            {
                return await this.GetWhere(filters.WherePredicates());
            }
            else
                return await this.GetAsync(null);
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<PaymentMethod> WherePrimary(IQueryable<PaymentMethod> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }
    }
}
