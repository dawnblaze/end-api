﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Responses;
using Endor.Api.Web.Includes;
using Endor.AzureStorage;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// employee service
    /// </summary>
    public class EmployeeService : AtomCRUDService<EmployeeData, short>, ISimpleListableViewService<SimpleEmployeeData, short>, ILocatorParentService<EmployeeData, short>
    {
        private int? _userLinkID = null;
        private EmployeeClone _employeeCloneOptions = null;
        private EndorOptions _endorOptions = null;
        private const string OptionKeyCopySettings = "Employee.Clone.CopySettings";
        private const string OptionKeyCopyDocuments = "Employee.Clone.CopyDocuments";
        private const string OptionKeyLicenseUsersCount = "License.Users.Count";

        /// <summary>
        /// gets simple list set
        /// </summary>
        public DbSet<SimpleEmployeeData> SimpleListSet => ctx.SimpleEmployeeData;

        /// <summary>
        /// employee svc ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper"></param>
        public EmployeeService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// default include property strings
        /// </summary>
        public override string[] IncludeDefaults => new string[]
        {
            "Business",
            "EmployeeLocators",
            "EmployeeTeamLinks",
            "UserLinks",
            "BoardLinks"
        };

        /// <summary>
        /// gets default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// get locators for ILocatorParentService
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IEnumerable<BaseLocator<short, EmployeeData>> GetLocators(EmployeeData model) => model.EmployeeLocators;
        /// <summary>
        /// set locators for ILocatorParentService
        /// </summary>
        /// <param name="model"></param>
        /// <param name="value"></param>
        public void SetLocators(EmployeeData model, IEnumerable<BaseLocator<short, EmployeeData>> value) =>
            model.EmployeeLocators = value.Select(x => (EmployeeLocator)x).ToList();

        /// <summary>
        /// before create hook
        /// makes sure email doesn't already exist
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(EmployeeData newModel, Guid? tempGuid = null)
        {
            /* This will be to User instead of at the Employee Level
             * if (await DoesLocatorsContainDuplicateEmailAddress(newModel)) {
                throw new Exception("Duplicate email usage");
            }*/
            newModel.UserLinks = null;//prevent employee creation from inserting userlinks(userlinks are created via invite)
            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// before update hook
        /// makes sure email doesn't already exist
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(EmployeeData oldModel, EmployeeData newModel)
        {
            newModel.LongName = default(string);
            var updatedUserLink = newModel?.UserLinks?.FirstOrDefault(ul => ul.ID != 0);//get existing userlink
            if (updatedUserLink != null)
            {
                var existingUserLink = await this.ctx.
                                        UserLink.
                                        FirstOrDefaultAsync(ul => ul.EmployeeID == oldModel.ID);

                if (existingUserLink != null)
                {
                    existingUserLink.UserAccessType = updatedUserLink.UserAccessType;
                    existingUserLink.RightsGroupListID = updatedUserLink.RightsGroupListID;
                }

            }
            newModel.UserLinks = null;//prevent updates from inserting userlinks(userlinks are created via invite)
            await base.DoBeforeUpdateAsync(oldModel, newModel);
        }

        /// <summary>
        /// before clone hook
        /// sets clones business to null
        /// </summary>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(EmployeeData clone)
        {
            clone.Business = null;
            clone.UserLinks = null;
            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Performs additional actions after Cloning an Employee
        /// </summary>
        /// <param name="clonedFromID">Employee ID we cloned from</param>
        /// <param name="clone">Cloned (new) Employee data model</param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(short clonedFromID, EmployeeData clone)
        {
            try
            {
                clone.First = this._employeeCloneOptions.FirstName;
                clone.Last = this._employeeCloneOptions.LastName;
                ctx.EmployeeData.Update(clone);
                await ctx.SaveChangesAsync();

                EmployeeData originalEmployeeData = await this.ctx.EmployeeData.IncludeAll(new[] { "EmployeeLocators", "UserLinks" }).AsNoTracking().FirstOrDefaultPrimaryAsync(this.BID, clonedFromID);

                OptionValue copySettings = (await base.GetUserOptionValueByName(OptionKeyCopySettings, this._userLinkID)).Value;
                OptionValue copyDocuments = (await base.GetUserOptionValueByName(OptionKeyCopyDocuments, this._userLinkID)).Value;

                //Always Copy Data and Reports Buckets

                //Copy Documents Bucket
                DocumentManager client = base.GetDocumentManager(clonedFromID, ClassType.Employee, BucketRequest.Data);
                await client.CloneAllBucketBlobsAsync(clone.ID, Bucket.Data, includeSpecialFolderBlobName: true);

                //Copy Reports Bucket
                client = base.GetDocumentManager(clonedFromID, ClassType.Employee, BucketRequest.Reports);
                await client.CloneAllBucketBlobsAsync(clone.ID, Bucket.Reports, includeSpecialFolderBlobName: true);

                //Conditionally copy documents based on option value (if set)
                if (copyDocuments != null && bool.TryParse(copyDocuments.Value, out bool copyDocumentsValue) && copyDocumentsValue)
                {//Don't copy if setting is null
                    client = base.GetDocumentManager(clonedFromID, ClassType.Employee, BucketRequest.Documents);
                    await client.CloneAllBucketBlobsAsync(clone.ID, Bucket.Documents, includeSpecialFolderBlobName: true);
                }

                //No special code required to clone locators since it is copied with the employee due to it being included in the default includes

                UserLink clonedFromUserLink = originalEmployeeData?.UserLinks?.FirstOrDefault(ul => ul.EmployeeID == clonedFromID);
                //Set permissions if not None (0)
                if (clonedFromUserLink != null && this._employeeCloneOptions != null && this._employeeCloneOptions.Email.HasValue() && _endorOptions != null)
                {
                    //If original Employee has a UserLink, compose user invite request with the same UserAccessType
                    var sendInviteRequest = new SendInviteRequest
                    {
                        Email = this._employeeCloneOptions.Email,
                        UserAccessType = clonedFromUserLink.UserAccessType,
                        RightsGroupListID = clonedFromUserLink.RightsGroupListID
                    };

                    EntityActionChangeResponse<short> sendInviteResponse =
                        await this.SendInvite(clone.ID, sendInviteRequest, this._endorOptions);

                    if (sendInviteResponse.Success)
                    {
                        //We can only copy user option values (settings) if we have a user link id returned from the send invite request
                        if (copySettings == null || bool.TryParse(copySettings.Value, out bool copySettingsValue) && copySettingsValue)
                        {
                            //Copy if setting is null
                            PutOptionValuesResponse putOptionValuesResp = await this.CopyOptionValues(clonedFromUserLink.ID, sendInviteResponse.Id);
                        }
                    }
                    else
                    {
                        throw new Exception("Failed to apply permissions and send user invite request.");
                    }
                }
                await base.DoAfterCloneAsync(clonedFromID, clone);
            }
            catch (Exception e)
            {
                await this.logger.Error(this.BID, "Employee Do After Clone Failed", e);
            }
        }

        /// <summary>
        /// Clones an Employee record with options posted from client modal selections
        /// </summary>
        /// <param name="clonedFromEmployeeID"></param>
        /// <param name="employeeClone"></param>
        /// <param name="userLinkID"></param>
        /// <param name="options"></param>
        public async Task<IActionResult> CloneWithOptions(short clonedFromEmployeeID, EmployeeClone employeeClone, int? userLinkID, EndorOptions options)
        {
            this._userLinkID = userLinkID;
            this._employeeCloneOptions = employeeClone;
            this._endorOptions = options;
            EmployeeData newEmployeeData;

            try
            {
                newEmployeeData = await this.CloneAsync(clonedFromEmployeeID);

                if (newEmployeeData == null)
                    throw new Exception("Employee clone failed");

                //Re-save the clone without any email locators
                newEmployeeData.EmployeeLocators = newEmployeeData.EmployeeLocators.Where(l => l.LocatorType != (int)LocatorType.Email).ToList();

                await ctx.SaveChangesAsync();

                //Existing locators from clone have already been copied in DoAfterCloneAsync however if that list of locators doesn't contain the posted email, we'll need to add one now
                if (employeeClone.Email.HasValue())
                {
                    if (newEmployeeData.EmployeeLocators == null)
                        newEmployeeData.EmployeeLocators = new List<EmployeeLocator>();

                    newEmployeeData.EmployeeLocators.Add
                    (
                        new EmployeeLocator
                        {
                            BID = this.BID,
                            ID = await RequestIDAsync(this.BID, typeof(EmployeeLocator)),
                            ParentID = newEmployeeData.ID,
                            LocatorType = (byte)LocatorType.Email,
                            LocatorSubType = 0, //Unknown
                            Locator = employeeClone.Email,
                            RawInput = employeeClone.Email,
                            SortIndex = 0,
                            IsValid = true,
                            IsVerified = false,
                            HasImage = false,
                            ModifiedDT = DateTime.UtcNow,
                        }
                    );

                    await ctx.SaveChangesAsync();
                }

                //Trigger Re-Index/Refresh
                await base.QueueIndexForModel(newEmployeeData.ID);
                await base.DoAfterCloneAsync(clonedFromEmployeeID, newEmployeeData);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }


            return new OkObjectResult(newEmployeeData);
        }

        /// <summary>
        /// Copy all Options from one User to another User
        /// </summary>
        /// <param name="clonedFromUserLinkId"></param>
        /// <param name="newUserLinkId"></param>
        /// <returns></returns>
        private async Task<PutOptionValuesResponse> CopyOptionValues(short clonedFromUserLinkId, int newUserLinkId)
        {
            GetOptionValuesResponse values = await _optionService.Get(BID, null, null, clonedFromUserLinkId, null, null, null);

            var optionValues = values.Values
                .Select(o => new Options
                {
                    OptionID = o.OptionID,
                    OptionName = o.Name,
                    Value = o.Value
                }).ToList();

            PutOptionValuesResponse response = await _optionService.Put(optionValues, null, this.BID, null, null, newUserLinkId, null, null);

            return response;
        }

        /// <summary>
        /// returns child associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<EmployeeData, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<EmployeeData, short>[]{
                CreateChildAssociation<EmployeeLocatorService, EmployeeLocator, int>((emp) => emp.EmployeeLocators),
                CreateChildLinkAssociation<EmployeeTeamLinkService, EmployeeTeamLink>((emp) => emp.EmployeeTeamLinks)
            };
        }

        /// <summary>
        /// after add hook
        /// persists temp uploads
        /// </summary>
        /// <param name="model"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(EmployeeData model, Guid? tempGuid)
        {
            if (tempGuid.HasValue)
            {
                IStorageContext dataStorage = new StorageContext(this.BID, BucketRequest.Data, new DocumentStorage.Models.DMID() { guid = tempGuid });
                IStorageContext documentStorage = new StorageContext(this.BID, BucketRequest.Documents, new DocumentStorage.Models.DMID() { guid = tempGuid });
                IStorageContext reportStorage = new StorageContext(this.BID, BucketRequest.Reports, new DocumentStorage.Models.DMID() { guid = tempGuid });
                DocumentManager dataDM = new DocumentManager(cache, dataStorage);
                await dataDM.PersistDocumentsAsync(tempGuid.Value, model.ID, model.ClassTypeID);

                DocumentManager documentDataDM = new DocumentManager(cache, documentStorage);
                await documentDataDM.PersistDocumentsAsync(tempGuid.Value, model.ID, model.ClassTypeID);

                DocumentManager reportDM = new DocumentManager(cache, reportStorage);
                await reportDM.PersistDocumentsAsync(tempGuid.Value, model.ID, model.ClassTypeID);

                IStorageContext permanentDataStorage = new StorageContext(this.BID, BucketRequest.Data, new DocumentStorage.Models.DMID() { id = model.ID, ctid = model.ClassTypeID });
                DocumentManager permanentDataDM = new DocumentManager(cache, permanentDataStorage);
                bool defaultImageSaved = await permanentDataDM.HasDefaultImage();

                if (defaultImageSaved)
                {
                    model.HasImage = true;
                    await this.ctx.SaveChangesAsync();
                }
            }

            await base.DoAfterAddAsync(model, tempGuid);
        }

        /// <summary>
        /// before delete hook
        /// reassigns linked objects using system "deleted" employee
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(EmployeeData model)
        {
            //(EmployeeData model) is the target for deletion
            EmployeeData systemDeletedEmployee = await this.ctx.EmployeeData
                                    .Where(emp =>
                                        (emp.First.Equals("DELETED") || emp.Last.Equals("DELETED"))
                                        //&& emp.IsSystem == true
                                        && emp.BID == model.BID
                                        && emp.ID == (short)(model.BID * -1))
                                    .FirstOrDefaultAsync();
            if (systemDeletedEmployee is null)
            {
                systemDeletedEmployee = new EmployeeData();
                systemDeletedEmployee.BID = model.BID;
                systemDeletedEmployee.ID = (short)(model.BID * -1);
                systemDeletedEmployee.First = "DELETED";
                systemDeletedEmployee.Last = "DELETED";
                systemDeletedEmployee.IsActive = false;
                systemDeletedEmployee.LocationID = model.LocationID;
                //systemDeletedEmployee. = true;
                this.ctx.EmployeeData.Add(systemDeletedEmployee);
            }

            var locators = model.EmployeeLocators;
            var userLinks = model.UserLinks;
            var teamLinks = model.EmployeeTeamLinks;

            locators.ToList().ForEach(locator =>
                {
                    locator.ParentID = systemDeletedEmployee.ID;
                    locator.Parent = null;
                    model.EmployeeLocators.Remove(locator);
                    systemDeletedEmployee.EmployeeLocators.Add(locator);
                });
            userLinks.ToList().ForEach(ul =>
                {
                    // Remove Dashboards
                    var dashboard = ctx.DashboardData.FirstOrDefault(i => i.UserLinkID == ul.ID);
                    if (dashboard != null)
                    {
                        ctx.Remove(dashboard);
                    }

                    ul.EmployeeID = systemDeletedEmployee.ID;
                    ul.Employee = null;
                    model.UserLinks.Remove(ul);
                    systemDeletedEmployee.UserLinks.Add(ul);
                });
            teamLinks.ToList().ForEach(tl =>
                {
                    tl.EmployeeID = systemDeletedEmployee.ID;
                    tl.Employee = null;
                    model.EmployeeTeamLinks.Remove(tl);
                    systemDeletedEmployee.EmployeeTeamLinks.Add(tl);
                });

            await base.DoBeforeDeleteAsync(model);
        }

        internal override void DoBeforeValidate(EmployeeData newModel)
        {
            this.StripOutTemporaryLocators(newModel);
        }

        /// <summary>
        /// returns clause for filtering on primary ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<EmployeeData> WherePrimary(IQueryable<EmployeeData> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Can Create User
        /// </summary>
        /// <returns></returns>
        public async Task<CanCreateUserResponse> CanCreateUser()
        {
            OptionValue optionMaximumUsers = (await base.GetUserOptionValueByName(OptionKeyLicenseUsersCount)).Value;

            int maximumUsers = 0;

            if (optionMaximumUsers != null && int.TryParse(optionMaximumUsers.Value, out int i))
                maximumUsers = i;

            int currentUserCount = ctx.UserLink.Count(x => x.BID == BID);

            bool result = (currentUserCount < maximumUsers);

            return new CanCreateUserResponse
            {
                Success = true,
                Value = result,
                CurrentUserCount = currentUserCount,
                AuthorizedUserCount = maximumUsers,
                Message = $"{(result ? "Can" : "Unable to")} create user.",
            };
        }

        /// <summary>
        /// Sends a user invite to an employee
        /// </summary>
        /// <param name="ID">EmployeeData ID</param>
        /// <param name="inviteRequest">Invite Request</param>
        /// <param name="options">Options</param>
        /// <param name="returnUserLink">if true, returns the userLink created from the invite</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse<short>> SendInvite(short ID, SendInviteRequest inviteRequest, EndorOptions options, bool returnUserLink = false)
        {
            CanCreateUserResponse canCreateUser = await CanCreateUser();

            if (!canCreateUser.Value.GetValueOrDefault(false))
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    ErrorMessage = "Maximum number of users has been reached"
                };

            EmployeeData emp = await GetAsync(ID, new EmployeeIncludes() { Location = IncludesLevel.Full });

            if (emp == null)
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    ErrorMessage = "Employee not found"
                };

            var storageClient = new EntityStorageClient((await this.cache.Get(this.BID)).StorageConnectionString, this.BID);

            Uri locationImageUri = null;

            if ((bool)emp?.Location.HasImage)
            {
                locationImageUri = await storageClient.GetDefaultImageUriWithNoExpiration(emp.Location.ClassTypeID, emp.Location.ID);
            }

            var inviteBody = new GenerateInviteRequest()
            {
                BID = this.BID,
                IsEmployee = true,
                SuggestedEmail = inviteRequest.Email,
                DisplayName = emp.ShortName,
                HasImage = (bool)emp?.Location.HasImage,
                ImageLocation = locationImageUri?.AbsoluteUri ?? string.Empty,
            };

            GenerateInviteResponse inviteResp = null;

            int isEmployeeUserName = 0;
            // Check whether if suggested email is a username of employee before by checking its employee locators.
            if (emp.EmployeeLocators != null)
            {
                isEmployeeUserName = emp.EmployeeLocators.Where(el => el.Locator == inviteBody.SuggestedEmail).Count();
            }

            using (var client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, options.AuthOrigin + "/User/GenerateInvite");
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToBase64String(Encoding.UTF8.GetBytes(options.TenantSecret)));
                request.Content = new StringContent(JsonConvert.SerializeObject(inviteBody), Encoding.UTF8, "application/json");

                HttpResponseMessage response = await client.SendAsync(request);
                string responseString = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        Id = -1,
                        Message = "Unauthorized",
                    };

                if (responseString == "Username already exists" && isEmployeeUserName > 0)
                {
                    request = new HttpRequestMessage(HttpMethod.Post, options.AuthOrigin + "/User/GenerateInviteForExistingUser");
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToBase64String(Encoding.UTF8.GetBytes(options.TenantSecret)));
                    request.Content = new StringContent(JsonConvert.SerializeObject(inviteBody), Encoding.UTF8, "application/json");

                    response = await client.SendAsync(request);
                    responseString = await response.Content.ReadAsStringAsync();


                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                        return new EntityActionChangeResponse<short>()
                        {
                            Success = false,
                            Id = -1,
                            Message = "Unauthorized",
                        };
                }

                if (response.StatusCode != HttpStatusCode.OK)
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        Id = -1,
                        ErrorMessage = responseString,
                    };

                inviteResp = JsonConvert.DeserializeObject<GenerateInviteResponse>(responseString);
            }

            var oldLinks = ctx.UserLink.Where(l => l.BID == this.BID && l.EmployeeID == emp.ID);
            UserLink oldUserLink = oldLinks.SingleOrDefault();

            UserLink newlink = new UserLink()
            {
                ID = (isEmployeeUserName > 0 && oldUserLink != null) ? oldUserLink.ID : await RequestIDAsync(this.BID, (int)ClassType.UserLink),
                BID = (isEmployeeUserName > 0 && oldUserLink != null) ? oldUserLink.BID : this.BID,
                EmployeeID = (isEmployeeUserName > 0 && oldUserLink != null) ? oldUserLink.BID : emp.ID,
                TempUserID = inviteResp.ID,
                UserAccessType = inviteRequest.UserAccessType,
                RightsGroupListID = inviteRequest.RightsGroupListID,
                UserName = inviteRequest.Email,
                DisplayName = (isEmployeeUserName > 0 && oldUserLink != null) ? oldUserLink.DisplayName : emp.ShortName,
                AuthUserID = null
            };

            if (isEmployeeUserName == 0 || oldUserLink == null)
            {
                ctx.UserLink.RemoveRange(oldLinks);
                ctx.UserLink.Add(newlink);
            }
            else
            {
                oldUserLink.TempUserID = inviteResp.ID;
                oldUserLink.UserAccessType = inviteRequest.UserAccessType;
                oldUserLink.RightsGroupListID = inviteRequest.RightsGroupListID;
                oldUserLink.UserName = inviteRequest.Email;
                oldUserLink.AuthUserID = null;
            }
            await ctx.SaveChangesAsync();

            await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnUpdate(emp) });

            if (returnUserLink)
            {
                return new UserLinkEntityChangeResponse()
                {
                    Success = true,
                    Id = emp.ID,
                    UserLink = newlink
                };
            }
            else
            {
                return new EntityActionChangeResponse<short>()
                {
                    Success = true,
                    Id = emp.ID,
                    Message = inviteResp.ID.ToString()
                };
            }
        }

        /// <summary>
        /// Revoke Access for an employee
        /// </summary>
        /// <param name="ID">EmployeeData ID</param>
        /// <param name="options">Options</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse<short>> RevokeAccess(short ID, EndorOptions options)
        {
            EmployeeData emp = await GetAsync(ID, new EmployeeIncludes() { Location = IncludesLevel.Full });
            if (emp == null)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    Id = -1,
                    ErrorMessage = "Employee not found",
                };
            }
            var currentLinks = ctx.UserLink.Where(l => l.BID == this.BID && l.EmployeeID == emp.ID);
            foreach (var link in currentLinks)
            {
                using (var client = new HttpClient())
                {
                    HttpResponseMessage response = null;
                    if (link.TempUserID != null)
                    {
                        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, options.AuthOrigin + "/user/revokeinvite");
                        request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToBase64String(Encoding.UTF8.GetBytes(options.TenantSecret)));
                        request.Content = new StringContent(JsonConvert.SerializeObject(new
                        {
                            invite = link.TempUserID
                        }), Encoding.UTF8, "application/json");

                        response = await client.SendAsync(request);
                    }
                    else if (link.AuthUserID != null)
                    {
                        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, options.AuthOrigin + "/user/revokeaccess");
                        request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToBase64String(Encoding.UTF8.GetBytes(options.TenantSecret)));
                        request.Content = new StringContent(JsonConvert.SerializeObject(new
                        {
                            authuserid = link.AuthUserID,
                            bid = link.BID
                        }), Encoding.UTF8, "application/json");

                        response = await client.SendAsync(request);
                    }

                    string responseString = await response.Content.ReadAsStringAsync();
                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                        return new EntityActionChangeResponse<short>()
                        {
                            Success = false,
                            Id = -1,
                            Message = "Unauthorized",
                        };

                    if (response.StatusCode != HttpStatusCode.OK)
                        return new EntityActionChangeResponse<short>()
                        {
                            Success = false,
                            Id = -1,
                            ErrorMessage = responseString,
                        };
                }
                ctx.UserLink.Remove(link);
                ctx.DashboardData.RemoveRange(ctx.DashboardData.Where(t => t.BID == BID && t.UserLinkID == link.ID));
            }

            try
            {
                await ctx.SaveChangesAsync();

                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnUpdate(emp) });

                return new EntityActionChangeResponse<short>()
                {
                    Success = true,
                    Id = emp.ID,
                    Message = "Successfully removed access"
                };
            }
            catch (Exception e)
            {
                await logger.Error(BID, "There was an error while removing access", e);
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    Id = emp.ID,
                    Message = "There was an error while removing access"
                };
            }

        }


        internal async Task<TimeCardDetailStatus[]> GetRecentTimeCardStatuses(int employeeID)
        {
            //lookup our time card status options using this helper
            var lookup = await TimeCardStatusOptionsLookup.Lookup(BID, ctx, migrationHelper);

            //we want to look at one specific employee's timecards
            IQueryable<TimeCardDetail> queryEmployeesTimecards = this.ctx.TimeCardDetail
                .Where(x => x.BID == this.BID && x.EmployeeID == employeeID)
                .Include(a => a.TimeClockBreak) //expand these so we can see their names
                .Include(a => a.TimeClockActivity)
                .Include(a => a.OrderItemStatus);

            //there are 3 options that enable/disable specific statuses
            //so we have options A/B/C
            //and I can't find a great way to chain OR statements
            //so we're going to brute force it, and no I don't like it either
            //if A&B&C
            if (lookup.AllowBreaks && lookup.AllowCustomStatus && lookup.AllowItemStatus)
                queryEmployeesTimecards = queryEmployeesTimecards.Where(x => x.TimeClockBreakID != null || x.TimeClockActivityID != null || x.OrderItemStatusID != null);
            //if A&B
            else if (lookup.AllowBreaks && lookup.AllowCustomStatus)
                queryEmployeesTimecards = queryEmployeesTimecards.Where(x => x.TimeClockBreakID != null || x.TimeClockActivityID != null);
            //if B&C
            else if (lookup.AllowCustomStatus && lookup.AllowItemStatus)
                queryEmployeesTimecards = queryEmployeesTimecards.Where(x => x.TimeClockActivityID != null || x.OrderItemStatusID != null);
            //if A&C
            else if (lookup.AllowBreaks && lookup.AllowItemStatus)
                queryEmployeesTimecards = queryEmployeesTimecards.Where(x => x.TimeClockBreakID != null || x.OrderItemStatusID != null);
            //if A
            else if (lookup.AllowBreaks)
                queryEmployeesTimecards = queryEmployeesTimecards.Where(x => x.TimeClockBreakID != null);
            //if B
            else if (lookup.AllowCustomStatus)
                queryEmployeesTimecards = queryEmployeesTimecards.Where(x => x.TimeClockActivityID != null);
            //if C
            else if (lookup.AllowItemStatus)
                queryEmployeesTimecards = queryEmployeesTimecards.Where(x => x.OrderItemStatusID != null);

            //now that we have filtered our timecards
            //we order by startDT
            var orderedQuery = queryEmployeesTimecards
                .OrderByDescending(x => x.StartDT);

            //then we actually execute the query
            List<TimeCardDetail> list = await orderedQuery
                .ToListAsync();

            //after getting data
            //we use GroupBy to select unique ones
            //then take top 5
            var orderedList = list.GroupBy(y => new { y.OrderItemStatusID, y.TimeClockActivityID, y.TimeClockBreakID })
                .Select(z => z.First()).Take(5);

            //then we turn these into statuses
            return orderedList.Select(t => t.ToStatus(lookup)).ToArray();
        }

        /// <summary>
        /// Make a temporary user permanent - this should only be called during new user invite process
        /// </summary>
        /// <param name="GUID">User invite GUID</param>
        /// <param name="userID">new User ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse<int>> MakePermanent(Guid GUID, int userID)
        {

            var currentLinks = ctx.UserLink.Where(l => l.TempUserID == GUID);
            if ((currentLinks == null) || (currentLinks.Count() == 0))
            {
                return new EntityActionChangeResponse<int>()
                {
                    Success = false,
                    Id = -1,
                    ErrorMessage = "User Invite Not Found",
                };
            }
            await currentLinks.ForEachAsync(x => { x.AuthUserID = userID; x.TempUserID = null; });
            await ctx.SaveChangesAsync();

            return new EntityActionChangeResponse<int>()
            {
                Success = true,
                Id = userID,
                Message = "Employee has been made permanent"
            };
        }

        #region Permissions Related Methods

        /// <summary>
        /// Send reset password email
        /// </summary>
        /// <param name="ID">EmployeeData ID</param>
        /// <param name="options">Options</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse<short>> SendResetPasswordEmail(short ID, EndorOptions options)
        {
            EmployeeData emp = await GetAsync(ID, new EmployeeIncludes() { Location = IncludesLevel.Full });
            if (emp == null)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    Id = -1,
                    ErrorMessage = "Employee not found",
                };
            }
            var currentLinks = ctx.UserLink.Where(l => l.BID == this.BID && l.EmployeeID == emp.ID);
            foreach (var link in currentLinks)
            {
                using (var client = new HttpClient())
                {
                    HttpResponseMessage response = null;
                    if (link.AuthUserID != null)
                    {
                        // dynamic requestObj = new JObject();
                        // requestObj.AuthUserID = link.AuthUserID;
                        // requestObj.BID = link.BID;

                        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, options.AuthOrigin + "/user/resetpassword");
                        request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToBase64String(Encoding.UTF8.GetBytes(options.TenantSecret)));
                        request.Content = new StringContent(JsonConvert.SerializeObject(new
                        {
                            UserID = 0,
                            Username = link.UserName,
                            BID = link.BID
                        }), Encoding.UTF8, "application/json");

                        response = await client.SendAsync(request);
                    }

                    string responseString = await response.Content.ReadAsStringAsync();
                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                        return new EntityActionChangeResponse<short>()
                        {
                            Success = false,
                            Id = -1,
                            Message = "Unauthorized",
                        };

                    if (response.StatusCode != HttpStatusCode.OK)
                        return new EntityActionChangeResponse<short>()
                        {
                            Success = false,
                            Id = -1,
                            ErrorMessage = responseString,
                        };
                }
            }

            return new EntityActionChangeResponse<short>()
            {
                Success = true,
                Id = emp.ID,
                Message = "Successfully sent reset password email."
            };
        }

        /// <summary>
        /// Links or Unlinks a RightsGroupList to an Employee
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="rightsGroupListID">RightsGroupList.ID</param>
        /// <param name="isLinked">If it is to link or unlink</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse<short>> LinkRightsGroupList(short employeeID, short rightsGroupListID, bool isLinked)
        {
            try
            {
                var rightsGroupList = await ctx.RightsGroupList.Where(x => x.BID == this.BID && x.ID == rightsGroupListID).FirstOrDefaultAsync();
                var employee = await GetAsync(employeeID, new EmployeeIncludes() { Location = IncludesLevel.Full });

                // check RightsGroupList specified is valid
                if (rightsGroupList == null)
                    throw new Exception($"Invalid RightsGroupList specified. RightsGroupListID={rightsGroupListID}");

                // check RightsGroup specified is valid
                if (employee == null)
                    throw new Exception($"Invalid Employee specified. EmployeeID={employeeID}");

                UserLink existingUserLink;
                if (isLinked)
                {
                    existingUserLink = await ctx.UserLink.Where(x => x.BID == this.BID && x.EmployeeID == employee.ID).FirstOrDefaultAsync();
                    // check Userlink can be found
                    if (existingUserLink == null)
                        throw new Exception($"Userlink could not be found. EmployeeID={employeeID}");

                    existingUserLink.RightsGroupListID = rightsGroupListID == 0 ? (short?)null : rightsGroupListID;
                }
                else
                {
                    existingUserLink = await ctx.UserLink.Where(x => x.BID == this.BID && x.EmployeeID == employee.ID && x.RightsGroupListID == rightsGroupListID).FirstOrDefaultAsync();
                    // check Userlink can be found
                    if (existingUserLink == null)
                        throw new Exception($"Userlink could not be found. EmployeeID={employeeID} with RightsGroupListID={rightsGroupListID}");

                    existingUserLink.RightsGroupListID = null;
                }

                var affectedRows = await ctx.SaveChangesAsync();
                if (affectedRows > 0)
                {
                    var refershEntities = new List<RefreshEntity>();
                    refershEntities.AddRange(DoGetRefreshMessagesOnUpdate(employee));
                    refershEntities.Add(GetRefreshEntityForUserlink(existingUserLink));
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refershEntities });
                    return new EntityActionChangeResponse<short>()
                    {
                        Id = rightsGroupListID,
                        Message = $"Successfully {(isLinked ? "linked" : "unlinked")} Employee:{employeeID} to RightsGroupList:{rightsGroupListID}",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        Id = rightsGroupListID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        public async Task SendEmployeeUserLinkActiveInactiveRefreshMessage(short employeeID, bool isActivating)
        {
            var employee = await GetAsync(employeeID);
            var existingUserLink = await ctx.UserLink.Where(x => x.BID == this.BID && x.EmployeeID == employee.ID).FirstOrDefaultAsync();
            
            if(existingUserLink != null)
            {
                var refershEntities = new List<RefreshEntity>
                {
                    GetRefreshEntityForUserlink(existingUserLink, !isActivating)
                };
                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refershEntities });
            }
        }

        /// <summary>
        /// Sets UserAccessType of an Employee
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="userAccessType">UserAccessType</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse<short>> SetUserAccessType(short employeeID, UserAccessType userAccessType)
        {
            try
            {
                var employee = await GetAsync(employeeID, new EmployeeIncludes() { });
                UserLink existingUserLink = await ctx.UserLink.Where(x => x.BID == this.BID && x.EmployeeID == employee.ID).FirstOrDefaultAsync();

                // check Userlink can be found
                if (existingUserLink == null)
                    throw new Exception($"Userlink could not be found. EmployeeID={employeeID}");

                existingUserLink.UserAccessType = userAccessType;

                var affectedRows = await ctx.SaveChangesAsync();

                if (affectedRows > 0)
                {
                    var refreshEntities = new List<RefreshEntity>();
                    
                    refreshEntities.AddRange(DoGetRefreshMessagesOnUpdate(employee));
                    refreshEntities.Add(GetRefreshEntityForUserlink(existingUserLink));

                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshEntities });

                    return new EntityActionChangeResponse<short>()
                    {
                        Id = employeeID,
                        Message = $"Successfully updated userAccessType.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        Id = employeeID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }

            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        private RefreshEntity GetRefreshEntityForUserlink(UserLink userLink, bool isInactivate = false)
        {
            return new RefreshEntity
            {
                BID = BID,
                ClasstypeID = (int)ClassType.UserLink,
                DateTime = DateTime.UtcNow,
                ID = userLink.ID,
                RefreshMessageType = !isInactivate ? RTM.Enums.RefreshMessageType.Change : RTM.Enums.RefreshMessageType.Delete,
                Data = userLink.UserName
            };
        }

        #endregion

        /// <summary>
        /// Gets Time Card objects specific for this employee
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="onlyOpen">If it is to return open or close timecards</param>
        /// <param name="startDT">start DateTime of timecards to pull</param>
        /// <param name="endDT">end DateTime of timecards to pull</param>
        /// <param name="includeDetails">If it is to return with details or not</param>
        /// <returns></returns>
        public async Task<List<TimeCard>> GetTimeCards(int employeeID, bool? onlyOpen = false, DateTime? startDT = null, DateTime? endDT = null, bool? includeDetails = false)
        {
            var timeCardSvc = new TimeCardService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);

            var timeCardFilter = new TimeCardFilter()
            {
                EmployeeID = employeeID,
                OnlyOpen = onlyOpen,
                StartDT = startDT,
                EndDT = endDT,
                IncludeDetails = includeDetails
            };
            var timeCardList = await timeCardSvc.GetWithFiltersAsync(timeCardFilter);
            return timeCardList;
        }

        /// <summary>
        /// Gets Time Card Summary specific for this employee
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <returns></returns>
        public async Task<GenericResponse<EmployeeTimeCardSummaryData>> GetTimeCardSummary(int employeeID)
        {
            SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
            {
                Direction = System.Data.ParameterDirection.Output
            };

            object[] myParams = {
                new SqlParameter("@BID", BID),
                new SqlParameter("@EmployeeID", employeeID),
                new SqlParameter("@WeekStartDT", SundayMorning()),
                new SqlParameter("@DayStartDT", DateTime.UtcNow),
                resultParam
            };
            var rowResult = await ctx.Database.GetModelFromQueryAsync<EmployeeTimeCardSummaryData>("EXEC @Result = dbo.[Employee.TimeClock.HoursThisWeek] @BID, @EmployeeID, @WeekStartDT, @DayStartDT;", parameters: myParams);

            if (rowResult.Count() == 0)
            {
                return new GenericResponse<EmployeeTimeCardSummaryData>
                {
                    Data = null,
                    Message = "No result found.",
                    Success = true
                };
            }
            else
            {
                return new GenericResponse<EmployeeTimeCardSummaryData>
                {
                    Data = rowResult.FirstOrDefault(),
                    Message = "Successfully retrieved Time Card Summary Value.",
                    Success = true
                };
            }
        }

        /// <summary>
        /// Get TimeCard Info specific for this employee
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <returns></returns>
        public async Task<EmployeeNotificationsData> GetTimeCardInfo(short? employeeID)
        {
            SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
            {
                Direction = System.Data.ParameterDirection.Output
            };

            object[] myParams = {
                new SqlParameter("@BID", BID),
                new SqlParameter("@EmployeeID", employeeID),
                resultParam
            };

            var rowResult = await ctx.Database.GetModelFromQueryAsync<EmployeeNotificationsInfoData>("EXEC @Result = dbo.[Employee.GetNotificationInfo] @BID, @EmployeeID;", parameters: myParams);

            var timeCardDetailModel = await ctx.TimeCardDetail.Where(tcd => tcd.BID == BID && tcd.ID == rowResult.FirstOrDefault().TimeCardDetailID).FirstOrDefaultAsync();

            string statusType = null;

            if (timeCardDetailModel != null)
            {
                if (timeCardDetailModel.OrderItemStatusID != null)
                {
                    statusType = "order";
                }

                if (timeCardDetailModel.TimeClockActivityID != null)
                {
                    statusType = "activity";
                }

                if (timeCardDetailModel.TimeClockBreakID != null)
                {
                    statusType = "break";
                }
            }

            var TimeCardInfo = new TimeCardInfoData
            {
                ID = rowResult.FirstOrDefault().TimeCardID,
                DetailID = rowResult.FirstOrDefault().TimeCardDetailID,
                StatusType = statusType,
                StatusName = rowResult.FirstOrDefault().StatusName
            };

            return new EmployeeNotificationsData()
            {
                UnreadMessageCount = rowResult.FirstOrDefault().UnreadMessageCount,
                UnreadAlertCount = rowResult.FirstOrDefault().UnreadAlertCount,
                EmployeesViewingPage = new EmployeeViewingPageData[]
                {
                    new EmployeeViewingPageData()
                    {
                        ID = 52,
                        Initials = "JS"
                    },
                    new EmployeeViewingPageData()
                    {
                        ID = 221,
                        Initials = "PK"
                    }
                },
                BannerMessage = new Random().Next(1, 6) == 1 ? "Trial expires in 5 days. Click <a ref=\"www.corebridgesoftware.net\"> here </a> to sign up and keep your account current." : null,

                IsLoggedIn = rowResult.FirstOrDefault().IsLoggedIn,

                TimeCardInfo = TimeCardInfo
            };
        }

        /// <summary>
        /// Return Sunday morning of the current week
        /// </summary>
        private DateTime SundayMorning()
        {
            int diff = (7 + (DateTime.UtcNow.DayOfWeek - DayOfWeek.Sunday)) % 7;
            return DateTime.UtcNow.AddDays(-1 * diff).Date;
        }

        /// <summary>
        /// Create a new TimeCard Object and Clocks the employee in for the day at the current time
        /// If any of the optional parameters are specified, also creates a TimeCardDetail Object with the specified information.
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="longitude">Longitude</param>
        /// <param name="orderItemStatusID">OrderItemStatusID</param>
        /// <param name="breakActivityID">BreakActivityID</param>
        /// <param name="timeClockActivityID">TimeClockActivityID</param>
        /// <param name="orderItemID">OrderItemID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse<short>> ClockInEmployee(short employeeID, decimal? latitude, decimal? longitude, short? orderItemStatusID, short? breakActivityID, short? timeClockActivityID, short? orderItemID)
        {
            try
            {
                var timeCardSvc = new TimeCardService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);

                int? alreadyClockedIn = await ctx.TimeCard.Where(tc => tc.BID == BID && tc.EmployeeID == employeeID && tc.IsClosed == false).Select(tc => tc.EmployeeID).FirstOrDefaultAsync();

                if (alreadyClockedIn.HasValue && alreadyClockedIn.Value != 0)
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        ErrorMessage = "The employee already has an open TimeCard entry",
                        Success = false,
                    };
                }

                decimal? latitudeResult = latitude.HasValue
                               ? (decimal?)Math.Round(latitude.Value, 10)
                               : null;

                decimal? longitudeResult = longitude.HasValue
                               ? (decimal?)Math.Round(longitude.Value, 10)
                               : null;

                var timeCardModel = new TimeCard()
                {
                    BID = this.BID,
                    EmployeeID = employeeID,
                    StartDT = DateTime.UtcNow,
                    LatStart = latitudeResult,
                    LongStart = longitudeResult
                };

                var newlyCreatedTimeCard = await timeCardSvc.CreateAsync(timeCardModel);


                if (latitude.HasValue || longitude.HasValue || orderItemStatusID.HasValue || breakActivityID.HasValue || timeClockActivityID.HasValue || orderItemID.HasValue)
                {
                    short? orderItemStatusIDResult = orderItemStatusID.HasValue && orderItemStatusID.Value != 0 ? (short?)orderItemStatusID.Value : null;
                    short? breakActivityIDResult = breakActivityID.HasValue && breakActivityID.Value != 0 ? (short?)breakActivityID.Value : null;
                    short? timeClockActivityIDResult = timeClockActivityID.HasValue && timeClockActivityID.Value != 0 ? (short?)timeClockActivityID.Value : null;
                    short? orderItemIDResult = orderItemID.HasValue && orderItemID.Value != 0 ? (short?)orderItemID.Value : null;

                    var paramArray = new List<short?>
                    {
                        orderItemStatusIDResult ?? 0,
                        breakActivityIDResult ?? 0,
                        timeClockActivityIDResult ?? 0
                    };

                    var filteredParamArray = paramArray.Where(pa => pa != 0).ToArray();

                    if (filteredParamArray.Length > 1)
                    {
                        return new EntityActionChangeResponse<short>()
                        {
                            Success = false,
                            ErrorMessage = "Only zero or one of the TimeClockActivityID, BreakActivityID, and OrderItemStatusID may specified"
                        };
                    }

                    var timeCardDetailSvc = new TimeCardDetailService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);

                    var timeCardDetailModel = new TimeCardDetail()
                    {
                        BID = this.BID,
                        EmployeeID = employeeID,
                        StartDT = DateTime.UtcNow,
                        LatStart = latitudeResult,
                        LongStart = longitudeResult,
                        OrderItemStatusID = orderItemStatusIDResult,
                        TimeClockBreakID = breakActivityIDResult,
                        TimeClockActivityID = timeClockActivityIDResult,
                        OrderItemID = orderItemIDResult,
                        TimeCardID = newlyCreatedTimeCard.ID
                    };

                    await timeCardDetailSvc.CreateAsync(timeCardDetailModel);
                }

                return new EntityActionChangeResponse<short>()
                {
                    Success = true,
                    Message = "Employee successfully clocked in"
                };
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        /// <summary>
        /// Create a new TimeCard Object and Clocks the employee in for the day at the current time
        /// If any of the optional parameters are specified, also creates a TimeCardDetail Object with the specified information.
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="longitude">Longitude</param>
        /// <param name="connectionID">ConnectionIDHeaderKey</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse<short>> ClockOutEmployee(short employeeID, decimal? latitude, decimal? longitude, string connectionID)
        {
            try
            {

                var alreadyClockedIn = await ctx.TimeCard.Where(tc => tc.BID == BID && tc.EmployeeID == employeeID && tc.IsClosed == false).FirstOrDefaultAsync();
                var clockedOutDT = DateTime.UtcNow;

                if (alreadyClockedIn == null)
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        ErrorMessage = "The employee did not have an open TimeCard",
                        Success = false,
                    };
                }

                decimal? latitudeResult = latitude.HasValue
                               ? (decimal?)Math.Round(latitude.Value, 10)
                               : null;

                decimal? longitudeResult = longitude.HasValue
                               ? (decimal?)Math.Round(longitude.Value, 10)
                               : null;

                var timeCardSvc = new TimeCardService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);

                alreadyClockedIn.LatEnd = latitudeResult;
                alreadyClockedIn.LongEnd = longitudeResult;
                alreadyClockedIn.EndDT = clockedOutDT;

                await timeCardSvc.UpdateAsync(alreadyClockedIn, connectionID);

                var timeCardDetailModel = ctx.TimeCardDetail.Where(tcd => tcd.BID == BID && tcd.EmployeeID == employeeID && tcd.TimeCardID == alreadyClockedIn.ID && tcd.IsClosed == false).ToList();

                timeCardDetailModel.ForEach(tcd =>
                {
                    tcd.LatEnd = latitudeResult;
                    tcd.LongEnd = longitudeResult;
                    tcd.EndDT = clockedOutDT;
                });

                await ctx.SaveChangesAsync();

                return new EntityActionChangeResponse<short>()
                {
                    Success = true,
                    Message = "Employee successfully clocked out"
                };
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        /// <summary>
        /// Create a new TimeCard Object and Clocks the employee in for the day at the current time
        /// If any of the optional parameters are specified, also creates a TimeCardDetail Object with the specified information.
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="longitude">Longitude</param>
        /// <param name="orderItemStatusID">OrderItemStatusID</param>
        /// <param name="breakActivityID">BreakActivityID</param>
        /// <param name="timeClockActivityID">TimeClockActivityID</param>
        /// <param name="orderItemID">OrderItemID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse<short>> ClockOnTaskEmployee(short employeeID, decimal? latitude, decimal? longitude, short? orderItemStatusID, short? breakActivityID, short? timeClockActivityID, short? orderItemID)
        {
            try
            {
                var alreadyClockedIn = await ctx.TimeCard.Where(tc => tc.BID == BID && tc.EmployeeID == employeeID && tc.IsClosed == false).FirstOrDefaultAsync();
                var timeCardDetailSvc = new TimeCardDetailService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);

                if (alreadyClockedIn == null)
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        ErrorMessage = "The employee did not have an open TimeCard",
                        Success = false,
                    };
                }

                decimal? latitudeResult = latitude.HasValue
                               ? (decimal?)Math.Round(latitude.Value, 10)
                               : null;

                decimal? longitudeResult = longitude.HasValue
                               ? (decimal?)Math.Round(longitude.Value, 10)
                               : null;

                short? orderItemStatusIDResult = orderItemStatusID.HasValue && orderItemStatusID.Value != 0 ? (short?)orderItemStatusID.Value : null;
                short? breakActivityIDResult = breakActivityID.HasValue && breakActivityID.Value != 0 ? (short?)breakActivityID.Value : null;
                short? timeClockActivityIDResult = timeClockActivityID.HasValue && timeClockActivityID.Value != 0 ? (short?)timeClockActivityID.Value : null;
                short? orderItemIDResult = orderItemID.HasValue && orderItemID.Value != 0 ? (short?)orderItemID.Value : null;

                var paramArray = new List<short?>
                    {
                        orderItemStatusIDResult ?? 0,
                        breakActivityIDResult ?? 0,
                        timeClockActivityIDResult ?? 0
                    };

                var filteredParamArray = paramArray.Where(pa => pa != 0).ToArray();

                if (filteredParamArray.Length > 1)
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        ErrorMessage = "Only zero or one of the TimeClockActivityID, BreakActivityID, and OrderItemStatusID may specified"
                    };
                }


                var timeCardDetailModel = new TimeCardDetail()
                {
                    BID = this.BID,
                    EmployeeID = employeeID,
                    StartDT = DateTime.UtcNow,
                    LatStart = latitudeResult,
                    LongStart = longitudeResult,
                    OrderItemStatusID = orderItemStatusIDResult,
                    TimeClockBreakID = breakActivityIDResult,
                    TimeClockActivityID = timeClockActivityIDResult,
                    OrderItemID = orderItemIDResult,
                    TimeCardID = alreadyClockedIn.ID
                };

                await timeCardDetailSvc.CreateAsync(timeCardDetailModel);

                return new EntityActionChangeResponse<short>()
                {
                    Success = true,
                    Message = "Employee successfully clocked on task"
                };
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        /// <summary>
        /// Closes any open TimeCardDetail Objects (there can be more than one) by setting the CloseDT to the current time.
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="longitude">Longitude</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse<short>> ClockOffTaskEmployee(short employeeID, decimal? latitude, decimal? longitude)
        {
            try
            {

                var alreadyClockedIn = await ctx.TimeCard.Where(tc => tc.BID == BID && tc.EmployeeID == employeeID && tc.IsClosed == false).FirstOrDefaultAsync();
                var clockedOutDT = DateTime.UtcNow;

                if (alreadyClockedIn == null)
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        ErrorMessage = "The employee did not have an open TimeCard",
                        Success = false,
                    };
                }

                decimal? latitudeResult = latitude.HasValue
                               ? (decimal?)Math.Round(latitude.Value, 10)
                               : null;

                decimal? longitudeResult = longitude.HasValue
                               ? (decimal?)Math.Round(longitude.Value, 10)
                               : null;

                var timeCardDetailModel = ctx.TimeCardDetail.Where(tcd => tcd.BID == BID && tcd.EmployeeID == employeeID && tcd.TimeCardID == alreadyClockedIn.ID && tcd.IsClosed == false).ToList();

                timeCardDetailModel.ForEach(tcd =>
                {
                    tcd.LatEnd = latitudeResult;
                    tcd.LongEnd = longitudeResult;
                    tcd.EndDT = clockedOutDT;
                });

                await ctx.SaveChangesAsync();

                return new EntityActionChangeResponse<short>()
                {
                    Success = true,
                    Message = "Employee successfully clocked off task"
                };
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        /// <summary>
        /// GetEmployeeNotificationInfo
        /// </summary>
        /// <param name="EmployeeID"></param>
        public async Task<EmployeeNotificationInfo> GetEmployeeNotificationInfo(short EmployeeID)
        {
            string sqlExecute = "EXEC dbo.[Employee.GetNotificationInfo] @BID, @EmployeeID";
            var paramBID = new SqlParameter("@BID", this.BID);
            var paramEmployeeID = new SqlParameter("@EmployeeID", EmployeeID);
            var result = (await this.ctx
                                .Database
                                .GetModelFromQueryAsync<EmployeeNotificationInfo>(sqlExecute, paramBID, paramEmployeeID)).FirstOrDefault();
            return result;
        }

        /// <summary>
        /// Get all employees without the included defaults
        /// </summary>
        public async Task<List<EmployeeList>> GetEmployees()
        {
            var result = await ctx.EmployeeData.Where(x => x.BID == this.BID).Select(y => new EmployeeList()
            {
                ID = y.ID,
                LongName = y.LongName,
                Position = y.Position,
                IsActive = y.IsActive
            }).ToListAsync();
            return result;
        }

        /// <summary>
        /// Sets an Employee's default Email account
        /// </summary>
        /// <param name="employeeID">Employee's ID</param>
        /// <param name="emailAccountID">Email Account's ID</param>
        /// <returns></returns>
        public async Task<BooleanResponse> SetDefaultEmailAccount(short employeeID, short emailAccountID)
        {
            var employee = await GetAsync(employeeID);
            if (employee == null)
            {
                return new BooleanResponse()
                {
                    Success = false,
                    ErrorMessage = $"Employee with ID `{employeeID} not found.",
                    Message = $"Employee with ID `{employeeID} not found."
                };
            }

            var emailAccount = await ctx.EmailAccountData.FirstOrDefaultAsync(a => a.BID == BID && a.ID == emailAccountID);
            if (emailAccount == null)
            {
                return new BooleanResponse()
                {
                    Success = false,
                    ErrorMessage = $"EmailAccount with ID `{emailAccountID} not found.",
                    Message = $"EmailAccount with ID `{emailAccountID} not found."
                };
            }

            if (!emailAccount.IsActive)
            {
                return new BooleanResponse()
                {
                    Success = false,
                    ErrorMessage = $"Forbidden: EmailAccount with ID `{emailAccountID} is not active.",
                    Message = $"Forbidden: EmailAccount with ID `{emailAccountID} is not active."
                };
            }
            else
            {
                employee.DefaultEmailAccountID = emailAccountID;
                ctx.EmployeeData.Update(employee);
                await ctx.SaveChangesAsync();

                return new BooleanResponse()
                {
                    Success = true,
                    Message = $"Successfully set Default Email Account."
                };
            }
        }

        /// <summary>
        /// Clears an Employee's default Email account
        /// </summary>
        /// <param name="employeeID">Employee's ID</param>
        /// <returns></returns>
        public async Task<BooleanResponse> ClearDefaultEmailAccount(short employeeID)
        {
            var employee = await GetAsync(employeeID);
            if (employee == null)
            {
                return new BooleanResponse()
                {
                    Success = false,
                    ErrorMessage = $"Employee with ID `{employeeID} not found.",
                    Message = $"Employee with ID `{employeeID} not found."
                };
            }

            employee.DefaultEmailAccountID = null;
            ctx.EmployeeData.Update(employee);
            await ctx.SaveChangesAsync();

            return new BooleanResponse()
            {
                Success = true,
                Message = $"Successfully cleared Default Email Account."
            };
        }

        /// <summary>
        /// Force Delete
        /// </summary>
        /// <param name="EmployeeID">ID of the Employee to Delete</param>
        /// <param name="userLinkID">UserLink.ID</param>
        public async Task<EntityActionChangeResponse<short>> ForceDeleteEmployee(short EmployeeID, short userLinkID)
        {
            try
            {
                UserLink userLinkData = await ctx.UserLink.Where(ul => ul.BID == this.BID && ul.ID == userLinkID).FirstOrDefaultAsync();

                if (userLinkData == null)
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        ErrorMessage = "User not found"
                    };
                }

                if (userLinkData.UserAccessType < UserAccessType.SupportManager)
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        ErrorMessage = "Access denied."
                    };
                }

                EmployeeData toDelete = await this.GetAsync(EmployeeID);

                if (toDelete == null)
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        ErrorMessage = "Employee not found"
                    };
                }

                List<RefreshEntity> refreshes = DoGetRefreshMessagesOnDelete(toDelete);

                refreshes.AddRange(await ctx.AlertDefinition.RemoveRangeWhereAtomAsync(this.BID, x => x.EmployeeID == EmployeeID));

                ctx.BoardEmployeeLink.RemoveRange(await ctx.BoardEmployeeLink.Where(x => x.BID == BID && x.EmployeeID == EmployeeID).ToArrayAsync());
                ctx.EmployeeTeamLink.RemoveRange(await ctx.EmployeeTeamLink.Where(x => x.BID == BID && x.EmployeeID == EmployeeID).ToArrayAsync());

                refreshes.AddRange(await ctx.EmployeeLocator.RemoveRangeWhereAtomAsync(this.BID, x => x.ParentID == EmployeeID));

                refreshes.AddRange(await ctx.TimeCard.RemoveRangeWhereAtomAsync(this.BID, x => x.EmployeeID == EmployeeID));
                refreshes.AddRange(await ctx.TimeCardDetail.RemoveRangeWhereAtomAsync(this.BID, x => x.EmployeeID == EmployeeID));

                var participants = await ctx.MessageParticipantInfo.Where(x => x.BID == BID && x.EmployeeID == EmployeeID).ToArrayAsync();
                ctx.MessageParticipantInfo.RemoveRange(participants);
                int[] participantIDs = participants.Select(x => x.ID).ToArray();
                ctx.MessageDeliveryRecord.RemoveRange(await ctx.MessageDeliveryRecord.Where(x => x.BID == BID && participantIDs.Contains(x.ParticipantID)).ToArrayAsync());
                ctx.MessageHeader.RemoveRange(await ctx.MessageHeader.Where(x => x.BID == BID && participantIDs.Contains(x.ParticipantID)).ToArrayAsync());
                ctx.MessageParticipantInfo.RemoveRange(await ctx.MessageParticipantInfo.Where(x => x.BID == BID && x.EmployeeID == EmployeeID).ToArrayAsync());
                ctx.MessageHeader.RemoveRange(await ctx.MessageHeader.Where(x => x.BID == BID && x.EmployeeID == EmployeeID).ToArrayAsync());
                refreshes.AddRange(await ctx.MessageHeader.RemoveRangeWhereAtomAsync(this.BID, x => x.EmployeeID == EmployeeID || participantIDs.Contains(x.ParticipantID)));


                refreshes.AddRange(await ctx.UserLink.RemoveRangeWhereAsync(this.BID, ClassType.UserLink, x => x.EmployeeID == EmployeeID, y => y.ID));

                ctx.EmployeeData.Remove(toDelete);

                var save = ctx.SaveChanges();

                if (save > 0)
                {
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshes });
                    await taskQueuer.IndexModel(refreshes);

                    return new EntityActionChangeResponse<short>()
                    {
                        Success = true,
                        Message = "Successfully Deleted"
                    };
                }
                else
                {
                    return new EntityActionChangeResponse<short>()
                    {
                        Success = false,
                        ErrorMessage = "Nothing to delete"
                    };
                }
            }
            catch (DbUpdateException exc)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    ErrorMessage = exc.InnerException.Message
                };
            }
            catch (Exception exc)
            {
                return new EntityActionChangeResponse<short>()
                {
                    Success = false,
                    ErrorMessage = exc.ToString()
                };
            }
        }


        /// <summary>
        /// Checks if the Email Account can be deleted
        /// </summary>
        /// <param name="id">Team ID to check for</param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(short id)
        {
            var resp = await base.CanDelete(id);
            // allowed to proceed despite Part.QuickItem.CategoryLinks
            var pattern = @"\[(.*?)\]";
            if ((resp.Value.HasValue && resp.Value.Value == false))
            {
                var matches = Regex.Matches(resp.Message, pattern);
                bool foundDeletionIssue = false;
                foreach (Match m in matches)
                {
                    if (!m.ToString().Contains("User.Link")) foundDeletionIssue = true;
                }
                if (!foundDeletionIssue)
                {
                    resp.Value = true;
                    resp.Message = "Can Delete Employee";
                }
            }
            return resp;
        }

        /// <summary>
        /// Get Employee LocationID
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public async Task<byte> GetLocationID(short employeeID)
        {
            try
            {
                var emp = await ctx.EmployeeData.FirstOrDefaultPrimaryAsync(BID, employeeID);
                return emp != null ? emp.LocationID : Convert.ToByte(0);
            }
            catch (Exception ex)
            {
                throw new Exception($"GetLocationID: {employeeID} -  {ex.Message}");
            }
        }

    }
}
