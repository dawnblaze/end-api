﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Responses;
using Endor.Api.Web.Includes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Endor.GLEngine.Classes;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Reconciliation Service
    /// </summary>
    public class ReconciliationService : AtomCRUDService<Reconciliation, int>
    {
        /// <summary>
        /// Reconciliation Service Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper"></param>
        public ReconciliationService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Includes for reconciliation
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null) =>
            includes.ToIncludesArray(this.IncludeDefaults);

        internal async Task<ServiceResponse> GetWithFiltersAsync(ReconciliationFilter filters)
        {
            var baseQuery = ctx.Reconciliation.AsQueryable();

            if (filters.IncludeItems ?? false)
                baseQuery = baseQuery.Include(a => a.Items);

            var result = await baseQuery.WherePrimaryAll(BID, filters.WherePredicates()).ToListAsync();

            return new ServiceResponse()
            {
                Success = true,
                Value = result
            };
        }

        /// <summary>
        /// Reconciliate GL Data by location
        /// </summary>
        /// <param name="locationID"></param>
        /// <param name="enteredByID"></param>
        /// <param name="accountingDT"></param>
        /// <param name="createAdjustments"></param>
        /// <returns></returns>
        internal async Task<List<Reconciliation>> Reconcile(byte? locationID, short? enteredByID, DateTime? accountingDT, bool createAdjustments, List<CashDrawerAdjustment> cashDrawerAdjustments)
        {
            var glEngine = new Endor.GLEngine.GLEngine(this.BID, this.ctx);
            var reconciliations = await glEngine.Reconcile(locationID, enteredByID, accountingDT, createAdjustments, cashDrawerAdjustments, async (ctx, bid, classType) => await RequestIDIntegerAsync(bid, classType));

            await this.ctx.SaveChangesAsync();

            await base.QueueIndexForModels(reconciliations.Select(r => r.ID).ToList());
            List<RefreshEntity> refreshes = new List<RefreshEntity>();
            reconciliations.ForEach(a => refreshes.Add(new RefreshEntity()
            {
                BID = BID,
                ID = a.ID,
                ClasstypeID = (int)ClassType.Reconciliation,
                RefreshMessageType = RefreshMessageType.New
            }));
            await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshes });

            await this.ctx.SaveChangesAsync();

            return reconciliations;
        }

        /// <summary>
        /// ReconciliationtService WherePrimary
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<Reconciliation> WherePrimary(IQueryable<Reconciliation> query, int ID) =>
            query.WherePrimary(this.BID, ID);

        /// <summary>
        /// ReconciliationtService GetChildAssociations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<Reconciliation, int>[] GetChildAssociations() =>
            new IChildServiceAssociation<Reconciliation, int>[0];

        internal override void DoBeforeValidate(Reconciliation newModel) { }

        /// <summary>
        /// Query Reconciliation Simple List
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        internal async Task<IEnumerable<SimpleReconciliation>> SimpleList(SimpleReconciliationFilter filters) =>
            this.SelectWhereAll(filters.WherePredicates(), filters);

        /// <summary>
        /// Tell if the reconciliation can be deleted 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(int id)
        {
            if (await CanDeleteReconciliation(id))
                return new BooleanResponse()
                {
                    Success = true,
                    Value = true
                };

            return new BooleanResponse
            {
                Message = "You can`t delete the reconciliation if It`s not the last created.",
                ErrorMessage = "You can`t delete the reconciliation if It`s not the last created."
            };
        }

        /// <summary>
        /// Custom local rules to delete a reconciliation
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private async Task<bool> CanDeleteReconciliation(int id)
        {
            var tryingToDeleteReconciliation = this.WherePrimary(this.ctx.Reconciliation.AsQueryable(), id).FirstOrDefault();

            if (tryingToDeleteReconciliation == null)
                return false;

            return await IsLastReconciliationForLocation(tryingToDeleteReconciliation.LocationID, id);
        }

        public async Task DeleteReconciliationWithItems(int id)
        {
            var baseQuery = ctx.Reconciliation.Include(a => a.Items);
            var result = await baseQuery.WherePrimary(BID, id).FirstAsync();
            ctx.ReconciliationItem.RemoveRange(result.Items);
            ctx.Reconciliation.Remove(result);
            await ctx.SaveChangesAsync();
        }

        public async Task<List<Reconciliation>> GenerateReconciliationAdjustmentsPreview(byte? locationID, short? enteredByID, DateTime? accountingDT)
        {
            Endor.GLEngine.GLEngine gLEngine = new Endor.GLEngine.GLEngine(this.BID, this.ctx);
            var reconciliationsAdjustments = await gLEngine.GenerateReconciliationAdjustmentsPreview(locationID, enteredByID);
            return reconciliationsAdjustments ?? new List<Reconciliation>();
        }

        public async Task<List<Reconciliation>> GenerateReconciliationPreview(byte? locationID, short? enteredByID, DateTime? accountingDT, EnumIncludeAdjustments? IncludeAdjustments)
        {
            var gLEngine = new GLEngine.GLEngine(this.BID, this.ctx);

            var reconciliations = new List<Reconciliation>();

            switch (IncludeAdjustments)
            {
                case EnumIncludeAdjustments.Yes:
                    var recon = await gLEngine.GenerateReconciliationPreview(locationID, enteredByID, accountingDT) ?? new List<Reconciliation>();
                    var reconciliationsAdjustments = await gLEngine.GenerateReconciliationAdjustmentsPreview(locationID, enteredByID) ?? new List<Reconciliation>();
                    reconciliations.AddRange(reconciliationsAdjustments);
                    reconciliations.AddRange(recon);
                    break;
                case EnumIncludeAdjustments.Only:
                    var reconciliationsAdjustmentsOnly = await gLEngine.GenerateReconciliationAdjustmentsPreview(locationID, enteredByID) ?? new List<Reconciliation>();
                    reconciliations.AddRange(reconciliationsAdjustmentsOnly);
                    break;
                case EnumIncludeAdjustments.No:
                default:
                    var reconOnly = await gLEngine.GenerateReconciliationPreview(locationID, enteredByID, accountingDT) ?? new List<Reconciliation>();
                    reconciliations.AddRange(reconOnly);
                    break;
            }

            return reconciliations;
        }

        /// <summary>
        /// Check if the reconciliation is the last of the location id
        /// </summary>
        /// <param name="locationID"></param>
        /// <returns></returns>
        private async Task<bool> IsLastReconciliationForLocation(byte locationID, int id)
        {
            var lastReconciliationForLocation = await this.Where(a => a.LocationID == locationID).OrderByDescending(a => a.CreatedDT).FirstOrDefaultAsync();
            return lastReconciliationForLocation == null ? true : lastReconciliationForLocation.ID == id;
        }

        /// <summary>
        /// Search the adjustments for reconciliations
        /// </summary>
        /// <param name="locationID"></param>
        /// <returns></returns>
        internal async Task<HasAdjustmentsResponse> HasAdjustments(byte? locationID)
        {
            var glEngine = new GLEngine.GLEngine(this.BID, this.ctx);

            var adjustments = await glEngine.GetAdjustmentsNeeded(locationID);

            return new HasAdjustmentsResponse
            {
                HasAdjustments = adjustments.HasAdjustments,
                Adjustments = adjustments.Adjustments?.Select(a => new HasAdjustmentsResponse.ReconciliationAdjustmentsIntenal
                {
                    LocationID = a.LocationID,
                    StartingDT = a.StartingDT,
                    EndingDT = a.EndingDT
                })?.ToList() ?? new List<HasAdjustmentsResponse.ReconciliationAdjustmentsIntenal>()
            };
        }

        /// <summary>
        /// Returns an array of ReconciliationPaymentInfo Objects for the specified ReconciliationID. 
        /// </summary>
        /// <param name="id">Reconciliation ID</param>
        /// <returns></returns>
        public async Task<GenericResponse<List<ReconciliationPaymentInfo>>> GetPaymentInfo(int id)
        {
            var reconciliation = await ctx.Reconciliation.FirstOrDefaultAsync(x => x.BID == BID && x.ID == id);
            if (reconciliation == null)
                return new GenericResponse<List<ReconciliationPaymentInfo>>()
                {
                    Data = null,
                    Success = false,
                    Message = $"Reconciliation with ID {id} not found.",
                    ErrorMessage = $"Reconciliation with ID {id} not found."
                };

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@BID", SqlDbType = SqlDbType.SmallInt, Value = BID },
                new SqlParameter() { ParameterName = "@ReconciliationID", SqlDbType = SqlDbType.Int, Value = id },
            };

            var result = await ctx.Database.GetModelFromQueryAsync<ReconciliationPaymentInfo>(
                @"SELECT * FROM [Accounting.Reconciliation.PaymentInfo.ByID](@BID, @ReconciliationID);"
                , parameters: sp.ToArray());

            if (result.Count() > 0)
                return new GenericResponse<List<ReconciliationPaymentInfo>>()
                {
                    Data = result.ToList(),
                    Success = true,
                    Message = "Successfully retrieved Reconciliation Payment Info."
                };
            else
                return new GenericResponse<List<ReconciliationPaymentInfo>>()
                {
                    Data = result.ToList(),
                    Success = true,
                    Message = $"No Reconciliation Payment Info found for Reconciliation {id}."
                };
        }

        /// <summary>
        /// Returns an array of ReconciliationPaymentInfo Objects for the specified location. These collect all un-posted payment entries.  
        /// This call does NOT support adjusting entries. They must be requested by specifiying the AdjustedReconciliationID.
        /// </summary>
        /// <param name="locationID">Location ID</param>
        /// <returns></returns>
        public async Task<GenericResponse<List<ReconciliationPaymentInfo>>> GetPreviewPaymentInfo(int locationID)
        {
            var location = await ctx.LocationData.FirstOrDefaultAsync(x => x.BID == BID && x.ID == locationID);
            if (location == null)
                return new GenericResponse<List<ReconciliationPaymentInfo>>()
                {
                    Data = null,
                    Success = false,
                    Message = $"Location with ID {locationID} not found.",
                    ErrorMessage = $"Location with ID {locationID} not found."
                };

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@BID", SqlDbType = SqlDbType.SmallInt, Value = BID },
                new SqlParameter() { ParameterName = "@LocationID", SqlDbType = SqlDbType.Int, Value = locationID },
                new SqlParameter() { ParameterName = "@EndingDT", SqlDbType = SqlDbType.DateTime2, Value = DBNull.Value },
            };

            var result = await ctx.Database.GetModelFromQueryAsync<ReconciliationPaymentInfo>(
                @"SELECT * FROM [Accounting.Reconciliation.PaymentInfo.Preview](@BID, @LocationID, @EndingDT);"
                , parameters: sp.ToArray());

            if (result.Count() > 0)
                return new GenericResponse<List<ReconciliationPaymentInfo>>()
                {
                    Data = result.ToList(),
                    Success = true,
                    Message = "Successfully retrieved Reconciliation Payment Info."
                };
            else
                return new GenericResponse<List<ReconciliationPaymentInfo>>()
                {
                    Data = result.ToList(),
                    Success = true,
                    Message = $"No Reconciliation Payment Info found for Location {locationID}."
                };
        }

        /// <summary>
        /// Returns an array of ReconciliationPaymentInfo Objects that are un-reconciled but are adjustments for the specified AdjustmentReconciliationID.
        /// </summary>
        /// <param name="adjustedReconciliationID">Adjusted Reconciliation ID</param>
        /// <returns></returns>
        public async Task<GenericResponse<List<ReconciliationPaymentInfo>>> GetPreviewPaymentInfoForAdjustedReconciliation(int adjustedReconciliationID)
        {
            var reconciliation = await ctx.Reconciliation.FirstOrDefaultAsync(x => x.BID == BID && x.ID == adjustedReconciliationID);
            if (reconciliation == null)
                return new GenericResponse<List<ReconciliationPaymentInfo>>()
                {
                    Data = null,
                    Success = false,
                    Message = $"Reconciliation Adjustment with ID {adjustedReconciliationID} not found.",
                    ErrorMessage = $"Reconciliation Adjustment with ID {adjustedReconciliationID} not found."
                };

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() { ParameterName = "@BID", SqlDbType = SqlDbType.SmallInt, Value = BID },
                new SqlParameter() { ParameterName = "@AdjustedReconciliationID", SqlDbType = SqlDbType.Int, Value = adjustedReconciliationID },
            };

            var result = await ctx.Database.GetModelFromQueryAsync<ReconciliationPaymentInfo>(
                @"SELECT * FROM [Accounting.Reconciliation.PaymentInfo.AdjustedPreview](@BID,@AdjustedReconciliationID);"
                , parameters: sp.ToArray());

            if (result.Count() > 0)
                return new GenericResponse<List<ReconciliationPaymentInfo>>()
                {
                    Data = result.ToList(),
                    Success = true,
                    Message = "Successfully retrieved Reconciliation Payment Info."
                };
            else
                return new GenericResponse<List<ReconciliationPaymentInfo>>()
                {
                    Data = result.ToList(),
                    Success = true,
                    Message = $"No Reconciliation Payment Info found for Adjusted Reconciliation {adjustedReconciliationID}."
                };
        }

        /// <summary>
        /// get voided order within reconciliation
        /// </summary>
        /// <param name="locationID"></param>
        /// <param name="reconciliationID"></param>
        /// <returns></returns>
        public async Task<ICollection<OrderData>> GetVoidedOrders(byte? locationID, int? reconciliationID = null)
        {
            IQueryable<OrderData> voidedOrders = this.ctx.OrderData
                                .AsNoTracking()
                                .Include(x => x.EmployeeRoles)
                                    .ThenInclude(r => r.Employee)
                                .WherePrimary(this.BID, x =>
                                    x.OrderStatusID == OrderOrderStatus.OrderVoided
                                );

            if (reconciliationID.HasValue)
            {
                Reconciliation reconciliation = await this.ctx.Reconciliation
                                        .AsNoTracking()
                                        .FirstOrDefaultAsync(x => x.ID == reconciliationID.Value);
                voidedOrders = voidedOrders.Where(x =>
                                    x.Dates.Any(d =>
                                        d.KeyDateType == OrderKeyDateType.Voided &&
                                        d.KeyDate != null &&
                                        (d.KeyDate > reconciliation.LastAccountingDT && d.KeyDate <= reconciliation.AccountingDT)
                                    )
                                );

            }
            else
            {
                Reconciliation lastKnownRecon = await this.ctx.Reconciliation
                                        .AsNoTracking()
                                        .OrderByDescending(x => x.AccountingDT)
                                        .FirstOrDefaultAsync(x => x.LocationID == locationID);
                voidedOrders = voidedOrders.Where(x =>
                                    x.Dates.Any(d =>
                                        d.KeyDateType == OrderKeyDateType.Voided &&
                                        d.KeyDate != null &&
                                        (lastKnownRecon == null || d.KeyDate > lastKnownRecon.AccountingDT)
                                    )
                                );
            }

            if (locationID.HasValue)
            {
                voidedOrders = voidedOrders.Where(x => x.LocationID == locationID.Value);
            }

            return await voidedOrders.ToArrayAsync();
        }

        /// <summary>
        /// Retrieve the Cash Drawer balances for the current unreconciled period. An optional AccountingDT can be supplied to ensure the returned data matches the initial preview information supplied to the client
        /// </summary>
        /// <param name="locationID"></param>
        /// <param name="AccountingDT"></param>
        /// <returns></returns>
        public async Task<List<CashDrawerBalance>> GetCashDrawerBalance(byte locationID, DateTime? AccountingDT = null)
        {
            var glEngine = new Endor.GLEngine.GLEngine(this.BID, this.ctx);
            return await glEngine.GetCashDrawerBalance(locationID, AccountingDT);
        }

        /// <summary>
        /// Retrieve the historical Cash Drawer balances for a previous reconciled period.
        /// </summary>
        /// <param name="reconciliationID"></param>
        /// <returns></returns>
        public async Task<List<CashDrawerBalance>> GetHistoricalCashDrawerBalance(int reconciliationID)
        {
            var glEngine = new Endor.GLEngine.GLEngine(this.BID, this.ctx);
            return await glEngine.GetHistoricalCashDrawerBalance(reconciliationID);
        }
    }
}
