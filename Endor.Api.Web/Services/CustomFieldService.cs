﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Service to handle custom fields
    /// </summary>
    public class CustomFieldService
    {
        private readonly ApiContext _ctx;
        private readonly RemoteLogger _logger;
        private readonly IRTMPushClient _rtmClient;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        public CustomFieldService(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient)
        {
            _ctx = context;
            _logger = logger;
            _rtmClient = rtmClient;
        }

        /// <summary>
        /// Creates a lazy service from the types in this service, given a value and a BID function
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <returns></returns>
        public static Lazy<CustomFieldService> CreateService(ApiContext ctx, RemoteLogger logger, IRTMPushClient rtmClient)
        {
            return new Lazy<CustomFieldService>(() => new CustomFieldService(ctx, logger, rtmClient));
        }

        private bool ValidateCustomFieldValue(short bid, int CTID, CustomFieldValue model)
        {
            var def = _ctx.CustomFieldDefinition.FirstOrDefault(t => t.BID == bid && t.ID == model.ID && t.AppliesToClassTypeID == CTID);

            if (def == null)
                return false;

            return true;
        }

        private string DeleteCFRecord<I, T>(short bid, int CTID, I ID)
            where I : struct, IConvertible
            where T : class, ICustomFieldRecord<I>, new()
        {
            T cfValueToDeleteInDB = _ctx.Set<T>().Where(x => x.BID == bid && x.AppliesToClassTypeID == CTID && x.ID.Equals(ID)).FirstOrDefault();

            if (cfValueToDeleteInDB != null)
            {
                _ctx.Set<T>().Remove(cfValueToDeleteInDB);
                _ctx.SaveChanges();
            }

            return "{}";
        }

                private T ReadCF<I, T>(short bid, int CTID, I ID)
            where I : struct, IConvertible
            where T : class, ICustomFieldRecord<I>, new()
        {
            return _ctx.Set<T>().FirstOrDefault(cf => cf.BID == bid && cf.ID.Equals(ID) && cf.AppliesToClassTypeID == CTID);
        }

        private string UpdateCF<I, T>(short bid, int CTID, I ID, short CFID, CustomFieldValue newModel)
            where I : struct, IConvertible
            where T : class, ICustomFieldRecord<I>, new()
        {
            if (newModel != null)
            {
                //Remove from DB if empty JSON is passed []
                if ((newModel.ID == 0) && (newModel.V == null))
                {
                    return DeleteCFRecord<I, T>(bid, CTID, ID);
                }

                newModel.ID = CFID;

                if (!ValidateCustomFieldValue(bid, CTID, newModel))
                    return null;
            }
            else
            {
                return DeleteCFRecord<I, T>(bid, CTID, ID);
            }

            T cfValues = ReadCF<I, T>(bid, CTID, ID);

            string json;
            if (cfValues == null)
            {
                json = null;

                cfValues = new T()
                {
                    BID = bid,
                    ID = ID,
                    ClassTypeID = CTID + 1,
                    AppliesToClassTypeID = CTID,
                };
                _ctx.Set<T>().Add(cfValues);
            }
            else
            {
                json = ConvertCustomFieldXMLtoJSON(cfValues.DataXML);
            }

            List<CustomFieldValue> CFVs;

            if (string.IsNullOrWhiteSpace(json))
                CFVs = new List<CustomFieldValue>();
            else
                CFVs = JsonConvert.DeserializeObject<List<CustomFieldValue>>(json);

            List<CustomFieldValue> newList = CFVs.Where(x => x.ID != CFID).ToList();

            if (newModel != null)
                newList.Add(newModel);
            
            foreach (var CFV in newList)
            {
                //get CFD 
                var cfd = _ctx.CustomFieldDefinition.Where(x => x.ID == CFV.ID && x.BID == bid).FirstOrDefault();
                if (cfd != null)
                {
                    CFV.DataType = cfd.DataType;
                }
            }

            string result = JsonConvert.SerializeObject(newList);

            //cfValues.DataJSON = result;
            cfValues.DataXML = ConvertCustomFieldJSONtoXML(result);

            if (CTID != (int)ClassType.Order)
                cfValues.ModifiedDT = DateTime.UtcNow;

            _ctx.SaveChanges();

            return result;
        }

        private string UpdateCFs<I, T>(short bid, int CTID, I ID, CustomFieldValue[] newModel, bool disableSaveChanges = false)
            where I : struct, IConvertible
            where T : class, ICustomFieldRecord<I>, new()
        {
            if (newModel != null)
            {
                //Remove from DB if empty JSON is passed []
                if (newModel.Count() == 0)
                {
                    return DeleteCFRecord<I, T>(bid, CTID, ID);
                }

                foreach (var cfv in newModel)
                {
                    if (!ValidateCustomFieldValue(bid, CTID, cfv))
                        return null;
                }
            }
            else
            {
                return DeleteCFRecord<I, T>(bid, CTID, ID);
            }

            T cfValues = ReadCF<I, T>(bid, CTID, ID);

            if (cfValues == null)
            {
                cfValues = new T()
                {
                    BID = bid,
                    ID = ID,
                    ClassTypeID = CTID + 1,
                    AppliesToClassTypeID = CTID,
                };
                _ctx.Set<T>().Add(cfValues);
            }

            string result;

            if (newModel == null)
                result = "[]";
            else
                result = JsonConvert.SerializeObject(newModel);

            //cfValues.DataJSON = result;
            cfValues.DataXML = ConvertCustomFieldJSONtoXML(result);

            if (!disableSaveChanges) _ctx.SaveChanges();

            return result;
        }

        /// <summary>
        /// Updates a Custom Field
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="CTID"></param>
        /// <param name="ID"></param>
        /// <param name="CFID"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public string UpdateCustomField(short bid, int CTID, int ID, short CFID, CustomFieldValue newModel)
        {
            switch (CTID)
            {
                case (int)ClassType.Company:
                    return UpdateCF<int, CompanyCustomData>(bid, CTID, ID, CFID, newModel);
                case (int)ClassType.Contact:
                    return UpdateCF<int, ContactCustomData>(bid, CTID, ID, CFID, newModel);
                case (int)ClassType.Order:
                    return UpdateCF<int, OrderCustomData>(bid, CTID, ID, CFID, newModel);
                case (int)ClassType.Opportunity:
                    return UpdateCF<int, OpportunityCustomData>(bid, CTID, ID, CFID, newModel);
                default:
                    return UpdateCF<int, CustomFieldOtherData>(bid, CTID, ID, CFID, newModel);
            }
        }

        /// <summary>
        /// Updates a Custom Field
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="CTID"></param>
        /// <param name="ID"></param>
        /// <param name="newModel"></param>
        /// <param name="disableSaveChanges"></param>
        /// <returns></returns>
        public string UpdateCustomFields(short bid, int CTID, int ID, CustomFieldValue[] newModel, bool disableSaveChanges = false)
        {
            switch (CTID)
            {
                case (int)ClassType.Company:
                    return UpdateCFs<int, CompanyCustomData>(bid, CTID, ID, newModel, disableSaveChanges);
                case (int)ClassType.Contact:
                    return UpdateCFs<int, ContactCustomData>(bid, CTID, ID, newModel, disableSaveChanges);
                case (int)ClassType.Order:
                    return UpdateCFs<int, OrderCustomData>(bid, CTID, ID, newModel, disableSaveChanges);
                case (int)ClassType.Opportunity:
                    return UpdateCFs<int, OpportunityCustomData>(bid, CTID, ID, newModel, disableSaveChanges);
                default:
                    return UpdateCFs<int, CustomFieldOtherData>(bid, CTID, ID, newModel, disableSaveChanges);
            }
        }

        /// <summary>
        /// Deletes a Custom Field
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="CTID"></param>
        /// <param name="ID"></param>
        /// <param name="CFID"></param>
        /// <returns></returns>
        public string DeleteCustomField(short bid, int CTID, int ID, short CFID)
        {
            switch (CTID)
            {
                case (int)ClassType.Company:
                    return UpdateCF<int, CompanyCustomData>(bid, CTID, ID, CFID, null);
                case (int)ClassType.Contact:
                    return UpdateCF<int, ContactCustomData>(bid, CTID, ID, CFID, null);
                case (int)ClassType.Order:
                    return UpdateCF<int, OrderCustomData>(bid, CTID, ID, CFID, null);
                case (int)ClassType.Opportunity:
                    return UpdateCF<int, OpportunityCustomData>(bid, CTID, ID, CFID, null);
                default:
                    return UpdateCF<int, CustomFieldOtherData>(bid, CTID, ID, CFID, null);
            }
        }

        /// <summary>
        /// Deletes a Custom Field
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="CTID"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public string DeleteCustomFields(short bid, int CTID, int ID)
        {
            switch (CTID)
            {
                case (int)ClassType.Company:
                    return UpdateCFs<int, CompanyCustomData>(bid, CTID, ID, null);
                case (int)ClassType.Contact:
                    return UpdateCFs<int, ContactCustomData>(bid, CTID, ID, null);
                case (int)ClassType.Order:
                    return UpdateCFs<int, OrderCustomData>(bid, CTID, ID, null);
                case (int)ClassType.Opportunity:
                    return UpdateCFs<int, OpportunityCustomData>(bid, CTID, ID, null);
                default:
                    return UpdateCFs<int, CustomFieldOtherData>(bid, CTID, ID, null);
            }
        }

        private ICustomFieldRecord<int> DoReadCustomFields(short bid, int CTID, int ID)
        {
            switch (CTID)
            {
                case (int)ClassType.Company:
                    return ReadCF<int, CompanyCustomData>(bid, CTID, ID);
                case (int)ClassType.Contact:
                    return ReadCF<int, ContactCustomData>(bid, CTID, ID);
                case (int)ClassType.Order:
                    return ReadCF<int, OrderCustomData>(bid, CTID, ID);
                case (int)ClassType.Opportunity:
                    return ReadCF<int, OpportunityCustomData>(bid, CTID, ID);
                default:
                    return ReadCF<int, CustomFieldOtherData>(bid, CTID, ID);
            }
        }

        /// <summary>
        /// Reads a custom field
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="CTID"></param>
        /// <param name="ID"></param>
        /// <param name="CFID"></param>
        /// <returns></returns>
        public CustomFieldValue ReadCustomField(short bid, int CTID, int ID, short CFID)
        {
            var result = DoReadCustomFields(bid, CTID, ID);
            if (result == null)
                return null;

            if (String.IsNullOrWhiteSpace(result.DataXML))
            {
                return null;
            }

            string json = ConvertCustomFieldXMLtoJSON(result.DataXML);

            var CFVs = JsonConvert.DeserializeObject<CustomFieldValue[]>(json);
            //var CFVs = JsonConvert.DeserializeObject<CustomFieldValue[]>(result.DataJSON);

            CustomFieldValue CFV = CFVs.FirstOrDefault(t => t.ID == CFID);
            var cfd = _ctx.CustomFieldDefinition.Where(x => x.ID == CFV.ID && x.BID == bid).FirstOrDefault();
            if (cfd != null)
                CFV.DataType = cfd.DataType;

            return CFV;
        }

        /// <summary>
        /// Reads custom fields
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="CTID"></param>
        /// <param name="ID"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        public List<CustomFieldValue> ReadCustomFields(short bid, int CTID, int ID, CustomFieldValueFilters filters)
        {
            var result = DoReadCustomFields(bid, CTID, ID);
            if (result == null)
                return null;

            if (String.IsNullOrWhiteSpace(result.DataXML))
            {
                return null;
            }

            string json = ConvertCustomFieldXMLtoJSON(result.DataXML);

            var CFVs = JsonConvert.DeserializeObject<CustomFieldValue[]>(json);
            //var CFVs = JsonConvert.DeserializeObject<CustomFieldValue[]>(result.DataJSON);

            var activeOnly = ((!filters.IsActive.HasValue) || (filters.IsActive.Value));
            var includeNull = ((filters.IncludeNull.HasValue) && (filters.IncludeNull.Value));
            var CFVList = new List<CustomFieldValue>();
            foreach (var CFV in CFVs)
            {
                //get CFD 
                var cfd = _ctx.CustomFieldDefinition.Where(x => x.ID == CFV.ID && x.BID == bid).FirstOrDefault();
                
                if (((cfd != null) && (cfd.IsActive || !activeOnly)) &&
                    (!includeNull || CFV.V != null))
                {
                    CFV.DataType = cfd.DataType;
                    CFVList.Add(CFV);
                }
            }

            return CFVList;
        }

        /// <summary>
        /// Convert JSON to XML
        /// </summary>
        /// <param name="JSON"></param>
        //XML needs to have root <data> tag
        public static string ConvertCustomFieldJSONtoXML(string JSON)
        {
            var prefix = "{" + JsonConvert.SerializeObject("data") + ":";
            var postfix = "}";
            string result = prefix + JSON + postfix;

            XmlDocument xmlDoc = JsonConvert.DeserializeXmlNode(result, "root");

            string xml = xmlDoc.InnerXml;
            xml = xml.Replace("<root>", "");
            xml = xml.Replace("</root>", "");

            return xml;
        }

        /// <summary>
        /// Convert XML to JSON
        /// </summary>
        /// <param name="XML"></param>
        //Remove root <data> tag from converted JSON
        public static string ConvertCustomFieldXMLtoJSON(string XML)
        {
            if (!XML.Contains("<root>"))
                XML = "<root>" + XML + "</root>";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(XML);
            
            var root = "{" + JsonConvert.SerializeObject("root") + ":";
            var data = "{" + JsonConvert.SerializeObject("data") + ":";
            string json = JsonConvert.SerializeXmlNode(doc);
            json = json.Replace(root, "");
            json = json.Replace(data, "", StringComparison.CurrentCultureIgnoreCase);
            json = json.Replace("}}", "");
            if (json.Substring(0,1) != "[")
            {
                json = "[" + json + "]";
            }

            return json;
        }
    }
}