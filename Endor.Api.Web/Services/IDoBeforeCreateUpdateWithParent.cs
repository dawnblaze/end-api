﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="cM"></typeparam>
    /// <typeparam name="pM"></typeparam>
    public interface IDoBeforeCreateUpdateWithParent<cM, pM>
        where cM : class
        where pM : class
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        void DoBeforeCreateWithParent(cM child, pM parent);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        void DoBeforeUpdateWithParent(cM child, pM oldParent, pM newParent);
    }

}
