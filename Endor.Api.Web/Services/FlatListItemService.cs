﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Flat List Item Service
    /// </summary>
    public class FlatListItemService : AtomCRUDService<FlatListItem, short>
    {
        /// <summary>
        /// Constructs a flat list item service with injected params
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public FlatListItemService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Collects the array of includes to expand
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return null;
        }

        /// <summary>
        /// Returns an IQueryable filtered by primary key
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<FlatListItem> WherePrimary(IQueryable<FlatListItem> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Child Associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<FlatListItem, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<FlatListItem, short>[]
            {
            };
        }

        internal override void DoBeforeValidate(FlatListItem newModel)
        {
        }

        /// <summary>
        /// Create function 
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task<FlatListItem> CreateAsync(FlatListItem newModel, Guid? tempGuid = null)
        {
            // set new model's defaults
            newModel.IsSystem = false;
            newModel.IsAdHoc = false;



            // get FlatListType.IsAlphaSorted reference
            var flatListType = await ctx.EnumFlatListType.AsNoTracking().FirstOrDefaultAsync(i => i.ID == newModel.FlatListType);

            // 2018-06-26 19:43-0
            // Per the comp the new models will be added at the top
            // this means the default sort index from the ui is likely a -1            
            if (newModel.SortIndex == null)
            {
                newModel.SortIndex = (short)-1;
            }

            await UpdateModelsSortIndexes(newModel, flatListType);

            var result = await base.CreateAsync(newModel, tempGuid);

            return result;
        }

        private async Task UpdateModelsSortIndexes(FlatListItem newModel, EnumFlatListType flatListType)
        {
            // skip shifting when IsAlphaSorted, will resort by name later
            var shifterQuery = ctx.FlatListItem.Where(t => t.BID == this.BID && t.FlatListType == newModel.FlatListType && !t.IsAdHoc);
            List<FlatListItem> itemsToUpdate = await shifterQuery.ToListAsync();
            itemsToUpdate.Add(newModel);

            if (!flatListType.IsAlphaSorted)
            {
                itemsToUpdate = itemsToUpdate.OrderBy(t => t.SortIndex).ThenBy(t => t.Name).ToList();
            }
            else
            {
                itemsToUpdate = itemsToUpdate.OrderBy(t => t.Name).ToList();
            }

            for (int i = 0; i < itemsToUpdate.Count; i++)
            {
                itemsToUpdate[i].SortIndex = (short)i;
            }
        }

        internal async Task<ServiceResponse> MoveBefore(short id, short targetID)
        {
            var moverAndTarget = await ctx.FlatListItem.Where(t => t.BID == this.BID && (t.ID == id || t.ID == targetID)).ToListAsync();
            var mover = moverAndTarget.FirstOrDefault(t => t.ID == id);
            var target = moverAndTarget.FirstOrDefault(t => t.ID == targetID);

            if (mover == null)
                return new ServiceResponse() { Success = false, IsNotFound = true, Message = "Entity does not exists or cannot be found" };

            if (target == null)
                return new ServiceResponse() { Success = false, IsNotFound = true, Message = "Target does not exist or cannot be found" };

            if (target.FlatListType != mover.FlatListType || target.ID == mover.ID)
                return new ServiceResponse() { Success = false, HasError = true, Message = "Invalid Target, targets must have the same list type and cannot be the same record" };

            var shifterQuery = ctx.FlatListItem.Where(t => t.BID == this.BID && t.FlatListType == mover.FlatListType);

            List<FlatListItem> shifters = null;

            if (mover.SortIndex > target.SortIndex) // moving up
            {
                shifters = await shifterQuery.Where(t => t.SortIndex > target.SortIndex && t.SortIndex < mover.SortIndex).ToListAsync();
                shifters.ForEach(t => t.SortIndex += 1);
                mover.SortIndex = target.SortIndex;
                target.SortIndex++;
            }
            else // moving down
            {
                shifters = await shifterQuery.Where(t => t.SortIndex < target.SortIndex && t.SortIndex > mover.SortIndex).ToListAsync();
                shifters.ForEach(t => t.SortIndex -= 1);
                mover.SortIndex = (short)(target.SortIndex - 1);
            }

            try
            {
                await ctx.SaveChangesAsync();

                return new ServiceResponse() { Success = true };
            }
            catch (Exception e)
            {
                return new ServiceResponse() { Success = false, HasError = true, Message = e.ToString() };
            }

        }

        internal async Task<ServiceResponse> ForceCreate(FlatListItem model)
        {
            await DoBeforeCreateAsync(model);
            ctx.FlatListItem.Add(model);

            try
            {
                await ctx.SaveChangesAsync();
                return new ServiceResponse() { Success = true, Value = model };
            }
            catch (Exception e)
            {
                return new ServiceResponse() { Success = false, HasError = true, Message = e.ToString() };
            }
        }

        internal async Task<ServiceResponse> ForceDelete(short id)
        {
            var toBeDeleted = await ctx.FlatListItem.FirstOrDefaultAsync(t => t.BID == this.BID && t.ID == id);
            if (toBeDeleted == null)
                return new ServiceResponse() { Success = false, IsNotFound = true };

            // DELETE does not actually delete the record from the database, but instead sets the record as AdHoc = 1.
            // As such, it is always supported and the CanDelete action always returns true.
            // https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/690749547/FlatListItem+API
            toBeDeleted.IsAdHoc = true;
            ctx.FlatListItem.Update(toBeDeleted);

            try
            {
                bool save = (await ctx.SaveChangesAsync()) > 0;
                if (save)
                {
                    return new ServiceResponse()
                    {
                        Success = true,
                        Message = "Successfully Deleted"
                    };
                }
                else
                {
                    return new ServiceResponse()
                    {
                        Success = false,
                        Message = "Nothing to delete"
                    };
                }
            }
            catch (Exception e)
            {

                return new ServiceResponse() { Success = false, HasError = true, Message = e.ToString() };
            }
        }

        internal async Task<ServiceResponse> MoveAfter(short id, short targetID)
        {
            var moverAndTarget = await ctx.FlatListItem.Where(t => t.BID == this.BID && (t.ID == id || t.ID == targetID)).ToListAsync();
            var mover = moverAndTarget.FirstOrDefault(t => t.ID == id);
            var target = moverAndTarget.FirstOrDefault(t => t.ID == targetID);

            if (mover == null)
                return new ServiceResponse() { Success = false, IsNotFound = true, Message = "Entity does not exists or cannot be found" };

            if (target == null)
                return new ServiceResponse() { Success = false, IsNotFound = true, Message = "Target does not exist or cannot be found" };

            if (target.FlatListType != mover.FlatListType || target.ID == mover.ID)
                return new ServiceResponse() { Success = false, HasError = true, Message = "Invalid Target, targets must have the same list type and cannot be the same record" };

            var shifterQuery = ctx.FlatListItem.Where(t => t.BID == this.BID);

            List<FlatListItem> shifters = null;

            if (mover.SortIndex > target.SortIndex) // moving up
            {
                shifters = await shifterQuery.Where(t => t.SortIndex > target.SortIndex && t.SortIndex < mover.SortIndex).ToListAsync();
                shifters.ForEach(t => t.SortIndex += 1);
                mover.SortIndex = (short)(target.SortIndex + 1);

            }
            else // moving down
            {
                shifters = await shifterQuery.Where(t => t.SortIndex < target.SortIndex && t.SortIndex > mover.SortIndex).ToListAsync();
                shifters.ForEach(t => t.SortIndex -= 1);
                mover.SortIndex = (short)(target.SortIndex);
                target.SortIndex--;
            }

            try
            {
                await ctx.SaveChangesAsync();

                return new ServiceResponse() { Success = true };
            }
            catch (Exception e)
            {
                return new ServiceResponse() { Success = false, HasError = true, Message = e.ToString() };
            }
        }

        /// <summary>
        /// Returns the simpleflatlistitems based on a passed in query
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        internal async Task<List<SimpleFlatListItem>> SimpleListAsync(FlatListItemFilter filters)
        {
            var query = ctx.FlatListItem.Where(t => t.BID == this.BID)
                            .WhereAll(filters.WherePredicates())
                            .OrderBy(i => i.SortIndex)
                            .Select<FlatListItem, SimpleFlatListItem>(SelectSimpleExpression());
            return await query.ToListAsync();
        }

        internal Expression<Func<FlatListItem, SimpleFlatListItem>> SelectSimpleExpression()
        {
            return t => new SimpleFlatListItem()
            {
                BID = t.BID,
                ClassTypeID = (int)ClassType.FlatListItem,
                DisplayName = t.Name,
                ID = t.ID,
                IsActive = t.IsActive,
                IsDefault = false
            };
        }

        /// <summary>
        /// Overridden CanDelete for FlatListItem
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(short id)
        {
            var target = await ctx.FlatListItem.FirstOrDefaultAsync(t => t.BID == this.BID && t.ID == id);
            if (target == null)
                return new BooleanResponse() { Success = false };

            return target.IsSystem ? new BooleanResponse() { Success = true, Value = false } : new BooleanResponse() { Success = true, Value = true };
        }

        /// <summary>
        /// This action is taken before delete occurs
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(FlatListItem model)
        {
            if ((await CanDelete(model.ID)).Value.GetValueOrDefault(false))
            {
                model.IsAdHoc = true;
                model.SortIndex = null;
            }

            return;
        }

        internal async Task<List<FlatListItem>> GetWithFiltersAsync(FlatListItemFilter filters)
        {
            if (filters != null)
            {
                return await this.GetWhere(filters.WherePredicates());
            }
            else
                return await this.GetAsync(null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(FlatListItem oldModel, FlatListItem newModel)
        {
            await base.DoBeforeUpdateAsync(oldModel, newModel);
            var flatListType = await ctx.EnumFlatListType.AsNoTracking().FirstOrDefaultAsync(t => t.ID == oldModel.FlatListType);
            await UpdateModelsSortIndexes(oldModel, flatListType);
        }

        /// <summary>
        /// Returns a collection of Flat List Items that have the same name as the passed in item
        /// </summary>
        /// <param name="flatListItem"></param>
        /// <returns></returns>
        public ICollection<FlatListItem> GetSharedFlatListName(FlatListItem flatListItem)
        {
            return this.ctx.FlatListItem
                            .Where(x=>
                                x.BID == flatListItem.BID &&
                                x.IsAdHoc == false &&
                                x.Name.Trim().ToLower().Equals(flatListItem.Name.Trim().ToLower()) &&
                                x.FlatListType == flatListItem.FlatListType &&
                                x.ID != flatListItem.ID
                            )
                            .ToList();
        }
    }
}
