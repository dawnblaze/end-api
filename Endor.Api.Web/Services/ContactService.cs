﻿using Endor.EF;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Endor.Tasks;
using Endor.Api.Web.Classes;
using Endor.Tenant;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.DocumentStorage.Models;
using Microsoft.Graph;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Contact Service
    /// </summary>
    public class ContactService : AtomCRUDService<ContactData, int>, ISimpleListableViewService<SimpleContactData, int>, ILocatorParentService<ContactData, int>
    {
        /// <summary>
        /// Property to get  SimpleContactData from ApiContext
        /// </summary>
        public DbSet<SimpleContactData> SimpleListSet => ctx.SimpleContactData;

        /// <summary>
        /// Contact Service Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">ITaskQueuer</param>
        /// <param name="cache">ITenantDataCache</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public ContactService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Default Child Properties to include from Database Context
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "ContactLocators", "CompanyContactLinks", "CompanyContactLinks.Company" };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Clone's the CustomFields for a Contact
        /// </summary>
        /// <param name="oldID">Old Contact ID</param>
        /// <param name="newID">New Contact ID</param>
        /// <returns></returns>
        private async Task<bool> CloneCustomFieldsForContact(int oldID, int newID)
        {
            try
            {
                CustomFieldService customFieldService = new CustomFieldService(this.ctx, this.logger, this.rtmClient);
                var customFieldList = customFieldService.ReadCustomFields(this.BID, (int)ClassType.Contact, oldID, new CustomFieldValueFilters());
                if (customFieldList != null)
                {
                    customFieldService.UpdateCustomFields(this.BID, (int)ClassType.Contact, newID, customFieldList.ToArray());
                }
                return true;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Clone of Contact Custom Fields failed", e);
                return false;
            }
        }

        /// <summary>
        /// Contact Service Override of DoBeforeUpdateAsync
        /// </summary>
        /// <param name="oldModel">Old ContactData</param>
        /// <param name="updatedModel">Updated ContactData</param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(ContactData oldModel, ContactData updatedModel)
        {
            // try to check if the contact is linked with any company
            var companyContactLink = ctx.CompanyContactLink.Where(c => c.ContactID == updatedModel.ID);
            if (companyContactLink == null)
            {
                await this.CreateCompanyForContact(updatedModel);
            }

            await UpdateCompanyContactLink(oldModel, updatedModel);
            await base.DoBeforeUpdateAsync(oldModel, updatedModel);
        }

        /// <summary>
        /// Gets the Old Model Async
        /// </summary>
        /// <param name="newModel">New Model</param>
        /// <returns></returns>
        public override async Task<ContactData> GetOldModelAsync(ContactData newModel)
        {
            return await WherePrimary(ctx.ContactData.IncludeAll(IncludeDefaults).AsNoTracking(), newModel.ID).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Contact Service Override of DoAfterUpdateAsync
        /// </summary>
        /// <param name="oldModel">Old ContactData</param>
        /// <param name="updatedModel">Updated ContactData</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(ContactData oldModel, ContactData updatedModel, string connectionID)
        {
            await base.DoAfterUpdateAsync(oldModel, updatedModel, connectionID);
        }

        /// <summary>
        /// Handle CompanyContactLinks in ContactService
        /// </summary>
        /// <param name="oldModel">Old ContactData</param>
        /// <param name="updatedModel">Updated ContactData</param>
        private async Task UpdateCompanyContactLink(ContactData oldModel, ContactData updatedModel)
        {
            if (updatedModel.CompanyContactLinks == null || updatedModel.CompanyContactLinks.Count == 0)
                return;

            var companyContactLinksToRemove = new List<CompanyContactLink>();
            foreach (var ol in oldModel.CompanyContactLinks)
            {
                if (!updatedModel.CompanyContactLinks.Any(cl => cl.CompanyID == ol.CompanyID))
                {
                    if (ol.IsBilling == false && ol.IsPrimary == false)
                        companyContactLinksToRemove.Add(ol);
                }
            }

            var companyContactLinksToAdd = new List<CompanyContactLink>();
            var badCompanyIDLinks = new List<CompanyContactLink>();
            foreach (var nl in updatedModel.CompanyContactLinks)
            {
                if (!oldModel.CompanyContactLinks.Any(ol => ol.CompanyID == nl.CompanyID))
                {
                    var company = await ctx.CompanyData.FirstOrDefaultAsync(x => x.BID == BID && x.ID == nl.CompanyID);
                    if (company != null)
                        companyContactLinksToAdd.Add(nl);
                    else
                        badCompanyIDLinks.Add(nl);
                }
            }
            foreach (var link in badCompanyIDLinks)
                updatedModel.CompanyContactLinks.Remove(link);

            ctx.CompanyContactLink.RemoveRange(companyContactLinksToRemove);
            ctx.CompanyContactLink.AddRange(companyContactLinksToAdd);
            await ctx.SaveChangesAsync();
        }

        /// <summary>
        /// Contact Service Override of DoBeforeCloneAsync
        /// </summary>
        /// <param name="clone">Cloned ContactData</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(ContactData clone)
        {
            var newName = GetNextClonedName(ctx.Set<ContactData>().AsNoTracking().Where(t => t.BID == BID && t.Last.StartsWith(t.Last)).ToList(), clone.Last, t => t.Last);
            clone.Last = newName;

            if (clone.CompanyContactLinks != null)
            {
                if (clone.CompanyContactLinks.FirstOrDefault(l => l.Company.IsAdHoc == true) != null)
                {
                    var link = clone.CompanyContactLinks.FirstOrDefault(l => l.Company.IsAdHoc == true);
                    var companyID = await RequestIDAsync(link.Company.BID, Convert.ToInt32(ClassType.Company));

                    // Create new Company and link the company id to new contact
                    link.Company.ID = companyID;
                    // set the contact's company id to the created company
                    link.CompanyID = companyID;
                }
                else
                {
                    clone.CompanyContactLinks = null;
                }
            }

            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// sets up cloned from links
        /// </summary>
        public override async Task DoAfterCloneAsync(int clonedFromID, ContactData clone)
        {
            // Clone Custom Fields
            await CloneCustomFieldsForContact(clonedFromID, clone.ID);

            // Clone CompanyContactLinks
            var clonedFromLinks = await ctx.CompanyContactLink.Include(l => l.Company)
                .Where(l => l.BID == BID && l.ContactID == clonedFromID).ToListAsync();

            if (clonedFromLinks != null && clonedFromLinks.Count > 0)
            {
                if (!clonedFromLinks.Any(l => l.Company.IsAdHoc))
                {
                    List<CompanyContactLink> toClone = new List<CompanyContactLink>();
                    foreach(var link in clonedFromLinks)
                    {
                        toClone.Add(new CompanyContactLink()
                        {
                            BID = BID,
                            ContactID = clone.ID,
                            CompanyID = link.CompanyID,
                        });
                    }
                    ctx.CompanyContactLink.AddRange(toClone);
                    await ctx.SaveChangesAsync();
                }
            }

            // Copy All Files
            DocumentManager client = base.GetDocumentManager(clonedFromID, ClassType.Contact, BucketRequest.Documents);
            await client.CloneAllBucketBlobsAsync(clone.ID, Bucket.Documents, includeSpecialFolderBlobName: true);

            await base.DoAfterCloneAsync(clonedFromID, clone);

        }

        /// <summary>
        /// Get Child Service Associations for ContactData - Includes ContactLocators
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<ContactData, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<ContactData, int>[]{
                CreateChildAssociation<ContactLocatorService, ContactLocator, int>((a) => a.ContactLocators),
                CreateChildLinkAssociation<CompanyContactLinkService, CompanyContactLink>((a) => a.CompanyContactLinks)
            };
        }

        /// <summary>
        /// Gets a list of Contacts associated with a Company by ID
        /// </summary>
        /// <param name="companyId">Company ID</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        public async Task<List<ContactData>> GetForCompanyId(int companyId, ContactIncludes includes = null)
        {
            return await ctx.CompanyContactLink.Include("Contact")
                .Where(l => l.CompanyID == companyId).Select(l => l.Contact).ToListAsync();
        }

        /// <summary>
        /// Gets a list of Contacts associated with a Company by its ID
        /// </summary>
        /// <param name="companyId">Company ID</param>
        /// <returns></returns>
        public async Task<List<CompanyContactLink>> GetCompanyContactLinks(int companyId)
        {
            var company = await ctx.CompanyData.FirstOrDefaultAsync(c => c.BID == BID && c.ID == companyId);
            if (company == null)
                return null;

            return await ctx.CompanyContactLink.Include("Contact")
                .Where(l => l.BID == BID && l.CompanyID == companyId).ToListAsync();
        }

        /// <summary>
        /// Actions to perform before creating the Contact
        /// </summary>
        /// <param name="newModel">Contact</param>
        /// <returns></returns>
        internal async Task PrepareModelForCreateAsync(ContactData newModel)
        {
            if (newModel == null)
                return;

            if (newModel.StatusID == 0)
                newModel.StatusID = (byte)CompanyStatus.Lead;

            if (newModel.LocationID == null || newModel.LocationID == 0)
            {
                LocationData defaultLocation = await GetDefaultLocation(this.BID);
                newModel.LocationID = defaultLocation.ID;
            }

            if (newModel.CompanyContactLinks != null && newModel.CompanyContactLinks.Count > 0)
            {
                await this.CreateCompanyForContact(newModel);
            }
            else
            {
                newModel.CompanyContactLinks = new List<CompanyContactLink>()
                {
                    new CompanyContactLink()
                    {
                        BID = BID,
                        ContactID = newModel.ID,
                        CompanyID = 0,
                        IsActive = true,
                        Company = new CompanyData()
                        {
                            BID = BID,
                            ID = 0
                        }
                    }
                };
                await this.CreateCompanyForContact(newModel);
            }
        }

        /// <summary>
        /// Sets the ContactRole for the Contact and Company
        /// </summary>
        /// <param name="id">Contact's ID</param>
        /// <param name="companyID">Company's ID</param>
        /// <param name="role">Contact Role to set</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> SetContactRole(int id, int companyID, ContactRole role)
        {
            try
            {
                var existingContact = await ctx.ContactData.IncludeAll(new[] { "CompanyContactLinks", "CompanyContactLinks.Company" })
                    .AsNoTracking().FirstOrDefaultAsync(x => x.BID == BID && x.ID == id);
                if (existingContact == null)
                {
                    return new EntityActionChangeResponse
                    {
                        Id = id,
                        ErrorMessage = "Contact not found",
                        Success = false
                    };
                }

                if (existingContact.CompanyContactLinks?.FirstOrDefault(x => x.CompanyID == companyID) == null)
                {
                    return new EntityActionChangeResponse
                    {
                        Id = id,
                        ErrorMessage = $"Company or Contact do not exist or are Not Associated with the each other. CompanyID={companyID} ContactID={id}",
                        Success = false
                    };
                }

                List<int> oldContactIDs = null;
                if (role == ContactRole.Primary)
                {
                    oldContactIDs = await ctx.CompanyContactLink
                        .Where(x => x.BID == BID && x.CompanyID == companyID && x.ContactID != id && x.IsPrimary == true)
                        .Select(x => x.ContactID).ToListAsync();
                }
                if (role == ContactRole.Billing)
                {
                    oldContactIDs = await ctx.CompanyContactLink
                        .Where(x => x.BID == BID && x.CompanyID == companyID && x.ContactID != id && x.IsBilling == true)
                        .Select(x => x.ContactID).ToListAsync();
                }
                if (role == (ContactRole.Billing | ContactRole.Primary))
                {
                    oldContactIDs = await ctx.CompanyContactLink
                        .Where(x => x.BID == BID && x.CompanyID == companyID && x.ContactID != id && (x.IsBilling == true || x.IsPrimary == true))
                        .Select(x => x.ContactID).ToListAsync();
                }

                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                var rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Contact.Action.SetRole] @BID, @ContactID, @CompanyID, @RoleID, @Result;"
                    , parameters: new[]{
                                            new SqlParameter("@BID", BID),
                                            new SqlParameter("@ContactID", id),
                                            new SqlParameter("@CompanyID", companyID),
                                            new SqlParameter("@RoleID", role),
                                            resultParam
                });

                if (rowResult > 0)
                {
                    if (oldContactIDs != null && oldContactIDs.Count > 0)
                    {
                        oldContactIDs.Insert(0, id);
                        await QueueIndexForModels(oldContactIDs);
                    }
                    else
                    {
                        await QueueIndexForModel(id);
                    }

                    await taskQueuer.IndexModel(this.BID, ClassType.Company.ID(), companyID);

                    return new EntityActionChangeResponse
                    {
                        Id = id,
                        Message = $"Successfully set Contact Role for Contact `{id}` on Company `{companyID}`.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = id,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = id,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Sets a Contact's Active/Inactive state
        /// </summary>
        /// <param name="id">Contact's ID</param>
        /// <param name="role">For Active should be Observer, otherwise Inactive</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> SetContactActiveState(int id, ContactRole role)
        {
            try
            {
                bool setActive = (role != ContactRole.Inactive);
                string setState = setActive ? "Active" : "Inactive";
                var existingContact = await ctx.ContactData.IncludeAll(new[] { "CompanyContactLinks", "CompanyContactLinks.Company" })
                    .AsNoTracking().FirstOrDefaultAsync(x => x.BID == BID && x.ID == id);
                if (existingContact == null)
                {
                    return new EntityActionChangeResponse
                    {
                        Id = id,
                        ErrorMessage = "Contact not found",
                        Success = false
                    };
                }
                
                if (existingContact.CompanyContactLinks == null || existingContact.CompanyContactLinks.Count <= 0)
                {
                    return new EntityActionChangeResponse
                    {
                        Success = true,
                        Id = id,
                    };
                }

                if (!setActive && existingContact.CompanyContactLinks.FirstOrDefault().Company.IsAdHoc == false && existingContact.CompanyContactLinks.Any(x => x.IsBilling == true || x.IsPrimary == true))                
                {
                    return new EntityActionChangeResponse
                    {
                        Success = false,
                        Message = "Contact is set as either Billing or Primary for a Company and cannot be set to Inactive.",
                        Id = id,
                    };
                }

                foreach(var link in existingContact.CompanyContactLinks)
                {
                    if(link.Company.IsAdHoc && role == ContactRole.Inactive)
                    {
                        await SetAdhocContactToInActive(link);
                    }
                    else
                    {
                        await SetContactRole(id, link.CompanyID, role);
                    }
                }

                return new EntityActionChangeResponse
                {
                    Success = true,
                    Message = $"Successfully set Contact to {setState}.",
                    Id = id,
                };
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = id,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        private async Task SetAdhocContactToInActive(CompanyContactLink link)
        {
            var linkToDeactivate = await ctx.CompanyContactLink.FirstOrDefaultAsync(x => x.CompanyID == link.CompanyID && x.ContactID == link.ContactID);
            linkToDeactivate.Roles = ContactRole.Inactive;
            ctx.CompanyContactLink.Update(linkToDeactivate);
            await ctx.SaveChangesAsync();
        }

        /// <summary>
        /// Sets a group of Contacts to Active or Inactive
        /// </summary>
        /// <param name="ids">Contact Ids</param>
        /// <param name="role">For Active should be Observer, otherwise Inactive</param>
        /// <returns></returns>
        public async Task<EntitiesActionChangeResponse> SetContactActiveStateMultiple(int[] ids, ContactRole role)
        {
            try
            {
                string setState = (role != ContactRole.Inactive) ? "Active" : "Inactive";
                foreach (var id in ids)
                {
                    var resp = await SetContactActiveState(id, role);
                    if (resp.HasError)
                        throw new Exception($"Unable to set Contact {id}'s active status to {setState}. Multiple set active aborted.");
                }
                return new EntitiesActionChangeResponse
                {
                    Ids = ids,
                    Message = "Successfully updated Contact(s) active status.",
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new EntitiesActionChangeResponse
                {
                    Ids = ids,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Either Creates or Removes a Company Contact link with a Company for the Contact
        /// </summary>
        /// <param name="id">Contact's ID</param>
        /// <param name="companyID">Company's ID</param>
        /// <param name="link">Whether this is to Link or Unlink</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> UpdateCompanyContactLink(int id, int companyID, bool link)
        {
            try
            {
                var setString = link ? "Link" : "Unlink";
                var existingContact = await ctx.ContactData.Include("CompanyContactLinks")
                    .AsNoTracking().FirstOrDefaultAsync(x => x.BID == BID && x.ID == id);
                if (existingContact == null)
                {
                    return new EntityActionChangeResponse
                    {
                        Id = id,
                        ErrorMessage = "Contact not found",
                        Success = false
                    };
                }

                if (link)
                {
                    var existingCompany = await ctx.CompanyData.FirstOrDefaultAsync(c => c.BID == BID && c.ID == companyID);
                    if (existingCompany == null)
                    {
                        return new EntityActionChangeResponse
                        {
                            Id = id,
                            ErrorMessage = "Company not found.",
                            Success = false
                        };
                    }

                    var newLink = new CompanyContactLink()
                    {
                        BID = BID,
                        CompanyID = companyID,
                        ContactID = id,
                        Roles = ContactRole.Observer,
                        IsActive = true
                    };
                    ctx.CompanyContactLink.Add(newLink);
                    await ctx.SaveChangesAsync();

                    await QueueIndexForModel(id);
                    await taskQueuer.IndexModel(BID, ClassType.Company.ID(), companyID);
                }
                else
                {
                    var existingLink = await ctx.CompanyContactLink
                        .FirstOrDefaultAsync(l => l.BID == BID && l.ContactID == id && l.CompanyID == companyID);
                    if (existingLink != null)
                    {
                        if (existingLink.IsBilling == true || existingLink.IsPrimary == true)
                        {
                            return new EntityActionChangeResponse
                            {
                                Success = false,
                                ErrorMessage = "Contact is set as either Billing or Primary for the Company and cannot be unlinked.",
                                Id = id,
                            };
                        }

                        var hasAssociatedOrders = ctx.TransactionHeaderData.Include(o => o.ContactRoles)
                            .Any(o => o.BID == BID && o.CompanyID == companyID && o.ContactRoles.Any(r => r.ContactID == id) == true);
                        if (hasAssociatedOrders)
                        {
                            return new EntityActionChangeResponse
                            {
                                Success = false,
                                ErrorMessage = "Contact is associated with one or more Orders or Estimates for the Company and cannot be unlinked.",
                                Id = id,
                            };
                        }

                        ctx.CompanyContactLink.Remove(existingLink);
                        await ctx.SaveChangesAsync();

                        await QueueIndexForModel(id);
                        await taskQueuer.IndexModel(BID, ClassType.Company.ID(), companyID);
                    }
                }

                return new EntityActionChangeResponse
                {
                    Id = id,
                    Message = $"Successfully {setString}ed Contact `{id}` for Company `{companyID}`.",
                    Success = true
                };

            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = id,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Returns a collection of a ContactData's ContactLocators
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IEnumerable<BaseLocator<int, ContactData>> GetLocators(ContactData model) => model.ContactLocators;

        /// <summary>
        /// Assigns a collection of locators to a ContactData Model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="value"></param>
        public void SetLocators(ContactData model, IEnumerable<BaseLocator<int, ContactData>> value) => model.ContactLocators = value.Select(x => (ContactLocator)x).ToList();

        /// <summary>
        /// Actions to perform before model validation occurs
        /// </summary>
        /// <param name="newModel">Contact</param>
        internal override void DoBeforeValidate(ContactData newModel)
        {
            if (newModel.CompanyContactLinks != null && newModel.CompanyContactLinks.Count > 0)
            {
                foreach(var link in newModel.CompanyContactLinks)
                {
                    if (link.CompanyID != 0)
                    {
                        //persist CRM information
                        var existingCompany = this.ctx.CompanyData
                            .FirstOrDefault(c => c.BID == BID && c.ID == link.CompanyID);

                        if (existingCompany == null)
                            throw new KeyNotFoundException($"Company with ID `{link.CompanyID}` not found.");

                        if (link.Company != null && newModel.ID == 0) {
                            existingCompany.IndustryID = link.Company.IndustryID;
                            existingCompany.StatusID = link.Company.StatusID;
                            existingCompany.TeamID = link.Company.TeamID;
                        }
                        link.Company = existingCompany;
                    }
                }
            }

            this.StripOutTemporaryLocators(newModel);
        }

        /// <summary>
        /// Creates any new Companies for the Contact
        /// </summary>
        /// <param name="contact">Contact</param>
        /// <returns></returns>
        private async Task CreateCompanyForContact(ContactData contact)
        {
            var optSvc = new OptionService(this.ctx, migrationHelper);
            var paymentTermDefaultID = 0;
            var res = await optSvc.Get(null, "Accounting.PaymentTerm.DefaultID", BID, null, null, null, null, null);
            if (res != null && res.Success && (int.TryParse(res.Value.Value, out int intResult)) && intResult > 0)
            {
                paymentTermDefaultID = intResult;
            }

            LocationData defaultLocation = await GetDefaultLocation(this.BID);

            CompanyService companySvc = new CompanyService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
            CompanyData newCompany;

            // END-11812
            if (contact.CompanyContactLinks.Any()) await SetPrimaryOrBillingContact(contact);

            foreach (var link in contact.CompanyContactLinks.Where(l => l.CompanyID == 0))
            {
                if (link.Company != null)
                {
                    newCompany = link.Company;
                }
                else
                {
                    newCompany = new CompanyData();
                    link.Company = newCompany;
                }
                if (newCompany != null)
                {
                    link.IsActive = true;
                }
                newCompany.BID = this.BID;
                newCompany.ID = 0;
                newCompany.Location = defaultLocation;
                newCompany.LocationID = defaultLocation.ID;
                newCompany.StatusText = "default"; // db computed
                newCompany.PricingTierID = 100; // default id for flatlist pricingtier

                if (paymentTermDefaultID > 0)
                {
                    newCompany.PaymentTermID = (Int16)paymentTermDefaultID;
                    newCompany.DefaultPaymentTerms = ctx.PaymentTerm.FirstOrDefault(t => t.ID == newCompany.PaymentTermID);
                }

                if (link.Company.Name == null || link.Company.Name.Length == 0)
                {
                    newCompany.Name = contact.First + " " + contact.Last;
                    link.Company.IsAdHoc = true;
                    link.Company.IsActive = true;
                    link.Roles = ContactRole.Primary;
                }
                else
                {
                    newCompany.Name = link.Company.Name;
                    link.Company.IsAdHoc = false;
                }

                if (newCompany.StatusID == 0)
                {
                    newCompany.StatusID = 2; // prospect
                }

                await companySvc.CreateAsync(newCompany); // insert company    
                link.CompanyID = newCompany.ID;
                link.Company = this.ctx.CompanyData.FirstOrDefault(c => c.BID == BID && c.ID == link.CompanyID);
            }
        }

        /// <summary>
        /// A Contact added to a Company should automatically be set as the Primary Contact 
        /// and Billing Contact if there are no others associated with it.
        /// It should remain set as the Primary / Billing until the user manually changes it.
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        private async Task SetPrimaryOrBillingContact(ContactData contact)
        {
            foreach (var item in contact.CompanyContactLinks)
            {
                var contactLinks = await this.ctx.CompanyContactLink.FirstOrDefaultAsync(link => link.BID == item.BID && link.CompanyID == item.CompanyID);
                if (contactLinks == null)
                {
                    item.Roles = ContactRole.PrimaryAndBilling;
                    break;
                }
                else
                {
                    var primaryAndBillingContact = await this.ctx.CompanyContactLink.FirstOrDefaultAsync(link => link.BID == item.BID && link.CompanyID == item.CompanyID && link.Roles == ContactRole.PrimaryAndBilling);
                    if (primaryAndBillingContact == null)
                    {
                        var primaryContact = await this.ctx.CompanyContactLink.FirstOrDefaultAsync(link => link.BID == item.BID && link.CompanyID == item.CompanyID && link.Roles == ContactRole.Primary);
                        if (primaryContact == null)
                        {
                            item.Roles = ContactRole.Primary;
                        }
                        else
                        {
                            var billingContact = await this.ctx.CompanyContactLink.FirstOrDefaultAsync(link => link.BID == item.BID && link.CompanyID == item.CompanyID && link.Roles == ContactRole.Billing);
                            if (billingContact == null) item.Roles = ContactRole.Billing;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the Default Location for the Business
        /// </summary>
        /// <param name="BID">Business ID</param>
        /// <returns></returns>
        private async Task<LocationData> GetDefaultLocation(short BID)
        {
            BusinessData business = await this.ctx.BusinessData
                                                        .Include(b => b.Locations)
                                                        .FirstOrDefaultAsync(b => b.BID == BID);

            LocationData defaultLocation = business.Locations.FirstOrDefault(l => l.IsDefault);
            if (defaultLocation == null)
            {
                defaultLocation = this.ctx.LocationData.FirstOrDefault(x => x.BID == this.BID);
            }

            return defaultLocation;
        }

        /// <summary>
        /// Lookup ContactData by query and ID - Override
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">ContactData ID</param>
        /// <returns></returns>
        public override IQueryable<ContactData> WherePrimary(IQueryable<ContactData> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// after add hook
        /// creates "Customer Shared" Folder and persists temp uploads
        /// </summary>
        /// <param name="model"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(ContactData model, Guid? tempGuid)
        {
            await base.InitializeCustomerSharedFolderAsync(model);
            await base.DoAfterAddAsync(model, tempGuid);
        }

        /// <summary>
        /// Force Delete
        /// </summary>
        /// <param name="ContactID">ID of the Contact to Delete</param>
        /// <param name="userLinkID">UserLink.ID</param>
        public async Task<EntityActionChangeResponse> ForceDeleteContact(short ContactID, short userLinkID)
        {
            try
            {
                UserLink userLinkData = await ctx.UserLink.Where(ul => ul.BID == this.BID && ul.ID == userLinkID).FirstOrDefaultAsync();

                if (userLinkData == null)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Contact not found"
                    };
                }

                if (userLinkData.UserAccessType < UserAccessType.SupportManager)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Access denied"
                    };
                }

                ContactData toDelete = await this.GetAsync(ContactID);

                if (toDelete == null)
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Contact not found"
                    };
                }

                List<RefreshEntity> refreshes = DoGetRefreshMessagesOnDelete(toDelete);

                refreshes.AddRange(await ctx.ContactCustomData.RemoveRangeWhereAtomAsync(this.BID, x => x.ID == ContactID));
                refreshes.AddRange(await ctx.ContactLocator.RemoveRangeWhereAtomAsync(this.BID, x => x.ParentID == ContactID));

                ctx.ContactTagLink.RemoveRange(await ctx.ContactTagLink.Where(x => x.BID == BID && x.ContactID == ContactID).ToArrayAsync());

                var participants = await ctx.MessageParticipantInfo.Where(x => x.BID == BID && x.ContactID == ContactID).ToArrayAsync();
                ctx.MessageParticipantInfo.RemoveRange(participants);
                int[] participantIDs = participants.Select(x => x.ID).ToArray();
                ctx.MessageDeliveryRecord.RemoveRange(await ctx.MessageDeliveryRecord.Where(x => x.BID == BID && participantIDs.Contains(x.ParticipantID)).ToArrayAsync());
                refreshes.AddRange(await ctx.MessageHeader.RemoveRangeWhereAtomAsync(this.BID, x => participantIDs.Contains(x.ParticipantID)));

                refreshes.AddRange(await ctx.OpportunityData.RemoveRangeWhereAsync(this.BID, ClassType.Opportunity, x => x.ContactID == ContactID, y => y.ID));
                refreshes.AddRange(await ctx.UserLink.RemoveRangeWhereAsync(this.BID, ClassType.UserLink, x => x.ContactID == ContactID, y => y.ID));

                ctx.ContactData.Remove(toDelete);

                var save = ctx.SaveChanges();

                if (save > 0)
                {
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = refreshes });
                    await taskQueuer.IndexModel(refreshes);

                    return new EntityActionChangeResponse()
                    {
                        Success = true,
                        Message = "Successfully Deleted"
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Success = false,
                        ErrorMessage = "Nothing to delete"
                    };
                }
            }
            catch (DbUpdateException exc)
            {
                return new EntityActionChangeResponse()
                {
                    Success = false,
                    ErrorMessage = exc.InnerException.Message
                };
            }
            catch (Exception exc)
            {
                return new EntityActionChangeResponse()
                {
                    Success = false,
                    ErrorMessage = exc.ToString()
                };
            }
        }

        public override async Task<BooleanResponse> CanDelete(int id)
        {
            var isAdHoc = ctx.CompanyContactLink.Include("Company").AsNoTracking()
                .Any(l => l.BID == BID && l.ContactID == id && l.Company.IsAdHoc);
            var primaryOrBilling = ctx.CompanyContactLink.AsNoTracking()
                .Any(l => l.BID == BID && l.ContactID == id && (l.IsBilling == true || l.IsPrimary == true));
            if (primaryOrBilling && !isAdHoc)
            {
                return new BooleanResponse()
                {
                    Success = true,
                    Value = false,
                    Message = "Contact is set as either Billing or Primary for the Company and cannot be Deleted."
                };
            }

            var linkedCompanyIDs = await ctx.CompanyContactLink.AsNoTracking()
                .Where(l => l.BID == BID && l.ContactID == id).Select(l => l.CompanyID).ToListAsync();
            var hasAssociatedOrders = ctx.TransactionHeaderData.AsNoTracking().Include(o => o.ContactRoles)
                .Any(o => o.BID == BID && linkedCompanyIDs.Contains(o.CompanyID) && o.ContactRoles.Any(r => r.ContactID == id) == true);
            if (hasAssociatedOrders)
            {
                return new BooleanResponse()
                {
                    Success = true,
                    Value = false,
                    Message = "Contact is associated with one or more Orders or Estimates for the Company and cannot be Deleted."
                };
            }

            return new BooleanResponse()
            {
                Success = true,
                Value = true,
                Message = "Can Delete."
            };
        }

        /// <summary>
        /// method called before deleting a record
        /// removed contact custom data first, as it is being added out of nowhere
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(ContactData model)
        {
            var canDelete = await CanDelete(model.ID);
            if (canDelete.Value == true)
            {
                var links = await ctx.CompanyContactLink.Include(l => l.Company)
                    .Where(x => x.BID == BID && x.ContactID == model.ID).ToListAsync();
                links = links.Where(l => l.Company.IsAdHoc == false).ToList();
                ctx.CompanyContactLink.RemoveRange(links);
            }
            else
            {
                throw new InvalidOperationException(canDelete.Message);
            }

            await ctx.ContactCustomData.RemoveRangeWhereAtomAsync(this.BID, x => x.ID == model.ID);
            await base.DoBeforeDeleteAsync(model);
        }

        /// <summary>
        /// Business logic to perform after deleting a model
        /// </summary>
        /// <param name="model">Deleted model</param>
        /// <returns></returns>
        protected override async Task DoAfterDeleteAsync(ContactData model)
        {
            await base.DoAfterDeleteAsync(model);

            if (model.CompanyContactLinks != null && model.CompanyContactLinks.Count > 0)
            {
                CompanyService companySvc = new CompanyService(this.ctx, this.BID, this.taskQueuer, this.cache, this.logger, this.rtmClient, migrationHelper);
                foreach(var link in model.CompanyContactLinks)
                {
                    CompanyData toDeleteCompany = await companySvc.GetAsync(link.CompanyID);
                    if (toDeleteCompany.IsAdHoc)
                    {
                        await companySvc.DeleteAsync(toDeleteCompany);
                    }
                }
            }
        }

        /// <summary>
        /// Validate Location
        /// </summary>
        /// <param name="locationID">Location ID</param>
        /// <returns></returns>
        public bool IsValidLocation(int? locationID)
        {
            var location = ctx.LocationData.Where(t => t.ID == locationID).FirstOrDefault();
            return location != null;
        }

        /// <summary>
        /// Gets the current status of the Contact
        /// </summary>
        /// <param name="contactID">Contact's ID</param>
        /// <returns></returns>
        public async Task<ContactStatusResponse> GetStatus(int contactID)
        {
            var result = new ContactStatusResponse
            {
                status = "lead",
                isStale = false,
                lastCreated = new DateTime()
            };
            var resultStatus = CompanyStatus.Lead;
            var contact = await ctx.ContactData.FirstOrDefaultAsync(x => x.BID == BID && x.ID == contactID);
            if (contact != null)
            {
                var lastOrder = await ctx.TransactionHeaderData
                    .Include(o => o.Dates)
                    .Where(o => o.TransactionType == (byte)OrderTransactionType.Order)
                    .Where(o => o.ContactRoles.Any(r => r.ContactID == contactID && r.RoleType == OrderContactRoleType.Primary))
                    .OrderByDescending(o => o.Dates.First(x => x.KeyDateType == OrderKeyDateType.Created))
                    .FirstOrDefaultAsync();

                if (lastOrder != null)
                {
                    // NOTE: customer
                    result.status = "customer";
                    resultStatus = CompanyStatus.Customer;
                    var orderCreatedKeyDate = lastOrder.Dates?.FirstOrDefault(x => x.KeyDateType == OrderKeyDateType.Created);
                    result.lastCreated = orderCreatedKeyDate?.KeyDT;
                }
                else
                {
                    // NOTE: get contacts last estimate/order
                    var lastEstimate = await ctx.TransactionHeaderData
                        .Include(o => o.Dates)
                        .Where(o => o.TransactionType == (byte)OrderTransactionType.Estimate)
                        .Where(o => o.ContactRoles.Any(r => r.ContactID == contactID && r.RoleType == OrderContactRoleType.Primary))
                        .OrderByDescending(o => o.Dates.First(x => x.KeyDateType == OrderKeyDateType.Created))
                        .FirstOrDefaultAsync();

                    if (lastEstimate != null)
                    {
                        // NOTE: prospect
                        result.status = "prospect";
                        resultStatus = CompanyStatus.Prospect;
                        var estimateCreatedKeyDate = lastEstimate.Dates?.FirstOrDefault(x => x.KeyDateType == OrderKeyDateType.Created);
                        result.lastCreated = estimateCreatedKeyDate?.KeyDT;
                    } else
                    {
                        // If Lead - Get the created date of the contact - base one https://corebridge.atlassian.net/browse/END-9607
                        result.lastCreated = contact.CreatedDate;
                    }
                }

                if (contact.StatusID != (byte)resultStatus)
                {
                    contact.StatusID = (byte)resultStatus;
                    ctx.ContactData.Update(contact);
                    await ctx.SaveChangesAsync();
                    await QueueIndexForModel(contact.ID);
                }

                var OptionID = 0;
                var staleDays = 0;

                switch (result.status)
                {
                    case "lead":
                        OptionID = 6121;
                        break;
                    case "prospect":
                        OptionID = 6122;
                        break;
                    case "customer":
                        OptionID = 6123;
                        break;
                }

                var CRMstaleDays = await ctx.OptionData.FirstOrDefaultAsync(x => x.OptionID == OptionID);

                if (CRMstaleDays != null)
                {
                    staleDays = CRMstaleDays.Value == "" ? 0 : Convert.ToInt32(CRMstaleDays.Value);
                }

                result.isStale = result.lastCreated?.AddDays(staleDays) < DateTime.Today;
            }
            else
            {
                throw new Exception("Contact not found!");
            }

            return result;
        }
    
        /// <summary>
        /// alternative simplelist using contactdata dbset
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<SimpleContactData[]> GetCustomSimpleList(SimpleContactFilters filters)
        {
            var simpleQuery = this.ctx.ContactData.Select(x => new SimpleContactData(){
                                    BID = x.BID,
                                    ID = x.ID,
                                    ClassTypeID = x.ClassTypeID,
                                    DisplayName = x.ShortName,
                                    IsActive = x.CompanyContactLinks.Any(cl => (cl.IsActive ?? false)),
                                    IsDefault = x.CompanyContactLinks.Any(cl => (cl.IsPrimary ?? false)),
                                    ParentID = x.CompanyContactLinks.Select(cl => (int?)cl.CompanyID).FirstOrDefault() ?? 0
                                });
            simpleQuery = simpleQuery
                            .Where(x => x.BID == this.BID)
                            .WhereAll(filters.WherePredicates());

            return await simpleQuery.ToArrayAsync();
        }
    }
}
