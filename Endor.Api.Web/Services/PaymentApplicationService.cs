﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PaymentApplication = Endor.Models.PaymentApplication;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// PaymentApplication Service Layer
    /// </summary>
    public class PaymentApplicationService : AtomCRUDService<PaymentApplication, int>
    {
        /// <summary>
        /// Rights Group List Layer constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public PaymentApplicationService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) { }

        /// <summary>
        /// Returns the includes
        /// </summary>
        /// <param name="includes">Children to include</param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Gets an PaymentApplication by ID for the given query
        /// </summary>
        /// <param name="query">Search Query</param>
        /// <param name="ID">PaymentApplication ID</param>
        /// <returns></returns>
        public override IQueryable<PaymentApplication> WherePrimary(IQueryable<PaymentApplication> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Child Associations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<PaymentApplication, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<PaymentApplication, int>[]
            {
            };
        }

        internal override void DoBeforeValidate(PaymentApplication newModel)
        {
        }

        /// <summary>
        /// Gets all models asynchronously with any specified includes
        /// </summary>
        /// <param name="includes">Child/Parent items to include</param>
        /// <returns></returns>
        public override async Task<List<PaymentApplication>> GetAsync(IExpandIncludes includes = null)
        {
            List<PaymentApplication> results = await ctx.Set<PaymentApplication>().IncludeAll(GetIncludes(includes))
                .Where(m => m.BID == BID)
                .OrderByDescending(t => t.ID)
                .Take(500)
                .ToListAsync();

            return await this.MapCollection(results, includes);
        }

        /// <summary>
        /// Returns PaymentApplication based on filters supplied in
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<List<PaymentApplication>> GetWithFiltersAsync(PaymentApplicationFilter filters)
        {
            if (filters != null && filters.HasFilters)
            {
                return await this.Where(filters.WherePredicates()).OrderByDescending(t => t.ID).ToListAsync();
            }

            return await this.GetAsync();
        }
    }
}
