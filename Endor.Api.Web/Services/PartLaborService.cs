﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Responses;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Labor Data Service
    /// </summary>
    public class PartLaborService : AtomCRUDService<LaborData, int>, ISimpleListableViewService<SimpleLaborData, int>, IPartCategoryLinkableService
    {
        private Lazy<LaborCategoryService> laborCategorySvc;

        /// <summary>
        /// Labor Data Service Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">ITaskQueuer</param>
        /// <param name="cache">ITenantDataCache</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="migrationHelper"></param>
        public PartLaborService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
            this.laborCategorySvc = new Lazy<LaborCategoryService>(()=>new LaborCategoryService(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper));
        }

        #region Overrides
        /// <summary>
        /// Get Child Service Associations for LaborData
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<LaborData, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<LaborData, int>[0];
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        internal override void DoBeforeValidate(LaborData newModel)
        {
        }
        #endregion Overrides

        /// <summary>
        /// Link/Unlink Labor Category with Labor Data
        /// </summary>
        /// <param name="laborCategoryID"></param>
        /// <param name="laborDataID"></param>
        /// <param name="isLinked"></param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkLaborCategory(short laborCategoryID, int laborDataID, bool isLinked)
        {
            EntityActionChangeResponse result = await this.laborCategorySvc.Value.LinkLabor(laborCategoryID, laborDataID, isLinked);

            base.DoGetRefreshMessagesOnUpdate(new LaborData()
            {
                ClassTypeID = Convert.ToInt32(ClassType.Labor),
                ID = Convert.ToInt16(laborDataID),
                BID = this.BID,
            });

            return result;
        }

        internal async Task<List<LaborData>> GetWithFiltersAsync(PartLaborFilter filters)
        {
            if (filters != null && filters.IsActive.HasValue)
            {
                return await this.GetWhere(filters.WherePredicates());
            }
            else
                return await this.GetAsync(null);
        }

        /// <summary>
        /// Property to get  SimpleLaborData from ApiContext
        /// </summary>
        public DbSet<SimpleLaborData> SimpleListSet => this.ctx.SimpleLaborData;

        private async Task<ICollection<SimpleLaborData>> GetByDropDownListValues(DropDownListValues listValues)
        {
            return await GetByIDsAndCategoriesAsync(listValues?.Components?.Select(x => x.ID.Value)?.ToList(), listValues?.Categories?.Select(x => x.ID.Value)?.ToList());
        }

        /// <summary>
        /// Get using list of IDs and CategoryIDs as filter
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="categoryIDs"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public async Task<ICollection<SimpleLaborData>> GetByIDsAndCategoriesAsync(ICollection<int> ids, ICollection<int> categoryIDs, bool? isActive = true)
        {
            List<int> IDs = ids?.ToList(), CategoryIDs = categoryIDs?.ToList();

            return await this.ctx.LaborData
                .Include(e => e.LaborCategoryLinks)
                .Select(e => new {
                    data = e,
                    CategoryIDs = e.LaborCategoryLinks.Select(catLink => catLink.CategoryID)
                }).Where(q =>
                    q.data.BID == this.BID &&
                    (isActive == null || q.data.IsActive == isActive)  &&
                    (
                        (IDs == null || IDs.Contains(q.data.ID)) ||
                        (CategoryIDs == null || q.CategoryIDs.Any(categoryID => CategoryIDs.Contains(categoryID)))
                    )
                ).Select(q => new SimpleLaborData(){
                        BID = q.data.BID,
                        ClassTypeID = q.data.ClassTypeID,
                        Description = q.data.Description,
                        DisplayName = q.data.Name,
                        HasImage = q.data.HasImage,
                        ID = q.data.ID,
                        InvoiceText = q.data.InvoiceText,
                        IsActive = q.data.IsActive,
                        IsDefault = false
                }).ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="variableID"></param>
        /// <param name="machineID"></param>
        /// <param name="profileName"></param>
        /// <returns></returns>
        public async Task<ICollection<SimpleLaborData>> GetByVariableAndProfile(int variableID, int? machineID, string profileName)
        {
            DropDownListValues listValues = await AssemblyHelper.GetListValues<DropDownListValues>(ctx, logger, BID, variableID, machineID, profileName);

            if (listValues == null)
                return new List<SimpleLaborData>();

            return await GetByIDsAndCategoriesAsync(listValues.Components?.Select(c => c.ID.Value).ToList(), listValues.Categories?.Select(c => c.ID.Value).ToList());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="variableID"></param>
        /// <param name="machineID"></param>
        /// <param name="profileName"></param>
        /// <returns></returns>
        public async Task<DropdownResponse<SimpleLaborData>> GetDropdownByVariable(int variableID, int? machineID, string profileName)
        {
            return await AssemblyHelper.GetDropdownResponse(ctx, logger, BID, variableID, machineID, profileName,
                async (DropDownListValues listValues) =>
                {
                    return await GetByDropDownListValues(listValues);
                });

        }

        /// <summary>
        /// Returns a list of `LaborData` that shares the same name as the `model`
        /// </summary>
        /// <param name="model">`LaborData` model to check Name from</param>
        public List<LaborData> ExistingNameOwners(LaborData model)
        {
            return this.ctx.LaborData.AsNoTracking()
                .Where(data =>
                        data.BID == this.BID
                        && data.Name.Trim().Equals(model.Name.Trim())
                        && data.ID != model.ID)
                .ToList();
        }

        /// <summary>
        /// Map the relevent data into the given Map Item
        /// </summary>
        public override async Task MapItem(LaborData item, IExpandIncludes includeExpander = null)
        {
            var includes = includeExpander!=null? includeExpander.GetIncludes():null;
            if (includes != null)
            {
                if (item.LaborCategoryLinks!=null)
                {
                    switch (includes[LaborDataIncludes.LaborCategoriesKey].Level)
                    {
                        case IncludesLevel.Simple:
                            item.SimpleLaborCategories = item.LaborCategoryLinks
                                                            .Select(link=>new SimpleLaborCategory(){
                                                                BID = link.LaborCategory.BID,
                                                                ID = link.LaborCategory.ID,
                                                                ClassTypeID = link.LaborCategory.ClassTypeID,
                                                                IsActive = link.LaborCategory.IsActive,
                                                                IsDefault = false,
                                                                DisplayName = link.LaborCategory.Name                                                        
                                                            }).ToList();
                            break;
                        case IncludesLevel.Full:
                            item.LaborCategories = item.LaborCategoryLinks.Select(link=>link.LaborCategory).ToList();
                            break;
                    }
                }
            }
            await Task.Yield();

        }

        /// <summary>
        /// LaborData Service Override of DoBeforeCreate
        /// </summary>
        /// <param name="newModel">LaborData</param>
        /// <param name="tempGuid">Optional Temp Guid</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(LaborData newModel, Guid? tempGuid = null)
        {
            await base.DoBeforeCreateAsync(newModel, tempGuid);
            if (newModel.SimpleLaborCategories != null)
            {
                if(newModel.LaborCategoryLinks==null)
                {
                    newModel.LaborCategoryLinks = new HashSet<LaborCategoryLink>();
                }
                foreach (var simpleItem in newModel.SimpleLaborCategories)
                {
                    newModel.LaborCategoryLinks.Add(new LaborCategoryLink() { BID = this.BID, PartID = newModel.ID, CategoryID = simpleItem.ID });
                }
            }
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<LaborData> WherePrimary(IQueryable<LaborData> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Remove all Labor Category Links
        /// </summary>
        public async Task unlinkAllCategoryAsync(int ID)
        {
            var existingLinks = this.ctx.LaborCategoryLink.Where(link => link.PartID == ID);
            if (existingLinks.Count() > 0)
            {
                foreach (var link in existingLinks)
                {
                    this.ctx.LaborCategoryLink.Remove(link);
                }
                await ctx.SaveChangesAsync();
            }
        }


        /// <summary>
        /// Checks if the Labor can be deleted
        /// </summary>
        /// <param name="id">Labor ID</param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(int id)
        {
            LaborData Labor = await this.GetAsync(id);
            if (Labor == null)
            {
                return new BooleanResponse()
                {
                    Message = "Labor Not Found",
                    Success = false
                };
            }
            int linkedLaborVariables = this.ctx.AssemblyVariable.Where(x => x.BID == this.BID && x.LinkedLaborID == id).Count();
            if (linkedLaborVariables > 0)
                return new BooleanResponse()
                {
                    Message = "Labor is associated with one or more Variables.",
                    Success = true,
                    Value = false
                };

            return new BooleanResponse()
            {
                Message = "Labor can be deleted.",
                Success = true,
                Value = true
            };
        }

        /// <summary>
        /// Changes clone name
        /// </summary>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override Task DoBeforeCloneAsync(LaborData clone)
        {
            //var newName = GetNextClonedName(clone.Name, t => t.Name);// (.Name + " (Clone)";
            var newName = GetNextClonedName(ctx.Set<LaborData>().AsNoTracking().Where(t => t.BID == BID && t.Name.StartsWith(t.Name)).ToList(), clone.Name, t => t.Name);

            if (clone.InvoiceText != null && clone.InvoiceText.Equals(clone.Name))
            {
                clone.InvoiceText = newName;
            }

            clone.Name = newName;

            return base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Called after clone
        /// </summary>
        /// <param name="clonedFromID"></param>
        /// <param name="clone"></param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(int clonedFromID, LaborData clone)
        {
            var links = ctx.LaborCategoryLink.Where(t => t.PartID == clonedFromID).ToList();
            foreach (var link in links)
            {
                link.PartID = clone.ID;
                ctx.LaborCategoryLink.Add(link);
            }
            await CloneCustomFieldForLabor(clonedFromID, clone.ID);

            ctx.SaveChanges();

            //Copy Documents Bucket
            DocumentManager client = base.GetDocumentManager(clonedFromID, ClassType.Labor, BucketRequest.Documents);
            await client.CloneAllBucketBlobsAsync(clone.ID, Bucket.Documents, includeSpecialFolderBlobName: true);

            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(LaborData oldModel, LaborData newModel, string connectionID)
        {
            await base.DoAfterUpdateAsync(oldModel, newModel, connectionID);
            await this.LinkOrUnlinkCategories(newModel, false);
        }


        private async Task<bool> CloneCustomFieldForLabor(int oldID, int newID)
        {
            try
            {
                CustomFieldService customFieldService = new CustomFieldService(this.ctx, this.logger, this.rtmClient);
                var customFieldList = customFieldService.ReadCustomFields(this.BID, (int)ClassType.Labor, oldID,
                    new CustomFieldValueFilters());
                //                foreach (var customFieldValue in customFieldList)
                //                {
                //                    var cfd = this.ctx.CustomFieldDefinition.FirstOrDefault(x => x.ID == customFieldValue.ID && x.BID == this.BID);
                //                }
                if (customFieldList!=null)
                    customFieldService.UpdateCustomFields(this.BID, (int)ClassType.Labor, newID,
                        customFieldList.ToArray());
                return true;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Clone of Labor Custom Fields failed", e);
                return false;
            }
        }

        /// <summary>
        /// LinkOrUnlinkCategories
        /// </summary>
        /// <param name="labor"></param>
        /// <param name="indexModel"></param>
        /// <returns></returns>
        protected async Task LinkOrUnlinkCategories(LaborData labor, bool indexModel = true)
        {
            if(labor.SimpleLaborCategories!=null)
            {
                var catSvc = this.laborCategorySvc.Value;
                var oldCategoryIDs = await this.ctx.LaborCategoryLink
                                    .AsNoTracking()
                                    .Where(cat => cat.BID==this.BID &&
                                        cat.PartID == labor.ID
                                    )
                                    .Select(x => x.CategoryID)
                                    .ToArrayAsync();

                await this.LinkOrUnlinkCategories(catSvc.LinkLabor, labor.ID, oldCategoryIDs, labor.SimpleLaborCategories.Select(y => y.ID).ToArray(), indexModel);
            }
        }        
    }
}
