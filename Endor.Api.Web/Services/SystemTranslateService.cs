﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// SystemTranslate service
    /// </summary>
    public class SystemTranslateService : AtomGenericService<SystemTranslationDefinition, int>
    {
        private readonly EndorOptions _options;

        /// <summary>
        /// Constructs an SystemTranslation service
        /// </summary>
        /// <param name="context">Context</param>
        /// <param name="options">Options</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public SystemTranslateService(ApiContext context, EndorOptions options, IMigrationHelper migrationHelper)
            : base(context, migrationHelper)
        {
            this._options = options;
        }

        /// <summary>
        /// Get valid includes for OptionData model
        /// </summary>
        public override string[] GetIncludes()
        {
            return new string[]
            {
            };
        }

        /// <summary>
        /// Retrieves Language Dictionary by language code
        /// </summary>
        /// <param name="lang">Language ID</param>
        /// <param name="justUnverified">Return only the unverified results</param>
        public async Task<GenericResponse<Dictionary<string,object>>> Get(byte lang, bool justUnverified = false)
        {
            try
            {
                var query = ctx.SystemTranslationDefinitions
                                    .Where(a => a.LanguageTypeId == lang && (!a.IsVerified || !justUnverified));

                var keys = query.Select(a => a.SourceText).Distinct();
                var dict = new Dictionary<string, object>();
                foreach (var key in keys)
                {
                    var entries = query.Where(a => a.SourceText==key);
                    var keyDict = await entries.ToDictionaryAsync(
                        row => row.AltMeaning.HasValue() ? row.AltMeaning : "",
                        row => row.TranslatedText
                        );
                    var s = GetTranslatedTextString(keyDict);
                    dict.Add(key, s);
                }
                return new GenericResponse<Dictionary<string, object>>()
                {
                    Data = dict,
                    Message = "Successfully retrieved Language Dictionary.",
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new GenericResponse<Dictionary<string, object>>()
                {
                    Data = null,
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Retrieves Language Dictionary by language code
        /// </summary>
        /// <param name="lang">Language ID</param>
        /// <param name="text">Return only the unverified results</param>
        public async Task<GenericResponse<Dictionary<string, object>>> GetLanguageEntry(byte lang, string text)
        {
            try
            {
                var baseText = text.Split('.')[0]; //remove all dot notations
                var query = ctx.SystemTranslationDefinitions
                                    .Where(a => a.LanguageTypeId == lang && (a.SourceText == baseText));
                //OrderBy
                if (query.Any())
                {
                    //build up json array
                    var entries = query.Where(a => a.SourceText == baseText);
                    var keyDict = await entries.ToDictionaryAsync(
                        row => row.AltMeaning.HasValue() ? row.AltMeaning : "",
                        row => row.TranslatedText
                        );
                    var s = GetTranslatedTextString(keyDict);
                    var dict = new Dictionary<string, object>();
                    dict.Add(baseText, s);

                    return new GenericResponse<Dictionary<string, object>>()
                    {
                        Data = dict,
                        Message = "Successfully retrieved Language Dictionary.",
                        Success = true
                    };
                } else
                {
                    var client = new HttpClient();
                    var baseTranslateAPI = "https://translation.googleapis.com/language/translate/v2";
                    var googleAPIKey = _options.Client.GoogleAPIKey;
                    var googleCode = this.ctx.SystemLanguageTypes.Where(l => l.ID == lang).Select(l => l.GoogleCode).First();
                    var stringContent = new StringContent("", UnicodeEncoding.UTF8, "application/json");
                    var getLanguageResponse = client.PostAsync($"{baseTranslateAPI}?key={googleAPIKey}&source=en&target={googleCode}&q={baseText}", stringContent);
                    var translatedText = baseText;
                    if ((await getLanguageResponse).IsSuccessStatusCode)
                    {
                        string content = await (await getLanguageResponse).Content.ReadAsStringAsync();
                        var obj = (dynamic)JsonConvert.DeserializeObject(content);
                        var newDefinition = new SystemTranslationDefinition()
                        {
                            IsVerified = false,
                            SourceText = baseText,
                            TranslatedText = obj.data.translations[0].translatedText.Value,
                            LanguageTypeId = lang
                        };
                        this.ctx.SystemTranslationDefinitions.Add(newDefinition);
                        await this.ctx.SaveChangesAsync();
                    }
                    return new GenericResponse<Dictionary<string, object>>()
                    {
                        Data = new Dictionary<string, object>() { { baseText, translatedText } },
                        Message = "Successfully retrieved Language Dictionary.",
                        Success = true
                    };
                }
            }
            catch (Exception ex)
            {
                return new GenericResponse<Dictionary<string, object>> ()
                {
                    Data = new Dictionary<string, object>(),
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        private object GetTranslatedTextString(Dictionary<string,string> dict)
        {
            if (dict.Count == 1)
            {
                //return single value
                return dict.First().Value;
            }
            else return dict;
            /*//return nested values
            string s = string.Join(
                    ",",
                    dict.Select(
                        p => string.Format(
                            "{0}:{1}",
                            p.Key,
                            p.Value))   
                );
            return "{" + s + "}";*/
        }

        /// <summary>
        /// Retrieves Option Categories by ID
        /// </summary>
        /// <param name="lang">Language ID</param>
        /// <param name="newDict">Return only the unverified results</param>
        public async Task<GenericResponse<string>> Post(byte lang, Dictionary<string, string> newDict)
        {
            try
            {
                foreach (var entry in newDict)
                {
                    var value = entry.Value.Trim();
                    if (!value.StartsWith('{'))
                    {
                        //single value
                        var query = ctx.SystemTranslationDefinitions
                            .Where(def => def.LanguageTypeId == lang && def.SourceText == entry.Key && def.AltMeaning == null);
                        ctx.SystemTranslationDefinitions.RemoveRange(query);
                        var newEntry = new SystemTranslationDefinition
                        {
                            LanguageTypeId = lang,
                            SourceText = entry.Key,
                            TranslatedText = entry.Value,
                            IsVerified = false
                        };
                        await ctx.SystemTranslationDefinitions.AddAsync(newEntry);
                    }
                    else
                    {
                        var data = JsonConvert.DeserializeObject<Dictionary<string,string>>(value);
                        foreach (var newEntry in data.ToList())
                        {
                            var AltMeaningVal = newEntry.Key.Length>0 ? newEntry.Key : null;
                            var query = ctx.SystemTranslationDefinitions
                                .Where(def => def.LanguageTypeId == lang && def.SourceText == entry.Key && def.AltMeaning == AltMeaningVal);
                            ctx.SystemTranslationDefinitions.RemoveRange(query);
                            var newEntry2 = new SystemTranslationDefinition
                            {
                                LanguageTypeId = lang,
                                SourceText = entry.Key,
                                TranslatedText = newEntry.Value,
                                AltMeaning = AltMeaningVal,
                                IsVerified = false
                            };
                            await ctx.SystemTranslationDefinitions.AddAsync(newEntry2);
                        }
                    }
                }
                await ctx.SaveChangesAsync();
                return new GenericResponse<string>()
                {
                    Data = "",
                    Message = "Successfully added language results.",
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new GenericResponse<string>()
                {
                    Data = "",
                    Message = ex.Message,
                    Success = false
                };
            }
        }

        /// <summary>
        /// Deletes individual translation of "text" value from the db
        /// </summary>
        /// <param name="lang">Language ID</param>
        /// <param name="text">Key to delete</param>
        public async Task<GenericResponse<string>> DeleteLanguageEntry(byte lang, string text)
        {
            try
            {
                var query = ctx.SystemTranslationDefinitions
                    .Where(a => a.LanguageTypeId == lang && a.SourceText == text);
                ctx.RemoveRange(query);
                
                await ctx.SaveChangesAsync();
                return new GenericResponse<string>()
                {
                    Data = "",
                    Message = "Successfully added language results.",
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new GenericResponse<string>()
                {
                    Data = "",
                    Message = ex.Message,
                    Success = false
                };
            }
        }

    }
}
