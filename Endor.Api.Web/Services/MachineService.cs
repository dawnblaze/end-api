﻿using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.CBEL.ObjectGeneration;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Endor.CBEL.Autocomplete;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// A service for <see cref=" MachineData"/>
    /// </summary>
    public class MachineService : AtomCRUDService<MachineData, short>, ISimpleListableViewService<SimpleMachineData, short>
    {
        private readonly Lazy<AssemblyService> _assemblySvc;

        /// <summary>
        /// Constructs a Machine service with injected params
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper"></param>
        /// <param name="autocompleteEngine"></param>
        public MachineService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper, IAutocompleteEngine autocompleteEngine)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
            this._assemblySvc = new Lazy<AssemblyService>(() => new AssemblyService(context, this.BID, taskQueuer, cache, logger, rtmClient, migrationHelper, autocompleteEngine));
        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] {
            "Instances",
            "Profiles",
            "Profiles.MachineProfileTables",
            "Profiles.MachineProfileVariables"
        };

        /// <summary>
        /// Returns an array of included navigation properties
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<MachineData, short>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<MachineData, short>[]
            {
                CreateChildAssociation<MachineInstanceService, MachineInstance, int>((i) => i.Instances),
                CreateChildAssociation<MachineProfileService, MachineProfile, int>((a) => a.Profiles)
            };
        }

        internal override void DoBeforeValidate(MachineData newModel) { }

        internal async Task<List<MachineData>> GetWithFiltersAsync(MachineFilter filters)
        {
            if (filters != null && filters.IsActive.HasValue)
            {
                return await this.GetWhere(filters.WherePredicates());
            }
            else
                return await this.GetAsync(null);
        }

        /// <summary>
        /// Gets simple list set
        /// </summary>
        public DbSet<SimpleMachineData> SimpleListSet => this.ctx.SimpleMachineData;

        /// <summary>
        /// Get using list of IDs and CategoryIDs as filter
        /// </summary>
        /// <param name="IDs"></param>
        /// <param name="CategoryIDs"></param>
        /// <returns></returns>
        public async Task<ICollection<SimpleMachineData>> GetByIDsAndCategoriesAsync(ICollection<int> IDs, ICollection<int> CategoryIDs)
        {
            var result = await this.ctx.MachineData
                            .Include(e => e.MachineCategoryLinks)
                            .Select(e => new
                            {
                                data = e,
                                CategoryIDs = e.MachineCategoryLinks.Select(catlink => catlink.CategoryID)
                            }).Where(q =>
                                q.data.BID == this.BID &&
                                q.data.IsActive &&
                                (IDs.Contains(q.data.ID) || q.CategoryIDs.Any(catID => CategoryIDs.Contains(catID)))
                            ).Select(q => new SimpleMachineData()
                            {
                                BID = q.data.BID,
                                ClassTypeID = q.data.ClassTypeID,
                                DisplayName = q.data.Name,
                                ID = q.data.ID,
                                IsActive = q.data.IsActive,
                                IsDefault = false
                            }).ToListAsync();
            return result;
        }

        /// <summary>
        /// Map the relevent data into the given Map Item
        /// </summary>
        public override async Task MapItem(MachineData item, IExpandIncludes includeExpander = null)
        {
            var includes = includeExpander != null ? includeExpander.GetIncludes() : null;
            if (includes != null)
            {
                if (item.MachineCategoryLinks != null)
                {
                    switch (includes[MachineDataIncludes.MachineCategoriesKey].Level)
                    {
                        case IncludesLevel.Simple:
                            item.SimpleMachineCategories = item.MachineCategoryLinks
                                                            .Select(link => new SimpleMachineCategory()
                                                            {
                                                                BID = link.MachineCategory.BID,
                                                                ID = link.MachineCategory.ID,
                                                                ClassTypeID = link.MachineCategory.ClassTypeID,
                                                                IsActive = link.MachineCategory.IsActive,
                                                                IsDefault = false,
                                                                DisplayName = link.MachineCategory.Name
                                                            }).ToList();
                            break;
                        case IncludesLevel.Full:
                            item.MachineCategories = item.MachineCategoryLinks.Select(link => link.MachineCategory).ToList();
                            break;
                    }
                }
            }
            await Task.Yield();

        }

        /// <summary>
        /// Links or Unlinks a Machine with a Machine Category
        /// </summary>
        /// <param name="machineDataID">Machine's ID</param>
        /// <param name="machineCategoryID">Machine Category's ID</param>
        /// <param name="isLinked">If it is to link or unlink</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> LinkMachineCategory(int machineDataID, short machineCategoryID, bool isLinked)
        {
            /*          
                @BID
                @MachineCategoryID
                @MachineDataID
                @IsLinked
                @Result Output
            */
            try
            {
                SqlParameter Result = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                Result.Direction = System.Data.ParameterDirection.Output;

                object[] myParams = {
                    new SqlParameter("@BID", this.BID),
                    new SqlParameter("@MachineCategoryID", machineCategoryID),
                    new SqlParameter("@MachineDataID", machineDataID),
                    new SqlParameter("@IsLinked", isLinked),
                    Result
                };

                int affectedRows = await ctx.Database.ExecuteSqlRawAsync(
                    "EXEC dbo.[Part.Machine.Category.Action.LinkMachine] @BID, @MachineCategoryID, @MachineDataID, @IsLinked, @Result Output;",
                    parameters: myParams);

                if (affectedRows > 0)
                {
                    await QueueIndexForModel(machineCategoryID);

                    return new EntityActionChangeResponse()
                    {
                        Id = machineCategoryID,
                        Message = $"Successfully {(isLinked ? "linked" : "unlinked")} MachineCategory:{machineCategoryID} to MachineData:{machineDataID}",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse()
                    {
                        Id = machineCategoryID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }

            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.StackTrace
                };
            }
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<MachineData> WherePrimary(IQueryable<MachineData> query, short ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Before create, makes sure that MachineCategory is linked to machine
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(MachineData newModel, Guid? tempGuid = null)
        {
            CheckNameUnique(newModel.Name);
            ValidateModel(newModel);

            if (newModel.SimpleMachineCategories != null)
            {
                if (newModel.MachineCategoryLinks == null)
                {
                    newModel.MachineCategoryLinks = new HashSet<MachineCategoryLink>();
                }
                foreach (var simpleItem in newModel.SimpleMachineCategories)
                {
                    newModel.MachineCategoryLinks.Add(new MachineCategoryLink() { BID = this.BID, PartID = newModel.ID, CategoryID = simpleItem.ID });
                }
            }

            await base.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// checks name uniqueness before updating
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override Task DoBeforeUpdateAsync(MachineData oldModel, MachineData newModel)
        {
            CheckNameUnique(newModel.Name, oldModel.ID);

            ValidateModel(newModel);

            return DoBeforeUpdateGatedForMachineDataAsync(oldModel, newModel);
        }

        //This method was written to solve exception detailed in END-11925 when updating MachineData IsActive value.
        public async Task DoBeforeUpdateGatedForMachineDataAsync(MachineData oldModel, MachineData newModel)
        {
            bool isUnsavedEntityID = Convert.ToInt32(newModel.ID) == UnsavedEntityID;

            if (newModel.BID == 0)
                newModel.BID = this.BID; // simple fix to issues saving children

            if (!isUnsavedEntityID && ctx.Entry(newModel).State == EntityState.Added)
            {
                //noop
            }
            else if (isUnsavedEntityID)
            {
                await base.DoBeforeCreateAsync(newModel, null);
            }
            else
            {
                
                //EF knows the SQL column is computed, so it throws errors if it isn't zero
                newModel.ClassTypeID = default(int);
                ctx.Entry(newModel).State = EntityState.Modified;
                this.SetModifiedDT(newModel);

            }

            Task childUpdate = childAssociations?.DoBeforeUpdateAsync(oldModel, newModel);

            if (childUpdate != null)
                await childUpdate;
        }

        /// <summary>
        /// </summary>
        /// <param name="model"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(MachineData model, Guid? tempGuid)
        {
            await this.CreateAndSaveCSandDLL(model);
            await base.DoAfterAddAsync(model, tempGuid);
        }

        /// <summary>        
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(MachineData oldModel, MachineData newModel, string connectionID)
        {
            await this.CreateAndSaveCSandDLL(newModel);
            await base.DoAfterUpdateAsync(oldModel, newModel, connectionID);
        }

        /// <summary>
        /// Create and Save CS and DLL
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> CreateAndSaveCSandDLL(MachineData model)
        {
            try
            {
                if (model.MachineTemplate == null && model.MachineTemplateID.HasValue)
                    model.MachineTemplate = ctx.AssemblyData.WherePrimary(BID, model.MachineTemplateID.Value).Include(w => w.Tables).Include(x => x.Variables).ThenInclude(y => y.Formulas).AsNoTracking().FirstOrDefault();

                if (model.MachineTemplate == null)
                {
                    throw new InvalidOperationException($"Machine (ID:{model.ID}) does not have Machine Template (ID:{model.MachineTemplateID}) so {nameof(CreateAndSaveCSandDLL)} cannot be completed");
                }

                //Create CBELAssemblyGenerator
                CBELMachineGenerator assemblyGenerator = CBELMachineGenerator.Create(ctx, model);
                //Save off files    
                CBEL.Common.AssemblyCompilationResponse compilationResult = await assemblyGenerator.Save(cache);
                // NOTE: on clone, recreates the machine template...
                model.MachineTemplate = null;
                return compilationResult.Success;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Error during creation or save of Machine: " + e.Message, e);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="variableID"></param>
        /// <param name="machineID"></param>
        /// <param name="profileName"></param>
        /// <returns></returns>
        public async Task<ICollection<SimpleMachineData>> GetByVariableAndProfile(int variableID, int? machineID, string profileName)
        {
            DropDownListValues listValues = await AssemblyHelper.GetListValues<DropDownListValues>(ctx, logger, BID, variableID, machineID, profileName);

            if (listValues == null)
                return new List<SimpleMachineData>();

            return await GetByIDsAndCategoriesAsync(listValues.Components?.Select(c => c.ID.Value).ToList(), listValues.Categories?.Select(c => c.ID.Value).ToList());
        }

        private void CheckNameUnique(string machineName, int machineID = 0)
        {
            var checkSameName = ctx.MachineData.AsNoTracking().FirstOrDefault(m => m.BID == BID
                && m.Name.ToLower() == machineName.ToLower()
                && m.ID != machineID);
            if (checkSameName != null)
            {
                throw new InvalidOperationException("Machine has a similar name");
            }
        }

        private void ValidateModel(MachineData newModel)
        {
            var errors = new List<string>();
            if (newModel.ExpenseAccountID.GetValueOrDefault(0) == 0)
                errors.Add("Expense Account is required");

            if (newModel.MachineTemplateID.GetValueOrDefault(0) == 0)
                errors.Add("Machine Template is required");

            // END-6965 - Removing Profile and Instance validation for now

            #region Check Profiles

            //var activeProfileCount = newModel.Profiles?.Where(i => i.IsActive == true).Count();
            //if (activeProfileCount.GetValueOrDefault(0) == 0)
            //    errors.Add("Machine must have 1 or more Active Profiles");
            //else
            //    newModel.ActiveProfileCount = (short)activeProfileCount;

            //if (newModel.Profiles?.Where(i => i.IsDefault == true).Count() > 1)
            //    errors.Add("Machine can only have a maximum of 1 Default Profile");

            //// check if all profile names are unique
            //var profileNameList = newModel.Profiles?.Select(i => i.Name.ToLower()).Distinct().ToList();
            //if (profileNameList.Count() < newModel.Profiles.Count())
            //    errors.Add("Machine Profile name must be unique per Machine.");

            #endregion

            #region Check Instances

            var activeInstanceCount = newModel.Instances?.Where(i => i.IsActive == true).Count();
            if (activeInstanceCount.GetValueOrDefault(0) == 0)
                errors.Add("Machine must have 1 or more Active Instances");
            else
                newModel.ActiveInstanceCount = (short)activeInstanceCount;

            if (newModel.Instances?.Where(i => i.HasServiceContract && (!i.ContractStartDate.HasValue || !i.ContractEndDate.HasValue)).Count() > 0)
                errors.Add("Machine Instance with a service contract requires start and end dates.");

            // check if all instance names are unique
            var instanceNameList = newModel.Instances?.Select(i => i.Name.ToLower()).Distinct().ToList();
            if (instanceNameList.Count() < newModel.Instances.Count())
                errors.Add("Machine Instance name must be unique per Machine.");

            #endregion

            if (errors.Count() > 0)
                throw new InvalidOperationException(String.Join(". ", errors.ToArray()));
        }

        /// <summary>
        /// Remove all Machine Category Links
        /// </summary>
        public async Task unlinkAllCategoryAsync(int ID)
        {
            var existingLinks = this.ctx.MachineCategoryLink.Where(link => link.PartID == ID);
            if (existingLinks.Count() > 0)
            {
                foreach (var link in existingLinks)
                {
                    this.ctx.MachineCategoryLink.Remove(link);
                }
                await ctx.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Checks if the Machine can be deleted
        /// </summary>
        /// <param name="id">Machine ID</param>
        /// <returns></returns>
        public override async Task<BooleanResponse> CanDelete(short id)
        {
            MachineData Machine = await this.GetAsync(id);
            if (Machine == null)
            {
                return new BooleanResponse()
                {
                    Message = "Machine Not Found",
                    Success = false
                };
            }
            int linkedMachineVariables = this.ctx.AssemblyVariable.Where(x => x.BID == this.BID && x.LinkedMachineID == id).Count();
            if (linkedMachineVariables > 0)
                return new BooleanResponse()
                {
                    Message = "Machine is associated with one or more Variables.",
                    Success = true,
                    Value = false
                };

            return new BooleanResponse()
            {
                Message = "Machine can be deleted.",
                Success = true,
                Value = true
            };
        }

        /// <summary>
        /// delete machine instances and profiles
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override Task DoBeforeDeleteAsync(MachineData model)
        {
            List<MachineInstance> instances = ctx.MachineInstance.WherePrimary(BID, x => x.MachineID == model.ID).ToList();
            this.ctx.MachineInstance.RemoveRange(instances);

            var linkedVariables = ctx.AssemblyVariable.WherePrimary(BID, x => x.LinkedMachineID == model.ID).ToList();
            this.ctx.AssemblyVariable.RemoveRange(linkedVariables);
            List<MachineProfile> profiles = ctx.MachineProfile.WherePrimary(BID, x => x.MachineID == model.ID).ToList();
            profiles.ForEach(t =>
            {
                this.ctx.MachineProfileVariable.RemoveRange(ctx.MachineProfileVariable.WherePrimary(BID, x => x.ProfileID == t.ID));
                this.ctx.MachineProfileTable.RemoveRange(ctx.MachineProfileTable.WherePrimary(BID, x => x.ProfileID == t.ID));
            });
            this.ctx.MachineProfile.RemoveRange(profiles);

            return base.DoBeforeDeleteAsync(model);
        }

        /// <summary>
        /// Overridden to throw valdation erros
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        protected override async Task<MachineData> HandleUpdateException(Exception e)
        {
            if (e is InvalidOperationException)
                throw e;

            return await base.HandleUpdateException(e);
        }

        /// <summary>
        /// Overridden to throw valdation erros
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        protected override async Task<MachineData> HandleCreateException(Exception e)
        {
            if (e is InvalidOperationException)
                throw e;

            return await base.HandleUpdateException(e);
        }

        /// <summary>
        /// get old model
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public override async Task<MachineData> GetOldModelAsync(MachineData newModel)
        {
            IQueryable<MachineData> oldModelQry = ctx.MachineData
                                                    .Include(x => x.Instances)
                                                    .Include(x => x.Profiles)
                                                        .ThenInclude(p => p.MachineProfileTables)
                                                    .Include(x => x.Profiles)
                                                        .ThenInclude(p => p.MachineProfileVariables)
                                                    .AsNoTracking();
            var oldModel = await WherePrimary(oldModelQry, newModel.ID).FirstOrDefaultAsync();

            return oldModel;
        }

        /// <summary>
        /// ValidateMachineProfilesJson
        /// </summary>
        /// <param name="profiles"></param>
        /// <param name="assemblyID"></param>
        /// <returns></returns>
        public async Task<ICollection<string>> ValidateMachineProfilesJson(ICollection<MachineProfile> profiles, int assemblyID)
        {
            foreach (var profile in profiles)
            {
                var err = await this._assemblySvc.Value.ValidateAssemblyDataJSON(profile.AssemblyOVDataJSON, assemblyID);
                if (err.Count > 0)
                {
                    return err;
                }
            }

            return null;
        }

        /// <summary>
        /// Get the Machines that has TemplateID of
        /// </summary>
        /// <param name="templateID"></param>
        /// <param name="categoryIDs"></param>
        /// <param name="machineIDs"></param>
        /// <param name="includeInactive"></param>
        /// <returns></returns>
        public async Task<MachinesAndCategories> GetMachinesWithTemplateID(int templateID, ICollection<int> categoryIDs, ICollection<int> machineIDs, bool includeInactive = false)
        {   
            categoryIDs = categoryIDs ?? new int[]{};
            machineIDs = machineIDs ?? new int[]{};

            //https://github.com/aspnet/EntityFrameworkCore/issues/17342
            var categoryListIDs = categoryIDs.ToList();
            var machineListIds = machineIDs.ToList();

            var machines = await this.ctx.MachineData
                            .Where(x => x.BID == this.BID)
                            .Include(m => m.MachineCategoryLinks)
                            .Where(m => m.MachineTemplateID == templateID &&
                                (categoryListIDs.Count == 0 || m.MachineCategoryLinks.Any(c => categoryListIDs.Contains(c.CategoryID))) &&
                                (machineListIds.Count == 0 || machineListIds.Contains(m.ID)) &&
                                (includeInactive || m.IsActive)
                            ).ToListAsync();
            
            var matchingCategoryIDs = machines.SelectMany(m => m.MachineCategoryLinks)
                                    .Select(c => c.CategoryID)
                                    .ToList();

            var categories = await this.ctx.MachineCategory
                                .Where(cat => cat.BID == this.BID && matchingCategoryIDs.Contains(cat.ID))
                                .ToListAsync();
            
            return new MachinesAndCategories(){
                Machines = machines,
                Categories = categories
            };
        }


        /// <summary>
        /// Machine Service Override of DoBeforeCloneAsync
        /// </summary>
        /// <param name="clone">Cloned MachineData</param>
        /// <returns></returns>
        public override async Task DoBeforeCloneAsync(MachineData clone)
        {
            MachineInstance machineInstance = new MachineInstance()
            {
                IsActive = true,
                Name = "New Machine Instance 01",
            };

            //clone.Name = GetNextClonedName(clone.Name, t => t.Name);
            clone.Name = GetNextClonedName(ctx.Set<MachineData>().AsNoTracking().Where(t => t.BID == BID && t.Name.StartsWith(t.Name)).ToList(), clone.Name, t => t.Name);
            clone.Instances = new List<MachineInstance>{machineInstance};
            await base.DoBeforeCloneAsync(clone);
        }

        /// <summary>
        /// Machine Service Override of DoAfterCloneAsync
        /// </summary>
        /// <param name="clonedFromID">Cloned MachineData ID</param>
        /// <param name="clone">Cloned MachineData</param>
        /// <returns></returns>
        public override async Task DoAfterCloneAsync(short clonedFromID, MachineData clone)
        {

            //Copy Documents Bucket
            DocumentManager client = base.GetDocumentManager(clonedFromID, ClassType.Machine, BucketRequest.Documents);
            await client.CloneAllBucketBlobsAsync(clone.ID, Bucket.Documents, includeSpecialFolderBlobName: true);

            // Create the category and material data
            var machineCategoryLinks = ctx.MachineCategoryLink.Where(categoryLink => categoryLink.PartID == clonedFromID).ToList();

            if (machineCategoryLinks != null && machineCategoryLinks.Count() > 0)
            {
                foreach (var link in machineCategoryLinks)
                {
                    link.PartID = clone.ID;
                    ctx.MachineCategoryLink.Add(link);
                }

                ctx.SaveChanges();
            }

            await CloneCustomFields(clonedFromID, clone.ID);

            await base.DoAfterCloneAsync(clonedFromID, clone);
        }

        private async Task<bool> CloneCustomFields(int oldID, int newID)
        {
            try
            {
                CustomFieldService customFieldService = new CustomFieldService(this.ctx, this.logger, this.rtmClient);
                var customFieldList = customFieldService.ReadCustomFields(this.BID, (int)ClassType.Machine, oldID, new CustomFieldValueFilters());

                if (customFieldList != null)
                    customFieldService.UpdateCustomFields(this.BID, (int)ClassType.Machine, newID, customFieldList.ToArray());

                return true;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Clone of Material Custom Fields failed", e);
                return false;
            }
        }
    }
}
