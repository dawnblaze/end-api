﻿using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Endor.Api.Web.Services
{
    /// <summary>
    /// SystemAssemblyVariableService
    /// </summary>
    public class SystemAssemblyVariableService : AtomGenericService<SystemAssemblyVariable, short>, ISimpleListableViewService<SimpleSystemAssemblyVariable, short>
    {
        /// <summary>
        /// gets simple list set
        /// </summary>
        public DbSet<SimpleSystemAssemblyVariable> SimpleListSet => ctx.SimpleSystemAssemblyVariable;

        /// <summary>
        /// Constructs System Assembly Variable Service
        /// </summary>
        /// <param name="context">Context</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public SystemAssemblyVariableService(ApiContext context, IMigrationHelper migrationHelper)
            : base(context, migrationHelper)
        {
        }

        /// <summary>
        /// Default Child Properties to include from Database Context
        /// </summary>
        public string[] IncludeDefaults => new string[]
        {

        };

        /// <summary>
        /// Get the names of navigation properties to include on Get
        /// </summary>
        /// <returns></returns>
        public override string[] GetIncludes()
        {
            return this.IncludeDefaults;
        }

        /// <summary>
        /// Returns all System Assembly Variables
        /// </summary>
        /// <returns></returns>
        public async Task<List<SystemAssemblyVariable>> GetAllAsync()
        {
            return await this.ctx.Set<SystemAssemblyVariable>().ToListAsync();
        }

        /// <summary>
        /// Returns a specific System Assembly Variable
        /// </summary>
        /// <returns></returns>
        public async Task<SystemAssemblyVariable> GetOneAsync(short ID)
        {
            return await this.ctx.Set<SystemAssemblyVariable>().Where(x => x.ID.Equals(ID)).FirstOrDefaultAsync();
        }
    }
}
