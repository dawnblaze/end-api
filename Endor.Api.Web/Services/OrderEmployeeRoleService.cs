﻿using Endor.EF;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Endor.Tasks;
using Endor.Api.Web.Classes;
using Endor.Tenant;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.RTM.Enums;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Order Employee Role Service Layer
    /// </summary>
    public class OrderEmployeeRoleService : AtomCRUDChildService<OrderEmployeeRole, int, int>
    {
        /// <summary>
        /// Order Service Layer
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderEmployeeRoleService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default children to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "Role" };

        /// <summary>
        /// Returns the includes
        /// </summary>
        /// <param name="includes">Children to include</param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// GetChildAssociations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<OrderEmployeeRole, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<OrderEmployeeRole, int>[0];
        }

        internal override void DoBeforeValidate(OrderEmployeeRole newModel)
        {
        }

        /// <summary>
        /// Adds a employee role to an order.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="employeeRoleID">The employee role type of the link</param>
        /// <param name="employeeID">Employee ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> AddEmployeeRole(int ID, short employeeRoleID, int? employeeID = null)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                SqlParameter employeeIdParam;

                if (employeeID.HasValue)
                {
                    employeeIdParam = new SqlParameter("@employeeID", employeeID.Value);
                }
                else
                {
                    employeeIdParam = new SqlParameter("@employeeID", DBNull.Value);
                }

                object[] myParams = {
                    new SqlParameter("@bid", BID),
                    new SqlParameter("@orderId", ID),
                    new SqlParameter("@employeeRoleID", employeeRoleID),
                    employeeIdParam,
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Order.Action.AddEmployeeRole] @bid, @orderId, @employeeRoleID, @employeeID", parameters: myParams);
                if (rowResult > 0)
                {
                    await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.Order), ID);
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(ID) });

                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "Successfully added a employee role.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Adds a Employee role to an order.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="employeeRoleID">The employee role type of the link</param>
        /// <param name="employeeID">Employee ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> AddEmployeeRole(int ID, short employeeRoleID, int employeeID)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                object[] myParams = {
                    new SqlParameter("@bid", BID),
                    new SqlParameter("@orderId", ID),
                    new SqlParameter("@employeeRoleID", employeeRoleID),
                    new SqlParameter("@employeeID", employeeID),
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Order.Action.AddEmployeeRole] @bid, @orderId, @employeeRoleID, @employeeID;", parameters: myParams);
                if (rowResult > 0)
                {
                    await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.Order), ID);
                    //await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(ID) });

                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "Successfully added a employee role.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Adds an Employee role to an OrderItem.
        /// </summary>
        /// <param name="orderItem">OrderItem to add the role to</param>
        /// <param name="employeeRoleID">The employee role type of the link</param>
        /// <param name="employeeID">Employee ID</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> AddOrderItemEmployeeRole(OrderItemData orderItem, short employeeRoleID, short employeeID, string connectionID)
        {
            try
            {
                var role = (await this.GetWhere(r => r.OrderID == orderItem.OrderID && r.OrderItemID == orderItem.ID && r.RoleID == employeeRoleID)).FirstOrDefault();
                if (role == null)
                {
                    role = new OrderEmployeeRole()
                    {
                        OrderID = orderItem.OrderID,
                        OrderItemID = orderItem.ID,
                        EmployeeID = employeeID,
                        RoleID = employeeRoleID
                    };

                    var created = await this.CreateAsync(role);
                    await this.RefreshOrderItem(orderItem.ID, RefreshMessageType.Change);
                }
                else if(employeeID==0)
                {
                    return await this.RemoveOrderItemEmployeeRole(orderItem, employeeRoleID, role.EmployeeID);
                }
                else if (role.EmployeeID != employeeID)
                {
                    role.EmployeeID = employeeID;
                    var updated = await this.UpdateAsync(role, connectionID);
                    await this.RefreshOrderItem(orderItem.ID, RefreshMessageType.Change);
                }

                return new EntityActionChangeResponse
                {
                    Id = orderItem.ID,
                    Message = "Successfully linked Employee Role to OrderItem",
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = orderItem.ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Removes a employee role from an order.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="employeeRoleID">The employee role type of the link</param>
        /// <param name="employeeID">Employee ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> RemoveEmployeeRole(int ID, short employeeRoleID, int employeeID)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                object[] myParams = {
                    new SqlParameter("@bid", BID),
                    new SqlParameter("@orderId", ID),
                    new SqlParameter("@RoleID", null),
                    new SqlParameter("@employeeRoleID", employeeRoleID),
                    new SqlParameter("@employeeID", employeeID),
                    resultParam
                };
                ((SqlParameter)myParams[2]).Value = DBNull.Value;
                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Order.Action.RemoveEmployeeRole] @bid, @orderId, @RoleID, @employeeRoleID, @employeeID;", parameters: myParams);
                if (rowResult > 0)
                {
                    await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.Order), ID);
                    //await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(ID) });

                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "Successfully deleted a employee role.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Removes an Employee Role from an OrderItem.
        /// </summary>
        /// <param name="orderItem">OrderItem to remove the role from</param>
        /// <param name="employeeRoleID">The employee role type of the link</param>
        /// <param name="employeeID">Employee ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> RemoveOrderItemEmployeeRole(OrderItemData orderItem, short employeeRoleID, short employeeID)
        {
            try
            {
                var role = (await this.GetWhere(r => r.OrderID == orderItem.OrderID && r.OrderItemID == orderItem.ID && r.RoleID == employeeRoleID && r.EmployeeID == employeeID)).FirstOrDefault();
                if (role != null)
                    await this.DeleteAsync(role);

                await this.RefreshOrderItem(orderItem.ID, RefreshMessageType.Change);
                return new EntityActionChangeResponse
                {
                    Id = orderItem.ID,
                    Message = "Successfully unlinked Employee Role from OrderItem",
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = orderItem.ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Updates a employee role of an order.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="employeeRoleID">The employee role type of the link</param>
        /// <param name="employeeID">Employee ID</param>
        /// <returns></returns>                       
        public async Task<EntityActionChangeResponse> UpdateEmployeeRole(int ID, short employeeRoleID, int? employeeID = null)
        {
            return await UpdateEmployeeRole(ID, null, employeeRoleID, employeeID);
        }

        /// <summary>
        /// Updates a employee role of an order.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="roleID">Role ID</param>
        /// <param name="employeeRoleID">The employee role type of the link</param>
        /// <param name="employeeID">Employee ID</param>
        /// <returns></returns>                       
        public async Task<EntityActionChangeResponse> UpdateEmployeeRole(int ID, int? roleID, short employeeRoleID, int? employeeID = null)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                object[] myParams = {
                    new SqlParameter("@bid", BID),
                    new SqlParameter("@orderId", ID),
                    new SqlParameter("@employeeRoleID", employeeRoleID),
                    new SqlParameter("@employeeID", employeeID),
                    new SqlParameter("@roleID", roleID),
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Order.Action.UpdateEmployeeRole] @bid, @orderId, @employeeRoleID, @employeeID, @roleID", parameters: myParams);
                if (rowResult > 0)
                {
                    await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.Order), ID);
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(ID) });

                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "Successfully updated a employee role.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Removes a employee role from an order.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="roleID">Role ID</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> RemoveEmployeeRole(int ID, int roleID)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                SqlParameter employeeIdParam = new SqlParameter("@employeeID", DBNull.Value);

                object[] myParams = {
                    new SqlParameter("@bid", BID),
                    new SqlParameter("@orderId", ID),
                    new SqlParameter("@employeeRoleID", DBNull.Value),
                    new SqlParameter("@roleID", roleID),
                    employeeIdParam,
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Order.Action.RemoveEmployeeRole] @bid = @bid, @orderId = @orderId, @roleId = @roleId, @employeeRoleID = @employeeRoleID, @employeeID = @employeeID", parameters: myParams);
                if (rowResult > 0)
                {
                    await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.Order), ID);
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(ID) });

                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "Successfully deleted a employee role.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        private List<RefreshEntity> DoGetRefreshMessages(int orderID)
        {
            return new List<RefreshEntity>()
                        {
                new RefreshEntity()
                {
                    BID = BID,
                    ClasstypeID = (int)ClassType.Order,
                    ID = orderID,
                    DateTime = DateTime.UtcNow,
                    RefreshMessageType = RefreshMessageType.Change,
                }
            };
        }

        /// <returns></returns>
        /// <summary>
        /// Checks if the Parent ID is valid
        /// </summary>
        /// <param name="model">OrderEmployeeRole to check</param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public override bool IsParentIDValid(OrderEmployeeRole model, int parentID)
        {
            return model.OrderID == parentID;
        }

        /// <summary>
        /// Performs additional actions before creating a new entity
        /// </summary>
        /// <param name="newModel">New entity to create</param>
        /// <param name="parentID">Entity's Parent ID</param>
        /// <param name="tempGuid">Temporary GUID for pre-creation reference</param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(OrderEmployeeRole newModel, int parentID, Guid? tempGuid = null)
        {
            if (!IsParentIDValid(newModel, parentID))
                throw new InvalidOperationException();
            //newModel = null;

            await base.DoBeforeCreateAsync(newModel, tempGuid);

            ctx.Entry(newModel).Property(t => t.ModifiedDT).IsModified = false;
        }

        /// <summary>
        /// Performs additional actions before updating an entity
        /// </summary>
        /// <param name="oldModel">Old Entity to update</param>
        /// <param name="parentID">Entity's Parent ID</param>
        /// <param name="newModel">New, Updated Entity</param>
        /// <returns></returns>
        public override async Task DoBeforeUpdateAsync(OrderEmployeeRole oldModel, int parentID, OrderEmployeeRole newModel)
        {
            if (!IsParentIDValid(newModel, parentID) || !IsParentIDValid(oldModel, parentID))
                throw new InvalidOperationException();

            await base.DoBeforeUpdateAsync(oldModel, newModel);

            if (oldModel?.EmployeeID != null && oldModel.EmployeeID == newModel.EmployeeID)
            {
                ctx.Entry(newModel).Property(t => t.EmployeeName).IsModified = false;
            }
        }

        /// <summary>
        /// Performs additional actions before deleting an entity
        /// </summary>
        /// <param name="model">Entity to Delete</param>
        /// <param name="parentID">Entity's Parent ID</param>
        /// <returns></returns>
        public override async Task DoBeforeDeleteAsync(OrderEmployeeRole model, int parentID)
        {
            if (!IsParentIDValid(model, parentID))
                throw new InvalidOperationException();

            await base.DoBeforeDeleteAsync(model);
        }

        /// <summary>
        /// Refresh an OrderItem after updating roles
        /// </summary>
        /// <param name="orderItemID"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private async Task RefreshOrderItem(int orderItemID, RefreshMessageType type)
        {
            await this.rtmClient.SendRefreshMessage(new RefreshMessage(){
                RefreshEntities = new List<RefreshEntity>(){
                    new RefreshEntity(){
                        BID = this.BID,
                        ID = Convert.ToInt32(orderItemID),
                        ClasstypeID = (int)ClassType.OrderItem,
                        DateTime = DateTime.UtcNow,
                        RefreshMessageType = type
                    }
                }
            });
        }

        /// <summary>
        /// Get's the ParentID for an Entity
        /// </summary>
        /// <param name="model">Entity to retrieve the ParentID for</param>
        /// <returns></returns>
        public override int GetParentID(OrderEmployeeRole model)
        {
            return model.OrderID;
        }

        /// <summary>
        /// Set's the ParentID for an Entity
        /// </summary>
        /// <param name="model">Entity to set the ParentID for</param>
        /// <param name="parentID">Parent ID</param>
        /// <returns></returns>
        public override void SetParentID(OrderEmployeeRole model, int parentID)
        {
            model.OrderID = parentID;
        }

        /// <summary>
        /// Sets the ModifiedDT for an Entity
        /// </summary>
        /// <param name="newModel">Entity to set the ModifiedDT for</param>
        protected override void SetModifiedDT(OrderEmployeeRole newModel)
        {
            if(newModel.ID == 0)
            {
                newModel.ModifiedDT = default(DateTime);
            }
            ctx.Entry(newModel).Property(x => x.ModifiedDT).IsModified = false;
        }

        /// <summary>
        /// query filter for where entity has parent of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public override IQueryable<OrderEmployeeRole> WhereParent(IQueryable<OrderEmployeeRole> query, int parentID)
        {
            return query.Where(t => t.OrderID == parentID && t.IsOrderRole);
        }

        /// <summary>
        /// query filter for primary key
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<OrderEmployeeRole> WherePrimary(IQueryable<OrderEmployeeRole> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }
    }
}