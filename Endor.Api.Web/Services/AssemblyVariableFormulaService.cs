﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Assembly Variable Formula Service
    /// </summary>
    public class AssemblyVariableFormulaService : AtomCRUDService<AssemblyVariableFormula, int>,
        IDoBeforeCreateUpdateWithParent<AssemblyVariableFormula, AssemblyVariable>
    {
        /// <summary>
        /// Assembly Element Service
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">Push Client</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public AssemblyVariableFormulaService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        internal override void DoBeforeValidate(AssemblyVariableFormula newModel)
        {

        }

        /// <summary>
        /// Default navigation properties to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { "" };

        /// <summary>
        /// Allows expanding on the default includes
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="ID">CustomFieldLayoutDefinition ID</param>
        /// <returns></returns>
        public override IQueryable<AssemblyVariableFormula> WherePrimary(IQueryable<AssemblyVariableFormula> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<AssemblyVariableFormula, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<AssemblyVariableFormula, int>[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        public void DoBeforeCreateWithParent(AssemblyVariableFormula child, AssemblyVariable parent)
        {
            child.VariableID = parent.ID;
        }

        /// <summary>
        /// Fixes the missing values for IDs when updating specific item. 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="oldParent"></param>
        /// <param name="newParent"></param>
        public void DoBeforeUpdateWithParent(AssemblyVariableFormula child, AssemblyVariable oldParent, AssemblyVariable newParent)
        {
            child.VariableID = newParent.ID;
            // child.BID is 0 by default, so it needs to set the BID based on the parent before updating the variable formula
            child.BID = newParent.BID;
        }

        /// <summary>
        /// DoBeforeDeleteAsync
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override Task DoBeforeDeleteAsync(AssemblyVariableFormula model)
        {
            model.Variable = null;
            return base.DoBeforeDeleteAsync(model);
        }
    } 
}
