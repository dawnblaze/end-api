﻿using ATE.Models;
using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Pricing Engine Service
    /// </summary>
    public class PricingService
    {
        /// <summary>
        /// Db Context
        /// </summary>
        protected readonly ApiContext ctx;
        /// <summary>
        /// BID
        /// </summary>
        public readonly short BID;
        /// <summary>
        /// Logger
        /// </summary>
        protected readonly RemoteLogger logger;
        /// <summary>
        /// rtm Client
        /// </summary>
        protected readonly IRTMPushClient rtmClient;
        /// <summary>
        /// Pricing Engine
        /// </summary>
        private readonly IPricingEngine pricingEngine;
        /// <summary>
        /// object to get cached tenant data
        /// </summary>
        protected readonly ITenantDataCache cache;
        /// <summary>
        /// object to get cached tenant data
        /// </summary>
        protected readonly IMemoryCache inMemoryCache;
        /// <summary>
        /// object to get task Queuer
        /// </summary>
        protected readonly ITaskQueuer taskQueuer;

        protected readonly IMigrationHelper migrationHelper;

        private const int OptionId = 7030;
        private const string OptionName = "Integration.Tax.BusinessKey";

        private OptionService _optionService;
        

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="bid"></param>
        /// <param name="rtmClient"></param>
        /// <param name="pricingEngine"></param>
        /// <param name="cache"></param>
        /// <param name="inMemoryCache"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public PricingService(ApiContext context, RemoteLogger logger, short bid, IRTMPushClient rtmClient, IPricingEngine pricingEngine, IMigrationHelper migrationHelper, ITenantDataCache cache, IMemoryCache inMemoryCache, ITaskQueuer taskQueuer)
        {
            this.ctx = context;
            migrationHelper.MigrateDb(ctx);
            this.migrationHelper = migrationHelper;
            this.BID = bid;
            this.logger = logger;
            this.rtmClient = rtmClient;
            this.pricingEngine = pricingEngine;
            this.cache = cache;
            this._optionService = new OptionService(context, migrationHelper);
            this.inMemoryCache = inMemoryCache;
            this.taskQueuer = taskQueuer;
            
        }

        /// <summary>
        /// Computes price for a line item that has not been saved.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="computeTax"></param>
        /// <returns></returns>
        async public Task<ItemPriceResult> ComputeItem(ItemPriceRequest request, bool computeTax)
        {
            //get tax nexus list
            if (computeTax)
            {
                await AddTaxInfo(request);
            }
            var result = await this.pricingEngine.Compute(request, computeTax);            
            result.TaxInfoList = await UpdateTaxItems(result.TaxInfoList);
            return result;
        }

        private async Task<List<TaxAssessmentResult>> UpdateTaxItems(List<TaxAssessmentResult> taxInfoList)
        {
            TaxGroupService taxGroupService = new TaxGroupService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
            TaxItemService taxItemService = new TaxItemService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper);
            if (taxInfoList!=null)
                foreach (var info in taxInfoList)
                {
                    if (info.NexusID == "1")
                    {
                        var taxGroup = new TaxGroup()
                        {
                            Name = info.InvoiceText,
                            TaxRate = info.TaxRate,
                            IsTaxExempt = false,
                            IsActive = false,
                            BID = BID
                        };
                        var existingGroup = ctx.TaxGroup.FirstOrDefault(x => x.BID == BID && x.Name == info.InvoiceText);
                        if (existingGroup == null)
                        {
                            existingGroup = await taxGroupService.CreateAsync(taxGroup);
                        }
                        info.TaxGroupID = 1;//set to taxJar group
                        foreach (var item in info.Items)
                        {
                            var invoiceText = item.InvoiceText.Split("/#")[0];
                            var splitArray = invoiceText.Split('/');

                            if (splitArray.Length > 1)
                                invoiceText = splitArray[splitArray.Length - 2];
                            var taxItem = new TaxItem()
                            {
                                Name = item.InvoiceText,
                                IsActive = false,
                                TaxRate = item.TaxRate,
                                TaxEngineType = TaxEngineType.TaxJar,
                                BID = BID,
                                InvoiceText = invoiceText,
                                GLAccountID = item.GLAccountID
                            };
                            var existingItem = ctx.TaxItem.FirstOrDefault(x => x.BID == BID && x.Name == item.InvoiceText);
                            if (existingItem == null)
                            {
                                existingItem = await taxItemService.CreateAsync(taxItem);
                                await taxGroupService.LinkTaxItem(existingGroup.ID, existingItem.ID, true);
                            }
                            item.TaxItemID = existingItem.ID;
                        }
                    }
                };
            return taxInfoList;
        }

        private async Task AddTaxInfo(ItemPriceRequest request)
        {
            if ((request.TaxNexusList == null) || (request.TaxNexusList.Count==0))
            {
                request.TaxNexusList = new Dictionary<string, TaxAssessmentNexus>();
                foreach (var Nexus in request.TaxInfoList.Select(x => x.NexusID))
                {
                    request.TaxNexusList.Add(Nexus,GetTaxAssessmentNexus(Nexus));
                }
            }
            string currency = "USD";
            foreach (var nexus in request.TaxNexusList.Values)
            {
                if (nexus.EngineType == TaxEngineType.TaxJar)
                {
                    nexus.ATERegistrationID = await GetRegistrationIDAsync(currency);
                    nexus.ATEApiUrl = await GetATEURLAsync(BID);
                    
                    if (nexus.ToAddress!=null && nexus.ToAddress.State?.Length!=2)
                    {
                        nexus.ToAddress.State = GetStateByName(nexus.ToAddress?.State);
                    }
                    if (nexus.FromAddress != null && nexus.FromAddress.State?.Length != 2)
                    {
                        nexus.FromAddress.State = GetStateByName(nexus.FromAddress?.State);
                    }
                }
            }

        }

        public string GetStateByName(string name)
        {
            switch (name.ToUpper())
            {
                case "ALABAMA":
                    return "AL";

                case "ALASKA":
                    return "AK";

                case "AMERICAN SAMOA":
                    return "AS";

                case "ARIZONA":
                    return "AZ";

                case "ARKANSAS":
                    return "AR";

                case "CALIFORNIA":
                    return "CA";

                case "COLORADO":
                    return "CO";

                case "CONNECTICUT":
                    return "CT";

                case "DELAWARE":
                    return "DE";

                case "DISTRICT OF COLUMBIA":
                    return "DC";

                case "FEDERATED STATES OF MICRONESIA":
                    return "FM";

                case "FLORIDA":
                    return "FL";

                case "GEORGIA":
                    return "GA";

                case "GUAM":
                    return "GU";

                case "HAWAII":
                    return "HI";

                case "IDAHO":
                    return "ID";

                case "ILLINOIS":
                    return "IL";

                case "INDIANA":
                    return "IN";

                case "IOWA":
                    return "IA";

                case "KANSAS":
                    return "KS";

                case "KENTUCKY":
                    return "KY";

                case "LOUISIANA":
                    return "LA";

                case "MAINE":
                    return "ME";

                case "MARSHALL ISLANDS":
                    return "MH";

                case "MARYLAND":
                    return "MD";

                case "MASSACHUSETTS":
                    return "MA";

                case "MICHIGAN":
                    return "MI";

                case "MINNESOTA":
                    return "MN";

                case "MISSISSIPPI":
                    return "MS";

                case "MISSOURI":
                    return "MO";

                case "MONTANA":
                    return "MT";

                case "NEBRASKA":
                    return "NE";

                case "NEVADA":
                    return "NV";

                case "NEW HAMPSHIRE":
                    return "NH";

                case "NEW JERSEY":
                    return "NJ";

                case "NEW MEXICO":
                    return "NM";

                case "NEW YORK":
                    return "NY";

                case "NORTH CAROLINA":
                    return "NC";

                case "NORTH DAKOTA":
                    return "ND";

                case "NORTHERN MARIANA ISLANDS":
                    return "MP";

                case "OHIO":
                    return "OH";

                case "OKLAHOMA":
                    return "OK";

                case "OREGON":
                    return "OR";

                case "PALAU":
                    return "PW";

                case "PENNSYLVANIA":
                    return "PA";

                case "PUERTO RICO":
                    return "PR";

                case "RHODE ISLAND":
                    return "RI";

                case "SOUTH CAROLINA":
                    return "SC";

                case "SOUTH DAKOTA":
                    return "SD";

                case "TENNESSEE":
                    return "TN";

                case "TEXAS":
                    return "TX";

                case "UTAH":
                    return "UT";

                case "VERMONT":
                    return "VT";

                case "VIRGIN ISLANDS":
                    return "VI";

                case "VIRGINIA":
                    return "VA";

                case "WASHINGTON":
                    return "WA";

                case "WEST VIRGINIA":
                    return "WV";

                case "WISCONSIN":
                    return "WI";

                case "WYOMING":
                    return "WY";
                case "ALBERTA":
                    return "AB";
                case "BRITISH COLUMBIA":
                    return "BC";
                case "MANITOBA":
                    return "MB";
                case "NEW BRUNSWICK":
                    return "NB";
                case "NEWFOUNDLAND AND LABRADOR":
                    return "NL";
                case "NOVA SCOTIA":
                    return "NS";
                case "NORTHWEST TERRITORIES":
                    return "NT";
                case "NUNAVUT":
                    return "NU";
                case "ONTARIO":
                    return "ON";
                case "PRINCE EDWARD ISLAND":
                    return "PE";
                case "QUEBEC":
                    return "QC";
                case "SASKATCHEWAN":
                    return "SK";
                case "YUKON":
                    return "YT";
            }

            throw new Exception("State could not be found in address.");
        }

        private TaxAssessmentNexus GetTaxAssessmentNexus(string nexusID)
        {
            short taxGroupID = Int16.Parse(nexusID);
            var taxGroup = ctx.TaxGroup.Where(x => x.BID == this.BID && x.ID == taxGroupID).FirstOrDefault();
            if (taxGroup == null) throw new Exception("Valid Tax Group could not be found");
            var taxItems = ctx.TaxGroupItemLink.Where(x => x.BID == this.BID && x.GroupID == taxGroupID).Select(x => x.TaxItem);
            var taxNexusRequest = new TaxAssessmentNexus()
            {
                EngineType = Models.TaxEngineType.Internal,
                InvoiceText = taxGroup.Name,
                TaxGroupID = taxGroupID,
                TaxAssessmentItems = taxItems.Select(x => new TaxAssessmentNexusItem()
                {
                    TaxItemID = x.ID,
                    InvoiceText = x.InvoiceText,
                    TaxRate = x.TaxRate
                }).ToList()
            };
            return taxNexusRequest;
        }

        /// <summary>
        /// Gets the registration key of the business
        /// </summary>
        /// <returns></returns>
        public async Task<Guid> GetRegistrationIDAsync(string currency)
        {
            //see if we have a registration key
            string key = "ExternalTaxEngineRegistration-" + this.BID.ToString();
            var registrationID = inMemoryCache.Get(key);
            if (registrationID!=null) return new Guid((string)registrationID);
            var businessKey = await PaymentAPEService.GetGenericBusinessKeyAsync(this.ctx, this.BID, await GetATEURLAsync(this.BID), "ate", _optionService, OptionId, OptionName);

            string ATE_URL = $"{await GetATEURLAsync(this.BID)}/ate";
            var timeZone = await PaymentAPEService.GetTimeZoneIDAsync(ctx, this.BID);
            var nexus = new Nexus();
            nexus.PhysicalAddresses = new List<ATE.Models.Address>();
            var locators = ctx.LocationLocator.Where(l => l.BID == BID && l.LocatorType == (byte)LocatorType.Address);
            foreach (var locator in locators)
            {
                AddressMetaData address = null;
                var paymentAddress = new ATE.Models.Address
                {
                    Street = "",
                    City = "",
                    State = "",
                    Country = "",
                    Zip = ""
                };
                if (locator != null)
                {
                    address = (AddressMetaData)locator.MetaDataObject;
                    paymentAddress.Street = address.Street1 + (!String.IsNullOrEmpty(address.Street2) ? " " + address.Street2 : "");
                    paymentAddress.City = address.City;
                    paymentAddress.State = address.State;
                    paymentAddress.Country = address.Country;
                    paymentAddress.Zip = address.PostalCode;
                }
                nexus.PhysicalAddresses.Add(paymentAddress);
            }
            nexus.EconomicRegions = new List<CountryNexus>()
            {
                new CountryNexus() {Country = "US"},
                new CountryNexus() {Country = "CA"},
                new CountryNexus() {Country = "UK"},
                new CountryNexus() {Country = "AU"},
                new CountryNexus() {Country = "NZ"}
            };
            var registration = new RegistrationRequest
            {
                AppType = "Endor",
                BusinessKey = businessKey,
                Currency = currency,
                IsSandboxed = true,
                Name = "Registration",
                ProcessorType = (int)ATE.Models.Processor.TaxJar,
                TimeZone = timeZone,
                Nexus = nexus
            };

            ATE.Models.RegistrationResponse registrationResponse = null;
            using (var client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, ATE_URL + "/registration");
                var registrationJSONString = JsonConvert.SerializeObject(registration);
                request.Content = new StringContent(registrationJSONString, Encoding.UTF8, "application/json");
                var response = await client.SendAsync(request);
                string responseString = await response.Content.ReadAsStringAsync();

                switch (response.StatusCode)
                {
                    case HttpStatusCode.OK:
                        registrationResponse = JsonConvert.DeserializeObject<ATE.Models.RegistrationResponse>(responseString);
                        break;
                    case HttpStatusCode.NotFound:
                        throw new InvalidOperationException("business key not found");
                    case HttpStatusCode.InternalServerError:
                        throw new InvalidOperationException("ATE error!");
                }
            }

            inMemoryCache.Set(key, registrationResponse.RegistrationID.ToString(),
                new DateTimeOffset(DateTime.UtcNow.Date.AddTicks(-1).AddDays(1)));
            return registrationResponse.RegistrationID;
        }

        private async Task<string> GetATEURLAsync(short bid)
        {
            return (await cache.Get(bid)).ExternalTaxEngineURL;
        }

        /// <summary>
        /// Computes price for an order that has not been saved.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="computeTax"></param>
        /// <returns></returns>
        async public Task<OrderPriceResult> ComputeOrder(OrderPriceRequest request, bool computeTax)
        {
            //get tax nexus list
            if (computeTax)
            {
                foreach (var item in request.Items)
                {
                    await AddTaxInfo(item);
                }
                if ((request.TaxNexusList == null) || (request.TaxNexusList.Count == 0))
                {
                    request.TaxNexusList = new Dictionary<string, TaxAssessmentNexus>();
                    foreach (var item in request.Items)
                    {
                        if (item.TaxNexusList == null) continue;
                        foreach (var taxNexus in item.TaxNexusList)
                        {
                            if (!request.TaxNexusList.ContainsKey(taxNexus.Key))
                                request.TaxNexusList.Add(taxNexus.Key, taxNexus.Value);
                        }
                    }
                }
                string currency = "USD";
                foreach (var nexus in request.TaxNexusList.Values)
                {
                    if (nexus.EngineType == TaxEngineType.TaxJar)
                    {
                        nexus.ATERegistrationID = await GetRegistrationIDAsync(currency);
                        nexus.ATEApiUrl = await GetATEURLAsync(BID);
                        if (nexus.ToAddress != null && nexus.ToAddress.State?.Length != 2)
                        {
                            nexus.ToAddress.State = GetStateByName(nexus.ToAddress?.State);
                        }
                        if (nexus.FromAddress != null && nexus.FromAddress.State?.Length != 2)
                        {
                            nexus.FromAddress.State = GetStateByName(nexus.FromAddress?.State);
                        }
                    }
                }
            }
            var result = await this.pricingEngine.Compute(request, computeTax);
            result.TaxInfoList = await UpdateTaxItems(result.TaxInfoList);
            return result;

        }

        /// <summary>
        /// Computes price for a destination that has not been saved.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="computeTax"></param>
        /// <returns></returns>
        async public Task<DestinationPriceResult> ComputeDestination(DestinationPriceRequest request, bool computeTax)
        {
            return await Task.FromResult(this.pricingEngine.Compute(request, computeTax));
        }


        /// <summary>
        /// Computes price for a machine template
        /// </summary>
        /// <param name="request"></param>
        /// <param name="machineTemplateID"></param>
        /// <returns></returns>
        async public Task<Dictionary<string, VariableValue>> ComputeAssembly(int machineTemplateID, Dictionary<string, VariableValue> request)
        {
            // creates the request
            ItemPriceRequest itemPriceRequest = new ItemPriceRequest();
            itemPriceRequest.Components = new List<ComponentPriceRequest>()
            {
                new ComponentPriceRequest() 
                {
                    ComponentID = machineTemplateID,
                    Variables = request,
                    ComponentType = OrderItemComponentType.Assembly,
                    TotalQuantity = 1
                }
            };


            // passes the request to be computed
            var itemPriceResult = await this.pricingEngine.Compute(itemPriceRequest, false);
            // gets the component that was passed
            var component = itemPriceResult.Components.Where(c => c.ComponentID == machineTemplateID).FirstOrDefault();
            // returns the variables 
            return component.Variables;
        }
    }
}
