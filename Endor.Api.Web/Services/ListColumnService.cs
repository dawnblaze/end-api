﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// A service for <see cref=" SystemListColumn"/>
    /// </summary>
    public class ListColumnService
    {
        private readonly ApiContext _ctx;
        private readonly IMigrationHelper _migrationHelper;

        /// <summary>
        /// Business ID
        /// </summary>
        public short BID { get; }

        /// <summary>
        /// Constructs a List Column service with injected params
        /// </summary>
        /// <param name="context">Api context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public ListColumnService(ApiContext context, short bid, IMigrationHelper migrationHelper)
        {
            this._ctx = context;
            this._migrationHelper = migrationHelper;
            this._migrationHelper.MigrateDb(_ctx);
            this.BID = bid;
        }

        /// <summary>
        /// Gets system list columns from the DB, layering per-User changes on top by querying Option values
        /// </summary>
        /// <param name="cTID"></param>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        internal async Task<List<SystemListColumn>> GetColumnsAsync(int cTID, int employeeID)
        {
            GetOptionValueResponse option = await new OptionService(this._ctx, _migrationHelper).Get(null, GetUserColumnOptionName(cTID), BID, null, null, employeeID, null, null);
            List<SystemListColumn> columns = await this._ctx.SystemListColumn.Where(x => x.ClassTypeID == cTID).ToListAsync();

            if (option?.Value != null)
            {
                UserListColumn[] userColumns = JsonConvert.DeserializeObject<UserListColumn[]>(option.Value.Value);
                if (userColumns != null)
                {
                    Dictionary<string, UserListColumn> fieldToUserColumnMap = new Dictionary<string, UserListColumn>();
                    fieldToUserColumnMap = userColumns.ToDictionary(x => x.Field);
                    foreach(var col in columns)
                    {

                        if (fieldToUserColumnMap.TryGetValue(col.Field, out UserListColumn matchingUserColumn))
                        {
                            col.IsExpandable = matchingUserColumn.IsExpandable;
                            col.IsFrozen = matchingUserColumn.IsFrozen;
                            col.IsVisible = matchingUserColumn.IsVisible;
                            col.SortIndex = matchingUserColumn.SortIndex;
                        }
                    }
                }
            }

            return columns;

        }

        /// <summary>
        /// Gets the key of the option used for user column data for a given CTID
        /// </summary>
        /// <param name="cTID"></param>
        /// <returns></returns>
        private string GetUserColumnOptionName(int cTID)
        {
            return $"UserColumn|{cTID}";
        }

        internal Task<bool> DeleteUserColumnsAsync(int cTID, int value)
        {
            var optionsSvc = new OptionService(this._ctx, _migrationHelper);
            
            //no way to update Options yet
            throw new NotImplementedException();
        }

        internal Task<SystemListColumn> AddOrUpdateColumnsAsync(int cTID, int value, UserListColumn userColumn)
        {
            //no way to update Options yet
            throw new NotImplementedException();
        }
    }
}
