﻿using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
//using Endor.Api.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using EFFunc = Microsoft.EntityFrameworkCore.EF;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Create/Read/Update/Delete Service w/ support for automatic includes on Read and parent-child associations on Create/Update/Delete
    /// </summary>
    /// <typeparam name="M">Model type</typeparam>
    /// <typeparam name="I">ID type</typeparam>
    public abstract class AtomCRUDService<M, I> : BaseCRUDService<M>
        where M : class, IAtom<I>, IAtom
        where I : struct, IConvertible
    {
        /// <summary>
        /// object to queue tasks with
        /// </summary>
        protected readonly ITaskQueuer taskQueuer;

        /// <summary>
        /// object to get cached tenant data
        /// </summary>
        protected readonly ITenantDataCache cache;

        /// <summary>
        /// Options Service
        /// </summary>
        protected readonly OptionService _optionService;

        /// <summary>
        /// Constructs an AtomCRUDService
        /// </summary>
        /// <param name="context">An APIContext injected</param>
        /// <param name="bid">the business ID for this service instance</param>
        /// <param name="taskQueuer">a </param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public AtomCRUDService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, logger, bid, rtmClient, migrationHelper)
        {
            this.taskQueuer = taskQueuer;
            this.cache = cache;
            this.childAssociations = this.GetChildAssociations();
            this._optionService = new OptionService(context, migrationHelper);
        }

        #region child associations
        /// <summary>
        /// An array of child associations. The array may be null
        /// </summary>
        protected readonly IChildServiceAssociation<M, I>[] childAssociations;

        /// <summary>
        /// Get child associations (services that handle business logic for child collections). May return null.
        /// </summary>
        /// <returns>IChildServiceAssociation or null</returns>
        protected abstract IChildServiceAssociation<M, I>[] GetChildAssociations();

        /// <summary>
        /// A helper method to construct child associations
        /// </summary>
        /// <remarks>
        /// This mostly exists so concrete classes do not need to specify M and I every time
        /// </remarks>
        /// <typeparam name="cS">Child Service type</typeparam>
        /// <typeparam name="cM">Child Model type</typeparam>
        /// <typeparam name="cI">Child ID type</typeparam>
        /// <param name="accessor">A function that returns the list of type cM</param>
        /// <returns></returns>
        protected ChildAtomServiceAssociation<cS, M, I, cM, cI> CreateChildAssociation<cS, cM, cI>(Func<M, ICollection<cM>> accessor)
            where cS : AtomCRUDService<cM, cI>
            where cM : class, IAtom<cI>
            where cI : struct, IConvertible
        {
            return new ChildAtomServiceAssociation<cS, M, I, cM, cI>(this.ctx, this.taskQueuer, this.cache, this.logger, this.rtmClient, this, accessor, migrationHelper);
        }

        /// <summary>
        /// A helper method to construct child associations to link tables
        /// </summary>
        /// <remarks>
        /// This mostly exists so concrete classes do not need to specify M and I every time
        /// </remarks>
        /// <typeparam name="cS">Child Service type</typeparam>
        /// <typeparam name="cM">Child Model type</typeparam>
        /// <param name="accessor">A function that returns the list of type cM</param>
        /// <returns></returns>
        protected ChildLinkServiceAssociation<cS, M, I, cM> CreateChildLinkAssociation<cS, cM>(Func<M, ICollection<cM>> accessor)
            where cS : LinkCRUDService<cM>
            where cM : class
        {
            return new ChildLinkServiceAssociation<cS, M, I, cM>(this.ctx, this.logger, this.rtmClient, this, accessor, migrationHelper);
        }
        #endregion

        /// <summary>
        /// Business logic to perform before model validation
        /// </summary>
        /// <param name="newModel">Model to validate</param>
        internal abstract void DoBeforeValidate(M newModel);

        #region Retrieve

        /// <summary>
        /// Names of the navigations properties included by default, if any
        /// </summary>
        /// <returns>Array of ICollection property names to be included</returns>
        public virtual string[] IncludeDefaults { get; }

        /// <summary>
        /// Get the names of navigation properties to include on Get
        /// </summary>
        /// <param name="includes"></param>
        /// <returns></returns>
        public abstract string[] GetIncludes(IExpandIncludes includes = null);

        /// <summary>
        /// Gets all models asynchronously with any specified includes
        /// </summary>
        /// <param name="includes">Child/Parent items to include</param>
        /// <returns></returns>
        public virtual async Task<List<M>> GetAsync(IExpandIncludes includes = null)
        {
            List<M> results = await ctx.Set<M>().IncludeAll(GetIncludes(includes)).Where(m => m.BID == BID).ToListAsync();

            return await this.MapCollection(results, includes);
        }

        /// <summary>
        /// Gets all models asynchronously based on specified filters and with specified includes
        /// </summary>
        /// <param name="filterPredicate">Filter criteria</param>
        /// <param name="includes">Child/Parent items to include</param>
        /// <returns></returns>
        public async Task<List<M>> FilteredGetAsync(Expression<Func<M, bool>> filterPredicate, IExpandIncludes includes = null)
        {
            List<M> results = await ctx.Set<M>()
                                        .IncludeAll(GetIncludes(includes))
                                        .Where(filterPredicate)
                                        .Where(m => m.BID == BID)
                                        .ToListAsync();

            return await this.MapCollection(results, includes);
        }

        /// <summary>
        /// Gets a single model asynchronously by ID with any specified includes
        /// </summary>
        /// <param name="ID">Model ID</param>
        /// <param name="includes">Child/Parent items to include</param>
        /// <returns></returns>
        public virtual async Task<M> GetAsync(I ID, IExpandIncludes includes = null)
        {
            var includedQ = ctx.Set<M>().IncludeAll(GetIncludes(includes));
            M result = await WherePrimary(includedQ, ID).FirstOrDefaultAsync();
            if (result != null)
                await this.MapItem(result, includes);
            return result;
        }

        /// <summary>
        /// Gets a single model asynchronously by ID and query
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public abstract IQueryable<M> WherePrimary(IQueryable<M> query, I ID);

        /// <summary>
        /// Maps a collection of models
        /// </summary>
        /// <param name="collection">Collection of models</param>
        /// <param name="includes">Child/Parent items to include</param>
        /// <returns></returns>
        public virtual async Task<List<M>> MapCollection(List<M> collection, IExpandIncludes includes = null)
        {
            collection.ForEach(async (item) => await this.MapItem(item, includes));
            await Task.CompletedTask;
            return collection;
        }

        /// <summary>
        /// Maps a specific item.
        /// </summary>
        /// <param name="item">Item to map</param>
        /// <param name="includes">Child/Parent includes</param>
        public virtual Task MapItem(M item, IExpandIncludes includes = null)
        {
            return Task.CompletedTask;
        }

        /// <summary>
        /// Gets a the first or default model for a given predicate
        /// </summary>
        /// <param name="predicate">Predicate to search against</param>
        /// <param name="asNoTracking">Set to true to disable entity tracking</param>
        /// <returns></returns>
        public Task<M> FirstOrDefaultAsync(Expression<Func<M, bool>> predicate, bool asNoTracking = false)
        {
            if (asNoTracking)
                return ctx.Set<M>().IncludeAll(GetIncludes()).AsNoTracking().Where(m => m.BID == BID).FirstOrDefaultAsync(predicate);
            else
                return ctx.Set<M>().IncludeAll(GetIncludes()).Where(m => m.BID == BID).FirstOrDefaultAsync(predicate);
        }

        /// <summary>
        /// Gets a collection of models for a given predicate
        /// </summary>
        /// <param name="predicate">Predicate to search against</param>
        /// <returns></returns>
        public async Task<List<M>> GetWhere(Expression<Func<M, bool>> predicate)
        {
            var results = await ctx.Set<M>().IncludeAll(GetIncludes()).Where(m => m.BID == BID).Where(predicate).ToListAsync();

            return await this.MapCollection(results);
        }

        /// <summary>
        /// A where clause method
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="includeAll"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public IQueryable<M> Where(Expression<Func<M, bool>> predicate, bool includeAll = false, IExpandIncludes includes = null)
        {
            IQueryable<M> query = ctx.Set<M>();
            if (includeAll)
                query = query.IncludeAll(GetIncludes());
            else if (includes != null)
                query = query.IncludeAll(GetIncludes(includes));

            query = query.Where(m => m.BID == BID).Where(predicate);

            return query;
        }

        /// <summary>
        /// A where clause method
        /// </summary>
        /// <param name="predicates"></param>
        /// <param name="includeAll"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public IQueryable<M> Where(Expression<Func<M, bool>>[] predicates, bool includeAll = false, IExpandIncludes includes = null)
        {
            IQueryable<M> query = ctx.Set<M>();
            if (includeAll)
                query = query.IncludeAll(GetIncludes());
            else if (includes != null)
                query = query.IncludeAll(GetIncludes(includes));

            query = query.Where(m => m.BID == BID).WhereAll(predicates);

            return query;
        }



        /// <summary>
        /// Gets a collection of models for a list of predicates
        /// </summary>
        /// <param name="predicates">List of predicates to search against</param>
        /// <returns></returns>
        public async Task<List<M>> GetWhere(Expression<Func<M, bool>>[] predicates)
        {
            var results = await ctx.Set<M>().IncludeAll(GetIncludes()).Where(m => m.BID == BID).WhereAll(predicates).ToListAsync();

            return await this.MapCollection(results);
        }

        /// <summary>
        /// Gets a collection of models for a list of predicates and mapping using some mapping object
        /// </summary>
        /// <param name="predicates">List of predicates to search against</param>
        /// <param name="mapper"></param>
        /// <returns></returns>
        public IEnumerable<S> SelectWhereAll<S>(Expression<Func<M, bool>>[] predicates, IQueryFilterMapper<M, S> mapper)
        {
            return mapper.Map(ctx.Set<M>().Where(m => m.BID == BID).WhereAll(predicates));
        }

        /// <summary>
        /// Gets an untracked snapshot of the model before new updates are made
        /// </summary>
        /// <param name="newModel">Updated version of the model</param>
        /// <returns></returns>
        public override async Task<M> GetOldModelAsync(M newModel)
        {
            return await WherePrimary(ctx.Set<M>().AsNoTracking(), newModel.ID).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Gets the next number for a given BID and ClassTypeID
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="classTypeID">ClassTypeID</param>
        /// <returns></returns>
        public async Task<I> GetNextNumber(short bid, int classTypeID)
        {
            return (I)Convert.ChangeType(await RequestIDIntegerAsync(bid, classTypeID), typeof(I));
        }

        /// <summary>
        /// Gets the next number for a given BID and ClassTypeID
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="classTypeID">ClassTypeID</param>
        /// <returns></returns>
        public async Task<I> GetMaxNumber(short bid, int classTypeID)
        {
            return (I)Convert.ChangeType(await RequestMaxIDIntegerAsync(bid, classTypeID), typeof(I));
        }

        /// <summary>
        /// Sets the next number for a given BID and ClassTypeID
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="classTypeID">ClassTypeID</param>
        /// <param name="nextID">Next ID</param>
        /// <param name="avoidLowering">Specifies whether to avoid lowering ID</param>
        /// <returns></returns>
        public async Task SetNextNumber(short bid, int classTypeID, int nextID, bool avoidLowering = false)
        {
            await SetIDIntegerAsync(bid, classTypeID, nextID, avoidLowering);
        }

        /// <summary>
        /// Requests an ID given the BID, after validating the Type has a mapped ClassType
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="t"></param>
        /// <exception cref="ArgumentException">if Type t is not mapped to a ClassType</exception>
        /// <returns></returns>
        protected virtual async Task<I> RequestIDAsync(short bid, Type t)
        {
            ClassType? ct = t.ClassType();
            if (ct.HasValue)
                return await RequestIDAsync(bid, (int)ct);
            else
                throw new ArgumentException($"Cannot request ID for null ClassType: {t.FullName} does not have a mapping; check t4 template");
        }

        /// <summary>
        /// Requests an ID from SQL via SPROC
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="classtypeID">Class Type ID</param>
        /// <returns></returns>
        protected async Task<I> RequestIDAsync(short bid, int classtypeID)
        {
            int newID = await RequestIDIntegerAsync(bid, classtypeID);
            try
            {
                return (I)Convert.ChangeType(newID, typeof(I));
            }
            catch (Exception e)
            {
                throw new Exception($"[Util.ID.GetID] returned '{newID}', then failed to Convert.ChangeType into {typeof(I).FullName}", e);
            }
        }

        /// <summary>
        /// Requests an ID from SQL via SPROC as an INT
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="classtypeID">Class Type ID</param>
        /// <returns></returns>
        protected async Task<int> RequestIDIntegerAsync(short bid, int classtypeID)
        {
            string sqlExecute = "EXEC @Result = dbo.[Util.ID.GetID] @BID, @ClassTypeID, @Count";

            List<object> myParams = new List<object>(){
                new SqlParameter("@BID", bid),
                new SqlParameter("@ClassTypeID", classtypeID),
                new SqlParameter("@Count", 1)
            };

            if (typeof(I).Equals(typeof(byte)))
            {
                myParams.Add(new SqlParameter("@StartingID", 10));
                sqlExecute += ", @StartingID";
            }

            SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
            {
                Direction = System.Data.ParameterDirection.Output
            };
            myParams.Add(resultParam);

            await ctx.Database.ExecuteSqlRawAsync(sqlExecute, parameters: myParams);

            return Convert.ToInt32(resultParam.Value);
        }

        /// <summary>
        /// Requests an ID from SQL via SPROC as an INT
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="classtypeID">Class Type ID</param>
        /// <returns></returns>
        protected async Task<int> RequestMaxIDIntegerAsync(short bid, int classtypeID)
        {
            string sqlExecute = "EXEC @Result = dbo.[Util.ID.GetMaxID] @BID, @ClassTypeID, @Count";

            List<object> myParams = new List<object>(){
                new SqlParameter("@BID", bid),
                new SqlParameter("@ClassTypeID", classtypeID),
                new SqlParameter("@Count", 1)
            };

            if (typeof(I).Equals(typeof(byte)))
            {
                myParams.Add(new SqlParameter("@StartingID", 10));
                sqlExecute += ", @StartingID";
            }

            SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
            {
                Direction = System.Data.ParameterDirection.Output
            };
            myParams.Add(resultParam);

            await ctx.Database.ExecuteSqlRawAsync(sqlExecute, parameters: myParams);

            return Convert.ToInt32(resultParam.Value);
        }

        /// <summary>
        /// Sets the next ID from SQL via SPROC
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="classtypeID">Class Type ID</param>
        /// <param name="nextID">Next ID</param>
        /// <param name="avoidLowering">Specifies whether to avoid lowering ID</param>
        /// <returns></returns>
        protected async Task SetIDIntegerAsync(short bid, int classtypeID, int nextID, bool avoidLowering = false)
        {
            string sqlExecute = "EXEC dbo.[Util.ID.SetID] @BID, @ClassTypeID, @NextID, @AvoidLowering";

            List<object> myParams = new List<object>(){
                new SqlParameter("@BID", bid),
                new SqlParameter("@ClassTypeID", classtypeID),
                new SqlParameter("@NextID", nextID),
                new SqlParameter("@AvoidLowering", avoidLowering)
                };
            await ctx.Database.ExecuteSqlRawAsync(sqlExecute, parameters: myParams);
        }
        #endregion

        #region Create

        /// <summary>
        /// Business logic on add. Base class handles ID and ClassTypeID 
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public override async Task DoBeforeCreateAsync(M newModel, Guid? tempGuid = null)
        {
            await this.DoBeforeCreateAsync(newModel, tempGuid);
        }

        /// <summary>
        /// Business logic on add. Base class handles ID and ClassTypeID
        /// ID fetch is optional
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <param name="getID"></param>
        public async Task DoBeforeCreateAsync(M newModel, Guid? tempGuid = null, bool getID = true)
        {
            this.SetModifiedDT(newModel);

            newModel.BID = this.BID;
            if (getID)
                newModel.ID = await RequestIDAsync(newModel.BID, typeof(M));

            Task childAdd = childAssociations?.DoBeforeAddAsync(newModel);

            if (childAdd != null)
                await childAdd;

            //EF knows the SQL column is computed, so it throws errors if it isn't zero
            newModel.ClassTypeID = 0;
            ctx.Set<M>().Add(newModel);
        }

        /// <summary>
        /// Business logic to do after adding the new model.
        /// </summary>
        /// <param name="model">Model that was added</param>
        /// <param name="tempGuid">The model's temporary guid from before creation</param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(M model, Guid? tempGuid)
        {
            try
            {
                if (tempGuid.HasValue)
                    await PersistDocuments(model, tempGuid);

                await QueueIndexForModel(model.ID);
                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnCreate(model) });
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Indexing failed after CREATE", ex);
            }
        }

        /// <summary>
        /// Keeps any objects associated with the newly created model
        /// </summary>
        /// <param name="model">Newly created model</param>
        /// <param name="tempGuid">Temporary GUID associated with the model</param>
        /// <returns></returns>
        private async Task PersistDocuments(M model, Guid? tempGuid)
        {
            StorageContext ctx = new StorageContext(BID, BucketRequest.All, new DocumentStorage.Models.DMID() { guid = tempGuid });
            var documentManager = new DocumentManager(cache, ctx);
            await documentManager.PersistDocumentsAsync(tempGuid.Value, Convert.ToInt32(model.ID), model.ClassTypeID);
        }

        #endregion

        #region Update

        /// <summary>
        /// Attempts to change the IsActive status on an entity
        /// </summary>
        /// <param name="ID">Entity's ID</param>
        /// <param name="active">Active status to set to</param>
        /// <param name="connectionID">Connection ID</param>
        /// <returns></returns>
        public virtual async Task<EntityActionChangeResponse<I>> SetActive(I ID, bool active, string connectionID)
        {
            EntityActionChangeResponse<I> resp = null;

            try
            {
                M model = await GetAsync(ID);

                if (model == null)
                    return new EntityActionChangeResponse<I>()
                    {
                        Message = "Entity not found",
                        ErrorMessage = "Entity not found",
                        Success = false,
                        Id = ID
                    };

                PropertyInfo isActiveProp = model.GetType().GetProperty("IsActive", BindingFlags.Public | BindingFlags.Instance);

                if (isActiveProp != null && isActiveProp.CanWrite)
                    isActiveProp.SetValue(model, active);

                M upResp = await this.UpdateAsync(model, connectionID);

                if (upResp != null && ((bool)upResp.GetType().GetProperty("IsActive", BindingFlags.Public | BindingFlags.Instance).GetValue(upResp) == active))
                {
                    string name = GetName(upResp.GetType(), upResp, new string[] { "Name", "LongName", "DomainName" });

                    resp = new EntityActionChangeResponse<I>()
                    {
                        Message =
                            string.IsNullOrWhiteSpace(name)
                            ? $"Status updated to {(active ? "active" : "inactive")}."
                            : $"{name} status updated to {(active ? "active" : "inactive")}.",
                        Success = true,
                        Id = ID
                    };
                }
                else
                {
                    resp = new EntityActionChangeResponse<I>()
                    {
                        Message = "Unable to update active status.",
                        ErrorMessage = "Unable to update active status.",
                        Success = false,
                        Id = ID
                    };
                }
            }
            catch (Exception ex)
            {
                resp = new EntityActionChangeResponse<I>()
                {
                    Message = ex.Message,
                    ErrorMessage = ex.Message,
                    Success = false,
                    Id = ID
                };
            }

            return resp;
        }

        private string GetName(Type type, M upResp, string[] namePropertyNameList)
        {
            for (int i = 0; i < namePropertyNameList.Length; i++)
            {
                var prop = type.GetProperty(namePropertyNameList[i]);

                if (prop == null)
                    continue;
                else
                {
                    return prop.GetValue(upResp).ToString();
                }
            }

            return null;
        }

        /// <summary>
        /// Business logic on update. Base class handles ModifiedDT
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        public override async Task DoBeforeUpdateAsync(M oldModel, M newModel)
        {
            bool isUnsavedEntityID = Convert.ToInt32(newModel.ID) == UnsavedEntityID;

            if (newModel.BID == 0)
                newModel.BID = this.BID; // simple fix to issues saving children

            if (!isUnsavedEntityID && ctx.Entry(newModel).State == EntityState.Added)
            {
                //noop
            }
            else if (isUnsavedEntityID)
            {
                await DoBeforeCreateAsync(newModel, null);
            }
            else
            {
                //Have to use oldModel.BID because newModel doesn't have BID set
                var existingEntry = ctx.ChangeTracker.Entries<M>().FirstOrDefault(t => t.Entity.BID == oldModel.BID && Convert.ToInt32(t.Entity.ID) == Convert.ToInt32(oldModel.ID));
                if (existingEntry!=null)
                {
                    try
                    {
                        existingEntry.State = EntityState.Detached;
                    } catch (Exception ex)
                    {
                        await logger.Error(this.BID, "Error", ex);
                    }
                    
                }
                //EF knows the SQL column is computed, so it throws errors if it isn't zero
                newModel.ClassTypeID = default(int);
                ctx.Entry(newModel).State = EntityState.Modified;
                this.SetModifiedDT(newModel);

            }

            Task childUpdate = childAssociations?.DoBeforeUpdateAsync(oldModel, newModel);

            if (childUpdate != null)
                await childUpdate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newModel"></param>
        protected virtual void SetModifiedDT(M newModel)
        {
            if (newModel is IModifyDateTimeTrackable)
                (newModel as IModifyDateTimeTrackable).ModifiedDT = DateTime.UtcNow;
        }

        /// <summary>
        /// Business logic for after having updated a model
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel">Model object to update</param>
        /// <param name="connectionID"></param>
        /// <returns></returns>
        protected override async Task DoAfterUpdateAsync(M oldModel, M newModel, string connectionID)
        {
            try
            {
                await QueueIndexForModel(newModel.ID);
                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnUpdate(newModel) }, connectionID);
                await new UserDraftService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper).DeletePendingDrafts(newModel.ClassTypeID, Convert.ToInt32(newModel.ID));
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Indexing failed after UPDATE", ex);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Checks if the Entity can be deleted
        /// </summary>
        /// <param name="id">Entity's ID</param>
        /// <returns></returns>
        public virtual async Task<BooleanResponse> CanDelete(I id)
        {
            //if (!await this.ExistsAsync(id))
            //{
            //    return new BooleanResponse()
            //    {
            //        Message = "Not found",
            //        Success = false,
            //        Value = null,
            //    };

            //}

            try
            {
                var bidParam = new SqlParameter("@BID", this.BID);
                var idParam = new SqlParameter("@ID", typeof(I)) { Value = id };
                var classTypeIdParam = new SqlParameter("@ClassTypeID", typeof(M).ClassType().GetValueOrDefault(0));
                var showCantDeleteReasonParam = new SqlParameter("@ShowCantDeleteReason", true);
                var resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Bit);
                var cantDeleteReasonParam = new SqlParameter("@CantDeleteReason", System.Data.SqlDbType.VarChar, 1024);

                resultParam.Direction = System.Data.ParameterDirection.Output;
                cantDeleteReasonParam.Direction = System.Data.ParameterDirection.Output;

                object[] myParams = {
                    bidParam,
                    idParam,
                    classTypeIdParam,
                    showCantDeleteReasonParam,
                    cantDeleteReasonParam,
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC dbo.[IAtom.Action.CanDelete] @BID, @ID, @ClassTypeID, @ShowCantDeleteReason, @Result OUTPUT, @CantDeleteReason OUTPUT;", parameters: myParams);
                if (resultParam.Value != null)
                {
                    if ((bool)resultParam.Value)
                        return new BooleanResponse()
                        {
                            Message = $"CanDelete: {resultParam.Value}",
                            Success = true,
                            Value = (bool)resultParam.Value
                        };
                    else
                        return new BooleanResponse()
                        {
                            Message = cantDeleteReasonParam.Value.ToString(),
                            Success = true,
                            Value = (bool)resultParam.Value
                        };
                }
                else
                {
                    return new BooleanResponse()
                    {
                        Message = cantDeleteReasonParam.Value.ToString(),
                        Success = false,
                        Value = null,
                    };
                }
            }
            catch (Exception ex)
            {
                return new BooleanResponse()
                {
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Business logic on delete
        /// </summary>
        /// <param name="model"></param>
        public override async Task DoBeforeDeleteAsync(M model)
        {
            var modelClassTypeID = model.ClassTypeID;
            //add this if because if childAssociations is null the await fails
            if (childAssociations != null) 
                await childAssociations?.DoBeforeDeleteAsync(model);
            //EF knows the SQL column is computed, so it throws errors if it isn't zero
            model.ClassTypeID = default(int);

            // can't remove an entity if it is detached 
            if (ctx.Entry(model).State == EntityState.Detached)
            {
                ctx.Entry(model).State = EntityState.Modified;
            }

            ctx.Set<M>().Remove(model);

            model.ClassTypeID = modelClassTypeID;//reset the original classTypeID since it will be needed by for the refreshMsg
        }

        /// <summary>
        /// Business logic to perform after deleting a model
        /// </summary>
        /// <param name="model">Deleted model</param>
        /// <returns></returns>
        protected override async Task DoAfterDeleteAsync(M model)
        {
            try
            {
                await QueueIndexForModel(model.ID);
                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnDelete(model) });
                await new UserDraftService(ctx, BID, taskQueuer, cache, logger, rtmClient, migrationHelper).DeletePendingDrafts(model.ClassTypeID, Convert.ToInt32(model.ID));
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Indexing failed after DELETE", ex);
            }
        }

        #endregion

        #region Clone

        /// <summary>
        /// Gets an untracked copy of the model by ID
        /// </summary>
        /// <param name="ID">Model ID</param>
        /// <returns></returns>
        public Task<M> GetClone(I ID)
        {
            //trick with EF to clone:
            //https://stackoverflow.com/questions/42504782/clone-and-link-child-using-asnotracking-in-entity-framework
            //loads entities without tracking them so changing the IDs out and calling Add makes them insert new records
            return WherePrimary(ctx.Set<M>().AsNoTracking().IncludeAll(GetIncludesForClone()), ID).FirstOrDefaultAsync();
        }

        /// <summary>
        /// get includes for cloning
        /// </summary>
        /// <returns></returns>
        public virtual string[] GetIncludesForClone()
        {
            return this.GetIncludes();
        }


        /// <summary>
        /// Gets the Next Name for a Cloned entity
        /// </summary>
        /// <param name="query"></param>
        /// <param name="originalName"></param>
        /// <param name="nameSelector"></param>
        /// <returns></returns>
        public string GetNextClonedName(IEnumerable<dynamic> query, string originalName, Func<M, string> nameSelector)
        {
            var names = query.Select(t =>
            {
                if (nameSelector(t) == originalName)
                    return new { Name = nameSelector(t), LastCloneNumber = (int?)0 };

                if (nameSelector(t) == (originalName + " (Clone)"))
                    return new { Name = nameSelector(t), LastCloneNumber = (int?)1 };

                if ((nameSelector(t).StartsWith(originalName + " (Clone) ") && nameSelector(t).EndsWith("(Clone)")))
                    return new { Name = nameSelector(t), LastCloneNumber = (int?)null };
                bool hasDataPastClone = nameSelector(t).Length > originalName.Length + "(Clone) (".Length;
                if (hasDataPastClone)
                {
                    int startIndex = originalName.Length + " (Clone) (".Length;
                    int lengthSub = nameSelector(t).Length - originalName.Length - " (Clone) (".Length - 1;
                    if (lengthSub > 0)
                    {
                        string substringNumberSelection = nameSelector(t).Substring(startIndex, lengthSub);
                        bool matchedNameAndCloneWithNumber = substringNumberSelection.All(c => (char.IsNumber(c)));
                        if (matchedNameAndCloneWithNumber)
                            return new { Name = nameSelector(t), LastCloneNumber = (int?)int.Parse(substringNumberSelection) };
                    }
                }

                return new { Name = nameSelector(t), LastCloneNumber = (int?)null };
            }).OrderByDescending(t => t.LastCloneNumber).ToList();

            var lastOfTheNames = names.FirstOrDefault();

            if (lastOfTheNames != null && lastOfTheNames.LastCloneNumber.GetValueOrDefault(0) > 0)
            {
                return originalName + " (Clone)" + $" ({lastOfTheNames.LastCloneNumber + 1})";
            }

            return originalName + " (Clone)";
        }


        /// <summary>
        /// Clones a model from a given ID
        /// </summary>
        /// <param name="ID">ID of the model to clone</param>
        /// <returns></returns>
        public async Task<M> CloneAsync(I ID)
        {
            try
            {
                M clone = await this.GetClone(ID);

                await DoBeforeCloneAsync(clone);

                M cloneAfterSave = await this.CreateAsync(clone, null);

                if (cloneAfterSave != null)
                    await DoAfterCloneAsync(ID, cloneAfterSave);

                return cloneAfterSave;
            }
            catch (Exception e)
            {
                await logger.Error(this.BID, "Clone failed", e);
                //return a more meaningful message...
                throw new Exception($"Clone failed - {e.Message}", e);
            }
        }

        /// <summary>
        /// Business logic to perform before Cloning a model
        /// </summary>
        /// <param name="clone">Model to clone</param>
        /// <returns></returns>
        public virtual async Task DoBeforeCloneAsync(M clone)
        {
            clone.ID = await RequestIDAsync(clone.BID, clone.ClassTypeID);

            if (this.childAssociations != null)
            {
                foreach (var v in this.childAssociations)
                {
                    await v.ForEachChildDoBeforeCloneAsync(clone);
                }
            }
        }

        /// <summary>
        /// Business logic to perform after Cloning a model
        /// </summary>
        /// <param name="clonedFromID"></param>
        /// <param name="clone">Cloned model</param>
        /// <returns></returns>
        public virtual async Task DoAfterCloneAsync(I clonedFromID, M clone)
        {
            try
            {
                await QueueIndexForModel(clone.ID);
                await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessagesOnCreate(clone) });
            }
            catch (Exception ex)
            {
                await logger.Error(this.BID, "Indexing failed after CLONE", ex);
            }
        }

        #endregion

        #region RefreshEntities

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newModel"></param>
        /// <returns></returns>
        public virtual List<RefreshEntity> DoGetRefreshMessagesOnCreate(M newModel)
        {
            return new List<RefreshEntity>()
            {
                new RefreshEntity()
                {
                    ClasstypeID = newModel.ClassTypeID,
                    DateTime = newModel.ModifiedDT,
                    RefreshMessageType = RefreshMessageType.New,
                    BID = newModel.BID,
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="update"></param>
        /// <returns></returns>
        public virtual List<RefreshEntity> DoGetRefreshMessagesOnUpdate(M update)
        {
            return new List<RefreshEntity>()
            {
                new RefreshEntity()
                {
                    ClasstypeID = update.ClassTypeID,
                    ID = Convert.ToInt32(update.ID),
                    DateTime = update.ModifiedDT,
                    RefreshMessageType = RefreshMessageType.Change,
                    BID = update.BID,
                },
                new RefreshEntity()
                {
                    ClasstypeID = update.ClassTypeID,
                    DateTime = update.ModifiedDT,
                    RefreshMessageType = RefreshMessageType.Change,
                    BID = update.BID
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public virtual List<RefreshEntity> DoGetRefreshMessagesOnDelete(M deleted)
        {
            return new List<RefreshEntity>()
            {
                new RefreshEntity()
                {
                    BID = deleted.BID,
                    ClasstypeID = deleted.ClassTypeID,
                    ID = Convert.ToInt32(deleted.ID),
                    DateTime = DateTime.UtcNow,
                    RefreshMessageType = RefreshMessageType.Delete,
                },
                new RefreshEntity()
                {
                    BID = deleted.BID,
                    ClasstypeID = deleted.ClassTypeID,
                    DateTime = DateTime.UtcNow,
                    RefreshMessageType = RefreshMessageType.Delete,
                }
            };
        }


        #endregion RefreshEntities

        #region Queue Index

        /// <summary>
        /// Re-index the the model
        /// </summary>
        /// <param name="id">Model Id</param>
        /// <returns></returns>
        public async Task QueueIndexForModel(I id)
        {
            await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(typeof(M).ClassType()), Convert.ToInt32(id));
        }

        /// <summary>
        /// Re-index the the models
        /// </summary>
        /// <param name="ids">int[]</param>
        /// <returns></returns>
        protected async Task QueueIndexForModels(ICollection<I> ids)
        {
            if (ids == null)
            {
                return;
            }

            if (ids.Count < 1)
            {
                return;
            }

            var intIDs = ids.Select(num => Convert.ToInt32(num)).ToList();
            await this.taskQueuer.IndexMultipleID(this.BID, Convert.ToInt32(typeof(M).ClassType()), intIDs);
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="update"></param>
        protected void SetUpdateModelProperties(int ID, M update)
        {
            update.ID = (I)Convert.ChangeType(ID, typeof(I));
            SetCommonModelProperties(update);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newModel"></param>
        protected void SetCreateModelProperties(M newModel)
        {
            SetCommonModelProperties(newModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        protected void SetCommonModelProperties(M model)
        {
            model.BID = this.BID;
        }

        #region NoTracking Helper funcitons
        /*/// <summary>
        /// </summary>
        /// <param name="ID">ID of entity to look for</param>
        /// <returns>True if found else false</returns>
        public async Task<bool> ExistsAsync(I ID)
        {
            var set = ctx.Set<M>().AsNoTracking();
            if (typeof(I) == typeof(int))
                return (await set.Select(t => new { BID = t.BID, ID = ((IAtom<int>)t).ID }).FirstOrDefaultAsync(t => t.BID == this.BID && t.ID == ID)) != null;

            if (typeof(I) == typeof(short))
                return (await set.Select(t => new { BID = t.BID, ID = ((IAtom<short>)t).ID }).FirstOrDefaultAsync(t => t.BID == this.BID && t.ID == ID)) != null;

            if (typeof(I) == typeof(byte))
                return (await set.Select(t => new { BID = t.BID, ID = ((IAtom<byte>)t).ID }).FirstOrDefaultAsync(t => t.BID == this.BID && t.ID == ID)) != null;

            return (await set.Select(t => new { BID = t.BID, ID = Convert.ToInt32(t.ID) }).FirstOrDefaultAsync(t => t.BID == this.BID && t.ID == ID)) != null;
        }*/

        #endregion
        #region instance lazy factory functions
        /// <summary>
        /// Creates a service for a different set of entities (for use for child entities)
        /// </summary>
        /// <typeparam name="S">The service type</typeparam>
        /// <typeparam name="sM"></typeparam>
        /// <typeparam name="sI"></typeparam>
        /// <param name="ctx"></param>
        /// <param name="bid"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <returns></returns>
        public Lazy<S> GetLazyChildAtomService<S, sM, sI>(ApiContext ctx, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, short bid, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            where S : AtomCRUDService<sM, sI>
            where sM : class, IAtom<sI>
            where sI : struct, IConvertible
        {
            return AtomCRUDService<sM, sI>.CreateService<S>(ctx, taskQueuer, cache, logger, bid, rtmClient, migrationHelper);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <typeparam name="sM"></typeparam>
        /// <param name="ctx"></param>
        /// <param name="logger"></param>
        /// <param name="bid"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <returns></returns>
        public Lazy<S> GetLazyChildLinkService<S, sM>(ApiContext ctx, RemoteLogger logger, short bid, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            where S : LinkCRUDService<sM>
            where sM : class
        {
            return LinkCRUDService<sM>.CreateService<S>(ctx, logger, bid, rtmClient, migrationHelper);
        }

        /// <summary>
        /// Lazy link service generator for link services with CRUD service dependencies
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <typeparam name="sM"></typeparam>
        /// <param name="ctx"></param>
        /// <param name="logger"></param>
        /// <param name="bid"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <returns></returns>
        public Lazy<S> GetLazyChildLinkService<S, sM>(ApiContext ctx, RemoteLogger logger, short bid, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            where S : LinkCRUDService<sM>
            where sM : class
        {
            return LinkCRUDService<sM>.CreateService<S>(ctx, logger, bid, rtmClient, taskQueuer, cache, migrationHelper);
        }
        #endregion

        #region static factory functions
        /// <summary>
        /// Sets up a lazy service from the types in this service given static values
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="ctx"></param>
        /// <param name="bid"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="logger"></param>
        /// <param name="cache"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <returns></returns>
        public static Lazy<S> CreateService<S>(ApiContext ctx, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, short bid, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            where S : AtomCRUDService<M, I>
        {
            //if you change this, change the function version too
            return new Lazy<S>(() => Activator.CreateInstance(typeof(S), ctx, bid, taskQueuer, cache, logger, rtmClient, migrationHelper) as S);
        }

        /// <summary>
        /// Creates a lazy service from the types in this service, given a value and a BID function
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="ctx"></param>
        /// <param name="bidFunc"></param>
        /// <param name="cache"></param>
        /// <param name="logger"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <returns></returns>
        public static Lazy<S> CreateService<S>(ApiContext ctx, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, Func<short> bidFunc, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            where S : AtomCRUDService<M, I>
        {
            //if you change this, change the non-function version too
            return new Lazy<S>(() => 
            Activator.CreateInstance(typeof(S), ctx, bidFunc(), taskQueuer, cache, logger, rtmClient, migrationHelper) as S);
        }
        #endregion

        /// <summary>
        ///  get complete details of the current user's userlink
        /// </summary>
        /// <param name="userClaim">System.Security.Claims.ClaimsPrincipal</param>
        public async Task<UserLink> GetUserLinkAsync(System.Security.Claims.ClaimsPrincipal userClaim)
        {
            return await this.ctx.UserLink
                            .Include(x => x.Employee)
                            .Include(x => x.Contact)
                            .FirstOrDefaultAsync(x => x.BID == userClaim.BID().Value && x.AuthUserID == userClaim.UserID().Value);
        }

        /// <summary>
        /// Initalizes the Document Management Storage
        /// </summary>
        /// <param name="model">Model for which storage will effect</param>
        /// <returns></returns>
        public async Task InitializeStorageAsync(M model)
        {
            var docman = this.GetDocumentManager(model);
            await docman.InitializeAllAsync();
        }

        /// <summary>
        /// Initializes the Custom Shared Folder in document storage
        /// </summary>
        /// <param name="model">Model for which storage will effect</param>
        /// <returns></returns>
        public async Task InitializeCustomerSharedFolderAsync(M model)
        {
            var templateDMID = new Endor.DocumentStorage.Models.DMID()
            {
                ctid = model.ClassTypeID,
                classFolder = "template"
            };
            var templateStorage = new StorageContext(model.BID, BucketRequest.Documents, templateDMID);
            var templateManager = new DocumentManager(this.cache, templateStorage);

            var permissions = new Endor.AzureStorage.DocumentPermissionOptions();
            permissions.CanDelete = false;
            permissions.CanMove = false;
            permissions.CanRename = false;
            permissions.IsShared = true;
            await templateManager.AddFolder("Customer Shared", int.Parse($"{model.ID}"), model.ClassTypeID, permissions);
        }

        private DocumentManager GetDocumentManager(M model)
        {
            var dmid = new Endor.DocumentStorage.Models.DMID()
            {
                ctid = model.ClassTypeID,
                id = int.Parse($"{model.ID}")
            };
            var storage = new StorageContext(model.BID, BucketRequest.Documents, dmid);
            return new DocumentManager(this.cache, storage);
        }


        /// <summary>
        /// Get an Option Value for a User by Option Name
        /// </summary>
        /// <param name="optionName"></param>
        /// <param name="userLinkID"></param>
        /// <returns></returns>
        protected async Task<GetOptionValueResponse> GetUserOptionValueByName(string optionName, int? userLinkID = null)
        {
            return await _optionService.Get(optionID: null, optionName: optionName, locationId: null, bid: this.BID, companyID: null, contactID: null, storeFrontID: null, userLinkID: userLinkID);
        }



        /// <summary>
        /// Gets a DocumentManager instance for M based on ID
        /// </summary>
        /// <param name="id">ID of entity to get DocumentManager Instance For</param>
        /// <param name="classType">ClassTypeID of entity to get DocumentManager Instance For</param>
        /// <param name="bucketRequest">Bucket of Storage Context</param>
        /// <returns></returns>
        protected DocumentManager GetDocumentManager(I id, ClassType classType, BucketRequest bucketRequest)
        {
            var dmid = new DocumentStorage.Models.DMID()
            {
                ctid = (int?)classType,
                id = int.Parse($"{id}")
            };
            var storage = new StorageContext(this.BID, bucketRequest, dmid);
            return new DocumentManager(this.cache, storage);
        }


        /// <summary>
        /// Gets a DocumentManager instance for M based on DMID
        /// </summary>
        /// <param name="dmid"></param>
        /// <param name="bucketRequest"></param>
        /// <returns></returns>
        protected DocumentManager GetDocumentManager(DocumentStorage.Models.DMID dmid, BucketRequest bucketRequest)
        {
            var storage = new StorageContext(this.BID, bucketRequest, dmid);
            return new DocumentManager(this.cache, storage);
        }

        /// <summary>
        /// Gets a paginated list of model
        /// </summary>
        /// <param name="filters">Query parameters to filter a property</param>
        /// <param name="skip">Number of items to skip</param>
        /// <param name="take">Number of items to take</param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public async Task<PagedList<M>> GetPagedListAsync(IQueryFilters<M> filters, int? skip = null, int? take = null, IExpandIncludes includes = null)
        {
            var pagedList = new PagedList<M>();

            //apply filters to queryable first
            IQueryable<M> filteredQuery = this.ctx.Set<M>().Where(m => m.BID == BID);

            if (filters != null)
            {
                filteredQuery = this.Where(filters.WherePredicates(), false, includes);
            }
            else if (includes != null)
            {
                filteredQuery = filteredQuery.IncludeAll(GetIncludes(includes));
            }

            //count the unpaged result
            pagedList.TotalCount = await filteredQuery.CountAsync();

            //apply paging if present
            if (skip.HasValue)
            {
                filteredQuery = filteredQuery.Skip(skip.Value);
            }

            if (take.HasValue)
            {
                filteredQuery = filteredQuery.Take(take.Value);
            }

            pagedList.Results = await filteredQuery.ToListAsync();

            if (includes != null)
                pagedList.Results = await this.MapCollection(pagedList.Results.ToList(), includes);

            return pagedList;
        }
    }
}
