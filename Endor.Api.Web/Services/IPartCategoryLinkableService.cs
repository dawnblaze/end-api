﻿using Endor.Api.Web.Classes;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// delegate for a categoryService method that links categories to parts
    /// </summary>
    /// <param name="partCategoryID"></param>
    /// <param name="partID"></param>
    /// <param name="isLinked"></param>
    /// <param name="indexModel"></param>
    /// <returns></returns>
    public delegate Task<EntityActionChangeResponse> PartCategoryLinkMethod(short partCategoryID, int partID, bool isLinked, bool indexModel = true);

    /// <summary>
    /// interface for entities who have links to part categories
    /// </summary>
    public interface IPartCategoryLinkableService
    {
        /// <summary>
        /// queue index accessor
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task QueueIndexForModel(int id);
    }

    /// <summary>
    /// business logic for generic linking
    /// </summary>
    public static class PartCategoryLinkableServiceExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entitySvc"></param>
        /// <param name="categorySvcLinkMethod"></param>
        /// <param name="entityID"></param>
        /// <param name="oldCategoryIDs"></param>
        /// <param name="newCategoryIDs"></param>
        /// <param name="indexCategory"></param>
        /// <returns></returns>
        public static async Task LinkOrUnlinkCategories(this IPartCategoryLinkableService entitySvc, PartCategoryLinkMethod categorySvcLinkMethod, int entityID, short[] oldCategoryIDs, short[] newCategoryIDs, bool indexCategory = true)
        {
            bool anySuccess = false;
            var removedLinkCategoryIDs = oldCategoryIDs.Where(oldCategoryID => !newCategoryIDs.Any(newID => newID == oldCategoryID)).ToList();
            foreach (var removedLinkCategoryID in removedLinkCategoryIDs)
            {
                EntityActionChangeResponse response = await categorySvcLinkMethod(removedLinkCategoryID, entityID, false, indexCategory);
                anySuccess = anySuccess || response.Success;
            }

            var newCategorIDs = newCategoryIDs.Where(newCategoryID => !oldCategoryIDs.Any(oldCategoryID => oldCategoryID == newCategoryID)).ToList();
            foreach (var newCategoryID in newCategoryIDs)
            {
                EntityActionChangeResponse response = await categorySvcLinkMethod(newCategoryID, entityID, true, indexCategory);
                anySuccess = anySuccess || response.Success;
            }

            if (anySuccess)
                await entitySvc.QueueIndexForModel(entityID);
        }
    }
}
