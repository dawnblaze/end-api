﻿using Endor.EF;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Endor.Tasks;
using Endor.Api.Web.Classes;
using Endor.Tenant;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.RTM.Enums;
using System.Threading;

namespace Endor.Api.Web.Services
{
    /// <summary>
    /// Order Date Key Service Layer
    /// </summary>
    public class OrderKeyDateService : AtomCRUDService<OrderKeyDate, int>, ICloneableChildCRUDService<OrderData, OrderKeyDate>
    {
        /// <summary>
        /// Order Service Layer
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="bid">Business ID</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderKeyDateService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Default children to include
        /// </summary>
        public override string[] IncludeDefaults => new string[] { };

        /// <summary>
        /// Returns the includes
        /// </summary>
        /// <param name="includes">Children to include</param>
        /// <returns></returns>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return includes.ToIncludesArray(this.IncludeDefaults);
        }

        /// <summary>
        /// GetChildAssociations
        /// </summary>
        /// <returns></returns>
        protected override IChildServiceAssociation<OrderKeyDate, int>[] GetChildAssociations()
        {
            return null;
        }

        internal override void DoBeforeValidate(OrderKeyDate newModel)
        {
        }

        /// <summary>
        /// Gets a key date based on order ID and KeyDateType
        /// </summary>
        /// <param name="OrderID">Order Id</param>
        /// <param name="orderKeyDateType">OrderKeyDateType</param>
        /// <returns></returns>
        public async Task<OrderKeyDate> GetOrderKeyDate(int OrderID, OrderKeyDateType orderKeyDateType)
        {
            return (await this.FilteredGetAsync(t => ((t.OrderID == OrderID) && (t.KeyDateType == orderKeyDateType)) , null)).FirstOrDefault();
        }

        /// <summary>
        /// Gets a key date based on order ID, order item ID and KeyDateType
        /// </summary>
        /// <param name="OrderID">Order Id</param>
        /// <param name="OrderItemID">Order Item Id</param>
        /// <param name="orderKeyDateType">OrderKeyDateType</param>
        /// <returns></returns>
        public async Task<OrderKeyDate> GetOrderItemKeyDate(int OrderID, int OrderItemID, OrderKeyDateType orderKeyDateType)
        {
            return (await this.FilteredGetAsync(t => ((t.OrderID == OrderID) && (t.OrderItemID == OrderItemID) && (t.KeyDateType == orderKeyDateType)), null)).FirstOrDefault();
        }

        /// <summary>
        /// Sets a key date value.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="keyDateType">The Key Date Type to be set</param>
        /// <param name="dateTime">Date (and optionally Time) to set the key date to.  If not set, use the current date/time</param>
        /// <param name="isFirmDate">The Date is a firm date</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> ChangeKeyDate(int ID, OrderKeyDateType keyDateType, DateTime? dateTime, bool? isFirmDate)
        {
            return await ChangeKeyDate(ID, null, keyDateType, dateTime, isFirmDate);
        }

        /// <summary>
        /// Sets a key date value.
        /// </summary>
        /// <param name="OrderID">Order ID</param>
        /// <param name="OrderItemID">Order Item ID</param>
        /// <param name="keyDateType">The Key Date Type to be set</param>
        /// <param name="dateTime">Date (and optionally Time) to set the key date to.  If not set, use the current date/time</param>
        /// <param name="isFirmDate">The Date is a firm date</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> ChangeKeyDate(int OrderID, int? OrderItemID, OrderKeyDateType keyDateType, DateTime? dateTime, bool? isFirmDate)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                object[] myParams = {
                    new SqlParameter("@bid", BID),
                    new SqlParameter("@orderId", OrderID),
                    new SqlParameter("@OrderItemID", (object) OrderItemID ?? DBNull.Value),
                    new SqlParameter("@DestinationID", DBNull.Value),
                    new SqlParameter("@keyDateType", keyDateType),
                    dateTime.HasValue ? new SqlParameter("@dateTime", dateTime.Value) : new SqlParameter("@dateTime", DBNull.Value),
                    new SqlParameter("@isFirmDate", isFirmDate ?? false),
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Order.Action.ChangeKeyDate] @bid, @orderId, @OrderItemID, @DestinationID, @keyDateType, @dateTime, @isFirmDate;", parameters: myParams);
                if (rowResult > 0)
                {
                    await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.Order), OrderID);
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(OrderID) });

                    return new EntityActionChangeResponse
                    {
                        Id = OrderID,
                        Message = "Successfully set the key date of an order.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = OrderID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = OrderID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Removes a key date value.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="keyDateType">The Key Date Type to be set</param>
        /// <returns></returns>
        public async Task<EntityActionChangeResponse> RemoveKeyDate(int ID, OrderKeyDateType keyDateType)
        {
            try
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                object[] myParams = {
                    new SqlParameter("@bid", BID),
                    new SqlParameter("@orderId", ID),
                    new SqlParameter("@OrderItemID", DBNull.Value),
                    new SqlParameter("@DestinationID", DBNull.Value),
                    new SqlParameter("@keyDateType", (byte)keyDateType),
                    resultParam
                };

                int rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Order.Action.RemoveKeyDate] @bid, @orderId, @OrderItemID, @DestinationID, @keyDateType;", parameters: myParams);
                if (rowResult > 0)
                {
                    await this.taskQueuer.IndexModel(this.BID, Convert.ToInt32(ClassType.Order), ID);
                    await rtmClient.SendRefreshMessage(new RefreshMessage() { RefreshEntities = DoGetRefreshMessages(ID) });

                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "Successfully deleted the key date of an order.",
                        Success = true
                    };
                }
                else
                {
                    return new EntityActionChangeResponse
                    {
                        Id = ID,
                        Message = "No rows affected.",
                        Success = false
                    };
                }
            }
            catch (Exception ex)
            {
                return new EntityActionChangeResponse
                {
                    Id = ID,
                    Message = ex.Message,
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        private List<RefreshEntity> DoGetRefreshMessages(int orderID)
        {
            return new List<RefreshEntity>()
                        {
                new RefreshEntity()
                {
                    BID = BID,
                    ClasstypeID = (int)ClassType.Order,
                    ID = orderID,
                    DateTime = DateTime.UtcNow,
                    RefreshMessageType = RefreshMessageType.Change,
                }
            };
        }

        /// <summary>
        /// Sets the ID of childModel to a new ID and the ParentID of childModel to parentModel.ID
        /// </summary>
        /// <param name="parentModel"></param>
        /// <param name="childModel"></param>
        /// <returns></returns>
        public Task DoBeforeCloneForParent(OrderData parentModel, OrderKeyDate childModel)
        {
            childModel.OrderID = parentModel.ID;
            return Task.CompletedTask;
        }

        /// <summary>
        /// query filter for where primary key of ID
        /// </summary>
        /// <param name="query"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IQueryable<OrderKeyDate> WherePrimary(IQueryable<OrderKeyDate> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// END-9643 Change the order save to handle the Balance Due key date.
        /// Computes balance due date.
        /// When the order is saved, compute the Balance Due date and save it unless the date is NULL.
        /// When the order is saved, delete Balance Due Key Date if it is present and should be NULL
        /// </summary>
        /// <param name="BID">BID</param>
        /// <param name="OrderID">Order ID</param>
        /// <returns></returns>
        public async Task<DateTime?> ComputeBalanceDueDate(short BID, int OrderID)
        {
            try
            {
                object[] myParams = {
                    new SqlParameter("@BID", BID),
                    new SqlParameter("@OrderID", OrderID)
                };

                IEnumerable<BalanceDueDateResult> result = await ctx.Database.GetModelFromQueryAsync<BalanceDueDateResult>("EXEC dbo.[Order.Action.ComputeBalanceDueDate] @BID, @OrderID;", parameters: myParams);

                return result.FirstOrDefault()?.BalanceDueDate;
   
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// END-10014 Change the order save to handle the Early Payment key date.
        /// When the order is saved, if the status is Invoiced, 
        /// compute the Early Payment date and save it unless the date is NULL.
        /// </summary>
        /// <param name="BID">BID</param>
        /// <param name="OrderID">Order ID</param>
        /// <returns></returns>
        public async Task<DateTime?> ComputeEarlyPaymentDate(short BID, int OrderID)
        {
            try
            {
                object[] myParams = {
                    new SqlParameter("@BID", BID),
                    new SqlParameter("@OrderID", OrderID)
                };

                IEnumerable<EarlyPaymentDateResult> result = await ctx.Database.GetModelFromQueryAsync<EarlyPaymentDateResult>("EXEC dbo.[Order.Action.ComputeEarlyPaymentDate] @BID, @OrderID;", parameters: myParams);

                return result.FirstOrDefault()?.EarlyPaymentDate;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}