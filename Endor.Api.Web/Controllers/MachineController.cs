﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Web.Annotation;
using Endor.CBEL.Autocomplete;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Machine Controller
    /// </summary>
    [Route("api/machinepart")]
    public class MachineController : CRUDController<MachineData, MachineService, short>, ISimpleListableViewController<SimpleMachineData, short>
    {
        /// <summary>
        /// List service for Machine controller
        /// </summary>
        public ISimpleListableViewService<SimpleMachineData, short> ListableService => this._service;
        /// <summary>
        /// Machine Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper"></param>
        /// <param name="autocompleteEngine"></param>
        public MachineController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper, IAutocompleteEngine autocompleteEngine)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper, (BID) => {
                return new MachineService(context, BID, taskQueuer, cache, logger, rtmClient, migrationHelper, autocompleteEngine);
            })
        {
        }

        /// <summary>
        /// Returns all of the MachineDatas
        /// can be filtered by IsActive
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MachineData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MachineData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] MachineFilter filters)
        {
            return new OkObjectResult(await _service.GetWithFiltersAsync(filters));
        }

        #region Overridden CRUD Methods
        /// <summary>
        /// Returns all of the MachineDatas
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsoletegetall")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MachineData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MachineData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns a single MachineData by ID
        /// </summary>
        /// <param name="ID">MachineData ID</param>
        /// <param name="includes"></param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MachineData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MachineData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWith(short ID, [FromQuery] MachineDataIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }

        /// <summary>
        /// Returns a single MachineData by ID
        /// </summary>
        /// <param name="ID">MachineData ID</param>
        /// <returns></returns>
        [HttpGet("obsolete/{ID}")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MachineData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MachineData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single MachineData by ID
        /// </summary>
        /// <param name="ID">MachineData ID</param>
        /// <param name="update">Updated MachineData data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MachineData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MachineData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the MachineDatas does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] MachineData update)
        {
            try
            {
                return await base.Update(ID, update);
            }
            catch (InvalidOperationException e)
            {
                GenericResponse<AssemblyData> genericResponse = new GenericResponse<AssemblyData>() { ErrorMessage = e.Message };
                return genericResponse.ToResult();
            }
        }

        /// <summary>
        /// Creates a new MachineData
        /// </summary>
        /// <param name="newModel">Updated MachineData data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MachineData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the MachineDatas creation fails")]
        public override async Task<IActionResult> Create([FromBody] MachineData newModel, [FromQuery] Guid? tempID = null)
        {
            try
            {
                return await base.Create(newModel, tempID);
            }
            catch (InvalidOperationException e)
            {
                GenericResponse<AssemblyData> genericResponse = new GenericResponse<AssemblyData>() { ErrorMessage = e.Message };
                return genericResponse.ToResult();
            }
        }

        /// <summary>
        /// Deletes a MachineData by ID
        /// </summary>
        /// <param name="ID">ID of the MachineData to delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MachineData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the MachineDatas does not exist or the deletion fails")]
        public override async Task<IActionResult> Delete(short ID)
        {
            await this._service.unlinkAllCategoryAsync(ID);
            return await base.Delete(ID);
        }
        #endregion Overridden CRUD Methods


        /// <summary>
        /// Returns true | false based on whether the MachineData can be deleted.
        /// </summary>
        /// <param name="ID">ID of the MachineData to delete</param>
        /// <returns></returns>
        [HttpGet("{ID}/action/candelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete(ID)).ToResult();
        }

        /// <summary>
        /// Sets a MachineData to Active
        /// </summary>
        /// <param name="ID">MachineData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the MachineData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the MachineData does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets a MachineData to Inactive
        /// </summary>
        /// <param name="ID">MachineData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the MachineData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the MachineData does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Gets a Simple list of MachineDatas
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleMachineData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleMachineData[]> FilteredSimpleList([FromQuery] SimpleListItemIsActiveFilter<SimpleMachineData, short> filters)
        {
            return await this.GetSimpleList(User.BID().Value, filters);
        }

        /// <summary>
        /// Gets a SimpleList base on categoryIDs and ids
        /// </summary>
        /// <returns></returns>
        [Route("simplelist/categoryandid")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleMachineData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<ICollection<SimpleMachineData>> GetByIDsAndCategories([FromQuery]ICollection<int> id, [FromQuery]ICollection<int> categoryid)
        {
            return await this._service.GetByIDsAndCategoriesAsync(id, categoryid);
        }

        /// <summary>
        /// Gets a SimpleList base on variableID and ProfileName
        /// </summary>
        /// <returns></returns>
        [Route("simplelist/byvariable/{variableID}")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleMachineData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<ICollection<SimpleMachineData>> GetByVariableID(int variableID, [FromQuery] int? machineID, [FromQuery] string profileName)
        {
            return await this._service.GetByVariableAndProfile(variableID, machineID, profileName);
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsoletesimplelist")]
        [Obsolete]
        public Task<SimpleMachineData[]> SimpleList()
        {
            return this.FilteredSimpleList(null);
        }

        /// <summary>
        /// links a machine category to a machine part
        /// </summary>
        /// <param name="machineDataID">MachineData.ID</param>
        /// <param name="machineCategoryID">MachineCategory.ID</param>
        /// <returns></returns>
        [HttpPost("{machineDataID}/action/linkmachinecategory/{machineCategoryID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> LinkMachine([FromRoute]int machineDataID, [FromRoute]short machineCategoryID)
        {
            return (await this._service.LinkMachineCategory(machineDataID, machineCategoryID, true)).ToResult();
        }

        /// <summary>
        /// unlinks a machine category from a machine part
        /// </summary>
        /// <param name="machineDataID">MachineData.ID</param>
        /// <param name="machineCategoryID">MachineCategory.ID</param>
        /// <returns></returns>
        [HttpPost("{machineDataID}/action/unlinkmachinecategory/{machineCategoryID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> UnLinkMachine([FromRoute]int machineDataID, [FromRoute]short machineCategoryID)
        {
            return (await this._service.LinkMachineCategory(machineDataID, machineCategoryID, false)).ToResult();
        }

        /// <summary>
        /// TryValidateModel
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            var machine = model as MachineData;
            var activeProfiles = (machine?.Profiles ?? new List<MachineProfile>())
                                    .Where(p => p.IsActive)
                                    .ToList();

            if (activeProfiles.Count < 1)
            {
                this.ModelState.AddModelError("MachineProfiles", "At least one active profile is required");
            }

            return base.TryValidateModel(model, prefix);
        }

        /// <summary>
        /// Get the Machines that has TemplateID of
        /// </summary>
        /// <param name="templateID"></param>
        /// <param name="CategoryID"></param>
        /// <param name="MachineID"></param>
        /// <param name="IncludeInactive"></param>
        /// <returns></returns>
        [HttpGet("list/template/{templateID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MachinesAndCategories))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If specified TemplateID does not exist")]
        public async Task<Object> MachinesWithTemplateID([FromRoute] int templateID, [FromQuery]ICollection<int> CategoryID, [FromQuery]ICollection<int> MachineID, [FromQuery]bool IncludeInactive = false)
        {
            Object result = await this._service.GetMachinesWithTemplateID(templateID, CategoryID, MachineID, IncludeInactive);

            if (result == null)
                return NotFound();

            return Ok(result);
        }
    }
}
