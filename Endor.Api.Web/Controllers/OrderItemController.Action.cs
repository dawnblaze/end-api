﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Tenant;
using Endor.Tasks;
using Endor.Api.Web.Classes.Request;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    public partial class OrderItemController
    {
        #region Move
        /// <summary>
        /// Moves a line item before another item with in an order
        /// </summary>
        /// <returns></returns>
        [Route("{ID}/action/movebefore/{targetItemID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "One or both of the item IDs is not found.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "The target item is not in the same order as the source item.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> MoveBefore(int ID, int targetItemID)
        {
            string failureReason = await this._service.MoveBefore(ID, targetItemID);

            if (string.IsNullOrWhiteSpace(failureReason))
                return Ok();

            if (failureReason == "Record not found")
                return NotFound();

            return BadRequest(failureReason);
        }

        /// <summary>
        /// Moves a line item after another item with in an order
        /// </summary>
        /// <returns></returns>
        [Route("{ID}/action/moveafter/{targetItemID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "One or both of the item IDs is not found.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "The target item is not in the same order as the source item.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> MoveAfter(int ID, int targetItemID)
        {
            string failureReason = await this._service.MoveAfter(ID, targetItemID);

            if (string.IsNullOrWhiteSpace(failureReason))
                return Ok();

            if (failureReason == "Record not found")
                return NotFound();

            return BadRequest(failureReason);
        }
        #endregion Move

        #region LinkEmployee

        /// <summary>
        /// Adds an Employee Role to an OrderItem
        /// </summary>
        /// <param name="id">OrderItem's ID</param>
        /// <param name="employeeRoleID">Employee Role's ID</param>
        /// <param name="employeeID">Employee's ID</param>
        /// <returns></returns>
        [Route("{id}/Action/LinkEmployee/{employeeRoleID}/{employeeID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the OrderItem fails to add an Employee Role.")]
        public async Task<IActionResult> AddEmployeeRole(int id, short employeeRoleID, short employeeID)
        {
            var orderItem = await this._service.GetAsync(id);
            if (orderItem == null)
                return new NotFoundObjectResult($"OrderItem {id} not found.");

            EntityActionChangeResponse resp = await this._orderEmployeeRoleService.Value.AddOrderItemEmployeeRole(orderItem, employeeRoleID, employeeID, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Removes an Employee Role from an OrderItem
        /// </summary>
        /// <param name="ID">OrderItem's ID</param>
        /// <param name="employeeRoleID">Employee Role's ID</param>
        /// <param name="employeeID">Employee's ID</param>
        /// <returns></returns>
        [Route("{ID}/Action/LinkEmployee/{employeeRoleID}/{employeeID}")]
        [HttpDelete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the OrderItem fails to remove an Employee Role.")]
        public async Task<IActionResult> RemoveEmployeeRole(int ID, short employeeRoleID, short employeeID)
        {
            var orderItem = await this._service.GetAsync(ID);
            if (orderItem == null)
                return new NotFoundObjectResult($"OrderItem {ID} not found.");

            EntityActionChangeResponse resp = await this._orderEmployeeRoleService.Value.RemoveOrderItemEmployeeRole(orderItem, employeeRoleID, employeeID);
            return resp.ToResult();
        }

        #endregion

        /// <summary>
        /// Returns true | false based on whether the OrderItem can be deleted.
        /// </summary>
        /// <param name="ID">OrderItem ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(int ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Changes the OrderItem's Status by the Status Name or ID
        /// </summary>
        /// <param name="id">OrderItem's ID</param>
        /// <param name="status">Status to change to by Name or ID</param>
        /// <returns></returns>
        [Route("{id}/Action/ChangeItemStatus/{status}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> ChangeOrderItemStatus(int id, string status)
        {
            var orderItem = await this._service.GetAsync(id);
            if (orderItem == null)
                return new NotFoundObjectResult($"OrderItem {id} not found.");

            EntityActionChangeResponse resp = await this._service.ChangeStatus(id, status);
            if (resp.Success)
                await _taskQueuer.CheckOrderStatus(User.BID().Value, orderItem.OrderID, (int)orderItem.OrderStatusID);

            return resp.ToResult();
        }

        /// <summary>
        /// Changes the OrderItem's Status to the Default Item Status
        /// </summary>
        /// <param name="id">OrderItem's ID</param>
        /// <returns></returns>
        [Route("{id}/Action/ChangeItemStatus")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> ChangeOrderItemStatus(int id)
        {
            var orderItem = await this._service.GetAsync(id);
            if (orderItem == null)
                return new NotFoundObjectResult($"OrderItem {id} not found.");

            EntityActionChangeResponse resp = await this._service.ChangeStatus(id, null);
            if (resp.Success)
                await _taskQueuer.CheckOrderStatus(User.BID().Value, orderItem.OrderID, (int)orderItem.OrderStatusID);

            return resp.ToResult();
        }


        /// <summary>
        /// Changes the OrderItem's from one value to another
        /// </summary>
        /// <param name="id">OrderItem's ID</param>
        /// <param name="boardMove"></param>
        /// <returns></returns>
        [Route("{id}/action/boardmove")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> BoardMove(int id, [FromBody] BoardMoveRequest boardMove)
        {
            var orderItem = await this._service.GetAsync(id);
            if (orderItem == null)
                return new NotFoundObjectResult($"OrderItem {id} not found.");

            EntityActionChangeResponse resp = await this._service.BoardMove(id, orderItem, boardMove, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        #region LinkContact

        /// <summary>
        /// Adds a Contact Role to an OrderItem
        /// </summary>
        /// <param name="id">OrderItem's ID</param>
        /// <param name="contactRoleType">Contact Role Type</param>
        /// <param name="contactID">Contact's ID</param>
        /// <returns></returns>
        [Route("{id}/Action/LinkContact/{contactRoleType}/{contactID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the OrderItem fails to add a Contact Role.")]
        public async Task<IActionResult> AddContactRole(int id, OrderContactRoleType contactRoleType, int contactID)
        {
            var orderItem = await this._service.GetAsync(id);
            if (orderItem == null)
                return new NotFoundObjectResult($"OrderItem {id} not found.");

            EntityActionChangeResponse resp = await this._orderContactRoleService.Value.AddOrderItemContactRole(orderItem, contactRoleType, contactID, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Removes a Contact Role to an OrderItem
        /// </summary>
        /// <param name="id">OrderItem's ID</param>
        /// <param name="contactRoleType">Contact Role Type</param>
        /// <param name="contactID">Contact's ID</param>
        /// <returns></returns>
        [Route("{id}/Action/LinkContact/{contactRoleType}/{contactID}")]
        [HttpDelete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the OrderItem fails to remove a Contact Role.")]
        public async Task<IActionResult> RemoveContactRole(int id, OrderContactRoleType contactRoleType, int contactID)
        {
            var orderItem = await this._service.GetAsync(id);
            if (orderItem == null)
                return new NotFoundObjectResult($"OrderItem {id} not found.");

            EntityActionChangeResponse resp = await this._orderContactRoleService.Value.RemoveOrderItemContactRole(orderItem, contactRoleType, contactID);
            return resp.ToResult();
        }

        #endregion

        #region KeyDates

        /// <summary>
        /// Sets the Key Date to the current Date time for the Order Item
        /// </summary>
        /// <param name="id">Order Item's ID</param>
        /// <param name="keyDateType">Order Key Date Type enum</param>
        /// <param name="isFirmDate">(Optional) Can be specified to indicate there is no flexibility in the date. Defaults to false.</param>
        /// <returns></returns>
        [Route("{id}/Action/Date/{keyDateType}")]
        [HttpPut]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> SetOrderItemKeyDate(int id, OrderKeyDateType keyDateType, [FromQuery] bool isFirmDate = false)
        {
            var orderItem = await this._service.GetAsync(id);
            if (orderItem == null)
                return new NotFoundObjectResult($"OrderItem {id} not found.");

            EntityActionChangeResponse resp = await this._service.SetKeyDate(orderItem.OrderID, id, keyDateType, isFirmDate);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets the Key Date to the specified Date/DateTime for the Order Item
        /// </summary>
        /// <param name="id">Order Item's ID</param>
        /// <param name="keyDateType">Order Key Date Type enum</param>
        /// <param name="keyDate">Specified Date/DateTime</param>
        /// <param name="isFirmDate">(Optional) Can be specified to indicate there is no flexibility in the date. Defaults to false.</param>
        /// <returns></returns>
        [Route("{id}/Action/Date/{keyDateType}/{keyDate}")]
        [HttpPut]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> SetOrderItemKeyDate(int id, OrderKeyDateType keyDateType, DateTime keyDate, [FromQuery] bool isFirmDate = false)
        {
            var orderItem = await this._service.GetAsync(id);
            if (orderItem == null)
                return new NotFoundObjectResult($"OrderItem {id} not found.");

            EntityActionChangeResponse resp = await this._service.SetKeyDate(orderItem.OrderID, id, keyDateType, isFirmDate, keyDate);
            return resp.ToResult();
        }

        /// <summary>
        /// Removes the Key Date from the Order Item
        /// </summary>
        /// <param name="id">Order Item's ID</param>
        /// <param name="keyDateType">Order Key Date Type enum</param>
        /// <returns></returns>
        [Route("{id}/Action/Date/{keyDateType}")]
        [HttpDelete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> RemoveOrderItemKeyDate(int id, OrderKeyDateType keyDateType)
        {
            var orderItem = await this._service.GetAsync(id);
            if (orderItem == null)
                return new NotFoundObjectResult($"OrderItem {id} not found.");

            EntityActionChangeResponse resp = await this._service.RemoveKeyDate(orderItem.OrderID, id, keyDateType);
            return resp.ToResult();
        }

        #endregion
    }
}
