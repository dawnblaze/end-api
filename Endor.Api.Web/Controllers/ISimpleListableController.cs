﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// A controller that can return simple list items
    /// </summary>
    /// <typeparam name="SLI"></typeparam>
    /// <typeparam name="I"></typeparam>
    public interface ISimpleListableController<SLI, I> where SLI : SimpleListItem<I> where I : struct, IConvertible
    {
        /// <summary>
        /// A public endpoint for getting simple lists
        /// </summary>
        /// <returns></returns>
        Task<SLI[]> SimpleList();
    }

    /// <summary>
    /// A controller that has its simple list backed by a ISimpleListableViewService
    /// </summary>
    /// <typeparam name="SLI"></typeparam>
    /// <typeparam name="I"></typeparam>
    public interface ISimpleListableViewController<SLI, I> : ISimpleListableController<SLI, I>
        where SLI : SimpleListItem<I> where I : struct, IConvertible
    {
        /// <summary>
        /// A getter for a service that exposes a simple list DbSet
        /// </summary>
        ISimpleListableViewService<SLI, I> ListableService { get; }
    }

    /// <summary>
    /// Extension for automagically getting simple lists from ISimpleListableViewControllers
    /// </summary>
    public static class SimpleListableExtensions
    {
        /// <summary>
        /// Returns an array of simple list objects, optionally filtered
        /// </summary>
        /// <typeparam name="SLI"></typeparam>
        /// <typeparam name="I"></typeparam>
        /// <param name="controller"></param>
        /// <param name="bid"></param>
        /// <param name="filters">an optional filter object</param>
        /// <param name="take">an option to limit result</param>
        /// <returns></returns>
        public static async Task<SLI[]> GetSimpleList<SLI, I>(this ISimpleListableViewController<SLI, I> controller, short bid, IQueryFilters<SLI> filters = null, int take = 0) where SLI : SimpleListItem<I> where I : struct, IConvertible
        {
            DbSet<SLI> simpleListSet = controller.ListableService.SimpleListSet;
            IQueryable<SLI> queryable = simpleListSet.Where(x => x.BID == bid);

            if (filters != null)
                queryable = queryable.WhereAll(filters.WherePredicates());

            if (take > 0)
                queryable = queryable.Take(take);

            return await queryable.ToArrayAsync();
        }
    }
}
