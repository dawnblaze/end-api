﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Request;
using Endor.Api.Web.Classes.Responses;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Payment controller to handle the payments being submitted as well as the listing of payments recorded
    /// </summary>
    [Route("api/payment")]
    [Authorize]
    public class PaymentMasterController : CRUDController<PaymentMaster, PaymentMasterService, int>
    {        
        /// <summary>
        /// Lazy loaded service
        /// </summary>
        protected readonly Lazy<PaymentApplicationService> _lazyPaymentApplicationService;
        /// <summary>
        /// Model Service
        /// </summary>
        protected PaymentApplicationService _PaymentApplicationService { get { return _lazyPaymentApplicationService.Value; } }


        /// <summary>
        /// Constructs a payment Controller
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="rtmClient"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public PaymentMasterController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Get List of Payments with filters
        /// </summary>
        /// <param name="filters">Payment filters object</param>
        /// <param name="includes">Payment includes object</param>
        /// <param name="count">Count of results (defaults to 500)</param>
        /// <returns></returns>
        [HttpGet("master")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentApplication[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetMasterList([FromQuery] PaymentFilters filters, [FromQuery] PaymentIncludes includes, [FromQuery] int count = 500)
        {            
            return new OkObjectResult(await this._service.FilteredGetAsync(filters, includes, count));
        }

        /// <summary>
        /// Gets a specific payment with filters and included objects
        /// </summary>
        /// <param name="id">ID of the payment master</param>
        /// <param name="includes">Payment includes object</param>
        /// <returns></returns>
        [HttpGet("master/{id}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentApplication[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the specified master payment id is not found")]
        public async Task<IActionResult> GetMasterById(int id, [FromQuery] PaymentIncludes includes)
        {
            
            return new OkObjectResult(await this._service.GetAsync(id, includes));
        }
    }
}