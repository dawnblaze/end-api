﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.DNSManagement;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Domain Controller
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public partial class DomainController : CRUDController<DomainData, DomainService, short>
    {
        private EndorOptions _options;

        private readonly IDNSManager _dnsManager;

        /// <summary>
        /// DomainController Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="options">Options</param>
        /// <param name="dnsManager"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DomainController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, EndorOptions options, IDNSManager dnsManager, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
            _options = options;
            this._dnsManager = dnsManager;
        }

        #region Overridden CRUD Methods
        /// <summary>
        /// Returns all of the Domains
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsolete")]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Get a list of Domains based on supplied criteria
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <returns>Returns an array of Domains</returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DomainData[]), "An array of Domains")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid parameters were supplied or there was a different issue with the request")]
        public async Task<IActionResult> ReadWithFilters([FromQuery]DomainFilter filters)
        {
            this._service.DNSManager = _dnsManager;
            List<DomainData> result = await _service.GetWithFiltersAsync(filters);
            if (result == null)
            {
                return BadRequest();
            }

            return Ok(result);
        }

        /// <summary>
        /// Returns a single Domain by ID
        /// </summary>
        /// <param name="ID">Domain ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DomainData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Domain does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            this._service.DNSManager = _dnsManager;
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single Domain by ID
        /// </summary>
        /// <param name="ID">Domain ID</param>
        /// <param name="update">Updated Domain data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DomainData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Domain does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Domain does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] DomainData update)
        {
            this._service.DNSManager = _dnsManager;
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new Domain
        /// </summary>
        /// <param name="newModel">New Domain data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DomainData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Domain creation fails")]
        public override async Task<IActionResult> Create([FromBody] DomainData newModel, [FromQuery] Guid? tempID = null)
        {
            this._service.DNSManager = _dnsManager;
            BooleanResponse result = await this._service.InstallCustomDomain(newModel);

            DateTime installationTime = DateTime.UtcNow;
            newModel.DNSLastAttemptDT = installationTime;
            newModel.DNSLastAttemptResult = result.Message;

            if(result.Success)
            {
                newModel.DNSVerifiedDT = installationTime;
            }

            //Continue even if the custom domain installation fails.
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing Domain to a new Domain
        /// </summary>
        /// <param name="ID">ID of the Domain to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DomainData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Domain does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Domain does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            this._service.DNSManager = _dnsManager;
            return await Task.FromResult(NotFound("Unable to clone domains"));
        }

        /// <summary>
        /// Deletes a Domain by ID
        /// </summary>
        /// <param name="ID">ID of the Domain to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Domain does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Domain does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            this._service.DNSManager = _dnsManager;
            return await base.Delete(ID);
        }
        #endregion Overridden CRUD Methods

        /// <summary>
        /// Returns DNS Host Name Target from config
        /// </summary>
        /// <returns></returns>
        [HttpGet("dnshostnametarget")]
        public async Task<IActionResult> ReadDnsHostNmeTarget()
        {
            return Ok(await this._dnsManager.DNSHostNameTargetAsync());
        }

    }
}
