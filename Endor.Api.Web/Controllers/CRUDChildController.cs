﻿using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Base child controller public class
    /// </summary>
    /// <typeparam name="M"></typeparam>
    /// <typeparam name="S"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="PI"></typeparam>
    [Authorize]
    [Route("api/obsolete/{parentID}/child")]
    public class CRUDChildController<M, S, I,  PI> : CRUDChildInternalController<M, S, I, PI>
        where M : class, IAtom<I>
        where S : AtomCRUDChildService<M, I, PI>
        where I : struct, IConvertible
        where PI : struct, IConvertible
    {
        /// <summary>
        /// Child Controller constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CRUDChildController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Base read by parentID
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        [HttpGet()]
        public virtual async Task<IActionResult> Read([FromRoute]PI parentID)
        {
            return await DoRead(parentID);
        }

        /// <summary>
        /// Base read specific from parent id 
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        public virtual async Task<IActionResult> ReadByParentIdAndId([FromRoute]PI parentID, [FromRoute]I ID)
        {
            return await DoRead(parentID, ID);
        }

        /// <summary>
        /// Updates a child
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="ID"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        public virtual async Task<IActionResult> Update([FromRoute]PI parentID, [FromRoute]I ID, [FromBody] M update)
        {
            return await DoUpdate(parentID, ID, update);
        }

        /// <summary>
        /// Creates a new child
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="newModel"></param>
        /// <param name="tempID"></param>
        /// <returns></returns>
        [HttpPost()]
        public virtual async Task<IActionResult> Create([FromRoute]PI parentID, [FromBody] M newModel, [FromQuery] Guid? tempID = null)
        {
            return await DoCreate(parentID, newModel, tempID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        public virtual async Task<IActionResult> Clone([FromRoute]PI parentID, [FromRoute]I ID)
        {
            return await DoClone(ID);
        }

        /// <summary>
        /// Deletes a child
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        public virtual async Task<IActionResult> Delete(PI parentID, I ID)
        {
            return await DoDelete(parentID, ID);
        }


        internal async Task<IActionResult> ReadWith(PI parentID, IExpandIncludes includes)
        {
            return await DoReadWith(parentID, includes);
        }

        internal async Task<IActionResult> ReadWith(PI parentID, I ID, IExpandIncludes includes)
        {
            return await DoReadWith(parentID, ID, includes);
        }

        internal async Task<IActionResult> FilteredReadWith(PI parentID, Expression<Func<M, bool>> filterPredicate, IExpandIncludes includes)
        {
            return await DoFilteredReadWith(parentID, filterPredicate, includes);
        }

    }
}
