﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// This is a CRM Industry Controller
    /// </summary>
    [Route("api/industry")]
    public class IndustryController : CRUDController<CrmIndustry, IndustryService, short>, ISimpleListableViewController<SimpleCrmIndustry, short>
    {
        /// <summary>
        /// Listable service for Companies
        /// </summary>
        public ISimpleListableViewService<SimpleCrmIndustry, short> ListableService => this._service;

        /// <summary>
        /// Industry API Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public IndustryController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overridden CRUD Methods


        /// <summary>
        /// Returns all of the Industry
        /// </summary>
        /// <param name="Name">Query parameters to filter Name property</param>
        /// <param name="IsActive">Query parameters to filter IsActive property</param>
        /// <param name="IsTopLevelOnly">Query parameters to filter IsTopLevel property</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CrmIndustry[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public Task<IActionResult> GetByFilter([FromQuery] string Name = "", [FromQuery] bool? IsActive = null, [FromQuery] bool? IsTopLevelOnly = null)
        {
            Name = Name.ToLower();
            return base.FilteredReadWith(
             ti => (ti.Name.Contains(Name ?? ti.Name))
                   && (ti.IsActive == (bool)(IsActive ?? ti.IsActive))
                   && (ti.IsTopLevel == (bool)(IsTopLevelOnly ?? ti.IsTopLevel)), null);
        }

        /// <summary>
        /// Old read method
        /// </summary>
        /// <returns></returns>
        [HttpGet("Obsolete")]
        [Obsolete]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single Industry by ID
        /// </summary>
        /// <param name="ID">Industry ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CrmIndustry))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Industry does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single Industry by ID
        /// </summary>
        /// <param name="ID">Industry ID</param>
        /// <param name="update">Updated Industry data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CrmIndustry))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Industry does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Industry does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] CrmIndustry update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new Industry
        /// </summary>
        /// <param name="newModel">New Industry data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CrmIndustry))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Industry creation fails")]
        public override async Task<IActionResult> Create([FromBody] CrmIndustry newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing Industry to a new Industry
        /// </summary>
        /// <param name="ID">ID of the Industry to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CrmIndustry))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Industry does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Industry does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes a Industry by ID
        /// </summary>
        /// <param name="ID">ID of the Industry to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Industry does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Industry does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            BooleanResponse resp = await this._service.CanDelete((short)ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                return await base.Delete(ID);
            }
            else
            {
                return new BadRequestObjectResult(resp);
            }
        }

        #endregion

        /// <summary>
        /// Sets a Industry to to Active
        /// </summary>
        /// <param name="ID">Industry ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the Industry does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the Industry does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Industry to to Inactive
        /// </summary>
        /// <param name="ID">Industry ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the Industry does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the Industry does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Industry's Parent
        /// </summary>
        /// <param name="ID">Industry ID</param>
        /// <param name="parentId">Industry Parent ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setparent/{parentId}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Industry does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Industry does not exist or fails to set the parent.")]
        public async Task<IActionResult> SetParent(short ID, short? parentId)
        {
            EntityActionChangeResponse resp = await this._service.SetParent(ID, parentId);

            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the industry can be deleted.
        /// </summary>
        /// <param name="ID">Industry ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Gets a Simple list of Industries
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("simpleobsolete")]        
        public Task<SimpleCrmIndustry[]> SimpleList()
        {
            return null;
        }

        /// <summary>
        /// Returns a filtered list of Simple Industries based on Istoplevel or Isactive
        /// </summary>
        /// <param name="IsTopLevel"></param>
        /// <param name="IsActive"></param>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleCrmIndustry[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleCrmIndustry[]> GetSimpleFilterList([FromQuery] bool? IsTopLevel = null, bool? IsActive = null)
        {
            return await this._service.SimpleList(IsTopLevel, IsActive);
        }

        /// <summary>
        /// Pagination List
        /// </summary>
        /// <param name="Name">Query parameters to filter Name property</param>
        /// <param name="IsActive">Query parameters to filter IsActive property</param>
        /// <param name="IsTopLevelOnly">Query parameters to filter IsTopLevel property</param>
        /// <param name="skip">Number of items to skip</param>
        /// <param name="take">Number of items to take</param>
        /// <returns></returns>
        [HttpGet("paged")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CrmIndustry[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<PagedList<CrmIndustry>> GetPagedList([FromQuery] string Name = "", [FromQuery] bool? IsActive = null, [FromQuery] bool? IsTopLevelOnly = null,
                                                        [FromQuery]int? skip = null,
                                                        [FromQuery]int? take = null)
        {

            CrmIndustryFilter filters = new CrmIndustryFilter();
            filters.Name = Name;
            filters.IsActive = IsActive;
            filters.IsTopLevel = IsTopLevelOnly;

            return await this._service.GetPagedListAsync(filters, skip, take, null);
        }
    }
}
