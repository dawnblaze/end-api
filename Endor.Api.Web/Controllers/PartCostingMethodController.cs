﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Controller has a list of Part Costing Methods
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    public class PartCostingMethodController : GenericController<EnumMaterialCostingMethod, EnumMaterialCostingMethodService, byte>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public PartCostingMethodController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) 
            : base(context, logger, rtmClient, migrationHelper)
        {
        }

        // GET: api/<controller>
        /// <summary>
        /// Returns a list of PartCostingMethods
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await this._service.Get());
        }
    }
}