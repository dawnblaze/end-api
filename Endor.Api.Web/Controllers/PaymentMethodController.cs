﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using Endor.Security;
using Endor.Api.Web.Includes;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Controller for payment method entity
    /// </summary>
    [Route("api/paymentmethod")]
    public class PaymentMethodController : CRUDController<PaymentMethod, PaymentMethodService, int>, ISimpleListableViewController<SimplePaymentMethod, int>
    {
        /// <summary>
        /// ctor for payment method controller
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public PaymentMethodController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Listable service for PaymentMethods
        /// </summary>
        public ISimpleListableViewService<SimplePaymentMethod, int> ListableService => this._service;

        #region Overridden CRUD Methods
        /// <summary>
        /// Returns all PaymentMethods and any included child objects specified in query parameters
        /// </summary>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <param name="IsActive">Query parameters to filter IsActive property</param>
        /// <param name="Name">Query parameters to filter Name property</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentMethod[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetWithIncludes([FromQuery] PaymentMethodIncludes includes, [FromQuery] bool? IsActive = null, [FromQuery] string Name = "")
        {
            Name = Name.ToLower();
            return await base.FilteredReadWith(
                (tg => (tg.IsActive == IsActive || IsActive == null) &&
                        (String.IsNullOrEmpty(Name) || tg.Name.ToLower().Contains(Name)))
                , includes
            );
        }

        /// <summary>
        /// Obsolete function, only here for interface, superseded by <see cref="GetWithIncludes(PaymentMethodIncludes,  bool?, String)"/>
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsoletegetall")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns a single PaymentMethod by ID
        /// </summary>
        /// <param name="ID">PaymentMethod ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentMethod))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If PaymentMethod does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single PaymentMethod by ID
        /// </summary>
        /// <param name="ID">PaymentMethod ID</param>
        /// <param name="update">Updated PaymentMethod data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentMethod))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If PaymentMethod does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the PaymentMethods does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] PaymentMethod update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new PaymentMethod
        /// </summary>
        /// <param name="newModel">Updated PaymentMethod data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentMethod))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the PaymentMethods creation fails")]
        public override async Task<IActionResult> Create([FromBody] PaymentMethod newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Deletes a PaymentMethod by ID
        /// </summary>
        /// <param name="ID">ID of the PaymentMethod to delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If PaymentMethod does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the PaymentMethod does not exist or the deletion fails")]
        public override async Task<IActionResult> Delete(int ID)
        {
            BooleanResponse resp = await this._service.CanDelete(ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                return await base.Delete(ID);
            }
            else if (resp.ErrorMessage.Contains("Does not Exist"))
            {
                return Ok();
            }

            return BadRequest(resp);
        }
        #endregion Overridden CRUD Methods

        /// <summary>
        /// Sets a PaymentMethod to Active
        /// </summary>
        /// <param name="ID">PaymentMethod ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the PaymentMethod does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the PaymentMethod does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets a PaymentMethod to Inactive
        /// </summary>
        /// <param name="ID">PaymentMethod ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the PaymentMethod does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the PaymentMethod does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }


        /// <summary>
        /// Returns true | false based on whether the PaymentMethod can be deleted.
        /// </summary>
        /// <param name="ID">PaymentMethod ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Returns a simple list of payment methods
        /// can by filtered by IsActive
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        [HttpGet("simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimplePaymentMethod[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimplePaymentMethod[]> FilteredSimpleList([FromQuery] SimpleListItemIsActiveFilter<SimplePaymentMethod, int> filters)
        {
            return await this.GetSimpleList(User.BID().Value, filters);
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsoletesimplelist")]
        [Obsolete]
        public Task<SimplePaymentMethod[]> SimpleList()
        {
            return this.FilteredSimpleList(null);
        }
    }
}
