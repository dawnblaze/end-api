﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Data.Common;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

class EmployeeContactUserLinkIDs
{
    public short UserLinkID { get; set; }
    public short? EmployeeID { get; set; }
    public int? ContactID { get; set; }
}

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Security Controller
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("[controller]")]
    [Route("Api/[controller]")]
    public class SecurityController : Controller
    {
        private readonly SecurityService _service;
        private readonly DbContextOptions<ApiContext> _options;
        private readonly ITenantDataCache _tenantCache;
        private readonly RemoteLogger _logger;
        private readonly EndorOptions _secretOptions;
        private readonly ITaskQueuer _queuer;
        private readonly IRTMPushClient _rtmPush;
        private readonly IMigrationHelper _migrationHelper;

        /// <summary>
        /// Security Controller Constructor
        /// </summary>
        public SecurityController(ApiContext context, DbContextOptions<ApiContext> options, EndorOptions secretOptions, ITenantDataCache tenantCache, RemoteLogger logger, ITaskQueuer queuer, IRTMPushClient rtmPush, IMigrationHelper migrationHelper)
        {
            _options = options;
            _tenantCache = tenantCache;
            _logger = logger;
            _secretOptions = secretOptions;
            _queuer = queuer;
            _rtmPush = rtmPush;
            _migrationHelper = migrationHelper;
            _service = new SecurityService(context);
        }

        /// <summary>
        /// Retrieve Security Result for UserId
        /// </summary>
        [HttpGet]
        [HttpOptions]
        public async Task<IActionResult> Get([FromQuery]short bid, [FromQuery]int? userID = null, [FromQuery]string tempUserID = null)
        {
            bool tempUserIDHasValue = !string.IsNullOrWhiteSpace(tempUserID);
            if (!tempUserIDHasValue && !userID.HasValue)
            {
                return BadRequest("Either tempUserID or userID parameter required");
            }

            ApiContext _ctx = await GetCtx(bid);
            if (_ctx == null)
                return StatusCode(500, "Database initialization error");

            EmployeeContactUserLinkIDs ids = await GetUserIDs(_ctx, bid, userID, tempUserID, tempUserIDHasValue);
            if (ids == null)
            {
                return BadRequest("Unable to find user given parameters");
            }

            var lookupResult = await GetUserSecurityRights(bid, _ctx, ids);
            if (lookupResult.Success)
            {
                return Ok(lookupResult.Data);
            }
            else
            {
                return BadRequest(lookupResult.ErrorMessage);
            }
        }

        /// <summary>
        /// Make a temporary user permanent - this should only be called during new user invite process
        /// </summary>
        /// <param name="BID"></param>
        /// <param name="GUID">User Invite GUID</param>
        /// <param name="userID">new User ID</param>
        /// <returns></returns>
        [HttpPost("{BID}/MakePermanent/{GUID}/To/{userID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If user invite is not found.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If user is already permanent.")]
        public async Task<IActionResult> MakePermanent(short BID, Guid GUID, int userID)
        {
            if (!SecretMatches(HttpContext.Request.Headers["Authorization"]))
                return Unauthorized();

            ApiContext _ctx = await GetCtx(BID);
            EmployeeService _service = new EmployeeService(_ctx, BID, _queuer, _tenantCache, _logger, _rtmPush, _migrationHelper);
            EntityActionChangeResponse<int> resp = await _service.MakePermanent(GUID, userID);

            if (!resp.HasError)
                return resp.ToResult();

            if (resp.ErrorMessage.Contains("unauthorized", StringComparison.CurrentCultureIgnoreCase))
                return Unauthorized();

            if (resp.ErrorMessage.Contains("not found", StringComparison.CurrentCultureIgnoreCase))
                return NotFound(resp.ErrorMessage);

            if (resp.ErrorMessage.Contains("already exists", StringComparison.CurrentCultureIgnoreCase))
                return BadRequest(resp.ErrorMessage);

            return StatusCode(500, resp.ErrorMessage);
        }

        private bool SecretMatches(string auth)
        {
            if (auth == null || !auth.StartsWith("Bearer "))
            {
                return false;
            }
            else
            {
                string value = auth.Substring("Bearer ".Length);
                try
                {
                    return Encoding.UTF8.GetString(Convert.FromBase64String(value)) == _secretOptions.TenantSecret;
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Builds the definition of the security rights for TypeScript and returns the security right enum as a TypeScript enum.
        /// </summary>
        /// <returns></returns>
        [HttpGet("RightDefinitions")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(string))]
        public IActionResult RightDefinitions()
        {
            var typeScript = "enum SecurityRights\n{\n";

            foreach (var enumItem in Enum.GetValues(typeof(Security.SecurityRight)))
                typeScript += $"\t{enumItem} = {(int)enumItem},\n";

            typeScript = typeScript[0..^2];
            typeScript += "\n};";

            return Ok(typeScript);
        }

        /// <summary>
        /// Returns a collection of the calling User’s rights. This is returned as a JSON array of INT by default, or as a Base64Encoded String when the Base64 Format is requested.
        /// </summary>
        /// <param name="filter">Filter to specify the format JSON = 0 or Base64 = 1</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("Rights")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(string))]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SecurityRightsResponses.JSONRights))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid parameters were suplied")]
        public async Task<IActionResult> Rights([FromQuery] SecurityRightFilter filter)
        {
            var result = await this._service.Rights(filter, User.BID().Value, User.UserLinkID().Value);
            
            return result.Success ? 
                (IActionResult)Ok(result.Value) : 
                BadRequest(result.Message);
        }

        /// <summary>
        /// Returns a list of rights the requesting user has with expanded source information that explains how , in the form of an array of objects.  
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("Rights/WithSource")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SecurityRightsProceduresResults.RightsForSource[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid parameters were suplied")]
        public async Task<IActionResult> RightsWithSource()
        {
            var result = await this._service.RightsWithSource(User.BID().Value, User.UserLinkID().Value);

            return result.Success ?
                (IActionResult)Ok(result.Value) :
                BadRequest(result.Message);
        }

        /// <summary>
        /// Get the user security rights
        /// since there are multiple error states, return a GenericResponse
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="_ctx"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        private async Task<GenericResponse<SecurityResult>> GetUserSecurityRights(short bid, ApiContext _ctx, EmployeeContactUserLinkIDs ids)
        {
            SecurityResult results = new SecurityResult()
            {
                employeeID = ids.EmployeeID,
                contactID = ids.ContactID,
                userlinkID = ids.UserLinkID
            };
            using (DbConnection conn = _ctx.Database.GetDbConnection())
            {
                conn.Open();

                using (var cmdAcessType = conn.CreateCommand())
                {
                    try
                    {
                        DbParameter bidParam = cmdAcessType.CreateParameter();
                        bidParam.DbType = System.Data.DbType.Int16;
                        bidParam.ParameterName = "BID";
                        bidParam.Value = bid;

                        DbParameter userIDParam = cmdAcessType.CreateParameter();
                        userIDParam.DbType = System.Data.DbType.Int32;
                        userIDParam.ParameterName = "userLinkID";
                        userIDParam.Value = ids.UserLinkID;

                        cmdAcessType.Parameters.AddRange(new DbParameter[2] { bidParam, userIDParam });

                        cmdAcessType.CommandText = $"SELECT UserAccessType FROM [User.Link] WHERE BID = @BID AND ID = @userLinkID";
                        using (var reader = await cmdAcessType.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                if (await reader.IsDBNullAsync(0))
                                    return new GenericResponse<SecurityResult>()
                                    {
                                        Success = false,
                                        ErrorMessage = "No user access type found with given bid and userlinkid"
                                    };

                                UserAccessType accessType = (UserAccessType)reader.GetByte(0);
                                results.accessType = accessType.ToString();
                            }

                            else
                                return new GenericResponse<SecurityResult>()
                                {
                                    Success = false,
                                    ErrorMessage = "No user link record found with given bid and userlinkid"
                                };
                        }
                    }
                    catch (Exception ex)
                    {
                        await _logger.Error(bid, "Error reading user's access type ", ex);
                        return new GenericResponse<SecurityResult>()
                        {
                            Success = false,
                            ErrorMessage = "Error reading user's access type"
                        };
                    }
                }

                using (var command = conn.CreateCommand())
                {
                    try
                    {
                        DbParameter bidParam = command.CreateParameter();
                        bidParam.DbType = System.Data.DbType.Int16;
                        bidParam.ParameterName = "BID";
                        bidParam.Value = bid;

                        DbParameter userIDParam = command.CreateParameter();
                        userIDParam.DbType = System.Data.DbType.Int32;
                        userIDParam.ParameterName = "userID";
                        userIDParam.Value = ids.UserLinkID;

                        command.Parameters.AddRange(new DbParameter[2] { bidParam, userIDParam });

                        command.CommandText = $"SELECT dbo.[User.Security.Rights.String] (@BID, @userID)";
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                if (await reader.IsDBNullAsync(0))
                                    return new GenericResponse<SecurityResult>()
                                    {
                                        Success = false,
                                        ErrorMessage = "No user rights string found with given bid and userid"
                                    };

                                byte[] rightsBitArray = reader.GetValue(0) as byte[];

                                if (rightsBitArray == null)
                                    return new GenericResponse<SecurityResult>()
                                    {
                                        Success = false,
                                        ErrorMessage = "User rights string could not be converted to byte array"
                                    };
                                else {
                                    var reversedArray = Reverse(rightsBitArray);
                                    results.rights = Convert.ToBase64String(reversedArray);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        await _logger.Error(bid, "Error reading user.security.rights ", ex);
                        return new GenericResponse<SecurityResult>()
                        {
                            Success = false,
                            ErrorMessage = "Error reading user.security.rights"
                        };
                    }
                }
            }

            return new GenericResponse<SecurityResult>()
            {
                Data = results,
                Success = true
            };
        }

        private byte[] Reverse(byte[] byteArray)
        {
            BitArray myBitArr = new BitArray(byteArray.Reverse().ToArray());
            var bytes = new byte[(myBitArr.Length - 1) / 8 + 1];
            myBitArr.CopyTo(bytes, 0);
            /*for (int i = 0; i < bytes.Length; i++)
            {
                bytes[i] = reverseBitsByte(bytes[i]);
            }*/
            return bytes;
        }

        private byte reverseBitsByte(byte x)
        {
            byte intSize = 8;
            byte y = 0;
            for (int position = intSize - 1; position > 0; position--)
            {
                var z = (x & 1);
                y += (byte)(z << position);
                x >>= 1;
            }
            return y;
        }
        /// <summary>
        /// Get a migrated context
        /// returns null if there is an error
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        private async Task<ApiContext> GetCtx(short bid)
        {
            try
            {
                ApiContext _ctx = new ApiContext(_options, _tenantCache, bid);
                _migrationHelper.MigrateDb(_ctx);
                return _ctx;
            }
            catch (Exception e)
            {
                await _logger.Error(bid, "Error getting ctx and migrating it", e);
                return null;
            }
        }

        /// <summary>
        /// read IDs from the User.Link table
        /// returns null if there is an error or if no users match the parameters
        /// </summary>
        /// <param name="_ctx"></param>
        /// <param name="bid"></param>
        /// <param name="userID"></param>
        /// <param name="tempUserID"></param>
        /// <param name="tempUserIDHasValue"></param>
        /// <returns></returns>
        private async Task<EmployeeContactUserLinkIDs> GetUserIDs(ApiContext _ctx, short bid, int? userID, string tempUserID, bool tempUserIDHasValue)
        {
            try
            {
                IQueryable<UserLink> linkQuery = null;
                if (tempUserIDHasValue)
                {
                    linkQuery = _ctx.UserLink.Where(x => x.BID == bid && x.TempUserID == new Guid(tempUserID));
                }
                else
                {
                    linkQuery = _ctx.UserLink.Where(x => x.BID == bid && x.AuthUserID == userID);
                }
                return await linkQuery.Select(y => new EmployeeContactUserLinkIDs()
                {
                    UserLinkID = y.ID,
                    EmployeeID = y.EmployeeID,
                    ContactID = y.ContactID,
                }).FirstOrDefaultAsync();
            }
            catch (Exception e)
            {
                await _logger.Error(bid, "Error getting userIDs for security rights", e);

                return null;
            }
        }
    }
}
