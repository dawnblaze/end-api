﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Includes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// OrderItemSubStatus API Controller
    /// </summary>
    [Route("api/orderitemsubstatus")]
    public class OrderItemSubStatusController : CRUDController<OrderItemSubStatus, OrderItemSubStatusService, short>, ISimpleListableController<SimpleOrderItemSubStatus, short>
    {
        /// <summary>
        /// OrderItemSubStatus API Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderItemSubStatusController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns all OrderItemSubStatuses and any included child objects specified in query parameters
        /// </summary>
        /// <param name="includes">Query parameters to include OrderItemStatuses</param>
        /// <param name="filters">Query parameters to filter on OrderItemStatusID property</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemSubStatus[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetWithIncludes([FromQuery] OrderItemSubStatusIncludes includes, [FromQuery]OrderItemSubStatusFilter filters)
        {
            return await this.FilteredReadWith(filters.WherePredicate(), includes);
        }

        /// <summary>
        /// Gets a Simple list of OrderItemSubStatuses specified in query parameters
        /// </summary>
        /// <param name="filters">Query parameters to filter on OrderItemStatusID property</param>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleOrderItemSubStatus[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public Task<SimpleOrderItemSubStatus[]> FilteredSimpleList([FromQuery] SimpleOrderItemSubStatusFilters filters)
        {
            return Task.FromResult(this._service.GetSimpleList(User.BID().Value, filters).ToArray());
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsoletesimplelist")]
        [Obsolete]
        public Task<SimpleOrderItemSubStatus[]> SimpleList()
        {
            return this.FilteredSimpleList(null);
        }

        #region CRUD Overrides

        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single OrderItemSubStatus by ID
        /// </summary>
        /// <param name="ID">OrderItemSubStatus ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single OrderItemSubStatus by ID
        /// </summary>
        /// <param name="ID">OrderItemSubStatus ID</param>
        /// <param name="update">Updated OrderItemSubStatus data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemSubStatus))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItemSubStatus does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderItemSubStatus does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] OrderItemSubStatus update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new OrderItemSubStatus
        /// </summary>
        /// <param name="newModel">New OrderItemSubStatus data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemSubStatus))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderItemSubStatus creation fails")]
        public override async Task<IActionResult> Create([FromBody] OrderItemSubStatus newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Deletes a OrderItemSubStatus by ID
        /// </summary>
        /// <param name="ID">ID of the OrderItemSubStatus to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItemSubStatus does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderItemSubStatus does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            BooleanResponse resp = await this._service.CanDelete((short)ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                return await base.Delete(ID);
            }
            return new BadRequestObjectResult(resp);
        }

        /// <summary>
        /// Clones an existing OrderItemSubStatus to a new OrderItemSubStatus
        /// </summary>
        /// <param name="ID">ID of the OrderItemStatus to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemSubStatus))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source OrderItemSubStatus does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source OrderItemSubStatus does not exist or the cloning fails.")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> Clone(short ID)
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.Clone(ID);
        }

        #endregion

        #region Actions

        /// <summary>
        /// Returns true | false based on whether the OrderItemSubStatus can be deleted.
        /// </summary>
        /// <param name="ID">OrderItemSubStatus ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Links the OrderItemSubStatus to the specified SubStatus.
        /// </summary>
        /// <param name="OrderItemSubStatusID">OrderItemSubStatus ID</param>
        /// /// <param name="linkStatus">LinkSubStatus by Name or ID</param>
        /// <returns></returns>
        [HttpPost("{OrderItemSubStatusID}/action/linkstatus/{linkStatus}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the specified ID does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> LinkStatus(short OrderItemSubStatusID, string linkStatus)
        {
            EntityActionChangeResponse resp = await this._service.LinkStatus(OrderItemSubStatusID, linkStatus, true);
            return resp.ToResult();
        }

        /// <summary>
        /// Deletes the Link between the OrderItemSubStatus and the specified OrderItemStatus.
        /// </summary>
        /// <param name="OrderItemSubStatusID">OrderItemSubStatus ID</param>
        /// <param name="unLinkStatusID">OrderItemStatus ID</param>
        /// <returns></returns>
        [HttpPost("{OrderItemSubStatusID}/action/unlinkstatus/{unLinkStatusID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> UnLinkStatus(short OrderItemSubStatusID, short unLinkStatusID)
        {
            EntityActionChangeResponse resp = await this._service.LinkStatus(OrderItemSubStatusID, unLinkStatusID.ToString(), false);
            return resp.ToResult();
        }

        #endregion

        /// <summary>
        /// Pagination List
        /// </summary>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <param name="filters">Query parameters to filter a property</param>
        /// <param name="skip">Number of items to skip</param>
        /// <param name="take">Number of items to take</param>
        /// <returns></returns>
        [HttpGet("paged")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemSubStatus[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetPagedList([FromQuery] OrderItemSubStatusIncludes includes,
                                                        [FromQuery] OrderItemSubStatusFilter filters,
                                                        [FromQuery]int? skip = null,
                                                        [FromQuery]int? take = null)
        {
            return new OkObjectResult(await this._service.GetPagedListAsync(filters, skip, take, includes));
        }
    }
}
