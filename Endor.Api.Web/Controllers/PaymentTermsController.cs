﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// PaymentTerms Controller
    /// </summary>
    [Route("api/paymentterm")]
    public class PaymentTermsController : CRUDController<PaymentTerm, PaymentTermService, int>, ISimpleListableViewController<SimplePaymentTerm, int>
    {

        /// <summary>
        /// Property to get  SimplePaymentTerm from ApiContext
        /// </summary>
        public ISimpleListableViewService<SimplePaymentTerm, int> ListableService => this._service;

        /// <summary>
        /// PaymentTerms Controller Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="taskQueuer">ITaskQueuer</param>
        /// <param name="cache">ITenantDataCache</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public PaymentTermsController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns all of the PaymentTerms
        /// can be filtered by IsActive
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentTerm))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If PaymentTerm does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] PaymentTermFilter filters)
        {
            return new OkObjectResult(await _service.GetWithFiltersAsync(filters));
        }

        #region Overridden CRUD Methods
        /// <summary>
        /// Returns all of the PaymentTerms
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsoletegetall")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentTerm))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If PaymentTerm does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns a single PaymentTerm by ID
        /// </summary>
        /// <param name="ID">PaymentTerm ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentTerm))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If PaymentTerm does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single PaymentTerm by ID
        /// </summary>
        /// <param name="ID">PaymentTerm ID</param>
        /// <param name="update">Updated PaymentTerm data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentTerm))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If PaymentTerm does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the PaymentTerms does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] PaymentTerm update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new PaymentTerm
        /// </summary>
        /// <param name="newModel">Updated PaymentTerm data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentTerm))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the PaymentTerms creation fails")]
        public override async Task<IActionResult> Create([FromBody] PaymentTerm newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Deletes a PaymentTerm by ID
        /// </summary>
        /// <param name="ID">ID of the PaymentTerm to delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If PaymentTerm does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the PaymentTerms does not exist or the deletion fails")]
        public override async Task<IActionResult> Delete(int ID)
        {
            BooleanResponse resp = await this._service.CanDelete((short)ID);
            if (resp?.Success != true || !resp.Value.HasValue || !resp.Value.Value)
                return new BadRequestObjectResult(resp);
            if (!_service.CheckIfDefault(ID))
            {
                return await base.Delete(ID);
            }

            return new ForbidResult();

        }


        /// <summary>
        /// Clones an existing LaborData to a new LaborData
        /// </summary>
        /// <param name="ID">ID of the LaborData to clone</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentTerm))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source PaymentTerm does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source PaymentTerm does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await DoClone(ID);
        }


        #endregion Overridden CRUD Methods


        /// <summary>
        /// Returns true | false based on whether the PaymentTerm can be deleted.
        /// </summary>
        /// <param name="ID">ID of the PaymentTerm to delete</param>
        /// <returns></returns>
        [HttpGet("{ID}/action/candelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(int ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Sets a PaymentTerm to Active
        /// </summary>
        /// <param name="ID">PaymentTerm ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int) HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int) HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>),
            description: "If the PaymentTerm does not exist.")]
        [SwaggerCustomResponse((int) HttpStatusCode.Unauthorized,
            description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int) HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>),
            description: "If the PaymentTerm does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<int> resp =
                await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets a PaymentTerm to Inactive
        /// </summary>
        /// <param name="ID">PaymentTerm ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int) HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int) HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>),
            description: "If the PaymentTerm does not exist.")]
        [SwaggerCustomResponse((int) HttpStatusCode.Unauthorized,
            description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int) HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>),
            description: "If the PaymentTerm does not exist or fails to set as inactive.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Forbidden, typeof(EntityActionChangeResponse<int>),
            description: "If the PaymentTerm is default.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<int> resp =
                await this._service.SetInactiveIfNotDefault(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets a PaymentTerm to Default
        /// </summary>
        /// <param name="ID">PaymentTerm ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setdefault")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the PaymentTerm does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the PaymentTerm does not exist or fails to set as default.")]
        public async Task<IActionResult> SetDefault(short ID)
        {
            EntityActionChangeResponse resp = await this._service.SetDefault(ID);

            return resp.ToResult();
        }

        /// <summary>
        /// Set the PaymentTerm back to Default
        /// </summary>
        /// <returns></returns>
        [HttpPost("action/cleardefault")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the PaymentTerm does not exist or fails to set as default.")]
        public async Task<IActionResult> ClearDefault()
        {
            EntityActionChangeResponse resp = await this._service.SetDefault(null);

            return resp.ToResult();
        }

        /// <summary>
        /// Gets a Simple list of PaymentTerms
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimplePaymentTerm[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimplePaymentTerm[]> FilteredSimpleList([FromQuery] SimplePaymentTermFilter filters)
        {
            return await this.GetSimpleList<SimplePaymentTerm, int>(User.BID().Value, filters);
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsoletesimplelist")]
        [Obsolete]
        public Task<SimplePaymentTerm[]> SimpleList()
        {
            return this.FilteredSimpleList(null);
        }

        /// <summary>
        /// Pagination List
        /// </summary>
        /// <param name="filters">Query parameters to filter a property</param>
        /// <param name="skip">Number of items to skip</param>
        /// <param name="take">Number of items to take</param>
        /// <returns></returns>
        [HttpGet("paged")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentTerm))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If PaymentTerm does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetPagedList([FromQuery] PaymentTermFilter filters,
                                                        [FromQuery]int? skip = null,
                                                        [FromQuery]int? take = null)
        {
            return new OkObjectResult(await this._service.GetPagedListAsync(filters, skip, take, null));
        }
    }
}
