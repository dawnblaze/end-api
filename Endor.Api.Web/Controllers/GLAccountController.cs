﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// This is a CRM GLAccount Controller
    /// </summary>
    [Route("api/glaccounts")]
    public class GLAccountController :  CRUDController<GLAccount, GLAccountService, int>, ISimpleListableController<SimpleGLAccount, int>
    {

        /// <summary>
        /// GLAccount API Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public GLAccountController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the GlAccounts
        /// with filters
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GLAccount[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] GLAccountFilters filters)
        {
            return new OkObjectResult(await _service.GetWithFiltersAsync(filters));
        }

        /// <summary>
        /// Returns all GLAccounts
        /// use ReadWithFilters instead
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete("Use ReadWithFilters instead")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return ReadWithFilters(null);
        }

        /// <summary>
        /// Returns a single GLAccount by ID
        /// </summary>
        /// <param name="ID">GLAccount ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GLAccount))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the GLAccount does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single GLAccount by ID
        /// </summary>
        /// <param name="ID">GLAccount ID</param>
        /// <param name="update">Updated GLAccount data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GLAccount))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the GLAccount does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the GLAccount does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] GLAccount update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new GLAccount
        /// </summary>
        /// <param name="newModel">New GLAccount data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GLAccount))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the GLAccount creation fails")]
        public override async Task<IActionResult> Create([FromBody] GLAccount newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing GLAccount to a new GLAccount
        /// </summary>
        /// <param name="ID">ID of the GLAccount to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GLAccount))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source GLAccount does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source GLAccount does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes a GLAccount by ID
        /// </summary>
        /// <param name="ID">ID of the GLAccount to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the GLAccount does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the GLAccount does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            BooleanResponse resp = await this._service.CanDelete((short)ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                return await base.Delete(ID);
            }
            else
            {
                return new BadRequestObjectResult(resp);
            }
        }

        #endregion

        /// <summary>
        /// Sets a GLAccount to Active
        /// </summary>
        /// <param name="ID">GLAccount ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the GLAccount does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the GLAccount does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a GLAccount to Inactive
        /// </summary>
        /// <param name="ID">GLAccount ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the GLAccount does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the GLAccount does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the GLAccount can be deleted.
        /// </summary>
        /// <param name="ID">GLAccount ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Gets a Simple ienumerable of GLAccounts
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleListItem<int>[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IEnumerable<SimpleGLAccount>> GetEitherSimpleList([FromQuery]SimpleGLAccountFilters filters)
        {
            return await this._service.SimpleList(filters);
        }

        /// <summary>
        /// this is purely here to satisfy the interface
        /// routing is moved to GetEitherSimpleList instead
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("simpleobsolete")]
        public Task<SimpleGLAccount[]> SimpleList()
        {
            return null;
        }

        /// <summary>
        /// Validates GLAccount
        /// </summary>
        /// <param name="model">The GLAccount model to validate</param>
        /// <param name="prefix"> The key to use when looking up information in Microsoft.AspNetCore.Mvc.ControllerBase.ModelState</param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            if (model == null || !(model is GLAccount))
            {
                this.ModelState.AddModelError("model", "model is not an account");
                return false;
            }

            GLAccount account = (GLAccount)model;
            if (_service.IsParentIDASubaccount(account))
            {
                this.ModelState.AddModelError("ParentID", "Cannot set parent that is already your child");
            }

            if (!(_service.IsGLAccountNumberUnique(account)))
            {
                this.ModelState.AddModelError("Number", "Duplicate GL Account number");
            }

            return base.TryValidateModel(model, prefix);
        }
    }
}
