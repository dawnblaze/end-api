﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Includes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// This is a CRM TaxItem Controller
    /// </summary>
    [Route("api/taxitem")]
    public class TaxItemController : CRUDController<TaxItem, TaxItemService, short>, ISimpleListableViewController<SimpleTaxItem, short>
    {
        /// <summary>
        /// Listable service for TaxItems
        /// </summary>
        public ISimpleListableViewService<SimpleTaxItem, short> ListableService => this._service;

        /// <summary>
        /// TaxItem API Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public TaxItemController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {

        }


        #region Overridden CRUD Methods


        /// <summary>
        /// Returns all of the TaxItems
        /// </summary>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <param name="filters">Query parameters to filter a property</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxItem[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetWithIncludes([FromQuery] TaxItemIncludes includes, [FromQuery] TaxItemFilter filters)
        {
            return new OkObjectResult(await _service.GetWithFiltersAsync(filters, includes));
        }

        /// <summary>
        /// Pagination Testing
        /// </summary>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <param name="filters">Query parameters to filter a property</param>
        /// <param name="taxEngineType">Defaults to TaxJar</param>
        /// <param name="skip">Number of items to skip</param>
        /// <param name="take">Number of items to take</param>
        /// <returns></returns>
        [HttpGet("paged")]
        public async Task<IActionResult> GetPagedTesting([FromQuery] TaxItemIncludes includes,
                                                        [FromQuery] TaxItemFilter filters,
                                                        [FromQuery]int? skip = null,
                                                        [FromQuery]int? take = null)
        {
            return new OkObjectResult(await this._service.GetPagedList(filters, skip, take, includes));
        }

        /// <summary>
        /// Returns all of the TaxItems
        /// </summary>
        /// <returns></returns>
        [HttpGet("Obsolete")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single TaxItem by ID
        /// </summary>
        /// <param name="ID">TaxItem ID</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxItem))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the TaxItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetOneWithIncludes([FromRoute] short ID, [FromQuery] TaxItemIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }

        /// <summary>
        /// Returns a single TaxItem by ID
        /// </summary>
        /// <param name="ID">TaxItem ID</param>
        /// <returns></returns>
        [HttpGet("obsolete/{ID}")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> ReadById(short ID)
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single TaxItem by ID
        /// </summary>
        /// <param name="ID">TaxItem ID</param>
        /// <param name="update">Updated TaxItem data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxItem))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the TaxItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the TaxItem does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] TaxItem update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new TaxItem
        /// </summary>
        /// <param name="newModel">New TaxItem data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxItem))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the TaxItem creation fails")]
        public override async Task<IActionResult> Create([FromBody] TaxItem newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing TaxItem to a new TaxItem
        /// </summary>
        /// <param name="ID">ID of the TaxItem to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxItem))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source TaxItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source TaxItem does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes a TaxItem by ID
        /// </summary>
        /// <param name="ID">ID of the TaxItem to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the TaxItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the TaxItem does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            BooleanResponse resp = await this._service.CanDelete((short)ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                return await base.Delete(ID);
            }
            else
            {
                return new BadRequestObjectResult(resp);
            }
        }

        #endregion

        /// <summary>
        /// Sets a TaxItem to to Active
        /// </summary>
        /// <param name="ID">TaxItem ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the TaxItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the TaxItem does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a TaxItem to to Inactive
        /// </summary>
        /// <param name="ID">TaxItem ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the TaxItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the TaxItem does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the TaxItem can be deleted.
        /// </summary>
        /// <param name="ID">TaxItem ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Gets a Simple list of TaxItems
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleTaxItem[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleTaxItem[]> FilteredSimpleList([FromQuery] TaxItemFilter filters)
        {
            return await this._service.GetSimpleList(filters);
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsoletesimplelist")]
        [Obsolete]
        public Task<SimpleTaxItem[]> SimpleList()
        {
            return this.FilteredSimpleList(null);
        }


        /// <summary>
        /// Links a Tax Item by ID to a Tax Group by ID
        /// </summary>
        /// <param name="taxItemID">Tax Item ID</param>
        /// <param name="taxGroupID">Tax Group ID</param>
        /// <returns></returns>
        [Route("{taxItemID}/action/linktaxgroup/{taxGroupID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<TaxGroup>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<TaxGroup>), description: "If the Tax Group or Item does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<TaxGroup>), description: "If the Tax Group or Item does not exist or fails to link.")]
        public async Task<IActionResult> LinkTaxGroup([FromRoute]short taxItemID, [FromRoute]short taxGroupID)
        {
            return (await this._service.LinkTaxGroup(taxGroupID, taxItemID, true)).ToResult();
        }

        /// <summary>
        /// Unlinks a Tax Item by ID to a Tax Group by ID
        /// </summary>
        /// <param name="taxItemID">Tax Item ID</param>
        /// <param name="taxGroupID">Tax Group ID</param>
        /// <returns></returns>
        [Route("{taxItemID}/action/unlinktaxgroup/{taxGroupID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<TaxGroup>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<TaxGroup>), description: "If the Tax Group or Item does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<TaxGroup>), description: "If the Tax Group or Item does not exist or fails to unlink.")]
        public async Task<IActionResult> UnLinkTaxGroup([FromRoute]short taxItemID, [FromRoute]short taxGroupID)
        {
            return (await this._service.LinkTaxGroup(taxGroupID, taxItemID, false)).ToResult();
        }

    }
}
