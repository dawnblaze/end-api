﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Endor.Security;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Web.Annotation;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Linq;

namespace Endor.Api.Web.Controllers
{
    /*
     * /// <summary>
    /// This is a MessageBodyTemplate Controller
    /// </summary>
    [Route("api/message/bodytemplate")]
    public class MessageBodyTemplateController :CRUDController<MessageBodyTemplate, MessageBodyTemplateService, short>
    {
        private readonly ApiContext _ctx;
        private readonly Lazy<MessageBodyTemplateService> _lazyService;
        private MessageBodyTemplateService _service { get { return _lazyService.Value; } }
        /// <summary>
        /// Api Endpoint for MessageBodyTemplate
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public MessageBodyTemplateController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {

            this._ctx = context;
            migrationHelper.MigrateDb(_ctx);
            this._lazyService = new Lazy<MessageBodyTemplateService>(() => new MessageBodyTemplateService(context, User.BID().Value, taskQueuer, cache,logger, rtmClient, migrationHelper));

        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Obsolete Read Method
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsolete")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Returns list of MessageBodyTemplate. Can be filtered by AppliesToClassTypeID and TemplateType
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MessageBodyTemplate[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] MessageBodyTemplateFilter filters)
        {
            List<MessageBodyTemplate> result = await _service.GetWithFiltersAsyncJunction(filters);
            return Ok(result);

        }

        /// <summary>
        /// Returns a simple list of MessageBodyTemplate. Can be filtered by AppliesToClassTypeID and TemplateType
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleListItem<short>[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> FilteredSimpleList([FromQuery] MessageBodyTemplateFilter filters)
        {
            return new OkObjectResult(await this._service.GetSimpleListWithFiltersAsync(filters));
        }

        /// <summary>
        /// Returns the specified MessageBodyTemplate
        /// </summary>
        /// <param name="ID">MessageBodyTemplate ID</param>
        /// <returns></returns>
        [HttpGet("{ID}/obsolete")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> ReadById(short ID)
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Returns the specified MessageBodyTemplate
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MessageBodyTemplate))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetMessageBodyTemplateById(short ID)
        {
            return new ObjectResult(await this._service.GetFirstOrDefaultAsync(m => m.ID == ID));
        }

        /// <summary>
        /// Updates the existing MessageBodyTemplate
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MessageBodyTemplate))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the MessageBodyTemplate does not exist or the update fails.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] MessageBodyTemplate update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Deletes MessageBodyTemplate record
        /// </summary>
        /// <param name="ID">ID of the MessageBodyTemplate to Delete</param>
        /// <returns></returns>
        [Obsolete]
        [HttpDelete("{ID}/Obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            return await base.Delete(ID);
        }

        /// <summary>
        /// Deletes MessageBodyTemplate record
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerOperationFilter(typeof(IgnoreHttpRequestParameterOperationFilter))]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> DeleteWithForce(short ID)
        {
            return await base.Delete(ID);
        }

        /// <summary>
        /// Create new MessageBodyTemplate record
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempID"></param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MessageBodyTemplate))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public override async Task<IActionResult> Create([FromBody] MessageBodyTemplate newModel, [FromQuery] Guid? tempID = null)
        {
            var result = await this._service.CreateAsync(newModel, tempID);
            var exceptions = this._service.exceptions;
            if(exceptions == null)
            {
                return new OkObjectResult(result);
            }
            else
            {
                return new BadRequestObjectResult(exceptions.FirstOrDefault()?.Message);
            }
            
        }

        #endregion

        #region Overridden Action Methods

        /// <summary>
        /// Sets MessageBodyTemplate to active
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Route("{ID}/action/SetActive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets MessageBodyTemplate to inactive
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Route("{ID}/Action/SetInactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the MessageBodyTemplate does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Checks if MessageBodyTemplate can be deleted
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete(ID)).ToResult();
        }

        /// <summary>
        /// Move this record before the TargetID
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="TargetID"></param>
        /// <returns></returns>
        [Route("{ID}/Action/MoveBefore/{TargetID}")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> MoveBefore(short ID, short TargetID)
        {
            var success = await this._service.MoveTo(ID, TargetID);

            if (success.Success)
                return Ok();
            else if (success.Message.Contains("Not Found"))
            {
                return NotFound("Message Body Template Not Found");
            }

            return BadRequest(success.Message);
        }

        /// <summary>
        /// Move this record after the TargetID
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="TargetID"></param>
        /// <returns></returns>
        [Route("{ID}/Action/MoveAfter/{TargetID}")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the MessageBodyTemplate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> MoveAfter(short ID, short TargetID)
        {
            var success = await this._service.MoveTo(ID, TargetID, true);

            if (success.Success)
                return Ok();
            else if (success.Message.Contains("Not Found"))
            {
                return NotFound("Message Body Template Not Found");
            }

            return BadRequest(success.Message);
        }

        #endregion
    }*/
}
