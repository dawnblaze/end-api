﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Api.Web.Includes;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

using Endor.Api.Web.Annotation;
using Endor.Security.AuthorizationFilters;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Route ends up as api/employeeteam/
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public class EmployeeTeamController : CRUDController<EmployeeTeam, EmployeeTeamService, int>, ISimpleListableViewController<SimpleEmployeeTeam, int>
    {
        /// <summary>
        /// Listable service for EmployeeTeam
        /// </summary>
        public ISimpleListableViewService<SimpleEmployeeTeam, int> ListableService => this._service;

        /// <summary>
        /// Employee Team controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public EmployeeTeamController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns all taxgroups and any included child objects specified in query parameters
        /// </summary>
        /// <param name="ID">ID to retrieve</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [HasSecurityRight(SecurityRight.AccessTeamSetup)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxGroup[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetWithIncludes(int ID, [FromQuery] EmployeeTeamIncludes includes)
        {
            var getEmployeeTeamResult = await _service.GetWithIncludes(ID, includes);
           
            return getEmployeeTeamResult!=null ? (IActionResult) new OkObjectResult(getEmployeeTeamResult) : 
                                                 (IActionResult) new NotFoundResult();
        }

        /// <summary>
        /// Returns a simple list of 'Employee Team'
        /// </summary>
        [Route("simplelist")]
        [HttpGet]
        [HasSecurityRight(SecurityRight.AccessTeamSetup)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleEmployeeTeam[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleEmployeeTeam[]> SimpleList()
        {
            return await this.GetSimpleList<SimpleEmployeeTeam, int>(User.BID().Value);
        }

        /// <summary>
        /// Try's to validate the data model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            if (model == null || !(model is EmployeeTeam))
            {
                this.ModelState.AddModelError("model", "model is not an employee team");
                return false;
            }

            EmployeeTeam employeeTeam = (EmployeeTeam)model;

            if (String.IsNullOrWhiteSpace(employeeTeam.Name))
            {
                this.ModelState.AddModelError("Name", "Team Name is required");
            }

            base.TryValidateModel(model, prefix);
            this.ModelState.Remove("create.Name");
            return this.ModelState.IsValid;
        }

        /// <summary>
        /// Returns all of the 'Employee Team' on the current BID
        /// </summary>
        [HttpGet]
        [HasSecurityRight(SecurityRight.AccessTeamSetup)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeTeam[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [HttpGet]
        public override async Task<IActionResult> Read()
        {
            try
            {
                var allEmployeeTeams = await base.Read();
                var objResult = allEmployeeTeams as ObjectResult;
                if (objResult != null && objResult.Value != null)
                {
                    return await FilteredEmployeeTeam(objResult);
                }
                return allEmployeeTeams;
            }
            catch (Exception ex)
            {
                throw new Exception($"EmployeeTeam.Read: {ex.Message}");
            }
        }

        /// <summary>
        /// Returns all active employee team with same location
        /// </summary>
        /// <param name="objResult"></param>
        /// <returns></returns>
        private async Task<IActionResult> FilteredEmployeeTeam(ObjectResult objResult)
        {
            try
            {
                // END-12029 Fix applied was data type changed from byte to short EmployeeID
                List<EmployeeTeam> empTeam = objResult.Value as List<EmployeeTeam>;
                string employeeid = User.Claims.FirstOrDefault(c => c.Type.Equals("employeeid"))?.Value;
                byte userLocationID = await this._service.GetLocationID(Convert.ToInt16(employeeid));
                if (userLocationID == 0) throw new Exception($"Error: employeeid: {employeeid} has LocationID: {userLocationID}");
                if (empTeam != null)
                {
                    var teams = empTeam.Where(t => t.IsActive
                                                && t.EmployeeTeamLocationLinks != null
                                                && t.EmployeeTeamLocationLinks.FirstOrDefault(tl => tl.LocationID == userLocationID) != null)
                                                    .ToList();
                    return Ok(teams);
                }
                return Ok(new List<EmployeeTeam>());
            }
            catch (Exception ex)
            {
                throw new Exception($"FilteredEmployeeTeam: {ex.Message}");
            }
        }

        /// <summary>
        /// Returns a single EmployeeTeam by ID
        /// </summary>
        /// <param name="ID">EmployeeTeam ID</param>
        [HttpGet("obsolete/{ID}")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeTeam))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the EmployeeTeam does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single EmployeeTeam by ID
        /// </summary>
        /// <param name="ID">EmployeeTeam ID</param>
        /// <param name="update">Updated EmployeeTeam data model</param>
        [HttpPut("{ID}")]
        [HasSecurityRight(SecurityRight.CanManageTeams)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeTeam))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the EmployeeTeam does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the EmployeeTeam does not exist or the update fails.")]        
        public override async Task<IActionResult> Update(int ID, [FromBody] EmployeeTeam update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new EmployeeTeam
        /// </summary>
        /// <param name="newModel">New EmployeeTeam data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        [HttpPost]
        [HasSecurityRight(SecurityRight.CanManageTeams)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeTeam))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the EmployeeTeam creation fails")]        
        public override async Task<IActionResult> Create([FromBody] EmployeeTeam newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing EmployeeTeam to a new EmployeeTeam
        /// </summary>
        /// <param name="ID">ID of the EmployeeTeam to clone from</param>
        [HttpPost("{ID}/clone")]
        [HasSecurityRight(SecurityRight.CanManageTeams)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeTeam))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source EmployeeTeam does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source EmployeeTeam does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes a EmployeeTeam by ID
        /// </summary>
        /// <param name="ID">ID of the EmployeeTeam to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [HasSecurityRight(SecurityRight.CanManageTeams)]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the EmployeeTeam does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the EmployeeTeam does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }

        #region actions
        
        /// <summary>
        /// Sets a EmployeeTeam to to Active
        /// </summary>
        /// <param name="ID">EmployeeTeam ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the EmployeeTeam does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the EmployeeTeam does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a EmployeeTeam to to Inactive
        /// </summary>
        /// <param name="ID">EmployeeTeam ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the EmployeeTeam does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the EmployeeTeam does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the EmployeeTeam can be deleted.
        /// </summary>
        /// <param name="ID">EmployeeTeam ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Links a Team by ID to a Location by ID
        /// </summary>
        /// <param name="teamID">Employee Team ID</param>
        /// <param name="locationID">Location ID</param>
        /// <returns></returns>
        [Route("{teamID}/action/linklocation/{locationID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<EmployeeTeam>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<EmployeeTeam>), description: "If the Team or Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<EmployeeTeam>), description: "If the Team or Location does not exist or fails to link.")]
        public async Task<IActionResult> LinkLocation([FromRoute]int teamID, [FromRoute]byte locationID)
        {
            return (await this._service.LinkLocation(teamID, locationID, true)).ToResult();
        }

        /// <summary>
        /// Unlinks a Team by ID to a Location by ID
        /// </summary>
        /// <param name="teamID">Employee Team ID</param>
        /// <param name="locationID">Location ID</param>
        /// <returns></returns>
        [Route("{teamID}/action/unlinklocation/{locationID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<EmployeeTeam>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<EmployeeTeam>), description: "If the Team or Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<EmployeeTeam>), description: "If the Team or Location does not exist or fails to unlink.")]
        public async Task<IActionResult> UnLinkTaxItem([FromRoute]int teamID, [FromRoute]byte locationID)
        {
            return (await this._service.LinkLocation(teamID, locationID, false)).ToResult();
        }

        /// <summary>
        /// Links a Team by ID to a Employee by ID and assign a role
        /// </summary>
        /// <param name="teamID">Employee Team ID</param>
        /// <param name="employeeID">Location ID</param>
        /// <param name="roleID">Role ID</param>
        /// <returns></returns>
        [Route("{teamID}/action/linkemployee/{employeeID}/role/{roleID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<EmployeeTeam>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<EmployeeTeam>), description: "If the Team or Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<EmployeeTeam>), description: "If the Team or Location does not exist or fails to link.")]
        public async Task<IActionResult> LinkEmployeeRole([FromRoute]int teamID, [FromRoute]short employeeID, [FromRoute]short roleID)
        {
            return (await this._service.LinkEmployeeRole(teamID, employeeID, roleID, true)).ToResult();
        }

        /// <summary>
        /// Unlink a Team by ID to a Employee by ID and assign a role
        /// </summary>
        /// <param name="teamID">Employee Team ID</param>
        /// <param name="employeeID">Location ID</param>
        /// <param name="roleID">Role ID</param>
        /// <returns></returns>
        [Route("{teamID}/action/unlinkemployee/{employeeID}/role/{roleID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<EmployeeTeam>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<EmployeeTeam>), description: "If the Team or Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<EmployeeTeam>), description: "If the Team or Location does not exist or fails to link.")]
        public async Task<IActionResult> UnLinkEmployeeRole([FromRoute]int teamID, [FromRoute]short employeeID, [FromRoute]short roleID)
        {
            return (await this._service.LinkEmployeeRole(teamID, employeeID, roleID, false)).ToResult();
        }
        
        #endregion actions
    }
}
