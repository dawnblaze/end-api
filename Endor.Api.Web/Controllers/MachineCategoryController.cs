﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for MachineCategory
    /// </summary>
    [Authorize]
    [Route("api/machinecategory")]
    public class MachineCategoryController : CRUDController<MachineCategory, MachineCategoryService, short>, ISimpleListableViewController<SimpleMachineCategory, short>
    {
        /// <summary>
        /// Returns all of the MachineCategory
        /// in simple list view
        /// </summary>
        /// <returns></returns>
        public ISimpleListableViewService<SimpleMachineCategory, short> ListableService => this._service;
        /// <summary>
        /// Api Endpoint for Columns
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public MachineCategoryController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns all of the MachineCategories
        /// can be filtered by IsActive
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MachineCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MachineCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] MachineCategoryFilter filters)
        {
            return new OkObjectResult(await _service.GetWithFiltersAsync(filters));
        }

        #region Overridden CRUD Methods
        /// <summary>
        /// Returns all of the MachineCategories
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsoletegetall")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MachineCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MachineCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns a single MachineCategory by ID
        /// </summary>
        /// <param name="ID">MachineCategory ID</param>
        /// <returns></returns>
        [HttpGet("obselete/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MachineCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MachineCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Returns an individual machine category
        /// </summary>
        /// <param name="ID">MachineCategory.ID</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns>`MachineCategory`</returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MachineCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MachineCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithIncludes(short ID, [FromQuery]MachineCategoryIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }

        /// <summary>
        /// Updates a single MachineCategory by ID
        /// </summary>
        /// <param name="ID">MachineCategory ID</param>
        /// <param name="update">Updated MachineCategory data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MachineCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MachineCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the MachineCategory does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] MachineCategory update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new MachineCategory
        /// </summary>
        /// <param name="newModel">Updated MachineCategory data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MachineCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the MachineCategories creation fails")]
        public override async Task<IActionResult> Create([FromBody] MachineCategory newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Deletes a MachineCategory by ID
        /// </summary>
        /// <param name="ID">ID of the MachineCategory to delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MachineCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the MachineCategories does not exist or the deletion fails")]
        public override async Task<IActionResult> Delete(short ID)
        {
            await this._service.unlinkAllAsync(ID);
            return await base.Delete(ID);
        }

        /// <summary>
        /// Machine Category Clone Endpoint
        /// </summary>
        /// <param name="ID">ID of record to be cloned</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<MachineCategory>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<MachineCategory>), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public override async Task<IActionResult> Clone(short ID)
        {
            try
            {
                return await DoClone(ID);
            }
            catch (Exception e)
            {
                GenericResponse<MachineCategory> genericResponse = new GenericResponse<MachineCategory>() { ErrorMessage = e.Message };
                return genericResponse.ToResult();
            }
        }

        #endregion Overridden CRUD Methods


        /// <summary>
        /// Returns true | false based on whether the MachineCategory can be deleted.
        /// </summary>
        /// <param name="ID">ID of the MachineCategory to delete</param>
        /// <returns></returns>
        [HttpGet("{ID}/action/candelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
#pragma warning disable CS1998
        public async Task<IActionResult> CanDelete([FromRoute]short ID)
#pragma warning restore CS1998
        {
            return (new BooleanResponse() { Success = true, Message = "CanDelete: true", Value = true }).ToResult();
        }

        /// <summary>
        /// Sets a MachineCategory to Active
        /// </summary>
        /// <param name="ID">MachineCategory ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the MachineCategory does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the MachineCategory does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets a MachineCategory to Inactive
        /// </summary>
        /// <param name="ID">MachineCategory ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the MachineCategory does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the MachineCategory does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Gets a Simple list of MachineCategories
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleMachineCategory[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleMachineCategory[]> FilteredSimpleList([FromQuery] SimpleListItemIsActiveFilter<SimpleMachineCategory, short> filters)
        {
            return await this.GetSimpleList(User.BID().Value, filters);
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsoletesimplelist")]
        [Obsolete]
        public Task<SimpleMachineCategory[]> SimpleList()
        {
            return this.FilteredSimpleList(null);
        }

        /// <summary>
        /// links a machine category to a macine part
        /// </summary>
        /// <param name="machineCategoryID">MachineCategory.ID</param>
        /// <param name="machineDataID">MachineData.ID</param>
        /// <returns></returns>
        [HttpPost("{machineCategoryID}/action/linkmachine/{machineDataID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> LinkMachine([FromRoute]short machineCategoryID, [FromRoute]int machineDataID)
        {
            return (await this._service.LinkMachine(machineCategoryID, machineDataID, true)).ToResult();
        }

        /// <summary>
        /// unlinks a machine category from a machine part
        /// </summary>
        /// <param name="machineCategoryID">MachineCategory.ID</param>
        /// <param name="machineDataID">MachineData.ID</param>
        /// <returns></returns>
        [HttpPost("{machineCategoryID}/action/unlinkmachine/{machineDataID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> UnLinkMachine([FromRoute]short machineCategoryID, [FromRoute]int machineDataID)
        {
            return (await this._service.LinkMachine(machineCategoryID, machineDataID, false)).ToResult();
        }
        /// <summary>
        /// Validates MachineCategory name if already exists
        /// </summary>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            var categoryModel = (MachineCategory)model;
            var existingNameOwners = this._service.ExistingNameOwners(categoryModel);
            if (existingNameOwners.Count()>0)
            {
                this.ModelState.AddModelError("Name", $"A MachineCategory named {categoryModel.Name} already exists.");
            }

            return base.TryValidateModel(model, prefix);
        }        
    }
}
