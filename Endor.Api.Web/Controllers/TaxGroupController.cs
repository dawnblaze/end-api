﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Includes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// This is a CRM TaxGroup Controller
    /// </summary>
    [Route("api/taxgroup")]
    public class TaxGroupController : CRUDController<TaxGroup, TaxGroupService, short>, ISimpleListableViewController<SimpleTaxGroup, short>
    {
        /// <summary>
        /// Listable service for TaxGroups
        /// </summary>
        public ISimpleListableViewService<SimpleTaxGroup, short> ListableService => this._service;

        /// <summary>
        /// TaxGroup API Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public TaxGroupController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns all taxgroups and any included child objects specified in query parameters
        /// </summary>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <param name="filters">Query parameters to filter a property</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxGroup[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetWithIncludes([FromQuery] TaxGroupIncludes includes, [FromQuery] TaxGroupFilter filters)
        {
            return new OkObjectResult(await _service.GetWithFiltersAsync(filters, includes));
        }

        /// <summary>
        /// Gets a Simple list of TaxGroups
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleTaxGroup[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleTaxGroup[]> FilteredSimpleList([FromQuery] TaxGroupFilter filters)
        {
            return await this._service.GetSimpleList(filters);
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsoletesimplelist")]
        [Obsolete]
        public Task<SimpleTaxGroup[]> SimpleList()
        {
            return this.FilteredSimpleList(null);
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the TaxGroups
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single TaxGroup by ID
        /// </summary>
        /// <param name="ID">TaxGroup ID</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxGroup))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the TaxGroup does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetOneWithIncludes([FromRoute] short ID, [FromQuery] TaxGroupIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }

        /// <summary>
        /// Returns a single TaxGroup by ID
        /// </summary>
        /// <param name="ID">TaxGroup ID</param>
        /// <returns></returns>
        [HttpGet("obsolete/{ID}")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> ReadById(short ID)
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single TaxGroup by ID
        /// </summary>
        /// <param name="ID">TaxGroup ID</param>
        /// <param name="update">Updated TaxGroup data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxGroup))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the TaxGroup does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the TaxGroup does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] TaxGroup update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new TaxGroup
        /// </summary>
        /// <param name="newModel">New TaxGroup data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxGroup))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the TaxGroup creation fails")]
        public override async Task<IActionResult> Create([FromBody] TaxGroup newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing TaxGroup to a new TaxGroup
        /// </summary>
        /// <param name="ID">ID of the TaxGroup to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxGroup))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source TaxGroup does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source TaxGroup does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes a TaxGroup by ID
        /// </summary>
        /// <param name="ID">ID of the TaxGroup to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the TaxGroup does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the TaxGroup does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            BooleanResponse resp = await this._service.CanDelete((short)ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                return await base.Delete(ID);
            }
            return new BadRequestObjectResult(resp);
        }

        #endregion

        /// <summary>
        /// Returns a mapping of LocationIDs to the corresponding TaxGroupID
        /// </summary>
        /// <returns></returns>
        [HttpGet("LocationIDToDefaultTaxGroupID")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(Dictionary<byte, short?>))]
        public async Task<Dictionary<byte, short?>> LocationIDToDefaultTaxGroupID()
        {
            return await this._service.DefaultsForLocationsAsync();
        }

        #region actions

        /// <summary>
        /// Recompute the TaxRate of a TaxGroup
        /// </summary>
        /// <param name="ID">TaxGroup ID</param>
        /// <returns></returns>
        [Route("{ID}/action/recomputetaxrate")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the TaxGroup does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the TaxGroup does not exist.")]
        public async Task<IActionResult> RecomputeTaxRate(short ID)
        {
            EntityActionChangeResponse resp = await this._service.RecomputeTaxRate(ID);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a TaxGroup to to Active
        /// </summary>
        /// <param name="ID">TaxGroup ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the TaxGroup does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the TaxGroup does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a TaxGroup to to Inactive
        /// </summary>
        /// <param name="ID">TaxGroup ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the TaxGroup does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the TaxGroup does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the TaxGroup can be deleted.
        /// </summary>
        /// <param name="ID">TaxGroup ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Links a Tax Group by ID to a Tax Item by ID
        /// </summary>
        /// <param name="taxGroupID">Tax Group ID</param>
        /// <param name="taxItemID">Tax Item ID</param>
        /// <returns></returns>
        [Route("{taxGroupID}/action/linktaxitem/{taxItemID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<TaxGroup>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<TaxGroup>), description: "If the Tax Group or Item does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<TaxGroup>), description: "If the Tax Group or Item does not exist or fails to link.")]
        public async Task<IActionResult> LinkTaxItem([FromRoute]short taxGroupID, [FromRoute]short taxItemID)
        {
            return (await this._service.LinkTaxItem(taxGroupID, taxItemID, true)).ToResult();
        }

        /// <summary>
        /// Unlinks a Tax Group by ID to a Tax Item by ID
        /// </summary>
        /// <param name="taxGroupID">Tax Group ID</param>
        /// <param name="taxItemID">Tax Item ID</param>
        /// <returns></returns>
        [Route("{taxGroupID}/action/unlinktaxitem/{taxItemID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<TaxGroup>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<TaxGroup>), description: "If the Tax Group or Item does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<TaxGroup>), description: "If the Tax Group or Item does not exist or fails to unlink.")]
        public async Task<IActionResult> UnLinkTaxItem([FromRoute]short taxGroupID, [FromRoute]short taxItemID)
        {
            return (await this._service.LinkTaxItem(taxGroupID, taxItemID, false)).ToResult();
        }

        /// <summary>
        /// Links a Tax Group by ID to a Location by ID
        /// </summary>
        /// <param name="taxGroupID">Tax Group ID</param>
        /// <param name="locationID">Location ID</param>
        /// <returns></returns>
        [Route("{taxGroupID}/action/linklocation/{locationID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<TaxGroup>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<TaxGroup>), description: "If the Tax Group or Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<TaxGroup>), description: "If the Tax Group or Location does not exist or fails to link.")]
        public async Task<IActionResult> LinkLocation([FromRoute]short taxGroupID, [FromRoute]short locationID)
        {
            EntityActionChangeResponse resp = await this._service.LinkLocation(taxGroupID, locationID, true);
            return resp.ToResult();
        }

        /// <summary>
        /// Unlinks a Tax Group by ID to a Location by ID
        /// </summary>
        /// <param name="taxGroupID">Tax Group ID</param>
        /// <param name="locationID">Location ID</param>
        /// <returns></returns>
        [Route("{taxGroupID}/action/unlinklocation/{locationID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<TaxGroup>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<TaxGroup>), description: "If the Tax Group or Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<TaxGroup>), description: "If the Tax Group or Location does not exist or fails to unlink.")]
        public async Task<IActionResult> UnLinkLocation([FromRoute]short taxGroupID, [FromRoute]short locationID)
        {
            EntityActionChangeResponse resp = await this._service.LinkLocation(taxGroupID, locationID, false);
            return resp.ToResult();
        }
        #endregion

        /// <summary>
        /// Pagination List
        /// </summary>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <param name="filters">Query parameters to filter a property</param>
        /// <param name="skip">Number of items to skip</param>
        /// <param name="take">Number of items to take</param>
        /// <returns></returns>
        [HttpGet("paged")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxGroup[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetPagedList([FromQuery] TaxGroupIncludes includes,
                                                        [FromQuery] TaxGroupFilter filters,
                                                        [FromQuery]int? skip = null,
                                                        [FromQuery]int? take = null)
        {
            return new OkObjectResult(await this._service.GetPagedListAsync(filters, skip, take, includes));
        }
    }
}
