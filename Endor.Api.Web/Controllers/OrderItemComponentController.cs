using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Security;
using Endor.Tasks;
using Endor.Api.Web.Classes.Request;
using Endor.Api.Web.Annotation;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tenant;

namespace Endor.Api.Web.Controllers
{
	/// <summary>
    /// OrderItemComponentController
    /// </summary>
    [Route("api/[Controller]")]
    public partial class OrderItemComponentController: CRUDController<OrderItemComponent,OrderItemComponentService, int>
    {
		/// <summary>
		/// 
		/// </summary>
		/// <param name="context"></param>
		/// <param name="logger"></param>
		/// <param name="rtmClient"></param>
		/// <param name="taskQueuer"></param>
		/// <param name="cache"></param>
		/// <param name="migrationHelper"></param>
		public OrderItemComponentController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
			: base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="componentClassType"></param>
		/// <param name="componentID"></param>
		/// <param name="partName">Labor or Material Name</param>
		/// <returns></returns>
		[Route("GenerateComponent")]
		[HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemComponent))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]		
		public async Task<OrderItemComponent> GenerateComponent([FromQuery]ClassType componentClassType, [FromQuery]int componentID, [FromQuery]string partName = null)
		{
			return await this._service.GenerateComponent(componentClassType, componentID, null, partName);
		}
    }
}
