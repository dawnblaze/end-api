﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Pricing;

using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes.Clones;
using Microsoft.Extensions.Caching.Memory;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for Estimates
    /// </summary>
    [Authorize]
    [Route("API/[Controller]")]
    public class EstimateController : BaseTransactionHeaderController<EstimateData, EstimateService>
    {
        /// <summary>
        /// Defines this controller as for Estimates
        /// </summary>
        protected override OrderTransactionType TransactionType => OrderTransactionType.Estimate;

        /// <summary>
        /// Api Endpoint for Orders
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="pricingEngine">Pricing Engine</param>
        /// <param name="migrationHelper"></param>
        /// <param name="memoryCache">Memory Cache</param>
        public EstimateController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IPricingEngine pricingEngine, IMigrationHelper migrationHelper, IMemoryCache memoryCache)
            : base(context, logger, rtmClient, taskQueuer, cache, pricingEngine, migrationHelper, memoryCache)
        {
        }

        #region RecomputeGL

        /// <summary>
        /// Feature not supported.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="priority">Priority high|medium|low</param>
        /// <returns></returns>
        [Route("{ID}/Action/RecomputeGL")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Feature not supported.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
#pragma warning disable CS1998 // async lacking await operator
        public override async Task<IActionResult> RecomputeGL(int ID, string priority = "low")
        {
            return BadRequest("Feature not supported.");
        }
#pragma warning restore CS1998 // async lacking await operator

        #endregion RecomputeGL

        /// <summary>
        /// Kicks off the RecomputeGL Task on BgEngine
        /// </summary>
        /// <returns></returns>
        [Route("{ID}/Action/ChangeOrderStatus/{orderStatus}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the estimate order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If orderStatus is invalid.")]
        public override async Task<IActionResult> ChangeOrderStatus(int ID, OrderOrderStatus orderStatus)
        {
            var allowedOrderTypeStatusMap = new Dictionary<OrderTransactionType, OrderOrderStatus[]>()
            {
                {
                    OrderTransactionType.Estimate,
                    new OrderOrderStatus[] {
                        OrderOrderStatus.EstimateApproved,
                        OrderOrderStatus.EstimateLost,
                        OrderOrderStatus.EstimatePending,
                        OrderOrderStatus.EstimateVoided
                    }
                }
            };

            // get the Order
            var order = await _service.GetAsync(ID);
            if (order == null)
            {
                return new NotFoundResult();
            }

            // For Orders, the old status can't be Voided (29).
            // For Estimates, the old status can't be Voided (19).
            // For PO, the old status can't be Voided (49).
            OrderOrderStatus[] voidStatuses = new OrderOrderStatus[] {
                OrderOrderStatus.EstimateVoided
            };

            // Close status
            OrderOrderStatus[] closeStatus = new OrderOrderStatus[]
            {
                OrderOrderStatus.EstimateLost,
                OrderOrderStatus.EstimateVoided
            };

            // Estimate Approved Status
            OrderOrderStatus[] approvedStatus = new OrderOrderStatus[] { OrderOrderStatus.EstimateApproved };

            // Estimate Lost Status
            OrderOrderStatus[] lostStatus = new OrderOrderStatus[] { OrderOrderStatus.EstimateLost };

            // Estimate Voided Status
            OrderOrderStatus[] voidedStatus = new OrderOrderStatus[] { OrderOrderStatus.EstimateVoided };

            var changeCloseStatusToPending = false;
            // Check if Void/Estimate status is change to Pending
            if (closeStatus.Contains(order.OrderStatusID) && orderStatus == OrderOrderStatus.EstimatePending)
                changeCloseStatusToPending = true;

            // Validate against voided Orders / checks wether the initial status is void
            if (voidStatuses.Contains(order.OrderStatusID) && !changeCloseStatusToPending)
            {
                return BadRequest("Status is void");
            }

            // Restrictions on changing status
            var notAllowed = approvedStatus.Contains(order.OrderStatusID) || (lostStatus.Contains(order.OrderStatusID) && voidedStatus.Contains(orderStatus));
            
            // Check against allowed status within the Order type
            OrderOrderStatus[] allowedStatuses = allowedOrderTypeStatusMap.Where(item => item.Key == (OrderTransactionType)order.TransactionType).Select(i => i.Value).First();
            if (!changeCloseStatusToPending && (!allowedStatuses.Where(s => s > order.OrderStatusID).ToList().Contains(orderStatus) || notAllowed))
            {
                return BadRequest("Status is not allowed");
            }

            var response = await this._service.ChangeOrderStatus(order, orderStatus, Request.Headers[ConnectionIDHeaderKey]);

            if (response.Success)
            {
                // call the task service
                await _taskQueuer.RecomputeGL(User.BID().Value, (byte)GLEngine.Models.EnumGLType.Order, ID, "Estimate Status Change", "Estimate Status Change", User.EmployeeID(), null, (byte)GLEntryType.Order_Status_Change);
                await CreateKeyDateBasedOnOrderStatus(order.ID, orderStatus);
            }

            return Ok(response);
        }

        /// <summary>
        /// Convert Estimate
        /// </summary>
        /// <param name="ID">Estimate ID</param>
        /// <param name="ConvertOption"></param>
        /// <returns></returns>
        [Route("{ID}/action/convertestimate")]
        [HttpPost]

        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the estimate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If status is invalid.")]
        public async Task<IActionResult> ConvertEstimate(int ID, [FromBody] ConvertEstimateOption ConvertOption = null)
        {
            try
            {
                OrderOrderStatus[] allowedStatuses = new OrderOrderStatus[] {
                    OrderOrderStatus.OrderPreWIP,
                    OrderOrderStatus.OrderWIP,
                    OrderOrderStatus.OrderBuilt,
                };

                // Validate against allowed statuses
                if (ConvertOption != null && ConvertOption.OrderStatus.HasValue && !allowedStatuses.Contains(ConvertOption.OrderStatus.Value))
                {
                    return BadRequest("Status is not valid");
                }

                if (ConvertOption.OrderStatus == null)
                {
                    ConvertOption.OrderStatus = OrderOrderStatus.OrderWIP;
                }

                var response = await this._service.ConvertEstimate(ID, ConvertOption);

                if (response == null)
                    return NotFound("Estimate not found");

                return Ok(response);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Clones an existing Order to a new Order
        /// </summary>
        /// <param name="ID">ID of the Order to clone from</param>
        /// <param name="CopyFiles">Optional paramter for whether to copy files or not</param>
        /// <param name="SaveAsOrder">Save clone as an order?</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Order does not exist or the cloning fails.")]
        public async Task<IActionResult> DoCloneEstimate(int ID, [FromQuery]bool? CopyFiles = null, [FromQuery]bool? SaveAsOrder = null)
        {

            return await this.DoCloneTransaction(ID, CopyFiles, SaveAsOrder.GetValueOrDefault(false) ? OrderTransactionType.Order : (OrderTransactionType?)null);
        }

        /// <summary>
        /// Clones an existing Order to a new Order
        /// </summary>
        /// <param name="ID">ID of the Order to clone from</param>
        /// <param name="SaveAsOrder">Save clone as an order?</param>
        /// <param name="cloneOption"></param>
        /// <returns></returns>
        [HttpPost("{ID}/cloneWithOption")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Order does not exist or the cloning fails.")]
        public async Task<IActionResult> DoCloneEstimateWithOption(int ID, [FromQuery]bool? SaveAsOrder = null, [FromBody] EstimateCloneOption cloneOption = null)
        {
            return await this._service.CloneTransactionWithOptionAsync(ID, cloneOption, this.User.UserLinkID(), SaveAsOrder.GetValueOrDefault(false) ? OrderTransactionType.Order : (OrderTransactionType?)null);
        }

        /// <summary>
        /// Clones an existing Order to a new Order
        /// </summary>
        /// <param name="ID">ID of the Order to clone from</param>
        /// <param name="CopyFiles">Optional paramter for whether to copy files or not</param>
        /// <returns></returns>
        [Obsolete]
        [HttpPost("{ID}/clone/obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Order does not exist or the cloning fails.")]
        public override async Task<IActionResult> DoClone(int ID, [FromQuery]bool? CopyFiles = null)
        {
            return await this.DoCloneTransaction(ID, CopyFiles);
        }
    }
}