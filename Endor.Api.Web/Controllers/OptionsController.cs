﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Resources;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Options Controller
    /// </summary>
    [Authorize]
    [Route("api/options")]
    public class OptionsController : GenericController<OptionData, OptionService, int>
    {
        private readonly EndorOptions _options;
        /// <summary>
        /// Options Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="options">Options</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OptionsController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, EndorOptions options, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, migrationHelper)
        {
            _options = options;
        }

        /// <summary>
        /// Retrieves a single Option for the given criteria
        /// </summary>
        /// <param name="option">Option Value or ID</param>
        /// <param name="associationID">Association ID</param>
        /// <param name="bid">Business ID</param>
        /// <param name="locationID">Location ID</param>
        /// <param name="storeFrontID">Store Front ID</param>
        /// <param name="userLinkID">User Link ID</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{option}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<OptionValue>), description: "Option successfully retrieved")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<OptionValue>), description: "If the Option does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Option does not exist or fails to retrieve.")]
        public async Task<IActionResult> GetSingle(
                string option = null,
                byte? associationID = null,
                short? bid = null,
                short? locationID = null,
                short? storeFrontID = null,
                int? userLinkID = null,
                int? companyID = null,
                int? contactID = null
            )
        {
            //check that only one parameter has a value
            int sum = 0;
            sum += (associationID != null) ? 1 : 0;
            sum += (bid != null) ? 1 : 0;
            sum += (contactID != null) ? 1 : 0;
            sum += (companyID != null) ? 1 : 0;
            sum += (locationID != null) ? 1 : 0;
            sum += (userLinkID != null) ? 1 : 0;
            if (sum > 1)
            {
                return BadRequest("Only one (1) parameter can be specified at a time");
            }

            var validateResp = ValidateParameters(associationID, bid, locationID, storeFrontID, userLinkID, companyID, contactID);
            if (validateResp != null)
                return validateResp;

            string Name = "";
            if (!int.TryParse(option, out int ID))
                Name = option;

            GetOptionValueResponse resp = await this._service.Get(
                ID == 0 ? (int?)null : ID, Name, bid ?? User.BID().Value, locationID, storeFrontID,
                userLinkID, companyID, contactID);
            return resp.ToResult();
        }

        /// <summary>
        /// Retrieves multiple Options for the given criteria
        /// </summary>
        /// <param name="associationID">Association ID, pass 255 to have the server look it up for this user's BID</param>
        /// <param name="bid">Business ID</param>
        /// <param name="locationID">Location ID</param>
        /// <param name="storeFrontID">Store Front ID</param>
        /// <param name="userLinkID">User Link ID</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<OptionValue>), description: "Options successfully retrieved")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<OptionValue>), description: "If the Options do not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Options do not exist or fail to retrieve.")]
        public async Task<IActionResult> GetMany(
                byte? associationID = null,
                short? bid = null,
                short? locationID = null,
                short? storeFrontID = null,
                int? userLinkID = null,
                int? companyID = null,
                int? contactID = null
            )
        {
            //check that only one parameter has a value
            int sum = 0;
            sum += (associationID != null) ? 1 : 0;
            sum += (bid != null) ? 1 : 0;
            sum += (contactID != null) ? 1 : 0;
            sum += (companyID != null) ? 1 : 0;
            sum += (locationID != null) ? 1 : 0;
            sum += (userLinkID != null) ? 1 : 0;
            if (sum > 1)
            {
                return BadRequest("Only one (1) parameter can be specified at a time");
            }

            var validateResp = ValidateParameters(associationID, bid, locationID, storeFrontID, userLinkID, companyID, contactID);
            if (validateResp != null)
                return validateResp;

            GetOptionValuesResponse resp = await this._service.Get(
                bid ?? User.BID().Value, locationID, storeFrontID,
                userLinkID, companyID, contactID);
            return resp.ToResult();
        }

        /// <summary>
        /// Retrieves multiple Options for the given Category
        /// </summary>
        /// <param name="category">Category Name or ID</param>
        /// <returns></returns>
        [HttpGet]
        [Route("ByCategory/{category}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GetOptionValuesResponse), description: "Category's Options successfully retrieved")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GetOptionValuesResponse), description: "If the Option Category does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Option Category does not exist or fails to retrieve Options.")]
        public async Task<IActionResult> GetByCategory(
                string category = null
            )
        {
            int categoryID;

            if (int.TryParse(category, out categoryID))
            {
                //noop
            } 
            else
            {
                int? IDFromLookup = await this._service.GetCategoryIDByName(category);
                if (IDFromLookup.HasValue)
                    categoryID = IDFromLookup.Value;
                else
                    return BadRequest();
            }

            GetOptionValuesResponse resp = await this._service.Get(
                User.BID().Value, null, null,
                User.UserLinkID(), null, null, categoryID: categoryID);

            return resp.ToResult();
        }

        /// <summary>
        /// Updates a single Option for the given criteria if it exists.
        /// If it does not exist, a new AdHoc Option is created instead.
        /// </summary>
        /// <param name="option">Option Name or ID</param>
        /// <param name="value">Option Value</param>
        /// <param name="associationID">Association ID, pass 255 to have the server look it up for this user's BID</param>
        /// <param name="bid">Business ID</param>
        /// <param name="locationID">Location ID</param>
        /// <param name="storeFrontID">Store Front ID</param>
        /// <param name="userLinkID">User Link ID</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{option}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<OptionValue>), description: "Option successfully updated/created")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<OptionValue>), description: "If the Option does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Option does not exist or fails to update/create.")]
        public async Task<IActionResult> PutSingle(
            [FromBody] string value,
            string option = null,
            [FromQuery] byte? associationID = null,
            [FromQuery] short? bid = null,
            [FromQuery] short? locationID = null,
            [FromQuery] short? storeFrontID = null,
            [FromQuery] int? userLinkID = null,
            [FromQuery] int? companyID = null,
            [FromQuery] int? contactID = null)
        {
            string optionName = "";
            if (!int.TryParse(option, out int ID))
                optionName = option;

            var validateResp = ValidateParameters(associationID, bid, locationID, storeFrontID, userLinkID, companyID, contactID);
            if (validateResp != null)
                return validateResp;

            PutOptionValueResponse resp = await this._service.Put(
                ID == 0 ? (int?)null : ID, optionName, value, associationID, bid ?? User.BID(), locationID, storeFrontID, userLinkID, companyID, contactID);
            return resp.ToResult();
        }

        /// <summary>
        /// Updates multiple Options for the given criteria if they exist.
        /// If it they do not exist, new AdHoc Options are created instead.
        /// </summary>
        /// <param name="options">Options to update/create</param>
        /// <param name="associationID">Association ID, pass 255 to have the server look it up for this user's BID</param>
        /// <param name="bid">Business ID</param>
        /// <param name="locationID">Location ID</param>
        /// <param name="storeFrontID">Store Front ID</param>
        /// <param name="userLinkID">User Link ID</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        [HttpPut]
        [Route("")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<OptionValue>), description: "Options successfully updated/created")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<OptionValue>), description: "If the Options do not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Options do not exist or fail to update/create.")]
        public async Task<IActionResult> PutMany(
            [FromBody] List<Options> options,
            [FromQuery] byte? associationID = null,
            [FromQuery] short? bid = null,
            [FromQuery] short? locationID = null,
            [FromQuery] short? storeFrontID = null,
            [FromQuery] int? userLinkID = null,
            [FromQuery] int? companyID = null,
            [FromQuery] int? contactID = null)
        {
            var validateResp = ValidateParameters(associationID, bid, locationID, storeFrontID, userLinkID, companyID, contactID);
            if (validateResp != null)
                return validateResp;

            PutOptionValuesResponse resp = await this._service.Put(options, associationID, bid ?? User.BID(), locationID, storeFrontID, userLinkID, companyID, contactID);
            return resp.ToResult();
        }

        /// <summary>
        /// For a System Option, this will reset the Option to defaults.
        /// For an AdHoc Option, this will completely delete the Option.
        /// </summary>
        /// <param name="option">Option Value or ID</param>
        /// <param name="associationID">Association ID, pass 255 to have the server look it up for this user's BID</param>
        /// <param name="bid">Business ID</param>
        /// <param name="locationID">Location ID</param>
        /// <param name="storeFrontID">Store Front ID</param>
        /// <param name="userLinkID">User Link ID</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{option}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent, description: "Option successfully deleted/reset")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<OptionValue>), description: "If the Option does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Option does not exist or fails to delete/reset.")]
        public async Task<IActionResult> DeleteSingle(
            string option = null,
            [FromQuery] byte? associationID = null,
            [FromQuery] short? bid = null,
            [FromQuery] short? locationID = null,
            [FromQuery] short? storeFrontID = null,
            [FromQuery] int? userLinkID = null,
            [FromQuery] int? companyID = null,
            [FromQuery] int? contactID = null)
        {
            string optionName = "";
            if (!int.TryParse(option, out int ID))
                optionName = option;

            var validateResp = ValidateParameters(associationID, bid, locationID, storeFrontID, userLinkID, companyID, contactID);
            if (validateResp != null)
                return validateResp;

            DeleteOptionValueResponse resp = await this._service.Delete(
                ID == 0 ? (int?)null : ID, optionName, associationID, bid ?? User.BID(), locationID, storeFrontID, userLinkID, companyID, contactID);

            if (resp.Success)
                return NoContent();
            else
                return resp.ToResult();
        }

        /// <summary>
        /// For System Options, this will reset the Options to defaults.
        /// For AdHoc Options, this will completely delete the Options.
        /// </summary>
        /// <param name="options">Options to reset/delete</param>
        /// <param name="associationID">Association ID</param>
        /// <param name="bid">Business ID</param>
        /// <param name="locationID">Location ID</param>
        /// <param name="storeFrontID">Store Front ID</param>
        /// <param name="userLinkID">User Link ID</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent, description: "Options successfully deleted/reset")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<OptionValue>), description: "If the Options do not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Options do not exist or fail to delete/reset.")]
        public async Task<IActionResult> DeleteMany(
            [FromBody] List<Options> options,
            [FromQuery] byte? associationID = null,
            [FromQuery] short? bid = null,
            [FromQuery] short? locationID = null,
            [FromQuery] short? storeFrontID = null,
            [FromQuery] int? userLinkID = null,
            [FromQuery] int? companyID = null,
            [FromQuery] int? contactID = null)
        {
            var validateResp = ValidateParameters(associationID, bid, locationID, storeFrontID, userLinkID, companyID, contactID);
            if (validateResp != null)
                return validateResp;

            DeleteOptionValuesResponse resp = await this._service.Delete(options, associationID, bid, locationID, storeFrontID, userLinkID, companyID, contactID);
            
            if (resp.Success)
                return NoContent();
            else
                return resp.ToResult();
        }

        /// <summary>
        /// For System Options in the specified Category, this will reset the options to defaults.
        /// For AdHoc Options in the specified Category, this will completely delete the Options.
        /// </summary>
        /// <param name="category">Category Name or ID</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Category/{category}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent, description: "Category's Options successfully deleted/reset")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<OptionValue>), description: "If the Option Category does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Option Category does not exist or fails to delete/reset.")]
        public async Task<IActionResult> DeleteByCategory(string category = null)
        {
            string Name = "";
            if (!int.TryParse(category, out int ID))
                Name = category;

            DeleteOptionValuesResponse resp = await this._service.DeleteByCategory(
                ID == 0 ? (int?)null : ID, Name);

            if (resp.Success)
                return NoContent();
            else
                return resp.ToResult();
        }

        /// <summary>
        /// Get Google API Key
        /// </summary>
        /// <returns></returns>
        [HttpGet("google")]
        public async Task<IActionResult> GetGoogleOptions()
        {
            await Task.CompletedTask;
            if (_options.Client != null && !String.IsNullOrWhiteSpace(_options.Client.GoogleAPIKey))
                return Ok(_options.Client.GoogleAPIKey);

            return NotFound();
        }

        /// <summary>
        /// Validates that the parameters that have been passed into the Controller are in place
        /// </summary>
        /// <param name="associationID"></param>
        /// <param name="bid"></param>
        /// <param name="locationID"></param>
        /// <param name="storeFrontID"></param>
        /// <param name="userLinkID"></param>
        /// <param name="companyID"></param>
        /// <param name="contactID"></param>
        private BadRequestObjectResult ValidateParameters(byte? associationID, short? bid, short? locationID, short? storeFrontID, int? userLinkID, int? companyID, int? contactID)
        {
            if (bid.HasValue && User.BID() != bid.Value)
                return BadRequest("You cannot request options for a business that you are not logged in to");

            if (!bid.HasValue && !associationID.HasValue && !locationID.HasValue && !storeFrontID.HasValue && !userLinkID.HasValue && !companyID.HasValue && !contactID.HasValue)
                return BadRequest("Must provide at least one OptionLevel value for this Option");

            return null;
        }

    }
}
