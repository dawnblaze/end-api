﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /*
    /// <summary>
    /// DomainEmail Controller
    /// </summary>
    [Route("api/domain/email")]
    public class DomainEmailController : CRUDController<DomainEmail, DomainEmailService, short>, ISimpleListableViewController<SimpleDomainEmail, short>
    {
        /// <summary>
        /// DomainEmail Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DomainEmailController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns all of the DomainEmail
        /// in simple list view
        /// </summary>
        /// <returns></returns>
        public ISimpleListableViewService<SimpleDomainEmail, short> ListableService => this._service;

        /// <summary>
        /// Obsolete Read Method
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsolete")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Creates a new DomainEmail
        /// </summary>
        /// <param name="newModel">The Email Domail model to create</param>
        /// <param name="tempID">A temporary ID used when creating objects with document storage</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DomainEmail), "Created a new email domain")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid parameters were supplied or there was a different issue with the request")]
        public override Task<IActionResult> Create([FromBody] DomainEmail newModel, [FromQuery] Guid? tempID = null)
        {
            return base.Create(newModel, tempID);
        }

        /// <summary>
        /// Get a list of DomainEmail based on supplied criteria
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <returns>Returns an array of DomainEmail</returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DomainEmail[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] DomainEmailFilter filters)
        {
            return new OkObjectResult(await this._service.GetWithFiltersAsync(filters));
        }

        /// <summary>
        /// Gets a single DomainEmail by ID
        /// </summary>
        /// <param name="ID">The ID of the DomainEmail to be retrieved</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DomainEmail), "Successfully retrieved the DomainEmail")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "DomainEmail was not found")]
        public override Task<IActionResult> ReadById(short ID)
        {
            return base.ReadById(ID);
        }


        /// <summary>
        /// Updates a DomainEmail
        /// </summary>
        /// <param name="ID">The Id of the DomainEmail to update</param>
        /// <param name="update">Contains the updated DomainEmail model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DomainEmail), "Successfully updated the DomainEmail")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid request properties")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "DomainEmail was not found")]
        public override Task<IActionResult> Update(short ID, [FromBody] DomainEmail update)
        {
            return base.Update(ID, update);
        }

        /// <summary>
        /// Deletes a DomainEmail
        /// </summary>
        /// <param name="ID">The ID of the DomainEmail to be deleted</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "DomainEmail is no longer found")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Delete(short ID)
        {
            BooleanResponse resp = await this._service.CanDelete((short)ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                return await base.Delete(ID);
            }
            else
            {
                return new BadRequestObjectResult(resp);
            }
        }

        /// <summary>
        /// Sets a DomainEmail to Active
        /// </summary>
        /// <param name="ID">DomainEmail ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the DomainEmail does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the DomainEmail does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets an DomainEmail to Inactive
        /// </summary>
        /// <param name="ID">DomainEmail ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the DomainEmail does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the DomainEmail does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the DomainEmail can be deleted.
        /// </summary>
        /// <param name="ID">DomainEmail ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the DomainEmail does not exist.")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Tests a DomainEmail
        /// </summary>
        /// <param name="ID">DomainEmail ID</param>
        /// <param name="creds"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/test")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the DomainEmail does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the DomainEmail fails to test.")]
        public async Task<IActionResult> Test(short ID, [FromBody]DomainEmailTestCredentials creds)
        {
            return (await this._service.Test(ID, creds)).ToResult();
        }

        /// <summary>
        /// Links a DomainEmail and location
        /// </summary>
        /// <param name="ID">DomainEmail ID</param>
        /// <param name="locationID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/linklocation/{locationID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the DomainEmail does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the DomainEmail or Location does not exist, fails to link, or is associated with All Locations.")]
        public async Task<IActionResult> LinkLocation(short ID, byte locationID)
        {
            EntityActionChangeResponse resp = await this._service.LinkLocation(ID, locationID, true);

            return resp.ToResult();
        }

        /// <summary>
        /// UnLinks a DomainEmail and location
        /// </summary>
        /// <param name="ID">DomainEmail ID</param>
        /// <param name="locationID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/unlinklocation/{locationID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the DomainEmail does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the DomainEmail or Location does not exist, fails to unlink, or is associated with All Locations.")]
        public async Task<IActionResult> UnlinkLocation(short ID, byte locationID)
        {
            EntityActionChangeResponse resp = await this._service.LinkLocation(ID, locationID, false);

            return resp.ToResult();
        }

        /// <summary>
        /// returns list of all smtp configuration types
        /// </summary>
        /// <returns></returns>
        [HttpGet("smtpconfigurationtype")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SystemEmailSMTPConfigurationType[]))]
        public async Task<IActionResult> SMTPConfigurationType()
        {
            return new OkObjectResult(await _service.GetSMTPConfigurationTypes());
        }

        /// <summary>
        /// obselete-simplelist
        /// </summary>
        /// <returns></returns>
        [HttpGet("obselete-simplelist")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleDomainEmail[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleDomainEmail[]> SimpleList()
        {
            return await this.GetSimpleList<SimpleDomainEmail, short>(User.BID().Value);
        }

        /// <summary>
        /// Returns simple list of domain email
        /// optionally filtered by IsActive query parameter
        /// </summary>
        /// <returns></returns>
        [HttpGet("simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleDomainEmail[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If invalid filters are specified")]
        public async Task<SimpleDomainEmail[]> SimpleList([FromQuery]SimpleDomainEmailFilters filters)
        {
            return await this.GetSimpleList<SimpleDomainEmail, short>(User.BID().Value, filters);
        }
    }*/
}
