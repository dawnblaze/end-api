﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// This is a GLActivity Controller
    /// </summary>
    [Route("api/glactivity")]
    public class GLActivityController : CRUDController<ActivityGlactivity, GLActivityService, int>
    {

        /// <summary>
        /// GLActivity API Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public GLActivityController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns the GLActivity for the business
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ActivityGlactivity))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Read()
        {
            if (User.BID() == null)
                return Unauthorized();

            return await DoRead();
        }

        /// <summary>
        /// Returns a single GLActivity by ID
        /// </summary>
        /// <param name="ID">GLActivity ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ActivityGlactivity))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the GLActivity does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        #endregion

        /// <summary>
        /// Returns a single GLActivity by ID
        /// </summary>
        /// <param name="ID">GLActivity ID</param>
        /// <param name="returntype">ReturnType - one of final, current or difference</param>
        /// <returns></returns>
        [HttpGet("order/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ActivityGlactivity))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public Task<IActionResult> GenerateGLActivityForOrder(int ID, [FromQuery]string returntype = "difference")
        {
            //effectiveDate = effectiveDate ?? DateTime.UtcNow;
            return Task.FromResult(this._service.GenerateGLActivity(ID, returntype, User, DateTime.UtcNow));
        }


        /// <summary>
        /// Validates GLActivity
        /// </summary>
        /// <param name="model">The GLActivity model to validate</param>
        /// <param name="prefix"> The key to use when looking up information in Microsoft.AspNetCore.Mvc.ControllerBase.ModelState</param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            return base.TryValidateModel(model, prefix);
        }
    }
}
