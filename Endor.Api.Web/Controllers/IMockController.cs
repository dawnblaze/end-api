﻿using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    interface ICRUDController<M, I>
        where M : class, IAtom<I>
        where I : struct, IConvertible
    {
        Task<IActionResult> Read();
        Task<IActionResult> ReadById(int ID);
        Task<IActionResult> Update(int ID, [FromBody] M update);
        Task<IActionResult> Create([FromBody] M newModel, [FromQuery] Guid? tempID = null);
        Task<IActionResult> Clone(int ID);
        Task<IActionResult> Delete(int ID);
    }
}
