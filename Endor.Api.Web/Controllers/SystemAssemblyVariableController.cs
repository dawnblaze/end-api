﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Api.Web.Includes;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// SystemAssemblyVariableController
    /// </summary>
    [Route("api/SystemAssemblyVariable")]
    [Authorize]
    public class SystemAssemblyVariableController : GenericController<SystemAssemblyVariable, SystemAssemblyVariableService, short>, ISimpleListableViewController<SimpleSystemAssemblyVariable, short>
    {
        /// <summary>
        /// Listable service for Employee Controller
        /// </summary>
        public ISimpleListableViewService<SimpleSystemAssemblyVariable, short> ListableService => this._service;

        /// <summary>
        /// System Assembly Variables Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public SystemAssemblyVariableController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, migrationHelper)
        {

        }

        /// <summary>
        /// Returns JSON Collection of SystemAssemblyVariable Objects.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SystemAssemblyVariable[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetAll()
        {
            return new OkObjectResult(await this._service.GetAllAsync());
        }

        /// <summary>
        /// Gets a Simple list of System Assembly Variables
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleSystemAssemblyVariable[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleSystemAssemblyVariable[]> SimpleList()
        {
            return await this.GetSimpleList<SimpleSystemAssemblyVariable, short>(0);
        }

        /// <summary>
        /// Returns the specified SystemAssemblyVariable Object.
        /// </summary>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SystemAssemblyVariable))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetOne(short ID)
        {
            return new OkObjectResult(await this._service.GetOneAsync(ID));
        }
    }
}
