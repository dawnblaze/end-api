﻿using Endor.Api.Web.Classes;
using Endor.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Domain Controller Actions
    /// </summary>
    public partial class DomainController
    {
        /// <summary>
        /// Sets a Domain to Active
        /// </summary>
        /// <param name="ID">Domain ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the Domain does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the Domain does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            this._service.DNSManager = _dnsManager;
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets an Domain to Inactive
        /// </summary>
        /// <param name="ID">Domain ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the Domain does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the Domain does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            this._service.DNSManager = _dnsManager;
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Domain to be the default
        /// </summary>
        /// <param name="ID">Domain ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setdefault")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Domain does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Domain does not exist or fails to set as active.")]
        public async Task<IActionResult> SetDefault(short ID)
        {
            this._service.DNSManager = _dnsManager;
            EntityActionChangeResponse resp = await this._service.SetDefault(ID, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the Domain can be deleted.
        /// </summary>
        /// <param name="ID">Domain ID</param>
        /// <returns></returns>
        [HttpGet("{ID}/action/candelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Domain does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            this._service.DNSManager = _dnsManager;
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Tests the availability of a domain
        /// </summary>
        /// <param name="domain"></param>
        /// <returns></returns>
        [HttpPost("action/testavailability")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> TestAvailability(string domain)
        {
            this._service.DNSManager = _dnsManager;
            BooleanResponse response = await this._service.TestAvailability(domain);

            if (!response.Success)
                return BadRequest(response.ErrorMessage);

            if (!response.Value.GetValueOrDefault(false))
                return BadRequest(response.Message);

            return Ok(response.Message);
        }

        /// <summary>
        /// Refreshes the Dns and SSL statuses
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/refreshstatus")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<DomainData>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Domain does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> RefreshStatus(short ID)
        {
            // refresh DNS
            this._service.DNSManager = _dnsManager;
            GenericResponse<DomainRefreshDnsResponse> domainRefreshResponse = await _service.RefreshDns(ID, _options);

            if (!domainRefreshResponse.Success)
            {
                if (domainRefreshResponse.ErrorMessage.ToLower().Contains("not found"))
                    return new NotFoundResult();

                return new BadRequestObjectResult(new GenericResponse<DomainData>() { Success = domainRefreshResponse.Success, ErrorMessage = domainRefreshResponse.ErrorMessage });
            }

            // refresh SSL
            var resp = await _service.RefreshSslStatus(ID);
            if (resp.HasError && (!string.IsNullOrWhiteSpace(resp.ErrorMessage) && resp.ErrorMessage.Contains("not found")))
                return new NotFoundResult();
            else
                return resp.ToResult();
        }

        /// <summary>
        /// Refreshes the Dns status
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/refreshdnsstatus")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DomainRefreshDnsResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Domain does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> RefreshDnsStatus(short ID)
        {
            this._service.DNSManager = _dnsManager;
            GenericResponse<DomainRefreshDnsResponse> domainRefreshResponse = await _service.RefreshDns(ID, _options);

            if (domainRefreshResponse.Success)
                return Ok(domainRefreshResponse);

            if (!string.IsNullOrWhiteSpace(domainRefreshResponse.ErrorMessage) && domainRefreshResponse.ErrorMessage.ToLower().Contains("not found"))
                return NotFound(domainRefreshResponse.ErrorMessage);

            //Return response object even on failure, needed to get property values representing the attempt and failure
            return BadRequest(domainRefreshResponse);
        }

        /// <summary>
        /// Refreshes the SSL status
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/refreshsslstatus")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<DomainData>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Domain does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> RefreshSslStatus(short ID)
        {
            this._service.DNSManager = _dnsManager;
            var resp = await _service.RefreshSslStatus(ID);
            if (resp.HasError && resp.ErrorMessage.Contains("not found"))
                return new NotFoundResult();
            else
                return resp.ToResult();
        }
    }
}
