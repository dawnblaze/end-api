﻿using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Models;
using Microsoft.AspNetCore.Authorization;
using Endor.RTM;
using Endor.Api.Web.Classes;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// SystemTranslate Controller
    /// </summary>
    [Route("api/system/translate")]
    [Authorize]
    public class SystemTranslateController : GenericController<SystemTranslationDefinition, SystemTranslateService, int>
    {
        
        /// <summary>
        /// Context
        /// </summary>
        protected ApiContext _context;

        private Lazy<SystemTranslateService> _lazy_service;

        /// <summary>
        /// Options Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="options">Options</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public SystemTranslateController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, EndorOptions options, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, migrationHelper)
        {
            this._logger = logger;
            this._context = context;

            this._lazy_service = new Lazy<SystemTranslateService>(() => new SystemTranslateService(context, options, migrationHelper));
        }


        #region Getters
        /// <summary>
        /// Returns JSON Dictionary for the language specified. If no language is specified return the users default language
        /// </summary>
        /// <param name="lang">language code to use</param>
        /// <param name="unverified">Determines if only unverified results are returned</param>
        [HttpGet("dict/{lang}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(Dictionary<string, string>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the language Code could not be found")]
        public async Task<IActionResult> GetLanguageDictionary(string lang = null, [FromQuery]bool? unverified = null)
        {
            var language = GetLanguageType(lang);
            if (!language.Any()) return BadRequest("The specified language could not be found");

            var response = await this._lazy_service.Value.Get(language.First().ID, (unverified.HasValue)?unverified.Value:false);
            if (!response.Success)
            {
                return BadRequest(response.Message);
            } 
            return Ok(response.Data);
        }

        /// <summary>
        /// Returns Entries for the language specified. If no language is specified return the users default language
        /// </summary>
        /// <param name="lang">language code to use</param>
        /// <param name="text">Base Value to Translate</param>
        [HttpGet("single/{lang}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(Dictionary<string, string>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the language Code could not be found")]
        public async Task<IActionResult> GetLanguageEntry(string lang = null, [FromQuery]string text = null)
        {
            var language = GetLanguageType(lang);
            if (!language.Any()) return BadRequest("The specified language could not be found");

            var response = await this._lazy_service.Value.GetLanguageEntry(language.First().ID, text);
            if (!response.Success)
            {
                return BadRequest(response.Message);
            }
            return Ok(response.Data);
        }

        /// <summary>
        /// Returns enum Dictionary for supported languages
        /// </summary>
        [HttpGet("languages")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(List<SystemLanguageType>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public IActionResult GetLanguageTypes(string lang = null, [FromQuery]bool? unverified = null)
        {
            var result = this._context.SystemLanguageTypes.ToList<SystemLanguageType>();         
            return Ok(result);
        }
        #endregion

        #region Create
        /// <summary>
        /// Returns JSON Dictionary for the language specified. If no language is specified return the users default language
        /// </summary>
        /// <param name="lang">language code to use</param>
        /// <param name="newDict">New Dictionary Values to add to Db</param>
        [HttpPost("upload/{lang?}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(Dictionary<string, string>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the language Code could not be found")]
        [Obsolete]
        public async Task<IActionResult> PostLanguageDictionary([FromBody] Dictionary<string,string> newDict, string lang = null)
        {
            var language = GetLanguageType(lang);
            if (!language.Any()) return BadRequest("The specified language could not be found");

            var response = await this._lazy_service.Value.Post(language.First().ID, newDict);
            if (!response.Success)
            {
                return BadRequest(response.Message);
            }
            return Ok(response.Data);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes individual translation of "text" value from the db
        /// </summary>
        /// <param name="lang">language code to use</param>
        /// <param name="text">New Dictionary Values to add to Db</param>
        [HttpDelete("delete/{lang?}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(Dictionary<string, string>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the language Code could not be found")]
        [Obsolete]
        public async Task<IActionResult> DeleteLanguageEntry(string lang = null, [FromQuery]string text = null)
        {
            var language = GetLanguageType(lang);
            if (!language.Any()) return BadRequest("The specified language could not be found");

            var response = await this._lazy_service.Value.DeleteLanguageEntry(language.First().ID, text);

            if (!response.Success)
            {
                return BadRequest(response.Message);
            }
            return Ok(response.Data);
        }
        #endregion

        private IQueryable<SystemLanguageType> GetLanguageType(string lang)
        {
            if ((lang == null) && (User != null))
            {
                var optSvc = new OptionService(this._context, _migrationHelper);
                var res = optSvc.Get(null, "Localization.Language", User.BID().Value, null, null, User.UserID(), null, null);
                lang = res.Result.Value.Value;
            }
            var language = this._context.SystemLanguageTypes.Where(l => l.Code == lang);
            return language;
        }
    }
}
