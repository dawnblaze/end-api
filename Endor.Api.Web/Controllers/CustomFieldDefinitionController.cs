﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{

    /// <summary>
    /// This is a Custom Field Controller
    /// </summary>
    [Route("api/cf/definition")]
    public class CustomFieldDefinitionController : CRUDController<CustomFieldDefinition, CustomFieldDefinitionService, short> 
    {
        /// <summary>
        /// Api Endpoint for Custom Fields Definitions
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CustomFieldDefinitionController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns all custom field definitions and any included child objects specified in query parameters
        /// </summary>
        /// <param name="filter">Query parameters to filter Custom Field Definition</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldDefinition[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetCustomFieldDefinitions([FromQuery] CustomFieldDefinitionFilter filter)
        {
            List<CustomFieldDefinition> result = await _service.GetWithFiltersAsync(filter);
            return Ok(result);
        }

        #region Overridden CRUD Methods
        /// <summary>
        /// Returns all of the Custom Definitions
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsoletegetall")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldDefinition))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If Custom Definition does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns all custom field definitions and any included child objects specified in query parameters
        /// </summary>
        /// <param name="ID">ID of the custom field definition to return</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldDefinition[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the CustomFieldDefinition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Clones a Custom Field Definition
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Obsolete]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }
        #endregion



        /// <summary>
        /// Updates a single custom field definition by ID
        /// </summary>
        /// <param name="ID">Custom Field Definition ID</param>
        /// <param name="update">Updated Custom Field Definition data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldDefinition))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Custom Field Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Custom Field Definition does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] CustomFieldDefinition update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new Custom Field Definition
        /// </summary>
        /// <param name="newModel">New Custom Field Definition data model</param>
        /// <param name="tempID"></param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldDefinition))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Custom Field Definition creation fails")]
        public override async Task<IActionResult> Create([FromBody] CustomFieldDefinition newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel);
        }

        /// <summary>
        /// Deletes a Custom Field Definition by ID
        /// </summary>
        /// <param name="ID">ID of the Custom Field Definition to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Custom Field Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Custom Field Definition does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            return await base.Delete(ID);
        }


        /// <summary>
        /// Sets a Custom Field Definition to to Active
        /// </summary>
        /// <param name="ID">Custom Field Definition ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Company does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Company does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Custom Field Definition to Inactive
        /// </summary>
        /// <param name="ID">Custom Field Definition Id</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetInactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Company does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Company does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Try's to validate the data model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            CustomFieldDefinition cfd = model as CustomFieldDefinition;
            if (cfd == null)
            {
                this.ModelState.AddModelError("model", "model is not a Custom Field Definition.");
                return false;
            }

            if (String.IsNullOrWhiteSpace(cfd.Name))
            {
                this.ModelState.AddModelError("Name", "Name must contain characters.");
            }

            var sharedNamesList = this._service.GetSharedName(cfd).ToList();
            if(sharedNamesList.Count > 0){
                this.ModelState.AddModelError("Name", $"Reference Key \"{cfd.Name}\" already exists.");
            }

            return base.TryValidateModel(model, prefix);
        }

        /// <summary>
        /// Returns true | false based on whether the MachineData can be deleted.
        /// </summary>
        /// <param name="ID">ID of the Custom Field Definition to delete</param>
        /// <returns></returns>
        [HttpGet("{ID}/action/candelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }
    }
}
