﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for Origin
    /// </summary>
    [Authorize]
    [Route("api/[Controller]")]
    public class OriginController : CRUDController<CrmOrigin, OriginService, short>, ISimpleListableViewController<SimpleOriginData, short>
    {

        /// <summary>
        /// Listable service for Origin
        /// </summary>
        public ISimpleListableViewService<SimpleOriginData, short> ListableService => this._service;

        /// <summary>
        /// Api Endpoint for Origin
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OriginController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {

        }

        /// <summary>
        /// Returns all of the Origin
        /// </summary>
        /// <param name="Name">Query parameters to filter Name property</param>
        /// <param name="IsActive">Query parameters to filter IsActive property</param>
        /// <param name="IsTopLevelOnly">Query parameters to filter IsTopLevel property</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleOriginData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public Task<IActionResult> GetByFilter([FromQuery] string Name = "", [FromQuery] bool? IsActive = null, [FromQuery] bool? IsTopLevelOnly = null)
        {
            Name = Name.ToLower();
            return base.FilteredReadWith(
              ti => (ti.Name.Contains(Name ?? ti.Name))
                    && (ti.IsActive == (bool)(IsActive ?? ti.IsActive))
                    && (ti.IsTopLevel == (bool)(IsTopLevelOnly ?? ti.IsTopLevel)), null);
        }

        /// <summary>
        /// Old read method
        /// </summary>
        /// <returns></returns>
        [HttpGet("Obsolete")]
        [Obsolete]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Gets a Simple list of Origin
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("simpleobsolete")]
        public Task<SimpleOriginData[]> SimpleList()
        {
            return null;
        }

        /// <summary>
        /// Returns a filtered list of Simple Origins based on Istoplevel or Isactive
        /// </summary>
        /// <param name="IsTopLevel"></param>
        /// <param name="IsActive"></param>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleOriginData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleOriginData[]> GetSimpleFilterList([FromQuery] bool? IsTopLevel = null, bool? IsActive = null)
        {
            return await this._service.SimpleList(IsTopLevel, IsActive);
        }

        /// <summary>
        /// Sets an Origin to Active
        /// </summary>
        /// <param name="ID">Origin Id</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetActive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the Origin does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the Origin does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets an Origin to InActive
        /// </summary>
        /// <param name="ID">Origin Id</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetInactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the Origin does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the Origin does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// set's the parent to the specified ID.  Set to NULL to clear.
        /// </summary>
        /// <param name="ID">targeted Origin Id to be set as a child</param>
        /// <param name="parentId">targeted Origin Id to be set as parent</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetParent/{parentId}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Origin does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Origin does not exist or fails to set parent")]
        public async Task<IActionResult> SetParent(short ID, int? parentId)
        {
            EntityActionChangeResponse resp = await this._service.SetParent(User.BID().Value, ID, parentId);
            return resp.ToResult();
        }

        /// <summary>
        /// checks if the origin is not in use and can be deleted
        /// </summary>
        /// <param name="ID">targeted Origin Id to be set as a child</param>
        /// <returns></returns>
        [Route("{ID}/Action/CanDelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the Origin does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        //CRUD METHODS-----------------------------------------------------------------------------------------------------------------------------------
        #region CRUDMETHODS

        /// <summary>
        /// Returns a single Origin by ID
        /// </summary>
        /// <param name="ID">Origin ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CrmOrigin))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Origin does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Creates a new Origin
        /// </summary>
        /// <param name="newModel">New Origin data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CrmOrigin))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Origin creation fails")]
        public override async Task<IActionResult> Create([FromBody] CrmOrigin newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Updates a single Origin by ID
        /// </summary>
        /// <param name="ID">Origin ID</param>
        /// <param name="update">Updated Origin data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CrmOrigin))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Origin does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Origin does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] CrmOrigin update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Deletes a Origin by ID
        /// </summary>
        /// <param name="ID">ID of the Origin to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Origin does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Origin does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            BooleanResponse resp = await this._service.CanDelete(ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                return await base.Delete(ID);
            }
            else
            {
                return new BadRequestObjectResult(resp);
            }
        }

        /// <summary>
        /// Clones an existing Origin
        /// </summary>
        /// <param name="ID">ID of the Origin to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CrmOrigin))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Origin does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Origin does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Pagination List
        /// </summary>
        /// <param name="filters">Query parameters to filter a property</param>
        /// <param name="skip">Number of items to skip</param>
        /// <param name="take">Number of items to take</param>
        /// <returns></returns>
        [HttpGet("paged")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxGroup[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetPagedList([FromQuery] CrmOriginFilter filters,
                                                        [FromQuery]int? skip = null,
                                                        [FromQuery]int? take = null)
        {
            return new OkObjectResult(await this._service.GetPagedListAsync(filters, skip, take, null));
        }

        #endregion

    }

}
