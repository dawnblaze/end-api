﻿using Endor.DocumentStorage.Models;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Controller for Document Management actions
    /// </summary>
    public partial class DMController
    {
        #region DeleteMany
        /// <summary>
        /// public facing (for unit tests) delete that is a passthrough to <see cref="BaseDeleteMany(RequestModel, string[])"/>
        /// this is obsoleted so we don't see it in swagger 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="filesAndFoldersToDelete"></param>
        /// <returns></returns>
        [Obsolete]
        public async Task<IActionResult> DeleteMany(RequestModel requestModel, string[] filesAndFoldersToDelete) => await this.BaseDeleteMany(requestModel, filesAndFoldersToDelete);

        /// <summary>
        /// Delete many files and folders: always returns 200 OK
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="filesAndFoldersToDelete"></param>
        /// <returns></returns>
        private async Task<IActionResult> BaseDeleteMany(RequestModel requestModel, string[] filesAndFoldersToDelete)
        {
            if (requestModel.Bucket == BucketRequest.All)
            {
                return BadRequest();
            }

            var ctx = requestModel.GetStorageContext(User.BID().Value, User.AID());
            var results = await new DocumentManager(_cache, ctx).DeleteDocumentsAsync(filesAndFoldersToDelete);
            //if (results == -404) {
            //    return NotFound();
            //}
            return Ok(results);
        }

        /// <summary>
        /// DeleteMany Route 1
        /// </summary>
        /// <param name="bucket">BucketRequest</param>
        /// <param name="ctid">ClassTypeID</param>
        /// <param name="id">ID</param>
        /// <param name="filesAndFoldersToDelete">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <returns></returns>
        /// <response code="200">Returns number of files deleted</response>
        /// <response code="400">If bucket is All</response>
        [HttpPost("delete/" + bucket_ctid_id_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:"If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files deleted")]
        public async Task<IActionResult> DeleteManyRoute1(BucketRequest bucket, int? ctid, int? id, [FromBody]string[] filesAndFoldersToDelete)
        {
            var requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, id = id };
            return await this.BaseDeleteMany(requestModel, filesAndFoldersToDelete);
        }

        /// <summary>
        /// DeleteMany Route 2
        /// </summary>
        /// <param name="bucket">BucketRequest</param>
        /// <param name="ctid">ClassTypeID</param>
        /// <param name="classFolder">class folder name</param>
        /// <param name="filesAndFoldersToDelete">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <returns></returns>
        /// <response code="200">Returns number of files deleted</response>
        /// <response code="400">If bucket is All</response>
        [HttpPost("delete/" + bucket_ctid_folder_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:"If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files deleted")]
        public async Task<IActionResult> DeleteManyRoute2(BucketRequest bucket, int? ctid, string classFolder, [FromBody]string[] filesAndFoldersToDelete)
        {
            var requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, classFolder = classFolder };
            return await this.BaseDeleteMany(requestModel, filesAndFoldersToDelete);
        }

        /// <summary>
        /// DeleteMany Route 3
        /// </summary>
        /// <param name="bucket">BucketRequest</param>
        /// <param name="guid">guid</param>
        /// <param name="filesAndFoldersToDelete">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <returns></returns>
        /// <response code="200">Returns number of files deleted</response>
        /// <response code="400">If bucket is All</response>
        [HttpPost("delete/" + bucket_temp_guid_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:"If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files deleted")]
        public async Task<IActionResult> DeleteManyRoute3(BucketRequest bucket, Guid? guid, [FromBody]string[] filesAndFoldersToDelete)
        {
            var requestModel = new RequestModel() { Bucket = bucket, guid = guid };
            return await this.BaseDeleteMany(requestModel, filesAndFoldersToDelete);
        }
        #endregion

        #region DuplicateMany
        /// <summary>
        /// public facing (for unit tests) duplicate that is a passthrough to <see cref="BaseDuplicateMany(RequestModel, string[])"/>
        /// this is obsoleted so we don't see it in swagger 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="filesAndFoldersToDuplicate"></param>
        /// <returns></returns>
        [Obsolete]
        public async Task<IActionResult> Duplicate(RequestModel requestModel, string[] filesAndFoldersToDuplicate) => await this.BaseDuplicateMany(requestModel, filesAndFoldersToDuplicate);

        /// <summary>
        /// duplicates many files and folders
        /// can return bad request, OK, and not found
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="filesAndFoldersToDuplicate"></param>
        /// <returns></returns>
        private async Task<IActionResult> BaseDuplicateMany(RequestModel requestModel, string[] filesAndFoldersToDuplicate)
        {
            if (requestModel.Bucket == BucketRequest.All)
            {
                return BadRequest();
            }

            var ctx = requestModel.GetStorageContext(User.BID().Value, User.AID());
            try
            {
                var results = await new DocumentManager(_cache, ctx).DuplicateDocumentsAsync(filesAndFoldersToDuplicate);
                
                return Ok(results);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// DuplicateMany Route 1
        /// </summary>
        /// <param name="bucket">BucketRequest</param>
        /// <param name="ctid">ClassTypeID</param>
        /// <param name="id">ID</param>
        /// <param name="filesAndFolders">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <returns></returns>
        /// <response code="200">Returns number of files deleted</response>
        /// <response code="400">If bucket is All</response>
        /// <response code="404"></response>
        [HttpPost("duplicate/" + bucket_ctid_id_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files duplicated")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> DuplicateManyRoute1(BucketRequest bucket, int? ctid, int? id, [FromBody]string[] filesAndFolders)
        {
            var requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, id = id };
            return await this.BaseDuplicateMany(requestModel, filesAndFolders);
        }

        /// <summary>
        /// DuplicateMany Route 2
        /// </summary>
        /// <param name="bucket">BucketRequest</param>
        /// <param name="ctid">ClassTypeID</param>
        /// <param name="classFolder">class folder name</param>
        /// <param name="filesAndFolders">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <returns></returns>
        /// <response code="200">Returns number of files deleted</response>
        /// <response code="400">If bucket is All</response>
        /// <response code="404"></response>
        [HttpPost("duplicate/" + bucket_ctid_folder_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files duplicated")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> DuplicateManyRoute2(BucketRequest bucket, int? ctid, string classFolder, [FromBody]string[] filesAndFolders)
        {
            var requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, classFolder = classFolder };
            return await this.BaseDuplicateMany(requestModel, filesAndFolders);
        }

        /// <summary>
        /// DuplicateMany Route 3
        /// </summary>
        /// <param name="bucket">BucketRequest</param>
        /// <param name="guid">guid</param>
        /// <param name="filesAndFolders">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <returns></returns>
        /// <response code="200">Returns number of files deleted</response>
        /// <response code="400">If bucket is All</response>
        /// <response code="404"></response>
        [HttpPost("duplicate/" + bucket_temp_guid_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files duplicated")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> DuplicateManyRoute3(BucketRequest bucket, Guid? guid, [FromBody]string[] filesAndFolders)
        {
            var requestModel = new RequestModel() { Bucket = bucket, guid = guid };
            return await this.BaseDuplicateMany(requestModel, filesAndFolders);
        }        
        #endregion

        #region MoveMany
        /// <summary>
        /// public facing (for unit tests) Move that is a passthrough to <see cref="BaseMoveMany(RequestModel, string, int?, int?, string[])"/>
        /// this is obsoleted so we don't see it in swagger 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="destination"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="filesAndFoldersToMove"></param>
        /// <returns></returns>
        [Obsolete]
        public async Task<IActionResult> Move(RequestModel requestModel, string destination, int? destCTID, int? destID, string[] filesAndFoldersToMove)
            => await this.BaseMoveMany(requestModel, destination, destCTID, destID, filesAndFoldersToMove);

        private async Task<IActionResult> BaseMoveMany(RequestModel requestModel, string destination, int? destCTID, int? destID, string[] filesAndFoldersToMove)
        {
            if (requestModel.Bucket == BucketRequest.All)
            {
                return BadRequest();
            }

            var ctx = requestModel.GetStorageContext(User.BID().Value, User.AID());

            var foldersToMove = filesAndFoldersToMove.Where(x=>x.EndsWith($"/{AzureStorage.EntityStorageClient.SpecialFolderBlobName}")).ToArray();
            var filesToMove = filesAndFoldersToMove.Where(x=>!x.EndsWith($"/{AzureStorage.EntityStorageClient.SpecialFolderBlobName}")).ToArray();

            var tskMoveFolders = new DocumentManager(_cache, ctx).MoveFoldersAsync(foldersToMove, destination, destCTID, destID, null);
            var tskMoveFiles = new DocumentManager(_cache, ctx).MoveFilesAsync(filesToMove, destination, destCTID, destID, null);

            var results = await Task.WhenAll(tskMoveFiles, tskMoveFolders);
            
            return Ok(results.Sum());
        }

        /// <summary>
        /// MoveMany Route 1
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="id"></param>
        /// <param name="destination"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="filesAndFolders">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <returns></returns>
        /// <response code="200">Returns number of files moved</response>
        /// <response code="400">If bucket is All</response>
        [HttpPost("move/" + bucket_ctid_id_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files moved")]
        public async Task<IActionResult> MoveManyRoute1(BucketRequest bucket, int? ctid, int? id, [FromQuery]string destination, [FromQuery]int? destCTID, [FromQuery]int? destID, [FromBody]string[] filesAndFolders)
        {
            var requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, id = id };
            return await this.BaseMoveMany(requestModel, destination, destCTID, destID, filesAndFolders);
        }

        /// <summary>
        /// MoveMany Route 2
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="classFolder"></param>
        /// <param name="destination"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="filesAndFolders">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <returns></returns>
        /// <response code="200">Returns number of files moved</response>
        /// <response code="400">If bucket is All</response>
        [HttpPost("move/" + bucket_ctid_folder_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files moved")]
        public async Task<IActionResult> MoveManyRoute2(BucketRequest bucket, int? ctid, string classFolder, [FromQuery]string destination, [FromQuery]int? destCTID, [FromQuery]int? destID, [FromBody]string[] filesAndFolders)
        {
            var requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, classFolder = classFolder };
            return await this.BaseMoveMany(requestModel, destination, destCTID, destID, filesAndFolders);
        }

        /// <summary>
        /// MoveMany Route 3
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="guid"></param>
        /// <param name="destination"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="filesAndFolders">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <returns></returns>
        /// <response code="200">Returns number of files moved</response>
        /// <response code="400">If bucket is All</response>
        [HttpPost("move/" + bucket_temp_guid_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files moved")]
        public async Task<IActionResult> MoveManyRoute3(BucketRequest bucket, Guid? guid, [FromQuery]string destination, [FromQuery]int? destCTID, [FromQuery]int? destID, [FromBody]string[] filesAndFolders)
        {
            var requestModel = new RequestModel() { Bucket = bucket, guid = guid };
            return await this.BaseMoveMany(requestModel, destination, destCTID, destID, filesAndFolders);
        }
        #endregion

        #region CopyMany
        /// <summary>
        /// public facing (for unit tests) copy that is a passthrough to <see cref="BaseMoveMany(RequestModel, string, int?, int?, string[])"/>
        /// this is obsoleted so we don't see it in swagger 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="destination"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="filesAndFoldersToMove"></param>
        /// <returns></returns>
        [Obsolete]
        public async Task<IActionResult> CopyMany(RequestModel requestModel, string destination, int? destCTID, int? destID, string[] filesAndFoldersToMove)
            => await this.BaseCopyMany(requestModel, destination, destCTID, destID, filesAndFoldersToMove);

        private async Task<IActionResult> BaseCopyMany(RequestModel requestModel, string destination, int? destCTID, int? destID, string[] filesAndFoldersToMove)
        {
            if (requestModel.Bucket == BucketRequest.All)
            {
                return BadRequest();
            }

            var ctx = requestModel.GetStorageContext(User.BID().Value, User.AID());
            var results = await new DocumentManager(_cache, ctx).CopyDocumentsAsync(filesAndFoldersToMove, destination, null, destCTID, destID);

            return Ok(results);
        }

        /// <summary>
        /// Copy Many Route 1
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="id"></param>
        /// <param name="destination"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="filesAndFolders">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <returns></returns>
        /// <response code="200">Returns number of files copied</response>
        /// <response code="400">If bucket is All</response>
        [HttpPost("copy/" + bucket_ctid_id_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files copied")]
        public async Task<IActionResult> CopyManyRoute1(BucketRequest bucket, int? ctid, int? id, string destination, int? destCTID, int? destID, string[] filesAndFolders)
        {
            var requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, id = id };
            return await this.BaseCopyMany(requestModel, destination, destCTID, destID, filesAndFolders);
        }

        /// <summary>
        /// Copy Many Route 2
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="classFolder"></param>
        /// <param name="destination"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="filesAndFolders">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <returns></returns>
        /// <response code="200">Returns number of files copied</response>
        /// <response code="400">If bucket is All</response>
        [HttpPost("copy/" + bucket_ctid_folder_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files copied")]
        public async Task<IActionResult> CopyManyRoute2(BucketRequest bucket, int? ctid, string classFolder, string destination, int? destCTID, int? destID, string[] filesAndFolders)
        {
            var requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, classFolder = classFolder };
            return await this.BaseCopyMany(requestModel, destination, destCTID, destID, filesAndFolders);
        }

        /// <summary>
        /// Copy Many Route 3
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="guid"></param>
        /// <param name="destination"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="filesAndFolders">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <returns></returns>
        /// <response code="200">Returns number of files copied</response>
        /// <response code="400">If bucket is All</response>
        [HttpPost("copy/" + bucket_temp_guid_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files copied")]
        public async Task<IActionResult> CopyManyRoute3(BucketRequest bucket, Guid? guid, string destination, int? destCTID, int? destID, string[] filesAndFolders)
        {
            var requestModel = new RequestModel() { Bucket = bucket, guid = guid };
            return await this.BaseCopyMany(requestModel, destination, destCTID, destID, filesAndFolders);
        }
        #endregion

        #region ZipMany
        /// <summary>
        /// public facing (for unit tests) Move that is a passthrough to <see cref="BaseZipMany(RequestModel, string[], bool)"/>
        /// this is obsoleted so we don't see it in swagger 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="filesAndFoldersToZip"></param>
        /// <param name="flatExport"></param>
        /// <returns></returns>
        [Obsolete]
        public async Task<IActionResult> ZipMany(RequestModel requestModel, string[] filesAndFoldersToZip, bool flatExport = false)
            => await this.BaseZipMany(requestModel, filesAndFoldersToZip, flatExport);

        private async Task<IActionResult> BaseZipMany(RequestModel requestModel, string[] filesAndFoldersToZip, bool flatExport = false)
        {
            if (requestModel.Bucket == BucketRequest.All)
            {
                return BadRequest();
            }

            var ctx = requestModel.GetStorageContext(User.BID().Value, User.AID());
            
            var results = await new DocumentManager(_cache, ctx).ZipDocumentsAsync(filesAndFoldersToZip.Select(t => t.TrimStart('/')).ToArray(), GetUploaderID(), GetUploaderCTID(), flatExport);
            if (results == null)
                return NotFound();

            return Ok(results);
        }

        /// <summary>
        /// ZipMany Route 1
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="id"></param>
        /// <param name="pathAndFilename"></param>
        /// <param name="filesAndFolders">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <param name="flatExport"></param>
        /// <returns></returns>
        /// <response code="200">Returns a URL with token to get the ZIP file from</response>
        /// <response code="400">If bucket is All</response>
        /// <response code="404">If there are no documents to zip</response>
        [HttpPost("zip/" + bucket_ctid_id_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns a URL with token to get the ZIP file from")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description:"If there are no documents to zip")]
        public async Task<IActionResult> ZipManyRoute1(BucketRequest bucket, int? ctid, int? id, string pathAndFilename, [FromBody]string[] filesAndFolders, [FromQuery] bool flatExport = false)
        {
            var requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, id = id, PathAndFilename = pathAndFilename };
            return await this.BaseZipMany(requestModel, filesAndFolders, flatExport);
        }

        /// <summary>
        /// ZipMany Route 2
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="classFolder"></param>
        /// <param name="pathAndFilename"></param>
        /// <param name="filesAndFolders">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <param name="flatExport"></param>
        /// <returns></returns>
        /// <response code="200">Returns a URL with token to get the ZIP file from</response>
        /// <response code="400">If bucket is All</response>
        /// <response code="404">If there are no documents to zip</response>
        [HttpPost("zip/" + bucket_ctid_folder_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns a URL with token to get the ZIP file from")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description:"If there are no documents to zip")]
        public async Task<IActionResult> ZipManyRoute2(BucketRequest bucket, int? ctid, string classFolder, string pathAndFilename, [FromBody]string[] filesAndFolders, [FromQuery] bool flatExport = false)
        {
            var requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, classFolder = classFolder, PathAndFilename = pathAndFilename };
            return await this.BaseZipMany(requestModel, filesAndFolders, flatExport);
        }

        /// <summary>
        /// ZipMany Route 3
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="guid"></param>
        /// <param name="pathAndFilename"></param>
        /// <param name="filesAndFolders">example value: `[ "fileName.jpg", "folderName/anotherFile.txt" ]`</param>
        /// <param name="flatExport"></param>
        /// <returns></returns>
        /// <response code="200">Returns a URL with token to get the ZIP file from</response>
        /// <response code="400">If bucket is All</response>
        /// <response code="404">If there are no documents to zip</response>
        [HttpPost("zip/" + bucket_temp_guid_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If bucket is All")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns a URL with token to get the ZIP file from")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description:"If there are no documents to zip")]
        public async Task<IActionResult> ZipManyRoute3(BucketRequest bucket, Guid? guid, string pathAndFilename, [FromBody]string[] filesAndFolders, [FromQuery] bool flatExport = false)
        {
            var requestModel = new RequestModel() { Bucket = bucket, guid = guid, PathAndFilename = pathAndFilename };
            return await this.BaseZipMany(requestModel, filesAndFolders, flatExport);
        }
        #endregion

        /// <summary>
        /// Zips up all of a users buckets
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Returns a URL with token to get the ZIP file from</response>
        [HttpGet("zip/all")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns a URL with token to get the ZIP file from")]
        public async Task<IActionResult> ZipAllOfUsers()
        {
            var ctx = new StorageContext(User.BID().Value, BucketRequest.All, null);
            var results = await new DocumentManager(_cache, ctx).ZipAllDocumentsAsync(GetUploaderID(), GetUploaderCTID());

            return Ok(results);
        }

        #region Initialize
        /// <summary>
        /// public facing (for unit tests) Move that is a passthrough to <see cref="BaseInitialize(RequestModel)"/>
        /// this is obsoleted so we don't see it in swagger 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        [Obsolete]
        public async Task<IActionResult> Initialize(RequestModel requestModel) => await this.BaseInitialize(requestModel);

        private async Task<IActionResult> BaseInitialize(RequestModel requestModel)
        {
            var ctx = requestModel.GetStorageContext(User.BID().Value, User.AID());
            var results = await new DocumentManager(_cache, ctx).InitializeAllAsync();

            return Ok(results);
        }

        /// <summary>
        /// Copy from template for a given ID/CTID - Route 1
        /// </summary>
        /// <param name="ctid"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Returns number of files copied</response>
        [HttpPost("copyfromtemplate/{ctid:int}/{id:int}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files copied")]
        public async Task<IActionResult> InitializeRoute1([FromRoute]int? ctid, int? id)
        {
            var requestModel = new RequestModel() { ctid = ctid, id = id };
            return await this.BaseInitialize(requestModel);
        }

        /// <summary>
        /// Copy from template for a given ID/tempID - Route 2
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="ctid"></param>
        /// <returns></returns>
        /// <response code="200">Returns number of files copied</response>
        [HttpPost("copyfromtemplate/temp/{guid:guid}/as/{ctid:int}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files copied")]
        public async Task<IActionResult> InitializeRoute2(Guid? guid, int? ctid)
        {
            var requestModel = new RequestModel() { ctid = ctid, guid = guid };
            return await this.BaseInitialize(requestModel);
        }
        #endregion

        /// <summary>
        /// Moves all files from a temporary bucket into a permanent bucket
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="ctid"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Returns number of files persisted</response>
        /// <response code="400">If any of GUID, CTID, and ID are omitted</response>
        [HttpPost("persist/all/temp/{guid:guid}/as/{ctid:int}/{id:int}")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If any of GUID, CTID, and ID are omitted")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files persisted")]
        public async Task<IActionResult> PersistTemporary(Guid? guid, int? ctid, int? id)
        {
            RequestModel requestModel = new RequestModel()
            {
                guid = guid,
                ctid = ctid,
                id = id
            };

            if (!(requestModel.guid.HasValue && requestModel.ctid.HasValue && requestModel.id.HasValue))
            {
                return BadRequest();
            }

            var ctx = requestModel.GetStorageContext(User.BID().Value, User.AID());
            var results = await new DocumentManager(_cache, ctx).PersistDocumentsAsync(requestModel.guid.Value, requestModel.id.Value, requestModel.ctid.Value);

            return Ok(results);
        }

        /// <summary>
        /// Calculates the total space used by a business in document storage
        /// 
        /// CURRENTLY UNIMPLEMENTED
        /// </summary>
        /// <returns></returns>
        /// <response code="500"></response>
        [HttpPost("calcusage")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CalcUsage()
        {
            await Task.Yield();
            return StatusCode(500);
        }

        /// <summary>
        /// Delete all storage for a given classtypeid/id
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Returns number of files deleted</response>
        [HttpPost("delete/{ctid:int}/{id:int}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files deleted")]
        public async Task<IActionResult> DeleteAll(int ctid, int id)
        {
            IStorageContext ctx = new RequestModel() { Bucket = BucketRequest.All, ctid = ctid, id = id }.GetStorageContext(User.BID().Value, User.AID());
            int deletedCount = await new DocumentManager(_cache, ctx).DeleteAllDocumentsAsync();

            return Ok(deletedCount);
        }

        /// <summary>
        /// Clones all storage for a given classtypeid/id into a destination ID
        /// </summary>
        /// <param name="ctid"></param>
        /// <param name="id"></param>
        /// <param name="destid"></param>
        /// <returns></returns>
        /// <response code="200">Returns number of files cloned</response>
        [HttpPost("clone/{ctid:int}/{id:int}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Returns number of files cloned")]
        public async Task<IActionResult> Clone(int ctid, int id, [FromQuery]int destid)
        {
            IStorageContext ctx = new RequestModel() { Bucket = BucketRequest.All, ctid = ctid, id = id }.GetStorageContext(User.BID().Value, User.AID());
            int clonedCount = await new DocumentManager(_cache, ctx).CloneAllDocumentsAsync(destid);

            return Ok(clonedCount);
        }
    }
}
