﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Controller for system info like timezones
    /// </summary>
    [Authorize]
    [Route("api/system")]
    public class SystemInfoController : GenericController<EnumTimeZone, EnumTimeZoneService, short>, ISimpleEnumListableController<SimpleEnumTimeZone, short>
    {
        /// <summary>
        /// Property to get  SimpleEnumTimeZone from ApiContext
        /// </summary>
        public ISimpleEnumListableService<SimpleEnumTimeZone, short> ListableService => this._service;

        /// <summary>
        /// System Info Controller Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public SystemInfoController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, migrationHelper)
        {
        }

        #region enum.TimeZone

        /// <summary>
        /// Endpoint for retrieving a simple list of timezones
        /// </summary>
        /// <returns></returns>
        [HttpGet("timezone/simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, type: typeof(SimpleEnumTimeZone[]), description:"An array of simple timezone enums")]
        public async Task<SimpleEnumTimeZone[]> SimpleEnumList()
        {
            return await this.GetSimpleEnumList<SimpleEnumTimeZone, short>();
        }

        #endregion
    }
}
