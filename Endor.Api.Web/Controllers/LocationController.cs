﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Security.AuthorizationFilters;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Location API Controller
    /// </summary>
    [Authorize]
    [Route("api/[Controller]")]
    public class LocationController : CRUDController<LocationData, LocationService, byte>, ISimpleListableViewController<SimpleLocationData, byte>
    {
        /// <summary>
        /// Listable service for Location Controller
        /// </summary>
        public ISimpleListableViewService<SimpleLocationData, byte> ListableService => this._service;

        /// <summary>
        /// Location API Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public LocationController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the Locations
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HasSecurityRight(SecurityRight.AccessLocationSetup)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single Location by ID
        /// </summary>
        /// <param name="ID">Location ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [HasSecurityRight(SecurityRight.AccessLocationSetup)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(byte ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single Location by ID
        /// </summary>
        /// <param name="ID">Location ID</param>
        /// <param name="update">Updated Location data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [HasSecurityRight(SecurityRight.CanEditLocations)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Location does not exist or the update fails.")]
        public override async Task<IActionResult> Update(byte ID, [FromBody] LocationData update)
        {
            if (update == null)
            {
                this.LogModelErrors();
                return BadRequest();
            }


            SetUpdateModelProperties(ID, update);

            ModelState.Clear();

            _service.DoBeforeValidate(update);
            TryValidateModel(update, "update");
           
            if (!ModelState.IsValid)
            {
                return this.ApiValidationError();
            }

            var result = await _service.UpdateAsync(update, Request.Headers[ConnectionIDHeaderKey], User.UserLinkID());
            if (result == null)
            {
                await _logger.Error(update.BID, "UpdateAsync returned null", null);
                return this.SQLSaveError(_service.exceptions);
            }

            // Warning:
            // This does not re-request from server, so any DB Level side-effects
            // will not be included.
            return new OkObjectResult(update);
            
        }

        /// <summary>
        /// Creates a new Location
        /// </summary>
        /// <param name="newModel">New Location data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [HasSecurityRight(SecurityRight.CanCreateNewLocations)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Location creation fails")]
        public override async Task<IActionResult> Create([FromBody] LocationData newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing Location to a new Location
        /// </summary>
        /// <param name="ID">ID of the Location to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/CloneObsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Location does not exist or the cloning fails.")]
        [Obsolete]
        public override async Task<IActionResult> Clone(byte ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Clones an existing Location to a new Location
        /// </summary>
        /// <param name="ID">ID of the Location to clone from</param>
        /// <param name="newName">New Location Name</param>
        /// <returns></returns>
        [HttpPost("{ID}/Clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Location does not exist or the cloning fails.")]
        public async Task<IActionResult> Clone(byte ID, [FromQuery] string newName)
        {
            if (String.IsNullOrWhiteSpace(newName))
                return await base.Clone(ID);
            
            try
            {
                var resp = await this._service.CloneAsync(ID, newName, this.User.UserLinkID());
                if (resp != null)
                    return new OkObjectResult(resp);
                else
                    return new BadRequestResult();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("not found"))
                    return new NotFoundResult();
                else
                    return new BadRequestObjectResult(ex.Message);
            }
        }

        /// <summary>
        /// Deletes a Location by ID
        /// </summary>
        /// <param name="ID">ID of the Location to Delete</param>
        /// <returns></returns>
        [Obsolete]
        [HttpDelete("{ID}/Obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Location does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(byte ID)
        {
            return await base.Delete(ID);
        }

        /// <summary>
        /// Deletes a Location by ID
        /// </summary>
        /// <param name="ID">ID of the Location to Delete</param>
        /// <param name="force"></param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [HasSecurityRight(SecurityRight.AccessModuleDevOps)]
        [SwaggerOperationFilter(typeof(IgnoreHttpRequestParameterOperationFilter))]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Location does not exist or the deletion fails.")]
        public async Task<IActionResult> DeleteWithForce(byte ID, bool force = false)
        {
            if (force)
            {
                var resp = await _service.ForceDelete(ID, User.UserLinkID().Value);
                return resp.ToResult();
            }
            return await base.Delete(ID);
        }

        #endregion

        /// <summary>
        /// Get simple list of Locations
        /// </summary>
        /// <returns>SimpleLocationData[]</returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleLocationData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleLocationData[]> FilteredSimpleList([FromQuery] SimpleListItemIsActiveFilter<SimpleLocationData, byte> filters)
        {
            return await this.GetSimpleList(User.BID().Value, filters);
        }
        
        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [Route("obsoletesimplelist")]
        [Obsolete]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleLocationData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleLocationData[]> SimpleList()
        {
            return await this.GetSimpleList<SimpleLocationData, byte>(User.BID().Value);
        }

        /// <summary>
        /// Sets a Business Location to the Default Business Location
        /// </summary>
        /// <param name="ID">Location Id</param>
        /// <returns></returns>
        [Route("{ID}/action/setdefault")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Location does not exist or fails to set as default.")]
        public async Task<IActionResult> SetDefault(byte ID)
        {
            EntityActionChangeResponse resp = await this._service.SetDefault(User.BID().Value, ID);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Business Location to to Active
        /// </summary>
        /// <param name="ID">Location Id</param>
        /// <returns></returns>
        [Route("{ID}/action/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<byte>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<byte>), description: "If the Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<byte>), description: "If the Location does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(byte ID)
        {
            EntityActionChangeResponse<byte> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Business Location to Inactive
        /// </summary>
        /// <param name="ID">Location Id</param>
        /// <returns></returns>
        [Route("{ID}/action/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<byte>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<byte>), description: "If the Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<byte>), description: "If the Location does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(byte ID)
        {
            EntityActionChangeResponse<byte> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets multiple Business Locations to Active
        /// </summary>
        /// <param name="ids">An array of Location Ids</param>
        /// <returns></returns>
        [Route("action/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntitiesActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntitiesActionChangeResponse), description: "If the Location(s) do not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntitiesActionChangeResponse), description: "If the Location(s) do not exist or fail to set as active.")]
        public async Task<IActionResult> SetActiveByIds([FromBody] byte[] ids)
        {
            EntitiesActionChangeResponse resp = await this._service.SetMultipleActive(ids, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets multiple Business Locations to Inactive
        /// </summary>
        /// <param name="ids">Array of Location Ids</param>
        /// <returns></returns>
        [Route("action/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntitiesActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntitiesActionChangeResponse), description: "If the Location(s) do not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntitiesActionChangeResponse), description: "If the Location(s) do not exist or fail to set as inactive.")]
        public async Task<IActionResult> SetInactiveByIds([FromBody] byte[] ids)
        {
            EntitiesActionChangeResponse resp = await this._service.SetMultipleActive(ids, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets the specified email account as the default for the location.
        /// </summary>
        /// <param name="ID">Location's ID</param>
        /// <param name="emailAccountID">EmailAccountData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setdefault/emailaccount/{emailAccountID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(BooleanResponse), description: "If the Location or EmailAccount does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Forbidden, description: "If the Email Account is currently inactive.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the Location fails to set the Default EmailAccount.")]
        public async Task<IActionResult> SetDefaultForLocation(byte ID, short emailAccountID)
        {
            return (await _service.SetDefaultEmailAccount(ID, emailAccountID)).ToResult();
        }

        /// <summary>
        /// Clears Location default email account
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <param name="employeeID">=</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/cleardefault/emailaccount")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(BooleanResponse), description: "If the EmailAccountData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the Location fails to clear the Default EmailAccount.")]
        public async Task<IActionResult> ClearDefaultEmailAccount(byte ID)
        {
            return (await _service.ClearDefaultEmailAccount(ID)).ToResult();
        }


        /// <summary>
        /// Try's to validate the data model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            if (model == null || !(model is LocationData))
            {
                this.ModelState.AddModelError("model", "model is not an location");
                return false;
            }

            LocationData location = (LocationData)model;
            if (String.IsNullOrWhiteSpace(location.LegalName))
            {
                this.ModelState.AddModelError("LegalName", "Location Name is required");
            }

            if (String.IsNullOrWhiteSpace(location.Abbreviation))
            {
                this.ModelState.AddModelError("Abbreviation", "Location Abbreviation is required");
            }

            if (!String.IsNullOrWhiteSpace(location.InvoicePrefix) && location.InvoicePrefix.Length > 4)
            {
                this.ModelState.AddModelError("InvoicePrefix", "Invoice Prefix must be 4 characters or less");
            }

            if (!String.IsNullOrWhiteSpace(location.EstimatePrefix) && location.EstimatePrefix.Length > 4)
            {
                this.ModelState.AddModelError("EstimatePrefix", "Estimate Prefix must be 4 characters or less");
            }

            if (!String.IsNullOrWhiteSpace(location.POPrefix) && location.POPrefix.Length > 4)
            {
                this.ModelState.AddModelError("POPrefix", "PO Prefix must be 4 characters or less");
            }

            var existingNameOwners = this._service.ExistingNameOwners(location);
            if (existingNameOwners.Count() > 0)
            {
                this.ModelState.AddModelError("Name", $"A Location named {location.Name} already exists.");
            }

            if(this._service.AbbreviationExists(location.Abbreviation ?? "", location.ID))
            {
                this.ModelState.AddModelError("Abbreviation", $"A Location with Abbreviation \"{location.Abbreviation}\" already exists.");
            }

            base.TryValidateModel(model, prefix);
            this.ModelState.Remove("create.Name");
            return this.ModelState.IsValid;
        }
    }
}
