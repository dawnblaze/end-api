﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// This is a CRM TaxabilityCode Controller
    /// </summary>
    [Route("api/taxcode")]
    public class TaxabilityCodeController : CRUDController<TaxabilityCode, TaxabilityCodeService, short>, ISimpleListableViewController<SimpleTaxabilityCode, short>
    {
        /// <summary>
        /// Listable service for TaxabilityCodes
        /// </summary>
        public ISimpleListableViewService<SimpleTaxabilityCode, short> ListableService => this._service;

        /// <summary>
        /// TaxabilityCode API Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public TaxabilityCodeController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {

        }

        #region Overridden CRUD Methods


        /// <summary>
        /// Returns all of the TaxabilityCodes
        /// </summary>
        /// <param name="filters">Query parameters to filter the TaxabilityCodes</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxabilityCode[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public Task<IActionResult> Get([FromQuery] TaxabilityCodeFilters filters)
        {
            return base.FilteredReadWith(filters.WherePredicate(), null);
        }

        /// <summary>
        /// Returns all of the TaxabilityCodes
        /// </summary>
        /// <returns></returns>
        [HttpGet("Obsolete")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single TaxabilityCode by ID
        /// </summary>
        /// <param name="ID">TaxabilityCode ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxabilityCode))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the TaxabilityCode does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetOneWithIncludes([FromRoute] short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Returns a single TaxabilityCode by ID
        /// </summary>
        /// <param name="ID">TaxabilityCode ID</param>
        /// <returns></returns>
        [HttpGet("obsolete/{ID}")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> ReadById(short ID)
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single TaxabilityCode by ID
        /// </summary>
        /// <param name="ID">TaxabilityCode ID</param>
        /// <param name="update">Updated TaxabilityCode data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxabilityCode))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the TaxabilityCode does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the TaxabilityCode does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] TaxabilityCode update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new TaxabilityCode
        /// </summary>
        /// <param name="newModel">New TaxabilityCode data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxabilityCode))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the TaxabilityCode creation fails")]
        public override async Task<IActionResult> Create([FromBody] TaxabilityCode newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing TaxabilityCode to a new TaxabilityCode
        /// </summary>
        /// <param name="ID">ID of the TaxabilityCode to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TaxabilityCode))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source TaxabilityCode does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source TaxabilityCode does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes a TaxabilityCode by ID
        /// </summary>
        /// <param name="ID">ID of the TaxabilityCode to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the TaxabilityCode does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the TaxabilityCode does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            BooleanResponse resp = await this._service.CanDelete((short)ID);
            if (resp?.Success == true)
            {
                if (resp.Value.HasValue && resp.Value.Value && resp.Message.Equals("Not found"))
                    return NoContent();
                else if (resp.Value.HasValue && resp.Value.Value)
                    return await base.Delete(ID);
                else if (resp.Value.HasValue && !resp.Value.Value && resp.Message.Equals("Unable to Delete Record - Reference by the following Tables: [Accounting.Tax.Code.ItemExemptionLink]"))
                    return await base.Delete(ID);
                else
                    return new BadRequestObjectResult(resp);
            }
            else
            {
                return new BadRequestObjectResult(resp);
            }
        }

        #endregion

        #region Actions

        /// <summary>
        /// Sets a TaxabilityCode to to Active
        /// </summary>
        /// <param name="ID">TaxabilityCode ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the TaxabilityCode does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the TaxabilityCode does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a TaxabilityCode to to Inactive
        /// </summary>
        /// <param name="ID">TaxabilityCode ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the TaxabilityCode does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the TaxabilityCode does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the TaxabilityCode can be deleted.
        /// </summary>
        /// <param name="ID">TaxabilityCode ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Gets a Simple list of TaxabilityCodes
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleTaxabilityCode[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleTaxabilityCode[]> FilteredSimpleList([FromQuery] SimpleListItemIsActiveFilter<SimpleTaxabilityCode, short> filters)
        {
            return await this.GetSimpleList(User.BID().Value, filters);
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsoletesimplelist")]
        [Obsolete]
        public Task<SimpleTaxabilityCode[]> SimpleList()
        {
            return this.FilteredSimpleList(null);
        }

        /// <summary>
        /// Links a TaxItem into Taxability Code
        /// </summary>
        /// <param name="ID">Taxability Code ID</param>
        /// <param name="taxItemID">Tax Item ID</param>
        /// <returns></returns>
        [Route("{ID}/action/linkexempttaxitem/{taxItemID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> LinkExemptTaxItem(short ID, short taxItemID)
        {
            return (await this._service.LinkTaxItem(ID, taxItemID)).ToResult();
        }

        /// <summary>
        /// Unlinks a TaxItem into Taxability Code
        /// </summary>
        /// <param name="ID">Taxability Code ID</param>
        /// <param name="taxItemID">Tax Item ID</param>
        /// <returns></returns>
        [Route("{ID}/action/unlinkexempttaxitem/{taxItemID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> UnlinkExemptTaxItem(short ID, short taxItemID)
        {
            return (await this._service.UnlinkTaxItem(ID, taxItemID)).ToResult();
        }

        #endregion

    }
}
