﻿using Endor.Api.Web.Services;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Endor.Security;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Api.Web.Classes;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// The controller for BusinessData
    /// </summary>
    [Route("api/business")]
    public class BusinessController : CRUDController<BusinessData, BusinessService, short>
    {
        private readonly ApiContext _context;
        private readonly EndorOptions _options;
        private readonly ITenantDataCache _cache;

        /// <summary>
        /// Api Endpoint for Business
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="options"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public BusinessController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, EndorOptions options, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
            _context = context;
            _options = options;
            _cache = cache;
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns the logged in user's Business
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BusinessData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Read()
        {
            if (User.BID() == null)
                return Unauthorized();

            return await _service.RetrieveByBID(User.BID().Value);
        }

        /// <summary>
        /// Returns a single Business by BID
        /// </summary>
        /// <param name="ID">Business ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BusinessData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Business does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not belong to the Business requested or is not logged in")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            if (User.BID() == null)
                return Unauthorized();

            if (User.BID().Value != ID && !isUserAuthorizedForAccessType(UserAccessType.SupportStaff))
                return Unauthorized();

            return await _service.RetrieveByBID(ID);
        }

        /// <summary>
        /// Updates a single Business by ID
        /// </summary>
        /// <param name="ID">Business ID</param>
        /// <param name="update">Updated Business data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BusinessData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Business does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If the business to update is not the user's business, or the user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Business does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] BusinessData update)
        {
            if (User.BID() == null)
                return Unauthorized();

            if (User.BID().Value == ID && !isUserAuthorizedForAccessType(UserAccessType.EmployeeAdministrator))
                return Unauthorized();

            if (User.BID().Value != ID && !isUserAuthorizedForAccessType(UserAccessType.SupportStaff))
                return Unauthorized();

            var resp = await _service.DoUpdate(ID, update, Request.Headers[ConnectionIDHeaderKey]);
            if (!resp.Success)
                return resp.ToResult();
            else
                return new OkObjectResult(resp.Data);
        }

        /// <summary>
        /// Update Business with new Association type
        /// </summary>
        /// <param name="newAssociationType">number</param>
        /// <param name="ID">Business ID</param>
        /// <returns>IGenericResponse</returns>
        [HttpPut("{ID}/AssociationType/{newAssociationType}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(IGenericResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Business does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If the business to update is not the user's business, or the user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Business does not exist or the update fails.")]
        public async Task<IActionResult> UpdateAssociationType(byte? newAssociationType, short ID)
        {
            if (User.BID() == null || User.BID().Value != ID)
                return Unauthorized();

            return (await this._service.UpdateAssociationType(newAssociationType, ID)).ToResult();
        }

        /// <summary>
        /// Creates a new Business
        /// </summary>
        /// <param name="newModel">New Business data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [Obsolete]
        [HttpPost("obselete")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotImplemented, description: "Creating Businesses is not yet implemented.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If the user does not have enough rights to Create a business.")]
        public override async Task<IActionResult> Create([FromBody] BusinessData newModel, [FromQuery] Guid? tempID = null)
        {
            if (User.BID() == null)
                return Unauthorized();

            if (!isUserAuthorizedForAccessType(UserAccessType.SupportManager))
                return Unauthorized();
            
            return await Task.FromResult(StatusCode((int)HttpStatusCode.NotImplemented));
        }

        /// <summary>
        /// Creates a new Business
        /// </summary>
        /// <param name="TemplateBID">Template BID</param>
        /// <param name="FirstName">The first name of the owner</param>
        /// <param name="LastName">The last name of the owner</param>
        /// <param name="EmailAddress">The email address of the owner.  The new login link will be sent to this email address</param>
        /// <param name="LocationName">The name of the first location for the Business</param>
        /// <param name="NewBusinessID">The BID to use</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.NotImplemented, description: "Creating Businesses is not yet implemented.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If the user does not have enough rights to Create a business.")]
        public async Task<IActionResult> CreateBusiness([FromQuery] short TemplateBID, [FromQuery] string FirstName, [FromQuery] string LastName, [FromQuery] string EmailAddress, [FromQuery] string LocationName, [FromQuery] short? NewBusinessID)
        {
            if (User.BID() == null)
                return Unauthorized();

            if (!isUserAuthorizedForAccessType(UserAccessType.SupportManager))
                return Unauthorized();

            return (await this._service.CreateBusinessAsync(TemplateBID, FirstName, LastName, EmailAddress, LocationName, NewBusinessID, _options, _cache, _migrationHelper)).ToResult();
        }


        /// <summary>
        /// Clones an existing Business to a new Business
        /// </summary>
        /// <param name="ID">ID of the Business to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotImplemented, description: "Cloning Businesses is not yet implemented.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If the user does not have enough rights to Create a business.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            if (User.BID() == null)
                return Unauthorized();

            if (!isUserAuthorizedForAccessType(UserAccessType.SupportManager))
                return Unauthorized();

            return (await this._service.CloneAsync(ID, _options, _cache)).ToResult();
        }

        /// <summary>
        /// Obsolete Delete
        /// </summary>
        /// <param name="ID">ID of the Business to Delete</param>
        /// <returns></returns>
        [Obsolete]
        [HttpDelete("obselete/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(IGenericResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Business does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user has not enough rights to delete the Businesses.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Business does not exist or the delete fails.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotImplemented, description: "If the endpoint has not yet been implemented.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            if (!isUserAuthorizedForAccessType(UserAccessType.SupportManager))
                return Unauthorized();

            return (await this._service.DeleteAsync(ID, _options, _cache, false)).ToResult();
        }

        /// <summary>
        /// Deletes a Business by ID with an optional TestOnly query param
        /// </summary>
        /// <param name="BID">Business ID</param>
        /// <param name="TestOnly">When true it rollback delete otherewise it's deleted</param>
        /// <returns>IGenericResponse</returns>
        [HttpDelete("{BID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(IGenericResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Business does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If the business to update is not the user's business, or the user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Business does not exist or the update fails.")]
        public async Task<IActionResult> Delete(short BID, [FromQuery] bool TestOnly = true)
        {
            if (!isUserAuthorizedForAccessType(UserAccessType.SupportManager))
                return Unauthorized();

            return (await this._service.DeleteAsync(BID, _options, _cache, TestOnly)).ToResult();
        }

        /// <summary>
        /// Returns a Simple List Collection of TimeZone Enums that are linked to this business for easy access.
        /// </summary>
        /// <returns></returns>
        [Route("timezone/simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleEnumTimeZone[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<List<SimpleEnumTimeZone>> TimezoneSimpleList()
        {

            return await _service.GetTimezoneSimpleListAsync(User.BID().Value);
        }

        #endregion

        /// <summary>
        /// link the specified Time Zone to Business
        /// </summary>
        /// <param name="TimeZoneID">TimeZone ID</param>
        /// <returns></returns>
        [HttpPost("action/link/timezone/{TimeZoneID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(IGenericResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Specified ID does not exists.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If the business to update is not the user's business, or the user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Business does not exist or the update fails.")]
        public async Task<IActionResult> LinkBusinessTimeZone([FromRoute] short TimeZoneID)
        {
            if (User.BID() == null)
                return Unauthorized();

            //check if timezone exists in timezone enums, otherwise return NotFound as described in wiki
            EnumTimeZoneService enumTimeZoneService = new EnumTimeZoneService(this._context, this._migrationHelper);
            var timeZone = enumTimeZoneService.SimpleEnumListSet.Where(x => x.ID == TimeZoneID).FirstOrDefault();
            if (timeZone == null)
            {
                return NotFound("Specified ID does not exists");
            }

            return (await this._service.LinkTimeZoneToBusiness(User.BID().Value, TimeZoneID)).ToResult();
        }


        /// <summary>
        /// Unlinks specified Time Zone to Business
        /// </summary>
        /// <param name="TimeZoneID">TimeZone ID</param>
        /// <returns></returns>
        [HttpPost("action/unlink/timezone/{TimeZoneID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(IGenericResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Specified ID does not exists.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If the business to update is not the user's business, or the user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Business does not exist or the update fails.")]
        public async Task<IActionResult> UnlinkBusinessTimeZone([FromRoute] short TimeZoneID)
        {
            if (User.BID() == null)
                return Unauthorized();

            //check if timezone exists in timezone enums, otherwise return NotFound as described in wiki
            EnumTimeZoneService enumTimeZoneService = new EnumTimeZoneService(this._context, this._migrationHelper);
            var timeZone = enumTimeZoneService.SimpleEnumListSet.Where(x => x.ID == TimeZoneID).FirstOrDefault();
            if (timeZone == null)
            {
                return NotFound("Specified ID does not exists");
            }

            return (await _service.UnlinkTimeZoneToBusiness(User.BID().Value, TimeZoneID)).ToResult();
        }

        #region Actions Endpoints

        /// <summary>
        /// Sets a Business to Active
        /// </summary>
        /// <param name="ID">Business ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the Business does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the Business does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            if (!isUserAuthorizedForAccessType(UserAccessType.SupportStaff))
                return Unauthorized();

            this._service.setOptions(_options);
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Business to to Inactive
        /// </summary>
        /// <param name="ID">Business ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the Business does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the Business does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            if (!isUserAuthorizedForAccessType(UserAccessType.SupportStaff))
                return Unauthorized();

            this._service.setOptions(_options);
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the Business can be deleted.
        /// </summary>
        /// <param name="ID">Business ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            if (!isUserAuthorizedForAccessType(UserAccessType.SupportManager))
                return await Task.FromResult(new OkObjectResult(new BooleanResponse()
                {
                    Success = true,
                    Value = false,
                    Message = "Insufficient rights to Delete a Business."
                }));
            else
                return await Task.FromResult(new OkObjectResult(new BooleanResponse()
                {
                    Success = true,
                    Value = true,
                    Message = "You can Delete the Business."
                }));
        }

        #endregion
    }
}
