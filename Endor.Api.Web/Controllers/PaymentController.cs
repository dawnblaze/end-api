﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Request;
using Endor.Api.Web.Classes.Responses;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Endor.Tenant;
using System.Net;

using Endor.Api.Web.Annotation;
using Swashbuckle.AspNetCore;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Payment controller to handle the payments being submitted as well as the listing of payments recorded
    /// </summary>
    [Route("api/payment")]
    [Authorize]
    public class PaymentController : GenericController<PaymentMaster, PaymentService, int>
    {
        private (short? userLink, string connectionID) GetUserLinkAndConnection()
        {
            var userLink = this.User.UserLinkID();
            var connectionID = Request.Headers[ConnectionIDHeaderKey].FirstOrDefault();
            return (userLink, connectionID);
        }
        /// <summary>
        /// 
        /// </summary>
        public const string ConnectionIDHeaderKey = "ConnectionID";

        /// <summary>
        /// Constructs a payment Controller
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="rtmClient"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public PaymentController(ApiContext context, RemoteLogger logger, ITaskQueuer taskQueuer, IRTMPushClient rtmClient, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, taskQueuer, rtmClient, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Post a new payment
        /// </summary>
        /// <param name="Capture"></param>
        /// <param name="paymentRequest"></param>
        [HttpPost("payment")]
        public async Task<IActionResult> PostPayment([FromBody] PaymentRequest paymentRequest, [FromQuery] bool Capture = false)
        {
            try
            {
                this._service.ServiceModelState = new ServiceModelState(ModelState);

                var info = GetUserLinkAndConnection();

                var response = await this._service.CreatePaymentTransactionAsync(paymentRequest, null, info.userLink, info.connectionID, Capture);

                if (response != null && ModelState.IsValid)
                    return Ok(response);

                return BadRequest(ModelState);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e);
            }
        }


        /// <summary>
        /// Get Total Payment Due of orders of a company's location
        /// </summary>
        /// <param name="companyID">company ID</param>
        /// <param name="locationID">location ID</param>
        /// <returns></returns>
        [HttpGet("TotalPaymentDue/{companyID}/{locationID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(decimal))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(decimal))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> CompanyLocationTotalPaymentDue(int companyID, int locationID)
        {
            try
            {
                var response = await this._service.GetCompanyLocationTotalPaymentDue(companyID, locationID);
                return Ok(response.GetValueOrDefault());
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e);
            }
        }

        /// <summary>
        /// Performs a refund
        /// </summary>
        /// <param name="paymentRefundRequest">Payment request info</param>
        /// <param name="UseNewPaymentInfo">If the UseNewPaymentInfo parameter is true (defaults false), new payment information must be provided and the payment is refunded to the payment information provided.  Otherwise, the payment information for the Master Payment is used.</param>
        /// <param name="Capture">Captures the request</param>
        /// <returns>Returns Payment response</returns>
        [HttpPost("refund")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> RefundRequest([FromBody] PaymentRefundRequest paymentRefundRequest, [FromQuery] bool UseNewPaymentInfo = false, [FromQuery] bool Capture = false)
        {
            try
            {
                this._service.ServiceModelState = new ServiceModelState(ModelState);
                if (paymentRefundRequest.PaymentTransactionType != PaymentTransactionType.Refund)
                {
                    return new BadRequestObjectResult("Payment Transaction Type must be of type Refund");
                }

                PaymentRequest paymentRequest = new PaymentRequest()
                {
                    PaymentTransactionType = (int)paymentRefundRequest.PaymentTransactionType,
                    Amount = paymentRefundRequest.Amount,
                    CurrencyType = paymentRefundRequest.CurrencyType,
                    Applications = paymentRefundRequest.Applications,
                    CompanyID = paymentRefundRequest.CompanyID
                };
                if (UseNewPaymentInfo)
                {
                    //paymentRequest.PaymentMethodID = paymentRefundRequest.PaymentMethodID.GetValueOrDefault();
                    //paymentRequest.PaymentType = paymentRefundRequest.PaymentType.GetValueOrDefault();
                    paymentRequest.DisplayNumber = paymentRefundRequest.DisplayNumber;
                    paymentRequest.ProcessorInfo = paymentRefundRequest.ProcessorInfo;
                }
                var info = GetUserLinkAndConnection();

                PaymentResponse response = null;
                if (paymentRefundRequest.MasterPaymentID.HasValue)
                {
                    response = await this._service.CreatePaymentTransactionAsync(paymentRequest, paymentRefundRequest.MasterPaymentID, info.userLink, info.connectionID, Capture);
                } else
                {
                    if (paymentRequest.CompanyID == 0) return new BadRequestObjectResult("CompanyID must be specified if MasterPaymentID is not defined");
                    response = new PaymentResponse()
                    {
                        PaymentTransactionType = (byte)paymentRequest.PaymentTransactionType,
                        Amount = paymentRequest.Amount,
                        CurrencyType = paymentRequest.CurrencyType,
                        TokenID = null,
                        DisplayNumber = paymentRequest.DisplayNumber,
                        MasterPayments = await this._service.CreatePaymentTransactionAsyncWithoutMasterPayment(paymentRequest, info.userLink, info.connectionID)
                    };
                }

                if (ModelState.Keys.Contains("Not Found"))
                    return NotFound(ModelState);

                if (response != null && ModelState.IsValid)
                    return Ok(response);

                return BadRequest(ModelState);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e);
            }
        }

        /// <summary>
        /// Post a new payment
        /// </summary>
        [HttpPost("creditgift")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> PostCreditGift([FromBody] CreditGiftRequest creditGiftRequest)
        {
            this._service.ServiceModelState = new ServiceModelState(ModelState);

            if (creditGiftRequest.PaymentTransactionType != PaymentTransactionType.Credit_Gift)
            {
                return new BadRequestObjectResult("Payment Transaction Type must be of type Credit Gift");
            }

            if (creditGiftRequest.PaymentMethodId != 251) //NonRefundable Credit
            {
                return new BadRequestObjectResult("Payment Method Type must be Non-Refundable Credit");
            }

            try
            {
                PaymentRequest paymentRequest = new PaymentRequest()
                {
                    PaymentTransactionType = (int)creditGiftRequest.PaymentTransactionType,
                    Amount = creditGiftRequest.Amount,
                    CurrencyType = creditGiftRequest.CurrencyType,
                    Applications = creditGiftRequest.Applications,
                    Notes = creditGiftRequest.Notes,
                    CompanyID = creditGiftRequest.CompanyID,
                    LocationID = creditGiftRequest.LocationID,
                    ReceivedLocationID = creditGiftRequest.ReceivedLocationID
                };
                var info = GetUserLinkAndConnection();

                var response = await this._service.CreatePaymentTransactionAsync(paymentRequest, null, info.userLink, info.connectionID, false);

                if (ModelState.Keys.Contains("Not Found"))
                    return NotFound(ModelState);

                if (response != null && ModelState.IsValid)
                    return Ok(response);

                return BadRequest(ModelState);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e);
            }
        }

        /// <summary>
        /// Post Transfer Credit Request
        /// </summary>
        /// <param name="paymentTransferCreditRequest"></param>
        /// <returns></returns>
        [HttpPost("transfertocredit")]
        public async Task<IActionResult> PostTransferCredit([FromBody] PaymentTransferCreditRequest paymentTransferCreditRequest)
        {
            this._service.ServiceModelState = new ServiceModelState(ModelState);

            if (paymentTransferCreditRequest.PaymentTransactionType != PaymentTransactionType.Refund_to_Refundable_Credit &&
                paymentTransferCreditRequest.PaymentTransactionType != PaymentTransactionType.Refund_to_Nonrefundable_Credit)
            {
                return new BadRequestObjectResult("Payment Transaction Type must be of type Refund to Credit");
            }

            if ((paymentTransferCreditRequest.PaymentMethodId != 250) && (paymentTransferCreditRequest.PaymentMethodId != 251)) //Refundable Credit
            {
                return new BadRequestObjectResult("Payment Method Type must be Refundable or Non-Refundable Credit");
            }

            try
            {
                PaymentRequest paymentRequest = new PaymentRequest()
                {
                    CompanyID = paymentTransferCreditRequest.CompanyID,
                    PaymentTransactionType = (int)paymentTransferCreditRequest.PaymentTransactionType,
                    Applications = paymentTransferCreditRequest.Applications,
                    Notes = paymentTransferCreditRequest.Notes,
                };
                var info = GetUserLinkAndConnection();
                if (paymentTransferCreditRequest.PaymentTransactionType == PaymentTransactionType.Refund_to_Refundable_Credit)
                    paymentRequest.PaymentMethodID = (int)PaymentMethodType.RefundableCredit;
                else paymentRequest.PaymentMethodID = (int)PaymentMethodType.NonRefundableCredit;
                PaymentResponse response = null;
                if (paymentTransferCreditRequest.MasterPaymentID.HasValue)
                {
                    response = await this._service.CreatePaymentTransactionAsync(paymentRequest, paymentTransferCreditRequest.MasterPaymentID, info.userLink, info.connectionID, false);
                } else
                {
                    response = new PaymentResponse()
                    {
                        PaymentTransactionType = (byte)paymentRequest.PaymentTransactionType,
                        Amount = paymentRequest.Amount,
                        CurrencyType = paymentRequest.CurrencyType,
                        TokenID = null,
                        DisplayNumber = paymentRequest.DisplayNumber,
                        MasterPayments = await this._service.CreatePaymentTransactionAsyncWithoutMasterPayment(paymentRequest, info.userLink, info.connectionID)
                    };
                }

                if (response != null && ModelState.IsValid)
                    return Ok(response);

                if (ModelState.Keys.Contains("Not Found"))
                    return NotFound(ModelState);

                return BadRequest(ModelState);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e);
            }
        }

        /// <summary>
        /// Applies Credit
        /// </summary>
        /// <param name="paymentApplyCreditRequest">Payment request info</param>
        [HttpPost("applycredit")]
        public async Task<IActionResult> PostApplyCredit([FromBody] PaymentApplyCreditRequest paymentApplyCreditRequest)
        {
            this._service.ServiceModelState = new ServiceModelState(ModelState);

            if (paymentApplyCreditRequest.PaymentTransactionType != PaymentTransactionType.Payment_from_Refundable_Credit
                && paymentApplyCreditRequest.PaymentTransactionType != PaymentTransactionType.Payment_from_Nonrefundable_Credit)
            {
                return new BadRequestObjectResult(new { Message = "Payment Transaction Type must be of type \"Payment from Refundable Credit\" or \"Payment from Nonrefundable Credit\"" });
            }

            if ((paymentApplyCreditRequest.PaymentMethodId != 250) && (paymentApplyCreditRequest.PaymentMethodId != 251)) //Refundable Credit
            {
                return new BadRequestObjectResult("Payment Method Type must be Refundable or Non-Refundable Credit");
            }

            try
            {
                PaymentRequest paymentRequest = new PaymentRequest()
                {
                    CompanyID = paymentApplyCreditRequest.CompanyID,
                    PaymentTransactionType = (int)paymentApplyCreditRequest.PaymentTransactionType,
                    Applications = paymentApplyCreditRequest.Applications,
                    Notes = paymentApplyCreditRequest.Notes
                };
                var info = GetUserLinkAndConnection();
                if (paymentApplyCreditRequest.PaymentTransactionType == PaymentTransactionType.Payment_from_Refundable_Credit)
                    paymentRequest.PaymentMethodID = (int)PaymentMethodType.RefundableCredit;
                else paymentRequest.PaymentMethodID = (int)PaymentMethodType.NonRefundableCredit;

                PaymentResponse response = null;
                if (paymentApplyCreditRequest.MasterPaymentID.HasValue)
                {
                    response = await this._service.CreatePaymentTransactionAsync(paymentRequest, paymentApplyCreditRequest.MasterPaymentID, info.userLink, info.connectionID, false);
                }
                else
                {
                    if (paymentRequest.CompanyID==0) return new BadRequestObjectResult("CompanyID must be specified if MasterPaymentID is not defined");
                    response = new PaymentResponse()
                    {
                        PaymentTransactionType = (byte)paymentRequest.PaymentTransactionType,
                        Amount = paymentRequest.Amount,
                        CurrencyType = paymentRequest.CurrencyType,
                        TokenID = null,
                        DisplayNumber = paymentRequest.DisplayNumber,
                        MasterPayments = await this._service.CreatePaymentTransactionAsyncWithoutMasterPayment(paymentRequest, info.userLink, info.connectionID)
                    };
                }


                if (response != null && ModelState.IsValid)
                    return Ok(new PaymentApplyCreditResponse()
                    {
                        PaymentTransactionType = (PaymentTransactionType)response.PaymentTransactionType,
                        MasterPayments = response.MasterPayments
                    });

                if (ModelState.Keys.Contains("Not Found"))
                    return NotFound(ModelState);

                return BadRequest(ModelState);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e);
            }
        }


        /// <summary>
        /// Performs a credit adjustment
        /// </summary>
        /// <param name="creditAdjustmentRequest">Payment request info</param>
        /// <returns>Returns Payment response</returns>
        [HttpPost("creditadjustment")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> CreditAdjustmentRequest([FromBody] PaymentCreditAdjustmentRequest creditAdjustmentRequest)
        {
            this._service.ServiceModelState = new ServiceModelState(ModelState);
            if (creditAdjustmentRequest.PaymentTransactionType != PaymentTransactionType.Credit_Adjustment)
            {
                return new BadRequestObjectResult("Payment Transaction Type must be of type Credit Gift");
            }
            if (creditAdjustmentRequest.PaymentMethodId != 251) //NonRefundable Credit
            {
                return new BadRequestObjectResult("Payment Method Type must be Non-Refundable Credit");
            }
            try
            {
                PaymentRequest paymentRequest = new PaymentRequest()
                {
                    CompanyID = creditAdjustmentRequest.CompanyID,
                    PaymentTransactionType = (int)creditAdjustmentRequest.PaymentTransactionType,
                    Amount = creditAdjustmentRequest.Amount,
                    CurrencyType = creditAdjustmentRequest.CurrencyType,
                    Applications = creditAdjustmentRequest.Applications,
                    Notes = creditAdjustmentRequest.Notes
                };
                var info = GetUserLinkAndConnection();

                PaymentResponse response = null;
                if (creditAdjustmentRequest.MasterPaymentID.HasValue)
                {
                    response = await this._service.CreatePaymentTransactionAsync(paymentRequest, creditAdjustmentRequest.MasterPaymentID, info.userLink, info.connectionID, false);
                } else
                {
                    if (paymentRequest.CompanyID == 0) return new BadRequestObjectResult("CompanyID must be specified if MasterPaymentID is not defined");
                    response = new PaymentResponse()
                    {
                        PaymentTransactionType = (byte)paymentRequest.PaymentTransactionType,
                        Amount = paymentRequest.Amount,
                        CurrencyType = paymentRequest.CurrencyType,
                        TokenID = null,
                        DisplayNumber = paymentRequest.DisplayNumber,
                        MasterPayments = await this._service.CreatePaymentTransactionAsyncWithoutMasterPayment(paymentRequest, info.userLink, info.connectionID)
                    };
                }

                if (response != null && ModelState.IsValid)
                    return Ok(response);

                if (ModelState.Keys.Contains("Not Found"))
                    return NotFound(ModelState);

                return BadRequest(ModelState);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e);
            }
        }

        /// <summary>
        /// Returns Company Credit Balances for a company
        /// </summary>
        /// <param name="companyid">Id of Company</param>
        /// <returns></returns>
        [HttpGet("availablecredit/{companyid}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public IActionResult AvailableCreditRequest(int companyid)
        {
            try
            {
                var response = this._service.GetAvailableCredit(companyid);
                return Ok(response);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e);
            }
        }
    }
}