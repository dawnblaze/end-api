﻿using Endor.DocumentStorage.Models;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.SwaggerGen;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    public partial class DMController
    {
        /// <summary>
        /// Customer Shared Name
        /// </summary>
        public const string CUSTOMER_SHARED = "CUSTOMER SHARED";
        /// <summary>
        /// Gets a document list for Entity level buckets
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="400">
        /// ID missing value
        /// CTID missing value
        /// ClassFolder has value
        /// Bucket is not Documents or Reports
        /// </response>
        [HttpGet(bucket_ctid_id_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(List<DMItem>))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:
            @"
            ID missing value
            CTID missing value
            ClassFolder has value
            Bucket is not Documents or Reports
            ")]
        public async Task<IActionResult> GetEntityLevelDocuments(BucketRequest bucket, int ctid, int id)
        {
            RequestModel requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, id = id };

            if (!requestModel.IsEntityLevel || !(requestModel.Bucket == BucketRequest.Documents || requestModel.Bucket == BucketRequest.Reports))
            {
                return BadRequest("Invalid Bucket");
            }
            return await GetDocuments(requestModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        /// Gets a document list for Entity level buckets
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet(bucket_ctid_id_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(List<DMItem>))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:
            @"
            ID missing value
            CTID missing value
            ClassFolder has value
            Bucket is not Documents or Reports
            ")]
        public async Task<IActionResult> GetEntityLevelDocumentsWithPathAndFilename(BucketRequest bucket, int ctid, int id)
        {
            RequestModel requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, id = id };

            if (!requestModel.IsEntityLevel || !(requestModel.Bucket == BucketRequest.Documents || requestModel.Bucket == BucketRequest.Reports))
            {
                return BadRequest("Invalid Bucket");
            }
            return await GetDocuments(requestModel);
        }

        /// <summary>
        /// Gets a document list for ClassType level buckets
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="classFolder"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="400">
        /// CTID missing value
        /// ClassFolder missing value
        /// ClassFolder == 'static', and bucket is NOT Data or Reports
        /// ClassFolder == 'template', and bucket is NOT Data or Documents
        /// </response>
        [HttpGet(bucket_ctid_folder_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(List<DMItem>))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:
            @"
            CTID missing value
            ClassFolder missing value
            
            ClassFolder == 'static', and bucket is NOT Data or Reports
            ClassFolder == 'template', and bucket is NOT Data or Documents
            ")]
        public async Task<IActionResult> GetClassTypeLevelDocuments(BucketRequest bucket, int ctid, string classFolder)
        {
            RequestModel requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, classFolder = classFolder };
            if (!requestModel.IsClassTypeLevel)
            {
                return BadRequest("Invalid Bucket");
            }
            if (requestModel.classFolder == "static" && !(requestModel.Bucket == BucketRequest.Data || requestModel.Bucket == BucketRequest.Reports))
            {
                return BadRequest("Invalid Bucket");
            }
            else if (requestModel.classFolder == "template" && !(requestModel.Bucket == BucketRequest.Data || requestModel.Bucket == BucketRequest.Documents))
            {
                return BadRequest("Invalid Bucket");
            }
            return await GetDocuments(requestModel);
        }

        /// <summary>
        /// Gets a document list for temp buckets
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="400">GUID missing value</response>
        [HttpGet(bucket_temp_guid_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(List<DMItem>))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:
            @"
            GUID missing value
            ")]
        public async Task<IActionResult> GetTemporaryDocuments(BucketRequest bucket, Guid guid)
        {
            RequestModel requestModel = new RequestModel() { Bucket = bucket, guid = guid};
            if (!requestModel.IsTemporary)
            {
                return BadRequest("Invalid Bucket");
            }

            return await GetDocuments(requestModel);
        }

        internal async Task<IActionResult> GetDocuments(RequestModel requestModel)
        {
            var ctx = requestModel.GetStorageContext(User.BID().Value, User.AID());
            List<DMItem> results = await new DocumentManager(_cache, ctx).GetDocumentsAsync();

            if (results != null && results.Count() > 0)
            {
                results = await this._userLinkService.Value.GetDocumentCreatorEmployeeName(results);
            }

            return Ok(results);
        }

        /// <summary>
        /// Adds a new document to a class type level bucket and folder
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="classFolder"></param>
        /// <param name="pathAndFilename"></param>
        /// <param name="designation"></param>
        /// <param name="remoteurl"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="400">
        /// If template folder and bucket is NOT Documents or Data
        /// If static folder and bucket is NOT Reports or Data
        /// @PostCommonBadRequestResponseDescription
        /// </response>
        [HttpPost(bucket_ctid_folder_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DMItem))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:
            @"If template folder and bucket is NOT Documents or Data
            If static folder and bucket is NOT Reports or Data
            "+ PostCommonBadRequestResponseDescription)]
        public async Task<IActionResult> PostClassTypeLevel(BucketRequest bucket, int ctid, string classFolder, string pathAndFilename, [FromQuery]string designation, [FromQuery]string remoteurl = null)
        {
            RequestModel requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, classFolder = classFolder, PathAndFilename = pathAndFilename};

            if (requestModel.ctid.HasValue && requestModel.classFolder == "template" && !(requestModel.Bucket == BucketRequest.Documents || requestModel.Bucket == BucketRequest.Data))
            {
                return BadRequest("Invalid Bucket");
            }
            else if (requestModel.ctid.HasValue && requestModel.classFolder == "static" && !(requestModel.Bucket == BucketRequest.Reports || requestModel.Bucket == BucketRequest.Data))
            {
                return BadRequest("Invalid Bucket");
            }
            return await Post(requestModel, designation, remoteurl);
        }

        /// <summary>
        /// Adds a new document to a temporary bucket and folder
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="guid"></param>
        /// <param name="pathAndFilename"></param>
        /// <param name="designation"></param>
        /// <param name="remoteurl"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="400">
        /// If folder and bucket is the Data bucket
        /// @PostCommonBadRequestResponseDescription
        /// </response>
        [HttpPost(bucket_temp_guid_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DMItem))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:
            @"If folder and bucket is the Data bucket
            " + PostCommonBadRequestResponseDescription)]
        public async Task<IActionResult> PostTemporary(BucketRequest bucket, Guid guid, string pathAndFilename, [FromQuery]string designation, [FromQuery]string remoteurl = null)
        {
            RequestModel requestModel = new RequestModel() { Bucket = bucket, guid = guid, PathAndFilename = pathAndFilename };

            
            //commented out, not sure why this condition exists
            //as per spec https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/845578283/MessageBody+Object
            //MessageBody needs access to temp Data bucket
            // if (requestModel.IsTemporary && requestModel.Bucket == BucketRequest.Data)
            // {
            //     return BadRequest("Invalid Bucket");
            // }
            return await Post(requestModel, designation, remoteurl);
        }

        private const string PostCommonBadRequestResponseDescription =
            @"
            If pathAndFilename does not have a value
            If uploading a folder and pathandFileName contains a dot
            If no file, folder, or file from URL is specified.
            If the file from URL is not valid.";
        private const string PutCommonOKResponseDescription = 
            @"
            When renameName is passed, number of files renamed.
            When destination is passed, number of files moved.
            When designation is passed, DMItem of the document updated.
            When a file is passed, DMItem of the document that was replaced.
            ";
        private const string PutCommonBadRequestResponseDescription = @"
            If no files, no rename name, no destination, no designations.
            If passing files in body and destination; this combination is a 'replace and rename' which is not supported, use POST and DELETE instead.
            ";

        /// <summary>
        /// Adds a new document to a target bucket and folder
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="id"></param>
        /// <param name="pathAndFilename"></param>
        /// <param name="designation"></param>
        /// <param name="remoteurl"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="400">
        /// If bucket is NOT Documents or Data
        /// @PostCommonBadRequestResponseDescription
        /// </response>
        [HttpPost(bucket_ctid_id_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DMItem))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: 
            @"If bucket is NOT Documents or Data
            "+PostCommonBadRequestResponseDescription)]
        public async Task<IActionResult> Post(BucketRequest bucket, int ctid, int id, string pathAndFilename, [FromQuery]string designation, [FromQuery]string remoteurl = null)
        {
            RequestModel requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, id = id, PathAndFilename = pathAndFilename };

            return await Post(requestModel, designation, remoteurl);
        }

        /// <summary>
        /// Adds a new document to a target bucket and folder
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="designation"></param>
        /// <param name="remoteurl"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="400">
        /// If bucket is NOT Documents or Data
        /// @PostCommonBadRequestResponseDescription
        /// </response>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DMItem))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:
            @"If bucket is NOT Documents or Data
            " + PostCommonBadRequestResponseDescription)]
        public async Task<IActionResult> Post(RequestModel requestModel, string designation, string remoteurl)
        {
            bool addFileFromUrl = !String.IsNullOrWhiteSpace(remoteurl);
            bool addFileFromForm = Request.HasFormContentType && (Request.Form?.Files != null);
            bool addFolder = !addFileFromForm && !addFileFromUrl;

            if (String.Equals(requestModel.PathAndFilename, "_TemplateFiles.json", StringComparison.OrdinalIgnoreCase))
            {
                return BadRequest("Failed to create '" + requestModel.PathAndFilename + "'");
            }

            if (addFolder)
            {
                if (String.IsNullOrWhiteSpace(requestModel.PathAndFilename))
                {
                    //do not have a file, do not have a path and file name
                    return BadRequest("No files or folder name specified");
                }
                else if (requestModel.PathAndFilename.Contains('.'))
                {
                    return BadRequest("Folders cannot contain dots in name");
                }
                else if (requestModel.PathAndFilename.ToUpper() == CUSTOMER_SHARED)
                {
                    return BadRequest("Failed to create '" + requestModel.PathAndFilename + "' folder");
                }
            }
            else if (String.IsNullOrWhiteSpace(requestModel.PathAndFilename))
            {
                //have a file, do not have a path and file name
                return BadRequest("Cannot upload file without path and file name");
            }

            if (requestModel.id.HasValue && !(requestModel.Bucket == BucketRequest.Documents || requestModel.Bucket == BucketRequest.Data))
            {
                return BadRequest("Invalid Bucket");
            }

            var ctx = requestModel.GetStorageContext(User.BID().Value, User.AID());

            DMItem result;
            if (addFolder)
            {
                result = await new DocumentManager(_cache, ctx).AddFolder(requestModel.PathAndFilename, GetUploaderID(), GetUploaderCTID());
            }
            else if (addFileFromForm)
            {
                result = await new DocumentManager(_cache, ctx).UploadDocumentAsync(requestModel.PathAndFilename, Request.Form, designation, GetUploaderID(), GetUploaderCTID());
            }
            else if (addFileFromUrl)
            {
                try
                {
                    result = await new DocumentManager(_cache, ctx).AddDocumentFromUrlAsync(requestModel.PathAndFilename, remoteurl, designation, GetUploaderID(), GetUploaderCTID());
                }
                catch (ArgumentException ae)
                {
                    return BadRequest(ae.Message);
                }
                catch (FormatException fe)
                {
                    return BadRequest(fe.Message);
                }
                catch (Exception)
                {
                    return BadRequest("Invalid request");
                }
            }
            else
            {
                return BadRequest("Invalid Request. A folder, file, or remote url must be specified");
            }

            return Ok(result);
        }

        /// <summary>
        /// Modifies a pre-existing class-type level document
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="classFolder"></param>
        /// <param name="pathAndFilename"></param>
        /// <param name="designation"></param>
        /// <param name="destination"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="renameName"></param>
        /// <returns></returns>
        /// <response code="200">@PutCommonOKResponseDescription</response>
        /// <response code="400">
        /// If targeting template and NOT Documents or Data buckets.
        /// If targeting static and NOT Reports or Data buckets.
        /// @PutCommonBadRequestResponseDescription
        /// </response>
        [HttpPut(bucket_ctid_folder_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: PutCommonOKResponseDescription)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:
            @"
            If targeting template and NOT Documents or Data buckets.
            If targeting static and NOT Reports or Data buckets.
            "+ PutCommonBadRequestResponseDescription)]
        public async Task<IActionResult> PutClassTypeLevel(BucketRequest bucket, int ctid, string classFolder, string pathAndFilename, [FromQuery]string designation, [FromQuery]string destination, [FromQuery]int? destCTID, [FromQuery]int? destID, [FromQuery]string renameName)
        {

            if (renameName.ToUpper() == CUSTOMER_SHARED)
            {
                return BadRequest("Failed to rename to '" + renameName + "' folder");
            }

            RequestModel requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, classFolder = classFolder, PathAndFilename = pathAndFilename };

            if (requestModel.ctid.HasValue && requestModel.classFolder == "template" && !(requestModel.Bucket == BucketRequest.Documents || requestModel.Bucket == BucketRequest.Data))
            {
                return BadRequest();
            }
            else if (requestModel.ctid.HasValue && requestModel.classFolder == "static" && !(requestModel.Bucket == BucketRequest.Reports || requestModel.Bucket == BucketRequest.Data))
            {
                return BadRequest();
            }

            return await Put(requestModel, designation, destination, destCTID, destID, renameName);
        }

        /// <summary>
        /// Modifies a pre-existing temporary document
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="guid"></param>
        /// <param name="pathAndFilename"></param>
        /// <param name="designation"></param>
        /// <param name="destination"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="renameName"></param>
        /// <returns></returns>
        /// <response code="200">@PutCommonOKResponseDescription</response>
        /// <response code="400">
        /// @PutCommonBadRequestResponseDescription
        /// </response>
        [HttpPut(bucket_temp_guid_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: PutCommonOKResponseDescription)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: PutCommonBadRequestResponseDescription)]
        public async Task<IActionResult> PutTemporary(BucketRequest bucket, Guid guid, string pathAndFilename, [FromQuery]string designation, [FromQuery]string destination, [FromQuery]int? destCTID, [FromQuery]int? destID, [FromQuery]string renameName)
        {
            RequestModel requestModel = new RequestModel() { Bucket = bucket, guid = guid, PathAndFilename = pathAndFilename };
            return await Put(requestModel, designation, destination, destCTID, destID, renameName);
        }



        /// <summary>
        /// Modifies a pre-existing document
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="id"></param>
        /// <param name="pathAndFilename"></param>
        /// <param name="designation"></param>
        /// <param name="destination"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="renameName"></param>
        /// <returns></returns>
        /// <response code="200">@PutCommonOKResponseDescription</response>
        /// <response code="400">
        /// If targeting NOT Documents or Data buckets.
        /// @PutCommonBadRequestResponseDescription
        /// </response>
        [HttpPut(bucket_ctid_id_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: PutCommonOKResponseDescription)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:
            @"
            If targeting NOT Documents or Data buckets.

            " + PutCommonBadRequestResponseDescription)]
        public async Task<IActionResult> PutEntity(BucketRequest bucket, int ctid, int id, string pathAndFilename, [FromQuery]string designation, [FromQuery]string destination, [FromQuery]int? destCTID, [FromQuery]int? destID, [FromQuery]string renameName)
        {
            RequestModel requestModel = new RequestModel() { Bucket = bucket, ctid = ctid, id = id, PathAndFilename = pathAndFilename };

            return await Put(requestModel, designation, destination, destCTID, destID, renameName);
        }

        internal async Task<IActionResult> Put(RequestModel requestModel, string designation, string destination, int? destCTID, int? destID, string renameName)
        {
            if (String.Equals(requestModel.PathAndFilename, "_TemplateFiles.json", StringComparison.OrdinalIgnoreCase))
            {
                return BadRequest("Failed to rename to '" + requestModel.PathAndFilename + "'");
            }

            if (requestModel.id.HasValue && !(requestModel.Bucket == BucketRequest.Documents || requestModel.Bucket == BucketRequest.Data))
            {
                return BadRequest();
            }

            var ctx = requestModel.GetStorageContext(User.BID().Value, User.AID());
            IFormFileCollection files = Request.HasFormContentType ? Request.Form.Files : null;

            if (files == null)
            {
                if (!String.IsNullOrWhiteSpace(renameName))
                {
                    if (renameName.ToUpper() == CUSTOMER_SHARED)
                    {
                        return BadRequest("Failed to rename to '" + renameName + "' folder");
                    }

                    if (requestModel.PathAndFilename.EndsWith($"/{AzureStorage.EntityStorageClient.SpecialFolderBlobName}"))
                    {
                        var results = await new DocumentManager(_cache, ctx).RenameFolderAsync(requestModel.PathAndFilename, renameName, designation);

                        return Ok(results);
                    }
                    else
                    {
                        //rename file
                        var results = await new DocumentManager(_cache, ctx).RenameFileAsync(requestModel.PathAndFilename, renameName, designation);

                        return Ok(results);
                    }
                }
                else if (!String.IsNullOrWhiteSpace(destination) || destID.HasValue || destCTID.HasValue)
                {
                    if (requestModel.PathAndFilename.EndsWith($"/{AzureStorage.EntityStorageClient.SpecialFolderBlobName}"))
                    {
                        //move folder
                        var results = await new DocumentManager(_cache, ctx).MoveFolderAsync(requestModel.PathAndFilename, destination, destCTID, destID, designation);

                        return Ok(results);
                    }
                    else
                    {
                        //move file
                        var results = await new DocumentManager(_cache, ctx).MoveFileAsync(requestModel.PathAndFilename, destination, destCTID, destID, designation);

                        return Ok(results);
                    }
                }
                else if (!String.IsNullOrWhiteSpace(designation))
                {
                    //update designation
                    var results = await new DocumentManager(_cache, ctx).UpdateDocumentMetadataAsync(requestModel.PathAndFilename, designation);

                    return Ok(results);
                }
                else
                {
                    return BadRequest("No body, rename, destination, or designation to update");
                }
            }
            else
            {
                if (String.IsNullOrWhiteSpace(destination))
                {
                    var results = await new DocumentManager(_cache, ctx).ReplaceDocumentAsync(requestModel.PathAndFilename, Request.Form, designation, GetUploaderID(), GetUploaderCTID());

                    return Ok(results);
                }
                else
                {
                    return BadRequest("Cannot replace and move at same time");
                }
            }

        }

        private bool IsSameFolderLevel(string pathAndFileName1, string pathAndFileName2)
        {
            pathAndFileName1 = (pathAndFileName1 ?? "").TrimEnd('/', '_').TrimStart('/');
            pathAndFileName2 = (pathAndFileName2 ?? "").TrimEnd('/', '_').TrimStart('/');

            string needle = "/";

            int needle1Count = (pathAndFileName1.Length - pathAndFileName1.Replace(needle, "").Length) / needle.Length;
            int needle2Count = (pathAndFileName2.Length - pathAndFileName2.Replace(needle, "").Length) / needle.Length;

            return needle1Count == needle2Count;
        }

        //------------------------------DELETE-------------------------------------------------------------------------

        internal async Task<IActionResult> Delete(RequestModel requestModel)
        {
            if (String.Equals(requestModel.PathAndFilename, "_TemplateFiles.json", StringComparison.OrdinalIgnoreCase))
            {
                return BadRequest("Failed to create '" + requestModel.PathAndFilename + "'");
            }

            var ctx = requestModel.GetStorageContext(User.BID().Value, User.AID());
            var results = await new DocumentManager(_cache, ctx).DeleteDocumentAsync(requestModel.PathAndFilename);

            return Ok(results);
        }

        /// <summary>
        /// Deletes a document from storage(Route 1)
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Returns number of deleted records</response>
        [HttpDelete(bucket_ctid_id_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description:"Returns number of deleted records")]
        public async Task<IActionResult> DeleteRoute1(BucketRequest bucket, int? ctid, int? id, string pathAndFilename)
        {
            return await this.Delete(new RequestModel(){Bucket=bucket, ctid=ctid, id=id, PathAndFilename=pathAndFilename});
        }


        /// <summary>
        /// Deletes a document from storage(Route 2)
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Returns number of deleted records</response>
        [HttpDelete(bucket_ctid_folder_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description:"Returns number of deleted records")]
        public async Task<IActionResult> DeleteRoute2(BucketRequest bucket, int? ctid, string classFolder, string pathAndFilename)
        {
            return await this.Delete(new RequestModel(){Bucket=bucket, ctid=ctid, classFolder=classFolder, PathAndFilename=pathAndFilename});
        }

        /// <summary>
        /// Deletes a document from storage(Route 3)
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Returns number of deleted records</response>
        [HttpDelete(bucket_temp_guid_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description:"Returns number of deleted records")]
        public async Task<IActionResult> DeleteRoute3(BucketRequest bucket, Guid? guid, string pathAndFilename)
        {
            return await this.Delete(new RequestModel(){Bucket=bucket, guid=guid, PathAndFilename=pathAndFilename});
        }
    }
}
