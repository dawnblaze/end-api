﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Api.Web.Includes;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Route ends up as api/dashboard/
    /// </summary>
    [Route("Api/System/Dashboard")]
    [Authorize]
    public class DashboardWidgetDefinitionController : GenericController<DashboardWidgetDefinition, DashboardWidgetDefinitionService, short>
    {
        /// <summary>
        /// Dashboard Widget Definition Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DashboardWidgetDefinitionController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, migrationHelper)
        {

        }

        /// <summary>
        /// Gets all Dashboard Widget Definitions for the given filters
        /// </summary>
        /// <param name="filters">Dashboard Widget Definition filters</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        [HttpGet("WidgetDefinitions")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardWidgetDefinition[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetWithFilters(DashboardWidgetDefinitionFilters filters)
        {
            return new OkObjectResult(await this._service.GetWithFilters(filters));
        }

        /// <summary>
        /// Gets the Dashboard Widget Definition for the given ID
        /// </summary>
        /// <param name="ID">Dashboard Widget Definition ID</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        [HttpGet("WidgetDefinitions/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardWidgetDefinition))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetForID(int ID)
        {
            return new OkObjectResult(await this._service.GetForID(ID));
        }

        /// <summary>
        /// Gets all Dashboard Widget Definition Categories for the given filters
        /// </summary>
        /// <param name="filters">Dashboard Widget Definition Category filters</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        [HttpGet("WidgetCategories")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(string[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetWithFilters(DashboardWidgetDefinitionCategoryFilters filters)
        {
            return new OkObjectResult(await this._service.GetCategoriesWithFilters(filters));
        }

    }
}
