﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Web.Annotation;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Endor.Tenant;
using Endor.CBEL.Autocomplete;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Endpoints for getting definitions from your system
    /// </summary>
    [Route("api/system/definition")]
    [Authorize]
    public class SystemDefinitionController : Controller
    {
        private readonly ApiContext _ctx;
        private readonly Lazy<SystemDefinitionService> _lazyService;
        private SystemDefinitionService _service { get { return _lazyService.Value; } }

        /// <summary>
        /// API Controller for System Colors
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <param name="autocompleteEngine">Autocomplete Engine</param>
        public SystemDefinitionController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, IMigrationHelper migrationHelper, IAutocompleteEngine autocompleteEngine)
        {
            this._ctx = context;
            this._lazyService = new Lazy<SystemDefinitionService>(() => new SystemDefinitionService(context, User.BID().Value, migrationHelper, autocompleteEngine));
        }

        /// <summary>
        /// Returns common definitions
        /// </summary>
        [HttpGet("common")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(string))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetCommonWithCustom()
        {
            return new ContentResult
            {
                ContentType = "text/typescript",
                StatusCode = (int)HttpStatusCode.OK,
                Content = await _service.GetCommonDefinition()
            };
        }

        /// <summary>
        /// Returns assembly common definitions
        /// </summary>
        /// <returns></returns>
        [HttpGet("common/assembly")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(string))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetCommonAssembly()
        {
            return new ContentResult
            {
                ContentType = "text/typescript",
                StatusCode = (int)HttpStatusCode.OK,
                Content = await _service.GetCommonDefinition((int)Models.ClassType.Assembly)
            };
        }

        /// <summary>
        /// Returns machine common definitions
        /// </summary>
        /// <returns></returns>
        [HttpGet("common/machine")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(string))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetCommonMachine()
        {
            return new ContentResult
            {
                ContentType = "text/typescript",
                StatusCode = (int)HttpStatusCode.OK,
                Content = await _service.GetCommonDefinition((int)Models.ClassType.Machine)
            };
        }

        /// <summary>
        /// Returns a dictionary of hints
        /// </summary>
        [HttpGet("hint")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(Dictionary<string, string>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetHint(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return NotFound();

            var result = await this._service.GetHint(name);

            if (!string.IsNullOrWhiteSpace(result))
                return new ContentResult
                {
                    ContentType = "text/typescript",
                    StatusCode = (int)HttpStatusCode.OK,
                    Content = result
                };
            else
                return NotFound();
        }

        /// <summary>
        /// Returns array of CBELFunctionInfo
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("common/functions")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CBEL.Metadata.CBELFunctionInfo[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetFunctions()
        {
            return Ok(await this._service.GetFunctions());
        }

        /// <summary>
        /// Returns array of CBELOperatorInfo
        /// </summary>
        /// <returns></returns>
        [HttpGet("common/operators")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CBEL.Operators.CBELOperatorInfo[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetOperators()
        {
            return Ok(await this._service.GetOperators());
        }

    }
}