﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

using Endor.Api.Web.Annotation;
using Swashbuckle.AspNetCore.Annotations;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for Contacts
    /// </summary>
    [Authorize]
    [Route("api/[Controller]")]
    public class ContactController : CRUDController<ContactData, ContactService, int>, ISimpleListableViewController<SimpleContactData, int>
    {
        /// <summary>
        /// Listable service for Contacts
        /// </summary>
        public ISimpleListableViewService<SimpleContactData, int> ListableService => this._service;

        /// <summary>
        /// Api Endpoint for Contacts
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public ContactController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Gets an array of Contacts and with any included child objects specified in query parameters
        /// </summary>
        /// <param name="CompanyID">Company ID to get Contacts for if specified</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ContactData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Company does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetForCompanyId([FromQuery] int? CompanyID, [FromQuery] ContactIncludes includes)
        {
            if (CompanyID.HasValue)
                return new OkObjectResult(await this._service.GetForCompanyId(CompanyID.Value, includes));
            else
                return await base.ReadWith(includes);
        }

        /// <summary>
        /// Gets the CompanyContactLinks with their Contacts for the specified CompanyID
        /// </summary>
        /// <param name="CompanyID">Company ID to get Contacts for if specified</param>
        /// <returns></returns>
        [HttpGet("Company/{CompanyID}/ContactLinks")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CompanyContactLink[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Company does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetCompanyContactLinks(int CompanyID)
        {
            var resp = await this._service.GetCompanyContactLinks(CompanyID);
            if (resp == null)
                return new NotFoundResult();

            return new OkObjectResult(resp);
        }

        /// <summary>
        /// Returns a single Contact by ID and any included child objects specified in query parameters
        /// </summary>
        /// <param name="ID">Contact ID</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ContactData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Contact does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetContactWithIncludes(int ID, [FromQuery] ContactIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the Contacts
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ContactData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Read()
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns a single Contact by ID
        /// </summary>
        /// <param name="ID">Contact ID</param>
        /// <returns></returns>
        [HttpGet("{ID}/obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ContactData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Contact does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single Contact by ID
        /// </summary>
        /// <param name="ID">Contact ID</param>
        /// <param name="update">Updated Contact data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ContactData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Contact does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Contact does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] ContactData update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new Contact
        /// </summary>
        /// <param name="newModel">New Contact data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ContactData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Contact creation fails")]
        public override async Task<IActionResult> Create([FromBody] ContactData newModel, [FromQuery] Guid? tempID = null)
        {
            await this._service.PrepareModelForCreateAsync(newModel);
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing Contact to a new Contact
        /// </summary>
        /// <param name="ID">ID of the Contact to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ContactData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Contact does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Contact does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes a Contact by ID
        /// </summary>
        /// <param name="ID">ID of the Contact to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}/Obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Contact does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Contact does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }


        /// <summary>
        /// Deletes a Contact by ID
        /// </summary>
        /// <param name="ID">ID of the Contact to Delete</param>
        /// <param name="force"></param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerOperationFilter(typeof(IgnoreHttpRequestParameterOperationFilter))]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Contact does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Contact does not exist or the deletion fails.")]
        public async Task<IActionResult> DeleteWithForce(short ID, [FromQuery]bool force = false)
        {
            if (force)
            {
                var resp = await _service.ForceDeleteContact(ID, User.UserLinkID().Value);
                return resp.ToResult();
            }

            return await base.Delete(ID);
        }

        #endregion

        /// <summary>
        /// Gets a Simple list of Contacts
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleContactData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleContactData[]> GetFilteredSimpleList([FromQuery] SimpleContactFilters filters)
        {
            return await this._service.GetCustomSimpleList(filters);
        }

        /// <summary>
        /// Gets a Simple list of Contacts
        /// </summary>
        /// <returns></returns>
        [Route("obseletesimplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleContactData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleContactData[]> SimpleList()
        {
            return await this.GetSimpleList<SimpleContactData, int>(User.BID().Value);
        }

        #region Contact Actions

        /// <summary>
        /// Sets a Contact to Active for a Company
        /// </summary>
        /// <param name="ID">Contact Id</param>
        /// <param name="CompanyID">The Company for which to set this Contact to active</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetActive/{CompanyID?}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Contact does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Contact does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(int ID, [FromRoute] int? CompanyID = 0)
        {
            if (CompanyID.HasValue && CompanyID != 0)
                return (await _service.SetContactRole(ID, CompanyID.Value, ContactRole.Observer)).ToResult();
            else
                return (await _service.SetContactActiveState(ID, ContactRole.Observer)).ToResult();
        }

        /// <summary>
        /// Sets a Contact to Inactive for a Company
        /// </summary>
        /// <param name="ID">Contact Id</param>
        /// <param name="CompanyID">The Company for which to set this Contact to inactive</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetInactive/{CompanyID?}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Contact does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Contact does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(int ID, int? CompanyID = 0)
        {
            if (CompanyID.HasValue && CompanyID != 0)
                return (await _service.SetContactRole(ID, CompanyID.Value, ContactRole.Inactive)).ToResult();
            else
                return (await _service.SetContactActiveState(ID, ContactRole.Inactive)).ToResult();
        }

        /// <summary>
        /// Sets multiple Contacts to Active
        /// </summary>
        /// <param name="ids">An array of Contact Ids</param>
        /// <returns></returns>
        [Route("Action/SetActive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntitiesActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntitiesActionChangeResponse), description: "If the Contact(s) do not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntitiesActionChangeResponse), description: "If the Contact(s) do not exist or fail to set as active.")]
        public async Task<IActionResult> SetActiveByIds([FromBody] int[] ids)
        {
            EntitiesActionChangeResponse resp = await this._service.SetContactActiveStateMultiple(ids, ContactRole.Observer);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets multiple Contacts to Inactive
        /// </summary>
        /// <param name="ids">Array of Contact Ids</param>
        /// <returns></returns>
        [Route("Action/SetInactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntitiesActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntitiesActionChangeResponse), description: "If the Contact(s) do not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntitiesActionChangeResponse), description: "If the Contact(s) do not exist or fail to set as inactive.")]
        public async Task<IActionResult> SetInactiveByIds([FromBody] int[] ids)
        {
            EntitiesActionChangeResponse resp = await this._service.SetContactActiveStateMultiple(ids, ContactRole.Inactive);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Contact as the Default Contact for a Company
        /// </summary>
        /// <param name="ID">Contact ID</param>
        /// <param name="CompanyID">The Company for which to set this Contact to Default</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetDefault/{CompanyID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Contact does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Contact does not exist or fails to set as Default.")]
        public async Task<IActionResult> SetDefault(int ID, int CompanyID)
        {
            EntityActionChangeResponse resp = await this._service.SetContactRole(ID, CompanyID, ContactRole.Primary);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Contact as the Billing Contact for a Company
        /// </summary>
        /// <param name="ID">Contact ID</param>
        /// <param name="CompanyID">The Company for which to set this Contact to Billing</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetBilling/{CompanyID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Contact does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Contact does not exist or fails to set as Billing.")]
        public async Task<IActionResult> SetBilling(int ID, int CompanyID)
        {
            EntityActionChangeResponse resp = await this._service.SetContactRole(ID, CompanyID, ContactRole.Billing);
            return resp.ToResult();
        }

        /// <summary>
        /// Links a Contact to a Company
        /// </summary>
        /// <param name="ID">Contact ID</param>
        /// <param name="CompanyID">The Company ID to link to</param>
        /// <returns></returns>
        [Route("{ID}/Action/Link/Company/{CompanyID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Contact does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Contact does not exist or fails to set as Billing.")]
        public async Task<IActionResult> LinkCompany(int ID, int CompanyID)
        {
            EntityActionChangeResponse resp = await this._service.UpdateCompanyContactLink(ID, CompanyID, true);
            return resp.ToResult();
        }

        /// <summary>
        /// Unlinks a Contact from a Company
        /// </summary>
        /// <param name="ID">Contact ID</param>
        /// <param name="CompanyID">The Company ID to unlink from</param>
        /// <returns></returns>
        [Route("{ID}/Action/Unlink/Company/{CompanyID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Contact does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Contact does not exist or fails to set as Billing.")]
        public async Task<IActionResult> UnlinkCompany(int ID, int CompanyID)
        {
            EntityActionChangeResponse resp = await this._service.UpdateCompanyContactLink(ID, CompanyID, false);
            return resp.ToResult();
        }

        #endregion

        /// <summary>
        /// Try's to validate the data model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            if (model == null || !(model is ContactData))
            {
                this.ModelState.AddModelError("model", "model is not a Contact.");
                return false;
            }

            ContactData contact = (ContactData)model;
            if (String.IsNullOrWhiteSpace(contact.Last))
            {
                this.ModelState.AddModelError("LastName", "Contact last name must contain characters.");
            }

            if (String.IsNullOrWhiteSpace(contact.First))
            {
                this.ModelState.AddModelError("FirstName", "Contact first name must contain characters.");
            }

            if (!_service.IsValidLocation(contact.LocationID))
            {
                this.ModelState.AddModelError("LocationID", "Invalid location.");
            }

            return base.TryValidateModel(model, prefix);
        }

        /// <summary>
        /// Returns a single Contact by ID and any included child objects specified in query parameters
        /// </summary>
        /// <param name="ID">Contact ID</param>
        /// <returns></returns>
        [HttpGet("status/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ContactStatusResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Contact does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> Status(int ID)
        {
            try
            {
                var response = await this._service.GetStatus(ID);
                return Ok(response);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e);
            }
        }
    }
}