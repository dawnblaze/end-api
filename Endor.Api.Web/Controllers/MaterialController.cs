﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes.Responses;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Material Controller
    /// </summary>
    [Route("api/materialpart")]
    public class MaterialController : CRUDController<MaterialData, MaterialService, int>, ISimpleListableViewController<SimpleMaterialData, int>
    {
        /// <summary>
        /// Listable service for Material Controller
        /// </summary>
        public ISimpleListableViewService<SimpleMaterialData, int> ListableService => this._service;

        /// <summary>
        /// Material Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public MaterialController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns all of the MaterialDatas
        /// can be filtered by IsActive
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MaterialData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MaterialData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] MaterialFilter filters)
        {
            return new OkObjectResult(await _service.GetWithFiltersAsync(filters));
        }

        /// <summary>
        /// Returns true | false based on whether the MaterialData can be deleted.
        /// </summary>
        /// <param name="ID">ID of the MaterialData to delete</param>
        /// <returns></returns>
        [HttpGet("{ID}/action/candelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(int ID)
        {
            return (await this._service.CanDelete(ID)).ToResult();
        }

        /// <summary>
        /// Sets a MaterialData to Active
        /// </summary>
        /// <param name="ID">MaterialData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the MaterialData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the MaterialData does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(int ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets a MaterialData to Inactive
        /// </summary>
        /// <param name="ID">MaterialData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the MaterialData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the MaterialData does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(int ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        #region Overridden CRUD Methods
        /// <summary>
        /// Returns all of the MaterialDatas
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsoletegetall")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MaterialData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MaterialData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.Read();
        }

        /// <summary>
        /// obselete
        /// </summary>
        /// <param name="ID">MaterialData ID</param>
        /// <returns></returns>
        [HttpGet("obselete/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MaterialData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MaterialData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [Obsolete]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Returns a single MaterialData by ID
        /// </summary>
        /// <param name="ID">MaterialData ID</param>
        /// <param name="includes">MaterialData includes</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MaterialData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MaterialData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWith(int ID, [FromQuery] MaterialDataIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }

        /// <summary>
        /// Updates a single MaterialData by ID
        /// </summary>
        /// <param name="ID">MaterialData ID</param>
        /// <param name="update">Updated MaterialData data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MaterialData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MaterialData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the MaterialDatas does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] MaterialData update)
        {
            try
            {
                return await base.Update(ID, update);
            }
            catch (InvalidOperationException e)
            {
                GenericResponse<MaterialData> genericResponse = new GenericResponse<MaterialData>() { ErrorMessage = e.Message };
                return genericResponse.ToResult();
            }
        }

        /// <summary>
        /// Creates a new MaterialData
        /// </summary>
        /// <param name="newModel">Updated MaterialData data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MaterialData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the MaterialDatas creation fails")]
        public override async Task<IActionResult> Create([FromBody] MaterialData newModel, [FromQuery] Guid? tempID = null)
        {
            try
            {
                return await base.Create(newModel, tempID);
            }
            catch (InvalidOperationException e)
            {
                GenericResponse<MaterialData> genericResponse = new GenericResponse<MaterialData>() { ErrorMessage = e.Message };
                return genericResponse.ToResult();
            }
        }

        /// <summary>
        /// Deletes a MaterialData by ID
        /// </summary>
        /// <param name="ID">ID of the MaterialData to delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MaterialData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the MaterialDatas does not exist or the deletion fails")]
        public override async Task<IActionResult> Delete(int ID)
        {
            await this._service.UnlinkAllCategoryAsync(ID);
            return await base.Delete(ID);
        }
        #endregion Overridden CRUD Methods

        /// <summary>
        /// Gets a Simple list of MaterialDatas
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleMaterialData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<ICollection<SimpleMaterialData>> FilteredSimpleList([FromQuery] SimpleListItemIsActiveFilter<SimpleMaterialData, int> filters)
        {
            return await this._service.GetByIDsAndCategoriesAsync(null, null, filters?.IsActive);
        }

        /// <summary>
        /// Gets a SimpleList base on categoryIDs and ids
        /// </summary>
        /// <returns></returns>
        [Route("simplelist/categoryandid")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleMaterialData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<ICollection<SimpleMaterialData>> GetByIDsAndCategories([FromQuery]ICollection<int> id, [FromQuery]ICollection<int> categoryid)
        {
            return await this._service.GetByIDsAndCategoriesAsync(id, categoryid);
        }

        /// <summary>
        /// Gets a SimpleList base on an assembly variable ID and, possibly, machine id and profile name
        /// </summary>
        /// <returns></returns>
        [Route("simplelist/byvariable/{variableID}")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleMaterialData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<ICollection<SimpleMaterialData>> GetByVariableAndProfile(int variableID, [FromQuery] int? machineID, [FromQuery] string profileName)
        {
            return await this._service.GetByVariableAndProfile(variableID, machineID, profileName);
        }

        [Route("dropdown/byvariable/{variableID}")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DropdownResponse<SimpleMaterialData>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<DropdownResponse<SimpleMaterialData>> GetDropdownByVariable(int variableID, [FromQuery] int? machineID, [FromQuery] string profileName)
        {
            return await this._service.GetDropdownByVariable(variableID, machineID, profileName);
        }

        /// <summary>
        /// links a material category to a material part
        /// </summary>
        /// <param name="materialDataID">MaterialData.ID</param>
        /// <param name="materialCategoryID">MaterialCategory.ID</param>
        /// <returns></returns>
        [HttpPost("{materialDataID}/action/linkmaterialcategory/{materialCategoryID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> LinkMaterial([FromRoute]int materialDataID, [FromRoute]short materialCategoryID)
        {
            var resp = await this._service.LinkMaterialCategory(materialDataID, materialCategoryID, true);


            return resp.ToResult();
        }

        /// <summary>
        /// unlinks a material category from a material part
        /// </summary>
        /// <param name="materialDataID">MaterialData.ID</param>
        /// <param name="materialCategoryID">MaterialCategory.ID</param>
        /// <returns></returns>
        [HttpPost("{materialDataID}/action/unlinkmaterialcategory/{materialCategoryID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> UnLinkMaterial([FromRoute]int materialDataID, [FromRoute]short materialCategoryID)
        {
            var resp = await this._service.LinkMaterialCategory(materialDataID, materialCategoryID, false);

            return resp.ToResult();
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsoletesimplelist")]
        [Obsolete]
        public async Task<SimpleMaterialData[]> SimpleList()
        {
            return (await this.FilteredSimpleList(null)).ToArray();
        }

        /// <summary>
        /// Checks if the model is valid
        /// </summary>
        /// <param name="model">Model to validate</param>
        /// <param name="prefix">Key to use in validation</param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            var castedModel = (MaterialData)model;
            var existingNameOwners = this._service.ExistingNameOwners(castedModel);
            if (existingNameOwners.Count() > 0)
            {
                this.ModelState.AddModelError("Name", $"A Material named {castedModel.Name} already exists.");
            }

            return base.TryValidateModel(model, prefix);
        }        
    }
}
