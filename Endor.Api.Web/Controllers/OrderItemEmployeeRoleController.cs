﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// OrderItemEmployeeRole Service
    /// </summary>
    [Route("api/orderitem/{parentID}/employeerole")]
    public class OrderItemEmployeeRoleController : CRUDChildController<OrderEmployeeRole, OrderItemEmployeeRoleService, int, int>
    {
        private readonly Lazy<OrderItemService> _orderItemService;

        /// <summary>
        /// OrderItemEmployeeRole Service Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="taskQueuer">ITaskQueuer</param>
        /// <param name="cache">ITenantDataCache</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderItemEmployeeRoleController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
            this._orderItemService = new Lazy<OrderItemService>(() => new OrderItemService(context, this.User.BID().Value, taskQueuer, cache, logger, rtmClient, migrationHelper));
        }


        /// <summary>
        /// Retrieve by Parent ID - Not Implemented
        /// </summary>
        [HttpGet("obsolete")]
        [Obsolete]
        public override Task<IActionResult> Read(int parentID)
        {
            throw new InvalidOperationException();
        }

        /// <summary>
        /// Retrieve by ParentId and ID - Not Implemented
        /// </summary>
        [HttpGet("obsolete/{ID}")]
        [Obsolete]
        public override Task<IActionResult> ReadByParentIdAndId(int parentID, int ID)
        {
            throw new InvalidOperationException();
        }
        
        /// <summary>
        /// Gets all of the OrderEmployeeRoles associated with the OrderItem by its ID
        /// </summary>
        /// <param name="parentID">OrderItem's ID</param>
        /// <param name="includeDefaults">If IncludeDefault is true, the result will include employee roles at the Order Level is there is not a version that exists on the Item Level.</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderEmployeeRole[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> ReadWith(int parentID, [FromQuery] bool includeDefaults = false)
        {
            var results = await this._service.GetByParentIDWithDefaultsAsync(parentID, includeDefaults);
            if (results == null)
                return NotFound();

            return Ok(results);
        }

        /// <summary>
        /// Retrieves a specific OrderEmployeeRole associated with an OrderItem by the OrderEmployeeRole's ID
        /// </summary>
        /// <param name="parentID">OrderItem's ID</param>
        /// <param name="ID">OrderEmployeeRole's ID</param>
        /// <param name="useDefaultIfNotFound">If UseDefaultIfNotFound is true, the result will include the employee roles at the Order Level is there is not a version that exists on the Item Level.</param>
        /// <returns></returns>
        
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderEmployeeRole))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem or OrderEmployeeRole does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> ReadWith(int parentID, int ID, [FromQuery] bool useDefaultIfNotFound = false)
        {
            return await base.ReadByParentIdAndId(parentID, ID);
        }

        /// <summary>
        /// Creates an OrderEmployeeRole to be associated with the OrderItem
        /// </summary>
        /// <param name="parentID">OrderItem's ID</param>
        /// <param name="orderEmployeeRole">New OrderEmployeeRole object</param>
        /// <param name="tempID">temp Guid for saving documents</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderEmployeeRole))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the creation for the OrderEmployeeRole fails.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public override async Task<IActionResult> Create(int parentID, [FromBody] OrderEmployeeRole orderEmployeeRole, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(parentID, orderEmployeeRole, tempID);
        }

        /// <summary>
        /// Updates the EmployeeID for an OrderEmployeeRole associated with the OrderItem
        /// </summary>
        /// <param name="parentID">OrderItem's ID</param>
        /// <param name="ID">OrderEmployeeRole's ID</param>
        /// <param name="update">Updated OrderEmployeeRole object</param>
        /// <returns></returns>
        [Route("{ID}")]
        [HttpPut]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem or OrderEmployeeRole does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the update for the OrderEmployeeRole fails or the OrderEmployeeRole is not associated with the OrderItem.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]

        public override async Task<IActionResult> Update(int parentID, int ID, [FromBody] OrderEmployeeRole update)
        {
            return await base.Update(parentID, ID, update);
        }

        /// <summary>
        /// Creates an OrderEmployeeRole to be associated with the OrderItem
        /// </summary>
        /// <param name="parentID">OrderItem's ID</param>
        /// <param name="ID">OrderEmployeeRole's ID</param>
        /// <returns></returns>
        [Route("{ID}")]
        [HttpDelete]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem or OrderEmployeeRole does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the delete for the OrderEmployeeRole fails or the OrderEmployeeRole is not associated with the OrderItem.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public override async Task<IActionResult> Delete(int parentID, int ID)
        {
            return await base.Delete(parentID, ID);
        }
    }
}
