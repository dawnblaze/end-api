﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;
using Swashbuckle.AspNetCore.Annotations;
using Endor.Api.Web.Classes.Responses;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for Part Labor
    /// </summary>
    [Route("api/laborpart")]
    //[ApiExplorerSettings(GroupName = @"LaborPart", IgnoreApi = false)]
    public class PartLaborController : CRUDController<LaborData, PartLaborService, int>, ISimpleListableViewController<SimpleLaborData, int>
    {

        /// <summary>
        /// Listable service for Labor Data
        /// </summary>
        public ISimpleListableViewService<SimpleLaborData, int> ListableService => this._service;

        /// <summary>
        /// Api Endpoint Constructor for Part Labor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper"></param>
        public PartLaborController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns all of the LaborDatas
        /// can be filtered by IsActive
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LaborData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If LaborData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] PartLaborFilter filters)
        {
            return new OkObjectResult(await _service.GetWithFiltersAsync(filters));
        }

        #region Overridden CRUD Methods
        /// <summary>
        /// Returns all of the LaborDatas
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsoletegetall")]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LaborData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If LaborData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns a single LaborData by ID
        /// </summary>
        /// <param name="ID">LaborData ID</param>
        /// <param name="includes"></param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LaborData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If LaborData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWith(int ID, [FromQuery] LaborDataIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }

        /// <summary>
        /// Obselete
        /// </summary>
        /// <param name="ID">LaborData ID</param>
        /// <returns></returns>
        [HttpGet("obsolete/{ID}")]
        [Obsolete]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LaborData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If LaborData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }        



        /// <summary>
        /// Updates a single LaborData by ID
        /// </summary>
        /// <param name="ID">LaborData ID</param>
        /// <param name="update">Updated LaborData data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LaborData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If LaborData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the LaborDatas does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] LaborData update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new LaborData
        /// </summary>
        /// <param name="newModel">Updated LaborData data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LaborData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the LaborDatas creation fails")]
        public override async Task<IActionResult> Create([FromBody] LaborData newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Deletes a LaborData by ID
        /// </summary>
        /// <param name="ID">ID of the LaborData to delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If LaborData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the LaborDatas does not exist or the deletion fails")]
        public override async Task<IActionResult> Delete(int ID)
        {
            await this._service.unlinkAllCategoryAsync(ID);
            return await base.Delete(ID);
        }

        /// <summary>
        /// Clones an existing LaborData to a new LaborData
        /// </summary>
        /// <param name="ID">ID of the LaborData to clone</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await DoClone(ID);
        }
        #endregion Overridden CRUD Methods


        /// <summary>
        /// Returns true | false based on whether the LaborData can be deleted.
        /// </summary>
        /// <param name="ID">ID of the LaborData to delete</param>
        /// <returns></returns>
        [HttpGet("{ID}/action/candelete")]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(int ID)
        {
            return (await this._service.CanDelete(ID)).ToResult();
        }

        /// <summary>
        /// Sets a LaborData to Active
        /// </summary>
        /// <param name="ID">LaborData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the LaborData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the LaborData does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(int ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets a LaborData to Inactive
        /// </summary>
        /// <param name="ID">LaborData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the LaborData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the LaborData does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(int ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Gets a Simple list of LaborDatas
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleLaborData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleLaborData[]> FilteredSimpleList([FromQuery] SimpleListItemIsActiveFilter<SimpleLaborData, int> filters)
        {
            return await this.GetSimpleList(User.BID().Value, filters);
        }

        /// <summary>
        /// Gets a SimpleList base on categoryIDs and ids
        /// </summary>
        /// <returns></returns>
        [Route("simplelist/categoryandid")]
        [HttpGet]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleLaborData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<ICollection<SimpleLaborData>> GetByIDsAndCategories([FromQuery]ICollection<int> id, [FromQuery]ICollection<int> categoryid)
        {
            return await this._service.GetByIDsAndCategoriesAsync(id, categoryid);
        }

        /// <summary>
        /// Gets a SimpleList base on an assembly variable ID and, possibly, machine id and profile name
        /// </summary>
        /// <returns></returns>
        [Route("simplelist/byvariable/{variableID}")]
        [HttpGet]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleLaborData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<ICollection<SimpleLaborData>> GetByVariableAndProfile(int variableID, [FromQuery] int? machineID, [FromQuery] string profileName)
        {
            return await this._service.GetByVariableAndProfile(variableID, machineID, profileName);
        }

        [Route("dropdown/byvariable/{variableID}")]
        [HttpGet]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DropdownResponse<SimpleLaborData>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<DropdownResponse<SimpleLaborData>> GetDropdownByVariable(int variableID, [FromQuery] int? machineID, [FromQuery] string profileName)
        {
            return await this._service.GetDropdownByVariable(variableID, machineID, profileName);
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsoletesimplelist")]
        [Obsolete]
        [SwaggerOperation(Tags = new[] { "Management Reports" })]
        public Task<SimpleLaborData[]> SimpleList()
        {
            return this.FilteredSimpleList(null);
        }

        /// <summary>
        /// Links a `LaborData` to a `LaborCategory`
        /// </summary>
        /// <param name="laborCategoryID">LaborCategory.ID</param>
        /// <param name="laborDataID">LaborData.ID</param>
        /// <returns>`EntityActionChangeResponse`</returns>
        [HttpPost("{laborDataID}/action/linklaborcategory/{laborCategoryID}")]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]        
        public async Task<EntityActionChangeResponse> LinkLaborCategory([FromRoute]short laborCategoryID, [FromRoute]int laborDataID)
        {
            var resp = await this._service.LinkLaborCategory(laborCategoryID, laborDataID, true);

            return resp;
        }

        /// <summary>
        /// Unlinks a `LaborData` to a `LaborCategory`
        /// </summary>
        /// <param name="laborCategoryID">LaborCategory.ID</param>
        /// <param name="laborDataID">LaborData.ID</param>
        /// <returns>`EntityActionChangeResponse`</returns>
        [HttpPost("{laborDataID}/action/unlinklaborcategory/{laborCategoryID}")]
        [SwaggerOperation(Tags = new[] { "LaborPart" })]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]        
        public async Task<EntityActionChangeResponse> UnLinkLaborCategory([FromRoute]short laborCategoryID, [FromRoute]int laborDataID)
        {
            var resp = await this._service.LinkLaborCategory(laborCategoryID, laborDataID, false);

            return resp;            
        }

        /// <summary>
        /// Tries to validate the data model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            var laborModel = (LaborData)model;
            var existingNameOwners = this._service.ExistingNameOwners(laborModel);
            if (existingNameOwners.Count() > 0)
            {
                this.ModelState.AddModelError("Name", $"A Labor named {laborModel.Name} already exists.");
            }

            return base.TryValidateModel(model, prefix);
        }
    }
}
