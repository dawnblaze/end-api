﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for Orders
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    public class OrderEmployeeRoleController : CRUDChildController<OrderEmployeeRole, OrderEmployeeRoleService, int, int>
    {
        /// <summary>
        /// Api Endpoint for Orders
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderEmployeeRoleController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {

        }

        #region Overridden CRUD Methods
        /// <summary>
        /// Obsolete endpoint
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{parentID}/obsolete/employeerole/")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read(int parentID)
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read(parentID);
        }

        /// <summary>
        /// Obsolete endpoint
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{parentID}/obsolete/employeerole/{ID}")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> ReadByParentIdAndId(int parentID, int ID)
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.DoRead(parentID, ID);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="includes"></param>
        /// <param name="employeeRoleID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{parentID}/employeerole/")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderEmployeeRole[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadFiltered(int parentID, [FromQuery]OrderEmployeeRoleIncludes includes, [FromQuery]short? employeeRoleID = null)
        {
            if (employeeRoleID.HasValue)
            {
                var resultQuery = _service.WhereParent(_service.Where(t => t.RoleID == employeeRoleID.Value,includes:includes), parentID);

                return Ok(resultQuery.ToListAsync());
            }

            return await base.ReadWith(parentID, includes);
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="parentID"></param>
        ///// <param name="ID"></param>
        ///// <param name="includes"></param>
        ///// <returns></returns>
        //[HttpGet("{parentID}/obsolete/employeerole/{ID}")]
        //[Obsolete]
        //public Task<IActionResult> ReadWith(int parentID, int ID, IExpandIncludes includes = null)
        //{
        //    return base.ReadWith(parentID, ID, includes);
        //}
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="ID"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{parentID}/employeerole/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderEmployeeRole))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithIncludes(int parentID, int ID, [FromQuery]OrderEmployeeRoleIncludes includes)
        {
            return await base.DoReadWith(parentID, ID, includes);
        }

        /// <summary>
        /// Create an Employee Role
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="newModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("{parentID}/employeerole/")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderEmployeeRole))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> Create(int parentID, [FromBody]OrderEmployeeRole newModel, Guid? tempGuid = null)
        {
            return base.DoCreate(parentID, newModel, tempGuid);
        }

        /// <summary>
        /// Delete an employee role
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{parentID}/employeerole/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> Delete(int parentID, int ID)
        {
            return base.DoDelete(parentID, ID);
        }

        /// <summary>
        /// Update an employee role
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="ID"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{parentID}/employeerole/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> Update(int parentID, int ID, [FromBody]OrderEmployeeRole update)
        {
            return base.DoUpdate(parentID, ID, update);
        }
        #endregion

        /// <summary>
        /// This is an override of CRUDChildController.Clone virtual method
        /// in order to fix swagger.json validation error:
        /// "the endpoint is defined with a "parentID" parameter where the "in" property value is "path"
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost("{parentID}/employeerole/{ID}/clone")]
        public override async Task<IActionResult> Clone([FromRoute]int parentID, [FromRoute]int ID)
        {
            return await DoClone(ID);
        }

    }
}
