﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// This is a QuickItem Controller
    /// </summary>
    [Route("api/QuickItem")]
    public class QuickItemController : CRUDController<QuickItemData, QuickItemService, int>, ISimpleListableViewController<SimpleQuickItemData, int>
    {
        /// <summary>
        /// Listable service for QuickItems
        /// </summary>
        public ISimpleListableViewService<SimpleQuickItemData, int> ListableService => this._service;

        /// <summary>
        /// QuickItem API Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public QuickItemController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {

        }
        

        #region Overridden CRUD Methods

        /// <summary>
        /// Updates a single QuickItem by ID
        /// </summary>
        /// <param name="ID">QuickItem ID</param>
        /// <param name="update">Updated QuickItem data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(QuickItemData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the QuickItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the QuickItem does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] QuickItemData update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new QuickItem-Obsolete
        /// </summary>
        /// <param name="newModel">New QuickItem data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [Obsolete]
        [HttpPost("obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(QuickItemData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the QuickItem creation fails")]
        public override async Task<IActionResult> Create([FromBody] QuickItemData newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Creates a new QuickItem
        /// </summary>
        /// <param name="newModel">New QuickItem data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <param name="copyFiles">defaults to false</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(QuickItemData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the QuickItem creation fails")]
        public async Task<IActionResult> CreateQuickItem([FromBody] QuickItemData newModel, [FromQuery] Guid? tempID = null, [FromQuery]bool copyFiles = false)
        {
            newModel.DataJSON = this._service.RenumberLineItem(newModel.DataJSON);
            string oldModelDataJSON = newModel.DataJSON; //We need the Line Item IDs and Line Item Numbers for copying the line item files

            var result = await base.Create(newModel, tempID);
            if (result is OkObjectResult)
            {
                await _service.CopyDefaultImage(newModel.ID, oldModelDataJSON);

                if (copyFiles)
                {
                    await this._service.CopyLineItemFiles(newModel, oldModelDataJSON);
                }
            }
            return result;
        }        

        /// <summary>
        /// Deletes a QuickItem by ID
        /// </summary>
        /// <param name="ID">ID of the QuickItem to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the QuickItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the QuickItem does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            BooleanResponse resp = await this._service.CanDelete((int)ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
                return await base.Delete(ID);

            return new BadRequestObjectResult(resp);
        }

        /// <summary>
        /// Gets a Simple list of QuickItems
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleQuickItemData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleQuickItemData[]> FilteredSimpleList([FromQuery] SimpleQuickItemFilters filters)
        {
            return await this._service.GetSimpleList(filters);
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsoletesimplelist")]
        [Obsolete]
        public Task<SimpleQuickItemData[]> SimpleList()
        {
            return this.FilteredSimpleList(null);
        }

        #endregion

        #region Actions

        /// <summary>
        /// Sets a QuickItem to Active
        /// </summary>
        /// <param name="ID">QuickItem ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the QuickItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the TaxabilityCode does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(int ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a QuickItem to to Inactive
        /// </summary>
        /// <param name="ID">QuickItem ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the QuickItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the QuickItem does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(int ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the QuickItem can be deleted.
        /// </summary>
        /// <param name="ID">QuickItem ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(int ID)
        {
            return (await this._service.CanDelete((int)ID)).ToResult();
        }

        /// <summary>
        /// links a QuickItemCategory to a QuickItem
        /// </summary>
        /// <param name="ID">QuickItem.ID</param>
        /// <param name="CategoryID">QuickItemCategory.ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/LinkCategory/{CategoryID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> LinkCategory([FromRoute]int ID, [FromRoute]short CategoryID)
        {
            var resp = await this._service.LinkCategory(ID, CategoryID, true);


            return resp.ToResult();
        }

        /// <summary>
        /// unlinks a material category from a material part
        /// </summary>
        /// <param name="ID">QuickItem.ID</param>
        /// <param name="CategoryID">QuickItemCategory.ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/UnlinkCategory/{CategoryID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> UnLinkCategory([FromRoute]int ID, [FromRoute]short CategoryID)
        {
            var resp = await this._service.LinkCategory(ID, CategoryID, false);

            return resp.ToResult();
        }

        #endregion
    }
}
