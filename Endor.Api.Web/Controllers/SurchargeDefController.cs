﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for SurchargeDef
    /// </summary>
    [Authorize]
    [Route("api/[Controller]")]
    public class SurchargeDefController : CRUDController<SurchargeDef, SurchargeDefService, short>, ISimpleListableViewController<SimpleSurchargeDef, short>
    {
        /// <summary>
        /// Listable service for SurchargeDef
        /// </summary>
        public ISimpleListableViewService<SimpleSurchargeDef, short> ListableService => this._service;

        /// <summary>
        /// Api Endpoint for SurchargeDef
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public SurchargeDefController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {

        }

        /// <summary>
        /// Returns all of the SurchargeDef
        /// filtered by IsActive and/or AppliedByDefault
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SurchargeDef[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetByFilter([FromQuery] SurchargeDefFilter filters)
        {
            return new OkObjectResult(await _service.GetWithFiltersAsync(filters));
        }

        /// <summary>
        /// Old read method
        /// </summary>
        /// <returns></returns>
        [HttpGet("Obsolete")]
        [Obsolete]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Sets an SurchargeDef to Active
        /// </summary>
        /// <param name="ID">SurchargeDef Id</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetActive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the SurchargeDef does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the SurchargeDef does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets an SurchargeDef to InActive
        /// </summary>
        /// <param name="ID">SurchargeDef Id</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetInactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the SurchargeDef does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the SurchargeDef does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// checks if the SurchargeDef is not in use and can be deleted
        /// </summary>
        /// <param name="ID">targeted SurchargeDef Id to be set as a child</param>
        /// <returns></returns>
        [Route("{ID}/Action/CanDelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the SurchargeDef does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        //CRUD METHODS-----------------------------------------------------------------------------------------------------------------------------------
        #region CRUDMETHODS

        /// <summary>
        /// Returns a single SurchargeDef by ID
        /// </summary>
        /// <param name="ID">SurchargeDef ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SurchargeDef))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the SurchargeDef does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Creates a new SurchargeDef
        /// </summary>
        /// <param name="newModel">New SurchargeDef data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SurchargeDef))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the SurchargeDef creation fails")]
        public override async Task<IActionResult> Create([FromBody] SurchargeDef newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Updates a single SurchargeDef by ID
        /// </summary>
        /// <param name="ID">SurchargeDef ID</param>
        /// <param name="update">Updated SurchargeDef data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SurchargeDef))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the SurchargeDef does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the SurchargeDef does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] SurchargeDef update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Deletes a SurchargeDef by ID
        /// </summary>
        /// <param name="ID">ID of the SurchargeDef to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the SurchargeDef does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the SurchargeDef does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            BooleanResponse resp = await this._service.CanDelete(ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                return await base.Delete(ID);
            }
            else
            {
                return new BadRequestObjectResult(resp);
            }
        }

        /// <summary>
        /// Clones an existing SurchargeDef
        /// </summary>
        /// <param name="ID">ID of the SurchargeDef to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SurchargeDef))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source SurchargeDef does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source SurchargeDef does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }

        #endregion

        /// <summary>
        /// Gets a Simple list of SurchargeDefs
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleSurchargeDef[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleSurchargeDef[]> GetFilteredSimpleList([FromQuery] SimpleSurchargeDefFilters filters)
        {
            return await this.GetSimpleList<SimpleSurchargeDef, short>(User.BID().Value, filters);
        }

        /// <summary>
        /// Gets a Simple list of SurchargeDefs
        /// </summary>
        /// <returns></returns>
        [HttpGet("obseletesimplelist")]
        [Obsolete]
        public async Task<SimpleSurchargeDef[]> SimpleList()
        {
            return await this.GetFilteredSimpleList(null);
        }

        #region Move Action
        /// <summary>
        /// Reorders the Source record so it's sort index is lower than the sort index of the specified Target record.  
        /// </summary>
        /// <returns></returns>
        /// <param name="ID"></param>
        /// <param name="targetID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/movebefore/{targetID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Reorders the Source record so it's sort index is lower than the sort index of the specified Target record.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Specified ID does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid target type or target type does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials")]
        public async Task<IActionResult> MoveBefore(short ID, short targetID)
        {
            return (await _service.MoveBefore(ID, targetID)).ToResult();
        }

        /// <summary>
        /// Reorders the Source record so it's sort index is higher than the sort index of the specified Target record.  
        /// </summary>
        /// <returns></returns>
        /// <param name="ID"></param>
        /// <param name="targetID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/moveafter/{targetID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Reorders the Source record so it's sort index is higher than the sort index of the specified Target record.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Specified ID does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid target type or target type does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials")]
        public async Task<IActionResult> MoveAfter(short ID, short targetID)
        {
            return (await _service.MoveAfter(ID, targetID)).ToResult();
        }
        #endregion Move

        /// <summary>
        /// Pagination List
        /// </summary>
        /// <param name="filters">Query parameters to filter a property</param>
        /// <param name="skip">Number of items to skip</param>
        /// <param name="take">Number of items to take</param>
        /// <returns></returns>
        [HttpGet("paged")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SurchargeDef[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetPagedList([FromQuery] SurchargeDefFilter filters,
                                                        [FromQuery]int? skip = null,
                                                        [FromQuery]int? take = null)
        {
            return new OkObjectResult(await this._service.GetPagedListAsync(filters, skip, take, null));
        }
    }

}
