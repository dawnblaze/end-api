﻿using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Security;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{    
    /// <summary>
    /// An order destination controller
    /// </summary>
    [Route("api/orderdestination")]
    public class OrderDestinationController : CRUDController<OrderDestinationData, OrderDestinationService, int>
    {
        /// <summary>
        /// Constructs an order destination controller
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderDestinationController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) 
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }
        
        #region Overridden obsolete endpoints

        /// <summary>
        /// Obsolete read
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Obsolete read by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("obsolete/{id}")]
        [Obsolete]
        public override Task<IActionResult> ReadById(int ID)
        {
            return base.ReadById(ID);
        }

        #endregion
        
        /// <summary>
        /// Returns an array of OrderDestination with expands based on query param
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderDestinationData[]), "Retrieve a collection of OrderDestination Object records in JSON format.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> ReadWithIncludes([FromQuery] OrderDestinationIncludes includes)
        {
            return await base.ReadWith(includes);
        }

        /// <summary>
        /// Returns a single OrderDestination with expands based on query param
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderDestinationData), "Returns an OrderDestination Object record in JSON format.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> ReadWithIncludes(int ID, [FromQuery] OrderDestinationIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }

        /// <summary>
        /// Creates a new OrderDestination
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempID"></param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderDestinationData), "Creates a new OrderDestination Object record.  The new record is passed in the body of the message. The saved record is returned in the response message.\r\nThe OrderID is passed in the body and must be an open Order.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderDestination creation fails.")]        
        public override async Task<IActionResult> Create([FromBody] OrderDestinationData newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Updates an existing OrderDestination
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderDestinationData), "Updates an existing OrderDestination Object record.  The revisions are passed in the body of the message. The saved record is returned in the response message.\r\nThe OrderID is passed in the body and must be the Order ID that the order destination is already on.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderDestination does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderDestination does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] OrderDestinationData update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Deletes an OrderDestination
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderDestination does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderDestination does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }

        /// <summary>
        /// Clones an OrderDestination into a new OrderDestination
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderDestinationData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source OrderDestination does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source OrderDestination does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }
    }
}
