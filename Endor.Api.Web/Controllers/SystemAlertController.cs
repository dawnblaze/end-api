﻿using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tenant;
using Endor.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// controller for 
    /// /system/alert/actions 
    /// and 
    /// /system/automation/actions
    /// </summary>
    [Authorize]
    [Route("api/system")]
    public class SystemAlertController : Controller
    {
        private readonly ApiContext _ctx;
        private readonly Lazy<SystemAlertService> _lazyService;
        private SystemAlertService _service { get { return _lazyService.Value; } }

        /// <summary>
        /// public ctor for DI for System alert ctrlr
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public SystemAlertController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, IMigrationHelper migrationHelper)
        {
            this._ctx = context;
            migrationHelper.MigrateDb(_ctx);
            this._lazyService = new Lazy<SystemAlertService>(() => new SystemAlertService(context, User.BID().Value, migrationHelper));

        }

        /// <summary>
        /// public GET for system alert actions
        /// </summary>
        /// <param name="DataType"></param>
        /// <returns></returns>
        [HttpGet("alert/actions")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SystemAutomationActionDefinition[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetSystemAlertActions([FromQuery] DataType DataType)
        {
            return await getActions(DataType, (t) => t.AppliesToAlerts);
        }

        /// <summary>
        /// public GET for system alert triggers
        /// </summary>
        /// <param name="DataType"></param>
        /// <returns></returns>
        [HttpGet("alert/triggers")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SystemAutomationTriggerDefinition[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetSystemAlertTriggers([FromQuery] DataType DataType)
        {
            return await getTriggers(DataType, (t) => t.AppliesToAlerts);
        }

        private async Task<IActionResult> getTriggers(DataType dataType, Expression<Func<SystemAutomationTriggerDefinition, bool>> predicate)
        {
            return new OkObjectResult(await _service.GetTriggersAsync(dataType, predicate));
        }

        private async Task<IActionResult> getActions(DataType dataType, Expression<Func<SystemAutomationActionDefinition, bool>> predicate)
        {
            return new OkObjectResult(await _service.GetActionsAsync(dataType, predicate));
        }

        /// <summary>
        /// public GET for system automation actions
        /// </summary>
        /// <param name="DataType"></param>
        /// <returns></returns>
        [HttpGet("automation/actions")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SystemAutomationActionDefinition[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetSystemAutomationActions([FromQuery] DataType DataType)
        {
            return await getActions(DataType, (t) => t.AppliesToAutomations);
        }

        /// <summary>
        /// public GET for system alert triggers
        /// </summary>
        /// <param name="DataType"></param>
        /// <returns></returns>
        [HttpGet("automation/triggers")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SystemAutomationTriggerDefinition[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetSystemAutomationTriggers([FromQuery] DataType DataType)
        {
            return await getTriggers(DataType, (t) => t.AppliesToAutomations);
        }
    }
}
