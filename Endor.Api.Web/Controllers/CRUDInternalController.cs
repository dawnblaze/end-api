﻿using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// This is the internal response generator, no endpoint
    /// </summary>
    /// <typeparam name="M"></typeparam>
    /// <typeparam name="S"></typeparam>
    /// <typeparam name="I"></typeparam>
    public class CRUDInternalController<M, S, I> : Controller
        where M : class, IAtom<I>
        where S : AtomCRUDService<M, I>
        where I : struct, IConvertible
    {
        /// <summary>
        /// 
        /// </summary>
        public const string ConnectionIDHeaderKey = "ConnectionID";
        /// <summary>
        /// Lazy implementation of the service
        /// we use a Lazy because at ctor time User.BID is null
        /// and when we actually need it User.BID is filled
        /// </summary>
        protected readonly Lazy<S> _lazyService;
        /// <summary>
        /// Logger
        /// </summary>
        protected RemoteLogger _logger;
        /// <summary>
        /// RTM Client
        /// </summary>
        protected IRTMPushClient _rtmClient;
        /// <summary>
        /// Migration Helper
        /// </summary>
        protected IMigrationHelper _migrationHelper;

        /// <summary>
        /// Getter for the service
        /// </summary>
        protected S _service { get { return _lazyService.Value; } }

        /// <summary>
        /// CRUDInternalController Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="taskQueuer">ITaskQueuer</param>
        /// <param name="cache">ITenantDataCache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <param name="customLazySvcGenerator">optional function that is passed User.BID and must return an instance of S</param>
        public CRUDInternalController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper, Func<short, S> customLazySvcGenerator = null)
        {
            //we need the lambda for User.BID because at constructor time User is null
            //so we use the lazy in the service because by the time we need a service, the user exists
            if (customLazySvcGenerator == null)
                this._lazyService = AtomCRUDService<M, I>.CreateService<S>(context, taskQueuer, cache, logger, () => User.BID().Value, rtmClient, migrationHelper);
            else
                this._lazyService = new Lazy<S>(() => customLazySvcGenerator(User.BID().Value));
            this._logger = logger;
            this._rtmClient = rtmClient;
            this._migrationHelper = migrationHelper;
        }

        internal async Task<IActionResult> DoReadWith(IExpandIncludes includes)
        {//do something special here
            try
            {
                return new OkObjectResult(await _service.GetAsync(includes));
            }
            catch (InvalidOperationException ioe)
            {
                if (ioe.Message.Contains("The property") && ioe.Message.Contains("'Simple"))
                {
                    return BadRequest("Invalid includes were specified.");
                }
                return BadRequest(ioe);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        internal async Task<IActionResult> DoFilteredReadWith(Expression<Func<M, bool>> filterPredicate, IExpandIncludes includes)
        {//do something special here
            try
            {
                var result = await _service.FilteredGetAsync(filterPredicate, includes);
                if (result != null)
                    return new OkObjectResult(result);

                return NotFound();
            }
            catch (InvalidOperationException ioe)
            {
                if (ioe.Message.Contains("The property") && ioe.Message.Contains("'Simple"))
                {
                    return BadRequest("Invalid includes were specified.");
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        internal async Task<IActionResult> DoReadWith(I ID, IExpandIncludes includes)
        {
            //Handle missing entity here ObjectNotFoundResult
            //Handle incorrect includes with bad request.
            try
            {
                M result = await _service.GetAsync(ID, includes);

                if (result == null)
                    return NotFound();

                return Ok(result);
            }
            catch (InvalidOperationException ioe)
            {
                if (ioe.Message.Contains("The property") && ioe.Message.Contains("'Simple"))
                {
                    return BadRequest("Invalid includes were specified.");
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        internal async Task<IActionResult> DoRead()
        {
            return new OkObjectResult(await _service.GetAsync());
        }

        internal async Task<IActionResult> DoRead(I ID)
        {
            M model = await _service.GetAsync(ID);

            if (model == null)
            {
                return new NotFoundResult();
            }

            return new OkObjectResult(model);
        }

        internal async Task<IActionResult> DoUpdate(I ID, M update, string connectionID)
        {
            if (update == null)
            {
                this.LogModelErrors();
                return BadRequest();
            }

            SetUpdateModelProperties(ID, update);

            try
            {
                Validate(update, "update");
                if (!ModelState.IsValid)
                    return this.ApiValidationError();
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("not found"))
                {
                    return new NotFoundObjectResult(new GenericResponse<M>()
                    {
                        Data = update,
                        Success = false,
                        ErrorMessage = ex.Message
                    });
                }
                return this.SQLSaveError(new List<Exception>() { ex });
            }

            var result = await _service.UpdateAsync(update, connectionID);
            if (result == null)
            {
                await _logger.Error(update.BID, "UpdateAsync returned null", null);
                return this.SQLSaveError(_service.exceptions);
            }

            // Warning:
            // This does not re-request from server, so any DB Level side-effects
            // will not be included.
            return new OkObjectResult(update);
        }

        internal async Task<IActionResult> DoCreate(M newModel, Guid? tempGuid = null)
        {
            if (newModel == null)
            {
                var body = new StreamReader(Request.Body);
                body.BaseStream.Seek(0, SeekOrigin.Begin);
                var requestBody = body.ReadToEnd();

                this.LogModelErrors();
                return new BadRequestObjectResult(String.Format("Body missing or invalid.\n\n{0}", requestBody));
            }
            else
            {
                SetCreateModelProperties(newModel);
                try
                {
                    Validate(newModel, "create");
                    if (!ModelState.IsValid)
                        return this.ApiValidationError();
                }
                catch (Exception ex)
                {
                    if (ex.Message.ToLower().Contains("not found"))
                    {
                        return new NotFoundObjectResult(new GenericResponse<M>()
                        {
                            Data = newModel,
                            Success = false,
                            ErrorMessage = ex.Message
                        });
                    }
                    return this.SQLSaveError(new List<Exception>() { ex });
                }

                var result = await _service.CreateAsync(newModel, tempGuid);
                if (result == null)
                {
                    await _logger.Error(newModel.BID, "AddAsync returned null", null);
                    return this.SQLSaveError(_service.exceptions);
                }

                return new OkObjectResult(newModel);
            }
        }

        private void Validate(M newModel, string prefix)
        {
            ModelState.Clear();

            _service.DoBeforeValidate(newModel);
            TryValidateModel(newModel, prefix);
        }

        /// <summary>
        /// LogModelErrors
        /// </summary>
        /// <returns></returns>
        protected void LogModelErrors()
        {
            if (!ModelState.IsValid)
            {
                Console.BackgroundColor = ConsoleColor.Yellow;
                Console.ForegroundColor = ConsoleColor.Black;
                var validationErrors = ModelState.Values.Where(E => E.Errors.Count > 0)
                                        .SelectMany(stateVal => stateVal.Errors)
                                        .Select(err => err.Exception.Message.ToString())
                                        .ToList()
                                        .Aggregate((a, b) => a + "\n" + b);
                Console.WriteLine(validationErrors);
                this._logger.Error(User.BID().Value, validationErrors, new Exception(validationErrors));
            }
        }

        internal async Task<IActionResult> DoClone(I ID)
        {
            try
            {
                var result = await _service.CloneAsync(ID);
                if (result == null)
                {
                    this.LogModelErrors();

                    if (_service.exceptions != null && _service.exceptions.Count > 0)
                    {
                        return new BadRequestObjectResult("Clone failed - " + string.Join(",", _service.exceptions));
                    }
                    return BadRequest("Clone faied - clone returns null"); ;
                }
                else
                {
                    return new OkObjectResult(result);
                }
            }
            catch (Exception e)
            {
                //return a more meaningful message...
                return BadRequest(e.Message);
            }
        }

        internal async Task<IActionResult> DoDelete(I ID)
        {
            M toDelete = await _service.GetAsync(ID);

            if (toDelete == null)
            {
                await _service.QueueIndexForModel(ID);
                return NoContent();
            }
            else
            {
                // when this returns, the classtypeID = 0 (i don't know why at this point)
                var isDeleted = await _service.DeleteAsync(toDelete);

                if (isDeleted)
                    return new NoContentResult();
                else
                    return this.SQLSaveError(_service.exceptions);
            }
        }

        /// <summary>
        /// SetUpdateModelProperties
        /// </summary>
        /// <param name="ID">ID of record</param>
        /// <param name="update">Model being Updated</param>
        protected void SetUpdateModelProperties(I ID, M update)
        {
            update.ID = ID;
            SetCommonModelProperties(update);
        }

        /// <summary>
        /// SetCreateModelProperties
        /// </summary>
        /// <param name="newModel">New Model</param>
        protected void SetCreateModelProperties(M newModel)
        {
            SetCommonModelProperties(newModel);
        }

        /// <summary>
        /// SetCommonModelProperties
        /// </summary>
        /// <param name="model">Model</param>
        protected void SetCommonModelProperties(M model)
        {
            model.BID = this.User.BID().Value;
        }

        /// <summary>
        /// Checks the User's AccessType Claim and compares against the indicated UserAccessType
        /// </summary>
        /// <param name="accessType">UserAccessType enum to match or exceed for authorization</param>
        /// <returns></returns>
        protected bool isUserAuthorizedForAccessType(UserAccessType accessType)
        {
            var accessTypeClaim = User.Claims.FirstOrDefault(c => c.Type.Equals("accesstype"))?.Value;
            if (accessTypeClaim == null)
                return false;

            var userAccessType = Enum.Parse<UserAccessType>(accessTypeClaim);
            if (userAccessType >= accessType)
                return true;
            else
                return false;
        }
    }
}
