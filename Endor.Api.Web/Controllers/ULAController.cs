﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Endor.Models;
using Endor.Tenant;
using Endor.Security;
using Microsoft.AspNetCore.Authorization;
using Endor.EF;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.AEL;
using Endor.Api.Web.Classes;
using System;
using Endor.Api.Web.Services;
using Newtonsoft.Json;
using Endor.Api.Web.Classes.Responses;
using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// ULAController
    /// </summary>
    [Route("api/ula")]
    [Authorize]
    public class ULAController : Controller
    {
        private readonly ITenantDataCache _tenantCache;
        private readonly ApiContext _ctx;
        private readonly IMigrationHelper _migrationHelper;
        private static DateTime checkDate = new DateTime(2019, 1, 1);
        /// <summary>
        /// Ctor for Endpoints for getting info on your user
        /// </summary>
        /// <param name="tenantCache"></param>
        /// <param name="ctx"></param>
        /// <param name="etagCache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public ULAController(ITenantDataCache tenantCache, ApiContext ctx, EtagCache etagCache, IMigrationHelper migrationHelper)
        {
            _tenantCache = tenantCache;
            _ctx = ctx;
            _migrationHelper = migrationHelper;
            _migrationHelper.MigrateDb(_ctx);
        }

        /// <summary>
        /// Return the
        /// </summary>
        [HttpGet("check")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CheckULA))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> CheckULA()
        {
            var ULAInfo = new CheckULA();
            var optSvc = new OptionService(this._ctx, _migrationHelper);
            DateTime? MULAAcceptedDT = null;
            DateTime? EULAAcceptedDT = null;
            var res = await optSvc.Get(null, "EULA.AcceptedDT", User.BID().Value, null, null, this.User.UserLinkID(), null, null);
            if (res != null && res.Success && res.Value.Value !=null)
            {
                DateTime.TryParse(res.Value.Value, out DateTime tmpEULAAcceptedDT);
                EULAAcceptedDT = tmpEULAAcceptedDT;
            }
            

            res = await optSvc.Get(null, "MULA.AcceptedDT", User.BID().Value, null, null, /*this.User.UserLinkID()*/ null, null, null);
            if (res != null && res.Success && res.Value.Value != null)
            {
                DateTime.TryParse(res.Value.Value, out DateTime tmpMULAAcceptedDT);
                MULAAcceptedDT = tmpMULAAcceptedDT;
            }

            ULAInfo.MULA = new ULAData()
            {
                AcceptedDT = MULAAcceptedDT,
                Needed = false
            };

            var userLink = this._ctx.UserLink.FirstOrDefault(ul => ul.ID == User.UserLinkID());

            if (userLink != null && userLink.UserAccessType < UserAccessType.FranchiseStaff)
            {
                ULAInfo.MULA.Needed = (userLink.UserAccessType == UserAccessType.EmployeeAdministrator) &&
                                      (!MULAAcceptedDT.HasValue || (MULAAcceptedDT.Value.CompareTo(checkDate) <= 0));

                if (userLink.EmployeeID.HasValue)
                {
 
                    ULAInfo.EULA = new ULAData()
                    {
                        AcceptedDT = EULAAcceptedDT,
                        Needed = (userLink.UserAccessType <= UserAccessType.EmployeeAdministrator) &&
                                 (!EULAAcceptedDT.HasValue || (EULAAcceptedDT.Value.CompareTo(checkDate) <= 0))
                    };
                }
                else
                {
                    if (userLink.ContactID.HasValue)
                    {
                        //must be a contact
                        ULAInfo.EULA = new ULAData()
                        {
                            AcceptedDT = EULAAcceptedDT,
                            Needed = (!EULAAcceptedDT.HasValue || (EULAAcceptedDT.Value.CompareTo(checkDate) <= 0))
                        };
                    }
                    else
                    {
                        ULAInfo.EULA = new ULAData()
                        {
                            AcceptedDT = EULAAcceptedDT,
                            Needed = true
                        };
                    }
                }
            } else
            {
                ULAInfo.EULA = new ULAData()
                {
                    AcceptedDT = EULAAcceptedDT,
                    Needed = false
                };
            }
            string output = JsonConvert.SerializeObject(ULAInfo, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            return new OkObjectResult(output);

        }

        /// <summary>
        /// 
        /// </summary>
        [HttpGet("eula")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericULAResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetULA()
        {
            var bid = (short)User.BID();
            var aid = User.AID();
            var dmid = new DocumentStorage.Models.DMID()
            {
                ctid = (int?)ClassType.Business,
                classFolder = "association"
            };
            var storage = new StorageContext(bid, BucketRequest.Data, dmid, aid);
            var dm = new DocumentManager(this._tenantCache, storage);
            var dmItem = new DocumentStorage.Models.DMItem() { Path = "" };

            var userLink = this._ctx.UserLink.Where(ul => ul.ID == User.UserLinkID()).FirstOrDefault();
            if (userLink.UserAccessType < UserAccessType.FranchiseStaff)
            {
                if (userLink.EmployeeID.HasValue)
                {
                    dmItem.Name = "EULA.html";
                    var doc = await dm.ReadTextAsync(dmItem);
                    return new OkObjectResult(new GenericResponse<String>()
                    {
                        Success = true,
                        Data = doc
                    });
                }
                else if (userLink.ContactID.HasValue)
                {
                    //must be a contact
                    dmItem.Name = "CULA.html";
                    var doc = await dm.ReadTextAsync(dmItem);
                    return new OkObjectResult(new GenericResponse<String>()
                    {
                        Success = true,
                        Data = doc
                    });
                }
            }

            return new OkObjectResult("EULA is not defined for this User Access Type");
        }

        /// <summary>
        /// 
        /// </summary>
        [HttpGet("mula")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericULAResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetMULA()
        {
            var bid = (short)User.BID();
            var aid = User.AID();
            var dmid = new DocumentStorage.Models.DMID()
            {
                ctid = (int?)ClassType.Business,
                classFolder = "association"
            };
            var storage = new StorageContext(bid, BucketRequest.Data, dmid, aid);
            var dm = new DocumentManager(this._tenantCache, storage);
            var dmItem = new DocumentStorage.Models.DMItem() { Path = "" };

            var userLink = this._ctx.UserLink.Where(ul => ul.ID == User.UserLinkID()).FirstOrDefault();
            if ((userLink.UserAccessType == UserAccessType.EmployeeAdministrator) ||
                (userLink.UserAccessType >= UserAccessType.ContactAdministrator))
            {
                dmItem.Name = "MULA.html";
                var doc = await dm.ReadTextAsync(dmItem);
                return new OkObjectResult(new GenericResponse<String>()
                {
                    Success = true,
                    Data = doc
                });
            }

            return new OkObjectResult(new GenericResponse<String>()
            {
                Success = false,
                Data = "MULA is not defined for this User Access Type"
            });
        }

        /// <summary>
        /// Return the
        /// </summary>
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost("eulaaccepted")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CheckULA))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> AcceptEULA()
        {
            var bid = (short)User.BID();
            var aid = User.AID();

            var assocDMID = new DocumentStorage.Models.DMID() { ctid = (int?)ClassType.Business, classFolder = "association" };
            var assocDm = new DocumentManager(this._tenantCache, new StorageContext(bid, BucketRequest.Data, assocDMID, aid));
            var dmItem = new DocumentStorage.Models.DMItem() { Path = "" };
            var optSvc = new OptionService(this._ctx, _migrationHelper);
            var now = DateTime.UtcNow;
            var timestamp = (Int32)(now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            var userLink = this._ctx.UserLink.Where(ul => ul.ID == User.UserLinkID()).FirstOrDefault();
            if (userLink.UserAccessType < UserAccessType.FranchiseStaff)
            {
                if (userLink.EmployeeID.HasValue)
                {
                    dmItem.Name = "EULA.html";
                    var contents = await assocDm.ReadTextAsync(dmItem);

                    var bussinessDMID = new DocumentStorage.Models.DMID() { ctid = (int?)ClassType.Employee, classFolder = $"{userLink.EmployeeID}/EULA" };
                    var bussinessDm = new DocumentManager(this._tenantCache, new StorageContext(bid, BucketRequest.Data, bussinessDMID));
                    var EULAFileName = $"EULA-{timestamp}.html";
                    var EULAInfoFileName = $"EULA-Info-{timestamp}.json";
                    var upload = await bussinessDm.UploadTextAsync(EULAFileName, contents, (int)userLink.EmployeeID, (int)ClassType.Employee);
                    var eulaInfo = new EULAInfo()
                    {
                        UserName = User.UserName(),
                        AcceptedDT = DateTime.UtcNow,
                        EmployeeID = (short)User.EmployeeID(),
                        IP = Request.HttpContext.Connection.RemoteIpAddress?.ToString() ?? "",
                        ComputerName = "",
                        Browser = Request.Headers["User-Agent"] + "",
                    };
                    var uploadInfo = await bussinessDm.UploadTextAsync(EULAInfoFileName, JsonConvert.SerializeObject(eulaInfo), (int)userLink.EmployeeID, (int)ClassType.Employee);
                    await optSvc.Put(null, "EULA.AcceptedDT", now.ToString(), null, User.BID().Value, null, null, this.User.UserLinkID(), null, null);
                    return new OkObjectResult(new GenericResponse<EULAInfo>()
                    {
                        Success = true,
                        Data = eulaInfo
                    });
                }
                else if (userLink.ContactID.HasValue)
                {
                    //must be a contact
                    dmItem.Name = "CULA.html";
                    var contents = await assocDm.ReadTextAsync(dmItem);

                    var bussinessDMID = new DocumentStorage.Models.DMID() { ctid = (int?)ClassType.Contact, classFolder = $"{userLink.ContactID}/EULA" };
                    var bussinessDm = new DocumentManager(this._tenantCache, new StorageContext(bid, BucketRequest.Data, bussinessDMID));
                    var EULAFileName = $"EULA-{timestamp}.html";
                    var EULAInfoFileName = $"EULA-Info-{timestamp}.json";
                    var upload = await bussinessDm.UploadTextAsync(EULAFileName, contents, (int)userLink.ContactID, (int)ClassType.Contact);
                    var eulaInfo = new EULAInfo()
                    {
                        UserName = User.UserName(),
                        AcceptedDT = DateTime.UtcNow,
                        ContactID = userLink.ContactID,
                        IP = Request.HttpContext.Connection.RemoteIpAddress?.ToString() ?? "",
                        ComputerName = "",
                        Browser = Request.Headers["User-Agent"] + "",
                    };
                    var uploadInfo = await bussinessDm.UploadTextAsync(EULAInfoFileName, JsonConvert.SerializeObject(eulaInfo), (int)userLink.ContactID, (int)ClassType.Contact);
                    await optSvc.Put(null, "EULA.AcceptedDT", now.ToString(), null, User.BID().Value, null, null, this.User.UserLinkID(), null, null);
                    return new OkObjectResult(new GenericResponse<EULAInfo>()
                    {
                        Success = true,
                        Data = eulaInfo
                    });
                }

            }

            return BadRequest();
        }

        /// <summary>
        /// Return the
        /// </summary>
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost("mulaaccepted")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CheckULA))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> AcceptMULA()
        {
            var bid = (short)User.BID();
            var aid = User.AID();

            var assocDMID = new DocumentStorage.Models.DMID() { ctid = (int?)ClassType.Business, classFolder = "association" };
            var assocDm = new DocumentManager(this._tenantCache, new StorageContext(bid, BucketRequest.Data, assocDMID, aid));
            var dmItem = new DocumentStorage.Models.DMItem() { Path = "" };
            var optSvc = new OptionService(this._ctx, _migrationHelper);
            var now = DateTime.UtcNow;
            var timestamp = (Int32)(now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            dmItem.Name = "MULA.html";
            var contents = await assocDm.ReadTextAsync(dmItem);

            var bussinessDMID = new DocumentStorage.Models.DMID() { ctid = (int?)ClassType.Business, classFolder = $"{bid}/MULA" };
            var bussinessDm = new DocumentManager(this._tenantCache, new StorageContext(bid, BucketRequest.Data, bussinessDMID));
            var EULAFileName = $"MULA-{timestamp}.html";
            var EULAInfoFileName = $"MULA-Info-{timestamp}.json";
            var upload = await bussinessDm.UploadTextAsync(EULAFileName, contents, (int)bid, (int)ClassType.Business);
            var eulaInfo = new EULAInfo()
            {
                Subject = "MULA Accepted",
                UserName = User.UserName(),
                AcceptedDT = DateTime.UtcNow,
                EmployeeID = (short)User.EmployeeID(),
                IP = Request.HttpContext.Connection.RemoteIpAddress?.ToString() ?? "",
                ComputerName = "",
                Browser = Request.Headers["User-Agent"] + "",
            };
            var uploadInfo = await bussinessDm.UploadTextAsync(EULAInfoFileName, JsonConvert.SerializeObject(eulaInfo), (int)bid, (int)ClassType.Business);
            await optSvc.Put(null, "MULA.AcceptedDT", now.ToString(), null, User.BID().Value, null, null, null, null, null);
            await optSvc.Put(null, "EULA.AcceptedDT", now.ToString(), null, User.BID().Value, null, null, this.User.UserLinkID(), null, null);
            return new OkObjectResult(new GenericResponse<EULAInfo>()
            {
                Success = true,
                Data = eulaInfo
            });
        }
    }

    /// <summary>
    /// Base Class for ULA Acceptance data
    /// </summary>
    public class ULAData
    {
        /// <summary>
        /// Is ULA Acceptance Needed or not
        /// </summary>
        public bool Needed;
        /// <summary>
        /// Accepted DateTime
        /// </summary>
        public DateTime? AcceptedDT;
    }

    /// <summary>
    /// Base Class for ULA  data
    /// </summary>
    public class CheckULA
    {
        /// <summary>
        /// Master User License
        /// </summary>
        public ULAData MULA;
        /// <summary>
        /// End User License
        /// </summary>
        public ULAData EULA;
    }

    /// <summary>
    /// GenericULA Response
    /// </summary>
    public class GenericULAResponse : GenericResponse<string>
    {
        /// <summary>
        /// Success
        /// </summary>
        public new bool Success { get; set; }
        /// <summary>
        /// Data
        /// </summary>
        public new string Data { get; set; }
    }

}
