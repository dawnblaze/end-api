﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Tenant;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Option Category Controller
    /// </summary>
    [Authorize]
    [Route("api/options/category")]
    public class OptionCategoryController : GenericController<OptionCategory, OptionCategoryService, int>
    {
        /// <summary>
        /// Option Category Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OptionCategoryController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Returns a `GenericResponse` where `GenericResponse.Data` is an `OptionCategory` base on `OptionCategory.ID`
        /// </summary>
        /// <param name="id">finds a match using `OptionCategory.ID`</param>
        /// <param name="includeSectionBreadcrumbs"></param>
        [HttpGet]
        [Route("{id}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<OptionCategory>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> Get(int id, [FromQuery] bool includeSectionBreadcrumbs = false)
        {
            var resp = await this._service.Get(id, includeSectionBreadcrumbs);
            return resp.ToResult();
        }

        /// <summary>
        /// Returns a `GenericResponse` where `GenericResponse.Data` is an `OptionCategory` base on `OptionCategory.ID`
        /// </summary>
        /// <param name="id">finds a match using `OptionCategory.ID`</param>
        /// <param name="associationID"></param>
        /// <param name="bid"></param>
        /// <param name="locationID"></param>
        /// <param name="storeFrontID"></param>
        /// <param name="employeeID"></param>
        /// <param name="companyID"></param>
        /// <param name="contactID"></param>
        [HttpGet]
        [Route("{id}/definitionsAndValues")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SystemOptionDefinitionWithValue[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(string))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetSystemOptionDefinitionsWithValueByCategory(int id,
                byte? associationID = null,
                short? bid = null,
                short? locationID = null,
                short? storeFrontID = null,
                short? employeeID = null,
                int? companyID = null,
                int? contactID = null)
        {
            try
            {
                var resp = await this._service.GetSystemOptionDefinitionsWithValueByCategory(id, associationID, bid, locationID, storeFrontID, employeeID, companyID, contactID);
                return new OkObjectResult(resp);
            }
            catch(Exception e)
            {
                await this._logger.Error(User.BID().Value, "Error retrieving option definitions/values by category ID ", e);
                return new ObjectResult(e.Message)
                {
                    StatusCode = (int)HttpStatusCode.BadRequest
                };
            }
        }

        /// <summary>
        /// Returns a `GenericResponse` where `GenericResponse.Data` is an array of `OptionCategory`
        /// </summary>
        /// <param name="pageIndex">determines the pagination's `pageIndex` by skipping `n` number of items. Where `n`=`pageIndex*pageSize`</param>
        /// <param name="pageSize">determines the maximum number of items per page</param>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<OptionCategory[]>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> Get(int pageIndex = 0, int pageSize = 10)
        {
            var resp = await this._service.Get(pageIndex, pageSize);
            return resp.ToResult();
        }

        /// <summary>
        /// Returns a `GenericResponse` having `GenericResponse.OptionCategories` array and `GenericResponse.OptionSections` array
        /// </summary>
        /// <param name="level">filters the result using `OptionCategory.OptionLevels`</param>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OptionCategoryResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [Route("{level}/level")]
        public async Task<IActionResult> GetByLevel(byte level)
        {
            OptionCategoryResponse resp = await this._service.GetByLevel(level);
            return resp.ToResult();
        }

        /// <summary>
        /// Returns a `GenericResponse` having `GenericResponse.OptionCategories` array and `GenericResponse.OptionSections` array
        /// </summary>
        /// <param name="level">byte[]</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OptionCategoryResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [Route("levels")]
        public async Task<IActionResult> GetByLevels([FromQuery]ICollection<byte> level)
        {
            OptionCategoryResponse resp = await this._service.GetByLevel(level.ToArray());
            return resp.ToResult();
        }        
    }
}
