using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for CustomFieldLayoutDefinition
    /// https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/763199517/CustomFieldLayout+API
    /// </summary>
    [Authorize]
    [Route("api/cf/layout")]
    public class CustomFieldLayoutDefinitionController : CRUDController<CustomFieldLayoutDefinition, CustomFieldLayoutDefinitionService, short>
    {
        /// <summary>
        /// Api Endpoint for CustomFieldLayoutDefinition
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CustomFieldLayoutDefinitionController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) 
        : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns JSON Collection of CustomFieldLayoutDefinition Objects with embedded Containers and Elements.
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldLayoutDefinition), "Successfully retrieved the CustomFieldLayoutDefinition")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Read()
        {
            return await base.DoRead();
        }

        /// <summary>
        /// Returns JSON Collection of CustomFieldLayoutDefinition Objects with embedded Containers and Elements.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldLayoutDefinition), "Successfully retrieved the CustomFieldLayoutDefinition")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> FilteredRead([FromQuery]CustomFieldLayoutDefinitionFilter filter)
        {
            return new OkObjectResult(await this._service.GetAllWithFilters(filter));
        }        

        /// <summary>
        /// Returns the specified CustomFieldLayoutDefinition Object in JSON format  with embedded Containers and Elements.
        /// </summary>
        /// <param name="ID">The ID of the CustomFieldLayoutDefinition to be retrieved</param>
        /// <returns></returns>
        [HttpGet("{ID}/Obsolete")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldLayoutDefinition), "Successfully retrieved the CustomFieldLayoutDefinition")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "CustomFieldLayoutDefinition was not found")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.DoRead(ID);
        }

        /// <summary>
        /// Returns the specified CustomFieldLayoutDefinition Object in JSON format  with embedded Containers and Elements.
        /// </summary>
        /// <param name="ID">The ID of the CustomFieldLayoutDefinition to be retrieved</param>
        /// <param name="IncludeFullDetail"></param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldLayoutDefinition), "Successfully retrieved the CustomFieldLayoutDefinition")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "CustomFieldLayoutDefinition was not found")]
        public async Task<IActionResult> ReadFullLayout(short ID, [FromQuery]bool IncludeFullDetail = false)
        {
            return await base.DoRead(ID);
        }

        /// <summary>
        /// Returns JSON Collection of CustomFieldLayoutDefinition Objects in Simplelist.
        /// </summary>
        /// <returns></returns>
        [HttpGet("simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleCustomFieldLayoutDefinition[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<ICollection<SimpleCustomFieldLayoutDefinition>> GetSimpleList([FromQuery]CustomFieldLayoutDefinitionFilter filter)
        {
            return await this._service.GetSimpleListAsync(filter.WherePredicates()[0]);
        }

        /// <summary>
        /// Updates an existing CustomFieldLayoutDefinition Object.
        /// The revised object is passed-in in the body of the request message.
        /// The updated object is passed-back in the body of the response message.
        /// </summary>
        /// <param name="ID">The Id of the CustomFieldLayoutDefinition to update</param>
        /// <param name="update">Contains the updated CustomFieldLayoutDefinition model</param>
        /// <returns></returns>
        [HttpPut("Obsolete/{ID}")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldLayoutDefinition), "Successfully updated the CustomFieldLayoutDefinition")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid request properties")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "CustomFieldLayoutDefinition was not found")]
        public override async Task<IActionResult> Update(short ID, [FromBody] CustomFieldLayoutDefinition update)
        {
            return await base.DoUpdate(ID, update, Request.Headers[ConnectionIDHeaderKey]);
        }

        /// <summary>
        /// Updates an existing CustomFieldLayoutDefinition Object.
        /// </summary>
        /// <param name="ID">Layout ID</param>
        /// <param name="update">Layout JSON body</param>
        /// <param name="DoFullUpdate">deletes and reinserts all children and grandchildren to reflect the exact JSON body of the update, `defaults to false`</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldLayoutDefinition), "Successfully updated the CustomFieldLayoutDefinition")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid request properties")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "CustomFieldLayoutDefinition was not found")]
        public async Task<IActionResult> FullUpdate(short ID, [FromBody] CustomFieldLayoutDefinition update, [FromQuery]bool DoFullUpdate=false){
            //if(DoFullUpdate){
            //    await this._service.FullUpdateWithContainers(update, Request.Headers[ConnectionIDHeaderKey]);
            //}

            return await base.DoUpdate(ID, update, Request.Headers[ConnectionIDHeaderKey]);
        }

        /// <summary>
        /// Creates a new CustomFieldLayoutDefinition Object.
        /// The new object is passed-in in the body of the request message.
        /// The saved object is passed-back in the body of the response message.
        /// </summary>
        /// <param name="newModel">The CustomFieldLayoutDefinition model to create</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldLayoutDefinition), "Created a new CustomFieldLayoutDefinition")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]    
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid parameters were supplied or there was a different issue with the request")]
        public override async Task<IActionResult> Create([FromBody] CustomFieldLayoutDefinition newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.DoCreate(newModel, tempID);
        }

        /// <summary>
        /// Clone a CustomFieldLayoutDefinition. The content of the message contains the new cloned CustomFieldLayoutDefinition.
        /// </summary>
        /// <param name="ID">ID of the CustomFieldLayoutDefinition to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldLayoutDefinition))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source CustomFieldLayoutDefinition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source CustomFieldLayoutDefinition does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.DoClone(ID);
        }

        /// <summary>
        /// Deletes a CustomFieldLayoutDefinition
        /// </summary>
        /// <param name="ID">The ID of CustomFieldLayoutDefinition to be deleted</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "CustomFieldLayoutDefinition is no longer found")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Delete(short ID)
        {
            BooleanResponse resp = await this._service.CanDelete((short)ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                var res = await _service.ForceDeleteLayout(ID);
                return res.ToResult();
            }
            else
            {
                return new BadRequestObjectResult(resp);
            }
        }

        /// <summary>
        /// Sets the specified CustomFieldLayoutDefinition Active.  Always succeeds, even if the record is already active.
        /// </summary>
        /// <param name="ID">CustomFieldLayoutDefinition ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the CustomFieldLayoutDefinition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the CustomFieldLayoutDefinition does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            
            return resp.ToResult();
        }

        /// <summary>
        /// Sets the specified CustomFieldLayoutDefinition Inactive. Always succeeds, even if the record is already inactive.
        /// </summary>
        /// <param name="ID">CustomFieldLayoutDefinition ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the CustomFieldLayoutDefinition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the CustomFieldLayoutDefinition does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the CustomFieldLayoutDefinition can be deleted.
        /// </summary>
        /// <param name="ID">CustomFieldLayoutDefinition ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the CustomFieldLayoutDefinition does not exist.")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }
        
        /// <summary>
        /// Sets new sortindexes of multiple entities
        /// </summary>
        /// <param name="sortIndexRefs">a list of {ID,SortIndex}</param>
        /// <returns></returns>
        [HttpPut("SetSortIndex")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldLayoutDefinition[]), "Successfully retrieved the CustomFieldLayoutDefinition")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> SetSortIndexes([FromBody] List<IDSortIndexRef<short, short>> sortIndexRefs)
        {
            //return await this._service.SetSortIndexesAsync(sortIndexRefs);
            var resp = await this._service.SetSortIndexesAsync(sortIndexRefs);
            return resp.ToResult();
        }

        /// <summary>
        /// Try's to validate the data model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            //https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/763297820/CustomFieldLayoutDefinition+Object
            var layoutDef = model as CustomFieldLayoutDefinition;

            /*if(layoutDef.IsSystem){
                this.ModelState.AddModelError("IsSystem", "IsSystem=true is reserved for system generated layouts.");
                return false;
            }

            if(!layoutDef.IsSubTab){
                this.ModelState.AddModelError("IsSubtab", "IsSubTab=false is reserved for system generated layouts.");
                return false;
            }

            if(layoutDef.IsAllTab){
                this.ModelState.AddModelError("IsAllTab", "IsAllTab=true is reserved for system generated layouts.");
                return false;
            }*/

            return base.TryValidateModel(model, prefix);
        }        
    }
}