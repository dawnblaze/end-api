﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Tag Controller
    /// </summary>
    public class TagController : CRUDController<ListTag, TagService, short>
    {
        /// <summary>
        /// Tag API Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public TagController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns all tags specified in query parameters
        /// </summary>
        /// <param name="filters">Parameters to filter returned objects</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ListTag[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetWithFilters([FromQuery] TagFilter filters)
        {
            if (filters != null &&
                (filters.AssociatedClassTypeID.HasValue) ^ (filters.AssociatedID.HasValue))
            {
                return new BadRequestObjectResult("Both AssociatedClassTypeID and AssociatedID must be defined");
            }
            var result = await _service.GetWithFiltersAsync(filters);
            return new OkObjectResult(result.OrderBy(t => t.Name));
        }



        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the Tags
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single TaxGroup by ID
        /// </summary>
        /// <param name="ID">TaxGroup ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single Tag by ID
        /// </summary>
        /// <param name="ID">Tag ID</param>
        /// <param name="update">Updated Tag data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ListTag))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the TaxGroup does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the TaxGroup does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] ListTag update)
        {
            if (update.IsDeleted)
            {
                return new BadRequestObjectResult("Objects can not be created/updated in a deleted state. Use the Delete endpoint instead");
            }
            if (update.IsSystem)
            {
                return new BadRequestObjectResult("System objects can not be created or modified");
            }
            if (!this._service.CheckName(ID, update.Name.Trim()))
            {
                return new BadRequestObjectResult("Names must be unique where IsDelete = false");
            }
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new Tag
        /// </summary>
        /// <param name="newModel">New Tag data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ListTag))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Tag creation fails")]
        public override async Task<IActionResult> Create([FromBody] ListTag newModel, [FromQuery] Guid? tempID = null)
        {
            if (newModel.IsDeleted)
            {
                return new BadRequestObjectResult("Objects can not be created/updated in a deleted state. Use the Delete endpoint instead");
            }
            if (newModel.IsSystem)
            {
                return new BadRequestObjectResult("System objects can not be created or modified");
            }
            if (!this._service.CheckName(null, newModel.Name.Trim()))
            {
                return new BadRequestObjectResult("Names must be unique where IsDelete = false");
            }
            try
            {
                return await base.Create(newModel, tempID);
            } catch (Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }
        }

        /// <summary>
        /// Deletes a Tag by ID
        /// </summary>
        /// <param name="ID">ID of the TaxGroup to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ListTag))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Tag does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Tag does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            ListTag toDelete = await _service.GetAsync(ID);

            if (toDelete == null)
            {
                return NoContent();
            }
            else
            {
                toDelete.IsDeleted = true;
                try
                {
                    return await base.Update(ID, toDelete);
                }
                catch (Exception e)
                {
                    return new BadRequestObjectResult(e.Message);
                }
            }

        }

        #endregion

        #region actions

        /// <summary>
        /// Links a Tag by ID to a Object by ClassTypeID
        /// </summary>
        /// <param name="tagID">Tag ID</param>
        /// <param name="ctid">Class Type ID</param>
        /// <param name="objectid">Object ID</param>
        /// <returns></returns>
        [Route("{tagID}/action/link/{ctid}/{objectid}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<ListTag>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<ListTag>), description: "If the Tag or Item does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<ListTag>), description: "If the Tag or Item does not exist or fails to link.")]
        public async Task<IActionResult> LinkTaxItem([FromRoute]short tagID, [FromRoute]short ctid, [FromRoute]int objectid)
        {
            return (await this._service.LinkTag(tagID, ctid, objectid)).ToResult();
        }

        /// <summary>
        /// Unlinks a Tag by ID to a Tag by ID
        /// </summary>
        /// <param name="tagID">Tag ID</param>
        /// <param name="ctid">Class Type ID</param>
        /// <param name="objectid">Object ID</param>
        /// <returns></returns>
        [Route("{tagID}/action/unlink/{ctid}/{objectid}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<ListTag>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<ListTag>), description: "If the Tag or Item does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<ListTag>), description: "If the Tag or Item does not exist or fails to unlink.")]
        public async Task<IActionResult> UnLinkTaxItem([FromRoute]short tagID, [FromRoute]short ctid, [FromRoute]short objectid)
        {
            return (await this._service.UnLinkTag(tagID, ctid, objectid)).ToResult();
        }
        #endregion

        #region Helpers
        /*/// <summary>
        /// Validates ListTag
        /// </summary>
        /// <param name="model">The ListTag model to validate</param>
        /// <param name="prefix"> The key to use when looking up information in Microsoft.AspNetCore.Mvc.ControllerBase.ModelState</param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            if (model == null || !(model is ListTag))
            {
                this.ModelState.AddModelError("model", "model is not an account");
                return false;
            }
            ListTag tag = (ListTag)model;
            if (_service.IsSystem(tag.ID))
            {
                this.ModelState.AddModelError("System", "Cannot modify system value");
            }
            return base.TryValidateModel(model, prefix);
        }*/
        #endregion

        /// <summary>
        /// Pagination List
        /// </summary>
        /// <param name="filters">Query parameters to filter a property</param>
        /// <param name="skip">Number of items to skip</param>
        /// <param name="take">Number of items to take</param>
        /// <returns></returns>
        [HttpGet("paged")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ListTag[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetPagedList([FromQuery] TagFilter filters,
                                                        [FromQuery]int? skip = null,
                                                        [FromQuery]int? take = null)
        {
            if (filters != null &&
                (filters.AssociatedClassTypeID.HasValue) ^ (filters.AssociatedID.HasValue))
            {
                return new BadRequestObjectResult("Both AssociatedClassTypeID and AssociatedID must be defined");
            }
            var result = await _service.GetPagedListAsync(filters, skip, take, null);
            result.Results.OrderBy(t => t.Name);
            return new OkObjectResult(result);
        }
    }
}
