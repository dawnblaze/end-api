﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Order Contact Role Controller
    /// </summary>
    [Route("api/creditmemo/{parentID}/contactrole")]
    public class CreditMemoContactRoleController : CRUDChildController<OrderContactRole, OrderContactRoleService, int, int>
    {
        /// <summary>
        /// Constructs an OrderContactRoleController
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CreditMemoContactRoleController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {

        }

        /// <summary>
        /// Gets the Order's Contact Roles
        /// </summary>
        /// <param name="parentID">Order ID</param>
        /// <returns></returns>
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        [HttpGet("obsolete")]
        [Obsolete]
        public override async Task<IActionResult> Read(int parentID)
        {
            return await base.DoRead(parentID);
        }
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        /// <summary>
        /// Gets the specific Order Contact Role by ID and ParentID
        /// </summary>
        /// <param name="parentID">Order's ID</param>
        /// <param name="ID">Contact Role ID</param>
        /// <returns></returns>
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        [HttpGet("obsolete/{ID}")]
        [Obsolete]
        public override async Task<IActionResult> ReadByParentIdAndId(int parentID, int ID)
        {
            return await base.DoRead(parentID, ID);
        }
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        /// <summary>
        /// Gets the specific Order Contact Role by type and ParentID with Includes
        /// </summary>
        /// <param name="parentID">Order ID</param>
        /// <param name="includes">Order Contact Role includes</param>
        /// <param name="type">Order Contact Role Type</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRole[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWith(int parentID, [FromQuery]OrderContactRoleIncludes includes, [FromQuery]OrderContactRoleType? type = null)
        {
            if (type.HasValue)
            {
                Expression<Func<OrderContactRole, bool>> roleTypeClause = (t) => type.HasValue && t.RoleType == type.Value;
                return await FilteredReadWith(parentID, roleTypeClause, includes);
            }

            return await base.ReadWith(parentID, includes);
        }

        /// <summary>
        /// Gets the specific Order Contact Role by ID and ParentID with Includes
        /// </summary>
        /// <param name="parentID">Order ID</param>
        /// <param name="ID">Order Contact Role ID</param>
        /// <param name="includes">Order Contact Role includes</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> DoReadWithIncludes(int parentID, int ID, [FromQuery]OrderContactRoleIncludes includes = null)
        {
            return await base.ReadWith(parentID, ID, includes);
        }

        /// <summary>
        /// Updates the specific Order Contact Role by ID and ParentID with Includes
        /// </summary>
        /// <param name="parentID">Order ID</param>
        /// <param name="ID">Order Contact Role ID</param>
        /// <param name="updateModel">Updated OrderContactRole model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Update(int parentID, int ID, [FromBody] OrderContactRole updateModel)
        {
            return await base.Update(parentID, ID, updateModel);
        }

        /// <summary>
        /// Creates an OrderContact Role for the given Order
        /// </summary>
        /// <param name="parentID">Order ID</param>
        /// <param name="newModel">Order Contact Role model</param>
        /// <param name="tempGuid">Temporary GUID</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Create(int parentID, [FromBody] OrderContactRole newModel, Guid? tempGuid = null)
        {
            return await base.Create(parentID, newModel, tempGuid);
        }

        /// <summary>
        /// Delete's the specified Order Contact Role
        /// </summary>
        /// <param name="parentID">Order ID</param>
        /// <param name="id">Contact Role ID</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> Delete(int parentID, int id)
        {
            return base.Delete(parentID, id);
        }
    }
}
