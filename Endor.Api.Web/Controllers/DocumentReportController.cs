﻿// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Models;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Endpoints for getting info on your user
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public class DocumentReportController : Controller
    {
        private readonly ITenantDataCache _tenantCache;
        private readonly ApiContext _ctx;
        private readonly ITaskQueuer _taskQueuer;

        /// <summary>
        /// Ctor for Endpoints for getting info on your user
        /// </summary>
        /// <param name="tenantCache"></param>
        /// <param name="ctx"></param>
        /// <param name="migrationHelper"></param>
        /// <param name="taskQueuer"></param>
        public DocumentReportController(ITenantDataCache tenantCache, ApiContext ctx, IMigrationHelper migrationHelper, ITaskQueuer taskQueuer)
        {
            _tenantCache = tenantCache;
            _ctx = ctx;
            _taskQueuer = taskQueuer;
            migrationHelper.MigrateDb(_ctx);
        }

        /// <summary>
        /// Generates Document Report asynchronously
        /// </summary>
        /// <param name="documentReportType"></param>
        /// <param name="menuID"></param>
        /// <param name="DataID"></param>
        /// <param name="CustomFields"></param>
        /// <param name="OrderStatusFilter">Indicate the order filter to show in the statement report.( ShowInvoiced | ShowInvoicedInvoicing | ShowInvoicedInvoicingBuilt | ShowInvoicedInvoicingBuiltWIP )</param>
        /// <param name="Priority">Indicates the priority queue for this task.( low | medium | high )</param>
        /// <param name="Schedule">Indicates when the report should be run.  The value is passed as a string and may be one of the following values:"Immediate" → Place in queue immediately. Minutes(number) → Place in queue after the specified number of minutes have passed. DateTimeUTC → Place in the queue at the specified date and time.</param>
        /// <param name="OptionsValue"></param>
        /// <param name="DMFilePath">(Required) The path where the file is to be stored under this BID. Ex. Data/10000/reports/report.pdf</param>

        /// <returns></returns>
        [Route("generateasync/{documentReportType}/menu/{menuID}")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GenerateAsyncDocumentReportByMenuID(
            byte documentReportType, 
            int menuID, 
            [FromQuery] int[] DataID = null,
            [FromQuery] int[] CustomFields = null,
            [FromQuery] string OrderStatusFilter = "ShowInvoiced",
            [FromQuery] string Priority = "low",
            [FromQuery] string Schedule = "Immediate",
            [FromQuery] string OptionsValue = null,
            [FromQuery] string DMFilePath = null)
        {
            string errMsg = ValidateDMFilePath((DocumentReportType)documentReportType, DMFilePath);
            if (errMsg.Length != 0)
                return BadRequest(String.Format("Invalid DMFilePath. {0}", errMsg));

            this._taskQueuer.Priority = Priority;
            this._taskQueuer.Schedule = Schedule;
            return Ok(new { ReferenceID = await this._taskQueuer.GenerateDocumentReportByMenuID(this.User.AID().Value, this.User.BID().Value, documentReportType, menuID, DataID, CustomFields, OrderStatusFilter, OptionsValue, DMFilePath) });
        }

        /// <summary>
        /// Generates Document Report asynchronously
        /// </summary>
        /// <param name="documentReportType"></param>
        /// <param name="templateName"></param>
        /// <param name="DataID"></param>
        /// <param name="CustomFields"></param>
        /// <param name="OrderStatusFilter">Indicate the order filter to show in the statement report.( ShowInvoiced | ShowInvoicedInvoicing | ShowInvoicedInvoicingBuilt | ShowInvoicedInvoicingBuiltWIP )</param>
        /// <param name="Priority">Indicates the priority queue for this task.( low | medium | high )</param>
        /// <param name="Schedule">Indicates when the report should be run.  The value is passed as a string and may be one of the following values:"Immediate" → Place in queue immediately. Minutes(number) → Place in queue after the specified number of minutes have passed. DateTimeUTC → Place in the queue at the specified date and time.</param>
        /// <param name="OptionsValue"></param>
        /// <param name="DMFilePath">(Required) The path where the file is to be stored under this BID. Ex. Data/10000/reports/report.pdf</param>
        /// <returns></returns>
        [Route("generateasync/{documentReportType}/template/{templateName}")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GenerateAsyncDocumentReportByTemplateName(
            byte documentReportType, 
            string templateName,
            [FromQuery] int[] DataID = null,
            [FromQuery] int[] CustomFields = null,
            [FromQuery] string OrderStatusFilter = "ShowInvoiced", 
            [FromQuery] string Priority = "low",
            [FromQuery] string Schedule = "Immediate",
            [FromQuery] string OptionsValue = null,
            [FromQuery] string DMFilePath = null)
        {
            string errMsg = ValidateDMFilePath((DocumentReportType)documentReportType, DMFilePath);
            if (errMsg.Length != 0)
                return BadRequest(String.Format("Invalid DMFilePath. {0}", errMsg));

            this._taskQueuer.Priority = Priority;
            this._taskQueuer.Schedule = Schedule;
            return Ok(new { ReferenceID = await this._taskQueuer.GenerateDocumentReportByTemplateName(this.User.AID().Value, this.User.BID().Value, documentReportType, templateName, DataID, CustomFields, OrderStatusFilter, OptionsValue, DMFilePath) });
        }

        private static int AppliesToClassTypeFromDocumentReportType(DocumentReportType documentReportType)
        {
            switch (documentReportType)
            {
                case DocumentReportType.Estimate:
                    return (int)ClassType.Estimate;
                case DocumentReportType.WorkOrder:
                case DocumentReportType.Invoice:
                case DocumentReportType.PackingSlip:
                    return (int)ClassType.Order;
                case DocumentReportType.Proposal:
                    return (int)ClassType.Opportunity;
                case DocumentReportType.Statement:
                    return (int)ClassType.Company;
                case DocumentReportType.PurchaseOrder:
                    return 10100; //ReportingConstants.ClassType.PurchaseOrder;
                default:
                    return (int)ClassType.Order;
            }
        }

        private static string ValidateDMFilePath(DocumentReportType documentReportType, string DMFilePath)
        {
            string errMsg = "";

            if (string.IsNullOrWhiteSpace(DMFilePath))
            {
                errMsg = "DMFilePath is empty.";
            }
            else
            {
                // verify the path
                string[] filePath = DMFilePath.Split("/");

                var values = Enum.GetValues(typeof(Bucket)).Cast<Bucket>();
                // Check if it will be stored in Documents, Data, or Reports
                if (values.Where(x => x.ToString().ToLower() == filePath[0].ToLower()).Count() == 0)
                {
                    errMsg = "Invalid path. It must be either Documents, Data, or Reports. ";
                }

                int.TryParse(filePath[1], out int classTypeID);
                if (classTypeID != AppliesToClassTypeFromDocumentReportType(documentReportType))
                {
                    errMsg = "Invalid ClassTypeID. ";
                }

                // Check if it will be stored in a valid format. PDF, DOCX, or TX
                string[] fileFormat = filePath[filePath.Count() - 1].Split(".");
                switch (fileFormat[fileFormat.Count() - 1].ToLower())
                {
                    case "pdf":
                    case "docx":
                    case "tx":
                        break;
                    default:
                        errMsg += "Invalid format. It must be either PDF, DOCX, or TX. ";
                        break;
                }
            }

            return errMsg;
        }
    }
}
