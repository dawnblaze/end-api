using System;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Security;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using System.Transactions;
using System.Collections.Generic;
using System.Linq;
using MimeKit;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /*
    /// <summary>
    /// MessageHeaderController
    /// </summary>
    [Route("api/message")]
    [Authorize]
    public class MessageHeaderController : CRUDController<MessageHeader, MessageHeaderService, int>
    {

        private ApiContext _context;
        private ITenantDataCache _cache;
        private ITaskQueuer _taskQueuer;
        private EndorOptions _options;
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="options">Endor Options</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public MessageHeaderController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, EndorOptions options, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
            this._cache = cache;
            this._context = context;
            this._taskQueuer = taskQueuer;
            this._options = options;
        }


        /// <summary>
        /// GetSingleMessage
        /// </summary>
        /// <param name="headerID"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        [HttpGet("{headerID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(Message))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]                                
        public async Task<Message> GetSingleMessage([FromRoute]int headerID, [FromQuery]MessageIncludes includes){
            return await this._service.GetSingleMessage(headerID, includes);
        }

        /// <summary>
        /// GetFilteredMessages
        /// </summary>
        /// <param name="channelID"></param>
        /// <param name="includes"></param>
        /// <param name="filters"></param>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        [HttpGet("channel/{channelID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ICollection<Message>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]                        
        public async Task<ICollection<Message>> GetFilteredMessages([FromRoute]MessageChannelType channelID, 
                                                                    [FromQuery]MessageIncludes includes,
                                                                    [FromQuery]MessageFilters filters,
                                                                    [FromQuery]int skip=0,
                                                                    [FromQuery]int take=100){
            return await this._service.GetFilteredMessages(channelID, includes, filters, skip, take);
        }

        /// <summary>
        /// CreateMessage
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="tempID"></param>
        [HttpPost]
        public async Task<Message> CreateMessage([FromBody]Message msg, [FromQuery]Guid? tempID = null){
            return await this._service.CreateMessageAsync(msg, tempID);
        }

        /// <summary>
        /// Create Email Message
        /// </summary>
        /// <param name="msgBody"></param>
        [HttpPost("email")]
        public async Task<Message> CreateEmailMessage([FromBody]string msgBody)
        {
            Guid? tempID = null;
            
            //create To Participants
            List<MessageParticipantInfo> toParticipants = new List<MessageParticipantInfo>();
            toParticipants.AddRange(createParticipantInfo(MessageParticipantRoleType.To, ParseQueryString("ToEmail")));
            toParticipants.AddRange(createParticipantInfo(MessageParticipantRoleType.To, ClassType.Contact, ParseQueryInt("ToContactID")));
            toParticipants.AddRange(createParticipantInfo(MessageParticipantRoleType.To, ClassType.Employee, ParseQueryInt("ToEmployeeID"),true));
            
            //create cc Participate
            List<MessageParticipantInfo> ccParticipants = new List<MessageParticipantInfo>();
            ccParticipants.AddRange(createParticipantInfo(MessageParticipantRoleType.CC, ParseQueryString("CCEmail")));
            ccParticipants.AddRange(createParticipantInfo(MessageParticipantRoleType.CC, ClassType.Contact, ParseQueryInt("CCContactID")));
            ccParticipants.AddRange(createParticipantInfo(MessageParticipantRoleType.CC, ClassType.Employee, ParseQueryInt("CCEmployeeID"),true));

            //create From Participants
            List<MessageParticipantInfo> fromParticipants = new List<MessageParticipantInfo>();
            var employeeID = ParseQueryInt("From");
            fromParticipants.AddRange(createParticipantInfo(MessageParticipantRoleType.From, ClassType.Employee, employeeID,true));

            //create links
            List<MessageObjectLink> objectLinks = new List<MessageObjectLink>();
            List<int> orderID = ParseQueryInt("OrderID");
            List<int> estimateID = ParseQueryInt("EstimateID");
            List<int> companyID = ParseQueryInt("CompanyID");
            objectLinks.AddRange(createObjectLinks(ClassType.Order, orderID));
            objectLinks.AddRange(createObjectLinks(ClassType.Order, estimateID ));
            objectLinks.AddRange(createObjectLinks(ClassType.Order, companyID));
            //create message
            var subject = ParseQueryString("Subject");
            Message msg = new Message
            {
                ReceivedDT = DateTime.UtcNow,
                EmployeeID = employeeID.Any() ? (short)employeeID.First() : User.EmployeeID().GetValueOrDefault(),
                ParticipantID = User.EmployeeID().GetValueOrDefault(),
                Channels = MessageChannelType.Email,
                Subject = subject.Any() ? subject.First() : "",
                HasBody = true,
                Body = msgBody,
                BID = User.BID().GetValueOrDefault(),
                Participants = toParticipants.Concat(ccParticipants).Concat(fromParticipants).ToArray(),
                ObjectLinks = objectLinks.ToArray()
            };
            var msgToSend = await this._service.CreateMessageAsync(msg, tempID);

            var emailService = AtomCRUDService<EmailAccountData, short>.CreateService<EmailAccountService>(this._context, this._taskQueuer, this._cache, this._logger, () => User.BID().Value, this._rtmClient, _migrationHelper);
            var employeeAccountOverride = ParseQueryInt("EmailAccountID");
            MimeMessage emailMsg= new MimeMessage();
            
            
            await emailService.Value.SendEmail(msg,
                this._options.EncryptionPassPhrase,
                (short)employeeID.First(),
                orderID.Count > 0 ? (int?)orderID.First() : null,
                estimateID.Count > 0 ? (int?)estimateID.First() : null,
                companyID.Count > 0 ? (int?)companyID.First() : null,
                employeeAccountOverride.Count > 0 ? (short?)employeeAccountOverride.First() : null
                );
            return msgToSend;


        }
        private List<MessageObjectLink> createObjectLinks( ClassType type, List<int> IDs)
        {
            List<MessageObjectLink> returnList = new List<MessageObjectLink>();
            foreach (var id in IDs)
            {
                MessageObjectLink info = new MessageObjectLink()
                {
                    LinkedObjectClassTypeID = (int)type,
                    LinkedObjectID = id
                };
                returnList.Add(info);
            }
            return returnList;
        }

        private List<MessageParticipantInfo> createParticipantInfo(MessageParticipantRoleType roleType, List<string> emailAddresses)
        {
            List<MessageParticipantInfo> returnList = new List<MessageParticipantInfo>();
            foreach (var email in emailAddresses)
            {
                MessageParticipantInfo info = new MessageParticipantInfo()
                {
                    ParticipantRoleType = roleType,
                    Channel = MessageChannelType.Email,
                    UserName = email
                };
                returnList.Add(info);
            }
            return returnList;
        }

        private List<MessageParticipantInfo> createParticipantInfo(MessageParticipantRoleType roleType, ClassType type, List<int> IDs, bool IsEmployee = false)
        {
            List<MessageParticipantInfo> returnList = new List<MessageParticipantInfo>();
            foreach (var id in IDs)
            {
                var email = this._service.GetEmailsForClassType(type, id);
                if (email.Length > 0)
                {
                    MessageParticipantInfo info = new MessageParticipantInfo()
                    {
                        ParticipantRoleType = roleType,
                        Channel = MessageChannelType.Email,
                        UserName = email,
                        EmployeeID = IsEmployee ? (short?)id : null
                    };
                    returnList.Add(info);
                }
            }
            return returnList;
        }

        private List<string> ParseQueryString(string field)
        {
            return Request.Query[field].ToList();
        }

        private List<int> ParseQueryInt(string field)
        {
            List<int> returnList = new List<int>();
            var values = Request.Query[field].ToList();
            foreach (var val in values)
            {
                int x = 0;
                if (Int32.TryParse(val, out x))
                {
                    returnList.Add(x);
                }
            }
            return returnList;
        }
        /// <summary>
        /// Mark MessageHeader as deleted(will not do an actual delete on DB)
        /// </summary>
        /// <param name="headerID">int</param>
        /// <returns></returns>
        [HttpDelete("{headerID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]                
        public override async Task<IActionResult> Delete(int headerID)
        {
            return (await this._service.MarkAsDeleted(headerID, true, Request.Headers[ConnectionIDHeaderKey])).ToResult();
        }



        /// <summary>
        /// Undelete msgHeader
        /// </summary>
        /// <param name="headerID">int</param>
        /// <returns></returns>
        [HttpPost("{headerID}/action/undelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]                    
        public async Task<IActionResult> Undelete([FromRoute]int headerID){
            return (await this._service.MarkAsDeleted(headerID, false, Request.Headers[ConnectionIDHeaderKey])).ToResult();
        }

        /// <summary>
        /// Undelete multiple msgHeader
        /// </summary>
        /// <param name="headerIDs">int[]</param>
        /// <returns></returns>
        [HttpPost("action/undeletemany")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<IActionResult> UndeleteMany([FromBody]ICollection<int> headerIDs)
        {
            return (await this._service.MarkAsDeletedMany(headerIDs, false, Request.Headers[ConnectionIDHeaderKey])).ToResult();
        }

        /// <summary>
        /// Delete multiple msgHeader, (mark as deleted only, no actual delete on db)
        /// </summary>
        /// <param name="headerIDs">int[]</param>
        /// <returns></returns>
        [HttpPost("action/deletemany")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<IActionResult> DeleteMany([FromBody]ICollection<int> headerIDs)
        {
            return (await this._service.MarkAsDeletedMany(headerIDs, true, Request.Headers[ConnectionIDHeaderKey])).ToResult();
        }

        /// <summary>
        /// MarkAsRead
        /// </summary>
        /// <param name="headerID">int</param>
        /// <returns></returns>
        [HttpPost("{headerID}/action/markasread")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]        
        public async Task<IActionResult> MarkAsRead([FromRoute]int headerID){
            return (await this._service.MarkAsRead(headerID, true, Request.Headers[ConnectionIDHeaderKey])).ToResult();
        }

        /// <summary>
        /// MarkAsUnRead
        /// </summary>
        /// <param name="headerID">int</param>
        /// <returns></returns>
        [HttpPost("{headerID}/action/markasunread")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]                
        public async Task<IActionResult> MarkAsUnRead([FromRoute]int headerID){
            return (await this._service.MarkAsRead(headerID, false, Request.Headers[ConnectionIDHeaderKey])).ToResult();
        }

        /// <summary>
        /// MarkAllRead
        /// </summary>
        /// <param name="channelID">Endor.Models.MessageChannelType</param>
        /// <param name="EmployeeID">short?</param>
        [HttpPost("channel/{channelID}/action/markallread")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]                    
        public async Task<EntityActionChangeResponse> MarkAllRead([FromRoute]MessageChannelType channelID, [FromQuery] short? EmployeeID){
            if(!EmployeeID.HasValue){
                EmployeeID = (await this._service.GetUserLinkAsync(this.User))?.EmployeeID;
            }
            return await this._service.MarkAllRead(channelID, EmployeeID, true, Request.Headers[ConnectionIDHeaderKey]);
        }

        /// <summary>
        /// MarkAllDeleted
        /// </summary>
        /// <param name="channelID">Endor.Models.MessageChannelType</param>
        /// <param name="EmployeeID">short?</param>
        [HttpPost("channel/{channelID}/action/deleteall")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]                    
        public async Task<EntityActionChangeResponse> MarkAllDeleted([FromRoute]MessageChannelType channelID, [FromQuery] short? EmployeeID){
            if(!EmployeeID.HasValue){
                EmployeeID = (await this._service.GetUserLinkAsync(this.User))?.EmployeeID;
            }
            return await this._service.MarkAllDeleted(channelID, EmployeeID, true, Request.Headers[ConnectionIDHeaderKey]);
        }

        
        //overridden osbsolete base endpoints - needed to fix error on swagger
        #region ObsoleteEndpoints

        /// <summary>
        /// Base Read Overridden Endpoint
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete]
        public override async Task<IActionResult> Read()
        {
            return await base.DoRead();
        }        

        /// <summary>
        /// Base Read By ID Overridden Endpoint
        /// </summary>
        /// <param name="ID">ID Parameter of type I</param>
        /// <returns></returns>
        [HttpGet("obsolete/{ID}")]
        [Obsolete]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.DoRead(ID);
        }

        /// <summary>
        /// Base Update Overridden Endpoint
        /// </summary>
        /// <param name="ID">ID of record to be updated of type I</param>
        /// <param name="update">Model of record to be updated</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [Obsolete]
        public override async Task<IActionResult> Update(int ID, [FromBody] MessageHeader update)
        {
            return await base.DoUpdate(ID, update, Request.Headers[ConnectionIDHeaderKey]);
        }

        /// <summary>
        /// Base Create Overridden Endpoint
        /// </summary>
        /// <param name="newModel">Model of record to be updated</param>
        /// <param name="tempID">Optional Temp ID</param>
        /// <returns></returns>
        [HttpPost("obsolete")]
        [Obsolete]
        public override async Task<IActionResult> Create([FromBody] MessageHeader newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.DoCreate(newModel, tempID);
        }

        /// <summary>
        /// Base Clone Overridden Endpoint
        /// </summary>
        /// <param name="ID">ID of record to be cloned</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [Obsolete]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.DoClone(ID);
        }

        #endregion
    }*/
}