﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.CBEL.Common;
using Endor.CBEL.Exceptions;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.CBEL.Metadata;
using Endor.Api.Web.Annotation;
using Endor.Pricing;
using Microsoft.Extensions.Caching.Memory;
using Endor.CBEL.Autocomplete;
using Endor.Models.Autocomplete;
using Microsoft.AspNetCore.Authorization;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Assembly Controller
    /// </summary>
    [Route("API/AssemblyPart")]
    public class AssemblyController : CRUDController<AssemblyData, AssemblyService, int>, ISimpleListableViewController<SimpleAssemblyData, int>
    {
        private readonly Lazy<PricingService> _pricingService;

        /// <summary>
        /// List service for Assembly controller
        /// </summary>
        public ISimpleListableViewService<SimpleAssemblyData, int> ListableService => this._service;

        /// <summary>
        /// Assembly Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="pricingEngine">Pricing service</param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <param name="memoryCache">Memory cache</param>
        /// <param name="autocompleteEngine"></param>
        public AssemblyController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IPricingEngine pricingEngine, IMigrationHelper migrationHelper, IMemoryCache memoryCache, IAutocompleteEngine autocompleteEngine)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper, (BID) => { 
                return new AssemblyService(context, BID, taskQueuer, cache, logger, rtmClient, migrationHelper, autocompleteEngine);
            })
        {
            this._pricingService = new Lazy<PricingService>(() => new PricingService(context, logger, this.User.BID().Value, rtmClient, pricingEngine, migrationHelper, cache, memoryCache, taskQueuer));
        }

        /// <summary>
        /// Returns all of the AssemblyDatas
        /// can be filtered by IsActive
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AssemblyData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If AssemblyData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] AssemblyFilter filters, [FromQuery] AssemblyDataIncludes includes)
        {
            return new OkObjectResult(await _service.GetWithFiltersAsync(filters, includes));
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the AssemblyDatas
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("ObsoleteGetAll")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AssemblyData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If AssemblyData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns a single AssemblyData by ID
        /// </summary>
        /// <param name="ID">AssemblyData ID</param>
        /// <param name="includes"></param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AssemblyData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If AssemblyData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWith(int ID, [FromQuery] AssemblyDataIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }

        /// <summary>
        /// Returns multiple layouts given a AssemblyData.ID
        /// if
        /// </summary>
        /// <param name="ID">AssemblyData ID</param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet("{ID}/layout")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AssemblyLayout[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If AssemblyData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetLayouts(int ID, [FromQuery] AssemblyLayoutFilter filter)
        {
            try
            {
                AssemblyLayout[] result = await _service.GetLayouts(ID, filter);

                if (result == null)
                    return NotFound();

                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Returns a single AssemblyData by ID
        /// </summary>
        /// <param name="ID">AssemblyData ID</param>
        /// <returns></returns>
        [HttpGet("Obsolete/{ID}")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AssemblyData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If AssemblyData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single AssemblyData by ID
        /// </summary>
        /// <param name="ID">AssemblyData ID</param>
        /// <param name="update">Updated AssemblyData data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AssemblyData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If AssemblyData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the AssemblyDatas does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] AssemblyData update)
        {
            try
            {
                if (update.AssemblyType != AssemblyType.MachineTemplate)
                {
                    VariableValidationResult variableValidationResult = await this._service.ValidateVariables(update.Variables);

                    if (!variableValidationResult.Valid) return variableValidationResult.Response.ToResult();
                }

                var result = await DoUpdate(ID, update, Request.Headers[ConnectionIDHeaderKey]);

                return result;
            }
            catch (CBELCompilationException e)
            {
                string error = String.Join("\r\n", e.AssemblyResponse.Errors.Select(t => $"{t.VariableName}:{t.Message}"));
                GenericResponse<AssemblyData> genericResponse = new GenericResponse<AssemblyData>() { ErrorMessage = error };
                return genericResponse.ToResult();
            }
            catch (InvalidOperationException e)
            {
                GenericResponse<AssemblyData> genericResponse = new GenericResponse<AssemblyData>() { ErrorMessage = e.Message };
                return genericResponse.ToResult();
            }
        }

        /// <summary>
        /// Assembly Clone Endpoint
        /// </summary>
        /// <param name="ID">ID of record to be cloned</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        public override async Task<IActionResult> Clone(int ID)
        {
            try
            {
                var result = await _service.CloneAssembly(ID);
                if (result == null)
                {
                    this.LogModelErrors();

                    if (_service.exceptions != null && _service.exceptions.Count > 0)
                    {
                        return new BadRequestObjectResult("Clone failed - " + string.Join(",", _service.exceptions));
                    }
                    return BadRequest("Clone faied - clone returns null"); ;
                }
                else
                {
                    return new OkObjectResult(result);
                }
            }
           catch (Exception e)
            {
                GenericResponse<AssemblyData> genericResponse = new GenericResponse<AssemblyData>() { ErrorMessage = e.Message };
                return genericResponse.ToResult();
            }
        }

        /// <summary>
        /// Creates a new AssemblyData
        /// </summary>
        /// <param name="newModel">Updated AssemblyData data model</param>
        /// <param name="tempID">Temporary GUID referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AssemblyData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the AssemblyDatas creation fails")]
        public override async Task<IActionResult> Create([FromBody] AssemblyData newModel, [FromQuery] Guid? tempID = null)
        {
            try
            {
                if (newModel.AssemblyType != AssemblyType.MachineTemplate)
                {
                    VariableValidationResult variableValidationResult = await this._service.ValidateVariables(newModel.Variables);

                    if (!variableValidationResult.Valid) return variableValidationResult.Response.ToResult();
                }

                var result = await base.Create(newModel, tempID);
                return result;
            }
            catch (CBELCompilationException e)
            {
                string error = String.Join("\r\n", e.AssemblyResponse.Errors.Select(t => $"{t.VariableName}:{t.Message}"));
                GenericResponse<AssemblyData> genericResponse = new GenericResponse<AssemblyData>() { ErrorMessage = error };
                return genericResponse.ToResult();
            }
            catch (InvalidOperationException e)
            {
                GenericResponse<AssemblyData> genericResponse = new GenericResponse<AssemblyData>() { ErrorMessage = e.Message };
                return genericResponse.ToResult();
            }
        }

        /// <summary>
        /// Deletes a AssemblyData by ID
        /// </summary>
        /// <param name="ID">ID of the AssemblyData to delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If AssemblyData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the AssemblyDatas does not exist or the deletion fails")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }

        #endregion Overridden CRUD Methods

        /// <summary>
        /// Returns true | false based on whether the AssemblyData can be deleted.
        /// </summary>
        /// <param name="ID">ID of the AssemblyData to delete</param>
        /// <returns></returns>
        [HttpGet("{ID}/Action/CanDelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(int ID)
        {
            return (await this._service.CanDelete(ID)).ToResult();
        }

        /// <summary>
        /// Sets a AssemblyData to Active
        /// </summary>
        /// <param name="ID">AssemblyData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/Action/SetActive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the AssemblyData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the AssemblyData does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(int ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets a AssemblyData to Inactive
        /// </summary>
        /// <param name="ID">AssemblyData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/Action/SetInactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the AssemblyData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the AssemblyData does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(int ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Gets a Simple list of AssemblyDatas
        /// </summary>
        /// <returns></returns>
        [Route("SimpleList")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleAssemblyData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleAssemblyData[]> FilteredSimpleList([FromQuery] SimpleAssemblyFilters filters)
        {
            return await this.GetSimpleList<SimpleAssemblyData, int>(User.BID().Value, filters);
        }

        /// <summary>
        /// Gets a SimpleList base on categoryIDs and ids
        /// </summary>
        /// <returns></returns>
        [Route("simplelist/categoryandid")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleAssemblyData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<ICollection<SimpleAssemblyData>> GetByIDsAndCategories([FromQuery]ICollection<int> id, [FromQuery]ICollection<int> categoryid)
        {
            return await this._service.GetByIDsAndCategoriesAsync(id, categoryid);
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [HttpGet("ObsoleteSimpleList")]
        [Obsolete]
        public Task<SimpleAssemblyData[]> SimpleList()
        {
            return this.FilteredSimpleList(null);
        }

        /// <summary>
        /// links a Assembly category to a Assembly part
        /// </summary>
        /// <param name="AssemblyDataID">AssemblyData.ID</param>
        /// <param name="assemblyCategoryID">AssemblyCategory.ID</param>
        /// <returns></returns>
        [HttpPost("{AssemblyDataID}/Action/LinkAssemblyCategory/{assemblyCategoryID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> LinkAssembly([FromRoute]int AssemblyDataID, [FromRoute]short assemblyCategoryID)
        {
            return (await this._service.LinkAssemblyCategory(AssemblyDataID, assemblyCategoryID, true)).ToResult();
        }

        /// <summary>
        /// unlinks a assembly category from a assembly part
        /// </summary>
        /// <param name="assemblyDataID">assemblyData.ID</param>
        /// <param name="AssemblyCategoryID">assemblyCategory.ID</param>
        /// <returns></returns>
        [HttpPost("{assemblyDataID}/Action/UnlinkAssemblyCategory/{AssemblyCategoryID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> UnLinkAssembly([FromRoute]int assemblyDataID, [FromRoute]short AssemblyCategoryID)
        {
            return (await this._service.LinkAssemblyCategory(assemblyDataID, AssemblyCategoryID, false)).ToResult();
        }

        /// <summary>
        /// Validates an Assembly
        /// </summary>
        /// <param name="model">JSON format of full AssemblyData object including tables/variables</param>
        /// <returns></returns>
        [HttpPost("Action/Validate")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> Validate([FromBody] AssemblyData model)
        {
            AssemblyCompilationResponse result = await this._service.Validate(model);

            if (result.Success)
                return Ok(result);

            return BadRequest(result);
        }

        /// <summary>
        /// Retrieves the member list of the assembly used for code-complete and case-correction.  If the ID is not specifid, the method returns the list of all Members on a generic assembly (without any variables).
        /// </summary>
        /// <param name="ID">Assembly ID</param>
        /// <returns></returns>
        [HttpGet("{ID}/memberlist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> GetMemberList([FromRoute]int ID)
        {
            try
            {
                SortedDictionary<string, ICBELMember> memberList = await _service.GetMemberList(ID);
                return Ok(memberList);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        /// <summary>
        /// Retrieves the member list of the assembly used for code-complete and case-correction.  If the ID is not specifid, the method returns the list of all Members on a generic assembly (without any variables).
        /// </summary>
        /// <returns></returns>
        [HttpGet("memberlist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> GetMemberList()
        {
            try
            {
                SortedDictionary<string, ICBELMember> memberList = await _service.GetMemberList(null);
                return Ok(memberList);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        /// <summary>
        /// Attempt to validate a model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            if (model is AssemblyData assemblyData)
            {
                if (!Enum.IsDefined(typeof(AssemblyPricingType), assemblyData.PricingType)) 
                    this.ModelState.AddModelError(prefix, "Invalid Pricing Type specified");

                if (assemblyData.Variables != null)
                {
                    foreach (var item in assemblyData.Variables)
                    {
                        if (string.IsNullOrWhiteSpace(item.Name))
                        {
                            item.Name = !string.IsNullOrWhiteSpace(item.Label) ? item.Label.Replace(" ", "") : "";
                        }
                        if (!CSharpIdentifiers.IsValidParsedIdentifier(item.Name))
                        {
                            this.ModelState.AddModelError(prefix, $"Variable has an invalid name: {item.Name}");
                        }

                        if (item.IsFormula && (!String.IsNullOrWhiteSpace(item.DefaultValue) && item.DefaultValue[0] != '='))
                        {
                            this.ModelState.AddModelError(prefix, $"Variable is marked IsFormula=true but DefaultValue does not start with an = sign:{item.Name}");
                        }
                    }
                }

                if (assemblyData.Tables != null)
                {
                    foreach (var item in assemblyData.Tables)
                    {
                        if (!CSharpIdentifiers.IsValidParsedIdentifier(item.VariableName))
                        {
                            this.ModelState.AddModelError(prefix, $"Table has an invalid name: {item.VariableName}");
                        }
                    }
                }
            }
            else
            {
                this.ModelState.AddModelError(prefix, "Model failed deserialization");
            }

            return base.TryValidateModel(model, prefix);
        }

        /// <summary>
        /// ValidateDataJson
        /// </summary>
        /// <param name="json"></param>
        /// <param name="AssemblyDataID"></param>
        /// <param name="layoutType"></param>
        /// <returns></returns>
        [HttpPost("{AssemblyDataID}/ValidateDataJson")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<ICollection<string>> ValidateDataJson([FromBody]string json, int AssemblyDataID, [FromQuery] AssemblyLayoutType? layoutType = null)
        {
            return await this._service.ValidateAssemblyDataJSON(json, AssemblyDataID, layoutType);
        }

        [HttpPost("{machineTemplateID}/compute")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(Dictionary<string, VariableValue>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(Dictionary<string, VariableValue>), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> Compute(int machineTemplateID, [FromBody]Dictionary<string, VariableValue> request)
        {
            if (machineTemplateID == 0)
                return BadRequest("MachineTemplateID is missing of invalid.");

            try
            {
                Dictionary<string, VariableValue> result = await _pricingService.Value.ComputeAssembly(machineTemplateID, request);
                return Ok(result);
            }

            catch (Exception err)
            {
                return StatusCode(500, err);
            }   
        }
        /// <summary>
        /// Returns embedded assembly data by Id if it exists
        /// </summary>
        /// <returns></returns>
        [HttpGet("embedded/{id}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AssemblyData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If EmbeddedAssemblyData does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public IActionResult ReadEmbeddedAssembly(int id)
        {
            var assembly = this._service.GetEmbeddedAssembly(EmbeddedAssemblySingleton.GetInstance(), id);
            if (assembly == null)
            {
                return NotFound("Embedded Assembly not found.");
            }
            return new OkObjectResult(assembly);
        }

        /// <summary>
        /// Gets a SimpleList base on variableID and ProfileName
        /// </summary>
        /// <returns></returns>
        [Route("simplelist/byvariable/{variableID}")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleMachineData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<ICollection<NamedQuark>> GetByVariableID(int variableID, [FromQuery] int? machineID, [FromQuery] string profileName)
        {
            return await this._service.GetByVariableAndProfile(variableID, machineID, profileName);
        }

        /// <summary>
        /// Returns Assembly TS code used by the client
        /// </summary>
        /// <param name="ID">ID of the Assembly</param>
        /// <returns></returns>
        [HttpGet("{ID}/definition/class")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(string))] 
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(string), description: "If the action was unsuccessful")] 
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> GetDefinition_Class(int ID)
        {
            try
            {
                return new ContentResult
                {
                    ContentType = "text/typescript",
                    StatusCode = (int)HttpStatusCode.OK,
                    Content = await _service.GetDefinition_Class(ID)
                };
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>
        /// Returns Assembly TS code as local declare statements
        /// Returns the base assembly class with no variables if no ID is provided
        /// </summary>
        /// <param name="ID">ID of the Assembly</param>
        /// <returns></returns>
        [HttpGet("{ID}/definition/local")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(string))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(string), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> GetDefinition_Local(int ID)
        {
            try
            {
                return new ContentResult
                {
                    ContentType = "text/typescript",
                    StatusCode = (int)HttpStatusCode.OK,
                    Content = await _service.GetDefinition_Local(ID)
                };
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>
        /// Returns Assembly TS code as local declare statements
        /// Returns the assembly class from the provided path
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        [HttpGet("{ID}/definition/local/path")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(string))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(string), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> GetDefinition_Local_Path([FromRoute] int ID, [FromQuery] string path)
        {
            string result;

            try
            {
                result = await _service.GetDefinition_Local_Path(ID, path);
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }

            if (!string.IsNullOrWhiteSpace(result))
                return new ContentResult
                {
                    ContentType = "text/typescript",
                    StatusCode = (int)HttpStatusCode.OK,
                    Content = result
                };
            else
                return NoContent();
        }

        /// <summary>
        /// Returns Assembly TS code as local declare statements
        /// Returns the assembly class from the provided path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="assemblyVariables"></param>
        /// <returns></returns>
        [HttpPost("definition/local/path")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(string))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(string), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> PostDefinition_Local_Path([FromQuery] string path, [FromBody] List<AssemblyVariableAutocompleteBrief> assemblyVariables)
        {
            string result = "";

            if (string.IsNullOrWhiteSpace(path))
                return BadRequest("Path not defined");

            try
            {
                result = await _service.PostDefinition_Local_Path(assemblyVariables, path);
            }
            catch(KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }

            if (!string.IsNullOrWhiteSpace(result))
                return new ContentResult
                {
                    ContentType = "text/typescript",
                    StatusCode = (int)HttpStatusCode.OK,
                    Content = result
                };
            else
                return NoContent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assemblyVariableAutocompleteBrief"></param>
        /// <returns></returns>
        [HttpPost("definition/local/variables")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(string))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(string), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> GetVariables_Local([FromBody] List<AssemblyVariableAutocompleteBrief> assemblyVariableAutocompleteBrief)
        {
            try
            {
                return new ContentResult
                {
                    ContentType = "text/typescript",
                    StatusCode = (int)HttpStatusCode.OK,
                    Content = await _service.GetVariables_Local(assemblyVariableAutocompleteBrief)
                };
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>
        /// Returns the current Assembly C# code
        /// </summary>
        /// <param name="ID">ID of the Assembly</param>
        /// <returns></returns>
        [HttpGet("{ID}/definition/server")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(string))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(string), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> GetDefinition_Server(int ID)
        {
            try
            {
                return new ContentResult
                {
                    ContentType = "text/cs",
                    StatusCode = (int)HttpStatusCode.OK,
                    Content = await _service.GetDefinition_Server(ID)
                };
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>
        /// Deletes an assembly DLL
        /// </summary>
        /// <param name="ID">ID of the AssemblyData to delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}/dll/delete")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If Assembly does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Assembly does not exist or the deletion fails")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> DeleteAssemblyDLL(int ID)
        {
            if (User.BID() == null)
                return Unauthorized();

            if (!isUserAuthorizedForAccessType(UserAccessType.SupportStaff))
                return Unauthorized();

            return Ok(await this._service.DeleteDLL(User.BID().Value, ClassType.Assembly.ID(), ID, User.UserID()));
        }

        /// <summary>
        /// Deletes all assembly DLLs
        /// </summary>
        /// <returns></returns>
        [HttpDelete("all/dll/delete")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If Assembly does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Assembly does not exist or the deletion fails")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> DeleteAllAssemblyDLL()
        {
            if (User.BID() == null)
                return Unauthorized();

            if (!isUserAuthorizedForAccessType(UserAccessType.SupportStaff))
                return Unauthorized();

            return Ok(await this._service.DeleteAllDLL(User.BID().Value, User.UserID()));
        }
    }
}
