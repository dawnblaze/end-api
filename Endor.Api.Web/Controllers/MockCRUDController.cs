﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Base Mock CRUD Controller
    /// </summary>
    /// <typeparam name="M">Model type</typeparam>
    /// <typeparam name="I">ID type</typeparam>
    [Authorize]
    public abstract class MockCRUDController<M, I> : ICRUDController<M, I>
        where M : class, IAtom<I>
        where I : struct, IConvertible
    {
        private MockCRUDService<M, I> _service;
        /// <summary>
        /// Gets the Mock CRUD Service
        /// </summary>
        protected MockCRUDService<M, I> Service
        {
            get
            {
                if (_service == null)
                    _service = GetService();

                return _service;
            }
        }

        /// <summary>
        /// Gets all Mock data
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual async Task<IActionResult> Read()
        {
            return await Service.DoRead();
        }

        /// <summary>
        /// Gets a single Mock data item by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        public virtual async Task<IActionResult> ReadById(int ID)
        {
            return await Service.DoRead(ID);
        }

        /// <summary>
        /// Updates the Mock data item
        /// </summary>
        /// <param name="ID">Model's ID</param>
        /// <param name="update">Update Model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        public virtual async Task<IActionResult> Update(int ID, [FromBody] M update)
        {
            return await Service.DoUpdate(ID, update);
        }

        /// <summary>
        /// Creates a new Mock data item
        /// </summary>
        /// <param name="newModel">New Model</param>
        /// <param name="tempID">Temp GUID</param>
        /// <returns></returns>
        [HttpPost]
        public virtual async Task<IActionResult> Create([FromBody] M newModel, [FromQuery] Guid? tempID = null)
        {
            return await Service.DoCreate(newModel, tempID);
        }

        /// <summary>
        /// Clones a Mock data item by ID
        /// </summary>
        /// <param name="ID">Model's ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        public virtual async Task<IActionResult> Clone(int ID)
        {
            return await Service.DoClone(ID);
        }

        /// <summary>
        /// Deletes the Mock data item
        /// </summary>
        /// <param name="ID">Model ID</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        public virtual async Task<IActionResult> Delete(int ID)
        {
            return await Service.DoDelete(ID);
        }

        /// <summary>
        /// Sets the specified Mock data item active
        /// </summary>
        /// <param name="ID">Model ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setactive")]
        [HttpPost]
        public virtual async Task<IActionResult> SetActive(int ID)
        {
            GenericResponse<M> gr = new GenericResponse<M>()
            {
                Success = true,
                Message = "Successfully set to active."
            };
            return new OkObjectResult(await Task.FromResult(gr));
        }

        /// <summary>
        /// Sets the specified Mock data item inactive
        /// </summary>
        /// <param name="ID">Model ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setinactive")]
        [HttpPost]
        public virtual async Task<IActionResult> SetInactive(int ID)
        {
            GenericResponse<M> gr = new GenericResponse<M>()
            {
                Success = true,
                Message = "Successfully set to inactive."
            };
            return new OkObjectResult(await Task.FromResult(gr));
        }

        /// <summary>
        /// Sets the specified Mock data items active
        /// </summary>
        /// <param name="ids">Array of Model ID's</param>
        /// <returns></returns>
        [Route("action/setactive")]
        [HttpPost]
        public virtual async Task<IActionResult> SetActiveByIds([FromBody] int[] ids)
        {
            GenericResponse<M> gr = new GenericResponse<M>()
            {
                Success = true,
                Message = "Successfully set records to active."
            };
            return new OkObjectResult(await Task.FromResult(gr));
        }

        /// <summary>
        /// Sets the specified Mock data items inactive
        /// </summary>
        /// <param name="ids">Array of Model ID's</param>
        /// <returns></returns>
        [Route("action/setinactive")]
        [HttpPost]
        public virtual async Task<IActionResult> SetInactiveByIds([FromBody] int[] ids)
        {
            GenericResponse<M> gr = new GenericResponse<M>()
            {
                Success = true,
                Message = "Successfully set records to inactive."
            };
            return new OkObjectResult(await Task.FromResult(gr));
        }

        /// <summary>
        /// Gets the Mock CRUD Service
        /// </summary>
        /// <returns></returns>
        protected abstract MockCRUDService<M, I> GetService();
    }
}
