﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Swashbuckle.AspNetCore.SwaggerGen;

using Endor.Api.Web.Annotation;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.Extensions.Caching.Memory;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for Orders
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public abstract partial class BaseTransactionHeaderController<M, S> : CRUDController<M, S, int>, ISimpleListableViewController<SimpleOrderData, int>
        where M : TransactionHeaderData
        where S : BaseTransactionService<M>
    {
        /// <summary>
        /// Lazy Loaded OrderNote Service
        /// </summary>
        protected readonly Lazy<OrderNoteService> _orderNoteService;
        /// <summary>
        /// Lazy Loaded OrderOrderLink Service
        /// </summary>
        protected readonly Lazy<OrderOrderLinkService<M>> _orderOrderLinkService;
        /// <summary>
        /// Lazy Loaded Order ContactRole Service
        /// </summary>
        protected readonly Lazy<OrderContactRoleService> _orderContactRoleService;
        /// <summary>
        /// Lazy Loaded Order EmployeeRole Service
        /// </summary>
        protected readonly Lazy<OrderEmployeeRoleService> _orderEmployeeRoleService;
        /// <summary>
        /// Lazy Loaded OrderItem Service
        /// </summary>
        protected readonly Lazy<OrderItemService> _orderItemService;
        /// <summary>
        /// Lazy Loaded OrderDestination Service
        /// </summary>
        protected readonly Lazy<OrderDestinationService> _orderDestinationService;
        /// <summary>
        /// Lazy Loaded Pricing Service
        /// </summary>
        protected readonly Lazy<PricingService> _pricingService;
        /// <summary>
        /// Lazy Loaded OrderKeyDate Service
        /// </summary>
        protected readonly Lazy<OrderKeyDateService> _orderKeyDateService;
        /// <summary>
        /// Lazy Loaded Task Queuer
        /// </summary>
        protected ITaskQueuer _taskQueuer;

        /// <summary>
        /// Transaction Type
        /// </summary>
        protected abstract OrderTransactionType TransactionType { get; }

        /// <summary>
        /// Listable service for Orders
        /// </summary>
        public ISimpleListableViewService<SimpleOrderData, int> ListableService => this._service;

        private readonly IMemoryCache _inMemoryCache;

        /// <summary>
        /// API Endpoint for Orders
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="pricingEngine">Pricing Engine</param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <param name="memoryCache">Memory Cache</param>
        public BaseTransactionHeaderController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IPricingEngine pricingEngine, IMigrationHelper migrationHelper, IMemoryCache memoryCache)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
            this._taskQueuer = taskQueuer;
            this._orderNoteService = new Lazy<OrderNoteService>(() => new OrderNoteService(context, this.User.BID().Value, taskQueuer, cache, logger, rtmClient, migrationHelper));
            this._orderOrderLinkService = new Lazy<OrderOrderLinkService<M>>(() => new OrderOrderLinkService<M>(context, logger, this.User.BID().Value, rtmClient, migrationHelper));
            this._orderContactRoleService = new Lazy<OrderContactRoleService>(() => new OrderContactRoleService(context, this.User.BID().Value, taskQueuer, cache, logger, rtmClient, migrationHelper));
            this._orderEmployeeRoleService = new Lazy<OrderEmployeeRoleService>(() => new OrderEmployeeRoleService(context, this.User.BID().Value, taskQueuer, cache, logger, rtmClient, migrationHelper));
            this._orderItemService = new Lazy<OrderItemService>(() => new OrderItemService(context, this.User.BID().Value, taskQueuer, cache, logger, rtmClient, migrationHelper));
            this._orderDestinationService = new Lazy<OrderDestinationService>(() => new OrderDestinationService(context, this.User.BID().Value, taskQueuer, cache, logger, rtmClient, migrationHelper));
            this._pricingService = new Lazy<PricingService>(() => new PricingService(context, logger, this.User.BID().Value, rtmClient, pricingEngine, migrationHelper, cache, _inMemoryCache,taskQueuer));
            this._orderKeyDateService = new Lazy<OrderKeyDateService>(() => new OrderKeyDateService(context, this.User.BID().Value, taskQueuer, cache, logger, rtmClient, migrationHelper));
            this._inMemoryCache = memoryCache;
        }

        /// <summary>
        /// Gets an array of Orders and with any included child objects specified in query parameters
        /// </summary>
        /// <param name="locationID">Location ID for the Orders</param>
        /// <param name="statusID">Status ID for the Orders</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the user provides an invalid Order Transaction Type")]
        public async Task<IActionResult> GetOrdersWithIncludes([FromQuery] int? locationID, [FromQuery] int? statusID, [FromQuery] OrderIncludes includes)
        {
            return new OkObjectResult(await _service.GetOrdersForCriteria(TransactionType, locationID, statusID, includes));
        }

        /// <summary>
        /// Returns a single Order by ID and any included child objects specified in query parameters
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="version">Order Version</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetOrderWithIncludes(int ID, int? version, [FromQuery] OrderIncludes includes)
        {
            if (version.HasValue)
                return new OkObjectResult(await _service.GetForVersion(ID, version.Value, includes));
            else
                return await base.ReadWith(ID, includes);
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the Orders
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Read()
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns a single Order by ID
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <returns></returns>
        [HttpGet("{ID}/obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single Order by ID
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="update">Updated Order data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Order does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] M update)
        {
            // END-8642 Enforce the option "IsPORequired" on the Company record during Order Entry.
            bool isPORequired = await _service.IsPORequired(update);
            if (isPORequired && string.IsNullOrWhiteSpace(update.OrderPONumber))
            {
                this.LogModelErrors();
                return new BadRequestObjectResult("PO Required for this Customer.");
            }

            OrderTransactionType[] transationType = {
                OrderTransactionType.Order,
                OrderTransactionType.Estimate
            };

            if (transationType.Contains(TransactionType))
            {
                bool statusClosed = _service.IsTransactionAlreadyClosed(ID);
                if (statusClosed)
                    return new BadRequestObjectResult(String.Format("Unable to modify a Closed {0}.", TransactionType == OrderTransactionType.Order ? "Order" : "Estimate"));
            }

            var result = await base.Update(ID, update);
            // if (update != null && typeof(result) == OkObjectResult)
            //     await CreateKeyDateBasedOnOrderStatus(ID, update.OrderStatusID);

            switch (result)
            {
                case OkObjectResult ok:
                    // NOTE: do this only when result is 200, its triggering a refresh when having an error during update (e.g. validation error)
                    if (update != null)
                    {
                        await CreateKeyDateBasedOnOrderStatus(ID, update.OrderStatusID);
                        await _taskQueuer.RecomputeGL(User.BID().Value, (byte)GLEngine.Models.EnumGLType.Order, ID, "Order Updated", "Order Updated", User.EmployeeID(), null, (byte)GLEntryType.Order_Edited);
                    }
                    break;
            }

            return result;
        }

        /// <summary>
        /// Creates a new Order
        /// </summary>
        /// <param name="newModel">New Order data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Order creation fails")]
        public override async Task<IActionResult> Create([FromBody] M newModel, [FromQuery] Guid? tempID = null)
        {
            // END-8642 Enforce the option "IsPORequired" on the Company record during Order Entry.
            try
            {
                bool isPORequired = await _service.IsPORequired(newModel);
                if (isPORequired && string.IsNullOrWhiteSpace(newModel.OrderPONumber))
                {
                    this.LogModelErrors();
                    return new BadRequestObjectResult("PO Required for this Customer.");
                }
            }
            catch (Exception e)
            {
                await this._logger.Error((short)User.BID(), "Exception thrown determining if PO required. Please check the company ID", e);
                return new BadRequestObjectResult("Exception thrown determining if PO required. Please check the company ID");
            }

            var result = await base.Create(newModel, tempID);

            bool hasError = false;

            if (result is IStatusCodeActionResult statusCodeResult)
                hasError = statusCodeResult.StatusCode != (int)HttpStatusCode.OK;

            if (!hasError && (newModel != null) && (this.ModelState.ErrorCount == 0))
            {
                await CreateKeyDateBasedOnOrderStatus(newModel.ID, newModel.OrderStatusID, true);
                if (newModel.TransactionType == (byte)OrderTransactionType.Order)
                {
                    await _taskQueuer.RecomputeGL(User.BID().Value, (byte) GLEngine.Models.EnumGLType.Order,
                        newModel.ID, "Order Created", "Order Created", User.EmployeeID(), null,
                        (byte) GLEntryType.Order_New);
                }
            }

            return result;
        }

        /// <summary>
        /// Clones an existing Order to a new Order
        /// </summary>
        /// <param name="ID">ID of the Order to clone from</param>
        /// <param name="CopyFiles">Optional paramter for whether to copy files or not</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Order does not exist or the cloning fails.")]
        public virtual async Task<IActionResult> DoClone(int ID, [FromQuery]bool? CopyFiles = null)
        {
            return await this.DoCloneTransaction(ID, CopyFiles);
        }

        /// <summary>
        /// Clones an Order or Estimate
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="CopyFiles"></param>
        /// <param name="newTransactionType"></param>
        /// <returns></returns>
        protected async Task<IActionResult> DoCloneTransaction(int ID, bool? CopyFiles, OrderTransactionType? newTransactionType = null)
        {
            var result = await _service.CloneTransactionAsync(ID, CopyFiles, newTransactionType, this.User.UserLinkID());
            if (result == null)
            {
                this.LogModelErrors();
                return new BadRequestObjectResult("Clone failed");
            }
            else
            {
                return new OkObjectResult(result);
            }
        }

        /// <summary>
        /// Clones an existing Order to a new Order
        /// </summary>
        /// <param name="ID">ID of the Order to clone from</param>
        /// <returns></returns>
        [Obsolete]
        [HttpPost("{ID}/cloneobsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Order does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes an Order by ID
        /// </summary>
        /// <param name="ID">ID of the Order to Delete</param>
        /// <returns></returns>
        [Obsolete]
        [HttpDelete("{ID}/Obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Order does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }

        /// <summary>
        /// Deletes an Order by ID
        /// </summary>
        /// <param name="ID">ID of the Order to Delete</param>
        /// <param name="force"></param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerOperationFilter(typeof(IgnoreHttpRequestParameterOperationFilter))]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Order does not exist or the deletion fails.")]
        public async Task<IActionResult> DeleteWithForce(short ID, [FromQuery]bool force = false)
        {
            if (force)
            {
                var resp = await _service.ForceDelete(ID, User.UserLinkID().Value);
                return resp.ToResult();
            }

            return await base.Delete(ID);
        }

        #endregion

        /// <summary>
        /// Gets a Simple list of Orders - Obsolete
        /// </summary>
        /// <returns></returns>
        [HttpGet("obseletesimplelist")]
        [Obsolete]
        public async Task<SimpleOrderData[]> SimpleList()
        {
            return await this.GetSimpleList<SimpleOrderData, int>(User.BID().Value);
        }

        #region OrderNote

        /// <summary>
        /// Gets a List of Order Notes
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="orderNoteType">OrderNoteType</param>
        /// <returns></returns>
        [Route("{ID}/notes")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderNote[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ListOrderNotes(int ID, [FromQuery] OrderNoteType orderNoteType = OrderNoteType.Other)
        {
            List<OrderNote> notes = await this._orderNoteService.Value.GetWhere(n => n.BID == User.BID().Value && n.OrderID == ID && n.NoteType == orderNoteType && n.OrderItemID == null && n.DestinationID == null);
            notes.ForEach(n => n.CreatedDT = n.CreatedDT.ToLocalTime());
            return Ok(notes.ToArray());
        }

        /// <summary>
        /// Returns an OrderNote Object with the specified note id for the specified OrderItem.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="noteID">OrderNote ID</param>
        /// <returns></returns>
        [Route("{ID}/notes/{noteID}")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderNote[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public IActionResult GetOrderNote(int ID, int noteID)
        {
            OrderNote note = this._orderNoteService.Value.Where(n => n.BID == User.BID().Value && n.OrderID == ID && n.ID == noteID).FirstOrDefault();
            if (note == null)
            {
                return NotFound();
            }
            return Ok(note);
        }

        /// <summary>
        /// Creates an OrderNote
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="noteType">OrderNoteType</param>
        /// <param name="orderNote">OrderNote updates</param>
        /// <returns></returns>
        [Route("{ID}/action/notes/{noteType}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderNote))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> CreateOrderNote(int ID, OrderNoteType noteType, [FromBody] OrderNote orderNote)
        {
            orderNote.NoteType = noteType;
            orderNote.OrderID = ID;
            orderNote.BID = User.BID().Value;
            var result = await this._orderNoteService.Value.CreateAsync(orderNote);
            if (result == null)
            {
                await _logger.Error(orderNote.BID, "AddAsync returned null", null);
                return new BadRequestObjectResult("Creation failed");
            }
            return new OkObjectResult(orderNote);
        }

        /// <summary>
        /// Updates an OrderNote
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="noteID">OrderNote ID</param>
        /// <param name="orderNote">OrderNote updates</param>
        /// <returns></returns>
        [Route("{ID}/action/notes/{noteID}")]
        [HttpPut]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderNote))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> UpdateOrderNote(int ID, int noteID, [FromBody] OrderNote orderNote)
        {
            orderNote.OrderID = ID;
            orderNote.ID = noteID;
            orderNote.BID = User.BID().Value;
            var result = await this._orderNoteService.Value.UpdateAsync(orderNote, Request.Headers[ConnectionIDHeaderKey]);
            if (result == null)
            {
                await _logger.Error(orderNote.BID, "UpdateAsync returned null", null);
                return new BadRequestObjectResult("Update failed");
            }
            return new OkObjectResult(orderNote);
        }

        /// <summary>
        /// Creates or Updates an OrderNote
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="noteID">OrderNote ID</param>
        /// <returns></returns>
        [Route("{ID}/action/notes/{noteID}/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderNote))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> OrderNoteSetActive(int ID, int noteID)
        {
            EntityActionChangeResponse<int> resp = await this._orderNoteService.Value.SetActive(noteID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Creates or Updates an OrderNote
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="noteID">OrderNote ID</param>
        /// <returns></returns>
        [Route("{ID}/action/notes/{noteID}/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderNote))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> OrderNoteSetInactive(int ID, int noteID)
        {
            EntityActionChangeResponse<int> resp = await this._orderNoteService.Value.SetActive(noteID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Creates or Updates an OrderNote
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="noteID">OrderNote ID</param>
        /// <returns></returns>
        [Route("{ID}/action/notes/{noteID}")]
        [HttpDelete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> DeleteOrderNote(int ID, int noteID)
        {
            OrderNote toDelete = this._orderNoteService.Value.Where(n => n.BID == User.BID().Value && n.OrderID == ID && n.ID == noteID).FirstOrDefault();
            var response = new EntityActionChangeResponse
            {
                Id = noteID,
                Message = "No rows affected.",
                Success = false
            };

            if (toDelete != null)
            {
                var isDeleted = await this._orderNoteService.Value.DeleteAsync(toDelete);

                if (isDeleted)
                    return (new EntityActionChangeResponse
                    {
                        Id = noteID,
                        Message = "Successfully deleted order note.",
                        Success = true
                    }).ToResult();
                else
                    return response.ToResult();
            }

            return response.ToResult();
        }

        #endregion OrderNote

        #region OrderLinks

        /// <summary>
        /// Gets a List of Order Links
        /// </summary>
        /// <returns></returns>
        [Route("{ID}/links")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderOrderLink[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ListOrderLinks(int ID)
        {
            List<OrderOrderLink> links = await this._orderOrderLinkService.Value.GetWhere(n => n.OrderID == ID);
            return Ok(links);
        }

        #endregion OrderLinks

        #region OrderEmployeeRoles

        ///// <summary>
        ///// Returns a collection of employee roles for an order
        ///// </summary>
        ///// <param name="ID">Order ID</param>
        ///// <param name="type">The employee role type of the link</param>
        ///// <returns></returns>
        //[Route("{ID}/employeerole")]
        //[HttpGet]
        //[SwaggerCustomResponse((int)HttpStatusCode.OK)]
        //[SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        //public async Task<IActionResult> GetEmployeeRoles(int ID, EmployeeRoleType? type = null)
        //{
        //    List<OrderEmployeeRole> roles;
        //    if (type.HasValue)
        //        roles = await this._orderEmployeeRoleService.Value.GetWhere(n => n.OrderID == ID && n.RoleType == type.Value);
        //    else
        //        roles = await this._orderEmployeeRoleService.Value.GetWhere(n => n.OrderID == ID);

        //    return Ok(roles);
        //}

        ///// <summary>
        ///// Returns a employee role with a given ID for an order
        ///// </summary>
        ///// <param name="ID">Order ID</param>
        ///// <param name="roleID">The ID of the Employee Role</param>
        ///// <returns></returns>
        //[Route("{ID}/employeerole/{roleID}")]
        //[HttpGet]
        //[SwaggerCustomResponse((int)HttpStatusCode.OK)]
        //[SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        //[SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Order Employee Role is not found")]
        //public async Task<IActionResult> GetEmployeeRoleByID(int ID, int roleID)
        //{
        //    OrderEmployeeRole role = await this._orderEmployeeRoleService.Value.FirstOrDefault(n => n.OrderID == ID && n.ID == roleID);

        //    if (role == null)
        //        return NotFound();

        //    return Ok(role);
        //}

        ///// <summary>
        ///// Returns a collection of employee roles for an order
        ///// </summary>
        ///// <param name="ID">Order ID</param>
        ///// <param name="input">Order Employee Role input</param>
        ///// <returns></returns>
        //[Route("{ID}/employeerole")]
        //[HttpPost]
        //[SwaggerCustomResponse((int)HttpStatusCode.OK)]
        //[SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        //public async Task<IActionResult> AddEmployeeRole(int ID, [FromBody] OrderEmployeeRole input)
        //{

        //    EmployeeRoleType enumRoleType;
        //    try
        //    {

        //        if (input == null)
        //            return BadRequest("Empty or invalid model");
        //        else if (Enum.IsDefined(typeof(EmployeeRoleType), input.RoleType))
        //            enumRoleType = (EmployeeRoleType)input.RoleType;
        //        else
        //            return BadRequest("Empty or invalid model");

        //        if (input.EmployeeID > 0)
        //        {
        //            EntityActionChangeResponse resp = await this._orderEmployeeRoleService.Value.AddEmployeeRole(ID, enumRoleType, employeeID: input.EmployeeID);
        //            return resp.ToResult();
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }


        //    return BadRequest("Empty or invalid model");
        //}

        ///// <summary>
        ///// Updates a employee role for an order
        ///// </summary>
        ///// <param name="ID">Order ID</param>
        ///// <param name="roleID">Order Employee Role ID</param>
        ///// <param name="input">Order Employee Role input</param>
        ///// <returns></returns>
        //[Route("{ID}/employeerole/{roleID}")]
        //[HttpPut]
        //[SwaggerCustomResponse((int)HttpStatusCode.OK)]
        //[SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        //[SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Order Employee Role is not found")]
        //public async Task<IActionResult> UpdateEmployeetRole(int ID, int roleID, [FromBody]OrderEmployeeRole input)
        //{
        //    if (input == null)
        //        return BadRequest("Empty or invalid model");

        //    EmployeeRoleType enumRoleType;

        //    if (input == null)
        //        return BadRequest("Empty or invalid model");
        //    else if (Enum.IsDefined(typeof(EmployeeRoleType), input.RoleType))
        //    {
        //        enumRoleType = (EmployeeRoleType)input.RoleType;
        //    }
        //    else
        //        return BadRequest("Empty or invalid model");

        //    if (input.EmployeeID > 0)
        //    {
        //        EntityActionChangeResponse resp = await this._orderEmployeeRoleService.Value.UpdateEmployeeRole(ID, roleID, enumRoleType, employeeID: input.EmployeeID);
        //        return resp.ToResult();
        //    }

        //    return BadRequest("Empty or invalid model");
        //}

        ///// <summary>
        ///// Deletes a employee role for an order
        ///// </summary>
        ///// <param name="ID">Order ID</param>
        ///// <param name="roleID">Order Employee Role ID</param>
        ///// <returns></returns>
        //[Route("{ID}/employeerole/{roleID}")]
        //[HttpDelete]
        //[SwaggerCustomResponse((int)HttpStatusCode.OK)]
        //[SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        //[SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Order Employee Role is not found")]
        //public async Task<IActionResult> RemoveEmployeeRole(int ID, int roleID)
        //{
        //    EntityActionChangeResponse resp = await this._orderEmployeeRoleService.Value.RemoveEmployeeRole(ID, roleID);
        //    return resp.ToResult();
        //}

        #endregion OrderEmployeeRoles

        #region OrderItems

        /// <summary>
        /// Gets the Order Items associated with the Order by its ID
        /// </summary>
        /// <param name="ID">OrderID</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [Route("{ID}/orderitem")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ListOrderItems(int ID, [FromQuery] OrderItemIncludes includes)
        {
            List<OrderItemData> orderItems = await this._orderItemService.Value.GetOrderItemsForOrder(ID, includes);
            if (orderItems != null)
                return Ok(orderItems.ToArray());
            else
                return NotFound();
        }

        /// <summary>
        /// Gets the Simple List of Order Items associated with the Order by its ID
        /// </summary>
        /// <param name="ID">OrderID</param>
        /// <returns></returns>
        [Route("{ID}/orderitem/simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleOrderItemData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> SimpleListOrderItems(int ID)
        {
            List<SimpleOrderItemData> orderItems = await this._orderItemService.Value.GetSimpleOrderItemDataForOrder(ID);
            if (orderItems != null)
                return Ok(orderItems.ToArray());
            else
                return NotFound();
        }

        #endregion

        #region OrderDestinations
        /// <summary>
        /// Gets the Order Destinations associated with the Order by its ID
        /// </summary>
        /// <param name="ID">OrderID</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [Route("{ID}/orderdestination")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderDestinationData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ListOrderDestinations(int ID, [FromQuery] OrderDestinationIncludes includes)
        {
            List<OrderDestinationData> orderDestinations = await this._orderDestinationService.Value.GetOrderDestinationsForOrder(ID, includes);
            if (orderDestinations != null)
                return Ok(orderDestinations.ToArray());
            else
                return NotFound();
        }

        /// <summary>
        /// Gets the Simple List of Order Destinations associated with the Order by its ID
        /// </summary>
        /// <param name="ID">OrderID</param>
        /// <returns></returns>
        [Route("{ID}/orderdestination/simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleOrderItemData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> SimpleListOrderDestinations(int ID)
        {
            List<SimpleOrderDestinationData> orderDestinations = await this._orderDestinationService.Value.GetSimpleOrderDestinationDataForOrder(ID);
            if (orderDestinations != null)
                return Ok(orderDestinations.ToArray());
            else
                return NotFound();
        }

        #endregion

        /// <summary>
        /// Try's to validate the data model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            var entity = model as TransactionHeaderData;
            
            if (entity.CompanyID == 0)
            {
                this.ModelState.AddModelError("CompanyID", "Company is required");
            }

            if (entity.ContactRoles != null)
            {
                bool hasError = false;
                bool skipIDValidation = false;

                foreach (var contact in entity.ContactRoles)
                {
                    // Check if Contact is valid
                    if (string.IsNullOrWhiteSpace(contact.ContactName) && (contact.ContactID == null))
                    {
                        this.ModelState.AddModelError("Contact", "Contact Name/ID is required");
                        hasError = true;
                    }

                    // Routine to skip ID validation if Name exists and ContactID is null
                    if (!hasError && (contact.ContactID == null) && (!string.IsNullOrWhiteSpace(contact.ContactName)))
                    {
                        skipIDValidation = true;
                    }

                    if (!hasError && !skipIDValidation && !_service.IsValidContact(contact.ContactID))
                    {
                        this.ModelState.AddModelError("ContactID", "Invalid ContactID");
                        hasError = true;
                    }

                    // Check if Contact Role Type is valid
                    if (!_service.IsValidContactRoleType(contact.RoleType))
                    {
                        this.ModelState.AddModelError("ContactRoleType", "Invalid Contact's RoleType");
                        hasError = true;
                    }

                    if (hasError) break;
                }
            }

            if (entity.EmployeeRoles != null)
            {
                foreach (var employee in entity.EmployeeRoles)
                {
                    // Check if EmployeeID is valid
                    if (!_service.IsValidEmployee(employee.EmployeeID))
                    {
                        this.ModelState.AddModelError("EmployeeID", "Invalid EmployeeID");
                        break;
                    }

                    // Check if Employee has valid RoleID
                    if (!_service.IsValidEmployeeRoleID(employee.RoleID))
                    {
                        this.ModelState.AddModelError("EmployeeRoleID", "Invalid Employee's RoleID");
                        break;
                    }
                }
            }

            return base.TryValidateModel(model, prefix);
        }

        #region Pricing

        /// <summary>
        /// Computes the price of a order not already saved.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="computeTax"></param>
        /// <returns></returns>
        [Route("compute")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderPriceResult))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> Compute([FromBody] OrderPriceRequest request, [FromQuery] bool? computeTax = null)
        {
            if (request == null)
                return BadRequest("Price request object is missing or invalid.");
            foreach (var item in request.Items)
            {
                if ((item.TaxInfoList == null) || (item.TaxInfoList.Count == 0) || String.IsNullOrWhiteSpace(item.TaxInfoList.First().NexusID))
                {
                    return BadRequest("ItemPriceRequest TaxInfoList is not defined correctly");
                }
            }
            try
            {
                OrderPriceResult result = await _pricingService.Value.ComputeOrder(request, computeTax.GetValueOrDefault(false));
                return Ok(result);
            }

            catch (Exception err)
            {
                return StatusCode(500, err);
            }
        }

        #endregion Pricing

        #region Order/Estimate KeyDate Creation
        /// <summary>
        /// Kicks off the RecomputeGL Task on BgEngine
        /// </summary>
        /// <returns></returns>
        protected async Task<IActionResult> CreateKeyDateBasedOnOrderStatus(int OrderID, OrderOrderStatus orderStatus, bool IsNew = false)
        {
            bool UpdateExistingKeyDate = false;
            EntityActionChangeResponse resp = new EntityActionChangeResponse();
            OrderKeyDateType okdt = new OrderKeyDateType();
            resp.Success = true;
            resp.Message = "No OrderKeyDate Created.";

            // Save KeyDate depending on the order status
            // Created        - All      - When order or estimate is created.
            // Voided         - All      - If current Order Status is Voided (19 or 29) and Key Date record does not exist
            // Lost           - Estimate - If current Order Status is Lost (18) and Key Date record does not exist
            // Converted      - Estimate - If current Order Status is Approved (16) and Key Date record does not exist
            // Release to WIP - Order    - If current Order Status is >= WIP(22, 23, 24, 25, or 26) and Key Date record does not exist
            // Built          - Order    - If current Order Status is >= Built(23, 24, 25, or 26) and Key Date record does not exist
            // In Invoicing   - Order    - If current Order Status is >= Invoicing(24, 25, or 26) and Key Date record does not exist
            // Invoiced       - Order    - If current Order Status is >= Invoiced(25 or 26) and Key Date record does not exist
            // Closed         - Order    - If current Order Status is Closed (26) and Key Date record does not exist
            // Balance Due    - Order    - If current Order Status is >= WIP(22, 23, 24, 25, or 26) and Key Date record does not exist

            // Sample Scenario: I move an order to built, someone realizes it isn't built and moves it back, 
            //                  when I finish getting it ready, I move it back to built. Date should be updated.

            if (IsNew)
            {
                okdt = OrderKeyDateType.Created;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt);
            }

            if (orderStatus == OrderOrderStatus.EstimatePending)
            {
                okdt = OrderKeyDateType.Pending;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, true);
            }

            if ((orderStatus == OrderOrderStatus.OrderVoided))
            {
                okdt = OrderKeyDateType.Voided;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt);
            }

            if (orderStatus == OrderOrderStatus.EstimateVoided)
            {
                okdt = OrderKeyDateType.Voided;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, true);
            }

            if (orderStatus == OrderOrderStatus.EstimateLost)
            {
                okdt = OrderKeyDateType.Lost;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, true);
            }
            if (orderStatus == OrderOrderStatus.EstimateApproved)
            {
                okdt = OrderKeyDateType.Converted;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt);
            }

            if ((orderStatus == OrderOrderStatus.OrderWIP) || (orderStatus == OrderOrderStatus.OrderBuilt) || (orderStatus == OrderOrderStatus.OrderInvoicing) || (orderStatus == OrderOrderStatus.OrderInvoiced) || (orderStatus == OrderOrderStatus.OrderClosed))
            {
                if (orderStatus == OrderOrderStatus.OrderWIP) // Only update date and replace existing when primary status is selected. See Sample Scenario on comments above.
                    UpdateExistingKeyDate = true;
                else
                    UpdateExistingKeyDate = false;
                okdt = OrderKeyDateType.ReleasedToWIP;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, UpdateExistingKeyDate);
            }

            if ((orderStatus == OrderOrderStatus.OrderBuilt) || (orderStatus == OrderOrderStatus.OrderInvoicing) || (orderStatus == OrderOrderStatus.OrderInvoiced) || (orderStatus == OrderOrderStatus.OrderClosed))
            {
                if (orderStatus == OrderOrderStatus.OrderBuilt) // Only update date and replace existing when primary status is selected. See Sample Scenario on comments above.
                    UpdateExistingKeyDate = true;
                else
                    UpdateExistingKeyDate = false;
                okdt = OrderKeyDateType.Built;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, UpdateExistingKeyDate);
            }
            if ((orderStatus == OrderOrderStatus.OrderInvoicing) || (orderStatus == OrderOrderStatus.OrderInvoiced) || (orderStatus == OrderOrderStatus.OrderClosed))
            {
                if (orderStatus == OrderOrderStatus.OrderInvoicing) // Only update date and replace existing when primary status is selected. See Sample Scenario on comments above.
                    UpdateExistingKeyDate = true;
                else
                    UpdateExistingKeyDate = false;
                okdt = OrderKeyDateType.InInvoicing;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, UpdateExistingKeyDate);
            }
            if ((orderStatus == OrderOrderStatus.OrderInvoiced) || (orderStatus == OrderOrderStatus.OrderClosed))
            {
                if (orderStatus == OrderOrderStatus.OrderInvoiced) // Only update date and replace existing when primary status is selected. See Sample Scenario on comments above.
                    UpdateExistingKeyDate = true;
                else
                    UpdateExistingKeyDate = false;
                okdt = OrderKeyDateType.Invoiced;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, UpdateExistingKeyDate);
            }
            if (orderStatus == OrderOrderStatus.OrderClosed)
            {
                okdt = OrderKeyDateType.Closed;
                resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt);
            }

            if ((orderStatus == OrderOrderStatus.OrderInvoicing || orderStatus == OrderOrderStatus.OrderInvoiced))
            {
                okdt = OrderKeyDateType.BalanceDue;

                DateTime? balanceDueDate = await this._orderKeyDateService.Value.ComputeBalanceDueDate(this.User.BID().Value, OrderID);
                if (balanceDueDate != null)
                {
                    resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, UpdateExistingKeyDate, balanceDueDate);
                }
            }

            if (orderStatus == OrderOrderStatus.OrderInvoiced)
            {
                okdt = OrderKeyDateType.EarlyPaymentDate;

                DateTime? EarlyPaymentDate = await this._orderKeyDateService.Value.ComputeEarlyPaymentDate(this.User.BID().Value, OrderID);
                if (EarlyPaymentDate != null)
                {
                    resp = await CreateOrderKeyDateIfNotFound(OrderID, okdt, UpdateExistingKeyDate, EarlyPaymentDate);
                }
            }

            return resp.ToResult();
        }
 

        private async Task<EntityActionChangeResponse> CreateOrderKeyDateIfNotFound(int OrderID, OrderKeyDateType okdt, bool UpdateExisting = false, DateTime? dateTime = null)
        {
            EntityActionChangeResponse resp = new EntityActionChangeResponse();
            resp.Success = true;
            resp.Message = "No OrderKeyDate Created.";

            if (UpdateExisting)
            {
                resp = await this._orderKeyDateService.Value.ChangeKeyDate(OrderID, okdt, dateTime, null);
                await CleanOrderKeyDateRecords(OrderID, okdt);
            }
            else
            {
                OrderKeyDate okd = await this._orderKeyDateService.Value.GetOrderKeyDate(OrderID, okdt);
                if (okd == null)
                {
                    resp = await this._orderKeyDateService.Value.ChangeKeyDate(OrderID, okdt, dateTime, null);
                }
            }

            return resp;
        }

        // Delete all "advanced" OrderKeyDates.
        private async Task<EntityActionChangeResponse> CleanOrderKeyDateRecords(int OrderID, OrderKeyDateType okdt)
        {
            EntityActionChangeResponse resp = new EntityActionChangeResponse();
            resp.Success = true;
            resp.Message = "No Rows Affected.";

            if (okdt == OrderKeyDateType.ReleasedToWIP)
            {
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Built);
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.InInvoicing);
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Invoiced);
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Closed);
            }
            if (okdt == OrderKeyDateType.Built)
            {
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.InInvoicing);
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Invoiced);
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Closed);
            }
            if (okdt == OrderKeyDateType.InInvoicing)
            {
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Invoiced);
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Closed);
            }
            if (okdt == OrderKeyDateType.Invoiced)
            {
                resp = await this._orderKeyDateService.Value.RemoveKeyDate(OrderID, OrderKeyDateType.Closed);
            }

            return resp;
        }

        #endregion

        /// <summary>
        /// Get the next Estimate, Order or Invoice Number
        /// </summary>
        /// <param name="transactionType">Estimate, Order, or Invoice</param>
        [Route("GetNextNumber")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(int))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<int> GetNextTransactionNumber(ApiTransactionType transactionType)
        {
            return await base._service.GetNextTransactionNumber(transactionType);
        }

        /// <summary>
        /// Set the next Estimate, Order or Invoice Number
        /// </summary>
        /// <param name="transactionType">Estimate, Order, or Invoice</param>
        /// <param name="nextId"></param>
        [Route("SetNextNumber/{nextId}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> SetNextTransactionNumber(int nextId, [FromQuery] ApiTransactionType transactionType)
        {
            return (await this._service.SetNextTransactionNumber(transactionType, nextId)).ToResult();
        }

        /// <summary>
        /// Get the current Order or Invoice Number
        /// </summary>
        /// <param name="transactionType">Estimate, Order, or Invoice</param>
        [Route("GetCurrentNumber")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(int))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<int> GetCurrentTransactionNumber(ApiTransactionType transactionType)
        {
            return await base._service.GetCurrentTransactionNumber(transactionType);
        }
    }
}