﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Pricing;

using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes.Clones;
using Microsoft.Extensions.Caching.Memory;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for Orders
    /// </summary>
    [Authorize]
    [Route("api/[Controller]")]
    public class OrderController : BaseTransactionHeaderController<OrderData, OrderService>
    {
        /// <summary>
        /// Defines this controller as for Estimates
        /// </summary>
        protected override OrderTransactionType TransactionType => OrderTransactionType.Order;

        /// <summary>
        /// Api Endpoint for Orders
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="pricingEngine">Pricing Engine</param>
        /// <param name="memoryCache"></param>
        /// <param name="migrationHelper"></param>
        public OrderController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IPricingEngine pricingEngine, IMigrationHelper migrationHelper, IMemoryCache memoryCache)
            : base(context, logger, rtmClient, taskQueuer, cache, pricingEngine, migrationHelper, memoryCache)
        {
        }

        /// <summary>
        /// Kicks off the RecomputeGL Task on BgEngine
        /// </summary>
        /// <returns></returns>
        [Route("{ID}/action/changeorderstatus/{orderStatus}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If orderStatus is invalid.")]
        public override async Task<IActionResult> ChangeOrderStatus(int ID, OrderOrderStatus orderStatus)
        {
            var allowedOrderTypeStatusMap = new Dictionary<OrderTransactionType, OrderOrderStatus[]>()
            {
                {
                    OrderTransactionType.Order,
                    new OrderOrderStatus[] {
                        OrderOrderStatus.OrderBuilt,
                        OrderOrderStatus.OrderClosed,
                        OrderOrderStatus.OrderInvoiced,
                        OrderOrderStatus.OrderInvoicing,
                        OrderOrderStatus.OrderPreWIP,
                        OrderOrderStatus.OrderVoided,
                        OrderOrderStatus.OrderWIP
                    }
                }
            };

            // get the Order
            var order = await _service.GetAsync(ID);
            if (order == null)
            {
                return new NotFoundResult();
            }

            // For Orders, the old status can't be Voided (29).
            // For Estimates, the old status can't be Voided (19).
            // For PO, the old status can't be Voided (49).
            OrderOrderStatus[] voidStatuses = new OrderOrderStatus[] {
                OrderOrderStatus.OrderVoided
            };

            // Validate against voided Orders / checks wether the initial status is void
            if (voidStatuses.Contains(order.OrderStatusID))
            {
                return BadRequest("Status is void");
            }

            // Check against allowed status within the Order type
            OrderOrderStatus[] allowedStatuses = allowedOrderTypeStatusMap.Where(item => item.Key == (OrderTransactionType)order.TransactionType).Select(i => i.Value).First();
            if (!allowedStatuses.Contains(orderStatus))
            {
                return BadRequest("Status is not allowed");
            }

            var response = await this._service.ChangeOrderStatus(order, orderStatus, Request.Headers[ConnectionIDHeaderKey]);

            if (response.Success)
            {
                // call the task service
                await _taskQueuer.RecomputeGL(User.BID().Value, (byte)GLEngine.Models.EnumGLType.Order, ID, "Order Status Change", "Order Status Change", User.EmployeeID(), null, (byte)GLEntryType.Order_Status_Change);
                await CreateKeyDateBasedOnOrderStatus(order.ID, orderStatus);
            }


            return Ok(response);
        }

        /// <summary>
        /// Clones an existing Order to a new Order
        /// </summary>
        /// <param name="ID">ID of the Order to clone from</param>
        /// <param name="CopyFiles">Optional paramter for whether to copy files or not</param>
        /// <returns></returns>
        [Obsolete]
        [HttpPost("{ID}/clone/obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Order does not exist or the cloning fails.")]
        public override async Task<IActionResult> DoClone(int ID, [FromQuery]bool? CopyFiles = null)
        {
            return await this.DoCloneTransaction(ID, CopyFiles);
        }

        /// <summary>
        /// Clones an existing Order to a new Order
        /// </summary>
        /// <param name="ID">ID of the Order to clone from</param>
        /// <param name="CopyFiles">Optional paramter for whether to copy files or not</param>
        /// <param name="SaveAsEstimate">Save clone as an order?</param>
        /// <param name="SaveAsCreditMemo"></param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData))] 
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Order does not exist or the cloning fails.")]
        public async Task<IActionResult> DoCloneOrder(int ID, [FromQuery]bool? CopyFiles = null, [FromQuery]bool? SaveAsEstimate = null, [FromQuery] bool? SaveAsCreditMemo = null)
        {
            var transactionType = OrderTransactionType.Order;
            if (SaveAsEstimate.GetValueOrDefault(false))
            {
                transactionType = OrderTransactionType.Estimate;
            }
            if (SaveAsCreditMemo.GetValueOrDefault(false))
            {
                transactionType = OrderTransactionType.Memo;
            }
            return await this.DoCloneTransaction(ID, CopyFiles, transactionType);
        }

        /// <summary>
        /// Clones an existing Order to a new Order with Options
        /// </summary>
        /// <param name="ID">ID of the Order to clone from</param>
        /// <param name="SaveAsEstimate">Save clone as an order?</param>
        /// <param name="cloneOption"></param>
        /// <returns></returns>
        [HttpPost("{ID}/cloneWithOption")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Order does not exist or the cloning fails.")]
        public async Task<IActionResult> DoCloneOrderWithOption(int ID, [FromBody]OrderCloneOption cloneOption, [FromQuery]bool? SaveAsEstimate = null)
        {
            return await this._service.CloneTransactionWithOptionAsync(ID, cloneOption, this.User.UserLinkID(), SaveAsEstimate.GetValueOrDefault(false) ? OrderTransactionType.Estimate : (OrderTransactionType?)null);
        }

        /// <summary>
        /// Get all Locations without Open Order
        /// </summary>
        /// <returns></returns>
        [Route("locations")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(List<byte>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<List<byte>> GetLocationWithoutOpenOrder()
        {
            return await this._service.GetLocationWithoutOpenOrder();
        }

        /// <summary>
        /// Get all Companies without Open Order
        /// </summary>
        /// <returns></returns>
        [Route("companies")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(List<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<List<int>> GetCompanyWithoutOpenOrder()
        {
            return await this._service.GetCompanyWithoutOpenOrder();
        }

        /// <summary>
        /// Get a valid preview number base on classtype
        /// https://corebridge.atlassian.net/browse/END-12503
        /// </summary>
        /// <returns></returns>
        [Route("previewnumber")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(int))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<int?> GetPreviewNumber([FromQuery]ClassType? appliesToCTID)
        {
            return await this._service.GetPreviewNumber(appliesToCTID);
        }        
    }
}
