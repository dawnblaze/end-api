﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Payment Application Controller
    /// </summary>
    [Route("api/payment")]
    public class PaymentApplicationController :  CRUDController<PaymentApplication, PaymentApplicationService, int>
    {
        /// <summary>
        /// Payment Application Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public PaymentApplicationController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Obsolete Read Method
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsolete")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Obsolete Clone Method
        /// </summary>
        [Obsolete]
        [HttpPost("{ID}/clone")]
        public override Task<IActionResult> Clone(int ID)
        {
            return base.Clone(ID);
        }

        /// <summary>
        /// Obsolete Create Method
        /// </summary>
        [Obsolete]
        [HttpPost]
        public override Task<IActionResult> Create([FromBody] PaymentApplication newModel, [FromQuery] Guid? tempID = null)
        {
            return base.Create(newModel, tempID);
        }

        /// <summary>
        /// Obsolete Delete Method
        /// </summary>
        [Obsolete]
        [HttpDelete("{ID}")]
        public override Task<IActionResult> Delete(int ID)
        {
            return base.Delete(ID);
        }

        /// <summary>
        /// Obsolete Update Method
        /// </summary>
        [Obsolete]
        public override Task<IActionResult> Update(int ID, [FromBody] PaymentApplication update)
        {
            return base.Update(ID, update);
        }

        /// <summary>
        /// Get a list of PaymentApplications with filters
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <returns>Returns an array of Payment applications</returns>
        [HttpGet("application")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentApplication[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetApplicationWithFilters([FromQuery] PaymentApplicationFilter filters)
        {
            return new OkObjectResult(await this._service.GetWithFiltersAsync(filters));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("application/{id}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(PaymentApplication[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the specified payment application id is not found")]
        public override Task<IActionResult> ReadById(int id)
        {
            return base.ReadById(id);
        }

    }
}