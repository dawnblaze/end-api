﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Order Item Contact Role Controller
    /// </summary>
    [Route("api/orderitem/{parentID}/contactrole")]
    public class OrderItemContactRoleController : CRUDChildController<OrderContactRole, OrderItemContactRoleService, int, int>
    {
        private readonly Lazy<OrderItemService> _orderItemService;
        /// <summary>
        /// Order Item Contact Role Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderItemContactRoleController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
            this._orderItemService = new Lazy<OrderItemService>(() => new OrderItemService(context, this.User.BID().Value, taskQueuer, cache, logger, rtmClient, migrationHelper));
        }

        /// <summary>
        /// Obsolete GET method
        /// </summary>
        /// <param name="parentID">OrderItem ID</param>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete]
        public override Task<IActionResult> Read(int parentID)
        {
            throw new InvalidOperationException();
        }
        /// <summary>
        /// Obsolete GET method
        /// </summary>
        /// <param name="parentID">OrderItem ID</param>
        /// <param name="ID">OrderItemContactRole ID</param>
        /// <returns></returns>
        [HttpGet("obsolute/{ID}")]
        [Obsolete]
        public override Task<IActionResult> ReadByParentIdAndId(int parentID, int ID)
        {
            throw new InvalidOperationException();
        }
        
        /// <summary>
        /// Gets all of the OrderContactRoles associated with the OrderItem by its ID
        /// </summary>
        /// <param name="parentID">OrderItem's ID</param>
        /// <param name="includeDefaults">If IncludeDefault is true, the result will include contact roles at the Order Level is there is not a version that exists on the Item Level.</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRole[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> ReadWith(int parentID, [FromQuery] bool includeDefaults = false)
        {
            var results = await this._service.GetByParentIDWithDefaultsAsync(parentID, includeDefaults);
            if (results == null)
                return NotFound();

            return Ok(results);
        }

        /// <summary>
        /// Retrieves a specific OrderContactRole associated with an OrderItem by the OrderContactRole's ID
        /// </summary>
        /// <param name="parentID">OrderItem's ID</param>
        /// <param name="ID">OrderContactRole's ID</param>
        /// <param name="useDefaultIfNotFound">If UseDefaultIfNotFound is true, the result will include the contact roles at the Order Level is there is not a version that exists on the Item Level.</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRole[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem or OrderContactRole does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> ReadWith(int parentID, int ID, [FromQuery] bool useDefaultIfNotFound = false)
        {
            return await base.ReadByParentIdAndId(parentID, ID);
        }

        /// <summary>
        /// Creates an OrderContactRole to be associated with the OrderItem
        /// </summary>
        /// <param name="parentID">OrderItem's ID</param>
        /// <param name="create">New OrderContactRole object</param>
        /// <param name="tempID">temp Guid for saving documents</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRole))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the creation for the OrderContactRole fails.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public override async Task<IActionResult> Create(int parentID, [FromBody] OrderContactRole create, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(parentID, create, tempID);            
        }

        /// <summary>
        /// Updates the contactID for an OrderContactRole associated with the OrderItem
        /// </summary>
        /// <param name="parentID">OrderItem's ID</param>
        /// <param name="ID">OrderContactRole's ID</param>
        /// <param name="update">Updated OrderContactRole object</param>
        /// <returns></returns>
        
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRole))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem or OrderContactRole does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the update for the OrderContactRole fails or the OrderContactRole is not associated with the OrderItem.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]

        public override async Task<IActionResult> Update(int parentID, int ID, [FromBody] OrderContactRole update)
        {
            return await base.Update(parentID, ID, update);
        }

        /// <summary>
        /// Creates an OrderContactRole to be associated with the OrderItem
        /// </summary>
        /// <param name="parentID">OrderItem's ID</param>
        /// <param name="ID">OrderContactRole's ID</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem or OrderContactRole does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the delete for the OrderContactRole fails or the OrderContactRole is not associated with the OrderItem.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public override async Task<IActionResult> Delete(int parentID, int ID)
        {
            return await base.Delete(parentID, ID);            
        }
    }
}
