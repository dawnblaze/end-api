﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Models;
using Endor.Security;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using Endor.Tenant;
using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    public abstract partial class BaseTransactionHeaderController<M, S> : CRUDController<M, S, int>, ISimpleListableViewController<SimpleOrderData, int>
        where M : TransactionHeaderData
        where S : BaseTransactionService<M>
    {
        #region OrderLinks

        /// <summary>
        /// Links an Order to another Order
        /// </summary>
        /// <returns></returns>
        [Route("{ID}/Action/LinkOrder/{linkType}/{linkID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderOrderLink))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> LinkOrder(int ID, OrderOrderLinkType linkType, int linkID, [FromBody] OrderOrderLinkInput orderLinkInput = null)
        {
            var createdOrderLink = await this._orderOrderLinkService.Value.CreateAsync(ID, orderLinkInput, linkType, linkID);

            if (createdOrderLink != null)
                return Ok(createdOrderLink);

            return BadRequest();
        }

        /// <summary>
        /// Updates an OrderLink
        /// </summary>
        /// <param name="ID">Order ID to link FROM</param>
        /// <param name="linkType">Link Type</param>
        /// <param name="linkID">Order ID to link TO</param>
        /// <param name="orderLinkInput"></param>
        /// <returns></returns>
        [Route("{ID}/Action/UpdateOrderLink/{linkType}/{linkID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderOrderLink))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> UpdateOrderLink(int ID, OrderOrderLinkType linkType, int linkID, [FromBody] OrderOrderLinkInput orderLinkInput = null)
        {
            var updatedOrderLink = await _orderOrderLinkService.Value.UpdateAsync(ID, orderLinkInput, linkType, linkID, Request.Headers[ConnectionIDHeaderKey]);

            if (updatedOrderLink != null)
                return Ok(updatedOrderLink);

            return BadRequest();
        }

        /// <summary>
        /// Removes an OrderLink
        /// </summary>
        /// <param name="ID">Order ID to unlink FROM</param>
        /// <param name="linkType">Link Type</param>
        /// <param name="linkID">Order ID to unlink TO</param>
        /// <returns></returns>
        [Route("{ID}/Action/UnlinkOrder/{linkType}/{linkID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Order could not be unlinked")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> DeleteOrderLink(int ID, OrderOrderLinkType linkType, int linkID)
        {
            return (await this._orderOrderLinkService.Value.DeleteAsync(ID, linkType, linkID)).ToResult();
        }

        #endregion OrderLinks

        #region OrderContactRoles

        /// <summary>
        /// Adds a contact role to an order
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="contactRoleType">The contact role type of the link</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        [Route("{ID}/Action/LinkContact/{contactRoleType}/{contactID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the order fails to add contact role.")]
        public async Task<IActionResult> AddContactRole(int ID, OrderContactRoleType contactRoleType, int contactID)
        {
            EntityActionChangeResponse resp = await this._orderContactRoleService.Value.AddContactRole(ID, contactRoleType, contactID: contactID);
            return resp.ToResult();
        }

        /// <summary>
        /// Adds a contact role to an order
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="contactRoleType">The contact role type of the link</param>
        /// <param name="input">Contact info</param>
        /// <returns></returns>
        [Route("{ID}/Action/LinkContact/{contactRoleType}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the order fails to add contact role.")]
        public async Task<IActionResult> AddAdhocContactRole(int ID, OrderContactRoleType contactRoleType, [FromBody] OrderContactRoleService.ContactNameInput input)
        {
            EntityActionChangeResponse resp = await this._orderContactRoleService.Value.AddContactRole(ID, contactRoleType, input: input);
            return resp.ToResult();
        }

        /// <summary>
        /// Removes a contact role from an order
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="contactRoleType">The contact role type of the link</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        [Route("{ID}/Action/LinkContact/{contactRoleType}/{contactID}")]
        [HttpDelete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the order fails to remove a contact role.")]
        public async Task<IActionResult> RemoveContactRole(int ID, OrderContactRoleType contactRoleType, int contactID)
        {
            EntityActionChangeResponse resp = await this._orderContactRoleService.Value.RemoveContactRole(ID, contactRoleType, contactID: contactID);
            return resp.ToResult();
        }

        /// <summary>
        /// Removes a contact role from an order
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="contactRoleType">The contact role type of the link</param>
        /// <param name="input">Contact info</param>
        /// <returns></returns>
        [Route("{ID}/Action/LinkContact/{contactRoleType}")]
        [HttpDelete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the order fails to remove a contact role.")]
        public async Task<IActionResult> RemoveAdhocContactRole(int ID, OrderContactRoleType contactRoleType, [FromBody] OrderContactRoleService.ContactNameInput input)
        {
            EntityActionChangeResponse resp = await this._orderContactRoleService.Value.RemoveContactRole(ID, contactRoleType, input: input);
            return resp.ToResult();
        }

        /// <summary>
        /// Adds a contact role to an order
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="contactRoleType">The contact role type of the link</param>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        [Route("{ID}/Action/LinkContact/{contactRoleType}/{contactID}")]
        [HttpPut]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the order fails to add contact role.")]
        public async Task<IActionResult> UpdateContactRole(int ID, OrderContactRoleType contactRoleType, int contactID)
        {
            EntityActionChangeResponse resp = await this._orderContactRoleService.Value.UpdateContactRole(ID, contactRoleType, contactID: contactID);
            return resp.ToResult();
        }

        /// <summary>
        /// Adds a contact role to an order
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="contactRoleType">The contact role type of the link</param>
        /// <param name="input">Contact info</param>
        /// <returns></returns>
        [Route("{ID}/Action/LinkContact/{contactRoleType}")]
        [HttpPut]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the order fails to add contact role.")]
        public async Task<IActionResult> UpdateAdhocContactRole(int ID, OrderContactRoleType contactRoleType, [FromBody] OrderContactRoleService.ContactNameInput input)
        {
            EntityActionChangeResponse resp = await this._orderContactRoleService.Value.UpdateContactRole(ID, contactRoleType, input: input);
            return resp.ToResult();
        }
        
        #endregion OrderContactRoles

        #region OrderEmployeeRoles

        /// <summary>
        /// Adds a employee role to an order
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="employeeRoleID">The employee role type of the link</param>
        /// <param name="employeeID">Employee ID</param>
        /// <returns></returns>
        [Route("{ID}/Action/LinkEmployee/{employeeRoleID}/{employeeID}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the order fails to add employee role.")]
        public async Task<IActionResult> AddEmployeeRole(int ID, short employeeRoleID, int employeeID)
        {
            EntityActionChangeResponse resp = await this._orderEmployeeRoleService.Value.AddEmployeeRole(ID, employeeRoleID, employeeID);
            return resp.ToResult();
        }

        /// <summary>
        /// Removes a employee role from an order
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="employeeRoleID">The employee role type of the link</param>
        /// <param name="employeeID">Employee ID</param>
        /// <returns></returns>
        [Route("{ID}/Action/LinkEmployee/{employeeRoleID}/{employeeID}")]
        [HttpDelete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the order fails to remove a employee role.")]
        public async Task<IActionResult> RemoveEmployeeRole(int ID, short employeeRoleID, int employeeID)
        {
            EntityActionChangeResponse resp = await this._orderEmployeeRoleService.Value.RemoveEmployeeRole(ID, employeeRoleID, employeeID);
            return resp.ToResult();
        }

        /// <summary>
        /// Updates a employee role to an order
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="employeeRoleID">The employee role type of the link</param>
        /// <param name="employeeID">Employee ID</param>
        /// <returns></returns>
        [Route("{ID}/Action/LinkEmployee/{employeeRoleID}/{employeeID}")]
        [HttpPut]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the order fails to add employee role.")]
        public async Task<IActionResult> UpdateEmployeeRole(int ID, short employeeRoleID, int employeeID)
        {
            EntityActionChangeResponse resp = await this._orderEmployeeRoleService.Value.UpdateEmployeeRole(ID, employeeRoleID, employeeID);
            return resp.ToResult();
        }

        #endregion

        #region ChangeOrderStatus

        /// <summary>
        /// Kicks off the RecomputeGL Task on BgEngine
        /// </summary>
        /// <returns></returns>
        [Route("{ID}/Action/ChangeOrderStatus/{orderStatus}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If orderStatus is invalid.")]
        public virtual async Task<IActionResult> ChangeOrderStatus(int ID, OrderOrderStatus orderStatus)
        {
            var allowedOrderTypeStatusMap = new Dictionary<OrderTransactionType, OrderOrderStatus[]>()
            {
                {
                    OrderTransactionType.Order,
                    new OrderOrderStatus[] {
                        OrderOrderStatus.OrderBuilt,
                        OrderOrderStatus.OrderClosed,
                        OrderOrderStatus.OrderInvoiced,
                        OrderOrderStatus.OrderInvoicing,
                        OrderOrderStatus.OrderPreWIP,
                        OrderOrderStatus.OrderVoided,
                        OrderOrderStatus.OrderWIP,
                    }
                },
                {
                    OrderTransactionType.Estimate,
                    new OrderOrderStatus[] {
                        OrderOrderStatus.EstimateApproved,
                        OrderOrderStatus.EstimateLost,
                        OrderOrderStatus.EstimatePending,
                        OrderOrderStatus.EstimateVoided,
                    }
                },
                {
                    OrderTransactionType.PurchaseOrder,
                    new OrderOrderStatus[] {
                        OrderOrderStatus.POApproved,
                        OrderOrderStatus.POOrdered,
                        OrderOrderStatus.POReceived,
                        OrderOrderStatus.PORequested,
                        OrderOrderStatus.POVoided,
                    }
                },
                {
                    OrderTransactionType.Memo,
                    new OrderOrderStatus[] {
                        OrderOrderStatus.CreditMemoUnposted,
                        OrderOrderStatus.CreditMemoPosted,
                        OrderOrderStatus.CreditMemoVoided
                    }
                },
            };

            // get the Order
            var order = await _service.GetAsync(ID);
            if (order == null)
            {
                return new NotFoundResult();
            }

            // For Orders, the old status can't be Voided (29).
            // For Estimates, the old status can't be Voided (19).
            // For PO, the old status can't be Voided (49).
            OrderOrderStatus[] voidStatuses = new OrderOrderStatus[] {
                OrderOrderStatus.OrderVoided,
                OrderOrderStatus.EstimateVoided,
                OrderOrderStatus.POVoided,
                OrderOrderStatus.CreditMemoVoided
            };

            // Validate against voided Orders / checks wether the initial status is void
            if (voidStatuses.Contains(order.OrderStatusID))
            {
                return BadRequest("Status is void");
            }

            // Check against allowed status within the Order type
            OrderOrderStatus[] allowedStatuses = allowedOrderTypeStatusMap.Where(item => item.Key == (OrderTransactionType)order.TransactionType).Select(i => i.Value).First();
            if (!allowedStatuses.Contains(orderStatus))
            {
                return BadRequest("Status is not allowed");
            }

            var response = await this._service.ChangeOrderStatus(order, orderStatus, Request.Headers[ConnectionIDHeaderKey]);

            if (response.Success)
            {
                // call the task service
                await _taskQueuer.RecomputeGL(User.BID().Value, (byte)GLEngine.Models.EnumGLType.Order, ID, "Order Status Changed", "Order Status Changed", User.EmployeeID(), null, (byte)GLEntryType.Order_Status_Change);
                await CreateKeyDateBasedOnOrderStatus(order.ID, orderStatus);
            }

            return Ok(response);
        }

        #endregion ChangeOrderStatus

        #region CheckOrderStatus

        /// <summary>
        /// Kicks off the CheckOrderStatus Task on BgEngine
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <returns></returns>
        [Route("{ID}/Action/CheckOrderStatus")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If invalid transaction type.")]
        public async Task<IActionResult> CheckOrderStatus(int ID)
        {
            // get the Order
            var order = await _service.GetAsync(ID);
            if (order == null)
            {
                return new NotFoundResult();
            }

            // Estimate & Purchase Order transaction types should return 400 'Invalid transaction type'
            if (order.TransactionType == (byte)OrderTransactionType.Estimate || order.TransactionType == (byte)OrderTransactionType.PurchaseOrder)
            {
                return BadRequest("Invalid transaction type");
            }

            // call the task service
            await _taskQueuer.CheckOrderStatus(User.BID().Value, ID, (int)order.OrderStatusID);

            return Ok(new EntityActionChangeResponse()
            {
                Success = true
            });
        }

        #endregion CheckOrderStatus

        #region RecomputeGL

        /// <summary>
        /// Kicks off the CheckOrderStatus Task on BgEngine
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="priority">Priority high|medium|low</param>
        /// <returns></returns>
        [Route("{ID}/Action/RecomputeGL")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If invalid order transaction type")]
        public virtual async Task<IActionResult> RecomputeGL(int ID, string priority = "low")
        {
            var priorities = new string[]{ "high", "medium", "low" };
            if (!priorities.Contains(priority))
            {
                return BadRequest("Invalid priority.");
            }
            // get the Order
            var order = await _service.FirstOrDefaultAsync(o => o.BID == User.BID().Value && o.ID == ID && o.TransactionType == (byte)TransactionType);
            if (order == null)
            {
                return NotFound("Order ID does not exist.");
            }
            // call the task service
            _taskQueuer.Priority = priority;
            await _taskQueuer.RecomputeGL(User.BID().Value, (byte)GLEngine.Models.EnumGLType.Order, ID, "Trigger Recompute", "Trigger Recompute", User.EmployeeID(), null, (byte)GLEntryType.Order_Maintenance_Recompute);

            return Ok(new EntityActionChangeResponse()
            {
                Success = true
            });
        }

        #endregion RecomputeGL

        #region PricingCompute

        /// <summary>
        /// Kicks off the PricingCompute Task on BgEngine
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// /// <param name="priority">Priority high|medium|low</param>
        /// <returns></returns>
        [Route("{ID}/Action/Compute")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the specified transaction is in a closed status.")]
        public async Task<IActionResult> PricingCompute(int ID, string priority = "low")
        {
            // get the Order
            var order = await _service.GetAsync(ID);
            if (order == null)
                return NotFound("Order ID does not exist.");

            // Return '400' if Order is Closed
            if (order.OrderStatusID == OrderOrderStatus.OrderClosed)
                return BadRequest("Invalid transaction type");

            // call the task service
            _taskQueuer.Priority = priority;
            await _taskQueuer.OrderPricingCompute(User.BID().Value, ID);

            return Ok(new EntityActionChangeResponse()
            {
                Success = true
            });
        }

        /// <summary>
        /// Kicks off the PricingComputeTax Task on BgEngine
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// /// <param name="priority">Priority high|medium|low</param>
        /// <returns></returns>
        [Route("{ID}/Action/ComputeTax")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the specified transaction is in a closed status.")]
        public async Task<IActionResult> PricingComputeTax(int ID, string priority = "low")
        {
            // get the Order
            var order = await _service.GetAsync(ID);
            if (order == null)
                return NotFound("Order ID does not exist.");

            // Return '400' if Order is Closed
            if (order.OrderStatusID == OrderOrderStatus.OrderClosed)
                return BadRequest("Invalid transaction type");

            // call the task service
            _taskQueuer.Priority = priority;
            await _taskQueuer.OrderPricingComputeTax(User.BID().Value, ID);

            return Ok(new EntityActionChangeResponse()
            {
                Success = true
            });
        }

        #endregion

        #region CanDelete

        /// <summary>
        /// Checks if a Order can be deleted
        /// </summary>
        /// <param name="ID">Order Id</param>
        /// <returns></returns>
        [HttpGet("{ID}/Action/CanDelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        #endregion

        #region ChangeKeyDate - These functions use OrderKeyDateService

        /// <summary>
        /// Sets a key date value to a specified date.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="keyDateType">The Key Date Type to be set</param>
        /// <param name="dateTime">Specified Date/DateTime </param>
        /// <param name="isFirmDate">The Date is a firm date</param>
        /// <returns></returns>
        [Route("{ID}/Action/Date/{keyDateType}/{dateTime}")]
        [HttpPut]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the order fails to set its key date.")]
        public async Task<IActionResult> ChangeKeyDate(int ID, OrderKeyDateType keyDateType, DateTime dateTime, bool? isFirmDate = null)
        {
            EntityActionChangeResponse resp = await this._orderKeyDateService.Value.ChangeKeyDate(ID, keyDateType, dateTime, isFirmDate);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a key date value to the current date.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="keyDateType">The Key Date Type to be set</param>
        /// <param name="isFirmDate">The Date is a firm date</param>
        /// <returns></returns>
        [Route("{ID}/Action/Date/{keyDateType}")]
        [HttpPut]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the order fails to set its key date.")]
        public async Task<IActionResult> ChangeKeyDateToCurrent(int ID, OrderKeyDateType keyDateType, bool? isFirmDate = null)
        {
            EntityActionChangeResponse resp = await this._orderKeyDateService.Value.ChangeKeyDate(ID, keyDateType, null, isFirmDate);
            return resp.ToResult();
        }

        /// <summary>
        /// Removes a key date value.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="keyDateType">The Key Date Type to be set</param>
        /// <returns></returns>
        [Route("{ID}/Action/Date/{keyDateType}")]
        [HttpDelete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the order fails to remove its key date.")]
        public async Task<IActionResult> RemoveKeyDate(int ID, OrderKeyDateType keyDateType)
        {
            EntityActionChangeResponse resp = await this._orderKeyDateService.Value.RemoveKeyDate(ID, keyDateType);
            return resp.ToResult();
        }

        #endregion

    }
}