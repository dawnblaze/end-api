﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Flat list item controller
    /// </summary>
    [Route("api/FlatList")]
    public partial class FlatListController : CRUDController<FlatListItem, FlatListItemService, short>
    {
        /// <summary>
        /// Constructs a FlatList Item Controller
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public FlatListController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) 
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {            
        }

        #region Overridden CRUD Methods
        /// <summary>
        /// Returns all of the MachineDatas
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsoletegetall")]        
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns a FlatListItem based on ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(FlatListItem))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If Flatlist item does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> ReadById(short ID)
        {
            return base.ReadById(ID);
        }

        #endregion Overridden CRUD Methods

        /// <summary>
        /// Returns all of the FlatListItems
        /// can be filtered by IsActive
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(FlatListItem[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] FlatListItemFilter filters)
        {
            if (filters.FlatListType == 0)
                return BadRequest("FlatListType must be specified");

            var list = await _service.GetWithFiltersAsync(filters);
            list = list.OrderBy(i => i.SortIndex).ToList();

            return new OkObjectResult(list);
        }

        /// <summary>
        /// Creates a new FlatListItem
        /// </summary>
        /// <param name="newModel">New FlatListItem data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(FlatListItem))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the creation fails")]
        public override async Task<IActionResult> Create([FromBody] FlatListItem newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Updates a FlatListItem
        /// </summary>
        /// <param name="ID">ID of FlatListItem to update</param>
        /// <param name="update">the Updated FlatListItem model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(FlatListItem))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid or incorrect update model")]
        public override Task<IActionResult> Update(short ID, [FromBody] FlatListItem update)
        {
            return base.Update(ID, update);
        }

        /// <summary>
        /// Deletes a FlatListItem
        /// </summary>
        /// <param name="ID">The ID of the FlatListItem to delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async override Task<IActionResult> Delete(short ID)
        {
            var resp = await _service.ForceDelete(ID);
            return resp.ToResult();
        }

        /// <summary>
        /// Returns a simplelist of FlatListItems
        /// </summary>
        /// <param name="filters">The Filters specified to return</param>
        /// <returns></returns>
        [HttpGet("simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleFlatListItem[]), description: "A simple list of flatlistitems is returned")]
        public async Task<IActionResult> SimpleList([FromQuery] FlatListItemFilter filters)
        {
            if (filters.FlatListType == 0)
                return BadRequest("FlatListType must be specified");

            return Ok(await _service.SimpleListAsync(filters));
        }

        /// <summary>
        /// Clone
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost("obsolete")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Clone(short ID)
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Clone(ID);
        }

        /// <summary>
        /// Attempts to validate model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            var flatListItem = model as FlatListItem;
            var sharedNames = this._service.GetSharedFlatListName(flatListItem);

            if(sharedNames.Count > 0)
            {
                this.ModelState.AddModelError("Name", "Name already exists");
            }

            return base.TryValidateModel(model, prefix);
        }

        /// <summary>
        /// Pagination List
        /// </summary>
        /// <param name="filters">Query parameters to filter a property</param>
        /// <param name="skip">Number of items to skip</param>
        /// <param name="take">Number of items to take</param>
        /// <returns></returns>
        [HttpGet("paged")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(FlatListItem[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetPagedList([FromQuery] FlatListItemFilter filters,
                                                        [FromQuery]int? skip = null,
                                                        [FromQuery]int? take = null)
        {
            if (filters.FlatListType == 0)
                return BadRequest("FlatListType must be specified");

            var list = await _service.GetPagedListAsync(filters, skip, take, null);
            list.Results.OrderBy(i => i.SortIndex);

            return new OkObjectResult(list);
        }
    }
}
