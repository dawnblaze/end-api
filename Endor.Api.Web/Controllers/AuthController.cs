﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Web.Annotation;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Endpoints for getting info on your user
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/auth")]
    [Authorize]
    public class AuthController : Controller
    {
        private ApiContext _context;
        private RemoteLogger _logger;
        private IRTMPushClient _rtmClient;
        private ITaskQueuer _taskQueuer;
        private ITenantDataCache _cache;
        private IMigrationHelper _migrationHelper;
        private EndorOptions _options;
        /// <inheritdoc />
        /// <summary>
        /// constructor
        /// </summary>
        public AuthController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, EndorOptions options, IMigrationHelper migrationHelper)
        {
            _context = context;
            _logger = logger;
            _rtmClient = rtmClient;
            _taskQueuer = taskQueuer;
            _cache = cache;
            _options = options;
            _migrationHelper = migrationHelper;
        }

        /// <summary>
        /// Gets all support users
        /// </summary>
        /// <returns></returns>
        [HttpGet("user")]
        public async Task<IActionResult> GetAllUsers()
        {
            if (User.BID() == null)
                return Unauthorized();

            if (!IsUserAuthorizedForAccessType(UserAccessType.SupportStaff))
                return Unauthorized();

            List<UserLink> lstUser = await new AuthService(_context, User.BID().Value, _rtmClient, _taskQueuer, _migrationHelper).GetAllSupportUsers();
            return Ok(lstUser);
        }

        /// <summary>
        /// Add support user
        /// </summary>
        /// <returns></returns>
        [HttpPost("user")]
        public async Task<IActionResult> AddNewUser([FromQuery] string UserName, [FromQuery] string DisplayName, [FromQuery] byte AccessType)
        {
            if (string.IsNullOrEmpty(UserName))
                return BadRequest("Empty EmailAddress.");

            if (string.IsNullOrEmpty(DisplayName))
                return BadRequest("Empty DisplayName.");

            if (User.BID() == null)
                return Unauthorized();

            if (!IsUserAuthorizedForAccessType(UserAccessType.SupportStaff))
                return Unauthorized();

            EntityActionChangeResponse<short> sendInviteResponse = await new AuthService(_context, User.BID().Value, _rtmClient, _taskQueuer, _migrationHelper).AddNewSupportUser(UserName, DisplayName, AccessType, _options);
            if (sendInviteResponse.Success)
            {
                return Ok(sendInviteResponse);
            }
            else
            {
                return BadRequest(sendInviteResponse.ErrorMessage);
            }
            
        }
        /// <summary>
        /// Checks the User's AccessType Claim and compares against the indicated UserAccessType
        /// </summary>
        /// <param name="accessType">UserAccessType enum to match or exceed for authorization</param>
        /// <returns></returns>
        protected bool IsUserAuthorizedForAccessType(UserAccessType accessType)
        {
            var accessTypeClaim = User.Claims.FirstOrDefault(c => c.Type.Equals("accesstype"))?.Value;
            if (accessTypeClaim == null)
                return false;

            var userAccessType = Enum.Parse<UserAccessType>(accessTypeClaim);
            if (userAccessType >= accessType)
                return true;
            else
                return false;
        }

    }
}
