﻿using Endor.Api.Web.Classes;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    public partial class FlatListController
    {
        /// <summary>
        /// Sets the flat list item to active via ID
        /// </summary>
        /// <remarks>
        /// Sets the specified contact Active.  Always succeeds, even if the record is already active.
        /// </remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("{id}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse), description: "Sets the specified contact Active.  Always succeeds, even if the record is already active.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Specified ID does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials")]
        public async Task<IActionResult> SetActive(short id)
        {
            var setActiveResult = await _service.SetActive(id, true, Request.Headers[ConnectionIDHeaderKey]);
            if (setActiveResult == null || !setActiveResult.Success)
                return NotFound();

            return Ok(setActiveResult);
        }

        /// <summary>
        /// Sets the flat list item to inactive via ID
        /// </summary>
        /// <remarks>
        /// Sets the specified contact inctive.  Always succeeds, even if the record is already inactive.
        /// </remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("{id}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse), description: "Sets the specified contact inactive.  Always succeeds, even if the record is already inactive.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Specified ID does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials")]
        public async Task<IActionResult> SetInactive(short id)
        {
            var setActiveResult = await _service.SetActive(id, false, Request.Headers[ConnectionIDHeaderKey]);
            if (setActiveResult == null || !setActiveResult.Success)
                return NotFound();

            return Ok(setActiveResult);
        }

        /// <summary>
        /// Tests to see if record is deletable
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/action/candelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse), description: "Sets the specified contact inactive.  Always succeeds, even if the record is already inactive.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Specified ID does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials")]
        public async Task<IActionResult> CanDelete(short id)
        {
            return (await this._service.CanDelete((short)id)).ToResult();
        }

        /// <summary>
        /// Deletes a flat list item....forcefully
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Obsolete]
        [HttpPost("{id}/action/forcedelete")]
        public async Task<IActionResult> ForceDelete(short id)
        {
            var result = await _service.ForceDelete(id);
            return result.ToResult();
        }

        /// <summary>
        /// Deletes a flat list item....forcefully
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Obsolete]
        [HttpPost("action/forcecreate")]
        public async Task<IActionResult> ForceCreate([FromBody] FlatListItem model)
        {
            var result = await _service.ForceCreate(model);
            return result.ToResult();
        }

        /// <summary>
        /// Reorders the Source record so it's sort index is lower than the sort index of the specified Target record.  
        /// </summary>
        /// <remarks>
        /// Both records must be for the same FlatListType Enum.
        /// The records must be different.
        /// This action may involve renumbering all records in the List.  This should be done in a single SQL call.
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="targetID"></param>
        /// <returns></returns>
        [HttpPost("{id}/action/movebefore/{targetID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Reorders the Source record so it's sort index is lower than the sort index of the specified Target record.  ")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Specified ID does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid target type or target type does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials")]
        public async Task<IActionResult> MoveBefore(short id, short targetID)
        {
            return (await _service.MoveBefore(id, targetID)).ToResult();
        }

        /// <summary>
        /// Reorders the Source record so it's sort index is higher than the sort index of the specified Target record.  
        /// </summary>
        /// <remarks>
        /// Both records must be for the same FlatListType Enum.
        /// The records must be different.
        /// This action may involve renumbering all records in the List.  This should be done in a single SQL call.
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="targetID"></param>
        /// <returns></returns>
        [HttpPost("{id}/action/moveafter/{targetID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Reorders the Source record so it's sort index is higher than the sort index of the specified Target record.  ")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Specified ID does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid target type or target type does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials")]
        public async Task<IActionResult> MoveAfter(short id, short targetID)
        {
            return (await _service.MoveAfter(id, targetID)).ToResult();
        }
    }
}
