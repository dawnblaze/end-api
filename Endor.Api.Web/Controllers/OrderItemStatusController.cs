﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// OrderItemStatus Service
    /// </summary>
    [Route("api/orderitemstatus")]
    public class OrderItemStatusController : CRUDController<OrderItemStatus, OrderItemStatusService, short>, ISimpleListableController<SimpleOrderItemStatus, short>
    {
        /// <summary>
        /// OrderItemStatus API Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderItemStatusController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns all OrderItemStatus and any included child objects specified in query parameters
        /// </summary>
        /// <param name="filters">Query parameters to filter the OrderItemStatuses</param>
        /// <param name="includes">Query parameters to include Substatuses</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemStatus[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetMany([FromQuery]OrderItemStatusFilters filters, [FromQuery] OrderItemStatusIncludes includes)
        {
            return await this.FilteredReadWith(filters.WherePredicate(), includes);
        }

        /// <summary>
        /// Gets a Simple list of OrderItemStatuss
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleOrderItemStatus[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public SimpleOrderItemStatus[] FilteredSimpleList([FromQuery] SimpleOrderItemStatusFilters filters)
        {
            var q = this._service.GetSimpleList(User.BID().Value, filters);
            return  q.ToArray();
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsoletesimplelist")]
        [Obsolete]
        public Task<SimpleOrderItemStatus[]> SimpleList()
        {
            return Task.FromResult(this.FilteredSimpleList(null));
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the OrderItemStatuss
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single OrderItemStatus by ID
        /// </summary>
        /// <param name="ID">OrderItemStatus ID</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemStatus))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItemStatus does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetOneWithIncludes([FromRoute] short ID, [FromQuery] OrderItemStatusIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }

        /// <summary>
        /// Returns a single OrderItemStatus by ID
        /// </summary>
        /// <param name="ID">OrderItemStatus ID</param>
        /// <returns></returns>
        [HttpGet("obsolete/{ID}")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> ReadById(short ID)
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single OrderItemStatus by ID
        /// </summary>
        /// <param name="ID">OrderItemStatus ID</param>
        /// <param name="update">Updated OrderItemStatus data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemStatus))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItemStatus does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderItemStatus does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] OrderItemStatus update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new OrderItemStatus
        /// </summary>
        /// <param name="newModel">New OrderItemStatus data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemStatus))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderItemStatus creation fails")]
        public override async Task<IActionResult> Create([FromBody] OrderItemStatus newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing OrderItemStatus to a new OrderItemStatus
        /// </summary>
        /// <param name="ID">ID of the OrderItemStatus to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemStatus))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source OrderItemStatus does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source OrderItemStatus does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes a OrderItemStatus by ID
        /// </summary>
        /// <param name="ID">ID of the OrderItemStatus to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.Forbidden, description: "The Default ItemStatus can't be deleted.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItemStatus does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderItemStatus does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            BooleanResponse resp = await this._service.CanDelete(ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                return await base.Delete(ID);
            }
            var responseMessage = resp?.Message;
            if (responseMessage != null && responseMessage.Contains("Forbidden"))
            {
                return Forbid();
            }
            return new BadRequestObjectResult(resp);
        }

        #endregion

        #region Actions

        /// <summary>
        /// Sets a OrderItemStatus to to Active
        /// </summary>
        /// <param name="ID">OrderItemStatus ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the OrderItemStatus does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the OrderItemStatus does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a OrderItemStatus to to Inactive
        /// </summary>
        /// <param name="ID">OrderItemStatus ID</param>
        /// <returns></returns>
        [Route("{ID}/action/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the OrderItemStatus does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Forbidden, description: "403 - Forbidden - The Default ItemStatus can't be set inactive.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the OrderItemStatus does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            OrderItemStatus orderItemStatus = await _service.GetAsync(ID);

            if (orderItemStatus.IsDefault == true)
            {
                return StatusCode(403, "403 - Forbidden - The Default ItemStatus can't be set inactive.");
            }

            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the OrderItemStatus can be deleted.
        /// </summary>
        /// <param name="ID">OrderItemStatus ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Clears the default setting on any other OrderItemStatus under that OrderItemStatus
        /// </summary>
        /// <param name="ID">OrderItemStatus ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setdefault")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the OrderItemStatus does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the OrderItemStatus does not exist or fails to set as default.")]
        public async Task<IActionResult> SetDefault(short ID)
        {
            EntityActionChangeResponse resp = await this._service.SetDefault(ID);

            return resp.ToResult();
        }

        /// <summary>
        /// Reorders the ItemStatus so it's sort order is lower than the sort order of the specified (second) ItemStatus.
        /// </summary>
        /// <param name="orderItemStatusID">OrderItemStatus ID</param>
        /// <param name="moveBeforeID">Move Before ID</param>
        /// <returns></returns>
        [HttpPost("{orderItemStatusID}/action/movebefore/{moveBeforeID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If one of the specified IDs does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the specified statuses are not in the same Order Status or the move is not valid.")]
        public async Task<IActionResult> MoveBefore(short orderItemStatusID, short moveBeforeID)
        {
            EntityActionChangeResponse resp = await this._service.MoveBefore(orderItemStatusID, moveBeforeID);

            return resp.ToResult();
        }

        /// <summary>
        /// Reorders the ItemStatus so it's sort order is higher than the sort order of the specified (second) ItemStatus.
        /// </summary>
        /// <param name="orderItemStatusID">OrderItemStatus ID</param>
        /// <param name="moveAfterID">Move After ID</param>
        /// <returns></returns>
        [HttpPost("{orderItemStatusID}/action/moveafter/{moveAfterID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If one of the specified IDs does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the specified statuses are not in the same Order Status or the move is not valid.")]
        public async Task<IActionResult> MoveAfter(short orderItemStatusID, short moveAfterID)
        {
            EntityActionChangeResponse resp = await this._service.MoveAfter(orderItemStatusID, moveAfterID);

            return resp.ToResult();
        }

        /// <summary>
        /// Links the orderItemStatus to the specified SubStatus.
        /// </summary>
        /// <param name="orderItemStatusID">OrderItemStatus ID</param>
        /// <param name="orderItemSubStatus">orderItemSubStatus name or ID</param>
        /// <returns></returns>
        [HttpPost("{orderItemStatusID}/action/linksubstatus/{orderItemSubStatus}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the specified ID does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> LinkSubStatus(short orderItemStatusID, string orderItemSubStatus)
        {
            EntityActionChangeResponse resp = await this._service.LinkSubStatus(orderItemStatusID, orderItemSubStatus, true);
            return resp.ToResult();
        }

        /// <summary>
        /// Deletes the Link between the orderItemStatus and the specified Substatus.
        /// </summary>
        /// <param name="orderItemStatusID">OrderItemStatus ID</param>
        /// <param name="orderItemSubStatus">orderItemSubStatus name or ID</param>
        /// <returns></returns>
        [HttpPost("{orderItemStatusID}/action/unlinksubstatus/{orderItemSubStatus}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> UnLinkSubStatus(short orderItemStatusID, short orderItemSubStatus)
        {
            EntityActionChangeResponse resp = await this._service.LinkSubStatus(orderItemStatusID, orderItemSubStatus.ToString(), false);
            return resp.ToResult();
        }

        #endregion

    }
}
