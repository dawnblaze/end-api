﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Pricing;

using Endor.Api.Web.Annotation;
using Microsoft.Extensions.Caching.Memory;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for OrderItems
    /// </summary>
    [Authorize]
    [Route("api/[Controller]")]
    public partial class OrderItemController : CRUDController<OrderItemData, OrderItemService, int>, ISimpleListableViewController<SimpleOrderItemData, int>
    {
        private readonly Lazy<OrderItemNoteService> _orderNoteService;
        private readonly Lazy<OrderContactRoleService> _orderContactRoleService;
        private readonly Lazy<OrderEmployeeRoleService> _orderEmployeeRoleService;
        private readonly Lazy<PricingService> _pricingService;
        private readonly IPricingEngine pricingEngine;
        private ITaskQueuer _taskQueuer;
        private readonly IMemoryCache _inMemoryCache;

        /// <summary>
        /// Listable service for Orders
        /// </summary>
        public ISimpleListableViewService<SimpleOrderItemData, int> ListableService => this._service;

        /// <summary>
        /// Api Endpoint for Orders
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="pricingEngine">Pricing Engine</param>
        /// <param name="migrationHelper"></param>
        /// <param name="memoryCache">Memory Cache</param>
        public OrderItemController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IPricingEngine pricingEngine, IMigrationHelper migrationHelper, IMemoryCache memoryCache)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
            this._taskQueuer = taskQueuer;
            this.pricingEngine = pricingEngine;
            this._orderNoteService = new Lazy<OrderItemNoteService>(() => new OrderItemNoteService(context, this.User.BID().Value, taskQueuer, cache, logger, rtmClient, migrationHelper));
            this._orderContactRoleService = new Lazy<OrderContactRoleService>(() => new OrderContactRoleService(context, this.User.BID().Value, taskQueuer, cache, logger, rtmClient, migrationHelper));
            this._pricingService = new Lazy<PricingService>(() => new PricingService(context, logger, this.User.BID().Value, rtmClient, pricingEngine, migrationHelper,cache, memoryCache, taskQueuer));
            this._orderEmployeeRoleService = new Lazy<OrderEmployeeRoleService>(() => new OrderEmployeeRoleService(context, this.User.BID().Value, taskQueuer, cache, logger, rtmClient, migrationHelper));
        }

        /// <summary>
        /// Gets an array of OrderItems and with any included child objects specified in query parameters
        /// </summary>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> GetOrderItemsWithIncludes([FromQuery] OrderItemIncludes includes)
        {
            return new OkObjectResult(await _service.GetOrderItemsWithIncludes(includes));
        }

        /// <summary>
        /// Returns a single OrderItem by ID and any included child objects specified in query parameters
        /// </summary>
        /// <param name="ID">OrderItem ID</param>
        /// <param name="version">OrderItem's Order Version</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> GetOrderWithIncludes(int ID, int? version, [FromQuery] OrderItemIncludes includes)
        {
            if (version.HasValue)
                return new OkObjectResult(await _service.GetForVersion(ID, version.Value, includes));
            else
                return await base.ReadWith(ID, includes);
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the Orders
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public override async Task<IActionResult> Read()
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns a single OrderItem by ID
        /// </summary>
        /// <param name="ID">OrderItem ID</param>
        /// <returns></returns>
        [HttpGet("{ID}/obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single OrderItem by ID
        /// </summary>
        /// <param name="ID">OrderItem ID</param>
        /// <param name="update">Updated OrderItem data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderItem does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] OrderItemData update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new OrderItem
        /// </summary>
        /// <param name="newModel">New OrderItem data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderItem creation fails.")]
        public override async Task<IActionResult> Create([FromBody] OrderItemData newModel, [FromQuery] Guid? tempID = null)
        {
#warning Temporary Fix for front end submitting OrderItems without names
            if (tempID.HasValue && String.IsNullOrWhiteSpace(newModel.Name))
            {
                newModel.Name = "_";
            }

            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing OrderItem to a new OrderItem
        /// </summary>
        /// <param name="ID">ID of the OrderItem to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source OrderItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source OrderItem does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes an OrderItem by ID
        /// </summary>
        /// <param name="ID">ID of the OrderItem to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderItem does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderItem does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }

        #endregion

        /// <summary>
        /// Gets a Simple list of OrderItems
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleOrderItemData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<SimpleOrderItemData[]> SimpleList()
        {
            return await this.GetSimpleList<SimpleOrderItemData, int>(User.BID().Value);
        }

        #region OrderItemNotes
        /// <summary>
        /// Gets a List of Order Notes
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="orderNoteType">OrderNoteType</param>
        /// <returns></returns>
        [Route("{ID}/notes")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderNote[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ListOrderNotes(int ID, [FromQuery] OrderNoteType orderNoteType = OrderNoteType.Other)
        {
            List<OrderNote> notes = await this._orderNoteService.Value.GetWhere(n => n.BID == User.BID().Value && n.OrderItemID == ID && n.NoteType == orderNoteType);
            return Ok(notes.ToArray());
        }

        /// <summary>
        /// Returns an OrderNote Object with the specified note id for the specified OrderItem.
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="noteID">OrderNote ID</param>
        /// <returns></returns>
        [Route("{ID}/notes/{noteID}")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderNote[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public IActionResult GetOrderNote(int ID, int noteID)
        {
            OrderNote note = this._orderNoteService.Value.Where(n => n.BID == User.BID().Value && n.OrderItemID == ID && n.ID == noteID).FirstOrDefault();
            if (note == null)
            {
                return NotFound();
            }
            return Ok(note);
        }

        /// <summary>
        /// Creates an OrderNote
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="noteType">OrderNoteType</param>
        /// <param name="orderNote">OrderNote updates</param>
        /// <returns></returns>
        [Route("{ID}/action/notes/{noteType}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderNote))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> CreateOrderNote(int ID, OrderNoteType noteType, [FromBody] OrderNote orderNote)
        {
            orderNote.NoteType = noteType;
            orderNote.OrderItemID = ID;
            orderNote.BID = User.BID().Value;
            var result = await this._orderNoteService.Value.CreateAsync(orderNote);
            if (result == null)
            {
                await _logger.Error(orderNote.BID, "AddAsync returned null", null);
                return new BadRequestObjectResult("Creation failed");
            }
            return new OkObjectResult(orderNote);
        }

        /// <summary>
        /// Updates an OrderNote
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="noteID">OrderNote ID</param>
        /// <param name="orderNote">OrderNote updates</param>
        /// <returns></returns>
        [Route("{ID}/action/notes/{noteID}")]
        [HttpPut]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderNote))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> UpdateOrderNote(int ID, int noteID, [FromBody] OrderNote orderNote)
        {
            orderNote.OrderItemID = ID;
            orderNote.ID = noteID;
            orderNote.BID = User.BID().Value;
            var result = await this._orderNoteService.Value.UpdateAsync(orderNote, Request.Headers[ConnectionIDHeaderKey]);
            if (result == null)
            {
                await _logger.Error(orderNote.BID, "UpdateAsync returned null", null);
                return new BadRequestObjectResult("Update failed");
            }
            return new OkObjectResult(orderNote);
        }

        /// <summary>
        /// Creates or Updates an OrderNote
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="noteID">OrderNote ID</param>
        /// <returns></returns>
        [Route("{ID}/action/notes/{noteID}/setactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderNote))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> OrderNoteSetActive(int ID, int noteID)
        {
            EntityActionChangeResponse<int> resp = await this._orderNoteService.Value.SetActive(noteID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Creates or Updates an OrderNote
        /// </summary>
        /// <param name="ID">Order ID</param>
        /// <param name="noteID">OrderNote ID</param>
        /// <returns></returns>
        [Route("{ID}/action/notes/{noteID}/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderNote))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> OrderNoteSetInactive(int ID, int noteID)
        {
            EntityActionChangeResponse<int> resp = await this._orderNoteService.Value.SetActive(noteID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Creates or Updates an OrderNote
        /// </summary>
        /// <returns></returns>
        [Route("{ID}/action/notes/{noteID}")]
        [HttpDelete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<EntityActionChangeResponse> DeleteOrderNote(int ID, int noteID)
        {
            OrderNote toDelete = this._orderNoteService.Value.Where(n => n.BID == User.BID().Value && n.OrderItemID == ID && n.ID == noteID).FirstOrDefault();
            if (toDelete == null)
            {
                return new EntityActionChangeResponse
                {
                    Id = noteID,
                    Message = "No rows affected.",
                    Success = false
                };
            }
            else
            {
                // when this returns, the classtypeID = 0 (i don't know why at this point)
                var isDeleted = await this._orderNoteService.Value.DeleteAsync(toDelete);

                if (isDeleted)
                    return new EntityActionChangeResponse
                    {
                        Id = noteID,
                        Message = "Successfully deleted order note.",
                        Success = true
                    };
                else
                    return new EntityActionChangeResponse
                    {
                        Id = noteID,
                        Message = "No rows affected.",
                        Success = false
                    };
            }
        }
        #endregion OrderItemNotes

        #region Pricing
        /// <summary>
        /// Computes the price of a line item not already saved.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="computeTax"></param>
        /// <returns></returns>
        [Route("compute")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ItemPriceResult))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> Compute([FromBody] ItemPriceRequest request, [FromQuery] bool? computeTax = null)
        {
            if (request == null)
                return BadRequest("Price request object is missing of invalid.");

            if (computeTax.GetValueOrDefault(false))
            {
                if ((request.TaxInfoList == null) || (request.TaxInfoList.Count == 0) || String.IsNullOrWhiteSpace(request.TaxInfoList.First().NexusID))
                {
                    return BadRequest("ItemPriceRequest TaxInfoList is not defined correctly");
                }
            }

            try
            {
                ItemPriceResult result = await _pricingService.Value.ComputeItem(request, computeTax.GetValueOrDefault(false));
                return Ok(result);
            }

            catch (Exception err)
            {
                return StatusCode(500, err);
            }
        }
        /// <summary>
        /// Computes the price of a line item's components
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("componentcost")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderItemComponentCostResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> GetOrderItemComponentCosts([FromBody] OrderItemComponentCostRequest request)
        {
            if (request == null)
                return BadRequest("Price request object is missing of invalid.");

            try
            {
                OrderItemComponentCostResponse result = await _service.GetComponentCostAsync(request);
                return Ok(result);
            }

            catch (Exception err)
            {
                return StatusCode(500, err);
            }
        }
        #endregion Pricing
    }
}
