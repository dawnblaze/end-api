﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Services;
using Endor.Models;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Endor.Api.Web.Classes;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Employee Role controller for CRUD Operations
    /// </summary>
    [Route("api/[controller]")]
    public class EmployeeRoleController : CRUDController<EmployeeRole, EmployeeRoleService, short>
    {
        /// <summary>
        /// Constructs an employee role controller
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public EmployeeRoleController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Obsolete Read Method
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsolete")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Creates a new Employee Role
        /// </summary>
        /// <param name="newModel">The employee role model to create</param>
        /// <param name="tempID">A temporary ID used when creating objects with document storage</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeRole), "Created a new employee role")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid parameters were supplied or there was a different issue with the request")]
        public override Task<IActionResult> Create([FromBody] EmployeeRole newModel, [FromQuery] Guid? tempID = null)
        {
            return base.Create(newModel, tempID);
        }

        /// <summary>
        /// Get a list of Employee Roles based on supplied criteria
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <returns>Returns an array of EmployeeRole</returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeRole[]), "An array of Employee Roles")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid parameters were supplied or there was a different issue with the request")]
        public async Task<IActionResult> ReadWithFilters([FromQuery]EmployeeRoleFilters filters)
        {
            List<EmployeeRole> result = await _service.GetWithFiltersAsync(filters);
            if (result == null)
            {
                return BadRequest();
            }

            return Ok(result);
        }

        /// <summary>
        /// Gets a single Employee Role by ID
        /// </summary>
        /// <param name="ID">The ID of the employee role to be retrieved</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeRole), "Successfully retrieved the employee role")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Employee role was not found")]
        public override Task<IActionResult> ReadById(short ID)
        {
            return base.ReadById(ID);
        }

        /// <summary>
        /// Updates an Employee Role
        /// </summary>
        /// <param name="ID">The Id of the employee role to update</param>
        /// <param name="update">Contains the updated employee role model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeRole), "Successfully updated the employee role")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest,description:"Invalid request properties")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description:"Employee role was not found")]
        public override Task<IActionResult> Update(short ID, [FromBody] EmployeeRole update)
        {
            return base.Update(ID, update);
        }

        /// <summary>
        /// Deletes an Employee Role
        /// </summary>
        /// <param name="ID">The ID of the employee role to be deleted</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description:"Employee role is no longer found")]
        public override Task<IActionResult> Delete(short ID)
        {
            return base.Delete(ID);
        }

        /// <summary>
        /// Clones an Employee role
        /// </summary>
        /// <param name="ID">The ID of the employee role to be cloned</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeRole), "Cloned an employee role")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description:"Employee role to clone was not found")]
        public override Task<IActionResult> Clone(short ID)
        {
            return base.Clone(ID);
        }

        /// <summary>
        /// Returns a list of simple employee roles
        /// </summary>
        /// <param name="filters">A list of filter parameters</param>
        /// <returns></returns>
        [HttpGet("simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleEmployeeRole[]), "An array of Simple Employee Roles")]
        public async Task<SimpleEmployeeRole[]> SimpleList(EmployeeRoleFilters filters)
        {
            return await _service.GetSimpleEmployeeRolesWithFiltersAsync(filters);
        }

        /// <summary>
        /// Sets a EmployeeRole to Active
        /// </summary>
        /// <param name="ID">EmployeeRole ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the EmployeeRole does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the EmployeeRole does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets a EmployeeRole to Inactive
        /// </summary>
        /// <param name="ID">EmployeeRole ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the EmployeeRole does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the EmployeeRole does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Pagination List
        /// </summary>
        /// <param name="filters">Query parameters to filter a property</param>
        /// <param name="skip">Number of items to skip</param>
        /// <param name="take">Number of items to take</param>
        /// <returns></returns>
        [HttpGet("paged")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeRole[]), "An array of Employee Roles")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid parameters were supplied or there was a different issue with the request")]
        public async Task<IActionResult> GetPagedList([FromQuery] EmployeeRoleFilters filters,
                                                        [FromQuery]int? skip = null,
                                                        [FromQuery]int? take = null)
        {
            return new OkObjectResult(await this._service.GetPagedListAsync(filters, skip, take, null));
        }
    }
}
