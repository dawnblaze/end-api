﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// AlertDefinition Controller
    /// </summary>
    [Route("api/alert/definition")]
    public class AlertDefinitionController : CRUDController<AlertDefinition, AlertDefinitionService, short>, ISimpleListableViewController<SimpleAlertDefinition, short>
    {
        /// <summary>
        /// Alert Definition Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public AlertDefinitionController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Obsolete Read Method
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsolete")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Creates a new Alert Definition
        /// </summary>
        /// <param name="newModel">The Alert Definition model to create</param>
        /// <param name="tempID">A temporary ID used when creating objects with document storage</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AlertDefinition), "Created a new alert definition")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid parameters were supplied or there was a different issue with the request")]
        public override Task<IActionResult> Create([FromBody] AlertDefinition newModel, [FromQuery] Guid? tempID = null)
        {
            newModel.Trigger = null; //Triggers sent from the client can't be saved because they already exist
            return base.Create(newModel, tempID);
        }

        /// <summary>
        /// Get a list of Alert Definition based on supplied criteria
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <returns>Returns an array of Alert Definition</returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AlertDefinition[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] AlertDefinitionFilter filters)
        {
            return new OkObjectResult(await this._service.GetWithFiltersAsync(filters));
        }

        /// <summary>
        /// Gets a single Alert Definition by ID
        /// </summary>
        /// <param name="ID">The ID of the Alert Definition to be retrieved</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AlertDefinition), "Successfully retrieved the Alert Definition")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Alert Definition was not found")]
        public override Task<IActionResult> ReadById(short ID)
        {
            return base.ReadById(ID);
        }


        /// <summary>
        /// Updates a Alert Definition
        /// </summary>
        /// <param name="ID">The Id of the Alert Definition to update</param>
        /// <param name="update">Contains the updated Alert Definition model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AlertDefinition), "Successfully updated the Alert Definition")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid request properties")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Alert Definition was not found")]
        public override Task<IActionResult> Update(short ID, [FromBody] AlertDefinition update)
        {
            return base.Update(ID, update);
        }

        /// <summary>
        /// Deletes a Alert Definition
        /// </summary>
        /// <param name="ID">The ID of the Alert Definition to be deleted</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Alert Definition is no longer found")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Delete(short ID)
        {
            BooleanResponse resp = await this._service.CanDelete((short)ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                return await base.Delete(ID);
            }
            else
            {
                return new BadRequestObjectResult(resp);
            }
        }

        /// <summary>
        /// Clones an existing Alert Definition to a new Alert Definition
        /// </summary>
        /// <param name="ID">ID of the Alert Definition to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AlertDefinition))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Alert Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Alert Definition does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Listable service for Location Controller
        /// </summary>
        public ISimpleListableViewService<SimpleAlertDefinition, short> ListableService => this._service;
        /// <summary>
        /// Get simple list of SimpleAlertDefinition
        /// </summary>
        /// <returns>SimpleAlertDefinition[]</returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleAlertDefinition[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleAlertDefinition[]> FilteredSimpleList([FromQuery] SimpleAlertDefinitionFilter filters)
        {
            return await this.GetSimpleList(User.BID().Value, filters);
        }

        /// <summary>
        /// Try's to validate the data model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            if (model == null || !(model is AlertDefinition))
            {
                this.ModelState.AddModelError("model", "model is not an alert definition");
                return false;
            }

            AlertDefinition alert = (AlertDefinition)model;

            if (String.IsNullOrWhiteSpace(alert.Name))
            {
                this.ModelState.AddModelError("Name", "Alert Name is required");
            }

            base.TryValidateModel(model, prefix);
            this.ModelState.Remove("create.Name");
            return this.ModelState.IsValid;
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [Route("obsoletesimplelist")]
        [Obsolete]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleAlertDefinition[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleAlertDefinition[]> SimpleList()
        {
            return await this.GetSimpleList<SimpleAlertDefinition, short>(User.BID().Value);
        }

        /// <summary>
        /// Sets a Alert Definition to Active
        /// </summary>
        /// <param name="ID">Alert Definition ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Alert Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Alert Definition does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets an Alert Definition to Inactive
        /// </summary>
        /// <param name="ID">Alert Definition ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Alert Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Alert Definition does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the Alert Definition can be deleted.
        /// </summary>
        /// <param name="ID">Alert Definition ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Alert Definition does not exist.")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Updates the LastRunDT and CummRunCount fields for the Alert Definition.  This is called when the Alert is fired to record that is was run.
        /// </summary>
        /// <param name="ID">AlertDefinition ID</param>
        /// <returns></returns>
        [Route("{ID}/action/postresults")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Alert Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Alert Definition does not exist or fails to post results.")]
        public async Task<IActionResult> PostResults(short ID)
        {
            var resp = await this._service.PostResults(ID);
            return resp.ToResult();
        }

        /// <summary>
        /// Resets the LastRunDT and CummRunCount fields to NULL.
        /// </summary>
        /// <param name="ID">AlertDefinition ID</param>
        /// <returns></returns>
        [Route("{ID}/action/resettimer")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Alert Definition does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Alert Definition does not exist or fails to reset the timer.")]
        public async Task<IActionResult> ResetTimer(short ID)
        {
            var resp = await this._service.ResetTimer(ID);
            return resp.ToResult();
        }

        /// <summary>
        /// Resets the LastRunDT and CummRunCount fields to NULL for all Alerts for the given BID. 
        /// </summary>
        /// <returns></returns>
        [Route("all/action/resettimer")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntitiesActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntitiesActionChangeResponse), description: "If it fails to reset the timers.")]
        public async Task<IActionResult> ResetTimerForAll()
        {
            var resp = await this._service.ResetAllTimers();
            return resp.ToResult();
        }
    }
}
