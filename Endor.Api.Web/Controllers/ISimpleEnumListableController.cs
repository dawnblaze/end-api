﻿using Endor.Api.Web.Services;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// A controller that exposes simple listable enums
    /// </summary>
    /// <typeparam name="SLI"></typeparam>
    /// <typeparam name="I"></typeparam>
    public interface ISimpleEnumListableController<SLI, I> where SLI : SimpleEnumListItem<I>
    {
        /// <summary>
        /// a getter for an enum listable service
        /// </summary>
        ISimpleEnumListableService<SLI, I> ListableService { get; }

        /// <summary>
        /// public endpoint for an array of simple list objects from an enum
        /// </summary>
        /// <returns></returns>
        Task<SLI[]> SimpleEnumList();
    }

    /// <summary>
    /// Extension methods for ISimpleEnumListableController
    /// </summary>
    public static class SimpleEnumListableExtensions
    {
        /// <summary>
        /// Gets a simple enum list from the ListableService
        /// </summary>
        /// <typeparam name="SLI"></typeparam>
        /// <typeparam name="I"></typeparam>
        /// <param name="controller"></param>
        /// <returns></returns>
        public static async Task<SLI[]> GetSimpleEnumList<SLI, I>(this ISimpleEnumListableController<SLI, I> controller) where SLI: SimpleEnumListItem<I>{
            return await controller.ListableService.SimpleEnumListSet.ToArrayAsync();
        }
    }
}
