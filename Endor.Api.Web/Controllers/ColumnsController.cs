﻿using Endor.Api.Web.Services;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Gets the list of Columns for the given CTID
    /// </summary>
    /// <returns></returns>
    [Authorize]
    [Route("api/list/{CTID}/[controller]")]
    public class ColumnsController : Controller
    {
        private readonly ApiContext _ctx;
        private readonly Lazy<ListColumnService> _lazyService;
        private ListColumnService _service { get { return _lazyService.Value; } }

        /// <summary>
        /// Api Endpoint for Columns
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="migrationHelper"></param>
        public ColumnsController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, IMigrationHelper migrationHelper)
        {
            this._ctx = context;
            migrationHelper.MigrateDb(_ctx);
            this._lazyService = new Lazy<ListColumnService>(() => new ListColumnService(context, User.BID().Value, migrationHelper));
        }

        /// <summary>
        /// Creates a new Column
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="filter">SystemListColumn data</param>
        [HttpPost]
        public Task<IActionResult> Create(int CTID, SystemListColumn filter)
        {
            return Task.FromResult<IActionResult>(new NotFoundResult());
        }

        /// <summary>
        /// Get Columns
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        [HttpGet]
        public async Task<IActionResult> Read(int CTID)
        {
            List<SystemListColumn> columns = await _service.GetColumnsAsync(CTID, User.UserLinkID().Value);

            return new OkObjectResult(columns);
        }

        /// <summary>
        /// Update Column
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="filter">SystemListColumn data</param>
        [HttpPut]
        public async Task<IActionResult> Update(int CTID, UserListColumn filter)
        {
            if (filter == null || CTID != filter.TargetClassTypeID)
                return new BadRequestObjectResult("Inconsistent CTID");

            //add/update option of column/CTID key w/ body, scoped to user
            SystemListColumn newOrUpdatedColumn = await _service.AddOrUpdateColumnsAsync(CTID, User.UserLinkID().Value, filter);

            return new OkObjectResult(newOrUpdatedColumn);
        }

        /// <summary>
        /// Delete Column
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        [HttpDelete]
        public async Task<IActionResult> Delete(int CTID)
        {
            //DELETE all options of column/CTID key
            bool result = await _service.DeleteUserColumnsAsync(CTID, User.UserLinkID().Value);

            if (result)
                return new NoContentResult();
            else
                return new BadRequestResult();
        }
    }
}
