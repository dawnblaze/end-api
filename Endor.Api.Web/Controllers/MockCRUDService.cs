﻿using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Base Mock CRUD Service
    /// </summary>
    /// <typeparam name="M">Model type</typeparam>
    /// <typeparam name="I">ID type</typeparam>
    public class MockCRUDService<M, I>
        where M : class, IAtom<I>
        where I : struct, IConvertible
    {
        private List<M> entities;

        /// <summary>
        /// Gets the Mock CRUD Service
        /// </summary>
        /// <param name="modelList">List of mock Model data</param>
        public MockCRUDService(List<M> modelList)
        {
            this.entities = modelList;
        }

        internal async Task<IActionResult> DoRead()
        {
            return new OkObjectResult(await Task.FromResult(entities));
        }

        internal async Task<IActionResult> DoRead(int ID)
        {
            if (ID < 0)
                return new NotFoundResult();

            M model = entities.FirstOrDefault(e => Convert.ToInt32(e.ID) == ID);

            if (model == null)
                model = entities.FirstOrDefault();

            return new OkObjectResult(await Task.FromResult(model));
        }

        internal async Task<IActionResult> DoUpdate(int ID, M update)
        {
            if (ID < 0)
                return new NotFoundResult();

            M toUpdate = entities.FirstOrDefault(e => Convert.ToInt32(e.ID) == ID);
            if (update == null)
            {
                return new BadRequestObjectResult("Body missing or invalid");
            }
            else
            {
                entities.Remove(toUpdate);
                update.ID = toUpdate.ID; // ensure this isn't overwritten
                entities.Add(update);
                return new OkObjectResult(await Task.FromResult(update));
            }
        }

        internal async Task<IActionResult> DoCreate(M newModel, Guid? tempGuid = null)
        {
            if (newModel == null)
            {
                return new BadRequestObjectResult("Body missing or invalid");
            }
            else
            {
                newModel.ID = (I)GetNextID(newModel.ID.GetType());
                this.entities.Add(newModel);
                return new OkObjectResult(await Task.FromResult(newModel));
            }
        }

        internal async Task<IActionResult> DoClone(int ID)
        {
            if (ID < 0)
                return new NotFoundResult();

            M entity = entities.FirstOrDefault(e => Convert.ToInt32(e.ID) == ID);
            if (entity == null)
                entity = entities.FirstOrDefault();

            entities.Add(entity);
            return new OkObjectResult(await Task.FromResult(entities));
        }

        internal async Task<IActionResult> DoDelete(int ID)
        {
            if (ID < 0)
                return new NotFoundResult();

            M toDelete = entities.FirstOrDefault(e => Convert.ToInt32(e.ID) == ID);

            if (toDelete == null)
                toDelete = entities.FirstOrDefault();

            await Task.FromResult(entities.Remove(toDelete));
            return new NoContentResult();
        }

        #region Private Helpers
        
        /// <summary>
        /// Gets the next id in the series
        /// </summary>
        /// <param name="idType">ID type</param>
        /// <returns></returns>
        internal IConvertible GetNextID(Type idType)
        {
            switch (Type.GetTypeCode(idType))
            {
                case TypeCode.Byte:
                    return (I)GetNextID(entities.Max(e => Convert.ToByte(e.ID)));
                case TypeCode.Int16:
                    return (I)GetNextID(entities.Max(e => Convert.ToInt16(e.ID)));
                case TypeCode.Int32:
                    return (I)GetNextID(entities.Max(e => Convert.ToInt32(e.ID)));
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Gets the next ID in the series
        /// </summary>
        /// <param name="currentMaxID">Current Max ID as a byte</param>
        /// <returns></returns>
        private IConvertible GetNextID(byte currentMaxID)
        {
            return ++currentMaxID;
        }

        /// <summary>
        /// Gets the next ID in the series
        /// </summary>
        /// <param name="currentMaxID">Current Max ID as a short</param>
        /// <returns></returns>
        private IConvertible GetNextID(short currentMaxID)
        {
            return ++currentMaxID;
        }

        /// <summary>
        /// Gets the next ID in the series
        /// </summary>
        /// <param name="currentMaxID">Current Max ID as an int</param>
        /// <returns></returns>
        private IConvertible GetNextID(int currentMaxID)
        {
            return ++currentMaxID;
        }
        
        #endregion
    }
}
