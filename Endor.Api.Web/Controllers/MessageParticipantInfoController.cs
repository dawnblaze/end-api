﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// MessageParticipantInfoController
    /// </summary>
    /// 
    [Route("api/messageparticipantinfo")]
    [Authorize]
    public class MessageParticipantInfoController : CRUDController<MessageParticipantInfo, MessageParticipantInfoService, int>
    {
        /// <inheritdoc />
        /// <summary>
        /// constructor
        /// </summary>
        public MessageParticipantInfoController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }
    }
}
