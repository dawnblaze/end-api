﻿using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Pricing;
using Endor.RTM;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Net;
using System.Threading.Tasks;
using Endor.Security;
using Endor.Api.Web.Annotation;
using Endor.Tenant;
using Microsoft.Extensions.Caching.Memory;
using Endor.Tasks;
using Endor.Models;
using Endor.Api.Web.Classes;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for Destinations
    /// </summary>
    [Authorize]
    [Route("api/[Controller]")]
    public class DestinationController : CRUDController<DestinationData, DestinationService, short>, ISimpleListableViewController<SimpleDestinationData, short>
    {
        /// <summary>
        /// Listable service for Destinations
        /// </summary>
        public ISimpleListableViewService<SimpleDestinationData, short> ListableService => this._service;

        private readonly Lazy<PricingService> _pricingService;

        /// <summary>
        /// Api Endpoint for Destinations
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="memoryCache">Memory Cache</param>
        /// <param name="pricingEngine">Pricing Engine</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DestinationController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMemoryCache memoryCache, IPricingEngine pricingEngine, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
            _pricingService = new Lazy<PricingService>(() => new PricingService(context, logger, User.BID().Value, rtmClient, pricingEngine, migrationHelper, cache, memoryCache, taskQueuer));
        }

        /// <summary>
        /// Returns all of the Destinations for the given filters
        /// </summary>
        /// <param name="IsActive">Destination IsActive filter</param>
        /// <returns></returns>
        [HttpGet()]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DestinationData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilter([FromQuery] bool? IsActive)
        {
            if (IsActive.HasValue)
                return await base.FilteredReadWith((dd => dd.IsActive == IsActive.Value), null);
            else
                return await base.Read();
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the Destinations
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DestinationData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Read()
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns a single Destination by ID
        /// </summary>
        /// <param name="ID">Destination ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DestinationData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Destination does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single Destination by ID
        /// </summary>
        /// <param name="ID">Destination ID</param>
        /// <param name="update">Updated Destination data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DestinationData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Destination does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Destination does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] DestinationData update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new Destination
        /// </summary>
        /// <param name="newModel">New Destination data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DestinationData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Destination creation fails")]
        public override async Task<IActionResult> Create([FromBody] DestinationData newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing Destination to a new Destination
        /// </summary>
        /// <param name="ID">ID of the Destination to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DestinationData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Destination does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Destination does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes a Destination by ID
        /// </summary>
        /// <param name="ID">ID of the Destination to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Destination does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Destination does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            return await base.Delete(ID);
        }

        #endregion

        /// <summary>
        /// Gets a Simple list of Destinations
        /// </summary>
        /// <returns></returns>
        [Route("SimpleList")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleDestinationData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleDestinationData[]> GetFilteredSimpleList([FromQuery] SimpleDestinationFilters filters)
        {
            return await this.GetSimpleList<SimpleDestinationData, short>(User.BID().Value, filters);
        }

        /// <summary>
        /// Gets a list of Destinations for a given Destination Template
        /// </summary>
        /// <param name="TemplateID">Template's ID</param>
        /// <param name="IncludeInactive">If Inactive Destinations should be included</param>
        /// <returns></returns>
        [Route("List/Template/{TemplateID}")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleDestinationData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<DestinationData[]> GetDestinationsForTemplate(int TemplateID, [FromQuery] bool IncludeInactive = false)
        {
            return await this._service.GetDestinationsForTemplate(TemplateID, IncludeInactive);
        }

        /// <summary>
        /// Gets a Simple list of Destinations
        /// </summary>
        /// <returns></returns>
        [Route("ObsoleteSimpleList")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleDestinationData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleDestinationData[]> SimpleList()
        {
            return await this.GetSimpleList<SimpleDestinationData, short>(User.BID().Value);
        }

        #region Destination Actions

        /// <summary>
        /// Sets a Destination to Active
        /// </summary>
        /// <param name="ID">Destination Id</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetActive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Destination does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Destination does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Destination to Inactive
        /// </summary>
        /// <param name="ID">Destination Id</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetInactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Destination does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Destination does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the Destination can be deleted.
        /// </summary>
        /// <param name="ID">Destination ID</param>
        /// <returns></returns>
        [Route("{ID}/Action/CanDelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Destination does not exist.")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete(ID)).ToResult();
        }

        #endregion

        #region Pricing

        /// <summary>
        /// Computes the price of a destination not already saved.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="computeTax"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>  
        [Route("compute")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DestinationPriceResult))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> Compute([FromBody] DestinationPriceRequest request, [FromQuery] bool? computeTax = null)
        {
            if (request == null)
                return BadRequest("Price request object is missing or invalid.");

            try
            {
                DestinationPriceResult result = await _pricingService.Value.ComputeDestination(request, computeTax.GetValueOrDefault(false));
                return Ok(result);
            }

            catch (Exception err)
            {
                return StatusCode(500, err);
            }
        }
        
        #endregion Pricing
    }
}
