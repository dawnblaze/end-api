﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Api.Web.Includes;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Route ends up as api/dashboard/
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public partial class DashboardController : CRUDController<DashboardData, DashboardService, short>
    {

        private static string DuplicateEntryError = "Duplicate Entry: A dashboard already exists for this user and module combination";
        /// <summary>
        /// Dashboard controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DashboardController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns the Dashboards for the given filters
        /// </summary>
        /// <param name="filters">Dashboard Filters</param>
        /// <param name="includes">Dashboard Includes</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetDashboardsWithFilter(DashboardFilters filters, DashboardIncludes includes)
        {
            return new OkObjectResult(await this._service.GetWithFilters(filters, includes, User.UserLinkID()));
        }

        /// <summary>
        /// Returns a Dashboard by its ID
        /// </summary>
        /// <param name="ID">Dashboard ID</param>
        /// <param name="includes">Dashboard Includes</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetDashboardsWithFilter(short ID, DashboardIncludes includes)
        {
            var resp = await this._service.GetByID(ID, includes);
            if (resp == null)
                return new NotFoundResult();
            else
                return new OkObjectResult(resp);
        }

        /// <summary>
        /// Gets a simple list of Dashboards for the given filters
        /// </summary>
        /// <param name="filters">Simple Dashboard Filters</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>  
        [HttpGet("SimpleList")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleListItem<short>[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> SimpleList(SimpleDashboardFilters filters)
        {
            return new OkObjectResult(await _service.GetSimpleList(filters, User.UserLinkID()));
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the Dashboards
        /// </summary>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>  
        [HttpGet("obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> Read()
        {
            return await base.Read();
        }
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        /// <summary>
        /// Returns a single Dashboard by ID
        /// </summary>
        /// <param name="ID">Dashboard ID</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">If the Dashboard does not exist</response>
        /// <response code="401">If user does not have a BID or is not logged in</response>  
        [HttpGet("{ID}/obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Dashboard does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        /// <summary>
        /// Updates a single Dashboard by ID
        /// </summary>
        /// <param name="ID">Dashboard ID</param>
        /// <param name="update">Updated Dashboard data model</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">If the Dashboard does not exist</response>
        /// <response code="401">If user does not have a BID or is not logged in</response>  
        /// <response code="400">If the Dashboard does not exist or the update fails</response>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Dashboard does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Dashboard does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] DashboardData update)
        {
            var result = await base.Update(ID, update);
            if (this.ModelState.ErrorCount > 0)
            {
                if (this.ModelState.Values.SelectMany(t => t.Errors).Select(t => t.ErrorMessage).Where(m => m.Contains(DuplicateEntryError)).Any())
                    return this.StatusCode(409, DuplicateEntryError);

            }
            return result;
        }

        /// <summary>
        /// Creates a new Dashboard
        /// </summary>
        /// <param name="newModel">New Dashboard data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>  
        /// <response code="400">If the Dashboard creation fails</response>
        /// <response code="409">A dashboard already exists for this user and module combination</response>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Dashboard creation fails")]
        [SwaggerCustomResponse((int)HttpStatusCode.Conflict, description: "A dashboard already exists for this user and module combination")]
        public override async Task<IActionResult> Create([FromBody] DashboardData newModel, [FromQuery] Guid? tempID = null)
        {
            var result = await base.Create(newModel, tempID);
            if (this.ModelState.ErrorCount>0)
            {
                if (this.ModelState.Values.SelectMany(t => t.Errors).Select(t => t.ErrorMessage).Where(m => m.Contains(DuplicateEntryError)).Any())
                    return this.StatusCode(409, DuplicateEntryError) ;

            }
            return result;
        }

        /// <summary>
        /// Clones an existing Dashboard to a new Dashboard
        /// </summary>
        /// <param name="ID">ID of the Dashboard to clone from</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">If the Dashboard does not exist</response>
        /// <response code="401">If user does not have a BID or is not logged in</response>  
        /// <response code="400">If the source Dashboard does not exist or the cloning fails</response>
        [HttpPost("{ID}/clone/obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Dashboard does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Dashboard does not exist or the cloning fails.")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        /// <summary>
        /// Deletes a Dashboard by ID
        /// </summary>
        /// <param name="ID">ID of the Dashboard to Delete</param>
        /// <returns></returns>
        /// <response code="204"></response>
        /// <response code="404">If the Dashboard does not exist</response>
        /// <response code="401">If user does not have a BID or is not logged in</response>  
        /// <response code="400">If the Dashboard does not exist or the deletion fails.</response>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Dashboard does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Dashboard does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            return await base.Delete(ID);
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            //check whether a different model exists for that user and module
            if (this._service.CheckExistingDashboardConflict((DashboardData)model))
                this.ModelState.AddModelError("Conflict", DuplicateEntryError);
            return base.TryValidateModel(model, prefix);
        }
    }
}
