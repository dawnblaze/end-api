﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Endor.Api.Web.Annotation;
using Endor.Api.Web.Includes;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// EmailAccountData Controller
    /// </summary>\
    [Route("api/emailaccount")]
    [Authorize]
    public class EmailAccountController : CRUDController<EmailAccountData, EmailAccountService, short>, ISimpleListableViewController<SimpleEmailAccountData, short>
    {
        private EndorOptions _options;
        private readonly IEmailProvider _emailProvider;
        /// <summary>
        /// EmailAccountData Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="options">Endor Options</param>
        /// <param name="migrationHelper"></param>
        /// <param name="emailProvider"></param>
        public EmailAccountController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, EndorOptions options, IMigrationHelper migrationHelper, IEmailProvider emailProvider)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
            this._options = options;
            this._emailProvider = emailProvider;
        }

        /// <summary>
        /// Returns all of the EmailAccountData
        /// in simple list view
        /// </summary>
        /// <returns></returns>
        public ISimpleListableViewService<SimpleEmailAccountData, short> ListableService => this._service;

        /// <summary>
        /// Obsolete Read Method
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsolete")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Get a list of EmailAccountData by location
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <param name="locationID"></param>
        /// <returns>Returns an array of EmailAccountData</returns>
        [HttpGet("location/{locationID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFiltersForLocation(short locationID, [FromQuery] EmailAccountFilter filters)
        {
            return new OkObjectResult(await this._service.GetEmailAccountsForLocation(locationID,filters));
        }

        /// <summary>
        /// Get a list of EmailAccountData by employee
        /// </summary>
        /// <param name="employeeID">The ID of the employee</param>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <param name="IncludeShared"></param>
        /// <param name="orderid"></param>
        /// <param name="estimateid"></param>
        /// <param name="companyid"></param>
        /// <returns>Returns an array of EmailAccountData</returns>
        [HttpGet("employee/{employeeID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFiltersForEmployee(short employeeID, [FromQuery] EmailAccountFilter filters, [FromQuery] bool IncludeShared = true, [FromQuery] int? orderid = null, [FromQuery] int? estimateid = null, [FromQuery] int? companyid = null)
        {
            return new OkObjectResult(await this._service.GetEmailAccountsForEmployee(employeeID,IncludeShared,orderid,estimateid,companyid,filters));
        }

        /// <summary>
        /// Returns simple list of domain email
        /// optionally filtered by IsActive query parameter
        /// </summary>
        /// <param name="employeeID">The ID of the employee</param>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <param name="IncludeShared"></param>
        /// <param name="orderid"></param>
        /// <param name="estimateid"></param>
        /// <param name="companyid"></param>
        /// <returns></returns>
        [HttpGet("employee/{employeeID}/simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleEmailAccountData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If invalid filters are specified")]
        public async Task<IActionResult> SimpleListByEmployee(short employeeID, [FromQuery]EmailAccountFilter filters, [FromQuery] bool IncludeShared = true, [FromQuery] int? orderid = null, [FromQuery] int? estimateid = null, [FromQuery] int? companyid = null)
        {

            var results = await this._service.GetEmailAccountsForEmployee(employeeID, IncludeShared, orderid, estimateid, companyid, filters);
            return new OkObjectResult(MapToSimpleList(results));
        }

        /// <summary>
        /// Get the default email account for an employee
        /// </summary>
        /// <param name="orderid"></param>
        /// <param name="estimateid"></param>
        /// <param name="companyid"></param>
        /// <returns>Returns an array of EmailAccountData</returns>
        [HttpGet("default")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadDefaultWithFiltersForEmployee([FromQuery] int? orderid = null, [FromQuery] int? estimateid = null, [FromQuery] int? companyid = null)
        {
            if (!User.EmployeeID().HasValue) return new UnauthorizedResult();
            return new OkObjectResult(await this._service.GetDefaultEmailAccountForEmployee(User.EmployeeID().GetValueOrDefault(), orderid, estimateid, companyid));
        }

        /// <summary>
        /// Get the default email account for an employee
        /// </summary>
        /// <param name="employeeID"></param>
        /// <param name="orderid"></param>
        /// <param name="estimateid"></param>
        /// <param name="companyid"></param>
        /// <returns>Returns an array of EmailAccountData</returns>
        [HttpGet("employee/{employeeID}/default")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadDefaultWithFiltersForEmployee(short employeeID, [FromQuery] int? orderid = null, [FromQuery] int? estimateid = null, [FromQuery] int? companyid = null)
        {
            if (!User.EmployeeID().HasValue) return new UnauthorizedResult();
            return new OkObjectResult(await this._service.GetDefaultEmailAccountForEmployee(employeeID, orderid, estimateid, companyid));
        }


        /// <summary>
        /// Creates a new EmailAccountData
        /// </summary>
        /// <param name="newModel">The Email Account model to create</param>
        /// <param name="tempID">A temporary ID used when creating objects with document storage</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountData), "Created a new email account")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid parameters were supplied or there was a different issue with the request")]
        public override async Task<IActionResult> Create([FromBody] EmailAccountData newModel, [FromQuery] Guid? tempID = null)
        {
            this._service.EmailProvider = this._emailProvider;

            if ((!string.IsNullOrWhiteSpace(newModel.Credentials)) && (!string.IsNullOrWhiteSpace(this._options.EncryptionPassPhrase)))
            {
                ValidationResult validationResult = await _service.ValidateAsync(newModel, this._options.EncryptionPassPhrase);
                _service.UpdateStatus(newModel, validationResult.ValidToken);
            }else
            {
                _service.UpdateStatus(newModel, null);
            }

            ValidationDomainResult validateDomain = await _service.ValidateDomainAsync(newModel);
            if (validateDomain.IsValid)
            {
                return await base.Create(newModel, tempID);
            }

            return base.BadRequest(validateDomain.Message);
        }

        /// <summary>
        /// Get a list of EmailAccountData based on supplied criteria
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <param name="includes"></param>
        /// <returns>Returns an array of EmailAccountData</returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] EmailAccountFilter filters, EmailAccountIncludes includes = null)
        {
            return new OkObjectResult(await this._service.GetWithFiltersAsync(filters, includes));
        }

        /// <summary>
        /// Gets a single EmailAccountData by ID
        /// </summary>
        /// <param name="ID">The ID of the EmailAccountData to be retrieved</param>
        /// <param name="includes">Navigation Properties to include data for</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountData), "Successfully retrieved the EmailAccountData")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "EmailAccountData was not found")]
        public async Task<IActionResult> ReadByIdWithIncludes(short ID, [FromQuery] EmailAccountIncludes includes = null)
        {
            var resp = await _service.GetAsync(ID, includes);
            if (resp == null)
                return NotFound();
            else
                return new OkObjectResult(resp);
        }

        /// <summary>
        /// Gets a single EmailAccountData by ID
        /// </summary>
        /// <param name="ID">The ID of the EmailAccountData to be retrieved</param>
        /// <returns></returns>
        [HttpGet("obsolete/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountData), "Successfully retrieved the EmailAccountData")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "EmailAccountData was not found")]
        public override Task<IActionResult> ReadById(short ID)
        {
            return base.ReadById(ID);
        }

        /// <summary>
        /// Updates a EmailAccountData
        /// </summary>
        /// <param name="ID">The Id of the EmailAccountData to update</param>
        /// <param name="update">Contains the updated EmailAccountData model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmailAccountData), "Successfully updated the EmailAccountData")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid request properties")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "EmailAccountData was not found")]
        public override async Task<IActionResult> Update(short ID, [FromBody] EmailAccountData update)
        {
            if (!string.IsNullOrWhiteSpace(update.Credentials))
            {
                this._service.EmailProvider = this._emailProvider;
                ValidationResult validationResult = await this._service.ValidateAsync(update, this._options.EncryptionPassPhrase);
                _service.UpdateStatus(update, validationResult.ValidToken);
            }

            return await base.Update(ID, update);
        }

        /// <summary>
        /// Deletes a EmailAccountData
        /// </summary>
        /// <param name="ID">The ID of the EmailAccountData to be deleted</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "EmailAccountData is no longer found")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Delete(short ID)
        {
            BooleanResponse resp = await this._service.CanDelete((short)ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                return await base.Delete(ID);
            }
            else
            {
                return new BadRequestObjectResult(resp);
            }
        }

        /// <summary>
        /// Sets a EmailAccountData to Active
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the EmailAccountData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the EmailAccountData does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            
            return resp.ToResult();
        }

        /// <summary>
        /// Sets an EmailAccountData to Inactive
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the EmailAccountData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the EmailAccountData does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Clears this default
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <param name="employeeID">=</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/cleardefault")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the EmailAccountData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Forbidden, typeof(EntityActionChangeResponse<short>), description: "Email Account is currently inactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the EmailAccountData does not exist or fails to set as active.")]
        public async Task<IActionResult> ClearDefaultForEmployee(short ID, short employeeID)
        {
            var emailAccount = await this._service.GetAsync(ID);
            if (emailAccount == null)
            {
                return NotFound();
            }
            var result = await this._service.SetDefault(emailAccount, employeeID, ClassType.Employee);
            if (!result)
            {
                return new ForbidResult();
            }
            else
            {
                return new OkResult();
            }
        }

        /// <summary>
        /// Returns true | false based on whether the EmailAccountData can be deleted.
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the EmailAccountData does not exist.")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Tests a EmailAccountData
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/test")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the EmailAccountData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the EmailAccountData fails to test.")]
        public async Task<IActionResult> Test(short ID)
        {
            this._service.EmailProvider = this._emailProvider;
            return (await this._service.Test(ID, this._options.EncryptionPassPhrase)).ToResult();
        }

        /// <summary>
        /// Links an EmailAccount with an EmployeeTeam
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <param name="TeamID">EmployeeTeam ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/Action/LinkTeam/{TeamID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the EmailAccountData or the EmployeeTeam does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the EmailAccountData fails to link with the EmployeeTeam.")]
        public async Task<IActionResult> LinkTeam(short ID, short TeamID)
        {
            return (await _service.LinkTeam(ID, TeamID, true)).ToResult();
        }

        /// <summary>
        /// Unlinks an EmailAccount from an EmployeeTeam
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <param name="TeamID">EmployeeTeam ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/Action/UnlinkTeam/{TeamID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the EmailAccountData or the EmployeeTeam does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the EmailAccountData fails to unlink from the EmployeeTeam.")]
        public async Task<IActionResult> UnlinkTeam(short ID, short TeamID)
        {
            return (await _service.LinkTeam(ID, TeamID, false)).ToResult();
        }

        /// <summary>
        /// obselete-simplelist
        /// </summary>
        /// <returns></returns>
        [HttpGet("obselete-simplelist")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleEmailAccountData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleEmailAccountData[]> SimpleList()
        {
            return await this.GetSimpleList<SimpleEmailAccountData, short>(User.BID().Value);
        }

        /// <summary>
        /// Returns simple list of domain email
        /// optionally filtered by IsActive query parameter
        /// </summary>
        /// <param name="locationid"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        [HttpGet("location/{locationid}/simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleEmailAccountData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If invalid filters are specified")]
        public async Task<IActionResult> SimpleListByLocation(short locationid, [FromQuery]EmailAccountFilter filters)
        {
            var results = await this._service.GetEmailAccountsForLocation(locationid, filters);
            return new OkObjectResult( MapToSimpleList(results));
        }
        
        private SimpleEmailAccountData[] MapToSimpleList(List<EmailAccountData> results) {
            return results.Select(a => new SimpleEmailAccountData
            {
                ID = a.ID,
                ClassTypeID = a.ClassTypeID,
                DisplayName = a.DisplayName,
                EmployeeID = a.EmployeeID,
                IsActive = a.IsActive
            }).ToArray();           
        }

        /// <summary>
        /// Returns simple list of domain email
        /// optionally filtered by IsActive query parameter
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <param name="IncludeShared"></param>
        /// <param name="orderid"></param>
        /// <param name="estimateid"></param>
        /// <param name="companyid"></param>
        /// <returns></returns>
        [HttpGet("simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleEmailAccountData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If invalid filters are specified")]
        public async Task<IActionResult> SimpleListByEmployee([FromQuery]EmailAccountFilter filters, [FromQuery] bool IncludeShared = true, [FromQuery] int? orderid = null, [FromQuery] int? estimateid = null, [FromQuery] int? companyid = null)
        {
            if (!User.EmployeeID().HasValue) return new UnauthorizedResult();
            var results = await this._service.GetEmailAccountsForEmployee(User.EmployeeID().Value, IncludeShared, orderid, estimateid, companyid, filters);
            return new OkObjectResult(MapToSimpleList(results));
        }


    }
}
