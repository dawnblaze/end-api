﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Responses;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// TimeCardDetail Controller
    /// </summary>
    [Route("API/TimeCardDetail")]
    public class TimeCardDetailController : CRUDController<TimeCardDetail, TimeCardDetailService, short>
    {
        /// <summary>
        /// TimeCardDetailController Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public TimeCardDetailController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Obsolete Read Method
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsolete")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Creates a new TimeCardDetail
        /// </summary>
        /// <param name="newModel">The TimeCardDetail model to create</param>
        /// <param name="tempID">A temporary ID used when creating objects with document storage</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TimeCardDetail), "Created a new TimeCard detail")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid parameters were supplied or there was a different issue with the request")]
        public override Task<IActionResult> Create([FromBody] TimeCardDetail newModel, [FromQuery] Guid? tempID = null)
        {
            return base.Create(newModel, tempID);
        }

        /// <summary>
        /// Get a list of TimeCardDetail based on supplied criteria
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <returns>Returns an array of TimeCardDetail</returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TimeCardDetail[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] TimeCardDetailFilter filters)
        {
            return new OkObjectResult(await this._service.GetWithFiltersAsync(filters));
        }

        /// <summary>
        /// Gets a single TimeCardDetail by ID
        /// </summary>
        /// <param name="ID">The ID of the TimeCardDetail to be retrieved</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TimeCardDetail), "Successfully retrieved the TimeCard Detail")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "TimeCard was not found")]
        public override Task<IActionResult> ReadById(short ID)
        {
            return base.ReadById(ID);
        }

        /// <summary>
        /// Updates a TimeCardDetail
        /// </summary>
        /// <param name="ID">The Id of the TimeCardDetail to update</param>
        /// <param name="update">Contains the updated TimeCardDetail model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TimeCardDetail), "Successfully updated the TimeCard Detail")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid request properties")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "TimeCard Detail was not found")]
        public override Task<IActionResult> Update(short ID, [FromBody] TimeCardDetail update)
        {
            return base.Update(ID, update);
        }

        /// <summary>
        /// Deletes a TimeCardDetail
        /// </summary>
        /// <param name="ID">The ID of the TimeCardDetail to be deleted</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "TimeCard Detail is no longer found")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Delete(short ID)
        {
            return await base.Delete(ID);
        }
        
        /// <summary>
        /// Returns a simplified JSON array of valid TimeCard Detail Statuses and Activities.  These are made up of Break Statuses, Time Card Activities, and Order Item Statuses
        /// </summary>
        /// <returns></returns>
        [HttpGet("statuses")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TimeCardDetailStatus[]), "Successfully retrieved the TimeCard Statuses")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadStatusesAndActivities()
        {
            return Ok(await this._service.GetStatusesAndActivities());
        }
    }
}
