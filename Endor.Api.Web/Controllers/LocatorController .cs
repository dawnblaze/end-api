﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// abstract class for all locator controllers
    /// </summary>
    /// <typeparam name="L"></typeparam>
    /// <typeparam name="S"></typeparam>
    /// <typeparam name="LPI"></typeparam>
    /// <typeparam name="LP"></typeparam>
    public abstract class BaseLocatorController<L, S, LPI, LP> : CRUDController<L, S, int>
        where L : BaseLocator<LPI, LP>
        where S : BaseLocatorService<L, LPI, LP>
        where LPI : struct, IConvertible
        where LP : class, IAtom<LPI>
    {
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public BaseLocatorController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// returns a model by parentID
        /// returns OK or Not Found
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        [HttpGet("parent/{parentID}")]
        public virtual async Task<IActionResult> GetFromParent(LPI parentID)
        {
            List<L> models = await _service.GetForParent(parentID);

            if (models == null)
            {
                return new NotFoundResult();
            }

            return new OkObjectResult(models);
        }

        /// <summary>
        /// Updates a collection of locators
        /// </summary>
        /// <param name="locators">Locator Collection</param>
        /// <returns></returns>
        [HttpPut("updateall")]
        public async Task<IActionResult> UpdateAll([FromBody]L[] locators)
        {
            foreach (var locator in locators)
            {
                await base.Update(locator.ID, locator);
            }

            return new OkObjectResult(locators);
        }
    }

    /// <summary>
    /// The controller for employee locators
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public class EmployeeLocatorController : BaseLocatorController<EmployeeLocator, EmployeeLocatorService, short, EmployeeData>
    {
        /// <summary>
        /// employee locator ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public EmployeeLocatorController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the EmployeeLocators
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeLocator[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single EmployeeLocator by ID
        /// </summary>
        /// <param name="ID">EmployeeLocator ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the EmployeeLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single EmployeeLocator by ID
        /// </summary>
        /// <param name="ID">EmployeeLocator ID</param>
        /// <param name="update">Updated EmployeeLocator data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the EmployeeLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the EmployeeLocator does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] EmployeeLocator update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new EmployeeLocator
        /// </summary>
        /// <param name="newModel">New EmployeeLocator data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the EmployeeLocator creation fails")]
        public override async Task<IActionResult> Create([FromBody] EmployeeLocator newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing EmployeeLocator to a new EmployeeLocator
        /// </summary>
        /// <param name="ID">ID of the EmployeeLocator to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source EmployeeLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source EmployeeLocator does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes an EmployeeLocator by ID
        /// </summary>
        /// <param name="ID">ID of the EmployeeLocator to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the EmployeeLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the EmployeeLocator does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }

        /// <summary>
        /// Gets the locator given the employee parent ID
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        [HttpGet("parent/{parentID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeLocator[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the EmployeeLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> GetFromParent(short parentID)
        {
            return await base.GetFromParent(parentID);
        }
        #endregion

    }

    /// <summary>
    /// contact locator controller
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public class ContactLocatorController : BaseLocatorController<ContactLocator, ContactLocatorService, int, ContactData>
    {
        /// <summary>
        /// contact locator ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public ContactLocatorController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the ContactLocators
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ContactLocator[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single ContactLocator by ID
        /// </summary>
        /// <param name="ID">ContactLocator ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ContactLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the ContactLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single ContactLocator by ID
        /// </summary>
        /// <param name="ID">ContactLocator ID</param>
        /// <param name="update">Updated ContactLocator data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ContactLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the ContactLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the ContactLocator does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] ContactLocator update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new ContactLocator
        /// </summary>
        /// <param name="newModel">New ContactLocator data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ContactLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the ContactLocator creation fails")]
        public override async Task<IActionResult> Create([FromBody] ContactLocator newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing ContactLocator to a new ContactLocator
        /// </summary>
        /// <param name="ID">ID of the ContactLocator to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ContactLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source ContactLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source ContactLocator does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes an ContactLocator by ID
        /// </summary>
        /// <param name="ID">ID of the ContactLocator to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the ContactLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the ContactLocator does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }

        /// <summary>
        /// Gets the locator given the contact parent ID
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        [HttpGet("parent/{parentID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ContactLocator[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the ContactLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> GetFromParent(int parentID)
        {
            return await base.GetFromParent(parentID);
        }
        #endregion

    }

    /// <summary>
    /// company locator controller
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public class CompanyLocatorController : BaseLocatorController<CompanyLocator, CompanyLocatorService, int, CompanyData>
    {
        /// <summary>
        /// company locator ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CompanyLocatorController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the Company Locators
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CompanyLocator[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single CompanyLocator by ID
        /// </summary>
        /// <param name="ID">CompanyLocator ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CompanyLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the CompanyLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single CompanyLocator by ID
        /// </summary>
        /// <param name="ID">CompanyLocator ID</param>
        /// <param name="update">Updated CompanyLocator data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CompanyLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the CompanyLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the CompanyLocator does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] CompanyLocator update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new CompanyLocator
        /// </summary>
        /// <param name="newModel">New CompanyLocator data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CompanyLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the CompanyLocator creation fails")]
        public override async Task<IActionResult> Create([FromBody] CompanyLocator newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing CompanyLocator to a new CompanyLocator
        /// </summary>
        /// <param name="ID">ID of the CompanyLocator to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CompanyLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source CompanyLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source CompanyLocator does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes an CompanyLocator by ID
        /// </summary>
        /// <param name="ID">ID of the CompanyLocator to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the CompanyLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the CompanyLocator does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }

        /// <summary>
        /// Gets the locator given the company parent ID
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        [HttpGet("parent/{parentID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CompanyLocator[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the CompanyLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> GetFromParent(int parentID)
        {
            return await base.GetFromParent(parentID);
        }
        #endregion
    }

    /// <summary>
    /// business locator controller
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public class BusinessLocatorController : BaseLocatorController<BusinessLocator, BusinessLocatorService, short, BusinessData>
    {
        /// <summary>
        /// business locator ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public BusinessLocatorController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the BusinessLocators
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BusinessLocator[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single BusinessLocator by ID
        /// </summary>
        /// <param name="ID">BusinessLocator ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BusinessLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the BusinessLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single BusinessLocator by ID
        /// </summary>
        /// <param name="ID">BusinessLocator ID</param>
        /// <param name="update">Updated BusinessLocator data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BusinessLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the BusinessLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the BusinessLocator does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] BusinessLocator update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new BusinessLocator
        /// </summary>
        /// <param name="newModel">New BusinessLocator data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BusinessLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the BusinessLocator creation fails")]
        public override async Task<IActionResult> Create([FromBody] BusinessLocator newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing BusinessLocator to a new BusinessLocator
        /// </summary>
        /// <param name="ID">ID of the BusinessLocator to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BusinessLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source BusinessLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source BusinessLocator does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes an BusinessLocator by ID
        /// </summary>
        /// <param name="ID">ID of the BusinessLocator to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the BusinessLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the BusinessLocator does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }

        /// <summary>
        /// Gets the locator given the business parent ID
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        [HttpGet("parent/{parentID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BusinessLocator[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the BusinessLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> GetFromParent(short parentID)
        {
            return await base.GetFromParent(parentID);
        }
        #endregion

    }

    /// <summary>
    /// location locator controller
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public class LocationLocatorController : BaseLocatorController<LocationLocator, LocationLocatorService, byte, LocationData>
    {
        /// <summary>
        /// location locator ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public LocationLocatorController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the LocationLocators
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationLocator[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single LocationLocator by ID
        /// </summary>
        /// <param name="ID">LocationLocator ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the LocationLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single LocationLocator by ID
        /// </summary>
        /// <param name="ID">LocationLocator ID</param>
        /// <param name="update">Updated LocationLocator data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the LocationLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the LocationLocator does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] LocationLocator update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new LocationLocator
        /// </summary>
        /// <param name="newModel">New LocationLocator data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the LocationLocator creation fails")]
        public override async Task<IActionResult> Create([FromBody] LocationLocator newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing LocationLocator to a new LocationLocator
        /// </summary>
        /// <param name="ID">ID of the LocationLocator to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source LocationLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source LocationLocator does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes an LocationLocator by ID
        /// </summary>
        /// <param name="ID">ID of the LocationLocator to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the LocationLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the LocationLocator does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }

        /// <summary>
        /// Gets the locator given the Locator parent ID
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        [HttpGet("parent/{parentID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationLocator[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the LocationLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> GetFromParent(byte parentID)
        {
            return await base.GetFromParent(parentID);
        }
        #endregion

    }

    /// <summary>
    /// order contact role  locator  controller
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public class OrderContactRoleLocatorController : BaseLocatorController<OrderContactRoleLocator, OrderContactRoleLocatorService, int, OrderContactRole>
    {
        /// <summary>
        /// order contact role locator ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OrderContactRoleLocatorController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the OrderContactRoleLocators
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRoleLocator[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Gets the OrderContactRoleLocators given the OrderContactRoleLocator parent ID
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        [HttpGet("parent/{parentID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRoleLocator[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderContactRoleLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> GetFromParent(int parentID)
        {
            return await base.GetFromParent(parentID);
        }

        /// <summary>
        /// Returns a single OrderContactRoleLocator by ID
        /// </summary>
        /// <param name="ID">OrderContactRoleLocator ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRoleLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderContactRoleLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single OrderContactRoleLocator by ID
        /// </summary>
        /// <param name="ID">OrderContactRoleLocator ID</param>
        /// <param name="update">Updated OrderContactRoleLocator data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRoleLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderContactRoleLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderContactRoleLocator does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] OrderContactRoleLocator update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Deletes an OrderContactRoleLocator by ID
        /// </summary>
        /// <param name="ID">ID of the OrderContactRoleLocator to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderContactRoleLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderContactRoleLocator does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }

        /// <summary>
        /// Creates a new OrderContactRoleLocator
        /// </summary>
        /// <param name="newModel">New OrderContactRoleLocator data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRoleLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderContactRoleLocator creation fails")]
        public override async Task<IActionResult> Create([FromBody] OrderContactRoleLocator newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        #region Obsolete/Not Implemented Overridden CRUD Methods

        /// <summary>
        /// Clones an existing OrderContactRoleLocator to a new OrderContactRoleLocator
        /// </summary>
        /// <param name="ID">ID of the OrderContactRoleLocator to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRoleLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source OrderContactRoleLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source OrderContactRoleLocator does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }

        #endregion Obsolete/Not Implemented Overridden CRUD Methods

        #endregion Overridden CRUD Methods
    }
    

    /// <summary>
    /// Credit memo contact role  locator  controller
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public class CreditMemoContactRoleLocatorController : BaseLocatorController<OrderContactRoleLocator, OrderContactRoleLocatorService, int, OrderContactRole>
    {
        /// <summary>
        /// order contact role locator ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CreditMemoContactRoleLocatorController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the OrderContactRoleLocators
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRoleLocator[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Gets the OrderContactRoleLocators given the OrderContactRoleLocator parent ID
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        [HttpGet("parent/{parentID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRoleLocator[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderContactRoleLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> GetFromParent(int parentID)
        {
            return await base.GetFromParent(parentID);
        }

        /// <summary>
        /// Returns a single OrderContactRoleLocator by ID
        /// </summary>
        /// <param name="ID">OrderContactRoleLocator ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRoleLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderContactRoleLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single OrderContactRoleLocator by ID
        /// </summary>
        /// <param name="ID">OrderContactRoleLocator ID</param>
        /// <param name="update">Updated OrderContactRoleLocator data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRoleLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderContactRoleLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderContactRoleLocator does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] OrderContactRoleLocator update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Deletes an OrderContactRoleLocator by ID
        /// </summary>
        /// <param name="ID">ID of the OrderContactRoleLocator to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the OrderContactRoleLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderContactRoleLocator does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }

        /// <summary>
        /// Creates a new OrderContactRoleLocator
        /// </summary>
        /// <param name="newModel">New OrderContactRoleLocator data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRoleLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the OrderContactRoleLocator creation fails")]
        public override async Task<IActionResult> Create([FromBody] OrderContactRoleLocator newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        #region Obsolete/Not Implemented Overridden CRUD Methods

        /// <summary>
        /// Clones an existing OrderContactRoleLocator to a new OrderContactRoleLocator
        /// </summary>
        /// <param name="ID">ID of the OrderContactRoleLocator to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(OrderContactRoleLocator))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source OrderContactRoleLocator does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source OrderContactRoleLocator does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }

        #endregion Obsolete/Not Implemented Overridden CRUD Methods

        #endregion Overridden CRUD Methods
    }
}
