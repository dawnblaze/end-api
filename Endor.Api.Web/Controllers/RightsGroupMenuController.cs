﻿using Endor.Api.Web.Services;
using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// RightsGroupMenu Controller
    /// </summary>
    [Authorize]
    [Route("api/rightsgroupmenu")]
    public class RightsGroupMenuController : GenericController<RightsGroupMenuTree, RightsGroupMenuService, byte>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public RightsGroupMenuController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper) 
            : base(context, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Returns a nested JSON array of RightsGroupMenuTree Objects.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials")]
        public async Task<IActionResult> Get()
        {
            return await this._service.Get(User.BID().Value, User.UserID().Value);
        }

        /// <summary>
        /// Returns a list of Module Types that link to the Rights Group that is Enabled by this Menu Node
        /// </summary>
        /// <param name="menuID">RightsGroupMenu ID</param>
        /// <returns></returns>
        [Route("{menuID}/relatedmodules")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(BooleanResponse), description: "Specified ID does not exist")]
        public async Task<IActionResult> GetRelatedModules([FromRoute]short menuID)
        {
            return await this._service.GetRelatedModules(menuID);
        }
    }
}