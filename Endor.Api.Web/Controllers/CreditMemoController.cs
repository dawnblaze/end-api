﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Pricing;

using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes.Clones;
using Microsoft.Extensions.Caching.Memory;
using Endor.Api.Web.Classes.Responses;
using Endor.Api.Web.Classes.Request;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for Credit Memos
    /// </summary>
    [Authorize]
    [Route("api/[Controller]")]
    public class CreditMemoController : BaseTransactionHeaderController<CreditMemoData, CreditMemoService>
    {
        private readonly ApiContext _context;
        private readonly Lazy<PaymentService> _paymentService;

        /// <summary>
        /// Defines this controller as for Estimates
        /// </summary>
        protected override OrderTransactionType TransactionType => OrderTransactionType.Memo;

        /// <summary>
        /// Api Endpoint for Credit Memo
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="pricingEngine">Pricing Engine</param>
        /// <param name="migrationHelper"></param>
        public CreditMemoController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IPricingEngine pricingEngine, IMigrationHelper migrationHelper, IMemoryCache memoryCache)
            : base(context, logger, rtmClient, taskQueuer, cache, pricingEngine, migrationHelper, memoryCache)
        {
            this._context = context;
            this._paymentService = new Lazy<PaymentService>(() => new PaymentService(context, logger, this.User.BID().Value, taskQueuer, rtmClient, cache, migrationHelper));
        }

        /// <summary>
        /// Clones an existing Order to a new Order
        /// </summary>
        /// <param name="ID">ID of the Order to clone from</param>
        /// <param name="CopyFiles">Optional paramter for whether to copy files or not</param>
        /// <returns></returns>
        [Obsolete]
        [HttpPost("{ID}/clone/obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Order does not exist or the cloning fails.")]
        public override async Task<IActionResult> DoClone(int ID, [FromQuery]bool? CopyFiles = null)
        {
            return await this.DoCloneTransaction(ID, CopyFiles);
        }

        /// <summary>
        /// Updates a single Credit Memo by ID
        /// </summary>
        /// <param name="ID">Credit Memo ID</param>
        /// <param name="update">Updated Order data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TransactionHeaderData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Credit Memo does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Credit Memo does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] CreditMemoData update)
        {
            // END-8642 Enforce the option "IsPORequired" on the Company record during Order Entry.
            bool isPORequired = await _service.IsPORequired(update);
            if (isPORequired && string.IsNullOrWhiteSpace(update.OrderPONumber))
            {
                this.LogModelErrors();
                return new BadRequestObjectResult("PO Required for this Customer.");
            }

            // get the Order
            var creditmemo = await _service.GetAsync(ID);
            if (creditmemo == null)
            {
                return new NotFoundResult();
            }

            var allowedOrderTypeStatusMap = new Dictionary<OrderTransactionType, OrderOrderStatus[]>()
            {
                {
                    OrderTransactionType.Memo,
                    new OrderOrderStatus[] {
                        OrderOrderStatus.CreditMemoUnposted,
                        OrderOrderStatus.CreditMemoPosted,
                        OrderOrderStatus.CreditMemoVoided
                    }
                }
            };

            // For Orders, the old status can't be Voided (29).
            // For Estimates, the old status can't be Voided (19).
            // For PO, the old status can't be Voided (49).
            OrderOrderStatus[] voidStatuses = new OrderOrderStatus[] {
                OrderOrderStatus.CreditMemoVoided
            };

            // Validate against voided Orders / checks wether the initial status is void
            if (voidStatuses.Contains(creditmemo.OrderStatusID))
            {
                return BadRequest("Status is void");
            }

            // Check against allowed status within the Order type
            OrderOrderStatus[] allowedStatuses = allowedOrderTypeStatusMap.Where(item => item.Key == (OrderTransactionType)creditmemo.TransactionType).Select(i => i.Value).First();
            if (!allowedStatuses.Contains(update.OrderStatusID))
            {
                return BadRequest("Status is not allowed");
            }

            var oldStatuses = creditmemo.OrderStatusID;

            var result = await base.Update(ID, update);

            switch (result)
            {
                case OkObjectResult ok:
                    // NOTE: do this only when result is 200, its triggering a refresh when having an error during update (e.g. validation error)
                    if (update != null)
                    {
                        if (update.Items != null && update.Items.Count() > 0 && !(oldStatuses == OrderOrderStatus.CreditMemoUnposted && update.OrderStatusID == OrderOrderStatus.CreditMemoVoided))
                            try
                            {
                                await CreateOrUpdateCreditGiftAdjustment(update);

                                if (ModelState.Keys.Contains("Not Found"))
                                    return NotFound(ModelState);
                                if (!ModelState.IsValid)
                                    return BadRequest(ModelState);
                            }
                            catch (Exception e)
                            {
                                return new BadRequestObjectResult(e);
                            }

                        await CreateKeyDateBasedOnOrderStatus(ID, update.OrderStatusID);
                        await _taskQueuer.RecomputeGL(User.BID().Value, (byte)GLEngine.Models.EnumGLType.Order, ID, "CreditMemo Updated", "CreditMemo Updated", User.EmployeeID(), null, (byte)GLEntryType.Order_Edited);
                    }
                    break;
            }

            return result;
        }

        /// <summary>
        /// Kicks off the RecomputeGL Task on BgEngine
        /// </summary>
        /// <returns></returns>
        [Route("{ID}/action/changestatus/{creditMemoStatus}")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the order does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If credit memo status is invalid.")]
        public override async Task<IActionResult> ChangeOrderStatus(int ID, OrderOrderStatus creditMemoStatus)
        {
            // get the Order
            var creditmemo = await _service.GetAsync(ID);
            if (creditmemo == null)
            {
                return new NotFoundResult();
            }

            var allowedOrderTypeStatusMap = new Dictionary<OrderTransactionType, OrderOrderStatus[]>()
            {
                {
                    OrderTransactionType.Memo,
                    new OrderOrderStatus[] {
                        OrderOrderStatus.CreditMemoUnposted,
                        OrderOrderStatus.CreditMemoPosted,
                        OrderOrderStatus.CreditMemoVoided
                    }
                }
            };

            // For Orders, the old status can't be Voided (29).
            // For Estimates, the old status can't be Voided (19).
            // For PO, the old status can't be Voided (49).
            OrderOrderStatus[] voidStatuses = new OrderOrderStatus[] {
                OrderOrderStatus.CreditMemoVoided
            };

            // Validate against voided Orders / checks wether the initial status is void
            if (voidStatuses.Contains(creditmemo.OrderStatusID))
            {
                return BadRequest("Status is void");
            }

            // Check against allowed status within the Order type
            OrderOrderStatus[] allowedStatuses = allowedOrderTypeStatusMap.Where(item => item.Key == (OrderTransactionType)creditmemo.TransactionType).Select(i => i.Value).First();
            if (!allowedStatuses.Contains(creditMemoStatus))
            {
                return BadRequest("Status is not allowed");
            }

            var oldStatuses = creditmemo.OrderStatusID;

            // commented out - https://corebridge.atlassian.net/browse/END-12184
            // UNPOSTING paid credit memos is not allowed
            //if (oldStatuses == OrderOrderStatus.CreditMemoPosted && creditMemoStatus == OrderOrderStatus.CreditMemoUnposted)
            //{
            //    var paymentMaster = _context.PaymentMaster
            //        .AsNoTracking()
            //        .Include(t => t.PaymentApplications)
            //        .FirstOrDefault(
            //            t => t.BID == User.BID() &&
            //            t.PaymentTransactionType == (byte)PaymentTransactionType.Credit_Gift &&
            //            t.PaymentApplications.Any(ap => ap.OrderID == creditmemo.ID) );
            //    if (paymentMaster != null)
            //    {
            //        return BadRequest("We cannot unpost paid credit memos.");
            //    }
            //}

            var response = await this._service.ChangeOrderStatus(creditmemo, creditMemoStatus, Request.Headers[ConnectionIDHeaderKey]);

            if (creditmemo.Items != null && creditmemo.Items.Count() > 0 && !(oldStatuses == OrderOrderStatus.CreditMemoUnposted && creditmemo.OrderStatusID == OrderOrderStatus.CreditMemoVoided))
                try
                {
                    await CreateOrUpdateCreditGiftAdjustment(creditmemo);

                    if (ModelState.Keys.Contains("Not Found"))
                        return NotFound(ModelState);
                    if (!ModelState.IsValid)
                        return BadRequest(ModelState);
                }
                catch (Exception e)
                {
                    return new BadRequestObjectResult(e);
                }


            if (response.Success)
            {
                // call the task service
                await _taskQueuer.RecomputeGL(User.BID().Value, (byte)GLEngine.Models.EnumGLType.Order, ID, "CreditMemo Status Change", "CreditMemo Status Change", User.EmployeeID(), null, (byte)GLEntryType.Order_Status_Change);
                await CreateKeyDateBasedOnOrderStatus(creditmemo.ID, creditMemoStatus);
            }

            return Ok(response);
        }

        private async Task<PaymentResponse> CreateOrUpdateCreditGiftAdjustment(CreditMemoData creditMemo)
        {
            var paymentService = this._paymentService.Value;
            paymentService.ServiceModelState = new ServiceModelState(ModelState);

            var info = GetUserLinkAndConnection();
            var paymentMaster = _context.PaymentMaster
                .Include(t => t.PaymentApplications)
                .FirstOrDefault(
                    t => t.BID == User.BID() &&
                    t.PaymentTransactionType == (byte)PaymentTransactionType.Credit_Gift &&
                    t.PaymentApplications.Any(ap => ap.OrderID == creditMemo.ID));

            if (paymentMaster == null)
            {
                PaymentRequest paymentRequest = new PaymentRequest()
                {
                    PaymentTransactionType = (int)PaymentTransactionType.Credit_Gift,
                    Amount = creditMemo.PriceTotal ?? 0,
                    CurrencyType = null,
                    Notes = "",
                    CompanyID = creditMemo.CompanyID,
                    LocationID = creditMemo.LocationID,
                    ReceivedLocationID = creditMemo.PickupLocationID,
                    Applications = new List<PaymentApplicationRequest>()
                    {
                        new PaymentApplicationRequest()
                        {
                            Amount = creditMemo.PriceTotal ?? 0,
                            OrderID = creditMemo.ID,
                        }
                    },
                };

                return await paymentService.CreatePaymentTransactionAsync(paymentRequest, null, info.userLink, info.connectionID, false);
            }
            else
            {
                // check to see if we are voiding a CreditMemo
                if (creditMemo.OrderStatusID == OrderOrderStatus.CreditMemoVoided)
                {
                    await RefundCreditMemoPaymentsFromAMaster(paymentService, creditMemo, paymentMaster.ID);
                }

                var paymentStatuses = new byte[] { (byte)PaymentTransactionType.Credit_Gift, (byte)PaymentTransactionType.Credit_Adjustment };

                var currentAmmout = paymentMaster.PaymentApplications
                    .Where(a => paymentStatuses.Contains(a.PaymentTransactionType))
                    .Sum(a => a.Amount);

                var differenceApplication = (creditMemo.OrderStatusID != OrderOrderStatus.CreditMemoPosted) ?
                    -currentAmmout :
                    (creditMemo.PriceTotal - currentAmmout);

                if (differenceApplication != 0)
                {
                    // find and update existing one
                    PaymentRequest paymentRequest = new PaymentRequest()
                    {
                        PaymentTransactionType = (int)PaymentTransactionType.Credit_Adjustment,
                        Amount = differenceApplication.Value,
                        CurrencyType = null,
                        Notes = "",
                        CompanyID = creditMemo.CompanyID,
                        LocationID = creditMemo.LocationID,
                        ReceivedLocationID = creditMemo.PickupLocationID,
                        Applications = new List<PaymentApplicationRequest>()
                        {
                            new PaymentApplicationRequest()
                            {
                                Amount = differenceApplication.Value,
                                OrderID = creditMemo.ID,
                            }
                        },
                    };

                    return await paymentService.CreatePaymentTransactionAsync(paymentRequest, paymentMaster.ID, info.userLink, info.connectionID, false);
                }
            }

            return null;
        }

        private async Task<PaymentMaster> RefundCreditMemoPaymentsFromAMaster(PaymentService paymentService, CreditMemoData creditMemo, int masterID)
        {
            // get all the payments from that master
            var master = _context.PaymentMaster.Include(t => t.PaymentApplications).FirstOrDefault(t => t.ID == masterID);
            var applications = master.PaymentApplications.Where(t => t.PaymentTransactionType == (byte)PaymentTransactionType.Payment_from_Nonrefundable_Credit && t.Amount>0).ToList();
            var info = GetUserLinkAndConnection();

            foreach(var app in applications.Where(x => x.NonRefBalance>0))
            {
                // revert the payments made
                PaymentRequest paymentRequest = new PaymentRequest()
                {
                    PaymentTransactionType = (int)PaymentTransactionType.Refund_to_Nonrefundable_Credit,
                    PaymentMethodID = (int)PaymentMethodType.NonRefundableCredit,
                    Amount = app.Amount,
                    CurrencyType = null,
                    Notes = "",
                    CompanyID = creditMemo.CompanyID,
                    LocationID = creditMemo.LocationID,
                    ReceivedLocationID = creditMemo.PickupLocationID,                
                    Applications = new List<PaymentApplicationRequest>()
                    {
                        new PaymentApplicationRequest()
                        {
                            Amount = app.Amount, //app.Amount should never be negative
                            OrderID = null,
                        },
                        new PaymentApplicationRequest()
                        {
                            Amount = -app.Amount,
                            OrderID = app.OrderID,
                        }
                    },
                };
                await paymentService.CreatePaymentTransactionAsync(paymentRequest, masterID, info.userLink, info.connectionID, false);

                if (!paymentService.ServiceModelState.IsValid)
                    throw new Exception("An error occured while reverting the payments.");
            }
            return master;
        }


        /// <summary>
        /// Creates a new CreditMemo
        /// </summary>
        /// <param name="newModel">New CreditMemo data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CreditMemoData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Order creation fails")]
        public override async Task<IActionResult> Create([FromBody] CreditMemoData newModel, [FromQuery] Guid? tempID = null)
        {
            var result = await base.Create(newModel, tempID);
            try
            {
                if (newModel.Items != null && newModel.Items.Count() > 0 && newModel.OrderStatusID == OrderOrderStatus.CreditMemoPosted)
                {
                    //Only create credit gift if payment is posted
                    await CreateOrUpdateCreditGiftAdjustment(newModel);
                }
                if (ModelState.Keys.Contains("Not Found"))
                    return NotFound(ModelState);
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e);
            }
            return result;
        }

        private (short? userLink, string connectionID) GetUserLinkAndConnection()
        {
            var userLink = this.User.UserLinkID();
            var connectionID = Request.Headers[ConnectionIDHeaderKey].FirstOrDefault();
            return (userLink, connectionID);
        }
    }
}
