﻿using Endor.Api.Web.Services;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Endor.Security;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Api.Web.Classes;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// The controller for Custom Field Value
    /// </summary>
    [Route("api/cf/value")]
    [Authorize]
    public class CustomFieldValueController : CRUDInternalController<SystemListFilter, FilterService, int>
    {
        /// <summary>
        /// ApiContext
        /// </summary>
        protected ApiContext _ctx;
        /// <summary>
        /// Task Queuer
        /// </summary>
        protected ITaskQueuer _taskQueuer;
        /// <summary>
        /// Data Cache
        /// </summary>
        protected ITenantDataCache _DataCache;

        private Lazy<CustomFieldService> _customFieldService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CustomFieldValueController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
            //we need the lambda for User.BID because at constructor time User is null
            //so we use the lazy in the service because by the time we need a service, the user exists
            this._logger = logger;
            this._rtmClient = rtmClient;
            this._ctx = context;
            this._taskQueuer = taskQueuer;
            this._DataCache = cache;

            _customFieldService = CustomFieldService.CreateService(_ctx, _logger, _rtmClient);
        }

        private bool IsAssemblyClassTypeID(int CTID)
        {
            return CTID == (int)ClassType.Assembly;
        }

        private bool IsProductAssemblyType(short BID, int entityID)
        {
            return this._ctx.AssemblyData.Any(a => a.BID == BID && a.ID == entityID && a.AssemblyType == AssemblyType.Product);
        }

        #region Overridden CRUD Methods
        /// <summary>
        /// Returns the CustomFieldValue specified by the CTID and ID values
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="ID">Item ID</param>
        /// <param name="filters">Filters</param>
        /// <returns></returns>
        [HttpGet("{CTID}/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldValue[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Specified CTID or ID does not Exist")]
        public async Task<IActionResult> Read(int CTID, int ID, [FromQuery] CustomFieldValueFilters filters)
        {
            short BID = User.BID().Value;

            List<CustomFieldValue> result = null;

            if (IsAssemblyClassTypeID(CTID))
            {
                //Only fetch customer fields for Product AssemblyTypes.
                if(IsProductAssemblyType(BID, ID))
                {
                    result = _customFieldService.Value.ReadCustomFields(BID, CTID, ID, filters);
                }

            }
            else
            {

                result = _customFieldService.Value.ReadCustomFields(BID, CTID, ID, filters);
            }

            

            if (result == null)
                return NotFound();

            return await Task.FromResult(Ok(result));
        }

        /// <summary>
        /// Returns the CustomFieldValue specified by the CTID,CFID  and ID values
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="ID">Item ID</param>
        /// <param name="CFID">Custom Field Value ID</param>
        /// <returns></returns>
        [HttpGet("{CTID}/{ID}/{CFID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldValue[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "Specified CTID or ID does not Exist")]
        public async Task<IActionResult> ReadByID(int CTID, int ID, short CFID)
        {
            short BID = User.BID().Value;
            var result = _customFieldService.Value.ReadCustomField(BID, CTID, ID, CFID);

            if (result == null)
                return NotFound();

            return await Task.FromResult(Ok(result));
        }

        /// <summary>
        /// Replaces the CF Value for the object identified by theCustom Field ID={cfid}.
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="ID">Item ID</param>
        /// <param name="CFID">Custom Field Value ID</param>
        /// <param name="update">Updated Custom Field Value</param>
        /// <returns></returns>
        [HttpPut("{CTID}/{ID}/{CFID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldValue[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the CustomFieldValue does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If the CustomFieldValue to update is not the user's business, or the user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the CustomFieldValue does not exist or the update fails.")]
        public async Task<IActionResult> Update(int CTID, int ID, short CFID, [FromBody] CustomFieldValue update)
        {
            if (update == null)
            {
                return new BadRequestObjectResult("Body missing or invalid");
            }
            else
            {
                short BID = User.BID().Value;
                string result = _customFieldService.Value.UpdateCustomField(BID, CTID, ID, CFID, update);

                if (result == null)
                    return new BadRequestObjectResult("Creation failed");

                return await Task.FromResult(Ok(result));
            }
        }

        /// <summary>
        /// Creates a new CustomFieldValue Record
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="ID">Item ID</param>
        /// <param name="newModel">Custom Field Value model</param>
        /// <returns></returns>
        [HttpPut("{CTID}/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldValue[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Custom Field Value creation fails")]
        public async Task<IActionResult> Create(int CTID, int ID, [FromBody] CustomFieldValue[] newModel)
        {
            
            short BID = this.User.BID().Value;

            string result = null;

            if (IsAssemblyClassTypeID(CTID))
            {
                //Only update custom fields for Product AssemblyTypes.
                if(IsProductAssemblyType(BID, ID))
                {
                    result = _customFieldService.Value.UpdateCustomFields(BID, CTID, ID, newModel);
                }
            }
            else
            {
                result = _customFieldService.Value.UpdateCustomFields(BID, CTID, ID, newModel);
            }

            if (result == null)
                return new BadRequestObjectResult("Creation failed");

            return await Task.FromResult(Ok(result));
            
        }

        /// <summary>
        /// Deletes a CustomFieldValue by ID
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="ID">ID of the CustomFieldValue to Delete</param>
        /// <returns></returns>
        [HttpDelete("{CTID}/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "CustomFieldValue is removed")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Custom Field Value delete failed")]
        public async Task<IActionResult> Delete(int CTID, int ID)
        {
            short BID = User.BID().Value;
            string result = _customFieldService.Value.DeleteCustomFields(BID, CTID, ID);

            if (result == null)
                return new BadRequestObjectResult("Deletion failed");

            return await Task.FromResult(Ok(result));
        }

        /// <summary>
        /// Deletes a CustomFieldValue by ID and CFID
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="ID">ID of the CustomFieldValue to Delete</param>
        /// <param name="CFID">Custom Field Value ID</param>
        /// <returns></returns>
        [HttpDelete("{CTID}/{ID}/{CFID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CustomFieldValue[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Custom Field Value delete failed")]
        public async Task<IActionResult> DeleteCFID(int CTID, int ID, short CFID)
        {
            short BID = User.BID().Value;
            string result = _customFieldService.Value.DeleteCustomField(BID, CTID, ID, CFID);

            if (result == null)
                return new BadRequestObjectResult("Deletion failed");

            return await Task.FromResult(Ok(result));
        }

        /// <summary>
        /// Checks if the CFVs passed in the body of the message are valid 
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="newModel">Custom Field Values to validate</param>
        /// <returns></returns>
        [HttpPost("{CTID}/action/validate")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(string))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Custom Field Value delete failed")]
        public async Task<IActionResult> Validate(int CTID, [FromBody] CustomFieldValue[] newModel)
        {
            return await Task.FromResult(StatusCode((int)HttpStatusCode.NotImplemented));
        }

        #endregion
    }



}
