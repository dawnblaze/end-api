﻿using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// CRUDController - Base Controller for most top level entity controllers
    /// </summary>
    /// <typeparam name="M">Model Class</typeparam>
    /// <typeparam name="S">Service Class</typeparam>
    /// <typeparam name="I">ID Type Param e.g. byte, short, int, etc</typeparam>
    [Route("api/[controller]")]
    [Authorize]
    public class CRUDController<M, S, I> : CRUDInternalController<M, S, I>
        where M : class, IAtom<I>
        where S : AtomCRUDService<M, I>
        where I : struct, IConvertible
    {
        /// <summary>
        /// CRUDController Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        /// <param name="logger">RemoteLogger</param>
        /// <param name="rtmClient">IRTMPushClient</param>
        /// <param name="taskQueuer">ITaskQueuer</param>
        /// <param name="cache">ITenantDataCache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <param name="customLazySvcGenerator">optional function that is passed User.BID and must return an instance of S</param>
        public CRUDController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper, Func<short, S> customLazySvcGenerator = null)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper, customLazySvcGenerator) { }

        /// <summary>
        /// Base Read Endpoint - Can be overridden
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual async Task<IActionResult> Read()
        {
            return await DoRead();
        }

        internal async Task<IActionResult> ReadWith(IExpandIncludes includes)
        {
            return await DoReadWith(includes);
        }

        internal async Task<IActionResult> FilteredReadWith(Expression<Func<M, bool>> filterPredicate, IExpandIncludes includes)
        {
            return await DoFilteredReadWith(filterPredicate, includes);
        }

        internal async Task<IActionResult> ReadWith(I ID, IExpandIncludes includes)
        {
            return await DoReadWith(ID, includes);
        }

        /// <summary>
        /// Base Read By ID Endpoint - Can be overridden
        /// </summary>
        /// <param name="ID">ID Parameter of type I</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        public virtual async Task<IActionResult> ReadById(I ID)
        {
            return await DoRead(ID);
        }

        /// <summary>
        /// Base Update Endpoint - Can be overridden
        /// </summary>
        /// <param name="ID">ID of record to be updated of type I</param>
        /// <param name="update">Model of record to be updated</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        public virtual async Task<IActionResult> Update(I ID, [FromBody] M update)
        {
            return await DoUpdate(ID, update, Request.Headers[ConnectionIDHeaderKey]);
        }

        /// <summary>
        /// Base Create Endpoint - Can be overridden
        /// </summary>
        /// <param name="newModel">Model of record to be updated</param>
        /// <param name="tempID">Optional Temp ID</param>
        /// <returns></returns>
        [HttpPost]
        public virtual async Task<IActionResult> Create([FromBody] M newModel, [FromQuery] Guid? tempID = null)
        {
            return await DoCreate(newModel, tempID);
        }

        /// <summary>
        /// Base Clone Endpoint - Can be overridden
        /// </summary>
        /// <param name="ID">ID of record to be cloned</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        public virtual async Task<IActionResult> Clone(I ID)
        {
            return await DoClone(ID);
        }

        /// <summary>
        /// Base Delete Endpoint - Can be overridden
        /// </summary>
        /// <param name="ID">ID of record to be deleted</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        public virtual async Task<IActionResult> Delete(I ID)
        {
            return await DoDelete(ID);
        }

    }
}
