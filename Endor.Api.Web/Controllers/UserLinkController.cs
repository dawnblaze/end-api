﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// controller for userLink table
    /// </summary>
    [Route("api/userlink")]
    [Authorize]
    public class UserLinkController: Controller
    {
        private ApiContext _context;
        private RemoteLogger _logger;
        private IRTMPushClient _rtmClient;
        private ITaskQueuer _taskQueuer;
        private ITenantDataCache _cache;
        private IMigrationHelper _migrationHelper;

        /// <inheritdoc />
        /// <summary>
        /// constructor
        /// </summary>
        public UserLinkController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
        {
            _context = context;
            _logger = logger;
            _rtmClient = rtmClient;
            _taskQueuer = taskQueuer;
            _cache = cache;
            _migrationHelper = migrationHelper;
        }


        //[SwaggerOperationFilter(typeof(IgnoreHttpRequestParameterOperationFilter))]
        //[SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        //[SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Employee does not exist.")]
        //[SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        //[SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Employee does not exist or the deletion fails.")]
        /// <summary>
        /// Deletes a UserLink by ID
        /// </summary>
        /// <param name="ID">ID of the userlink to Delete</param>
        /// <param name="force"></param>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpDelete("{ID}")]
        public async Task<IActionResult> DeleteWithForce(short ID, bool force = false)
        {
            if (force)
            {
                return (await new UserLinkService(_context, User.BID().Value, _rtmClient, _taskQueuer, _migrationHelper).ForceDelete(User.UserLinkID().Value, ID)).ToResult();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
