﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Services;
using Endor.DNSManagement;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// SSL Certificate Controller
    /// </summary>
    [Route("Api/SSLCertificate")]
    public class SSLCertificateController : CRUDController<SSLCertificateData, SSLCertificateService, short>
    {
        private readonly IDNSManager _dnsManager;

        /// <summary>
        /// SSL Certificate Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="dnsManager">DNS Manager</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public SSLCertificateController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IDNSManager dnsManager, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
            _dnsManager = dnsManager;
        }

        /// <summary>
        /// Returns a simple list of SSL Certificates
        /// </summary>
        /// <returns></returns>
        [HttpGet("SimpleList")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SSLCertificateData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleListItem<short>[]> SimpleList()
        {
            return await _service.GetSimpleList();
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the SSLCertificates
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SSLCertificateData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single SSLCertificate by ID
        /// </summary>
        /// <param name="ID">SSLCertificate ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SSLCertificateData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the SSLCertificate does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the SSLCertificate does not exist.")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// This is not a public endpoint.
        /// </summary>
        /// <param name="ID">SSL Certificate ID</param>
        /// <param name="update">Updated SSL Certificate data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SSLCertificateData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "This is not a public endpoint.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
#pragma warning disable CS1998 // async lacking await operator
        public override async Task<IActionResult> Update(short ID, [FromBody] SSLCertificateData update)
        {
            return new NotFoundObjectResult("This is not a public endpoint");
        }
#pragma warning restore CS1998 // async lacking await operator

        /// <summary>
        /// Creates a new SSL Certificate
        /// </summary>
        /// <param name="newModel">New SSLCertificateData model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SSLCertificateData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the SSL Certificate creation fails")]
        public override async Task<IActionResult> Create([FromBody] SSLCertificateData newModel, [FromQuery] Guid? tempID = null)
        {
            this._service.DNSManager = _dnsManager;
            IActionResult processResult = await _service.ProcessCertificate(newModel);

            if (processResult is OkObjectResult)
            {
                return await base.Create(newModel, tempID);
            }
            else
            {
                return processResult;
            }
        }

        /// <summary>
        /// This is not a public endpoint.
        /// </summary>
        /// <param name="ID">ID of the SSL Certificate to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/Clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "This is not a public endpoint.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
#pragma warning disable CS1998 // async lacking await operator
        public override async Task<IActionResult> Clone(short ID)
        {
            return new NotFoundObjectResult("This is not a public endpoint");
        }
#pragma warning restore CS1998 // async lacking await operator

        /// <summary>
        /// This is not a public endpoint.
        /// </summary>
        /// <param name="ID">ID of the SSLCertificate to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "This is not a public endpoint.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
#pragma warning disable CS1998 // async lacking await operator
        public override async Task<IActionResult> Delete(short ID)
        {
            return new NotFoundObjectResult("This is not a public endpoint");
        }
#pragma warning restore CS1998 // async lacking await operator

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("thing")]
        public async Task<IActionResult> ProcessCertificate()
        {
            this._service.DNSManager = _dnsManager;
            return await _service.ProcessCertificate(new SSLCertificateData());
        }
    }
}
