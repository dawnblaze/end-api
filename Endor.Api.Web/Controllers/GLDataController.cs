﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Responses;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// This is GLData Controller
    /// </summary>
    [Route("api/gldata")]
    [ApiController]
    public class GLDataController : CRUDController<GLData, GLDataService, int>
    {
        /// <summary>
        /// GlData API Controller constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper"></param>
        public GLDataController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns fintered GLDatas
        /// with filters
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GLData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the parameters were wrong specified")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] GLDataFilters filters)
        {
            var serviceResponse = await _service.GetWithFiltersAsync(filters);
            return serviceResponse.ToResult();
        }

        /// <summary>
        /// Returns all GLAccounts
        /// use ReadWithFilters instead
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete("Use ReadWithFilters instead")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return ReadWithFilters(null);
        }

        /// <summary>
        /// Returns a single GLData by ID
        /// </summary>
        /// <param name="ID">GLData ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GLData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the GLData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Returns fintered GLDatas
        /// with filters
        /// </summary>
        /// <returns></returns>
        [HttpGet("summary")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GLDataSummaryResponse[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the parameters were wrong specified")]
        public async Task<IActionResult> GLDatasummary([FromQuery] GLDataSummaryFilters filters)
        {
            var serviceResponse = await _service.GetSummaryWithFiltersAsync(filters);
            return serviceResponse.ToResult();
        }
    }
}