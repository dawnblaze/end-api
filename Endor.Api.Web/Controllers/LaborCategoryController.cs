using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Labor category endpoints
    /// </summary>
    [Route("api/laborcategory")]
    public class LaborCategoryController : CRUDController<LaborCategory, LaborCategoryService, short>, ISimpleListableViewController<SimpleLaborCategory, short>
    {
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public LaborCategoryController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }
        /// <summary>
        /// Get the Service for the SimpleLaborCategory
        /// </summary>
        public ISimpleListableViewService<SimpleLaborCategory, short> ListableService => this._service;

        /// <summary>
        /// Returns all of the LaborCategories
        /// can be filtered by IsActive
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LaborCategory[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If invalid includes or filters are specified")]
        public async Task<IActionResult> FilteredRead([FromQuery] LaborCategoryFilter filter)
        {
            return await base.FilteredReadWith(item=>item.IsActive==(filter.IsActive ?? item.IsActive), null);
        }

        /// <summary>
        /// obselete
        /// </summary>
        /// <returns></returns>
        [HttpGet("obselete")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LaborCategory[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Read()
        {
            return await base.Read();
        }

        /// <summary>
        /// obselete
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("obselete/{ID}")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LaborCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If LaborCategory does not exist")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Returns an individual labor category
        /// </summary>
        /// <param name="ID">LaborCategory.ID</param>
        /// <param name="includes"></param>
        /// <returns>`LaborCategory`</returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LaborCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If LaborCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If invalid includes are specified")]
        public async Task<IActionResult> ReadWithIncludes(short ID, [FromQuery]LaborCategoryIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }        

        /// <summary>
        /// obselete-simplelist
        /// </summary>
        /// <returns></returns>
        [HttpGet("obselete-simplelist")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleLaborCategory[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleLaborCategory[]> SimpleList()
        {
            return await this.GetSimpleList<SimpleLaborCategory, short>(User.BID().Value);
        }

        /// <summary>
        /// Returns simple list of labor categories
        /// optionally filtered by IsActive query parameter
        /// </summary>
        /// <returns></returns>
        [HttpGet("simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleLaborCategory[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If invalid filters are specified")]
        public async Task<SimpleLaborCategory[]> SimpleList([FromQuery]SimpleLaborCategoryFilters filters)
        {
            return await this.GetSimpleList<SimpleLaborCategory, short>(User.BID().Value, filters);
        }        
        
        /// <summary>
        /// updates a single Labor Category by ID and body
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LaborCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If LaborCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the LaborCategory does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] LaborCategory update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Deletes a LaborCategory by ID
        /// with an optional Force query parameter
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Force">If true, delete all links to labor parts first</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If LaborCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the LaborCategory does not exist or the deletion fails")]
        public async Task<IActionResult> DeleteWithForce(short ID, [FromQuery]bool Force = false)
        {
            await this._service.unlinkAllAsync(ID);
            return await base.Delete(ID);
        }

        /// <summary>
        /// obsolete delete
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Obsolete]
        [HttpDelete("obselete/{ID}")]
        public override async Task<IActionResult> Delete(short ID)
        {
            return await base.DoDelete(ID);
        }

        /// <summary>
        /// Creates a new LaborCategory
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempID"></param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LaborCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the LaborCategory's creation fails")]
        public override async Task<IActionResult> Create([FromBody] LaborCategory newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.DoCreate(newModel, tempID);
        }

        //ACTIONS

        /// <summary>
        /// sets a LaborCategory to active
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> SetActive([FromRoute]short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// sets a LaborCategory to inactive
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> SetInActive([FromRoute]short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Returns whether a LaborCategory can be deleted
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("{ID}/action/candelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
#pragma warning disable CS1998
        public async Task<IActionResult> CanDelete([FromRoute]short ID)
#pragma warning restore CS1998
        {
            return (new BooleanResponse() { Success = true, Message = "CanDelete: true", Value = true }).ToResult();
        }

        /// <summary>
        /// links a labor category to a labor part
        /// </summary>
        /// <param name="laborCategoryID">LaborCategory.ID</param>
        /// <param name="laborDataID">LaborData.ID</param>
        /// <returns></returns>
        [HttpPost("{laborCategoryID}/action/linklabor/{laborDataID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> LinkLabor([FromRoute]short laborCategoryID, [FromRoute]int laborDataID)
        {
            return (await this._service.LinkLabor(laborCategoryID, laborDataID, true)).ToResult();
        }

        /// <summary>
        /// unlinks a labor category from a labor part
        /// </summary>
        /// <param name="laborCategoryID">LaborCategory.ID</param>
        /// <param name="laborDataID">LaborData.ID</param>
        /// <returns></returns>
        [HttpPost("{laborCategoryID}/action/unlinklabor/{laborDataID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> UnLinkLabor([FromRoute]short laborCategoryID, [FromRoute]int laborDataID)
        {
            return (await this._service.LinkLabor(laborCategoryID, laborDataID, false)).ToResult();
        }

        /// <summary>
        /// Labor Category Clone Endpoint
        /// </summary>
        /// <param name="ID">ID of record to be cloned</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<LaborCategory>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<LaborCategory>), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public override async Task<IActionResult> Clone(short ID)
        {
            try
            {
                return await DoClone(ID);
            }
            catch (Exception e)
            {
                GenericResponse<LaborCategory> genericResponse = new GenericResponse<LaborCategory>() { ErrorMessage = e.Message };
                return genericResponse.ToResult();
            }
        }

        /// <summary>
        /// Validate the Labor Category Model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            var categoryModel = (LaborCategory)model;
            var existingNameOwners = this._service.ExistingNameOwners(categoryModel);
            if (existingNameOwners.Count()>0)
            {
                this.ModelState.AddModelError("Name", $"A LaborCategory named {categoryModel.Name} already exists.");
            }

            return base.TryValidateModel(model, prefix);
        }            
    }
}