﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Endpoints for getting/setting info related to objects
    /// </summary>
    [Route("API/[controller]")]
    [Authorize]
    public class ObjectController : Controller
    {
        private readonly Lazy<ObjectService> _objectService;
        private readonly Lazy<TagService> _tagService;
        private readonly ITenantDataCache _tenantCache;
        private readonly ApiContext _ctx;
        private readonly IRTMPushClient _rtmClient;
        /// <summary>
        /// task queuer for triggering indexes
        /// </summary>
        protected readonly ITaskQueuer _taskQueuer;

        /// <summary>
        /// Ctor for Endpoints for objects
        /// </summary>
        /// <param name="tenantCache"></param>
        /// <param name="ctx"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public ObjectController(ITenantDataCache tenantCache, ApiContext ctx, RemoteLogger logger, IMigrationHelper migrationHelper, IRTMPushClient rtmClient = null, ITaskQueuer taskQueuer = null)
        {
            _tenantCache = tenantCache;
            _ctx = ctx;
            this._rtmClient = rtmClient;
            this._taskQueuer = taskQueuer;
            migrationHelper.MigrateDb(_ctx);

            this._objectService = new Lazy<ObjectService>(() => new ObjectService(ctx,_taskQueuer,_rtmClient));
            this._tagService = new Lazy<TagService>(() => new TagService(ctx, this.User.BID().Value, _taskQueuer, _tenantCache, logger, rtmClient, migrationHelper));
        }

        /// <summary>
        /// Returns a list of features supported on this classtype using the /api/object endpoints
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <returns></returns>
        [HttpGet("{CTID}/GetSupportedFeatures")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<string[]>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If setting the urgency fails")]
        public async Task<IActionResult> GetSupportedFeatures(int CTID)
        {
            return (await _objectService.Value.GetSupportedFeatures(CTID)).ToResult();
        }

        /// <summary>
        /// Sets the urgency level for an object
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="ID">Item ID</param>
        /// <param name="urgency">Urgency value - Valid values are "hi","normal"</param>
        /// <returns></returns>
        [HttpPost("{CTID}/{ID}/Action/SetUrgency/{urgency}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If setting the urgency fails")]
        public async Task<IActionResult> SetUrgency(int CTID, int ID, string urgency)
        {
            return (await _objectService.Value.SetUrgency(User.BID().Value, CTID, ID, urgency)).ToResult();
        }

        /// <summary>
        /// Sets the priority level for an object
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="ID">Item ID</param>
        /// <param name="priority">Integer Value between 1 and 32767</param>
        /// <returns></returns>
        [HttpPost("{CTID}/{ID}/Action/SetPriority/{priority}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If setting the urgency fails")]
        public async Task<IActionResult> SetPriority(int CTID, int ID, int priority)
        {
            return (await _objectService.Value.SetPriority(User.BID().Value, CTID, ID, priority)).ToResult();
        }

        /// <summary>
        /// Sets the specified Tag on the specified object
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="ID">Item ID</param>
        /// <param name="tagID">Tag ID</param>
        /// <returns></returns>
        [HttpPost("{CTID}/{ID}/Action/AddTag/{tagID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If setting the urgency fails")]
        public async Task<IActionResult> AddTag(int CTID, int ID, short tagID)
        {
            return (await this._tagService.Value.LinkTag(tagID, CTID, ID)).ToResult();
        }

        /// <summary>
        /// Sets the specified Tag on the specified object
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="ID">Item ID</param>
        /// <param name="tagIDList">Tag ID List</param>
        /// <returns></returns>
        [HttpPost("{CTID}/{ID}/Action/AddTag/")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If setting the urgency fails")]
        public async Task<IActionResult> AddTag(int CTID, int ID, [FromBody] short[] tagIDList)
        {
            foreach(short tagID in tagIDList)
            {
                await this._tagService.Value.LinkTag(tagID, CTID, ID);
            }

            var returnData = new EntityActionChangeResponse()
            {
                Id = ID,
                Message = "Successful",
                Success = true,
                ErrorMessage = ""
            };

            return returnData.ToResult();
        }

        /// <summary>
        /// Removes the specified Tag on the specified object
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="ID">Item ID</param>
        /// <param name="tagID">Tag ID</param>
        /// <returns></returns>
        [HttpPost("{CTID}/{ID}/Action/RemoveTag/{tagID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If setting the urgency fails")]
        public async Task<IActionResult> RemoveTag(int CTID, int ID, short tagID)
        {
            return (await this._tagService.Value.UnLinkTag(tagID, CTID, ID)).ToResult();

        }

        /// <summary>
        /// Removes the specified Tag on the specified object
        /// </summary>
        /// <param name="CTID">Class Type ID</param>
        /// <param name="ID">Item ID</param>
        /// <param name="tagIDList">Tag ID List</param>
        /// <returns></returns>
        [HttpPost("{CTID}/{ID}/Action/RemoveTag/")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If setting the urgency fails")]
        public async Task<IActionResult> RemoveTag(int CTID, int ID, [FromBody] short[] tagIDList)
        {
            foreach (short tagID in tagIDList)
            {
                await this._tagService.Value.UnLinkTag(tagID, CTID, ID);
            }

            var returnData = new EntityActionChangeResponse()
            {
                Id = ID,
                Message = "Successful",
                Success = true,
                ErrorMessage = ""
            };

            return returnData.ToResult();
        }
    }
}
