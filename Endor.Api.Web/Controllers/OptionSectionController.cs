﻿using Endor.Api.Web.Services;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Endor.Api.Web.Classes;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Option Section controller
    /// </summary>
    [Authorize]
    [Route("api/options/section")]
    public class OptionSectionController : GenericController<SystemOptionSection, OptionSectionService, int>
    {
        /// <summary>
        /// Option Section controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="options">Options</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public OptionSectionController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, EndorOptions options, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Returns an IGenericResponse where IGenericResponse.Value is an OptionSection
        /// </summary>
        /// <param name="section">If `section` can be parsed to a number, it will try to match `OptionSection.ID`, otherwise `OptionSection.Name` will be used</param>
        [HttpGet]
        [Route("{section}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GetSectionValueResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If no OptionSection is found by either OptionSection.Name or OptionSection.ID")]
        public async Task<IActionResult> Get(
                string section = null
            )
        {
            string Name = "";
            if (!int.TryParse(section, out int ID))
            {
                Name = section;
            }

            GetSectionValueResponse resp = await this._service.Get(
                ID == 0 ? (int?)null : ID, Name);
            return resp.ToResult();
        }
    }
}
