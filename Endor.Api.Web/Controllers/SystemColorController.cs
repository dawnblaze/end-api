﻿using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tenant;
using Endor.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api controller for /system/color
    /// </summary>
    [Authorize]
    [Route("Api/System")]
    public class SystemColorController : Controller
    {
        private readonly ApiContext _ctx;
        private readonly Lazy<SystemColorService> _lazyService;
        private SystemColorService _service { get { return _lazyService.Value; } }

        /// <summary>
        /// API Controller for System Colors
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public SystemColorController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, IMigrationHelper migrationHelper)
        {
            this._ctx = context;
            migrationHelper.MigrateDb(_ctx);
            this._lazyService = new Lazy<SystemColorService>(() => new SystemColorService(context, User.BID().Value, migrationHelper));
        }

        /// <summary>
        /// Retrieves the System Colors
        /// </summary>
        /// <returns></returns>
        [HttpGet("Color")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SystemColor[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetSystemAlertActions()
        {
            return new OkObjectResult(await _service.GetSystemColorsAsync());
        }

    }
}
