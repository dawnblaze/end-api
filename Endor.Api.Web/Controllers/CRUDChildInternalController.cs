﻿using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Child controller that knows about its parent type
    /// </summary>
    /// <typeparam name="M">Entity type</typeparam>
    /// <typeparam name="S">Entity Service type</typeparam>
    /// <typeparam name="I">Entity ID type</typeparam>
    /// <typeparam name="PI">Parent Entity ID type</typeparam>
    public class CRUDChildInternalController<M, S, I, PI> : CRUDInternalController<M, S, I>
        where M : class, IAtom<I>
        where S : AtomCRUDChildService<M, I, PI>
        where I : struct, IConvertible
        where PI : struct, IConvertible
    {
        /// <summary>
        /// Constructor for CRUDChildInternalController
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CRUDChildInternalController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        internal async Task<IActionResult> DoReadWith(PI parentID, IExpandIncludes includes)
        {
            try
            {
                return Ok(await _service.GetByParentIDAsync(parentID));
            }
            catch (InvalidOperationException ioe)
            {
                if (ioe.Message.Contains("The property") && ioe.Message.Contains("'Simple"))
                {
                    return BadRequest("Invalid includes were specified.");
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        internal async Task<IActionResult> DoReadWith(PI parentID, I ID, IExpandIncludes includes)
        {
            //Handle missing entity here ObjectNotFoundResult
            //Handle incorrect includes with bad request.
            try
            {
                M result = await _service.GetAsync(ID, includes);

                if (result == null || !_service.IsParentIDValid(result, parentID))
                    return NotFound();

                return Ok(result);
            }
            catch (InvalidOperationException ioe)
            {
                if (ioe.Message.Contains("The property") && ioe.Message.Contains("'Simple"))
                {
                    return BadRequest("Invalid includes were specified.");
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        internal async Task<IActionResult> DoRead(PI parentID)
        {
            return new OkObjectResult(await _service.GetByParentIDAsync(parentID));
        }

        internal async Task<IActionResult> DoRead(PI parentID, I ID)
        {
            M model = await _service.GetAsync(ID);

            if (model == null || !_service.IsParentIDValid(model, parentID))
                return NotFound();

            if (model == null)
            {
                return new NotFoundResult();
            }

            return new OkObjectResult(model);
        }

        internal async Task<IActionResult> DoFilteredReadWith(PI parentID, Expression<Func<M, bool>> filterPredicate, IExpandIncludes includes)
        {
            try
            {
                return new OkObjectResult(await _service.WhereParent(_service.Where(filterPredicate, includes: includes), parentID).ToListAsync());
            }
            catch (InvalidOperationException ioe)
            {
                if (ioe.Message.Contains("The property") && ioe.Message.Contains("'Simple"))
                {
                    return BadRequest("Invalid includes were specified.");
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        internal async Task<IActionResult> DoCreate(PI parentID, M newModel, Guid? tempGuid = null)
        {
            if (newModel == null)
            {
                return new BadRequestObjectResult("Body missing or invalid");
            }
            else
            {
                // This can be removed as it is handled on the service
                SetCreateModelProperties(parentID, newModel);

                Validate(newModel, "create");

                if (!ModelState.IsValid)
                {
                    return this.ApiValidationError();
                }

                var result = await _service.CreateAsync(newModel, tempGuid);
                if (result == null)
                {
                    await _logger.Error(newModel.BID, "AddAsync returned null", null);
                    return new BadRequestObjectResult("Creation failed");
                }

                return new OkObjectResult(newModel);
            }
        }

        internal async Task<IActionResult> DoUpdate(PI parentID, I ID, M update)
        {
            if (update == null)
                return BadRequest();

            SetUpdateModelProperties(parentID, ID, update);

            Validate(update, "update");

            if (!ModelState.IsValid)
            {
                return this.ApiValidationError();
            }

            //if (!(await _service.ExistsAsync(ID)))
            //{
            //    return NotFound();
            //}
            //else
            //{
            var result = await _service.UpdateAsync(update, Request.Headers[ConnectionIDHeaderKey]);
            if (result == null)
            {
                await _logger.Error(update.BID, "UpdateAsync returned null", null);
                return new BadRequestObjectResult("Update failed");
            }

#warning does not re-request from server
            return new OkObjectResult(update);
            //}
        }

        internal async Task<IActionResult> DoDelete(PI parentID, I ID)
        {
            M toDelete = await _service.GetAsync(ID);

            if (toDelete == null)
            {
                await _service.QueueIndexForModel(ID);
                return NoContent();
            }
            else
            {
                // when this returns, the classtypeID = 0 (i don't know why at this point)
                await _service.DeleteAsync(toDelete);
                                
                return new NoContentResult();
            }
        }

        /// <summary>
        /// Sets update model properties
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="ID"></param>
        /// <param name="update"></param>
        protected void SetUpdateModelProperties(PI parentID, I ID, M update)
        {
            _service.SetParentID(update, parentID);
            update.ID = ID;
            SetCommonModelProperties(update);
        }

        /// <summary>
        /// Sets create model properties
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="newModel"></param>
        protected void SetCreateModelProperties(PI parentID, M newModel)
        {
            _service.SetParentID(newModel, parentID);
            SetCommonModelProperties(newModel);
        }
                
        private void Validate(M newModel, string prefix)
        {
            ModelState.Clear();

            _service.DoBeforeValidate(newModel);

            TryValidateModel(newModel, prefix);
        }
    }
}