﻿using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Includes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// This is a Permission Groups (Rights Group List) Controller
    /// </summary>
    [Route("Api/RightsGroupList")]
    public class RightsGroupListController :  CRUDController<RightsGroupList, RightsGroupListService, short>
    {
        /// <summary>
        /// Rights Group API Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public RightsGroupListController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all Rights Groups List and any included child objects specified in query parameters
        /// </summary>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(RightsGroupList[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetWithIncludes([FromQuery] RightsGroupListIncludes includes)
        {
            return await base.FilteredReadWith(x=>true, includes);
        }

        /// <summary>
        /// Returns all of the Rights Group List
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single Rights Group List by ID
        /// </summary>
        /// <param name="ID">RightsGroupList ID</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(RightsGroupList))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the TaxGroup does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetOneWithIncludes([FromRoute] short ID, [FromQuery] RightsGroupListIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }

        /// <summary>
        /// Returns a single TaxGroup by ID
        /// </summary>
        /// <param name="ID">TaxGroup ID</param>
        /// <returns></returns>
        [HttpGet("obsolete/{ID}")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> ReadById(short ID)
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single RightsGroupList by ID
        /// </summary>
        /// <param name="ID">RightsGroupList ID</param>
        /// <param name="update">Updated RightsGroupList data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(RightsGroupList))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the RightsGroupList does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the RightsGroupList does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] RightsGroupList update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new RightsGroupList
        /// </summary>
        /// <param name="newModel">new RightsGroupList data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(RightsGroupList))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the RightsGroupList creation fails")]
        public override async Task<IActionResult> Create([FromBody] RightsGroupList newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Deletes a RightsGroupList by ID
        /// </summary>
        /// <param name="ID">ID of the RightsGroupList to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the RightsGroupList does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the RightsGroupList does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            await this._service.DeleteExistingRightsGroupLinksAsync(ID);

            BooleanResponse resp = await this._service.CanDelete((short)ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                return await base.Delete(ID);
            }
            else
            {
                return new BadRequestObjectResult(resp);
            }
        }

        #endregion

        /// <summary>
        /// Returns true | false based on whether the RightsGroupList can be deleted.
        /// </summary>
        /// <param name="ID">RightsGroupList ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// links a RightsGroupList to a RightsGroup
        /// </summary>
        /// <param name="rightsGroupListID">RightsGroupList.ID</param>
        /// <param name="rightsGroupID">RightsGroup.ID</param>
        /// <returns></returns>
        [HttpPost("{rightsGroupListID}/action/linkrightsgroup/{rightsGroupID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<EntityActionChangeResponse> LinkRightsGroup([FromRoute]short rightsGroupListID, [FromRoute]short rightsGroupID)
        {
            var resp = await this._service.LinkRightsGroup(rightsGroupListID, rightsGroupID, true);
            
            return resp;
        }

        /// <summary>
        /// unlinks a RightsGroupList to a RightsGroup
        /// </summary>
        /// <param name="rightsGroupListID">RightsGroupList.ID</param>
        /// <param name="rightsGroupID">RightsGroup.ID</param>
        /// <returns></returns>
        [HttpPost("{rightsGroupListID}/action/unlinkrightsgroup/{rightsGroupID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<EntityActionChangeResponse> UnLinkRightsGroup([FromRoute]short rightsGroupListID, [FromRoute]short rightsGroupID)
        {
            var resp = await this._service.LinkRightsGroup(rightsGroupListID, rightsGroupID, false);

            return resp;
        }

        /// <summary>
        /// Gets an array of SimpleRightsGroup List
        /// </summary>
        /// <returns></returns>
        [HttpGet("SimpleList")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleRightsGroup[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleRightsGroup[]> SimpleList()
        {
            return await _service.GetSimpleList();
        }

        /// <summary>
        /// Validates RightsGroupList
        /// </summary>
        /// <param name="model">The RightsGroupList model to validate</param>
        /// <param name="prefix"> The key to use when looking up information in Microsoft.AspNetCore.Mvc.ControllerBase.ModelState</param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            if (model == null || !(model is RightsGroupList))
            {
                this.ModelState.AddModelError("model", "model is not an RightsGroupList");
                return false;
            }

            return base.TryValidateModel(model, prefix);
        }

        /// <summary>
        /// Clones an existing RightsGroupList to a new RightsGroupList
        /// </summary>
        /// <param name="ID">ID of the RightsGroupList to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/CloneObsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(RightsGroupList))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source RightsGroupList does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source RightsGroupList does not exist or the cloning fails.")]
        [Obsolete]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Clones an existing RightGroupList to a new RightGroupList
        /// </summary>
        /// <param name="ID">ID of the RightGroupList to clone from</param>
        /// <param name="newName">New RightGroupList Name</param>
        /// <returns></returns>
        [HttpPost("{ID}/Clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(RightsGroupList))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Right Group List does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Right Group List does not exist or the cloning fails.")]
        public async Task<IActionResult> Clone(short ID, [FromQuery] string newName)
        {
            if (String.IsNullOrWhiteSpace(newName))
                return await base.Clone(ID);

            try
            {
                var resp = await this._service.CloneAsync(ID, newName, this.User.UserLinkID());
                if (resp != null)
                    return new OkObjectResult(resp);
                else
                    return new BadRequestResult();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("not found"))
                    return new NotFoundResult();
                else
                    return new BadRequestObjectResult(ex.Message);
            }
        }
    }
}
