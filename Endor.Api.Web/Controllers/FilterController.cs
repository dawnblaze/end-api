﻿using Endor.Api.Web.Services;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Microsoft.AspNetCore.Mvc;
using Endor.Security;
using Microsoft.AspNetCore.Authorization;
using Endor.Tenant;
using System.Net;
using Swashbuckle.AspNetCore.SwaggerGen;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Filter Controller
    /// </summary>
    [Route("api/list")]
    [Authorize]
    public class FilterController : CRUDInternalController<SystemListFilter, FilterService, int>
    {
        /// <summary>
        /// Filter Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public FilterController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Creates a new list filter
        /// </summary>
        /// <param name="CTID"></param>
        /// <param name="newModel"></param>
        /// <returns></returns>
        [HttpPost("{CTID}/[controller]")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, type: typeof(SystemListFilter), description: "The newly created list filter")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If body is empty, if the CTID in route and model mismatch, or if the creation fails")]
        public async Task<IActionResult> Create(int CTID, [FromBody] SystemListFilter newModel)
        {
            if (newModel == null)
                return BadRequest("Invalid or incorrectly formatted model");

            if (CTID != newModel.TargetClassTypeID)
                return new BadRequestObjectResult("Inconsistent CTID");

            return await DoCreate(newModel);
        }

        /// <summary>
        /// Deletes a list filter
        /// </summary>
        /// <param name="CTID"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete("{CTID}/[controller]/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent, description: "If deletion succeeds")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the list filter doesn't exist or if the deletion fails")]
        public async Task<IActionResult> Delete(int CTID, int ID)
        {
            return await DoDelete(ID);
        }

        /// <summary>
        /// This endpoint retrieves a simplelist of the employees filters for a Target ClassTypeID.  The result includes the available filters, subscription status, and other information that may be needed.
        /// </summary>
        /// <param name="CTID"></param>
        /// <returns></returns>
        [HttpGet("{CTID}/[controller]/simplelist", Order = -2)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, type: typeof(SimpleSystemListFilter[]), description: "The simple list filter")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If there are no list filters.")]
        public async Task<IActionResult> GetSimpleList(int CTID)
        {
            List<SimpleSystemListFilter> models = await _service.ReadSimpleListForUser(User.BID().Value, CTID, User.UserLinkID().Value);

            if (models == null)
            {
                return new NotFoundResult();
            }

            return new OkObjectResult(models);
        }

        /// <summary>
        /// Retrieve the details of a particular filter, including the user’s subscription status on that filter.
        /// </summary>
        /// <param name="CTID"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("{CTID}/[controller]/{ID}", Order = -1)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, type: typeof(SimpleSystemListFilter[]), description: "The list filter")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the list filter is not found.")]
        public async Task<IActionResult> Read(int CTID, int ID)
        {
            SystemListFilter models = await _service.GetAsync(ID);

            if (models == null)
            {
                return new NotFoundResult();
            }

            return new OkObjectResult(models);
        }

        /// <summary>
        /// Gets the lists filters for the given CTID
        /// </summary>
        /// <param name="CTID"></param>
        /// <returns></returns>
        [HttpGet("{CTID}/[controller]")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, type: typeof(SystemListFilter[]), description: "The list filters")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If there are no list filters.")]
        public async Task<IActionResult> ReadByClassTypeId(int CTID)
        {
            List<SystemListFilter> models = await _service.ReadMyListsForEmployee(User.BID().Value, CTID, User.UserLinkID().Value);

            if (models == null)
            {
                return new NotFoundResult();
            }

            return new OkObjectResult(models);
        }
        
        /// <summary>
        /// Updates the list filter
        /// </summary>
        /// <param name="CTID"></param>
        /// <param name="ID"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        [HttpPut("{CTID}/[controller]/{ID}", Order = -1)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, type: typeof(SystemListFilter), description: "The list filter")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If body is empty, if the CTID in route and model mismatch, or if the read fails")]
        public async Task<IActionResult> Update(int CTID, int ID, [FromBody] SystemListFilter update)
        {
            if (update == null)
                return BadRequest("Empty or invalid model");

            if (update.ID != ID || CTID != update.TargetClassTypeID)
                return new BadRequestObjectResult("Inconsistent ID or CTID");

            return await DoUpdate(ID, update, Request.Headers[ConnectionIDHeaderKey]);
        }
    }
}
