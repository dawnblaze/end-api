﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Api.Web.Classes.Responses;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

using Endor.Api.Web.Annotation;
using Swashbuckle.AspNetCore.Annotations;
using Endor.Security.AuthorizationFilters;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Route ends up as api/employee/
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public class EmployeeController : CRUDController<EmployeeData, EmployeeService, short>, ISimpleListableViewController<SimpleEmployeeData, short>
    {
        private EndorOptions _options;
        private readonly Lazy<MessageHeaderService> _messageHeaderService;
        /// <summary>
        /// Listable service for Employee Controller
        /// </summary>
        public ISimpleListableViewService<SimpleEmployeeData, short> ListableService => this._service;

        /// <summary>
        /// Employee API Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="options">Options</param>
        /// <param name="migrationHelper"></param>
        public EmployeeController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, EndorOptions options, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
            _options = options;
            this._messageHeaderService = new Lazy<MessageHeaderService>(() => new MessageHeaderService(context, this.User.BID().Value, taskQueuer, cache, logger, rtmClient, migrationHelper));


        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the Employees
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HasSecurityRight(SecurityRight.AccessEmployeeSetup)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> Read()
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single Employee by ID
        /// </summary>
        /// <param name="ID">Employee ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [HasSecurityRight(SecurityRight.AccessEmployeeSetup)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Employee does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single Employee by ID
        /// </summary>
        /// <param name="ID">Employee ID</param>
        /// <param name="update">Updated Employee data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [HasSecurityRight(SecurityRight.CanManageEmployees)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Employee does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Employee does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] EmployeeData update)
        {
            // these shouldn't be included in a normal update
            if (update.EmployeeTeamLinks != null)
                update.EmployeeTeamLinks = null;

            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new Employee
        /// </summary>
        /// <param name="newModel">New Employee data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [HasSecurityRight(SecurityRight.CanManageEmployees)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Employee creation fails")]
        public override async Task<IActionResult> Create([FromBody] EmployeeData newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing Employee to a new Employee - also used for Rights testing
        /// </summary>
        /// <param name="ID">ID of the Employee to clone from</param>
        /// <returns></returns>
        [Obsolete]
        [HttpPost("{ID}/clone/obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Employee does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Employee does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            return new OkObjectResult(await Task.FromResult(0));
            //return await base.Clone(ID);
        }

        /// <summary>
        /// Clones an existing Employee to a new Employee - also used for Rights testing
        /// </summary>
        /// <param name="ID">ID of the Employee to clone from</param>
        /// <returns></returns>
        [Obsolete]
        [HttpPost("{ID}/clone/obsoleteany")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Employee does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Employee does not exist or the cloning fails.")]
        public async Task<IActionResult> CloneRequiresAny(short ID)
        {
            return new OkObjectResult(await Task.FromResult(0));
            //return await base.Clone(ID);
        }

        /// <summary>
        /// Clones an existing Employee to a new Employee
        /// </summary>
        /// <param name="ID">ID of the Employee to clone from</param>
        /// <param name="employeeClone">Override settings from clone modal</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Employee does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Employee does not exist or the cloning fails.")]
        public async Task<IActionResult> CloneFromModal(short ID, [FromBody] EmployeeClone employeeClone)
        {
            return await this._service.CloneWithOptions(ID, employeeClone, this.User.UserLinkID(), _options);
        }

        /// <summary>
        /// Deletes a Employee by ID
        /// </summary>
        /// <param name="ID">ID of the Employee to Delete</param>
        /// <returns></returns>
        [Obsolete]
        [HttpDelete("{ID}/Obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Employee does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Employee does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            return await base.Delete(ID);
        }

        /// <summary>
        /// Deletes a Employee by ID
        /// </summary>
        /// <param name="ID">ID of the Employee to Delete</param>
        /// <param name="force"></param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [HasSecurityRight(SecurityRight.CanManageEmployees)]
        [SwaggerOperationFilter(typeof(IgnoreHttpRequestParameterOperationFilter))]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Employee does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Employee does not exist or the deletion fails.")]
        public async Task<IActionResult> DeleteWithForce(short ID, bool force = false)
        {
            if (force)
            {
                var resp = await _service.ForceDeleteEmployee(ID, User.UserLinkID().Value);
                return resp.ToResult();
            }
            return await base.Delete(ID);
        }

        #endregion

        /// <summary>
        /// Gets a Simple list of Employees
        /// </summary>
        /// <returns></returns>
        [Route("simplelist/obsolete")]
        [Obsolete]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleEmployeeData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleEmployeeData[]> SimpleList()
        {
            return await this.GetSimpleList<SimpleEmployeeData, short>(User.BID().Value);
        }

        /// <summary>
        /// Gets a Simple list of Employees
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleEmployeeData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleEmployeeData[]> GetFilteredSimpleList([FromQuery] SimpleEmployeeFilters filters)
        {
            try
            { 
                return await this.GetSimpleList<SimpleEmployeeData, short>(User.BID().Value, filters);
            }
            catch (Exception ex)
            {
                throw new Exception($"GetFilteredSimpleList: {ex.Message}");
            }
        }        

        /// <summary>
        /// Try's to validate the data model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            if (model == null || !(model is EmployeeData))
            {
                this.ModelState.AddModelError("model", "model is not an employee");
                return false;
            }

            EmployeeData employee = (EmployeeData)model;

            if (String.IsNullOrWhiteSpace(employee.Last))
            {
                this.ModelState.AddModelError("LastName", "Last Name is required");
            }

            if (String.IsNullOrWhiteSpace(employee.First))
            {
                this.ModelState.AddModelError("FirstName", "First Name is required");
            }

            //return base.TryValidateModel(model, prefix);
            base.TryValidateModel(model, prefix);
            this.ModelState.Remove("create.First");
            this.ModelState.Remove("create.Last");
            return this.ModelState.IsValid;
        }

        /// <summary>
        /// Sets a EmployeeData to Active
        /// </summary>
        /// <param name="ID">EmployeeData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the EmployeeData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the EmployeeData does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            await _service.SendEmployeeUserLinkActiveInactiveRefreshMessage(resp.Id, true);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets a EmployeeData to Inactive
        /// </summary>
        /// <param name="ID">EmployeeData ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the EmployeeData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the EmployeeData does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            await _service.SendEmployeeUserLinkActiveInactiveRefreshMessage(resp.Id, false);

            return resp.ToResult();
        }

        /// <summary>
        /// Sends a user invite to an employee
        /// </summary>
        /// <param name="ID">EmployeeData ID</param>
        /// <param name="body">Body Content</param>
        /// <param name="returnUserLink">if true, returns the UserLink created by the invite</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/sendinvite")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the EmployeeData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> SendInvite(short ID, [FromBody] SendInviteRequest body, [FromQuery]bool returnUserLink = false)
        {
            EntityActionChangeResponse<short> resp = await this._service.SendInvite(ID, body, _options, returnUserLink);

            if (!resp.HasError)
                return resp.ToResult();

            if (resp.ErrorMessage.Contains("unauthorized", StringComparison.CurrentCultureIgnoreCase))
                return Unauthorized();

            if (resp.ErrorMessage.Contains("not found", StringComparison.CurrentCultureIgnoreCase))
                return NotFound(resp.ErrorMessage);

            if (resp.ErrorMessage.Contains("already exists", StringComparison.CurrentCultureIgnoreCase))
                return BadRequest(resp.ErrorMessage);

            if (resp.ErrorMessage.Contains("maximum", StringComparison.CurrentCultureIgnoreCase))
                return BadRequest(resp.ErrorMessage);

            return StatusCode(500, resp.ErrorMessage);
        }

        /// <summary>
        /// Checks if it can create user
        /// </summary>
        /// <returns></returns>
        [HttpGet("action/cancreateuser")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> CanCreateUser()
        {
            return (await this._service.CanCreateUser()).ToResult();
        }

        /// <summary>
        /// Revoke Access for a user
        /// </summary>
        /// <param name="ID">Employee ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/revokeaccess")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the EmployeeData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> RevokeAccess(short ID)
        {

            EntityActionChangeResponse<short> resp = await this._service.RevokeAccess(ID, _options);

            if (!resp.HasError)
                return resp.ToResult();

            if (resp.ErrorMessage.Contains("unauthorized", StringComparison.CurrentCultureIgnoreCase))
                return Unauthorized();

            if (resp.ErrorMessage.Contains("not found", StringComparison.CurrentCultureIgnoreCase))
                return NotFound(resp.ErrorMessage);

            if (resp.ErrorMessage.Contains("already exists", StringComparison.CurrentCultureIgnoreCase))
                return BadRequest(resp.ErrorMessage);

            return StatusCode(500, resp.ErrorMessage);
        }

        /// <summary>
        /// Send reset password email
        /// </summary>
        /// <param name="ID">Employee ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/sendresetpasswordemail")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the EmployeeData does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<IActionResult> SendResetPasswordEmail(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SendResetPasswordEmail(ID, _options);

            if (!resp.HasError)
                return resp.ToResult();

            if (resp.ErrorMessage.Contains("unauthorized", StringComparison.CurrentCultureIgnoreCase))
                return Unauthorized();

            if (resp.ErrorMessage.Contains("not found", StringComparison.CurrentCultureIgnoreCase))
                return NotFound(resp.ErrorMessage);

            if (resp.ErrorMessage.Contains("already exists", StringComparison.CurrentCultureIgnoreCase))
                return BadRequest(resp.ErrorMessage);

            return StatusCode(500, resp.ErrorMessage);
        }

        /// <summary>
        /// links a Employee to a RightsGroupList
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="rightsGroupListID">RightsGroupList.ID</param>
        /// <returns></returns>
        [HttpPost("{employeeID}/action/setrightsgrouplist/{rightsGroupListID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<EntityActionChangeResponse<short>> SetRightsGroupList([FromRoute]short employeeID, [FromRoute]short rightsGroupListID)
        {
            var resp = await this._service.LinkRightsGroupList(employeeID, rightsGroupListID, true);

            return resp;
        }

        /// <summary>
        /// unlinks a Employee to a RightsGroupList
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="rightsGroupListID">RightsGroupList.ID</param>
        /// <returns></returns>
        [HttpPost("{employeeID}/action/unsetrightsgrouplist/{rightsGroupListID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<EntityActionChangeResponse<short>> UnsetRightsGroupList([FromRoute]short employeeID, [FromRoute]short rightsGroupListID)
        {
            var resp = await this._service.LinkRightsGroupList(employeeID, rightsGroupListID, false);

            return resp;
        }

        /// <summary>
        /// Sets the User Access type for Employee
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="userAccessType">UserAccessType</param>
        /// <returns></returns>
        [HttpPost("{employeeID}/action/setUserAccessType/{userAccessType}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<EntityActionChangeResponse<short>> UnsetRightsGroupList([FromRoute]short employeeID, [FromRoute]UserAccessType userAccessType)
        {
            var resp = await this._service.SetUserAccessType(employeeID, userAccessType);

            return resp;
        }

        /// <summary>
        /// gets notification info for the given employee
        /// </summary>
        /// <param name="mycurrentpage"></param>
        /// <returns></returns>
        [HttpGet("notificationinfo")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeNotificationsData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public Task<EmployeeNotificationsData> GetNotificationInfo([FromQuery]string mycurrentpage)
        {
            short? employeeID = this.User.EmployeeID();
            return this._service.GetTimeCardInfo(employeeID);
        }

        /// <summary>
        /// gets Time Card objects specific for this employee
        /// </summary>
        /// <param name="employeeID"></param>
        /// <param name="onlyOpen">Query parameters to return open or close timecards</param>
        /// <param name="startDT">Query parameters to identify start DateTime of timecards to pull</param>
        /// <param name="endDT">Query parameters to identify end DateTime of timecards to pull</param>
        /// <param name="includeDetails">Query parameters to return with details or not</param>
        /// <returns>Returns an array of TimeCard</returns>
        [HttpGet("{employeeID}/timecard")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(TimeCard[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetTimeCards(int employeeID, [FromQuery] bool? onlyOpen = false, [FromQuery] DateTime? startDT = null, [FromQuery] DateTime? endDT = null, [FromQuery] bool? includeDetails = false)
        {
            return new OkObjectResult(await this._service.GetTimeCards(employeeID, onlyOpen, startDT, endDT, includeDetails));
        }

        /// <summary>
        /// Gets Time Card Summary specific for this employee
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns>Returns an array of TimeCard</returns>
        [HttpGet("{employeeID}/timecardsummary")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<EmployeeTimeCardSummaryData>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<GenericResponse<EmployeeTimeCardSummaryData>> GetTimeCardSummary(int employeeID)
        {
            return await this._service.GetTimeCardSummary(employeeID);
        }

        /// <summary>
        /// Gets 5 most recent Time Card Statuses for this employee
        /// </summary>
        /// <param name="employeeID"></param>
        /// <returns>Returns an array of TimeCardDetailStatus</returns>
        [HttpGet("{employeeID}/recenttimecards")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<EmployeeTimeCardSummaryData>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<TimeCardDetailStatus[]> GetRecentTimeCards(int employeeID)
        {
            return await this._service.GetRecentTimeCardStatuses(employeeID);
        }

        /// <summary>
        /// Create a new TimeCard Object and Clocks the employee in for the day at the current time
        /// If any of the optional parameters are specified, also creates a TimeCardDetail Object with the specified information.
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="longitude">Longitude</param>
        /// <param name="orderItemStatusID">OrderItemStatusID</param>
        /// <param name="breakActivityID">BreakActivityID</param>
        /// <param name="timeClockActivityID">TimeClockActivityID</param>
        /// <param name="orderItemID">OrderItemID</param>
        /// <returns></returns>
        [HttpPost("{employeeID}/action/clockin")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.PreconditionFailed, description: "The employee already has an open TimeCard entry")]
        public async Task<IActionResult> ClockInEmployee(short employeeID, [FromQuery]decimal? latitude, [FromQuery]decimal? longitude, [FromQuery]short? orderItemStatusID, [FromQuery]short? breakActivityID, [FromQuery]short? timeClockActivityID, [FromQuery]short? orderItemID)
        {
            var resp = await this._service.ClockInEmployee(employeeID, latitude, longitude, orderItemStatusID, breakActivityID, timeClockActivityID, orderItemID);

            if (!resp.HasError)
                return resp.ToResult();

            if (resp.ErrorMessage.Contains("already has an open", StringComparison.CurrentCultureIgnoreCase))
                return StatusCode((int)HttpStatusCode.PreconditionFailed);

            if (resp.ErrorMessage.Contains("Only zero or one", StringComparison.CurrentCultureIgnoreCase))
                return BadRequest(resp.ErrorMessage);

            return StatusCode(500, resp.ErrorMessage);
        }

        /// <summary>
        /// Clocks the employee out for the day at the current time and closes the employees open TimeCard Object and any open TimeCardDetail Objects (there can be more than one).
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="longitude">Longitude</param>
        /// <returns></returns>
        [HttpPost("{employeeID}/action/clockout")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.PreconditionFailed, description: "The employee did not have an open TimeCard")]
        public async Task<IActionResult> ClockOutEmployee(short employeeID, [FromQuery]decimal? latitude, [FromQuery]decimal? longitude)
        {
            var resp = await this._service.ClockOutEmployee(employeeID, latitude, longitude, Request.Headers[ConnectionIDHeaderKey]);

            if (!resp.HasError)
                return resp.ToResult();

            if (resp.ErrorMessage.Contains("did not have an open", StringComparison.CurrentCultureIgnoreCase))
                return StatusCode((int)HttpStatusCode.PreconditionFailed);

            return StatusCode(500, resp.ErrorMessage);
        }

        /// <summary>
        /// Create a new TimeCardDetail Object at the current date and time with the specified information.
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="longitude">Longitude</param>
        /// <param name="orderItemStatusID">OrderItemStatusID</param>
        /// <param name="breakActivityID">BreakActivityID</param>
        /// <param name="timeClockActivityID">TimeClockActivityID</param>
        /// <param name="orderItemID">OrderItemID</param>
        /// <param name="dontCloseOtherTasks ">DontCloseOtherTasks </param>
        /// <returns></returns>
        [HttpPost("{employeeID}/action/clockontask")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.PreconditionFailed, description: "The employee did not have an open TimeCard.")]
        public async Task<IActionResult> ClockOnTaskEmployee(short employeeID, [FromQuery]decimal? latitude, [FromQuery]decimal? longitude, [FromQuery]short? orderItemStatusID, [FromQuery]short? breakActivityID, [FromQuery]short? timeClockActivityID, [FromQuery]short? orderItemID, [FromQuery]bool? dontCloseOtherTasks = false)
        {
            var resp = await this._service.ClockOnTaskEmployee(employeeID, latitude, longitude, orderItemStatusID, breakActivityID, timeClockActivityID, orderItemID);

            if (!resp.HasError)
                return resp.ToResult();

            if (resp.ErrorMessage.Contains("did not have an open", StringComparison.CurrentCultureIgnoreCase))
                return StatusCode((int)HttpStatusCode.PreconditionFailed);

            if (resp.ErrorMessage.Contains("Only zero or one", StringComparison.CurrentCultureIgnoreCase))
                return BadRequest(resp.ErrorMessage);

            return StatusCode(500, resp.ErrorMessage);
        }

        /// <summary>
        /// Closes any open TimeCardDetail Objects (there can be more than one) by setting the CloseDT to the current time.
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="longitude">Longitude</param>
        /// <returns></returns>
        [HttpPost("{employeeID}/action/clockofftask")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.PreconditionFailed, description: "The employee did not have an open TimeCard")]
        public async Task<IActionResult> ClockOffTaskEmployee(short employeeID, [FromQuery]decimal? latitude, [FromQuery]decimal? longitude)
        {
            var resp = await this._service.ClockOffTaskEmployee(employeeID, latitude, longitude);

            if (!resp.HasError)
                return resp.ToResult();

            if (resp.ErrorMessage.Contains("did not have an open", StringComparison.CurrentCultureIgnoreCase))
                return StatusCode((int)HttpStatusCode.PreconditionFailed);

            return StatusCode(500, resp.ErrorMessage);
        }


        /// <summary>
        /// Marks all the employee's message on the specified channel as 'Read'
        /// </summary>
        /// <param name="channelID">Endor.Models.MessageChannelType</param>
        /// <param name="employeeID">Employee.ID</param>
        [HttpPost("{employeeID}/messages/channel/{channelID}/action/markallread")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<EntityActionChangeResponse> MarkAllRead([FromRoute] short employeeID, [FromRoute]MessageChannelType channelID)
        {

            return await this._messageHeaderService.Value.MarkAllRead(channelID, employeeID, true, Request.Headers[ConnectionIDHeaderKey]);
        }
        

        /// <summary>
        /// Marks all the employee's message on the specified channel as 'Deleted'
        /// </summary>
        /// <param name="channelID">Endor.Models.MessageChannelType</param>
        /// <param name="employeeID">Employee.ID</param>
        [HttpPost("{employeeID}/messages/channel/{channelID}/action/deleteall")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<EntityActionChangeResponse> MarkAllDeleted([FromRoute] short employeeID, [FromRoute]MessageChannelType channelID)
        {

            return await this._messageHeaderService.Value.MarkAllDeleted(channelID, employeeID, true, Request.Headers[ConnectionIDHeaderKey]);
        }


        /// <summary>
        /// Retrieves the employee's messages on a specified channel.
        /// The 'employeeID' on the route will overwrite what is set on the message filter
        /// </summary>
        /// <param name="employeeID">Employee.ID</param>
        /// <param name="channelID">Endor.Models.MessageChannelType</param>
        /// <param name="includes"></param>
        /// <param name="filters"></param>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        [HttpGet("{employeeID}/messages/channel")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ICollection<Message>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]
        public async Task<ICollection<Message>> MessagesByChannel([FromRoute]short employeeID, 
            [FromQuery]MessageChannelType channelID,
            [FromQuery]MessageIncludes includes,
            [FromQuery]MessageFilters filters,
            [FromQuery]int skip = 0,
            [FromQuery]int take = 100)
        {
            filters.EmployeeID = employeeID;

            return await this._messageHeaderService.Value.GetFilteredMessages(channelID, includes, filters, skip, take);
            
        }

        /// <summary>
        /// GetEmployeeNotificationInfo
        /// </summary>
        /// <param name="EmployeeID">short</param>
        [HttpGet("notificationsinfo")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeNotificationInfo))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If Specified ID does not exist..")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "Invalid or missing credentials..")]        
        public async Task<Object> GetEmployeeNotificationInfo([FromQuery]short EmployeeID)
        {
            //https://corebridge.atlassian.net/browse/END-3048
            //#9
            return await this._service.GetEmployeeNotificationInfo(EmployeeID);
        }

        /// <summary>
        /// Returns all of the Employees without the included defaults
        /// </summary>
        /// <returns></returns>
        [HttpGet("list")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EmployeeList[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<List<EmployeeList>> List()
        {
            var resp = await this._service.GetEmployees();
            return resp;
        }

        /// <summary>
        /// Returns true | false based on whether the Employee can be deleted.
        /// </summary>
        /// <param name="ID">Employee ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Employee does not exist.")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Sets the specified email account as the default for the employee.
        /// </summary>
        /// <param name="ID">Employee's ID</param>
        /// <param name="emailAccountID">EmailAccount's ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setdefault/emailaccount/{emailAccountID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(BooleanResponse), description: "If the Employee or EmailAccount does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Forbidden, typeof(BooleanResponse), description: "Email Account is currently inactive or not private.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the Employee fails to set the Default EmailAccount.")]
        public async Task<IActionResult> SetDefaultEmailAccount(short ID, short emailAccountID)
        {
            return (await _service.SetDefaultEmailAccount(ID, emailAccountID)).ToResult();
        }

        /// <summary>
        /// Clears Employee default email account
        /// </summary>
        /// <param name="ID">EmailAccountData ID</param>
        /// <param name="employeeID">=</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/cleardefault/emailaccount")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(BooleanResponse), description: "If the Employee does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the Employee fails to clear the Default EmailAccount.")]
        public async Task<IActionResult> ClearDefaultEmailAccount(short ID)
        {
            return (await _service.ClearDefaultEmailAccount(ID)).ToResult();
        }

    }
}
