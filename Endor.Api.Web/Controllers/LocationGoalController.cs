﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Location Goal API Controller
    /// </summary>
    [Authorize]
    [Route("api/[Controller]")]
    public class LocationGoalController : CRUDController<LocationGoal, LocationGoalService, short>
    {

        /// <summary>
        /// Location API Controller constructor
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public LocationGoalController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the LocationGoals
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsolete")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return Task.FromResult((IActionResult)StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status405MethodNotAllowed));
        }

        /// <summary>
        /// Returns a single LocationGoal by ID
        /// </summary>
        /// <param name="ID">LocationGoal ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationGoal))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the LocationGoal does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await Task.FromResult((IActionResult)StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status405MethodNotAllowed));
        }

        /// <summary>
        /// Updates a single LocationGoal by ID
        /// </summary>
        /// <param name="ID">LocationGoal ID</param>
        /// <param name="update">Updated LocationGoal data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationGoal))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Location does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] LocationGoal update)
        {
            return await Task.FromResult((IActionResult)StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status405MethodNotAllowed));
        }

        /// <summary>
        /// Creates a new LocationGoal
        /// </summary>
        /// <param name="newModel">New LocationGoal data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationGoal))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Location creation fails")]
        public override async Task<IActionResult> Create([FromBody] LocationGoal newModel, [FromQuery] Guid? tempID = null)
        {
            return await Task.FromResult((IActionResult)StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status405MethodNotAllowed));
        }

        /// <summary>
        /// Clones an existing LocationGoal to a new LocationGoal
        /// </summary>
        /// <param name="ID">ID of the LocationGoal to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationGoal))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Location does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Location does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await Task.FromResult((IActionResult)StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status405MethodNotAllowed));

        }

        /// <summary>
        /// Deletes a LocationGoal by ID
        /// </summary>
        /// <param name="ID">ID of the LocationGoal to Delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the LocationGoal does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the LocationGoal does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(short ID)
        {
            return await Task.FromResult((IActionResult)StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status405MethodNotAllowed));
        }


        #endregion

        /// <summary>
        /// Get a list of LocationGoal based on supplied criteria
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <returns>Returns an array of Alert Definition</returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationGoal[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] LocationGoalFilter filters)
        {
            return new OkObjectResult(await this._service.GetWithFiltersAsync(filters));
        }

        /// <summary>
        /// Get location goal by month
        /// </summary>
        /// <param name="locationId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns>Returns an array of Alert Definition</returns>
        [HttpGet("{locationId}/{year}/{month}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(LocationGoal[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadMonthData(byte locationId, short year, byte month)
        {
            return new OkObjectResult(await this._service.GetLocationGoalByMonth(locationId,year,month));
        }

        /// <summary>
        /// Update location goal by month
        /// </summary>
        /// <param name="LocationID">Location ID</param>
        /// <param name="Year">LocationGoal Year</param>
        /// <param name="Month">LocationGoal Month</param>
        /// <param name="Budgeted">Optional. If supplied, the Budgeted amount is updated to this value. If not supplied, the existing Budgeted number retains its current value.</param>
        /// <param name="Actual">Optional. If supplied, the Actual amount is updated to this value. If not supplied, the existing Actual number retains its current value.</param>
        /// <returns>EntityActionChangeResponse</returns>
        [HttpPut("{LocationID}/{Year}/{Month}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> UpdateMonthData(byte LocationID, short Year, byte Month, [FromQuery] float? Budgeted, float? Actual)
        {
            var resp = await this._service.UpdateLocationGoalByMonth(LocationID, Year, Month, Budgeted, Actual);
            return resp.ToResult();
        }
    }
}
