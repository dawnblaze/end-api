﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Endor.Tenant;
using Endor.Models;
using Endor.Api.Web.Classes;
using System.Net;
using Swashbuckle.AspNetCore.SwaggerGen;
using Endor.Tasks;
using System.Linq;
using System.Linq.Expressions;
using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for APE
    /// </summary>
    [Route("api/devops")]
    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DevopsController : Controller
    {
        private readonly ApiContext _ctx;
        private readonly Lazy<DevopsService> _lazyService;
        private DevopsService _service { get { return _lazyService.Value; } }

        /// <summary>
        /// Api Endpoint for APE
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        /// <param name="taskQueuer">Task Queuer</param>
        public DevopsController(ApiContext context, ITenantDataCache cache, IMigrationHelper migrationHelper, ITaskQueuer taskQueuer = null)
        {
            this._ctx = context;
            migrationHelper.MigrateDb(_ctx);
            this._lazyService = new Lazy<DevopsService>(() => new DevopsService(context, User.BID().Value, cache, migrationHelper, taskQueuer, context));
        }

        /// <summary>
        /// Get Indexable Entities
        /// </summary>
        [HttpGet("reindex/entities")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid ClassType ID")]
        public async Task<List<ClassTypeEntity>> GetIndexableEntities()
        {
            try
            {
                List<ClassTypeEntity> result = await _service.GetIndexableEntityTypes();
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Reindex all
        /// </summary>
        [HttpGet("reindex")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid ClassType ID")]
        public async Task<IActionResult> Reindex()
        {
            try
            {
                var result = await _service.Reindex(0);
                return Ok(result);
            }
            catch (InvalidOperationException ie)
            {
                return BadRequest(ie.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Reindex a specific ClassTypeID
        /// </summary>
        [HttpGet("reindex/{ctid}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid ClassType ID")]
        public async Task<IActionResult> Reindex(int ctid)
        {
            try
            {
                var result = await _service.Reindex(ctid);
                return Ok(result);
            }
            catch (InvalidOperationException ie)
            {
                return BadRequest(ie.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Reindex a specific ClassTypeID for all BIDs
        /// </summary>
        [HttpGet("reindex/all/{ctid}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid ClassType ID")]
        public async Task<IActionResult> ReindexAllBID(int ctid)
        {
            if (!IsUserAuthorizedForAccessType(UserAccessType.SupportStaff))
                return Unauthorized();

            try
            {
                var result = await _service.ReindexAllBID(ctid);
                return Ok(result);
            }
            catch (InvalidOperationException ie)
            {
                return BadRequest(ie.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Checks the User's AccessType Claim and compares against the indicated UserAccessType
        /// </summary>
        /// <param name="accessType">UserAccessType enum to match or exceed for authorization</param>
        /// <returns></returns>
        private bool IsUserAuthorizedForAccessType(UserAccessType accessType)
        {
            
            var accessTypeClaim = User.Claims.FirstOrDefault(c => c.Type.Equals("accesstype"))?.Value;
            if (accessTypeClaim == null)
                return false;

            var userAccessType = Enum.Parse<UserAccessType>(accessTypeClaim);
            if (userAccessType >= accessType)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Populates tables with stored default values (BID -1)
        /// </summary>
        [HttpGet("copydefaultlog")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid ClassType ID")]
        public IActionResult RunCopyAllDefaults()
        {
            try
            {
                var result = _service.RunCopyAllDefaults();
                return Ok(result);
            }
            catch (InvalidOperationException ie)
            {
                return BadRequest(ie.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Clear CopyAllDefaults connection string log
        /// </summary>
        [HttpDelete("copydefaultlog")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid ClassType ID")]
        public IActionResult ResetCopyAllDefaults()
        {
            try
            {
                var result = _service.ResetCopyAllDefaults();
                return Ok(result);
            }
            catch (InvalidOperationException ie)
            {
                return BadRequest(ie.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}