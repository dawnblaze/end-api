﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Models;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Endor.Security;
using Endor.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    public partial class DashboardController
    {
        /// <summary>
        /// Sets the Dashboard to Active
        /// </summary>
        /// <param name="ID">Dashboard's ID</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">If the Dashboard does not exist</response>   
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        /// <response code="400">If the Dashboard does not exist or fails to set as active.</response>   
        [Route("{ID}/Action/SetActive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Dashboard does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Dashboard does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            return (await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey])).ToResult();
        }

        /// <summary>
        /// Sets the Dashboard to Inactive
        /// </summary>
        /// <param name="ID">Dashboard's ID</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">If the Dashboard does not exist</response>   
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        /// <response code="400">If the Dashboard does not exist or fails to set as inactive.</response>   
        [Route("{ID}/Action/SetInactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Dashboard does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Dashboard does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            return (await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey])).ToResult();
        }

        /// <summary>
        /// Clones the Dashboard and all of its Widgets
        /// </summary>
        /// <param name="ID">Dashboard's ID</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">If the Dashboard does not exist</response>   
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        /// <response code="400">If the Dashboard does not exist or fails to clone.</response>   
        [Route("{ID}/Action/Clone")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Dashboard does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Dashboard does not exist or fails to set as default.")]
        [Obsolete]
        public async Task<IActionResult> CloneAction(short ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Returns true | false based on whether the Dashboard can be deleted.
        /// </summary>
        /// <param name="ID">Company ID</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">If the Dashboard does not exist</response>   
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        /// <response code="400">If the Dashboard does not exist or the check fails.</response>   
        [Route("{ID}/Action/CanDelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Dashboard does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the Dashboard does not exist or the check fails.")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete(ID)).ToResult();
        }

    }
}
