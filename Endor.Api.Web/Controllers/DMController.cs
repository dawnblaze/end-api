﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Endor.AzureStorage;
using Microsoft.AspNetCore.Http;
using Endor.Logging.Client;

using Endor.Models;
using Endor.Security;
using Endor.Tenant;
using Endor.DocumentStorage.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.IO;
using Endor.EF;
using Endor.Api.Web.Services;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.RTM.Enums;
using Endor.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    //    GET /api/dm/{bucket}/{ctid}/{id}/
    //Description
    //Retrieves a file listing of the DM files in the documents bucket for the specified record.
    //URL Construction
    //The end point URL syntax is:
    //
    //GET ../api/dm/{bucket}/{ctid}/{id}//? Designation = { string }
    //POST../api/dm/{ bucket}/{ctid}/{id}/{Path/file}? Designation = { string }
    //POST../api/dm/{bucket}/{ctid}/template/{path/file}
    //POST /api/dm/{bucket}/{ctid}/static/{path/file}
    //PUT../api/dm/{ bucket}/{ctid}/{id}/{Path/file}//? Destination = { path / file }//&Designation={string}
    //PUT /api/dm/{bucket}/{ctid}/template/{path/file}
    //PUT /api/dm/{bucket}/{ctid}/static/{path/file}
    //DELETE ../api/dm/{bucket}/{ctid}/{id}/{path/file}
    //DELETE /api/dm/{bucket}/{ctid}/template/{path/file}
    //DELETE /api/dm/{bucket}/{ctid}/static/{path/file}
    //POST /api/dm/initialize/{ctid}/{id}
    //POST ../api/dm/initialize/{ctid}/{id}
    //POST /api/dm/move/{bucket}/{ctid}/{id}
    //POST /api/dm/copy/{bucket}/{ctid}/{id}
    //POST /api/dm/duplicate/{bucket}/{ctid}/{id}
    //POST /api/dm/delete/{bucket}/{ctid}/{id}
    //POST /api/dm/delete/all/{ctid}/{id}
    //GET /api/dm/zip/{bucket}/{ctid}/{id}
    //GET /api/dm/zip/all/
    //POST /api/dm/initialize/all/{ctid}/{id}
    //POST /api/dm/initialize/all/temp/{guid}/{ctid}
    //POST /api/dm/persist/all/temp/{guid}/{ctid}/{id}
    //POST ../api/dm/persist/all/temp/{guid}/{ctid}/{id}

    //Temporary Storage File Management
    //GET /api/dm/{bucket}/temp/{guid}
    //POST /api/dm/{bucket}/temp/{guid}/{path/file}
    //PUT /api/dm/{bucket}/temp/{guid}/{path/file}
    //DELETE /api/dm/{bucket}/temp/{guid}/{path/file}

    //Anonymous Access
    //GET /api/dm/filebytoken/{token}
    //POST /api/dm/gettoken/{bucket}/{ctid}/{id}/{path/file}
    //

    [Authorize]
    [Route("api/[controller]")]
    public partial class DMController : Controller
    {
        private const string bucket_ctid_id_route = "{bucket}/{ctid:int}/{id:int}";
        private const string bucket_ctid_folder_route = "{bucket}/{ctid:int}/shared/{classFolder}";
        private const string bucket_temp_guid_route = "{bucket}/temp/{guid:guid}";
        private const string pathAndFilename_segment = "/{*pathAndFilename}";
        /// <summary>
        /// Route for bucket and path name
        /// </summary>
        public const string bucket_ctid_id_pathAndFilename_route = bucket_ctid_id_route + pathAndFilename_segment;
        private const string bucket_ctid_folder_pathAndFilename_route = bucket_ctid_folder_route + pathAndFilename_segment;
        private const string bucket_temp_guid_pathAndFilename_route = bucket_temp_guid_route + pathAndFilename_segment;

        private readonly RemoteLogger _logger;
        private readonly ITenantDataCache _cache;
        private readonly ApiContext apiContext;
        private readonly IRTMPushClient _rtmClient;
        private readonly Lazy<UserLinkService> _userLinkService;
        /// <summary>
        /// task queuer for triggering indexes
        /// </summary>
        protected readonly ITaskQueuer _taskQueuer;

        /// <summary>
        /// ctor for a new dm controller
        /// </summary>
        /// <param name="apiContext"></param>
        /// <param name="logger"></param>
        /// <param name="tenantCache"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DMController(ApiContext apiContext, RemoteLogger logger, ITenantDataCache tenantCache, IMigrationHelper migrationHelper, IRTMPushClient rtmClient = null, ITaskQueuer taskQueuer = null)
        {
            this._logger = logger;
            this._cache = tenantCache;
            this.apiContext = apiContext;
            migrationHelper.MigrateDb(apiContext);
            this._rtmClient = rtmClient;
            this._taskQueuer = taskQueuer;
            this._userLinkService = new Lazy<UserLinkService>(() => new UserLinkService(apiContext, this.User.BID().Value,  rtmClient, taskQueuer, migrationHelper));
        }

        #region images
        /// <summary>
        /// Handles uploaded default images for entities(Route 1)
        /// </summary>
        /// <param name="ctid">ClassTypeID</param>
        /// <param name="id">ID</param>
        /// <param name="remoteurl">Guid</param>
        /// <returns></returns>
        /// <response code="200">If the upload is successful</response>
        /// <response code="401">If user does not have a BID or is not logged in</response>  
        /// <response code="400">If no files are included in the body/If multiple files are included in the body/If an error occurs during upload</response>  
        [HttpPost("{ctid:int}/{id:int}/defaultimage", Order = -1)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If the user has no userLinkID")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:
            @"
            If no files are included in the body
            If multiple files are included in the body
            If an error occurs during upload")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "If the upload is successful", type: typeof(DMItem))]
        public async Task<IActionResult> UploadDefaultImageRoute1(int? ctid, int? id, [FromQuery]string remoteurl = null)
        {
            var dmid = new DMID() { ctid = ctid, id = id };
            return await this.UploadDefaultImage(dmid, remoteurl);
        }

        /// <summary>
        /// Handles uploaded default images for entities(Route 2)
        /// </summary>
        /// <param name="guid">Guid</param>
        /// <param name="remoteurl">Guid</param>
        /// <returns></returns>
        /// <response code="200">If the upload is successful</response>
        /// <response code="401">If user does not have a BID or is not logged in</response>  
        /// <response code="400">If no files are included in the body/If multiple files are included in the body/If an error occurs during upload</response>  
        [HttpPost("temp/{guid:guid}/defaultimage", Order = -1)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If the user has no userLinkID")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:
            @"
            If no files are included in the body
            If multiple files are included in the body
            If an error occurs during upload")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "If the upload is successful", type: typeof(DMItem))]
        public async Task<IActionResult> UploadDefaultImageRoute2(Guid? guid, [FromQuery]string remoteurl = null)
        {
            var dmid = new DMID() { guid = guid };
            return await this.UploadDefaultImage(dmid, remoteurl);
        }

        private DocumentManager GetDefaultImageDocumentManager(DMID dmid)
        {
            return new DocumentManager(this._cache, new StorageContext(User.BID().Value, BucketRequest.Data, dmid));
        }

        private async Task<IActionResult> UploadDefaultImage(DMID dmid, string remoteurl = null)
        {
            if (User.UserLinkID() == null)
                return Unauthorized();

            try
            {
                IFormFileCollection files = Request.Form.Files;

                DMItem result = await GetDefaultImageDocumentManager(dmid).UploadDefaultImage(Request.Form.Files, GetUploaderID(), GetUploaderCTID(), remoteurl);

                if (result != null)
                {
                    if (dmid.IsEntityLevel)
                        await SaveHasImage(dmid, true);

                    return Ok(result);
                }
            }
            catch (ArgumentException argEx)
            {
                return BadRequest(argEx.Message);
            }
            catch (Exception ex)
            {
                await _logger.Error(User.BID().Value, "Error during image upload storage", ex);
                return BadRequest();
            }

            return BadRequest();
        }

        private async Task SaveHasImage(DMID dmid, bool state)
        {
            Type entityType = ModelToClassType.FromClassTypeID(dmid.ctid.Value);
            object convertedID = Convert.ChangeType(dmid.id, entityType.GetInterface("IAtom`1").GetGenericArguments()[0]);
            object entity = (await apiContext.FindAsync(entityType, User.BID().Value, convertedID));
            if (entity is IImageCandidate imageCandidate)
            {
                imageCandidate.HasImage = state;
                await this.SendRefreshMessage(imageCandidate);

                await apiContext.SaveChangesAsync();
            }
            else if (entity is OrderItemData item)
            {
                item.HasCustomImage = state;
                await this.SendRefreshMessage(item);
                await apiContext.SaveChangesAsync();
            }

            if (dmid.ctid.HasValue && dmid.id.HasValue)
                await this._taskQueuer.IndexModel(User.BID().Value, dmid.ctid.Value, dmid.id.Value);
        }

        private async Task SendRefreshMessage(object imageCandidate)
        {
            if(this._taskQueuer==null || this._rtmClient==null){
                return;
            }

            Type entityType = imageCandidate.GetType();
            var refreshEntity = new RefreshEntity(){
                BID = (short)entityType.GetProperty("BID").GetValue(imageCandidate),
                ClasstypeID = (int)entityType.GetProperty("ClassTypeID").GetValue(imageCandidate),
                ID = int.Parse(entityType.GetProperty("ID").GetValue(imageCandidate).ToString()),
                DateTime = DateTime.UtcNow,
                RefreshMessageType = RefreshMessageType.Change
            };
            var refreshMsg = new RefreshMessage(){
                RefreshEntities = new List<RefreshEntity>(){ refreshEntity }
            };
            await this._taskQueuer.IndexModel(refreshEntity.BID, refreshEntity.ClasstypeID, refreshEntity.ID);
            await this._rtmClient.SendRefreshMessage(refreshMsg);            
        }

        /// <summary>
        /// Removes uploaded default images for entities(Route 1)
        /// </summary>
        /// <param name="guid">Guid</param>
        /// <returns></returns>
        /// <response code="204">If the delete succeeded</response>
        /// <response code="401">If the user has no userLinkID</response>  
        /// <response code="400">If an error occurs during delete</response>  
        [HttpDelete("temp/{guid:guid}/defaultimage", Order = -1)]
        //[HttpDelete("{ctid:int}/{id:int}/defaultimage", Order = -1)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If the user has no userLinkID")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent, description: "If the delete succeeded")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If an error occurs during delete")]
        public async Task<IActionResult> DeleteDefaultImageRoute1(Guid? guid)
        {
            var dmid = new DMID() { guid = guid };
            return await this.DeleteDefaultImage(dmid);
        }

        /// <summary>
        /// Removes uploaded default images for entities(Route 2)
        /// </summary>
        /// <param name="ctid">ClassTypeID</param>
        /// <param name="id">ID</param>
        /// <returns></returns>
        /// <response code="204">If the delete succeeded</response>
        /// <response code="401">If the user has no userLinkID</response>  
        /// <response code="400">If an error occurs during delete</response>  
        [HttpDelete("{ctid:int}/{id:int}/defaultimage", Order = -1)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If the user has no userLinkID")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent, description: "If the delete succeeded")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If an error occurs during delete")]
        public async Task<IActionResult> DeleteDefaultImageRoute2(int? ctid, int? id)
        {
            var dmid = new DMID() { ctid = ctid, id = id};
            return await this.DeleteDefaultImage(dmid);
        }


        private async Task<IActionResult> DeleteDefaultImage(DMID dmid)
        {
            if (User.UserLinkID() == null)
                return Unauthorized();

            try
            {
                if (await GetDefaultImageDocumentManager(dmid).DeleteDefaultImage())
                {
                    await SaveHasImage(dmid, false);
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                await _logger.Error(User.BID().Value, "Error during delete default image", ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// Retrieves the storage url of the default image for an entity
        /// </summary>
        /// <param name="classtypeID">ClasstypeID</param>
        /// <param name="ID">ID</param>
        /// <returns></returns>
        /// <response code="200">An object with the URI of the new default image</response>
        [HttpGet("{classtypeID}/{ID}/defaultimageurl")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description:"An object with the URI of the new default image", type: typeof(BlobUrlResponse))]
        public async Task<IActionResult> GetDefaultImage(int classtypeID, int ID)
        {
            Uri redirectUri = await (await GetStorageClientAsync(User.BID().Value, User.AID())).GetDefaultImageUri(classtypeID, ID);

            return Ok(new BlobUrlResponse(redirectUri.AbsoluteUri));
        }

        /// <summary>
        /// Retrieves the storage url of the default image of an unsaved entity
        /// </summary>
        /// <param name="tempID">Guid</param>
        /// <returns></returns>
        /// <response code="200">An object with the URI of the new default image</response>
        [HttpGet("{tempID:guid}/defaultimageurl")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "An object with the URI of the new default image", type: typeof(BlobUrlResponse))]
        public async Task<IActionResult> GetTempDefaultImage(Guid tempID)
        {
            Uri redirectUri = await (await GetStorageClientAsync(User.BID().Value, User.AID())).GetTempDefaultImageUri(tempID);
            string query = System.Web.HttpUtility.UrlEncode(redirectUri.Query);
            UriBuilder builder = new UriBuilder(redirectUri)
            {
                Query = query
            };
            return Ok(new BlobUrlResponse(builder.Uri.AbsoluteUri));
        }


        #endregion

        /// <summary>
        /// A response with a single blobURL as a property
        /// </summary>
        public class BlobUrlResponse
        {
            /// <summary>
            /// empty ctor
            /// </summary>
            public BlobUrlResponse() { }

            /// <summary>
            /// default ctor
            /// </summary>
            /// <param name="url"></param>
            public BlobUrlResponse(string url)
            {
                this.BlobUrl = url;
            }
            /// <summary>
            /// The absolute URI of a blob
            /// </summary>
            [JsonProperty("blobUrl")]
            public string BlobUrl { get; set; }
        }

        /// <summary>
        /// Object for requesting at DM endpoints
        /// </summary>
        public class RequestModel : DMID
        {
            /// <summary>
            /// which bucket to target
            /// </summary>
            [JsonProperty("bucket")]
            public BucketRequest Bucket { get; set; }

            private string _pathAndFilename;
            /// <summary>
            /// getter/setter that handles URL encoding
            /// </summary>
            [JsonProperty("pathAndFilename")]
            public string PathAndFilename
            {
                get { return this._pathAndFilename; }
                set { this._pathAndFilename = value?.Replace("%2F", "/"); }
            }

            /// <summary>
            /// Get StorageContext of the given business ID
            /// </summary>
            /// <param name="bid">business ID</param>
            /// <param name="aid">association ID</param>
            /// <returns></returns>
            public IStorageContext GetStorageContext(short bid, short? aid = null)
            {
                return new StorageContext(bid, Bucket, new DMID()
                {
                    id = id,
                    ctid = ctid,
                    guid = guid,
                    classFolder = classFolder
                },aid);
            }

            /// <summary>
            /// Returns a new RequestModel with all the same property values of the instance
            /// </summary>
            /// <returns></returns>
            public RequestModel Clone()
            {
                return new RequestModel()
                {
                    Bucket = Bucket,
                    id = id,
                    ctid = ctid,
                    guid = guid,
                    classFolder = classFolder,
                    PathAndFilename = PathAndFilename
                };
            }
        }

        private async Task<EntityStorageClient> GetStorageClientAsync(short bid, short? aid = null)
        {
            return new EntityStorageClient((await _cache.Get(bid)).StorageConnectionString, bid, aid);
        }

        /// <summary>
        /// Gets the userlinkID from the User object
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when the User does not have an UserLinkID</exception>
        /// <returns></returns>
        private int GetUploaderID()
        {
            //if (User.EmployeeID().HasValue)
            //    return User.EmployeeID().Value;

            //if (User.ContactID().HasValue)
            //    return User.ContactID().Value;

            if (User.UserLinkID().HasValue)
                return User.UserLinkID().Value;

            throw new InvalidOperationException("a UserLinkID is required");
        }

        /// <summary>
        /// Gets the uploader CTID
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when the User does not have a UserLinkID</exception>
        /// <returns>The ClassTypeID of the User</returns>
        private int GetUploaderCTID()
        {
            //if (User.EmployeeID().HasValue)
            //    return (int)Models.ClassType.Employee;

            //if (User.ContactID().HasValue)
            //    return (int)Models.ClassType.Contact;

            if (User.UserLinkID().HasValue)
                return (int)Models.ClassType.UserLink;

            throw new InvalidOperationException("a UserLinkID is required");
        }
    }
}
