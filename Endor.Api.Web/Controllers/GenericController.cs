﻿using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Security;
using Endor.Tenant;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Generic Controller
    /// </summary>
    /// <typeparam name="M">Model object</typeparam>
    /// <typeparam name="S">Model Service</typeparam>
    /// <typeparam name="I">ID type</typeparam>
    public abstract class GenericController<M, S, I> : Controller
        where M : class
        where S : BaseGenericService
        where I : struct, IConvertible
    {
        /// <summary>
        /// Lazy loaded service
        /// </summary>
        protected readonly Lazy<S> _lazyService;
        /// <summary>
        /// Remote Logger
        /// </summary>
        protected RemoteLogger _logger;
        /// <summary>
        /// RTM Client
        /// </summary>
        protected IRTMPushClient _rtmClient;

        /// <summary>
        /// Migration Helper
        /// </summary>
        protected readonly IMigrationHelper _migrationHelper;

        /// <summary>
        /// RTM Client
        /// </summary>
        protected ITenantDataCache _cache;
        /// <summary>
        /// Model Service
        /// </summary>
        protected S _service { get { return _lazyService.Value; } }

        /// <summary>
        /// Generic Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public GenericController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
        {
            this._lazyService = AtomGenericService<M, I>.CreateService<S>(context, migrationHelper);
            this._logger = logger;
            this._rtmClient = rtmClient;
            this._migrationHelper = migrationHelper;
        }

        /// <summary>
        /// Generic Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="taskQueuer">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="cache">Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public GenericController(ApiContext context, RemoteLogger logger, ITaskQueuer taskQueuer, IRTMPushClient rtmClient, ITenantDataCache cache, IMigrationHelper migrationHelper)
        {
            this._lazyService = AtomGenericService<M, I>.CreateService<S>(context, logger, () => User.BID().Value, taskQueuer, rtmClient, cache, migrationHelper);
            this._logger = logger;
            this._rtmClient = rtmClient;
            this._cache = cache;
        }
    }
}
