﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;
using Swashbuckle.AspNetCore.Annotations;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// This is a Company Controller
    /// </summary>
    [Route("api/company")]
    public class CompanyController : CRUDController<CompanyData, CompanyService, int>, ISimpleListableViewController<SimpleCompanyData, int>
    {
        /// <summary>
        /// Listable service for Companies
        /// </summary>
        public ISimpleListableViewService<SimpleCompanyData, int> ListableService => this._service;

        /// <summary>
        /// Api Endpoint for Companys
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CompanyController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns all companies and any included child objects specified in query parameters
        /// </summary>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CompanyData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetCompanyAndChildren([FromQuery] CompanyIncludes includes)
        {
            return await base.ReadWith(includes);
        }

        /// <summary>
        /// Returns all company's ids in a link table /// </summary>
        /// <param name="contactID">Contact ID</param>
        /// <returns></returns>
        [HttpGet("Contact/{ContactID}/CompanyLinks")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CompanyContactLink[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetCompanyContactLinks(int contactID)
        {
            var resp = await this._service.GetCompanyContactLinks(contactID);
            if (resp == null)
                return new NotFoundResult();

            return new OkObjectResult(resp);
        }

        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CompanyData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetCompanyAndChildrenById(int ID, [FromQuery] CompanyIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the Companies
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Returns a single Company by ID
        /// </summary>
        /// <param name="ID">Company ID</param>
        /// <returns></returns>
        [HttpGet("{ID}/obsolete")]
        [Obsolete]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> ReadById(int ID)
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Returns true | false based on whether the Company can be deleted.
        /// </summary>
        /// <param name="ID">Company ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> CanDelete(int ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

        /// <summary>
        /// Updates a single Company by ID
        /// </summary>
        /// <param name="ID">Company ID</param>
        /// <param name="update">Updated Company data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CompanyData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Company does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Company does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] CompanyData update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new Company
        /// </summary>
        /// <param name="newModel">New Company data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CompanyData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Company creation fails")]
        public override async Task<IActionResult> Create([FromBody] CompanyData newModel, [FromQuery] Guid? tempID = null)
        {
            await this._service.PrepareModelForCreateAsync(newModel);
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Clones an existing Company to a new Company
        /// </summary>
        /// <param name="ID">ID of the Company to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(CompanyData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Company does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Company does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Deletes a Company by ID
        /// </summary>
        /// <param name="ID">ID of the Company to Delete</param>
        /// <returns></returns>
        [Obsolete]
        [HttpDelete("{ID}/Obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Company does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Company does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }

        /// <summary>
        /// Deletes a Company by ID
        /// </summary>
        /// <param name="ID">ID of the Company to Delete</param>
        /// <param name="force"></param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerOperationFilter(typeof(IgnoreHttpRequestParameterOperationFilter))]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Company does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the Company does not exist or the deletion fails.")]
        public async Task<IActionResult> DeleteWithForce(short ID, [FromQuery]bool force = false)
        {
            if (force)
            {
                var resp = await _service.ForceDeleteCompany(ID, User.UserLinkID().Value);
                return resp.ToResult();
            }

            return await base.Delete(ID);
        }

        /// <summary>
        /// Sets a Company to to Active
        /// </summary>
        /// <param name="ID">Company ID</param>
        /// <returns></returns>
        [Route("{ID}/action/SetActive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Company does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Company does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(int ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Company to Inactive
        /// </summary>
        /// <param name="ID">Company Id</param>
        /// <returns></returns>
        [Route("{ID}/Action/SetInactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the Company does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the Company does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(int ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /*
        /// <summary>
        /// Sets multiple Companys to Active
        /// </summary>
        /// <param name="ids">An array of Company Ids</param>
        /// <returns></returns>
        [Route("Action/SetActive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntitiesActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntitiesActionChangeResponse), description: "If the Company(s) do not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntitiesActionChangeResponse), description: "If the Company(s) do not exist or fail to set as active.")]
        public async Task<IActionResult> SetActive([FromBody] int[] ids)
        {
            EntitiesActionChangeResponse resp = await this._service.SetMultipleActive(ids, true);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets multiple Companys to inactive
        /// </summary>
        /// <param name="ids">An array of Company Ids</param>
        /// <returns></returns>
        [Route("Action/setinactive")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntitiesActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntitiesActionChangeResponse), description: "If the Company(s) do not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntitiesActionChangeResponse), description: "If the Company(s) do not exist or fail to set as inactive.")]
        public async Task<IActionResult> SetInActive([FromBody] int[] ids)
        {
            EntitiesActionChangeResponse resp = await this._service.SetMultipleActive(ids, false);
            return resp.ToResult();
        }
        */

        #endregion

        /// <summary>
        /// Gets a Simple list of Companies
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleCompanyData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleCompanyData[]> GetFilteredSimpleList([FromQuery] SimpleCompanyFilters filters, [FromQuery] int take = 0)
        {
            if (filters == null)
            {
                filters = new SimpleCompanyFilters();
            }
            if (!filters.IsAdHoc.HasValue)
            {
                filters.IsAdHoc = false;
            }
            return await this.GetSimpleList<SimpleCompanyData, int>(User.BID().Value, filters, take);
        }

        /// <summary>
        /// Gets a Simple list of Companies
        /// </summary>
        /// <returns></returns>
        [HttpGet("obseletesimplelist")]
        [Obsolete]
        public async Task<SimpleCompanyData[]> SimpleList()
        {
            return await this.GetFilteredSimpleList(null);
        }

        /// <summary>
        /// Sets a Company as a Client
        /// </summary>
        /// <param name="id">Company ID</param>
        /// <returns></returns>
        [Route("{id}/action/setclient")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<CompanyData>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<CompanyData>), description: "If the Company does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<CompanyData>), description: "If the Company does not exist or fails to set as Client.")]
        public async Task<IActionResult> SetClient([FromRoute]int id)
        {
            EntityActionChangeResponse resp = await this._service.SetClient(id, true);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Company as a Prospective Client
        /// </summary>
        /// <param name="id">Company ID</param>
        /// <returns></returns>
        [Route("{id}/action/setprospect")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<CompanyData>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<CompanyData>), description: "If the Company does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<CompanyData>), description: "If the Company does not exist or fails to set as Prospect.")]
        public async Task<IActionResult> SetProspect([FromRoute]int id)
        {
            EntityActionChangeResponse resp = await this._service.SetProspect(id, true);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets a Company as a Lead
        /// </summary>
        /// <param name="id">Company ID</param>
        /// <returns></returns>
        [Route("{id}/action/setlead/")]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<CompanyData>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(GenericResponse<CompanyData>), description: "If the Company does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<CompanyData>), description: "If the Company does not exist or fails to set as Lead.")]
        public async Task<IActionResult> SetLead([FromRoute]int id)
        {
            EntityActionChangeResponse resp = await this._service.SetLead(id, true);
            return resp.ToResult();
        }


        /// <summary>
        /// Try's to validate the data model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            CompanyData company = model as CompanyData;
            if (company == null)
            {
                this.ModelState.AddModelError("model", "model is not a Company.");
                return false;
            }

            if (String.IsNullOrWhiteSpace(company.Name))
            {
                this.ModelState.AddModelError("Name", "Name must contain characters.");
            }

            if (company.LocationID == 0)
            {
                this.ModelState.AddModelError("LocationID", "LocationID is required.");
            }
            else
            {
                if (!_service.IsValidLocation(company.LocationID))
                {
                    this.ModelState.AddModelError("LocationID", "Invalid LocationID.");
                }
            }

            if (prefix == "create")
            {
                if (company.RefundableCredit.GetValueOrDefault(0) != 0 || company.NonRefundableCredit.GetValueOrDefault(0) != 0)
                    this.ModelState.AddModelError("Company credit", "Company must have an empty or 0 value for credit when created");
            }

            return base.TryValidateModel(model, prefix);
        }
    }
}
