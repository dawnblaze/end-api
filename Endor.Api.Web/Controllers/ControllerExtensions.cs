﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Controller Extensions
    /// </summary>
    public static class ControllerExtensions
    {
        /// <summary>
        /// Get ApiValidationError
        /// </summary>
        public static BadRequestObjectResult ApiValidationError(this Controller self)
        {
            List<string> errors = self.ModelState.Values.SelectMany(t => t.Errors).Select(t => t.ErrorMessage).ToList();
            var errorResult = new { HasError = true, ErrorMessage = String.Join("\n", errors) };

            return self.BadRequest(errorResult);
        }

        /// <summary>
        /// Get ApiSQLError
        /// </summary>
        public static BadRequestObjectResult SQLSaveError(this Controller self, List<Exception> exceptions)
        {
            var errorResult = "Save failed";
            if (exceptions != null && exceptions.Count() > 0)
            {
                var first = exceptions.FirstOrDefault();
                if (first == null)
                {
                    //noop
                }
                else if (first.InnerException == null)
                {
                    errorResult = "INTERNAL ERROR: " + first.Message;
                }
                else
                {
                    errorResult = "INTERNAL ERROR: " + first.InnerException.Message;
                }
            }

            var Result = new { HasError = true, ErrorMessage = errorResult };
            return self.BadRequest(Result);
        }

    }
}
