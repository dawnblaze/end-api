﻿using Endor.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860


namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Task Test Controller
    /// </summary>
    [Route("[controller]")]
    public class TaskTestController : Controller
    {
        private ITaskQueuer _taskQueue;

        /// <summary>
        /// Task Test Constructor
        /// </summary>
        public TaskTestController(ITaskQueuer taskQueue)
        {
            _taskQueue = taskQueue;
        }

        /// <summary>
        /// Get By Class Type 
        /// </summary>
        // GET: api/tasktest/classtype?bid=x&ctid=Y
        [HttpGet("classtype")]
        public async Task<IActionResult> Get([FromQuery]short bid, [FromQuery]int ctid)
        {
            return new OkObjectResult(await _taskQueue.IndexClasstype(bid, ctid));
        }

        /// <summary>
        /// Test Task function
        /// </summary>
        // GET: api/tasktest/test
        [HttpGet("test")]
        public async Task<IActionResult> Test([FromQuery]short bid)
        {
            return new OkObjectResult(await _taskQueue.Test(bid));
        }
    }
}
