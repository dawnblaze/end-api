﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for AssemblyCategory
    /// </summary>
    [Authorize]
    [Route("api/AssemblyCategory")]
    public class AssemblyCategoryController : CRUDController<AssemblyCategory, AssemblyCategoryService, short>, ISimpleListableViewController<SimpleAssemblyCategory, short>
    {
        /// <summary>
        /// Returns all of the AssemblyCategory
        /// in simple list view
        /// </summary>
        /// <returns></returns>
        public ISimpleListableViewService<SimpleAssemblyCategory, short> ListableService => this._service;
        /// <summary>
        /// Api Endpoint for Columns
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public AssemblyCategoryController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns all of the AssemblyCategories
        /// can be filtered by IsActive
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AssemblyCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If AssemblyCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] AssemblyCategoryFilter filters)
        {
            return new OkObjectResult(await _service.GetWithFiltersAsync(filters));
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the AssemblyCategories
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsoletegetall")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AssemblyCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If AssemblyCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override async Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns a single AssemblyCategory by ID
        /// </summary>
        /// <param name="ID">AssemblyCategory ID</param>
        /// <returns></returns>
        [HttpGet("obselete/{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AssemblyCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If AssemblyCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Returns an individual Assembly category
        /// </summary>
        /// <param name="ID">AssemblyCategory.ID</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns>`AssemblyCategory`</returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AssemblyCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If AssemblyCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithIncludes(short ID, [FromQuery]AssemblyCategoryIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }

        /// <summary>
        /// Updates a single AssemblyCategory by ID
        /// </summary>
        /// <param name="ID">AssemblyCategory ID</param>
        /// <param name="update">Updated AssemblyCategory data model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AssemblyCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If AssemblyCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the AssemblyCategory does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] AssemblyCategory update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new AssemblyCategory
        /// </summary>
        /// <param name="newModel">Updated AssemblyCategory data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AssemblyCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the AssemblyCategories creation fails")]
        public override async Task<IActionResult> Create([FromBody] AssemblyCategory newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Deletes a AssemblyCategory by ID
        /// </summary>
        /// <param name="ID">ID of the AssemblyCategory to delete</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If AssemblyCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the AssemblyCategories does not exist or the deletion fails")]
        public override async Task<IActionResult> Delete(short ID)
        {
            await this._service.unlinkAllAsync(ID);
            return await base.Delete(ID);
        }

        /// <summary>
        /// Assembly Category Clone Endpoint
        /// </summary>
        /// <param name="ID">ID of record to be cloned</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<AssemblyCategory>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<AssemblyCategory>), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public override async Task<IActionResult> Clone(short ID)
        {
            try
            {
                return await DoClone(ID);
            }
            catch (Exception e)
            {
                GenericResponse<AssemblyCategory> genericResponse = new GenericResponse<AssemblyCategory>() { ErrorMessage = e.Message };
                return genericResponse.ToResult();
            }
        }

        #endregion Overridden CRUD Methods


        /// <summary>
        /// Returns true | false based on whether the AssemblyCategory can be deleted.
        /// </summary>
        /// <param name="ID">ID of the AssemblyCategory to delete</param>
        /// <returns></returns>
        [HttpGet("{ID}/action/candelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
#pragma warning disable CS1998
        public async Task<IActionResult> CanDelete([FromRoute]short ID)
#pragma warning restore CS1998
        {
            return (new BooleanResponse() { Success = true, Message = "CanDelete: true", Value = true }).ToResult();
        }

        /// <summary>
        /// Sets a AssemblyCategory to Active
        /// </summary>
        /// <param name="ID">AssemblyCategory ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the AssemblyCategory does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the AssemblyCategory does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets a AssemblyCategory to Inactive
        /// </summary>
        /// <param name="ID">AssemblyCategory ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<short>), description: "If the AssemblyCategory does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the AssemblyCategory does not exist or fails to set as inactive.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Gets a Simple list of AssemblyCategories
        /// </summary>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleAssemblyCategory[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleAssemblyCategory[]> FilteredSimpleList([FromQuery] SimpleListItemIsActiveFilter<SimpleAssemblyCategory, short> filters)
        {
            return await this.GetSimpleList(User.BID().Value, filters);
        }

        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsoletesimplelist")]
        [Obsolete]
        public Task<SimpleAssemblyCategory[]> SimpleList()
        {
            return this.FilteredSimpleList(null);
        }

        /// <summary>
        /// links a Assembly category to a Assembly part
        /// </summary>
        /// <param name="AssemblyCategoryID">AssemblyCategory.ID</param>
        /// <param name="assemblyDataID">AssemblyData.ID</param>
        /// <returns></returns>
        [HttpPost("{AssemblyCategoryID}/action/linkassembly/{assemblyDataID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> LinkAssembly([FromRoute]short AssemblyCategoryID, [FromRoute]int assemblyDataID)
        {
            return (await this._service.LinkAssembly(AssemblyCategoryID, assemblyDataID, true)).ToResult();
        }

        /// <summary>
        /// unlinks a assembly category from a assembly part
        /// </summary>
        /// <param name="AssemblyCategoryID">AssemblyCategory.ID</param>
        /// <param name="assemblyDataID">AssemblyData.ID</param>
        /// <returns></returns>
        [HttpPost("{AssemblyCategoryID}/action/unlinkassembly/{assemblyDataID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> UnLinkAssembly([FromRoute]short AssemblyCategoryID, [FromRoute]int assemblyDataID)
        {
            return (await this._service.LinkAssembly(AssemblyCategoryID, assemblyDataID, false)).ToResult();
        }
        /// <summary>
        /// Validates AssemblyCategory name if already exists
        /// </summary>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            var categoryModel = (AssemblyCategory)model;
            var existingNameOwners = this._service.ExistingNameOwners(categoryModel);
            if (existingNameOwners.Count() > 0)
            {
                this.ModelState.AddModelError("Name", $"A AssemblyCategory named {categoryModel.Name} already exists.");
            }

            return base.TryValidateModel(model, prefix);
        }
    }
}
