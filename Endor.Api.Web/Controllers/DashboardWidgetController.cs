﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Api.Web.Includes;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Route ends up as api/dashboard/
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public class DashboardWidgetController : CRUDController<DashboardWidgetData, DashboardWidgetService, int>
    {
        /// <summary>
        /// Dashboard controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public DashboardWidgetController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns the DashboardWidgets for the given filters
        /// </summary>
        /// <param name="filters">DashboardWidget Filters</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardWidgetData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetDashboardWidgetsWithFilter(DashboardWidgetFilter filters)
        {
            return new OkObjectResult(await this._service.GetWithFilters(filters));
        }

        /// <summary>
        /// Returns a DashboardWidget by its ID
        /// </summary>
        /// <param name="ID">DashboardWidget ID</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardWidgetData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetDashboardsWidgetsWithFilter(int ID)
        {
            var resp = await this._service.GetByID(ID);
            if (resp == null)
                return new NotFoundResult();
            else
                return new OkObjectResult(resp);
        }

        /// <summary>
        /// Returns DashboardWidgets from a query
        /// </summary>
        /// <param name="widgetID">Widget ID</param>
        /// <param name="forceRefresh">Force Refresh</param>
        /// <param name="widgetDefinitionID">Widget Definition ID</param>
        /// <param name="options">Dashboard Widget Options</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        [HttpGet("query")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardWidgetQueryResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetDashboardsWidgetsWithQuery([FromQuery] int? widgetID, [FromQuery] bool? forceRefresh, [FromQuery] short? widgetDefinitionID, [FromQuery] DashboardWidgetQueryOptions options)
        {
            try
            {
                DashboardWidgetQueryResponse result = await this._service.QueryDashboardWidget(widgetID, forceRefresh, widgetDefinitionID, options);

                if (result == null)
                    return NotFound();

                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Returns current location set time
        /// </summary>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        [HttpGet("locationtime")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DateTime))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetLocationTime()
        {
            try
            {
                var result = await this._service.GetCurrentLocationTime();

                if (result == null)
                    return NotFound();

                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        #region Overridden CRUD Methods

        /// <summary>
        /// Returns all of the DashboardWidgets
        /// </summary>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        [Obsolete]
        [HttpGet("obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardWidgetData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Read()
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns a single DashboardWidget by ID
        /// </summary>
        /// <param name="ID">DashboardWidget ID</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">If the DashboardWidget does not exist.</response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        [Obsolete]
        [HttpGet("{ID}/obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardWidgetData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the DashboardWidget does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Updates a single DashboardWidget by ID
        /// </summary>
        /// <param name="ID">DashboardWidget ID</param>
        /// <param name="update">Updated DashboardWidget data model</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">If the DashboardWidget does not exist.</response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        /// <response code="400">If the DashboardWidget does not exist or the update fails.</response>   
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardWidgetData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the DashboardWidget does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the DashboardWidget does not exist or the update fails.")]
        public override async Task<IActionResult> Update(int ID, [FromBody] DashboardWidgetData update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Creates a new DashboardWidget
        /// </summary>
        /// <param name="newModel">New DashboardWidget data model</param>
        /// <param name="tempID">Temporary guid referencer</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        /// <response code="400">If the DashboardWidget creation fails</response>   
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardWidgetData))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the DashboardWidget creation fails")]
        public override async Task<IActionResult> Create([FromBody] DashboardWidgetData newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        /// <summary>
        /// Deletes a DashboardWidget by ID
        /// </summary>
        /// <param name="ID">ID of the DashboardWidget to Delete</param>
        /// <returns></returns>
        /// <response code="204"></response>
        /// <response code="404">If the DashboardWidget does not exist.</response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        /// <response code="400">If the DashboardWidget does not exist or the deletion fails.</response>   
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the DashboardWidget does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the DashboardWidget does not exist or the deletion fails.")]
        public override async Task<IActionResult> Delete(int ID)
        {
            return await base.Delete(ID);
        }

        #endregion

        #region actions

        /// <summary>
        /// Clones an existing DashboardWidget to a new DashboardWidget
        /// </summary>
        /// <param name="ID">ID of the DashboardWidget to clone from</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">If the source DashboardWidget does not exist.</response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        /// <response code="400">If the source DashboardWidget does not exist or the cloning fails.</response>   
        [HttpPost("{ID}/action/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardWidgetData))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source DashboardWidget does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source DashboardWidget does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await base.Clone(ID);
        }


        /// <summary>
        /// Returns true | false based on whether the DashboardWidget can be deleted.
        /// </summary>
        /// <param name="ID">DashboardWidget ID</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">IfIf the source DashboardWidget does not exist.</response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        /// <response code="400">If the action was unsuccessful.</response>   
        /// <response code="500">If an error occurs.</response>   
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the DashboardWidget does not exist.")]
        public async Task<IActionResult> CanDelete(int ID)
        {
            return (await this._service.CanDelete((int)ID, User.UserLinkID())).ToResult();
        }

        /// <summary>
        /// Sets a DashboardWidget to Active
        /// </summary>
        /// <param name="ID">DashboardWidget ID</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">If the source DashboardWidget does not exist.</response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        /// <response code="400">If the DashboardWidget does not exist or fails to set as active.</response>   
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the DashboardWidget does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the DashboardWidget does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }

        /// <summary>
        /// Sets an DashboardWidget to Inactive
        /// </summary>
        /// <param name="ID">DashboardWidget ID</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">If the source DashboardWidget does not exist.</response>
        /// <response code="401">If user does not have a BID or is not logged in</response>   
        /// <response code="400">If the DashboardWidget does not exist or fails to set as inactive.</response>   
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<int>))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse<int>), description: "If the DashboardWidget does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<int>), description: "If the DashboardWidget does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<int> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);
            return resp.ToResult();
        }
        #endregion

    }
}
