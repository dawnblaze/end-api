﻿using Endor.Api.Web.Services;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Gets the list of Columns for the given CTID
    /// </summary>
    /// <returns></returns>
    [Authorize]
    [Route("api/list/{CTID}/[controller]")]
    public class CriteriaController : Controller
    {
        private readonly ApiContext ctx;

        /// <summary>
        /// Api Endpoint for Criteria
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public CriteriaController(ApiContext context, IMigrationHelper migrationHelper)
        {
            this.ctx = context;
            migrationHelper.MigrateDb(ctx);
        }

        /// <summary>
        /// Returns a list of SystemListFilterCriteria base on SystemListFilterCriteria.TargetClassTypeID
        /// </summary>
        /// <param name="CTID">used for matching SystemListFilterCriteria.TargetClassTypeID</param>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SystemListFilterCriteria[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the parameter CTID did not match any SystemListFilterCriteria.TargetClassTypeID")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> Read(int CTID)
        {
            List<SystemListFilterCriteria> criteria = await this.ctx.SystemListFilterCriteria.Where(x => x.TargetClassTypeID == CTID).ToListAsync();
            if (criteria == null)
                return new BadRequestResult();
            else
                return new OkObjectResult(criteria);
        }
    }
}
