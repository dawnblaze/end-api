﻿using System;
using System.Threading.Tasks;
using Endor.Api.Web.Services;
using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.Security;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// UserDraftController
    /// </summary>
    [Route("api/draft")]
    [Authorize]
    public class UserDraftController : CRUDController<UserDraft, UserDraftService, int>
    {


        /// <inheritdoc />
        /// <summary>
        /// constructor
        /// </summary>
        public UserDraftController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Get a list of UserDraft based on supplied criteria
        /// </summary>
        /// <param name="filters">A object that contains supplied filters</param>
        /// <returns>Returns an array of UserDraft</returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(UserDraft[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetUserDraftWithFilters([FromQuery] UserDraftFilters filters)
        {
            return new OkObjectResult(await this._service.GetWithFiltersAsync(filters, User.UserID()));
        }

        /// <summary>
        /// Returns all the UserDraft
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(DashboardData[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [Obsolete]
        public override async Task<IActionResult> Read()
        {
            return await base.Read();
        }

        /// <summary>
        /// Returns a UserDraft by ID
        /// </summary>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(UserDraft[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await this._service.GetUserDraftByID(ID);
        }
    }
}
