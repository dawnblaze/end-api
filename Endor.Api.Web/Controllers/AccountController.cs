﻿// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.EF;
using Endor.Models;
using Endor.Security;
using Endor.Tenant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Endpoints for getting info on your user
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public class AccountController : Controller
    {
        private readonly ITenantDataCache _tenantCache;
        private readonly ApiContext _ctx;

        /// <summary>
        /// Ctor for Endpoints for getting info on your user
        /// </summary>
        /// <param name="tenantCache"></param>
        /// <param name="ctx"></param>
        /// <param name="migrationHelper"></param>
        public AccountController(ITenantDataCache tenantCache, ApiContext ctx, IMigrationHelper migrationHelper)
        {
            _tenantCache = tenantCache;
            _ctx = ctx;

            migrationHelper.MigrateDb(_ctx);
        }
        
        /// <summary>
        /// Gets employee data for currently logged in user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HttpOptions]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(UserExtendedDetail))]
        [SwaggerCustomResponse((int)HttpStatusCode.Forbidden, description:"If user does not have BID and UserID is not found")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description:"If user does not have a BID or is not logged in")]
        public async Task<IActionResult> Get()
        {
            if (User.BID().HasValue)
            {
                UserLink user = await _ctx.UserLink.Include(x => x.Employee).Include(x => x.Contact).FirstOrDefaultAsync(x => x.BID == User.BID().Value && x.AuthUserID == User.UserID().Value);
                if (user != null)
                {
                    if (user.UserName == null)
                    {
                        user.UserName = User.UserName();
                    }

                    UserExtendedDetail userExtendedDetail = new UserExtendedDetail(user);

                    if (user.RightsGroupListID != null)
                    {
                        var rightsGroups = await _ctx.RightsGroupListRightsGroupLink
                            .Include(x => x.RightsGroup)
                            .Where(x => x.BID == User.BID().Value && x.ListID == user.RightsGroupListID)
                            .Select(x => x.RightsGroup)
                            .ToListAsync();

                        userExtendedDetail.RightsGroups = rightsGroups;
                    }

                    //https://corebridge.atlassian.net/browse/END-1851
                    return Ok(userExtendedDetail);
                }
                else
                {
                    return Forbid();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        /// <summary>
        /// Returns OK if token is valid
        /// </summary>
        /// <returns></returns>
        [HttpGet("validate")]
        [HttpOptions("validate")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description:"Always")]
        public IActionResult ValidateToken()
        {
            return Ok();
        }
    }
}
