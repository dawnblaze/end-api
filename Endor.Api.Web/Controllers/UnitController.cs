﻿using Endor.Api.Web.Annotation;
using Endor.EF;
using Endor.Models;
using Endor.Security;
using Endor.Tenant;
using Endor.Units;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Endpoints for getting unit related info
    /// </summary>
    [Route("Api/System/")]
    [Authorize]
    public class UnitController : Controller
    {
        private readonly ITenantDataCache _tenantCache;
        private readonly ApiContext _ctx;

        /// <summary>
        /// Ctor for Endpoints for getting info on your user
        /// </summary>
        /// <param name="tenantCache"></param>
        /// <param name="ctx"></param>
        /// <param name="migrationHelper"></param>
        public UnitController(ITenantDataCache tenantCache, ApiContext ctx, IMigrationHelper migrationHelper)
        {
            _tenantCache = tenantCache;
            _ctx = ctx;

            migrationHelper.MigrateDb(_ctx);
        }

        /// <summary>
        /// Returns JSON Collection of `Unit` Records.
        /// </summary>
        /// <param name="unitType">Unit Type</param>
        /// <param name="unitSystem">Unit System (Metric|Imperial|MetricAndImperial)</param>
        /// <returns></returns>
        [HttpGet("UnitInfo")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(List<UnitInfo>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
#pragma warning disable CS1998
        public async Task<IActionResult> Get([FromQuery] UnitType? unitType, [FromQuery] SystemOfUnits? unitSystem)
        {
            if (User.BID().HasValue)
            {
                List<UnitInfo> unitInfoList = null;
                if (unitType.HasValue && unitSystem.HasValue)
                    unitInfoList = UnitInfo.InfoListByUnitType(unitType.Value, unitSystem.Value);
                else if (unitType.HasValue && !unitSystem.HasValue)
                    unitInfoList = UnitInfo.InfoListByUnitType(unitType.Value);
                else if (!unitType.HasValue && unitSystem.HasValue)
                    unitInfoList = UnitInfo.InfoListBySystem(unitSystem.Value);
                else
                    unitInfoList = UnitInfo.InfoList;

                return new OkObjectResult(unitInfoList);
            }
            else
            {
                return Unauthorized();
            }
        }
#pragma warning restore CS1998

        /// <summary>
        /// Returns the JSON for a single UnitInfo Object.
        /// </summary>
        /// <param name="unitID">Unit ID</param>
        /// <returns></returns>
        [HttpGet("UnitInfo/{unitID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(UnitInfo))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
#pragma warning disable CS1998
        public async Task<IActionResult> GetByID(Unit unitID)
        {
            if (User.BID().HasValue)
            {
                return new OkObjectResult(unitID.Info());
            }
            else
            {
                return Unauthorized();
            }
        }
#pragma warning restore CS1998

        /// <summary>
        /// Returns JSON Collection of all UnitTypeInfo Objects.
        /// </summary>
        /// <returns></returns>
        [HttpGet("UnitTypeInfo")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(List<UnitTypeInfo>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
#pragma warning disable CS1998
        public async Task<IActionResult> GetUnitTypeInfo()
        {
            if (User.BID().HasValue)
            {
                return new OkObjectResult(UnitTypeInfo.InfoList);
            }
            else
            {
                return Unauthorized();
            }
        }
#pragma warning restore CS1998

        /// <summary>
        /// Returns JSON Collection of all UnitTypeInfo Objects.
        /// </summary>
        /// <param name="unitTypeID">Unit Type ID</param>
        /// <returns></returns>
        [HttpGet("UnitTypeInfo/{unitTypeID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(List<UnitInfo>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
#pragma warning disable CS1998
        public async Task<IActionResult> GetUnitTypeInfoByID(UnitType unitTypeID)
        {
            if (User.BID().HasValue)
            {
                return new OkObjectResult(UnitInfo.InfoListByUnitType(unitTypeID));
            }
            else
            {
                return Unauthorized();
            }
        }
#pragma warning restore CS1998

    }
}