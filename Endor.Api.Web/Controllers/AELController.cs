﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Endor.Models;
using Endor.Tenant;
using Endor.Security;
using Microsoft.AspNetCore.Authorization;
using Endor.EF;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.AEL;
using Endor.Api.Web.Classes;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Endpoints for getting info on your user
    /// </summary>
    [Route("api/ael")]
    [Authorize]
    public class AELController : Controller
    {
        private readonly ITenantDataCache _tenantCache;
        private readonly ApiContext _ctx;
        private readonly EtagCache _eTagCache;

        /// <summary>
        /// Ctor for Endpoints for getting info on your user
        /// </summary>
        /// <param name="tenantCache"></param>
        /// <param name="ctx"></param>
        /// <param name="etagCache"></param>
        /// <param name="migrationHelper"></param>
        public AELController(ITenantDataCache tenantCache, ApiContext ctx, EtagCache etagCache, IMigrationHelper migrationHelper)
        {
            _tenantCache = tenantCache;
            _ctx = ctx;
            _eTagCache = etagCache;
            migrationHelper.MigrateDb(_ctx);
        }
        
        /// <summary>
        /// Returns JSON Collection of `DataTypeInfo` Records.
        /// </summary>
        [HttpGet("datatype")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(Dictionary<string, DataTypeInfo>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetDataTypeDictionary()
        {
            var result = _eTagCache.GetResult(EtagKey.AELDataTypeDictionary, User.BID(), Request, Response, () => DataTypeDictionary.Instance);
            if (result == null) return await Task.FromResult(StatusCode((int)HttpStatusCode.NotModified));
            else return await Task.FromResult(Ok(result));
        }

        /// <summary>
        /// Returns a JSON Dictionary of (DataType, MemberInfo[])
        /// </summary>
        [HttpGet("member")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(Dictionary<string, List<IMemberInfo>>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetBusinessMemberDictionary([FromQuery]DataType? dataType = null)
        {
            var result = _eTagCache.GetResult(EtagKey.AELBusinessMemberDictionary, User.BID(), Request, Response, () =>
                            BusinessMemberDictionary
                                    .MemberDictionaryByDataType(new DataProvider(User.BID().Value, _ctx))
                                    .Where(dictionaryEntry => dictionaryEntry.Key == (dataType.HasValue ? dataType.Value : dictionaryEntry.Key))
                                    .ToDictionary(item => item.Key, item => item.Value));
                                    
            if (result == null) return await Task.FromResult(StatusCode((int)HttpStatusCode.NotModified));
            else return await Task.FromResult(Ok(result));
        }

        /// <summary>
        /// Returns JSON Collection of all `Operator` Records.
        /// </summary>
        [HttpGet("operator")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(Dictionary<string, OperatorInfo>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> GetOperatorDictionary()
        {
            var result = _eTagCache.GetResult(EtagKey.AELOperatorDictionary, User.BID(), Request, Response, () => OperatorDictionary.Instance);
            if (result == null) return await Task.FromResult(StatusCode((int)HttpStatusCode.NotModified));
            else return await Task.FromResult(Ok(result));

        }

    }
}
