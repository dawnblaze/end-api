﻿using Endor.Api.Web.Services;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Endor.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using System.Net;

namespace Endor.Api.Web.Controllers
{
    
    /*/// <summary>
    /// Gets the list of Columns for the given CTID
    /// </summary>
    /// <returns></returns>
    [Authorize]
    [Route("api/system/[controller]")]
    public class MessageTemplateTypeController : Controller
    {
        private readonly ApiContext _ctx;
        private readonly Lazy<MessageTemplateTypeService> _lazyService;
        private MessageTemplateTypeService _service { get { return _lazyService.Value; } }

        /// <summary>
        /// Api Endpoint for MessageTemplateType
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="migrationHelper"></param>
        public MessageTemplateTypeController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, IMigrationHelper migrationHelper)
        {
            this._ctx = context;
            migrationHelper.MigrateDb(_ctx);
            this._lazyService = new Lazy<MessageTemplateTypeService>(() => new MessageTemplateTypeService(context, User.BID().Value, migrationHelper));
        }

        /// <summary>
        /// Returns all of the SystemMessageTemplateType.
        /// Can be filtered by AppliesClasstypeID and ChannelType
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SystemMessageTemplateType[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] MessageTemplateTypeFilter filters)
        {
            return new OkObjectResult(await this._service.GetWithFiltersAsync(filters));
        }

        /// <summary>
        /// Returns a simple list of all of the SystemMessageTemplateType.
        /// Can be filtered by AppliesClasstypeID and ChannelType
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        [Route("simplelist")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleListItem<byte>[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> FilteredSimpleList([FromQuery] MessageTemplateTypeFilter filters)
        {
            return new OkObjectResult(await this._service.GetSimpleListWithFiltersAsync(filters));
        }

    }*/
}
