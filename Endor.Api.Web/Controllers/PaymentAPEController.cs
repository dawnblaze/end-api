﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Endor.Tenant;
using System.Net.Http;
using Endor.Api.Web.Classes;

using Endor.Api.Web.Annotation;
using Endor.Logging.Client;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Api Endpoint for APE
    /// </summary>
    [Route("api/payment/ape")]
    [Authorize]
    public class PaymentAPEController : Controller
    {
        private readonly ApiContext _ctx;
        private readonly RemoteLogger _logger;
        private readonly Lazy<PaymentAPEService> _lazyService;
        private PaymentAPEService _service { get { return _lazyService.Value; } }

        /// <summary>
        /// Api Endpoint for APE
        /// </summary>
        /// <param name="context">Api Context</param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public PaymentAPEController(ApiContext context, RemoteLogger logger, ITenantDataCache cache, IMigrationHelper migrationHelper)
        {
            this._ctx = context;
            this._logger = logger;
            migrationHelper.MigrateDb(_ctx);
            this._lazyService = new Lazy<PaymentAPEService>(() => new PaymentAPEService(context, logger, User.BID().Value, cache, migrationHelper));
        }

        /// <summary>
        /// Get Registration
        /// </summary>
        [HttpGet("registration")]
        public async Task<IActionResult> Registration(string currency = "USD")
        {
            try
            {
                var registration = await _service.GetRegistrationAsync(currency);
                return Ok(registration);
            }
            catch (InvalidOperationException ie)
            {
                await this._logger.Error(User.BID().Value, ie.Message, ie);
                return BadRequest(ie.Message);
            }
            catch (Exception ex)
            {
                await this._logger.Error(User.BID().Value, $"Registration - {ex.Message}", ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete business key
        /// </summary>
        [Route("businessKey")]
        [HttpDelete]
        public async Task<IActionResult> DeleteBusinessKey()
        {
            await _service.DeleteBusinessKey();
            return Ok();
        }

        /// <summary>
        /// Get Payment Page For Single Orders
        /// </summary>
        [HttpGet("paymentpage")]
        public async Task<IActionResult> SingleOrderPaymentPage([FromQuery] string RegistrationID, [FromQuery] int OrderID, [FromQuery] decimal Amount, [FromQuery] bool CanAlterAmount, [FromQuery] decimal MinAmount, [FromQuery] decimal MaxAmount)
        {
            try
            {
                APIPaymentPageRequest paymentPageRequest = new APIPaymentPageRequest();
                paymentPageRequest.RegistrationID = RegistrationID;
                paymentPageRequest.Amount = Amount;
                paymentPageRequest.MinAmount = MinAmount;
                paymentPageRequest.MaxAmount = MaxAmount;
                paymentPageRequest.Orders = new APIPaymentPageRequestOrders[1];
                paymentPageRequest.Orders[0] = new APIPaymentPageRequestOrders();
                paymentPageRequest.Orders[0].OrderID = OrderID;
                paymentPageRequest.Orders[0].Amount = Amount;
                paymentPageRequest.CanAlterAmount = CanAlterAmount;
                
                var registration = await _service.GetPaymentPage(paymentPageRequest);
                return Ok(registration);
            }
            catch (KeyNotFoundException ex)
            {
                await this._logger.Error(User.BID().Value, ex.Message, ex);
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                await this._logger.Error(User.BID().Value, $"SingleOrderPaymentPage - { ex.Message}", ex);
                return BadRequest($"Payment Integration Error:  {ex.Message}");
            }
        }

        /// <summary>
        /// Get Payment Page For Multiple Orders
        /// </summary>
        [HttpPost("paymentpage")]
        public async Task<IActionResult> MultipleOrdersPaymentPage([FromBody] APIPaymentPageRequest paymentPageRequest)
        {
            try
            {
                var registration = await _service.GetPaymentPage(paymentPageRequest);
                return Ok(registration);
            }
            catch (KeyNotFoundException ex)
            {
                await this._logger.Error(User.BID().Value, ex.Message, ex);
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                await this._logger.Error(User.BID().Value, $" MultipleOrdersPaymentPage - {ex.Message}", ex);
                return BadRequest($"Payment Integration Error : {ex.Message}");
            }
        }
    }

    /// <summary>
    /// Api Endpoint for APE
    /// </summary>
    [Route("api/payment/ape/test")]
    public class PaymentAPEControllerTest : Controller
    {
        /// <summary>
        /// Get Payment Page Test Harness
        /// </summary>
        [HttpGet]
        public ContentResult PaymentPageTest()
        {
            string style = "<style>body{ padding: 50px; color:#333333; font-size:small; font-family: Verdana, Arial; }</style>";
            string script = @"
<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script>
	var paymentPath = './paymentpage';
	
	function sendSingleTestPayment() {
		$(document).ready(function () {
			var registrationID = document.getElementById('registrationID').value;
			var orderID = document.getElementById('orderID').value;
			var amount = document.getElementById('amount').value;
			var authToken = document.getElementById('authToken').value;
		
			var request = new XMLHttpRequest();
			request.open('GET', paymentPath + '?RegistrationID=' + registrationID + '&OrderID=' + orderID + '&Amount=' + amount, true);
			request.setRequestHeader('Authorization', authToken);
			
			request.onreadystatechange=function()
			{
				if (request.readyState==4 && request.status==200)
				{
					var obj = JSON.parse(request.responseText);
					obj.paymentData['RegistrationID'] = registrationID;
					obj.paymentData['ID'] = 'tokenframe';
					
					obj.htmlCode = obj.htmlCode.replace('400', '700');
					obj.htmlCode = obj.htmlCode.replace('400', '700');
					
					$('#paymentTestHTMLCode').html(obj.htmlCode);
					$('#tokenframe').on('load', function(){
						console.log(JSON.stringify(obj.paymentData));
						this.contentWindow.postMessage(obj.paymentData, getHostname(this.src));
					});
				}
			}		
			request.send();
		});
	}

	function sendMultipleTestPayment() {
		$(document).ready(function () {
			var paymentRequest = document.getElementById('paymentRequest').value;
			var paymentRequestObj = JSON.parse(paymentRequest);
			var registrationID = paymentRequestObj.RegistrationID;
			var authToken = document.getElementById('authToken').value;
		
			var request = new XMLHttpRequest();
			request.open('POST', paymentPath, true);
			request.setRequestHeader('Authorization', authToken);
			request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
			
			request.onreadystatechange=function()
			{
				if (request.readyState==4 && request.status==200)
				{
					var obj = JSON.parse(request.responseText);
					obj.paymentData['RegistrationID'] = registrationID;
					obj.paymentData['ID'] = 'tokenframe';
					
					obj.htmlCode = obj.htmlCode.replace('400', '700');
					obj.htmlCode = obj.htmlCode.replace('400', '700');
					
					$('#paymentTestHTMLCode').html(obj.htmlCode);
					$('#tokenframe').on('load', function(){
						console.log(JSON.stringify(obj.paymentData));
						this.contentWindow.postMessage(obj.paymentData, getHostname(this.src));
					});
				}
			}		
			request.send(paymentRequest);
		});
	}

function getHostname(url) {
    var protocol;
	var hostname;
    if (url.indexOf('//') > -1) {
		protocol = url.split('/')[0];
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }
    hostname = hostname.split('?')[0];
    return protocol + '//' + hostname;
}
</script>
            ";
            string json = @"
{
  ""RegistrationID"" : ""guid"",
  ""Amount"" : ""number"",
  ""AmountToCredit"" : ""number"",
  ""CanAlterAmount"" : ""bool"",
  ""MinAmount"" : ""number"",
  ""MaxAmount"" : ""number"",
  ""Orders"" : [
      {""OrderID"" : ""number"", ""Amount"" : ""number""},
      {""OrderID"" : ""number"", ""Amount"" : ""number""},
      {""OrderID"" : ""number"", ""Amount"" : ""number""}
  ]
}
                    ";

            string body = String.Format(
@"

<html>
<head>
<title>Payment Test Harness</title>
{0}
{1}
</head>
<body>
<h1>Payment Test Harness</h1>

<table style='background-color:#eeeeee; padding:10px; font-size:12px; width:600px;'>
	<tr>
		<td width='150px;'>Authorization Token</td><td><input id='authToken' type='text' style='width:400px;' value=''></td>
	</tr>
</table>
<br /><br />
<table style='background-color:#eeeeee; padding:10px; font-size:12px; width:600px;'>
	<tr>
		<td width='150px;'>Registration ID</td><td><input id='registrationID' type='text' style='width:400px;' value=''></td>
	</tr>
	<tr>
		<td>Order ID</td><td><input id='orderID' type='text' value=''></td>
	</tr>
	<tr>
		<td>Amount </td><td><input id='amount' type='text' value=''></td>
	</tr>
	<tr><td colspan='2' style='padding:10px;'><button onclick='sendSingleTestPayment();'>Test Single Order Payment</button></td></tr>
</table>
<br /><br />
<table style='background-color:#eeeeee; padding:10px; font-size:12px; width:600px;'>
	<tr>
		<td width='150px;'>Payment Request</td>
		<td><textarea id='paymentRequest' rows='4' cols='50' style='width:400px; height:200px;'>
{2}
		</textarea></td>
		<tr><td colspan = '2' style='padding:10px;'><button onclick = 'sendMultipleTestPayment();' > Test Multiple Order Payment</button></td></tr>
	</tr>
</table>
<br /><br />
<br /><br />
<span id = 'paymentTestHTMLCode' ></ span >

</ body >
</ html >

", style, script, json);

            return new ContentResult
            {
                ContentType = "text/html",
                StatusCode = (int)System.Net.HttpStatusCode.OK,
                Content = body
            };
        }

    }
}