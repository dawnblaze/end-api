﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Endor.Api.Web.Annotation;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Classes.Request;
using Endor.Api.Web.Classes.Responses;
using Endor.Api.Web.Includes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.GLEngine.Classes;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Endor.Api.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReconciliationController : CRUDController<Reconciliation, ReconciliationService, int>, ISimpleListableController<SimpleReconciliation, int>
    {
        public ReconciliationController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        #region Overriden routes

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [HttpGet("obsolete")]
        [Obsolete("Use ReadWithFilters instead")]
        public override Task<IActionResult> Read()
        {
            return this.ReadWithFilters(null);
        }


        /// <summary>
        /// unused simple list method, required for type structure
        /// </summary>
        /// <returns></returns>
        [Route("obsoletesimplelist")]
        [Obsolete]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleReconciliation[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public Task<SimpleReconciliation[]> SimpleList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [Route("obsoletepost")]
        [Obsolete]
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleReconciliation[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Create([FromBody] Reconciliation newModel, [FromQuery] Guid? tempID = null)
        {
            return await DoCreate(newModel, tempID);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [Route("obsoleteput")]
        [Obsolete]
        [HttpPut]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleReconciliation[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override Task<IActionResult> Update(int ID, [FromBody] Reconciliation update)
        {
            throw new NotImplementedException();
        }

        [HttpPost("{ID}/obsoleteclone")]
        [Obsolete]
        public override async Task<IActionResult> Clone(int ID)
        {
            return await DoClone(ID);
        }
        #endregion

        /// <summary>
        /// Demarc the end of an accounting period and the start of a new accounting period
        /// </summary>
        /// <param name="LocationID">Location ID to reconcile</param>
        /// <param name="AccountingDT">Accounting Date</param>
        /// <param name="CreateAdjustments">Should create Adjustments to reconciliation</param>
        /// <param name="request"></param>
        /// /// <summary>
        /// Location ID to reconcile
        /// </summary>
        public byte? LocationID;

        /// <summary>
        /// Accounting Date
        /// </summary>
        public DateTime? AccountingDT;

        /// <summary>
        /// Should create Adjustments to reconciliation
        /// </summary>
        public bool CreateAdjustments;
        /// <returns></returns>
        [HttpPost("reconcile")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK)]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> Reconcile([FromQuery]byte? LocationID, [FromQuery]DateTime? AccountingDT, [FromQuery]bool CreateAdjustments)
        {
            try
            {
                string body;
                using (var reader = new StreamReader(Request.Body, Encoding.UTF8, true, 1024, true))
                {
                    body = await reader.ReadToEndAsync();
                }
                var cashDrawerAdjustments = JsonConvert.DeserializeObject<List<CashDrawerAdjustment>>(body);
                var reconciliations = await this._service.Reconcile(LocationID, User.EmployeeID(), AccountingDT, CreateAdjustments, cashDrawerAdjustments);

                return Ok(reconciliations);
            }
            catch (ApplicationException apex)
            {
                return Conflict(apex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Returns all current Reconciliations
        /// with filters
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(Reconciliation[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] ReconciliationFilter filters)
        {
            var serviceResponse = await _service.GetWithFiltersAsync(filters);
            return serviceResponse.ToResult();
        }

        /// <summary>
        /// Returns a Reconciliation preview with Adjustments
        /// </summary>
        /// <param name="LocationID">Location ID</param>
        /// <param name="AccountingDT">Accounting Date, defaults to now</param>
        /// <param name="IncludeAdjustments">Define if we need to add adjustments to the list</param>
        /// <returns></returns>
        [Route("preview")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(Reconciliation))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If locationID is invalid.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> PreviewReconciliation([FromQuery] byte? LocationID, [FromQuery] DateTime? AccountingDT, [FromQuery] EnumIncludeAdjustments? IncludeAdjustments)
        {
            try
            {
                var reconciliations = await _service.GenerateReconciliationPreview(LocationID, User.EmployeeID(), AccountingDT, IncludeAdjustments);
                return Ok(reconciliations);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Returns a single Reconciliation by ID
        /// </summary>
        /// <param name="ID">Reconciliation ID</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(Reconciliation))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Reconciliation does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> ReadById(int ID)
        {
            return await base.ReadWith(ID, new ReconciliationIncludes());
        }

        /// <summary>
        /// Delete a reconciliation
        /// </summary>
        /// <param name="ID">ID of record to be deleted</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Reconciliation is no longer found")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the Reconciliation does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Delete(int ID)
        {
            var exists = await this._service.FirstOrDefaultAsync(a => a.ID == ID, true);

            if (exists == null)
                return Ok();

            var resp = await this._service.CanDelete(ID);

            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
                await this._service.DeleteReconciliationWithItems(ID);
            else
                return new BadRequestObjectResult(resp);

            return Ok();
        }

        /// <summary>
        /// Gets a Simple ienumerable of Reconciliations
        /// </summary>
        /// <returns></returns>
        [HttpGet("simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleReconciliation[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IEnumerable<SimpleReconciliation>> GetEitherSimpleList([FromQuery]SimpleReconciliationFilter filters)
        {
            return await this._service.SimpleList(filters);
        }

        /// <summary>
        /// Has adjustment endpoint for reconciliations
        /// </summary>
        /// <param name="locationID"></param>
        /// <returns></returns>
        [HttpGet("hasadjustments")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(HasAdjustmentsResponse))]
        public async Task<IActionResult> HasAdjustments([FromQuery] byte? locationID)
        {
            var result = await _service.HasAdjustments(locationID);
            return Ok(result);
        }

        /// <summary>
        /// Returns an array of ReconciliationPaymentInfo Objects for the specified ReconciliationID. 
        /// </summary>
        /// <param name="ID">Reconciliation ID</param>
        /// <returns></returns>
        [HttpGet("{ID}/PaymentInfo")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ReconciliationPaymentInfo[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the specified Reconciliation does not exist.")]
        public async Task<IActionResult> GetPaymentInfo(int ID)
        {
            return (await this._service.GetPaymentInfo(ID)).ToResult();
        }

        /// <summary>
        /// Returns an array of ReconciliationPaymentInfo Objects for the specified location. These collect all un-posted payment entries.  
        /// This call does NOT support adjusting entries. They must be requested by specifiying the AdjustedReconciliationID.
        /// </summary>
        /// <param name="LocationID">Location ID</param>
        /// <returns></returns>
        [HttpGet("Preview/PaymentInfo")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ReconciliationPaymentInfo[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the specified Location does not exist.")]
        public async Task<IActionResult> GetPreviewPaymentInfo(int LocationID)
        {
            return (await this._service.GetPreviewPaymentInfo(LocationID)).ToResult();
        }

        /// <summary>
        /// Returns an array of ReconciliationPaymentInfo Objects that are un-reconciled but are adjustments for the specified AdjustmentReconciliationID.
        /// </summary>
        /// <param name="AdjustedReconciliationID">Adjusted Reconciliation ID</param>
        /// <returns></returns>
        [HttpGet("Preview/PaymentInfo/{AdjustedReconciliationID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(ReconciliationPaymentInfo[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the specified Adjustment Reconciliation does not exist.")]
        public async Task<IActionResult> GetPreviewPaymentInfoForAdjustedReconciliation(int AdjustedReconciliationID)
        {
            return (await this._service.GetPreviewPaymentInfoForAdjustedReconciliation(AdjustedReconciliationID)).ToResult();
        }

        /// <summary>
        /// get voided order within reconciliation
        /// </summary>
        /// <param name="locationID">locationID</param>
        /// <param name="reconciliationID">set to null for previews</param>
        /// <returns></returns>
        [HttpGet("VoidedOrders")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<ICollection<OrderData>> GetVoidedOrders([FromQuery]byte? locationID, [FromQuery]int? reconciliationID)
        {
            return await this._service.GetVoidedOrders(locationID, reconciliationID);
        }

        /// <summary>
        /// User to retrieve the cash drawer balances for the current unreconciled period
        /// </summary>
        /// <param name="locationID">locationID</param>
        /// <param name="AccountingDT">set to null for previews</param>
        /// <returns></returns>
        [HttpGet("preview/cashdrawer")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<ICollection<CashDrawerBalance>> PreviewCashDrawer([FromQuery]byte locationID, [FromQuery] DateTime? AccountingDT)
        {
            if (!AccountingDT.HasValue) AccountingDT = DateTime.UtcNow;
            return await this._service.GetCashDrawerBalance(locationID, AccountingDT);
        }

        /// <summary>
        /// Returns information about the CashDrawer Balance for the specified location during a previous reconciliation.
        /// </summary>
        /// <param name="ReconciliationID"></param>
        /// <returns></returns>
        [HttpGet("{ReconciliationID}/cashdrawer")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        public async Task<ICollection<CashDrawerBalance>> GetCashDrawerBalance(int ReconciliationID)
        {
            return await this._service.GetHistoricalCashDrawerBalance(ReconciliationID);
        }
    }
}