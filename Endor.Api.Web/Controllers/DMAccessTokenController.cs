﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Endor.AzureStorage;
using Microsoft.AspNetCore.Http;
using Endor.Logging.Client;

using Endor.Models;
using Endor.Security;
using Endor.Tenant;
using Endor.DocumentStorage.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.IO;
using Endor.EF;
using Endor.Api.Web.Services;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;
using Endor.RTM;
using Endor.RTM.Models;
using Endor.RTM.Enums;
using Endor.Tasks;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Net.Http;
using System.Text;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    //Anonymous Access
    //GET /api/dm/filebytoken/{token}
    //POST /api/dm/gettoken/{bucket}/{ctid}/{id}/{path/file}
    //


    /// <summary>
    /// DMAccessToken Controller
    /// </summary>
    [Route("api/dm/token")]
    public class DMAccessTokenController : Controller
    {
        private readonly RemoteLogger _logger;
        private readonly ITenantDataCache _cache;
        private readonly ApiContext apiContext;
        private readonly IRTMPushClient _rtmClient;
        /// <summary>
        /// task queuer for triggering indexes
        /// </summary>
        protected readonly ITaskQueuer _taskQueuer;
        /// <summary>
        /// Lazy implementation of the service
        /// </summary>
        protected readonly Lazy<DMAccessTokenService> _lazyService;
        /// <summary>
        /// Getter for the service
        /// </summary>
        protected DMAccessTokenService _service { get { return _lazyService.Value; } }

        /// <summary>
        /// ctor for a new dm controller
        /// </summary>
        /// <param name="apiContext"></param>
        /// <param name="logger"></param>
        /// <param name="tenantCache"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="migrationHelper"></param>
        public DMAccessTokenController(ApiContext apiContext, RemoteLogger logger, ITenantDataCache tenantCache, IRTMPushClient rtmClient = null, ITaskQueuer taskQueuer = null, IMigrationHelper migrationHelper = null)
        {
            this._logger = logger;
            this._cache = tenantCache;
            this.apiContext = apiContext;
            this._rtmClient = rtmClient;
            this._taskQueuer = taskQueuer;
            this._lazyService = new Lazy<DMAccessTokenService>(() => Activator.CreateInstance(typeof(DMAccessTokenService), apiContext, User.BID().Value, logger, rtmClient, migrationHelper) as DMAccessTokenService);
        }

        /// <summary>
        /// This method is used to access a file in blob storage using the Indirect Access token previously obtained with a call to Post ...
        /// </summary>
        /// <param name="token">ClasstypeID</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user does not have a BID or is not logged in</response>  
        [HttpGet("{token}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "Temporary Access Token", type: typeof(string))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Token Not Found")]
        public async Task<IActionResult> GetURLwSASToken(Guid token)
        {
            var result = await this.apiContext.DMAccessToken.Where(t => t.ID == token).ToListAsync();

            if (!result.Any())
            {
                return BadRequest("Token not found");
            }
            //check Expiration Date
            var tokenEntry = result.First();
            if (tokenEntry.ExpirationDate<DateTime.UtcNow)
            {
                return BadRequest("Token has expired");
            }
            if (tokenEntry.ViewsRemaining!=null && tokenEntry.ViewsRemaining<=0)
            {
                return BadRequest("Number of view has been exceeded");
            } 
            //update values
            if (tokenEntry.ViewsRemaining != null)
            {
                tokenEntry.ViewsRemaining--;
            }
            tokenEntry.ViewedCount++;
            tokenEntry.LastViewedDT = DateTime.UtcNow;
            this.apiContext.DMAccessToken.Update(tokenEntry);
            await this.apiContext.SaveChangesAsync();
            return Ok(tokenEntry.URL+"?"+ tokenEntry.SASToken);
        }

        /// <summary>
        /// Requests a anonymous URL to access a particular 
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="id"></param>
        /// <param name="pathAndFilename"></param>
        /// <param name="IsIndirect"></param>
        /// <param name="ExpirationDate"></param>
        /// <param name="MaxViews"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="400">If bucket is NOT Documents or Data or ID missing value/CTID missing value/ClassFolder has value/Bucket is not Documents or Reports</response>  
        [Authorize]
        [HttpPost(DMController.bucket_ctid_id_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(string))]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description:
            @"If bucket is NOT Documents or Data or
            ID missing value
            CTID missing value
            ClassFolder has value
            Bucket is not Documents or Reports\n")]
        public async Task<IActionResult> Post(BucketRequest bucket, int ctid, int id, string pathAndFilename, [FromQuery]bool IsIndirect = false, [FromQuery]DateTime? ExpirationDate = null, [FromQuery]short? MaxViews = null)
        {
            DMController.RequestModel requestModel = new DMController.RequestModel() { Bucket = bucket, ctid = ctid, id = id, PathAndFilename = pathAndFilename };
            var ctx = requestModel.GetStorageContext(User.BID().Value, User.AID());
            string prefix = ctx.Bin.BlobNamePrefix(bucket.ToBucket(), ctx.ID);
            string itemURL = $"{prefix}/{pathAndFilename}";
            DMItem item = new DMItem();
            item.URL = itemURL;
            var client = new EntityStorageClient((await _cache.Get(User.BID().Value)).StorageConnectionString, User.BID().Value, null);

            if (!IsIndirect)
            {
                //Direct Access
                var accessPolicy = new SharedAccessBlobPolicy()
                {
                    Permissions = SharedAccessBlobPermissions.Read,
                    SharedAccessExpiryTime = DateTime.MaxValue.AddDays(-2)
                };
                var uri = await client.GetCanonicalURLWithReadSASTokenAsync(item, accessPolicy);
                return Ok(uri.ToString());               
            }
            else
            {
                if (ExpirationDate==null)
                {
                    return BadRequest("Expiration Date must have a value");
                }
                //Indirect access
                var accessPolicy = new SharedAccessBlobPolicy()
                {
                    Permissions = SharedAccessBlobPermissions.Read,
                    SharedAccessExpiryTime = ExpirationDate.GetValueOrDefault()
                };
                var uri = (await client.GetCanonicalURLWithReadSASTokenAsync(item, accessPolicy)).ToString();

                var accessToken = new DMAccessToken()
                {
                    BID = User.BID().Value,
                    ID = Guid.NewGuid(),
                    URL = uri.Split('?')[0],
                    SASToken = uri.Split('?')[1],
                    ExpirationDate = ExpirationDate.GetValueOrDefault(),
                    ViewedCount = 0,
                    ViewsRemaining = MaxViews,
                    LastViewedDT = null
                };
                await this._service.CreateAsync(accessToken);
                return Ok( $"/api/dm/token/{accessToken.ID}" );
            }
        }

        /// <summary>
        /// Deletes all tokens that exist on a particular DM file. 
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="ctid"></param>
        /// <param name="id"></param>
        /// <param name="pathAndFilename"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        [Authorize]
        [HttpDelete(DMController.bucket_ctid_id_pathAndFilename_route)]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(int))]
        public async Task<IActionResult> Delete(BucketRequest bucket, int ctid, int id, string pathAndFilename)
        {
            DMController.RequestModel requestModel = new DMController.RequestModel() { Bucket = bucket, ctid = ctid, id = id, PathAndFilename = pathAndFilename };
            var ctx = requestModel.GetStorageContext(User.BID().Value, User.AID());
            string prefix = ctx.Bin.BlobNamePrefix(bucket.ToBucket(), ctx.ID);
            string itemURL = $"{prefix}/{pathAndFilename}";
            var resultsToDelete = this.apiContext.DMAccessToken.Where(t => t.BID == User.BID().Value && t.URL == itemURL).ToList();
            foreach (var result in resultsToDelete)
            {
                await this._service.DeleteAsync(result);
            }
            return Ok(0);
        }
    }
}
