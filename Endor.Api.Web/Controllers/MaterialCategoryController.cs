﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Http;
using Endor.Security;
using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Models;
using Endor.EF;
using Endor.Tasks;
using Endor.Tenant;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// Material category endpoints
    /// </summary>
    [Route("api/materialcategory")]
    public class MaterialCategoryController : CRUDController<MaterialCategory, MaterialCategoryService, short>, ISimpleListableViewController<SimpleMaterialCategory, short>
    {
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper">Migration Helper</param>
        public MaterialCategoryController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper)
            : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Returns all of the MaterialCategories
        /// in simple list view
        /// </summary>
        /// <returns></returns>
        public ISimpleListableViewService<SimpleMaterialCategory, short> ListableService => this._service;

        /// <summary>
        /// Returns all of the MaterialCategories
        /// can be filtered by IsActive
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MaterialCategory[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If invalid includes or filters are specified")]
        public async Task<IActionResult> FilteredRead([FromQuery] MaterialCategoryFilter filter)
        {
            return await base.FilteredReadWith(item => item.IsActive == (filter.IsActive ?? item.IsActive), null);
        }

        /// <summary>
        /// obselete
        /// </summary>
        /// <returns></returns>
        [HttpGet("obselete")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MaterialCategory[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Read()
        {
            return await base.Read();
        }

        /// <summary>
        /// obselete
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("obselete/{ID}")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MaterialCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MaterialCategory does not exist")]
        public override async Task<IActionResult> ReadById(short ID)
        {
            return await base.ReadById(ID);
        }

        /// <summary>
        /// Returns an individual material category
        /// </summary>
        /// <param name="ID">MaterialCategory.ID</param>
        /// <param name="includes">Query parameters to include child objects</param>
        /// <returns>`MaterialCategory`</returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MaterialCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MaterialCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If invalid includes are specified")]
        public async Task<IActionResult> ReadWithIncludes(short ID, [FromQuery]MaterialCategoryIncludes includes)
        {
            return await base.ReadWith(ID, includes);
        }

        /// <summary>
        /// obselete-simplelist
        /// </summary>
        /// <returns></returns>
        [HttpGet("obselete-simplelist")]
        [Obsolete]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleMaterialCategory[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<SimpleMaterialCategory[]> SimpleList()
        {
            return await this.GetSimpleList<SimpleMaterialCategory, short>(User.BID().Value);
        }

        /// <summary>
        /// Returns simple list of material categories
        /// optionally filtered by IsActive query parameter
        /// </summary>
        /// <returns></returns>
        [HttpGet("simplelist")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(SimpleMaterialCategory[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If invalid filters are specified")]
        public async Task<SimpleMaterialCategory[]> SimpleList([FromQuery]SimpleMaterialCategoryFilters filters)
        {
            return await this.GetSimpleList<SimpleMaterialCategory, short>(User.BID().Value, filters);
        }

        /// <summary>
        /// updates a single Material Category by ID and body
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MaterialCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MaterialCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the MaterialCategory does not exist or the update fails.")]
        public override async Task<IActionResult> Update(short ID, [FromBody] MaterialCategory update)
        {
            return await base.Update(ID, update);
        }

        /// <summary>
        /// Deletes a MaterialCategory by ID
        /// with an optional Force query parameter
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Force">If true, delete all links to material parts first</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.NoContent)]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If MaterialCategory does not exist")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the MaterialCategory does not exist or the deletion fails")]
        public async Task<IActionResult> DeleteWithForce(short ID, [FromQuery]bool Force = false)
        {
            await this._service.unlinkAllAsync(ID);
            return await base.Delete(ID);
        }

        /// <summary>
        /// obsolete delete
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Obsolete]
        [HttpDelete("obselete/{ID}")]
        public override async Task<IActionResult> Delete(short ID)
        {
            return await base.DoDelete(ID);
        }

        /// <summary>
        /// Creates a new MaterialCategory
        /// </summary>
        /// <param name="newModel"></param>
        /// <param name="tempID"></param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(MaterialCategory))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the MaterialCategory's creation fails")]
        public override async Task<IActionResult> Create([FromBody] MaterialCategory newModel, [FromQuery] Guid? tempID = null)
        {
            return await base.Create(newModel, tempID);
        }

        //ACTIONS

        /// <summary>
        /// sets a MaterialCategory to active
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> SetActive([FromRoute]short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// sets a MaterialCategory to inactive
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse<short>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse<short>), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> SetInActive([FromRoute]short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Returns whether a MaterialCategory can be deleted
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("{ID}/action/candelete")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
#pragma warning disable CS1998
        public async Task<IActionResult> CanDelete([FromRoute]short ID)
#pragma warning restore CS1998
        {
            return (new BooleanResponse() { Success = true, Message = "CanDelete: true", Value = true }).ToResult();
        }

        /// <summary>
        /// links a material category to a material part
        /// </summary>
        /// <param name="materialCategoryID">MaterialCategory.ID</param>
        /// <param name="materialDataID">MaterialData.ID</param>
        /// <returns></returns>
        [HttpPost("{materialCategoryID}/action/linkmaterial/{materialDataID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> LinkMaterial([FromRoute]short materialCategoryID, [FromRoute]int materialDataID)
        {
            return (await this._service.LinkMaterial(materialCategoryID, materialDataID, true)).ToResult();
        }

        /// <summary>
        /// unlinks a material category from a material part
        /// </summary>
        /// <param name="materialCategoryID">MaterialCategory.ID</param>
        /// <param name="materialDataID">MaterialData.ID</param>
        /// <returns></returns>
        [HttpPost("{materialCategoryID}/action/unlinkmaterial/{materialDataID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public async Task<IActionResult> UnLinkMaterial([FromRoute]short materialCategoryID, [FromRoute]int materialDataID)
        {
            return (await this._service.LinkMaterial(materialCategoryID, materialDataID, false)).ToResult();
        }

        /// <summary>
        /// Material Category Clone Endpoint
        /// </summary>
        /// <param name="ID">ID of record to be cloned</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(GenericResponse<MaterialCategory>))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(GenericResponse<MaterialCategory>), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        public override async Task<IActionResult> Clone(short ID)
        {
            try
            {
                return await DoClone(ID);
            }
            catch (Exception e)
            {
                GenericResponse<MaterialCategory> genericResponse = new GenericResponse<MaterialCategory>() { ErrorMessage = e.Message };
                return genericResponse.ToResult();
            }
        }

        /// <summary>
        /// Validates MaterialCategory name if already exists
        /// </summary>
        /// <returns></returns>
        public override bool TryValidateModel(object model, string prefix)
        {
            var materialCategoryModel = (MaterialCategory)model;
            var existingNameOwners = this._service.ExistingNameOwners(materialCategoryModel);
            if (existingNameOwners.Count()>0)
            {
                this.ModelState.AddModelError("Name", $"A MaterialCategory named {materialCategoryModel.Name} already exists.");
            }

            return base.TryValidateModel(model, prefix);
        }
    }
}