﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Security;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Endor.Api.Web.Annotation;

namespace Endor.Api.Web.Controllers
{
    /// <summary>
    /// AlertDefinitionAction Controller
    /// </summary>
    [Route("api/alert/definitionaction")]
    public class AlertDefinitionActionController : CRUDController<AlertDefinitionAction, AlertDefinitionActionService, short>
    {
        /// <summary>
        /// AlertDefinitionAction Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        /// <param name="taskQueuer">Task Queuer</param>
        /// <param name="cache">Tenant Data Cache</param>
        /// <param name="migrationHelper">Migration Helper</param>
        public AlertDefinitionActionController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, IMigrationHelper migrationHelper) : base(context, logger, rtmClient, taskQueuer, cache, migrationHelper)
        {
        }

        /// <summary>
        /// Obsolete Read Method
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HttpGet("obsolete")]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override Task<IActionResult> Read()
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
            return base.Read();
        }

        /// <summary>
        /// Creates a new AlertDefinitionAction
        /// </summary>
        /// <param name="newModel">The AlertDefinitionAction model to create</param>
        /// <param name="tempID">A temporary ID used when creating objects with document storage</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AlertDefinitionAction), "Created a new alert definition action")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid parameters were supplied or there was a different issue with the request")]
        public override Task<IActionResult> Create([FromBody] AlertDefinitionAction newModel, [FromQuery] Guid? tempID = null)
        {
            return base.Create(newModel, tempID);
        }

        /// <summary>
        /// Gets all AlertDefinitionAction for the specified Query Params
        /// </summary>
        /// <param name="filters">AlertDefinitionAction Filters</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AlertDefinitionAction[]))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public async Task<IActionResult> ReadWithFilters([FromQuery] AlertDefinitionActionFilter filters)
        {
            return new OkObjectResult(await this._service.GetAllWithFilters(filters));
        }

        /// <summary>
        /// Gets a single AlertDefinitionAction by ID
        /// </summary>
        /// <param name="ID">The ID of the AlertDefinitionAction to be retrieved</param>
        /// <returns></returns>
        [HttpGet("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AlertDefinitionAction), "Successfully retrieved the AlertDefinitionAction")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "AlertDefinitionAction was not found")]
        public override Task<IActionResult> ReadById(short ID)
        {
            return base.ReadById(ID);
        }


        /// <summary>
        /// Updates a AlertDefinitionAction
        /// </summary>
        /// <param name="ID">The Id of the AlertDefinitionAction to update</param>
        /// <param name="update">Contains the updated AlertDefinitionAction model</param>
        /// <returns></returns>
        [HttpPut("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AlertDefinitionAction), "Successfully updated the AlertDefinitionAction")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "Invalid request properties")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "AlertDefinitionAction was not found")]
        public override Task<IActionResult> Update(short ID, [FromBody] AlertDefinitionAction update)
        {
            return base.Update(ID, update);
        }

        /// <summary>
        /// Deletes a AlertDefinitionAction
        /// </summary>
        /// <param name="ID">The ID of the AlertDefinitionAction to be deleted</param>
        /// <returns></returns>
        [HttpDelete("{ID}")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, description: "AlertDefinitionAction is no longer found")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in")]
        public override async Task<IActionResult> Delete(short ID)
        {
            BooleanResponse resp = await this._service.CanDelete((short)ID);
            if (resp?.Success == true && (resp.Value.HasValue && resp.Value.Value))
            {
                return await base.Delete(ID);
            }
            else
            {
                return new BadRequestObjectResult(resp);
            }
        }

        /// <summary>
        /// Clones an existing Alert Definition Action to a new Alert Definition
        /// </summary>
        /// <param name="ID">ID of the Alert Definition Action to clone from</param>
        /// <returns></returns>
        [HttpPost("{ID}/clone")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(AlertDefinitionAction))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, description: "If the source Alert Definition Action does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, description: "If the source Alert Definition Action does not exist or the cloning fails.")]
        public override async Task<IActionResult> Clone(short ID)
        {
            return await base.Clone(ID);
        }

        /// <summary>
        /// Sets a AlertDefinitionAction to Active
        /// </summary>
        /// <param name="ID">AlertDefinitionAction ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the AlertDefinitionAction does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the AlertDefinitionAction does not exist or fails to set as active.")]
        public async Task<IActionResult> SetActive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, true, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Sets an AlertDefinitionAction to Inactive
        /// </summary>
        /// <param name="ID">AlertDefinitionAction ID</param>
        /// <returns></returns>
        [HttpPost("{ID}/action/setinactive")]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(EntityActionChangeResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the AlertDefinitionAction does not exist.")]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(EntityActionChangeResponse), description: "If the AlertDefinitionAction does not exist or fails to set as active.")]
        public async Task<IActionResult> SetInactive(short ID)
        {
            EntityActionChangeResponse<short> resp = await this._service.SetActive(ID, false, Request.Headers[ConnectionIDHeaderKey]);

            return resp.ToResult();
        }

        /// <summary>
        /// Returns true | false based on whether the AlertDefinitionAction can be deleted.
        /// </summary>
        /// <param name="ID">AlertDefinitionAction ID</param>
        /// <returns></returns>
        [Route("{ID}/action/candelete")]
        [HttpGet]
        [SwaggerCustomResponse((int)HttpStatusCode.OK, typeof(BooleanResponse))]
        [SwaggerCustomResponse((int)HttpStatusCode.Unauthorized, description: "If user does not have a BID or is not logged in.")]
        [SwaggerCustomResponse((int)HttpStatusCode.BadRequest, typeof(BooleanResponse), description: "If the action was unsuccessful")]
        [SwaggerCustomResponse((int)HttpStatusCode.InternalServerError, description: "If an error occurs")]
        [SwaggerCustomResponse((int)HttpStatusCode.NotFound, typeof(EntityActionChangeResponse), description: "If the AlertDefinitionAction does not exist.")]
        public async Task<IActionResult> CanDelete(short ID)
        {
            return (await this._service.CanDelete((short)ID)).ToResult();
        }

    }
}
