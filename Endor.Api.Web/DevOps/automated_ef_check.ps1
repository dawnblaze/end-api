# FOR LOCAL TESTING UN-COMMENT CODE BELOW
# -- UN-COMMENT START --
#$qa_connection_string=<CONNECTION STRING VALUE HERE";
#$qa_db_server=<DB SERVER VALUE HERE>;
#$qa_login_username=<USERNAME VALUE HERE>
#$qa_login_password = <USER PASSWORD HERE>
#$MasterDataDBName = $ENDOR_MASTER_SCHEMA_NAME;
#$incomingMsgHook=<MS TEAMS OR SLACK INCOMING WEBHOOK HERE>
# -- UN-COMMENT END --
[CmdletBinding()]
param (
    $qa_connection_string,
    $qa_db_server,
    $MasterDataDBName,
    $qa_login_username,
    $qa_login_password,
    $MS_TEAM_INCOMING_WEBHOOK
)
$errorCount=1; $failedLoginMessage = @(); 
$Notification = @"
    {
        "@type": "MessageCard",
        "@context": "https://schema.org/extensions",
        "summary": "Corebridge EF Validation",
        "themeColor": "0072C6",
        "title": "Corebridge EF Check",
         "sections": [
            {
                "activityTitle": "![TestImage](https://ibb.co/P5grw6B) DevOps Validation Process",
                "activitySubtitle": "By Team Mindstone",
                "activityImage": "https://ibb.co/P5grw6B",
                "facts": [
                    {
                        "name": "Database Name",
                        "value": "DB_NAME"
                    },
                    {
                        "name": "Date:",
                        "value": "DATETIME"
                    },
                    {
                        "name": "URL used",
                        "value": "URL_VALUE"
                    }
                ],
                "text": "TEXT_INFORMATION"
            }
        ]
    }
"@
$HasLoginFailures = $false; 
$sqlConDBData = New-Object System.Data.SqlClient.SqlConnection;
$sqlConDBData.ConnectionString=$qa_connection_string -f $qa_db_server,$MasterDataDBName,$qa_login_username,$qa_login_password;
$sqlConDBData.Open() 
$sqlQueryDBData = 'SELECT bus.bid, bus.DatabaseName, sd.url from (select distinct (select top 1 bid from [business.data] bd where bd.BusinessDatabaseID = dd.id) as BID ,connectionstring, DatabaseName from [database.data] dd where dd.IsCustomerDatabase =1 ) bus left join [Business.ServerLink] bsl on bsl.BID = bus.BID left join [server.data] sd on sd.id = bsl.ServerID left join [enum.Server.Type] est on sd.ServerType = est.ID where est.Name = {0}' -f '''Api'''
#$sqlQueryDBData = 'select bus.bid, sd.url from (select distinct (select top 1 bid from [business.data] bd where bd.BusinessDatabaseID = dd.id) as BID ,connectionstring from [database.data] dd where dd.IsCustomerDatabase =1 ) bus left join [Business.ServerLink] bsl on bsl.BID = bus.BID left join [server.data] sd on sd.id = bsl.ServerID left join [enum.Server.Type] est on sd.ServerType = est.ID where est.Name = ''Api'''
$cmdSqlDBData = New-Object System.Data.SqlClient.SqlCommand($sqlQueryDBData,$sqlConDBData);
$resultSetDBData = $cmdSqlDBData.ExecuteReader();
while($resultSetDBData.Read()){
   try {
     $BID_VALUE=($resultSetDBData["BID"].ToString());
     $BusDataDBName=($resultSetDBData["DatabaseName"].ToString());
     $apiURL = ($resultSetDBData["URL"].ToString());
     # FOR LOCAL TESTING UN-COMMENT CODE BELOW
     # -- UN-COMMENT START --
     #$serverToValidate='http://httpstat.us/500';
     #$queryStr=$serverToValidate
     # -- UN-COMMENT END --
     if ($apiURL -notmatch '\/$')
     {
       $apiURL += '/';
     }
     $serverToValidate = $apiURL+'security?bid=';
     $queryStr = '{0}{1}&userID=1' -f $serverToValidate,$BID_VALUE
     try{ 
         Invoke-WebRequest -Method 'Get' -Uri $queryStr | Select-Object -Property Content;
         $isFailed = $false;
     }catch{ 
        $FailedStatusCode=[int]$_.Exception.Response.StatusCode;
        if($FailedStatusCode -eq 500)
        { 
         $dateTime = Get-Date -format F
         $URL_ERROR_MESSAGE='500 Internal Server Error for  {0}' -f $queryStr;
         $NotificationBody = $Notification.Replace("DB_NAME","$BusDataDBName").Replace("DATETIME","$dateTime").Replace("URL_VALUE","$queryStr").Replace("TEXT_INFORMATION","$URL_ERROR_MESSAGE")
         $Command = (Invoke-RestMethod -uri $MS_TEAM_INCOMING_WEBHOOK -Method Post -body $NotificationBody -ContentType 'application/json')
         $userLoginResults+=$jsonParam;
         $HasLoginFailures = $true; 
         $errorCount++; 
        }
        "has errored logging in "+$queryStr+" with status code"+[int]$_.Exception.Response.StatusCode;
     }finally{  }
   } catch { $error[0];} 
   finally {  } 
   $whileCount++
}
$sqlConDBData.Close();
