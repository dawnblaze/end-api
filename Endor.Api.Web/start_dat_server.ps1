$env:APPSETTING_Auth:SymmetricKey = "QUZDN0JGMjgtNzlEMS00MDI0LTlCMDAtRUU5QjVENjk1QjNFICAgICAgICA";
$env:APPSETTING_Auth:ValidIssuer = "https://endorauth.localcyriousdevelopment.com:5001/";
$env:APPSETTING_Auth:ValidAudience = "https://endorapi.localcyriousdevelopment.com:5002/";
$env:APPSETTING_Endor:TenantSecret = "c71f5404-3425-4de4-9aae-93b7850b15e4";
$env:APPSETTING_Endor:certThumbprint = "54ad89346c3d56b09fe26b40bf2817cdfff4eb9f";
$env:APPSETTING_Endor:AuthOrigin = "https://endorauth.localcyriousdevelopment.com:5001";
$env:APPSETTING_Endor:MessagingServerURL = "https://endormessaging.localcyriousdevelopment.com:5004/";
$env:APPSETTING_Logging:RemoteURL= "https://endorlog.localcyriousdevelopment.com:5003";
$env:ASPNETCORE_URLS= "https://endorapi.localcyriousdevelopment.com:5002";
$env:ASPNETCORE_ENVIRONMENT= "Development";
dotnet run --Configuration Debug