using Newtonsoft.Json;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// 
    /// </summary>
    public class EmployeeNotificationInfo
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public short EmployeeID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? UnreadMessageCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? UnreadAlertCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? UnreadSystemNoticeCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? TimeCardID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsLoggedIn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? TimeCardDetailID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? OrderItemStatusID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? TimeClockActivityID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? TimeClockBreakID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string StatusName { get; set; }
    }
}