﻿using Endor.Api.Web.Classes.Responses;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// AssemblyHelper
    /// </summary>
    public static class AssemblyHelper
    {
        /// <summary>
        /// Get dropdown label/tooltip/datatype + options
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ctx"></param>
        /// <param name="logger"></param>
        /// <param name="bid"></param>
        /// <param name="variableID"></param>
        /// <param name="machineID"></param>
        /// <param name="profileName"></param>
        /// <param name="itemSelector"></param>
        /// <returns></returns>
        public static async Task<DropdownResponse<T>> GetDropdownResponse<T>(ApiContext ctx, RemoteLogger logger, short bid, int variableID, int? machineID, string profileName, Func<DropDownListValues, Task<ICollection<T>>> itemSelector)
        {
            var data = await GetVariableAndMachineVariable(ctx, logger, bid, variableID, machineID, profileName);

            return new DropdownResponse<T>()
            {
                Label = (data.profVar?.OverrideDefault ?? false) ? data.profVar?.Label : data.avar?.Label,
                DataType = (data.profVar?.OverrideDefault ?? false) ? data.profVar?.DataType ?? data.avar.DataType : data.avar.DataType,
                Tooltip = (data.profVar?.OverrideDefault ?? false) ? data.profVar?.Tooltip : data.avar.Tooltip,
                Options = await itemSelector(await GetOptionValuesResponse<DropDownListValues>(data, bid, logger))
            };
        }

        /// <summary>
        /// Returns the ListValues based on the Assembly Variable and, possibly, the Machine ID and Profile Name
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="logger"></param>
        /// <param name="bid"></param>
        /// <param name="variableID"></param>
        /// <param name="machineID"></param>
        /// <param name="profileName"></param>
        /// <returns></returns>
        public static async Task<T> GetListValues<T>(ApiContext ctx, RemoteLogger logger, short bid, int variableID, int? machineID, string profileName)
            where T : class
        {
            var data = await GetVariableAndMachineVariable(ctx, logger, bid, variableID, machineID, profileName);

            return await GetOptionValuesResponse<T>(data, bid, logger);
        }

        /// <summary>
        /// Deserializes option values
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="bid"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        private static async Task<T> GetOptionValuesResponse<T>((AssemblyVariable avar, MachineProfile prof, MachineProfileVariable profVar) data, short bid, RemoteLogger logger)
        {
            string listValuesJSON;

            if (data.profVar != null && data.profVar.OverrideDefault)
                listValuesJSON = data.profVar.ListValuesJSON;
            else
                listValuesJSON = data.avar.ListValuesJSON;

            if (string.IsNullOrWhiteSpace(listValuesJSON))
                return default(T);

            try
            {
                if (typeof(T) == typeof(ICollection<NamedQuark>))
                {
                    listValuesJSON = listValuesJSON.Replace("\"Label\":", "\"Name\":");
                    listValuesJSON = listValuesJSON.Replace("\"Integer\":", "\"ID\":");
                }

                return JsonConvert.DeserializeObject<T>(listValuesJSON);
            }
            catch (Exception e)
            {
                await logger.Error(bid, "Error deserializing ListValuesJSON in GetListValues.", e);
                return default(T);
            }
        }

        private static async Task<(AssemblyVariable avar, MachineProfile prof, MachineProfileVariable profVar)> GetVariableAndMachineVariable(ApiContext ctx, RemoteLogger logger, short bid, int variableID, int? machineID, string profileName)
        {
            AssemblyVariable variable = await ctx.AssemblyVariable
                                                 .Include(x => x.Assembly)
                                                 .FirstOrDefaultAsync(v => v.BID == bid && v.ID == variableID);
            if (variable == null)
                return (avar: null, prof: null, profVar: null);

            MachineProfileVariable machineProfileVariable = null;
            if (variable.Assembly != null
                && variable.Assembly.AssemblyType == AssemblyType.MachineTemplate
                && machineID.HasValue
                && !string.IsNullOrWhiteSpace(profileName))
            {
                MachineProfile machineProfile = await ctx.MachineProfile
                                                         .Include(mp => mp.MachineProfileVariables)
                                                         .FirstOrDefaultAsync(mp => mp.BID == bid && mp.MachineID == machineID && mp.Name == profileName);

                machineProfileVariable = machineProfile?.MachineProfileVariables?
                                                         .FirstOrDefault(mpf => mpf.BID == bid && mpf.VariableID == variableID);

                return (avar: variable, prof:  machineProfile, profVar: machineProfileVariable);
            }
            else
            {
                return (avar: variable, prof: null, profVar: null);
            }
        }
    }
}
