﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// notifications data
    /// </summary>
    public class EmployeeNotificationsData
    {
        /// <summary>
        /// the count of unread messages
        /// </summary>
        public int UnreadMessageCount { get; set; }
        /// <summary>
        /// the count of unread alerts
        /// </summary>
        public int UnreadAlertCount { get; set; }
        /// <summary>
        /// current time clock status
        /// </summary>
        public bool IsLoggedIn { get; set; }
        /// <summary>
        /// an object giving the employees current time clock status, 
        /// indicating if they are clocked in.  
        /// If they are clocked on to a detail, the station and TimeCardDetail ID.
        /// </summary>
        public TimeCardInfoData TimeCardInfo { get; set; }        
        /// <summary>
        /// a list of employee IDs and initials viewing the page
        /// </summary>
        public EmployeeViewingPageData[] EmployeesViewingPage { get; set; }
        /// <summary>
        /// a banner message
        /// </summary>
        public string BannerMessage { get; set; }
    }
}
