﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Object use for Employee dropdowns in boards
    /// </summary>
    public class EmployeeList
    {
        /// <summary>
        /// ID
        /// </summary>
        public short ID { get; set; }

        /// <summary>
        /// Long Name
        /// </summary>
        public string LongName { get; set; }

        /// <summary>
        /// Position
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// IsActive
        /// </summary>
        public bool IsActive { get; set; }
    }
}
