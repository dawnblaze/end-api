﻿using Endor.AEL;
using Endor.Models;
using Endor.RTM.Models;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.PeerHub
{
    /// <summary>
    /// Initiates the connection to PeerHost Server
    /// </summary>
    public class PeerHubClient : IDisposable
    {
        /// <summary>
        /// HubConnection reference
        /// </summary>
        public HubConnection Connection { get; private set; }

        /// <summary>
        /// Checks if currently connected to PeerHost server
        /// </summary>
        private bool isConnected;

        private readonly string peerHostHubUrl;

        private readonly EndorOptions endorOptions;

        /// <summary>
        /// Creates connection to PeerHost server
        /// </summary>
        /// <param name="options"></param>
        public PeerHubClient(EndorOptions options)
        {
            string peerHubUrl = options.PeerHostUrl;

            if (peerHubUrl == null)
                throw new ArgumentNullException("peerHostHubUrl");

            if (String.IsNullOrWhiteSpace(peerHubUrl) || !Uri.IsWellFormedUriString(peerHubUrl, UriKind.Absolute))
            {
                throw new ArgumentException("Invalid url", "peerHubUrl");
            }
            else if (!peerHubUrl.EndsWith("/peer/"))
            {
                UriBuilder builder = new UriBuilder(peerHubUrl);
                builder.Path = "/peer/";
                peerHubUrl = builder.ToString();
            }

            endorOptions = options;
            peerHostHubUrl = peerHubUrl;
        }

        /// <summary>
        /// Initiates the connection to peerhost server
        /// </summary>
        /// <returns></returns>
        public async Task<bool> ConnectAsync()
        {
            if (!isConnected)
            {
                Connection = new HubConnectionBuilder()
                    .WithUrl(peerHostHubUrl, (httpOptions) =>
                    {
                        httpOptions.Headers["Authorization"] = new AuthenticationHeaderValue("Internal", endorOptions.TenantSecret).ToString();
                    })
                    .ConfigureLogging((t) => t.AddConsole().AddFilter((s, l) => l == LogLevel.Trace))
                    .AddNewtonsoftJsonProtocol()
                    .WithAutomaticReconnect()
                    .Build();

                Connection.On<RefreshMessage>("refresh", (data) =>
                {
                    RefreshHandler(data);
                });


                Connection.Closed += async(e) =>
                {

                    this.isConnected = false;
                    // reconnect
                    await Connection.DisposeAsync();
                    await Task.Delay(1000);
                    await ConnectAsync();
                };

                try
                {
                    await Connection.StartAsync();
                    
                    isConnected = true;
                }
                catch (Exception)
                {
                    isConnected = false;
                }
            }

            return isConnected;
        }


        /// <summary>
        /// Join Group By Name
        /// </summary>
        /// <param name="groupKey"></param>
        /// <returns></returns>
        public async Task JoinGroupByName(string groupKey)
        {
            if (await ConnectAsync())
            {
                await Connection.InvokeAsync("joinGroupByName", groupKey);
            }
        }

        /// <summary>
        /// Removes the group from the subscription list
        /// </summary>
        /// <param name="names"></param>
        /// <returns></returns>
        public async Task LeaveGroups(string[] names)
        {
            if (await ConnectAsync())
            {
                await Connection.InvokeAsync("leaveGroups", names);
            }
        }

        private void RefreshHandler(RefreshMessage data)
        {
            int[] allowedClassTypeID = { (int)ClassType.EmployeeRole, (int)ClassType.CustomFieldDefinition, (int)ClassType.CustomFieldLayoutDefinition };
            
            foreach (var entity in data.RefreshEntities)
            {
                if (allowedClassTypeID.Contains(entity.ClasstypeID))
                {
                    BusinessMemberDictionary.ClearBIDDictionary(entity.BID);
                }
            }
        }

        /// <summary>
        /// Disppost the connection from Peerhost Server
        /// </summary>
        public async void Dispose()
        {
            if (Connection != null)
            {
                await Connection.DisposeAsync();
            }
        }

    }
}
