﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters for TimeCard entity
    /// </summary>>
    public class TimeCardFilter : IQueryFilters<TimeCard>, IExpandIncludes
    {
        /// <summary>
        /// Required. The ID of the Employee for whom TimeCards are being retrieved.
        /// </summary>
        public int? EmployeeID { get; set; }

        /// <summary>
        /// If true, only open timecards for the employee are returned. Defaults to false if not supplied.
        /// </summary>
        public bool? OnlyOpen { get; set; }

        /// <summary>
        /// If supplied, specifies the start date of timecards to pull.  Defaults to Sunday morning of the current week if not supplied.
        /// </summary>
        public DateTime? StartDT { get; set; }

        /// <summary>
        /// If supplied, specifies the end datetime of timecards to pull.  Defaults to Now() if not supplied.
        /// </summary>
        public DateTime? EndDT { get; set; }

        /// <summary>
        /// If true, includes the collection of TimeCards Details for each TimeCard.
        /// </summary>
        public bool? IncludeDetails { get; set; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<TimeCard, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<TimeCard, bool>>>();

            predicates.Add(ad => ad.EmployeeID == this.EmployeeID);

            if (this.OnlyOpen.HasValue)
                predicates.Add(ad => ad.IsClosed == !this.OnlyOpen);

            if (this.StartDT.HasValue)
                predicates.Add(ad => ad.StartDT >= this.StartDT);
            else
            {
                if (!(this.OnlyOpen.HasValue && this.OnlyOpen.Value == true))
                    predicates.Add(ad => ad.StartDT >= this.SundayMorning());
            }

            if (this.EndDT.HasValue)
                predicates.Add(ad => ad.EndDT <= this.EndDT);
            else
            {
                if (this.OnlyOpen.HasValue && this.OnlyOpen.Value == true)
                    predicates.Add(ad => ad.EndDT == null);
                else
                    predicates.Add(ad => ad.EndDT <= DateTime.UtcNow);
            }

            return predicates.ToArray();
        }

        /// <summary>
        /// Return Sunday morning of the current week
        /// </summary>
        private DateTime SundayMorning()
        {
            int diff = (7 + (DateTime.UtcNow.DayOfWeek - DayOfWeek.Sunday)) % 7;
            return DateTime.UtcNow.AddDays(-1 * diff).Date;
        }

        /// <summary>
        /// Return true if any filters are present
        /// </summary>
        internal bool HasFilters => WherePredicates().Length > 0;


        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { "TimeCardDetails", new IncludeSettings(this.IncludeDetails ?? false ? IncludesLevel.Full : IncludesLevel.None) },
            };
        }
    }
}
