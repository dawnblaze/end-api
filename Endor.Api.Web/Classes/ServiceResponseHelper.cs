﻿using Endor.Api.Web.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// helper methods for <see cref="ServiceResponse"/> object
    /// </summary>
    public static class ServiceResponseHelper
    {
        /// <summary>
        /// Converts a service response into a IActionResult
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static IActionResult ToResult(this ServiceResponse self)
        {
            if (self.Success)
            {
                if (self.Value != null)
                    return new OkObjectResult(self.Value);

                return new OkResult();
            }

            if (self.IsNotFound)
                return new NotFoundResult();


            if (self.Message != null)
                return new BadRequestObjectResult(self.Message);

            return new BadRequestResult();
        }
    }
}
