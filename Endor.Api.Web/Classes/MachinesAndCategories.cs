using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// 
    /// </summary>
    public class MachinesAndCategories
    {
      /// <summary>
      /// 
      /// </summary>
      /// <value></value>
      public ICollection<MachineData> Machines {get;set;}
      /// <summary>
      /// 
      /// </summary>
      /// <value></value>
      public ICollection<MachineCategory> Categories {get;set;}
    }
}
