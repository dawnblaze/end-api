using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
  /// <summary>
  /// 
  /// </summary>
  public class PartsToCategory
  {
    /// <summary>
    /// Link new simpleitems and unlink nonexisting links
    /// </summary>
    /// <param name="oldLinks"></param>
    /// <param name="linkedSimpleItems"></param>
    /// <param name="compareID"></param>
    /// <param name="removeLink"></param>
    /// <param name="createLink"></param>
    /// <typeparam name="CL"></typeparam>
    /// <typeparam name="SLI"></typeparam>
    /// <returns></returns>
    public static async Task LinkOrUnlink<CL,SLI>
      (ICollection<CL> oldLinks, ICollection<SLI> linkedSimpleItems, Func<CL,SLI,bool> compareID, Func<CL,Task> removeLink, Func<SLI,Task> createLink)
    {
      linkedSimpleItems = linkedSimpleItems ?? new SLI[]{};
      var removedLinks = oldLinks.Where(link => !linkedSimpleItems.Any(si => compareID(link,si))).ToList();
      foreach (var link in removedLinks)
      {
        await removeLink(link);
      }

      var newLinkedSimpleItems = linkedSimpleItems.Where(si => !oldLinks.Any(link => compareID(link,si))).ToList();
      foreach (var simpleItem in newLinkedSimpleItems)
      {
        await createLink(simpleItem);
      }
    }
  }
}
