﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// An interface that exposes an optional IsActive filter
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IIsActiveQueryFilter<T> : IQueryFilters<T>
    {
        /// <summary>
        /// When non-null, filters to only simple entities of IsActive: True or False
        /// </summary>
        bool? IsActive { get; set; }
    }
}
