﻿using Endor.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using Endor.Tenant;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Requires All Attribute
    /// </summary>
    public class RequiresAllAttribute : ActionFilterAttribute
    {
        private SecurityRight[] _rights;

        /// <summary>
        /// Requires All Attribute
        /// </summary>
        /// <param name="right">Security Rights</param>
        public RequiresAllAttribute(SecurityRight right)
            : this(new SecurityRight[] { right })
        {
        }

        /// <summary>
        /// Requires All Attribute
        /// </summary>
        /// <param name="rights">Security Rights</param>
        public RequiresAllAttribute(SecurityRight[] rights)
        {
            if (rights == null)
                throw new ArgumentNullException(nameof(rights), "You must specify at least one SecurityRight");

            _rights = rights;
        }

        /// <summary>
        /// On Action Executing
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var rightsCache = (RightsCache)context.HttpContext.RequestServices.GetService(typeof(RightsCache));
            var userBID = context.HttpContext.User.BID().Value;
            var userLinkID = context.HttpContext.User.UserLinkID().Value;

            foreach (var right in _rights)
            {
                if (!rightsCache.HasRight(userBID, userLinkID, right))
                {
                    context.Result = new UnauthorizedResult();
                    return;
                }

            }
        }
    }
    /// <summary>
    /// Requires Any Attribute
    /// </summary>
    public class RequiresAnyAttribute : ActionFilterAttribute
    {
        private SecurityRight[] _rights;
        /// <summary>
        /// Requires Any Attribute
        /// </summary>
        /// <param name="rights">Security Rights</param>
        public RequiresAnyAttribute(SecurityRight[] rights)
        {
            if (rights == null)
                throw new ArgumentNullException(nameof(rights), "You must specify at least one SecurityRight");

            _rights = rights;
        }

        /// <summary>
        /// On Action Executing
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var rightsCache = (RightsCache)context.HttpContext.RequestServices.GetService(typeof(RightsCache));
            var userBID = context.HttpContext.User.BID().Value;
            var userLinkID = context.HttpContext.User.UserLinkID().Value;

            if (rightsCache.HasAnyRight(userBID, userLinkID, _rights))
                return;

            context.Result = new UnauthorizedResult();
        }
    }
}
