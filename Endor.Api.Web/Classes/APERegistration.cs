﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
     /// Registration request model
     /// </summary>
    public class APERegistrationRequest
    {
        /// <summary>
        /// The Key of the Business 
        /// </summary>
        public string BusinessKey { get; set; }
        /// <summary>
        /// Merchant ID issued by the card service
        /// </summary>
        public string MerchantID { get; set; }
        /// <summary>
        /// Date/Time the registration expires
        /// </summary>
        public DateTime ExpirationDT { get; set; }
        /// <summary>
        /// Application the created the registration
        /// </summary>
        public string AppType { get; set; }
        /// <summary>
        /// If true, the sandbox credentials are used
        /// </summary>
        public bool IsSandboxed { get; set; }
        /// <summary>
        /// The payment processor to use
        /// </summary>
        public int ProcessorType { get; set; }
        /// <summary>
        /// List of credentials used by the processor
        /// </summary>
        public Dictionary<string, string> Credentials { get; set; }
        /// <summary>
        /// The friendly name of the registration
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The client's TimeZone
        /// </summary>
        public int TimeZone { get; set; }
        /// <summary>
        /// Currency code of the payments
        /// </summary>
        public string Currency { get; set; }
        /// <summary>
        /// Payment Methods
        /// </summary>
        public List<APEPaymentMethods> PaymentMethods { get; set; }

    }

    /// <summary>
    /// APE Payment Methods
    /// </summary>
    public class APEPaymentMethods
    {
        /// <summary>
        /// ID
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// PaymentScreenType
        /// </summary>
        public int PaymentScreenType { get; set; }
        /// <summary>
        /// SubMethods
        /// </summary>
        public List<APEPaymentSubMethods> SubMethods { get; set; }
    }

    /// <summary>
    /// APE Payment SubMethods
    /// </summary>
    public class APEPaymentSubMethods
    {
        /// <summary>
        /// ID
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// APE response
    /// </summary>
    public class APERegistrationResponse
    {
        /// <summary>
        /// Registration key 
        /// </summary>
        public string RegistrationID { get; set; }
        /// <summary>
        /// URL of APE
        /// </summary>
        public string ApeURL { get; set; }
        /// <summary>
        /// Expiration of the registration id
        /// </summary>
        public DateTime Expiration { get; set; }
        /// <summary>
        /// Provider/Processor details
        /// </summary>
        public APEProviderInfo Provider { get; set; }

        /// <summary>
        /// The result from test
        /// </summary>
        public APEProviderTest ProviderTest { get; set; }
    }

    /// <summary>
    /// APE Processor/Provider Information
    /// </summary>
    public class APEProviderInfo
    {
        /// <summary>
        /// indicates if intergration is enabled
        /// </summary>
        public bool IntegrationEnabled { get; set; }
        /// <summary>
        /// Provider's name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// indicates if card reader is enabled
        /// </summary>
        public bool EnableCardReader { get; set; }
        /// <summary>
        /// indicates if vaulting is enabled
        /// </summary>
        public bool EnableVault { get; set; }
    }

    /// <summary>
    /// APE Test Result
    /// </summary>
    public class APEProviderTest
    {
        /// <summary>
        /// Indicates whether the test was a success
        /// </summary>
        public bool? Success { get; set; }

        /// <summary>
        /// http response code from server during test
        /// </summary>
        public int ResponseCode { get; set; }

        /// <summary>
        /// messages from the response
        /// </summary>
        public string Message { get; set; }
    }
}
