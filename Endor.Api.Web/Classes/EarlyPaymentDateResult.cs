﻿using System;

namespace Endor.Api.Web.Classes
{
    public class EarlyPaymentDateResult
    {
        public DateTime? EarlyPaymentDate { get; set; }
    }
}
