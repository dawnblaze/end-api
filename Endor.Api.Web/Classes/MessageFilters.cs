using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Endor.Models;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// MessageFilters
    /// </summary>
    public class MessageFilters : IQueryFilters<MessageHeader>
    {
        /// <summary>
        /// short - required
        /// </summary>
        public short EmployeeID { get; set; } = 0;
        /// <summary>
        /// true(1), false(0), only(2)
        /// </summary>
        public TrueFalseOnly IncludeRead { get; set; } = TrueFalseOnly.False;
        /// <summary>
        /// true(1), false(0), only(2)
        /// </summary>
        public TrueFalseOnly IncludeSent { get; set; } = TrueFalseOnly.False;
        /// <summary>
        /// true(1), false(0), only(2)
        /// </summary>
        public TrueFalseOnly IncludeDeleted { get; set; } = TrueFalseOnly.False;

        /// <summary>
        /// WherePredicates
        /// </summary>
        public Expression<Func<MessageHeader, bool>>[] WherePredicates()
        {
            List<Expression<Func<MessageHeader, bool>>> predicates = new List<Expression<Func<MessageHeader, bool>>>();
            predicates.Add(header => 
                (this.IncludeRead==TrueFalseOnly.True || (this.IncludeRead==TrueFalseOnly.Only && header.IsRead) || (this.IncludeRead==TrueFalseOnly.False && !header.IsRead)) &&
                (this.IncludeSent==TrueFalseOnly.True || (this.IncludeSent==TrueFalseOnly.Only && header.InSentFolder) || (this.IncludeSent==TrueFalseOnly.False && !header.InSentFolder)) &&
                (this.IncludeDeleted==TrueFalseOnly.True || (this.IncludeDeleted==TrueFalseOnly.Only && header.IsDeleted) || (this.IncludeDeleted==TrueFalseOnly.False && !header.IsDeleted)) &&
                (header.EmployeeID == this.EmployeeID)
            );

            return predicates.ToArray();
        }
    }
}