﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using System.Text;
using System.Web;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Class to handle Etag Cache Management
    /// </summary>
    public class EtagCache
    {

        Dictionary<string, Dictionary<short, string>> _cache;

        /// <summary>
        /// Ctor for EtagCache
        /// </summary>
        public EtagCache()
        {
            this._cache = new Dictionary<string, Dictionary<short, string>>();
        }

        /// <summary>
        /// Check cache against Etag to determine if new data is needed
        /// </summary>
        public Object GetResult(EtagKey enumKey, short? BID, HttpRequest request, HttpResponse response, Func<object> action)
        {
            //if the value is missing
            var value = request.Headers["If-None-Match"];
            string key = Enum.GetName(typeof(EtagKey), enumKey);
            string queryString = request.QueryString.ToString();
            if (String.IsNullOrWhiteSpace(queryString))
            {
                key += ("?" + queryString);
            }

            var checkCache = CheckCache(key, BID.GetValueOrDefault(), value.FirstOrDefault());
            if ((!BID.HasValue) || !checkCache)
            {
                object data = action();
                var hash = GenerateHash(data);
                if (BID.HasValue) AddValueToCache(key, BID.GetValueOrDefault(), hash);
                response.Headers[HeaderNames.ETag] = hash;
                response.Headers["Access-Control-Expose-Headers"] = "ETag";
                return data;
            }
            else if (checkCache)
            {
                //cache needs update?
                object data = action();
                var newHash = GenerateHash(data);
                var oldHash = GetHashByKeyAndBID(key, BID.GetValueOrDefault());
                if (newHash != oldHash)
                {
                    if (BID.HasValue) AddValueToCache(key, BID.GetValueOrDefault(), newHash);
                    response.Headers[HeaderNames.ETag] = newHash;
                    response.Headers["Access-Control-Expose-Headers"] = "ETag";
                    return data;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        private string GetHashByKeyAndBID(string key, short BID)
        {
            if (!_cache.ContainsKey(key)) return String.Empty;
            if (!_cache[key].ContainsKey(BID)) return String.Empty;
            return _cache[key][BID];
        }

        /// <summary>
        /// Invalidate the cache entry
        /// </summary>
        public void InvalidateCacheEntry(EtagKey key, short BID)
        {
            foreach (var existingKey in _cache.Keys)
            {
                string keyToCheck = Enum.GetName(typeof(EtagKey), key);
                if (existingKey.StartsWith(keyToCheck))
                {
                    if (_cache[keyToCheck].ContainsKey(BID))
                    {
                        _cache[keyToCheck].Remove(BID);
                    }
                }
            }           
        }

        private bool CheckCache(string key, short BID, string value)
        {
            if (!_cache.ContainsKey(key)) return false;
            if (!_cache[key].ContainsKey(BID)) return false;
            return _cache[key][BID] == value;
        }

        private void AddValueToCache(string key, short BID, string value)
        {
            if (!_cache.ContainsKey(key)) _cache[key] = new Dictionary<short, string>();
            _cache[key][BID] = value;
        }

        private string GenerateHash(object data)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            var bytes = sha.ComputeHash(WriteFromObject(data));
            return HttpUtility.HtmlEncode(Convert.ToBase64String(bytes));
        }

        private static byte[] WriteFromObject(object data)
        {  
            // Serializer the User object to the stream.  
            string json = JsonConvert.SerializeObject(data, Formatting.Indented);
            return Encoding.ASCII.GetBytes(json);
        }

    }

    /// <summary>
    /// Keys for Etag Cache 
    /// </summary>
    public enum EtagKey
    {
        /// <summary>
        /// Business Member Dictionary for AEL
        /// </summary>
        AELBusinessMemberDictionary,
        /// <summary>
        /// Operator Dictionary for AEL
        /// </summary>
        AELOperatorDictionary,
        /// <summary>
        /// Data Type Dicitonary for AEL
        /// </summary>
        AELDataTypeDictionary
    }
}
