using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Simple Employee Filters for HTTP GET Methods
    /// </summary>
    public class SimpleEmployeeFilters : IQueryFilters<SimpleEmployeeData>
    {
        /// <summary>
        /// filter base on ID
        /// </summary>
        public ICollection<short> ID { get; set; }

        /// <summary>
        /// filter base on User LocationID
        /// </summary>
        public byte? LocationID { get; set; }

        /// <summary>
        /// Query Filter for filters on Employee
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimpleEmployeeData, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<SimpleEmployeeData, bool>>>();

            if ((this.ID?.Count ?? 0) > 0)
            {
                predicates.Add(emp => this.ID.Contains(emp.ID));
            }

            // END-12029
            predicates.Add(emp => emp.IsActive == true);

            // FIX ME ON THE MERGE 
            //if (LocationID.HasValue) predicates.Add(emp => emp.LocationID == LocationID.Value);
            // FIX ME ON THE MERGE 

            return predicates.ToArray();
        }
    }
}
