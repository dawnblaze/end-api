﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Dashboard Filters
    /// </summary>
    public class DashboardFilters : IQueryFilters<DashboardData>
    {
        /// <summary>
        /// Dashboard Filters
        /// </summary>
        public DashboardFilters()
        {
            if (!this.IsActive.HasValue)
                this.IsActive = true;

            if (!this.IncludeAll.HasValue)
                this.IncludeAll = false;
        }

        /// <summary>
        /// Module to Filter on. If null, returns Dashboards for all Modules.
        /// </summary>
        public Module? Module { get; set; }
        /// <summary>
        /// Flag to determine if only active Dashboards are returned when true or null.
        /// If false, both Active and Inactive are returned.
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// IncludeAllUsers → When true, includes all Dashboards for all users, whether shared or not. 
        /// <para>This requires Administrative access rights and is used in the Dashboard management screens.</para>
        /// </summary>
        public bool? IncludeAll { get; set; }

        /// <summary>
        /// Query Filter for filters on Dashboards
        /// </summary>
        /// <returns></returns>
        [Obsolete("Use the overloaded method that takes a UserID",true)]
        public Expression<Func<DashboardData, bool>>[] WherePredicates() { return null; }

        /// <summary>
        /// Query Filter for filters on Dashboards
        /// </summary>
        /// <param name="userLinkID">The calling User's ID</param>
        /// <returns></returns>
        public Expression<Func<DashboardData, bool>>[] WherePredicates(int? userLinkID)
        {
            if (!this.IncludeAll.HasValue)
                this.IncludeAll = false;

            var predicates = new List<Expression<Func<DashboardData, bool>>>();

            if (this.Module.HasValue)
                predicates.Add(dd => dd.Module == this.Module);

            if (!this.IsActive.HasValue || this.IsActive.Value)
                predicates.Add(dd => dd.IsActive == this.IsActive);

            if ((!this.IncludeAll.HasValue || !this.IncludeAll.Value) && userLinkID.HasValue)
            {
                predicates.Add(dd => dd.UserLinkID == userLinkID.Value);
            }

            return predicates.ToArray();
        }
    }

    /// <summary>
    /// Simple Dashboard Filters
    /// </summary>
    public class SimpleDashboardFilters : IQueryFilters<DashboardData>
    {
        /// <summary>
        /// Simple Dashboard Filters
        /// </summary>
        public SimpleDashboardFilters()
        {
            if (!this.IsActive.HasValue)
                this.IsActive = true;

            if (!this.IncludeAll.HasValue)
                this.IncludeAll = false;
        }

        /// <summary>
        /// Module to Filter on. If null, returns Dashboards for all Modules.
        /// </summary>
        public Module? Module { get; set; }
        /// <summary>
        /// Flag to determine if only active Dashboards are returned when true or null.
        /// If false, both Active and Inactive are returned.
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// Filters on the specified UserLinkID
        /// </summary>
        public bool? IncludeAll { get; set; }

        /// <summary>
        /// Query Filter for filters on Dashboards
        /// </summary>
        /// <returns></returns>
        [Obsolete("Use the overloaded method that takes a UserID", true)]
        public Expression<Func<DashboardData, bool>>[] WherePredicates() { return null; }

        /// <summary>
        /// Query Filter for filters on Board Definitions
        /// </summary>
        /// <returns></returns>
        public Expression<Func<DashboardData, bool>>[] WherePredicates(int? userID)
        {
            if (!this.IncludeAll.HasValue)
                this.IncludeAll = false;

            var predicates = new List<Expression<Func<DashboardData, bool>>>();

            if (!this.IsActive.HasValue || this.IsActive.Value)
                predicates.Add(dd => dd.IsActive == this.IsActive);

            if ((!this.IncludeAll.HasValue || !this.IncludeAll.Value) && userID.HasValue)
            {
                predicates.Add(dd => dd.UserLinkID == userID.Value);
            }

            return predicates.ToArray();
        }
    }
}
