﻿using Endor.Api.Web.Classes;
using Endor.Api.Web.Services;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Filters for employee role get endpoint
    /// </summary>
    public class EmployeeRoleFilters : IQueryFilters<EmployeeRole>
    {
        /// <summary>
        /// Filter for only active employee roles
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// Filter base on Name
        /// </summary>
        public string Name { get; set; }        

        /// <summary>
        /// Filter for getting employee roles that apply to a type of Object
        /// </summary>
        public EmployeeRoleType Type { get; set; }
        /// <summary>
        /// Returns an array of Expressions to pass as where clause parameters
        /// </summary>
        /// <returns></returns>
        public Expression<Func<EmployeeRole, bool>>[] WherePredicates()
        {
            List<Expression<Func<EmployeeRole, bool>>> predicates = new List<Expression<Func<EmployeeRole, bool>>>();

            if(this.IsActive != null)
                predicates.Add(t => t.IsActive);
            
            predicates.Add(erole => erole.Name.Contains((this.Name==null? "":this.Name)));

            predicates.Add(GetTypePredicate(Type));
            return predicates.ToArray();
        }

        /// <summary>
        /// Gets a type predicate expression based on the EmployeeRoleType specified
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Expression<Func<EmployeeRole, bool>> GetTypePredicate(EmployeeRoleType type)
        {
            switch (type)
            {
                case EmployeeRoleType.Team:
                    return t => t.AllowOnTeam;
                case EmployeeRoleType.Order:
                    return t => t.OrderRestriction > 0;
                case EmployeeRoleType.OrderItem:
                    return t => t.OrderItemRestriction > 0;
                case EmployeeRoleType.OrderDestination:
                    return t => t.OrderDestinationRestriction > 0;
                case EmployeeRoleType.Estimate:
                    return t => t.EstimateRestriction > 0;
                case EmployeeRoleType.EstimateItem:
                    return t => t.EstimateItemRestriction > 0;
                case EmployeeRoleType.EstimateDestination:
                    return t => t.EstimateDestinationRestriction > 0;
                case EmployeeRoleType.Opportunity:
                    return t => t.OpportunityRestriction > 0;
                case EmployeeRoleType.PO:
                    return t => t.PORestriction > 0;
                case EmployeeRoleType.All:
                default:
                    break;
            }

            return t => true;
        }

        /// <summary>
        /// Return true if any filters are present
        /// </summary>
        internal bool HasFilters => WherePredicates().Length > 0;
    }

    /// <summary>
    /// Describes the types of objects that the employee role is connected to
    /// </summary>
    public enum EmployeeRoleType
    {
        /// <summary>
        /// No filtering applied
        /// </summary>
        All,
        /// <summary>
        /// Filter where allowOnTeam
        /// </summary>
        Team,
        /// <summary>
        /// Filter where OrderRestriction > 0
        /// </summary>
        Order,
        /// <summary>
        /// Filter where OrderItemRestriction > 0
        /// </summary>
        OrderItem,
        /// <summary>
        /// Filter where OrderDestinationRestriction > 0
        /// </summary>
        OrderDestination,
        /// <summary>
        /// Filter where EstimateRestriction > 0
        /// </summary>
        Estimate,
        /// <summary>
        /// Filter where EstimateItemRestriction > 0
        /// </summary>
        EstimateItem,
        /// <summary>
        /// Filter where EstimateDestinationRestriction > 0
        /// </summary>
        EstimateDestination,
        /// <summary>
        /// Filter where OpportunityRestriction > 0
        /// </summary>
        Opportunity,
        /// <summary>
        /// Filter where PORestriction > 0
        /// </summary>
        PO
    }
}
