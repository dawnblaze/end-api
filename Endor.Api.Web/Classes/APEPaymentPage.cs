﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Base Payment Page Request
    /// </summary>
    public class APEPaymentPageRequest
    {
        /// <summary>
        /// ID
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// TransactionType
        /// </summary>
        public string TransactionType { get; set; }
        /// <summary>
        /// Amount
        /// </summary>
        public decimal? Amount { get; set; }
        /// <summary>
        /// AmountToCredit
        /// </summary>
        public decimal? AmountToCredit { get; set; }
        /// <summary>
        /// CanAlterAmount
        /// </summary>
        public bool? CanAlterAmount { get; set; }
        /// <summary>
        /// MinAmount
        /// </summary>
        public decimal? MinAmount { get; set; }
        /// <summary>
        /// MaxAmount
        /// </summary>
        public decimal? MaxAmount { get; set; }
        /// <summary>
        /// CanUseCompanyCredit
        /// </summary>
        public bool? CanUseCompanyCredit { get; set; }
        /// <summary>
        /// CompanyCreditBalance
        /// </summary>
        public decimal? CompanyCreditBalance { get; set; }
        /// <summary>
        /// CompanyID
        /// </summary>
        public int? CompanyID { get; set; }
        /// <summary>
        /// CompanyName
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// LocationID
        /// </summary>
        public int? LocationID { get; set; }
        /// <summary>
        /// LocationName
        /// </summary>
        public string LocationName { get; set; }
        /// <summary>
        /// ContactID
        /// </summary>
        public int? ContactID { get; set; }
        /// <summary>
        /// Contacts
        /// </summary>
        public APEPaymentPageRequestContacts[] Contacts { get; set; }
        /// <summary>
        /// Orders
        /// </summary>
        public APEPaymentPageRequestOrders[] Orders { get; set; }
        /// <summary>
        /// ShowCardReader
        /// </summary>
        public bool? ShowCardReader { get; set; }
        /// <summary>
        /// ShowPaymentTypes
        /// </summary>
        public bool? ShowPaymentTypes { get; set; }
        /// <summary>
        /// ShowVault
        /// </summary>
        public bool? ShowVault { get; set; }
        /// <summary>
        /// CanAddToVault
        /// </summary>
        public bool CanAddToVault { get; set; }
    }

    /// <summary>
    /// Payment Page Orders
    /// </summary>
    public class APEPaymentPageRequestOrders
    {
        /// <summary>
        /// OrderID
        /// </summary>
        public string OrderID { get; set; }
        /// <summary>
        /// Amount
        /// </summary>
        public decimal? Amount { get; set; }
        /// <summary>
        /// PO Number
        /// </summary>
        public string PONumber { get; set; }
        /// <summary>
        /// Tax Amount
        /// </summary>
        public decimal? TaxAmount { get; set; }
        /// <summary>
        /// Freight Amount
        /// </summary>
        public decimal? FreightAmount { get; set; }
        /// <summary>
        /// Order Date
        /// </summary>
        public DateTime? OrderDate { get; set; }
        /// <summary>
        /// Ship To Address
        /// </summary>
        public APEPaymentAddress ShipToAddress { get; set; }
        /// <summary>
        /// Ship From Address
        /// </summary>
        public APEPaymentAddress ShipFromAddress { get; set; }
        /// <summary>
        /// Array of line item info
        /// </summary>
        public APEPaymentLineItem[] Items { get; set; }
    }

    /// <summary>
    /// Payment Page Contacts
    /// </summary>
    public class APEPaymentPageRequestContacts
    {
        /// <summary>
        /// ContactID
        /// </summary>
        public string ContactID { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// Address details of a payment
    /// </summary>
    public class APEPaymentAddress
    {
        /// <summary>
        /// Street Address
        /// </summary>
        public string StreetAddress { get; set; }
        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// State
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// Postal Code
        /// </summary>
        public string PostalCode { get; set; }
        /// <summary>
        /// Country
        /// </summary>
        public string Country { get; set; }
    }

    /// <summary>
    /// Line item details of a payment
    /// </summary>
    public class APEPaymentLineItem
    {
        /// <summary>
        /// LineNumber
        /// </summary>
        public string LineNumber { get; set; }
        /// <summary>
        /// Product
        /// </summary>
        public string Product { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// UPC
        /// </summary>
        public string UPC { get; set; }
        /// <summary>
        /// Quantity
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// Units
        /// </summary>
        public string Units { get; set; }
        /// <summary>
        /// UnitPrice
        /// </summary>
        public decimal UnitPrice { get; set; }
        /// <summary>
        /// SubtotalPrice
        /// </summary>
        public decimal SubtotalPrice { get; set; }
        /// <summary>
        /// Discount
        /// </summary>
        public decimal Discount { get; set; }
        /// <summary>
        /// TaxAmount
        /// </summary>
        public decimal TaxAmount { get; set; }
    }

    /// <summary>
    /// APIPaymentPageRequest model used for Endor.Api
    /// This model is currently used for requesting payment page for multiple orders.
    /// </summary>
    public class APIPaymentPageRequest
    {
        /// <summary>
        /// Registration ID requested from APE
        /// </summary>
        public string RegistrationID { get; set; }
        /// <summary>
        /// Amount
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// AmountToCredit
        /// </summary>
        public decimal? AmountToCredit { get; set; }
        /// <summary>
        /// CanAlterAmount
        /// </summary>
        public bool? CanAlterAmount { get; set; }
        /// <summary>
        /// MinAmount
        /// </summary>
        public decimal? MinAmount { get; set; }
        /// <summary>
        /// MaxAmount
        /// </summary>
        public decimal? MaxAmount { get; set; }
        /// <summary>
        /// CanUseCompanyCredit
        /// </summary>
        public bool? CanUseCompanyCredit { get; set; }
        /// <summary>
        /// CompanyCreditBalance
        /// </summary>
        public decimal? CompanyCreditBalance { get; set; }
        /// <summary>
        /// The multiple order ids
        /// </summary>
        public APIPaymentPageRequestOrders[] Orders { get; set; }

        /// <summary>
        /// Company ID, use only when orders are empty
        /// </summary>
        public int? CompanyID { get; set; }
        /// <summary>
        /// Location ID, use only when orders are empty
        /// </summary>
        public int? LocationID { get; set; }
    }

    /// <summary>
    /// APIPaymentPageRequestOrders model used for Endor.Api
    /// </summary>
    public class APIPaymentPageRequestOrders
    {
        /// <summary>
        /// OrderID
        /// </summary>
        public int OrderID { get; set; }
        /// <summary>
        /// Amount
        /// </summary>
        public decimal? Amount { get; set; }
     }
}
