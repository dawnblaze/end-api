﻿namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// an object giving the employees current time clock status, 
    /// indicating if they are clocked in.  
    /// If they are clocked on to a detail, the station and TimeCardDetail ID.
    /// </summary>
    public class TimeCardInfoData
    {
        /// <summary>
        /// the ID of the timecard
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// the ID of the timecard detail
        /// </summary>
        public int DetailID { get; set; }
        /// <summary>
        /// the status of timecard
        /// </summary>
        public string StatusType { get; set; }
        /// <summary>
        /// the status name of timecard
        /// </summary>
        public string StatusName { get; set; }
    }
}