﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters for EmailAccount entity
    /// </summary>>
    public class SimpleEmailAccountFilters : IQueryFilters<SimpleEmailAccountData>
    {
        /// <summary>
        /// If only Active records should be returned.
        /// When not provided, returns both Active and InActive.
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimpleEmailAccountData, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<SimpleEmailAccountData, bool>>>();

            if (this.IsActive.HasValue)
                predicates.Add(de => de.IsActive == this.IsActive);
            else
                predicates.Add(de => de.IsActive == false);

            return predicates.ToArray();
        }

        /// <summary>
        /// Return true if any filters are present
        /// </summary>
        internal bool HasFilters => WherePredicates().Length > 0;
    }
}
