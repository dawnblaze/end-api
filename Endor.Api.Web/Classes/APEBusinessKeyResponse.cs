﻿using System;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Business Key Registration Response class
    /// </summary>
    public class APEBusinessKeyResponse
    {
        /// <summary>
        /// When successful, this specifies the business key.
        /// </summary>
        public Guid BusinessKey { get; set; }
    }
}
