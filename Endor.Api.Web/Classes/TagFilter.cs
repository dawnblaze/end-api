﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// filters for Tag endpoints
    /// </summary>
    public class TagFilter : IQueryFilters<ListTag>
    {

        /// <summary>
        /// optional parent ID filter
        /// </summary>
        public int? AssociatedID { get; set; }

        /// <summary>
        /// optional parent ID filter
        /// </summary>
        public int? AssociatedClassTypeID { get; set; }

        /// <summary>
        /// a filter for the name of the tag
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// query filter for filters on this object - this should never be used
        /// </summary>
        /// <returns></returns>
        public Expression<Func<ListTag, bool>>[] WherePredicates()
        {
            List<Expression<Func<ListTag, bool>>> predicates = new List<Expression<Func<ListTag, bool>>>();

            if (this.Name.HasValue())
            {
                predicates.Add(t => t.Name.ToLowerInvariant().Contains(this.Name.ToLower()));
            }

            return predicates.ToArray();
        }
    }
}
