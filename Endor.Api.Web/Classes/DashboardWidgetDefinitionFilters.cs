﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Dashboard Widget Definition Filters
    /// </summary>
    public class DashboardWidgetDefinitionFilters : IQueryFilters<DashboardWidgetDefinition>
    {
        /// <summary>
        /// The modules associated with the Widget Definition
        /// </summary>
        public short? Modules { get; set; }

        /// <summary>
        /// The category to filter for.  
        /// </summary>
        public string[] CategoryType { get; set; }

        /// <summary>
        /// Query Filter for filters on Dashboards
        /// </summary>
        /// <returns></returns>
        public Expression<Func<DashboardWidgetDefinition, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<DashboardWidgetDefinition, bool>>>();

            if (Modules.HasValue)
                predicates.Add(wd => ((!wd.Modules.HasValue) || ((wd.Modules.Value | Modules.Value) != 0)));

            if ((CategoryType!=null)&&(CategoryType.Length > 0))
                predicates.Add(wd => wd.WidgetCategoryLinks.Any(x => CategoryType.ToList().Contains(x.CategoryType.ToString())));

            return predicates.ToArray();
        }
    }

    /// <summary>
    /// Dashboard Widget Definition Category Filters
    /// </summary>
    public class DashboardWidgetDefinitionCategoryFilters : IQueryFilters<DashboardWidgetDefinition>
    {
        /// <summary>
        /// The modules associated with the Widget Definition
        /// </summary>
        public short? Modules { get; set; }

        /// <summary>
        /// Query Filter for filters on Dashboards
        /// </summary>
        /// <returns></returns>
        public Expression<Func<DashboardWidgetDefinition, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<DashboardWidgetDefinition, bool>>>();

            if (Modules.HasValue)
                predicates.Add(wd => ((!wd.Modules.HasValue)||((wd.Modules.Value | Modules.Value)!=0)));

            return predicates.ToArray();
        }
    }
}
