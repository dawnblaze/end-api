﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters for URLRegistrationDat entity
    /// </summary>>
    public class URLRegistrationDataFilter : IQueryFilters<URLRegistrationData>
    {
        /// <summary>
        /// Flag to determine if only active Dashboards are returned when true or null.
        /// If false, both Active and Inactive are returned.
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// Query Filter for filters on URLRegistrationData
        /// </summary>
        /// <returns></returns>
        [Obsolete("Use the overloaded method that takes a UserID", true)]
        public Expression<Func<URLRegistrationData, bool>>[] WherePredicates() { return null; }

        /// <summary>
        /// Query Filter for filters on URLRegistrationData
        /// </summary>
        /// <param name="userID">The calling User's ID</param>
        /// <returns></returns>
        public Expression<Func<URLRegistrationData, bool>>[] WherePredicates(int? userID)
        {
            var predicates = new List<Expression<Func<URLRegistrationData, bool>>>();

            if (!this.IsActive.HasValue || this.IsActive.Value)
                predicates.Add(dd => dd.IsActive);

            return predicates.ToArray();
        }
    }
}
