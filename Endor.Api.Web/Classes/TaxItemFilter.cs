﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters object for simple list endpoint of TaxItem controller
    /// </summary>
    public class TaxItemFilter : IQueryFilters<TaxItem>
    {
        /// <summary>
        /// Filters simplelist of TaxItem based on `IsActive` property
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// Optional filter for Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Optional filter for Searching TaxItem
        /// </summary>
        public string Search { get; set; }

        /// <summary>
        /// Optional filter for TaxEngineType
        /// </summary>
        public TaxEngineType? taxEngineType { get; set; }

        /// <summary>
        /// query filter for filters on this object
        /// </summary>
        /// <returns></returns>
        public Expression<Func<TaxItem, bool>>[] WherePredicates()
        {
            List<Expression<Func<TaxItem, bool>>> predicates = new List<Expression<Func<TaxItem, bool>>>();

            if (IsActive.HasValue)
                predicates.Add(taxItem => taxItem.IsActive == IsActive.Value);

            if (taxEngineType.HasValue)
                predicates.Add(taxItem => taxItem.TaxEngineType == taxEngineType.Value);
            else
                predicates.Add(taxItem => taxItem.TaxEngineType == TaxEngineType.Internal);

            if (!string.IsNullOrWhiteSpace(this.Name))
                predicates.Add(taxItem => taxItem.Name.ToLower().Contains(this.Name.Trim().ToLower()));

            if (!string.IsNullOrWhiteSpace(this.Search))
                predicates.Add(taxItem => taxItem.Name.ToLower().Contains(this.Search.Trim().ToLower())
                || taxItem.TaxRate.ToString().ToLower().Contains(this.Search.Trim().ToLower()));

            return predicates.ToArray();
        }
    }
}
