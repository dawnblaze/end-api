﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters object for simple list endpoint of order item substatus controller
    /// </summary>
    public class SimpleOrderItemSubStatusFilters : IQueryFilters<OrderItemSubStatus>, IQueryFilterMapper<OrderItemSubStatus, SimpleOrderItemSubStatus>
    {
        /// <summary>
        /// optional filter for orderitemstatusID
        /// </summary>
        public short? OrderItemStatusID { get; set; }

        /// <summary>
        /// maps a full order item substatus to a simple object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public IEnumerable<SimpleOrderItemSubStatus> Map(IQueryable<OrderItemSubStatus> source)
        {
            return source.Select((OrderItemSubStatus model) => new SimpleOrderItemSubStatus()
            {
                BID = model.BID,
                ID = model.ID,
                ClassTypeID = model.ClassTypeID,
                IsActive = true,
                DisplayName = model.Name,
                IsDefault = false,
                //OrderStatusID = model.OrderStatusID ?? 0,
            });
        }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<OrderItemSubStatus, bool>>[] WherePredicates()
        {
            List<Expression<Func<OrderItemSubStatus, bool>>> predicates = new List<Expression<Func<OrderItemSubStatus, bool>>>();

            if (OrderItemStatusID.HasValue)
            {
                predicates.Add(oisc => (oisc.OrderItemStatusSubStatusLinks != null) && oisc.OrderItemStatusSubStatusLinks.Where(ois => ois.StatusID == OrderItemStatusID).Any());
            }

            return predicates.ToArray();
        }
    }
}
