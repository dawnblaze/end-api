using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// filter object for IsActive on GET routes
    /// </summary>
    public class LaborCategoryFilter : IIsActiveQueryFilter<LaborCategory>
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public Expression<Func<LaborCategory, bool>>[] WherePredicates()
        {
            List<Expression<Func<LaborCategory, bool>>> predicates = new List<Expression<Func<LaborCategory, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(item => item.IsActive == IsActive.Value);
            }

            return predicates.ToArray();
        }
    }
}
