﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Filters for custom field values
    /// </summary>
    public class CustomFieldValueFilters
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IncludeNull { get; set; }

    }
}
