﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderItemComponentCostRequestEntry
    {
        /// <summary>
        /// 
        /// </summary>
        public int? MaterialID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? LaborID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public short? MachineID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? AssemblyID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Quantity { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class OrderItemComponentCostRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public List<OrderItemComponentCostRequestEntry> Components { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class OrderItemComponentUnitCost
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal UnitCost { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal CostNet { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class OrderItemComponentCostResponse
    {
        /// <summary>
        /// 
        /// </summary>
        public OrderItemComponentUnitCost Material { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public OrderItemComponentUnitCost Labor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public OrderItemComponentUnitCost Machine { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public OrderItemComponentUnitCost Assembly { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal UnitCostSum { get { return Material.UnitCost + Labor.UnitCost + Machine.UnitCost + Assembly.UnitCost; } }
        /// <summary>
        /// 
        /// </summary>
        public decimal CostNetSum { get { return Material.CostNet + Labor.CostNet + Machine.CostNet + Assembly.CostNet; } }
    }
}
