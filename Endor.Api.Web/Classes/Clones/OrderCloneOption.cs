﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Models;

namespace Endor.Api.Web.Classes.Clones
{
    public class OrderCloneOption
    {
        public DateTime DueDate { get; set; }

        public DateTime FollowupDate { get; set; }

        public OrderOrderStatus? OrderStatus { get; set; }

        public bool? KeepFiles { get; set; }

        public bool? KeepNotes { get; set; }

        public bool? UpdatePricing { get; set; }
    }
}
