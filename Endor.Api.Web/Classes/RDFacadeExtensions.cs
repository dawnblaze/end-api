﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// The purpose of this extension is to re-create the `dbContext.Database.SqlQuery` method that is missing on EF 7.0. 
    /// Added with GetModelFromQueryAsync&gt;T&lt; to reflect the class model
    /// 
    /// Source: https:github.comaspnetEntityFrameworkCoreissues/1862
    /// </summary>
    public static class RDFacadeExtensions
    {
        /// <summary>
        /// executes a sql command async using the DatabaseFacade
        /// </summary>
        /// <param name="databaseFacade"></param>
        /// <param name="sql"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static async Task<RelationalDataReader> ExecuteSqlRawAsync2(this DatabaseFacade databaseFacade,
            string sql,
            CancellationToken cancellationToken = default(CancellationToken),
            params object[] parameters)
        {
            var concurrencyDetector = databaseFacade.GetService<IConcurrencyDetector>();

            using (concurrencyDetector.EnterCriticalSection())
            {
                var rawSqlCommand = databaseFacade
                    .GetService<IRawSqlCommandBuilder>()
                    .Build(sql, parameters);

                try
                {
                    return await rawSqlCommand
                        .RelationalCommand
                        .ExecuteReaderAsync(
                            new RelationalCommandParameterObject(
                                databaseFacade.GetService<IRelationalConnection>(),
                                rawSqlCommand.ParameterValues,
                                null, null
                                ),
                            cancellationToken: cancellationToken);
                }
                catch(Exception ex)
                {
                    var xxx = ex;
                    return await Task.FromResult<RelationalDataReader>(null);
                }

                
            }
        }

        /// <summary>
        /// Get Model for the given query
        /// </summary>
        /// <param name="databaseFacade"></param>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<T>> GetModelFromQueryAsync<T>(this DatabaseFacade databaseFacade, string sql, params object[] parameters)
            where T : new()
        {
            var result = await databaseFacade.ExecuteSqlRawAsync2(sql, parameters: parameters);
            using (DbDataReader dr = result.DbDataReader)
            {
                List<T> lst = new List<T>();
                PropertyInfo[] props = typeof(T).GetProperties();
                while (dr.Read())
                {
                    T t = new T();
                    IEnumerable<string> actualNames = dr.GetColumnSchema().Select(o => o.ColumnName);
                    for (int i = 0; i < props.Length; ++i)
                    {
                        PropertyInfo pi = props[i];

                        if (!pi.CanWrite) continue;

                        System.ComponentModel.DataAnnotations.Schema.ColumnAttribute ca = pi.GetCustomAttribute(typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute)) as System.ComponentModel.DataAnnotations.Schema.ColumnAttribute;
                        string name = ca?.Name ?? pi.Name;

                        if (pi == null) continue;

                        if (!actualNames.Contains(name))
                        {
                            continue;
                        }
                        object value = dr[name];
                        Type pt = pi.DeclaringType;
                        bool nullable = pt.GetTypeInfo().IsGenericType && pt.GetGenericTypeDefinition() == typeof(Nullable<>);
                        if (value == DBNull.Value)
                        {
                            value = null;
                        }
                        if (value == null && pt.GetTypeInfo().IsValueType && !nullable)
                        {
                            value = Activator.CreateInstance(pt);
                        }
                        pi.SetValue(t, value);
                    }//for i
                    lst.Add(t);
                }//while
                return lst;
            }//using dr
        }
    }
}