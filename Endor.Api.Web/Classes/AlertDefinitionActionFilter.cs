﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters for AlertDefinition entity
    /// </summary>>
    public class AlertDefinitionActionFilter : IQueryFilters<AlertDefinitionAction>
    {
        /// <summary>
        /// If supplied, filters Alert Definition Actions for the specified AlertID.
        /// </summary>
        public int? AlertDefinitionID { get; set; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<AlertDefinitionAction, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<AlertDefinitionAction, bool>>>();

            if (this.AlertDefinitionID.HasValue)
                predicates.Add(ada => ada.AlertDefinitionID == this.AlertDefinitionID);

            return predicates.ToArray();
        }
    }
}
