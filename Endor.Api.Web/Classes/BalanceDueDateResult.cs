﻿using System;

namespace Endor.Api.Web.Classes
{
    public class BalanceDueDateResult
    {
        public DateTime? BalanceDueDate { get; set; }
    }
}
