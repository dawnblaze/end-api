﻿using Endor.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Record of a custom field
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CustomFieldRecord<T> : IAtom<T> where T : struct, IConvertible
    {
        /// <summary>
        /// Business ID
        /// </summary>
        public short BID { get; set; }

        /// <summary>
        /// The ID of the CustomFieldRecord is the same as the ID of the record it is associated with. 
        /// </summary>
        public T ID { get; set; }

        /// <summary>
        /// The ClassTypeID of the CustomFieldRecord is usually 1 greater than the ClassTypeID of the record type it is associated with.
        /// </summary>
        public int ClassTypeID { get; set; }

        /// <summary>
        /// Modified Date/Time
        /// </summary>
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// The AppliesToClassTypeID is the ClassTypeID of the record type associated with this record.
        /// </summary>
        public int AppliesToClassTypeID { get; set; }

        /// <summary>
        /// JSON array of CFValues 
        /// example --> [ {"ID":123, "V":"Yellow"}, {"ID":152, "V":"322.5"} ]
        /// </summary>
        public string CFValuesJSON { get; set; }

        /// <summary>
        /// The CFValuesList is read only, and composed from the CFValuesJSON on access. 
        /// This value is used for convenient access to the underlying data.  All CRUD operations
        /// and serialization should be limited to the CFValuesJSON object.
        /// </summary>
        public List<CustomFieldValue> CFValues {
            get
            {
                return JsonConvert.DeserializeObject<List<CustomFieldValue>>(CFValuesJSON);
            }
        }

        /// <summary>
        /// procedure to validate all of the Custom Field Values assigned (and perhaps those required but missing!)
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool Validate(out string message)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// return the list of CFDs associated with the AppliesToClassTypeID for this record.
        /// </summary>
        /// <param name="IsActiveOnly"></param>
        /// <returns></returns>
        public List<CustomFieldDefinition> GetDefinitions(bool IsActiveOnly = true)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// return the list of all CFVs in JSON, including those that are NULL
        /// </summary>
        /// <param name="IsActiveOnly"></param>
        /// <returns></returns>
        public List<CustomFieldValue> GetAllCFValuesJSON(bool IsActiveOnly = true)
        {
            throw new NotImplementedException();
        }

    }
}
