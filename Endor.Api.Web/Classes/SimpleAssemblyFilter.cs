﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// filters for simple list endpoints on the company controller
    /// </summary>
    public class SimpleAssemblyFilters : IQueryFilters<SimpleAssemblyData>
    {
        /// <summary>
        /// option filter for IsActive
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// optional filter for AssemblyType
        /// </summary>
        public AssemblyType? AssemblyType { get; set; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimpleAssemblyData, bool>>[] WherePredicates()
        {
            List<Expression<Func<SimpleAssemblyData, bool>>> predicates = new List<Expression<Func<SimpleAssemblyData, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(a => a.IsActive == IsActive.Value);
            }

            if (AssemblyType != null)
            {
                predicates.Add(a => a.AssemblyType == AssemblyType);
            }
                
            return predicates.ToArray();
        }
    }
}
