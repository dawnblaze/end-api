﻿using System;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Response from a Dashboard Widget Query
    /// </summary>
    public class DashboardWidgetQueryResponse
    {
        /// <summary>
        /// Result object
        /// </summary>
        public object Result { get; set; }
        /// <summary>
        /// Result DateTime
        /// </summary>
        public DateTime ResultDT { get; set; }
        /// <summary>
        /// Result was from cache
        /// </summary>
        public bool FromCache { get; set; }
    }
}
