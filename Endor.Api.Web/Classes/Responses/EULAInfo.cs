﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Responses
{
    /// <summary>
    /// EULA Info
    /// </summary>
    public class EULAInfo
    {
        /// <summary>
        /// Subject
        /// </summary>
        public string Subject { get; set; } = "EULA Accepted";
        /// <summary>
        /// Username
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Accepted Date
        /// </summary>
        public DateTime AcceptedDT { get; set; }
        /// <summary>
        /// EmployeeID
        /// </summary>
        public short? EmployeeID { get; set; }
        /// <summary>
        /// ContactID
        /// </summary>
        public int? ContactID { get; set; }
        /// <summary>
        /// IP Address
        /// </summary>
        public string IP { get; set; }
        /// <summary>
        /// Computer Name
        /// </summary>
        public string ComputerName { get; set; }
        /// <summary>
        /// Browser
        /// </summary>
        public string Browser { get; set; }
    }
}
