﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Responses
{
    /// <summary>
    /// response object for /api/timecarddetail/statuses
    /// </summary>
    public class TimeCardDetailStatus
    {
        /// <summary>
        /// type of break
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// ID of status
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// name of status
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// is paid or not
        /// </summary>
        public bool IsPaid { get; set; }
    }
}
