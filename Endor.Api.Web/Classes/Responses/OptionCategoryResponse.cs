﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// response object for option category operations
    /// </summary>
    public class OptionCategoryResponse : IGenericResponse
    {
        // <summary>
        // Option categories
        // </summary>
        //public List<SystemOptionCategory> OptionCategories { get; set; }

        /// <summary>
        /// Option Sections
        /// </summary>
        public List<SystemOptionSection> OptionSections { get; set; }

        /// <summary>
        /// Response message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// If the action was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// true if has error, otherwise false
        /// </summary>
        public bool HasError { get { return !this.Success; } }

        /// <summary>
        /// error message encountered for this operation
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
