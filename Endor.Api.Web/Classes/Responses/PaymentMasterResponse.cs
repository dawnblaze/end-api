﻿using Endor.Api.Web.Classes.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Responses
{
    /// <summary>
    /// Payment Master Response
    /// </summary>
    public class PaymentMasterResponse
    {
        /// <summary>
        /// Payment Master ID
        /// </summary>
        public int ID;

        /// <summary>
        /// Payment Applications
        /// </summary>
        public List<PaymentApplicationRequest> Applications;
    }
}
