﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Responses
{
    public class HasAdjustmentsResponse
    {
        public HasAdjustmentsResponse()
        {
            Adjustments = new List<ReconciliationAdjustmentsIntenal>();
        }

        /// <summary>
        /// Has adjustments?
        /// </summary>
        public bool HasAdjustments;

        /// <summary>
        /// Possible Adjusments
        /// </summary>
        public IList<ReconciliationAdjustmentsIntenal> Adjustments;

        public class ReconciliationAdjustmentsIntenal
        {
            /// <summary>
            /// Location ID for adjustable reconcicliation
            /// </summary>
            public byte LocationID;

            /// <summary>
            /// Starting Date Time for adjustable reconcicliation
            /// </summary>
            public DateTime StartingDT;

            /// <summary>
            /// Ending Date Time for adjustable reconcicliation
            /// </summary>
            public DateTime EndingDT;
        }
    }
}
