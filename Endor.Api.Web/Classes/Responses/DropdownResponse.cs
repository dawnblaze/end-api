﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Responses
{
    public class DropdownResponse<T>
    {
        public string Label { get; set; }
        public string Tooltip { get; set; }
        public DataType DataType { get; set; }
        public ICollection<T> Options { get; set; }            
    }
}
