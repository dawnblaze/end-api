﻿using Endor.Api.Web.Classes.Request;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Responses
{
    /// <summary>
    /// Payment Response
    /// </summary>
    public class PaymentResponse
    {
        /// <summary>
        /// Payment Transaction Type ID
        /// </summary>
        public int PaymentTransactionType;

        /// <summary>
        /// Total Amount
        /// </summary>
        public decimal Amount;

        /// <summary>
        /// Currency Type ID (optional)
        /// </summary>
        public byte? CurrencyType;

        /// <summary>
        /// Token ID
        /// </summary>
        public string TokenID;

        /// <summary>
        /// Payment Display Number
        /// </summary>
        public string DisplayNumber;

        /// <summary>
        /// Applications
        /// </summary>
        public List<PaymentMasterResponse> MasterPayments;

        /// <summary>
        /// Payment Processor Response
        /// </summary>
        public PaymentProcessorResponse ProcessorResponse;

    }
}
