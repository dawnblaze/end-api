﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Responses
{
    /// <summary>
    /// 
    /// </summary>
    public class UserLinkEntityChangeResponse: EntityActionChangeResponse<short>
    {
        /// <summary>
        /// Endor.Models.UserLink
        /// </summary>
        public UserLink UserLink { get; set; }
    }
}
