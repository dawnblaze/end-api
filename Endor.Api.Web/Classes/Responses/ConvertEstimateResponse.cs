﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{ 

/// <summary>
/// Response to ConvertEstimate
/// </summary>{
public class ConvertEstimateResponse
    {

        /// <summary>
        /// Estimate ID
        /// </summary>
        public int EstimateID { get; set; }

        /// <summary>
        /// Order ID
        /// </summary>
        public int OrderID { get; set; }

        /// <summary>
        /// OrderNumber
        /// </summary>
        public string OrderNumber { get; set; }

        /// <summary>
        /// Price Change
        /// </summary>
        public bool PriceChange { get; set; }

        /// <summary>
        /// Old Price
        /// </summary>
        public decimal OldPrice { get; set; }

        /// <summary>
        /// New Price
        /// </summary>
        public decimal NewPrice { get; set; }


    }
}
