﻿using System;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// CanCreateUserResponse
    /// </summary>
    public class ContactStatusResponse
    {
        /// <summary>
        /// lead|prospect|customer
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// if last transaction is greater than or equal to 6 months
        /// </summary>
        public bool? isStale { get; set; }

        /// <summary>
        /// last created date of estimate/order
        /// </summary>
        public DateTime? lastCreated { get; set; }
    }
}
