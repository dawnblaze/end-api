﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Response for when an Entity has an action change attempt
    /// </summary>
    public class EntityActionChangeResponse<I> : IGenericResponse
        where I : struct, IConvertible
    {
        /// <summary>
        /// Entity Id
        /// </summary>
        public I Id { get; set; }

        /// <summary>
        /// Response message
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        /// <summary>
        /// If the action was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// If there is an error
        /// </summary>
        public bool HasError => !this.Success;

        /// <summary>
        /// Error message, if there is an error
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ErrorMessage { get; set; }
    }


    /// <summary>
    /// Response for when an Entity (with a Id of Integer) has an action change attempt
    /// </summary>
    public class EntityActionChangeResponse : EntityActionChangeResponse<int>
    {

    }
}
