﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Response for Getting an Option Value
    /// </summary>
    public class PutOptionValuesResponse : IGenericResponse
    {
        /// <summary>
        /// list of values updated
        /// </summary>
        public List<OptionValues> Values { get; set; }

        /// <summary>
        /// Response message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// If the action was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// true if has error, otherwise false
        /// </summary>
        public bool HasError { get { return !this.Success; } }

        /// <summary>
        /// error message encountered for this operation
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
