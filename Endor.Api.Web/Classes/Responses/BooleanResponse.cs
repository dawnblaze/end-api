using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Response for true/false return types
    /// </summary>
    public class BooleanResponse : IGenericResponse 
    {
        /// <summary>
        /// Response Value
        /// </summary>
        public bool? Value { get; set; }

        /// <summary>
        /// Response message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// If the action was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// If there is an error
        /// </summary>
        public bool HasError { get { return !this.Success; } }

        /// <summary>
        /// Error message, if there is an error
        /// </summary>
        public string ErrorMessage { get; set; }

    }
}
