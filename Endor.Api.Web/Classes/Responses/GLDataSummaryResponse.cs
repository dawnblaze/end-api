﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Responses
{
    /// <summary>
    /// Response for GLData summary
    /// </summary>
    public class GLDataSummaryResponse
    {
        /// <summary>
        /// GLData`s Account ID
        /// </summary>
        public int GLAccountID { get; set; }
        /// <summary>
        /// GLData`s Account Name
        /// </summary>
        public string GLAccountName { get; set; }
        /// <summary>
        /// GLData Sum of Amount
        /// </summary>
        public decimal Amount { get; set; }
    }
}
