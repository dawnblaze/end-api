﻿using Endor.Api.Web.Classes.Request;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Responses
{

    /// <summary>
    /// Payment ApplyCreditRequest
    /// </summary>
    public class PaymentApplyCreditResponse
    {
        /// <summary>
        /// Payment Transaction Type ID
        /// </summary>
        public PaymentTransactionType PaymentTransactionType;

        /// <summary>
        /// Applications
        /// </summary>
        public List<PaymentMasterResponse> MasterPayments;
    }
}
