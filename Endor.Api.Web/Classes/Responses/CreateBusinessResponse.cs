﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Response to create business
    /// </summary>
    public class CreateBusinessResponse
    {
        /// <summary>
        /// Business Id
        /// </summary>
        public short BID { get; set; }

        /// <summary>
        /// Response message
        /// </summary>
        public string Response { get; set; }

        /// <summary>
        /// Whether or not the action was successful
        /// </summary>
        public bool Success { get; set; }
    }
}
