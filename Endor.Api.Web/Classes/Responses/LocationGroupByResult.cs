﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Available Credit Result
    /// </summary>
    public class LocationGroupByResult
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public LocationGroupByResult() { }

        /// <summary>
        /// Location Name
        /// </summary>
        public string LocationName;

        /// <summary>
        /// Location ID
        /// </summary>
        public byte LocationID;

        /// <summary>
        /// Sum of Refundable Credit
        /// </summary>
        public decimal RefundableCredit;

        /// <summary>
        /// Sum of NonRefundableCredit
        /// </summary>
        public decimal NonRefundableCredit;

        /// <summary>
        /// Total Credit
        /// </summary>
        public decimal TotalCredit => RefundableCredit + NonRefundableCredit;
    }
}
