﻿namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// CanCreateUserResponse
    /// </summary>
    public class CanCreateUserResponse : BooleanResponse
    {
        /// <summary>
        /// Authorized User Count
        /// </summary>
        public int AuthorizedUserCount { get; set; }

        /// <summary>
        /// Current User Count
        /// </summary>
        public int CurrentUserCount { get; set; }
    }
}
