﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Responses
{
    /// <summary>
    /// 
    /// </summary>
    public class PaymentProcessorResponse
    {
        /// <summary>
        /// 
        /// </summary>
        public string status;
        /// <summary>
        /// 
        /// </summary>
        public bool? isSandboxed;
        /// <summary>
        /// 
        /// </summary>
        public string token;
        /// <summary>
        /// 
        /// </summary>
        public int vaultID;
        /// <summary>
        /// 
        /// </summary>
        public string referenceNumber;
        /// <summary>
        /// 
        /// </summary>
        public bool? AddressMatch;
        /// <summary>
        /// 
        /// </summary>
        public bool? CVVMatch;
        /// <summary>
        /// 
        /// </summary>
        public int? responseCode;
        /// <summary>
        /// 
        /// </summary>
        public string responseText;
        /// <summary>
        /// 
        /// </summary>
        public string responseProcessor;
        /// <summary>
        /// 
        /// </summary>
        public string batchID;
        /// <summary>
        /// 
        /// </summary>
        public bool? avsMatch;
        /// <summary>
        /// 
        /// </summary>
        public string avsMatchCode;
        /// <summary>
        /// 
        /// </summary>
        public bool? cvvMatch;
        /// <summary>
        /// 
        /// </summary>
        public string cvvMatchCode;
        /// <summary>
        /// 
        /// </summary>
        public string authCode;
    }
}
