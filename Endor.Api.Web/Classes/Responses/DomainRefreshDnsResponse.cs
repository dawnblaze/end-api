﻿using System;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Response of Domain RefreshDnsStatus call
    /// </summary>
    public class DomainRefreshDnsResponse
    {
        /// <summary>
        /// Domain
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// DNSLastAttemptDT
        /// </summary>
        public DateTime DNSLastAttemptDT { get; set; }

        /// <summary>
        /// DNSLastAttemptResult
        /// </summary>
        public string DNSLastAttemptResult { get; set; }

        /// <summary>
        /// DNSVerifiedDT
        /// </summary>
        public DateTime? DNSVerifiedDT { get; set; }
    }
}
