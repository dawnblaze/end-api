﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// hack class to remove need for null coalesce in Map function below
    /// </summary>
    public class SimpleOrderItemStatusTemp: SimpleOrderItemStatus
    {
        /// <summary>
        /// declare this as nullable
        /// to remove need for null-coalesce in Map function below
        /// </summary>
        public new OrderOrderStatus? OrderStatusID { get; set; }
    }

    /// <summary>
    /// query filters object for simple list endpoint of order item status controller
    /// </summary>
    public class SimpleOrderItemStatusFilters: IQueryFilters<OrderItemStatus>, IQueryFilterMapper<OrderItemStatus, SimpleOrderItemStatus>
    {
        /// <summary>
        /// optional filter for IsActive
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// optional filter for TransactionType
        /// </summary>
        public OrderTransactionType? TransactionType { get; set; }
        /// <summary>
        /// optional filter for OrderStatusID
        /// </summary>
        public OrderOrderStatus? OrderStatusID { get; set; }

        /// <summary>
        /// maps orderItemStatus to SimpleOrderItemStatus
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public IEnumerable<SimpleOrderItemStatus> Map(IQueryable<OrderItemStatus> source)
        {
            return source.Select((OrderItemStatus model) => new SimpleOrderItemStatusTemp()
            {
                BID = model.BID,
                ID = model.ID,
                ClassTypeID = model.ClassTypeID,
                IsActive = model.IsActive,
                DisplayName = model.Name,
                IsDefault = model.IsDefault,
                OrderStatusID = model.OrderStatusID,
                //OrderStatusID = model.OrderStatusID ?? 0,
                //OrderStatusID = (OrderOrderStatus)(byte)(model.OrderStatusID ?? 0l),
                //OrderStatusID = (OrderOrderStatus)Convert.ToByte(model.OrderStatusID),
                TransactionType = model.TransactionType
            });
        }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<OrderItemStatus, bool>>[] WherePredicates()
        {
            List<Expression<Func<OrderItemStatus, bool>>> predicates = new List<Expression<Func<OrderItemStatus, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(acc => acc.IsActive == IsActive.Value);
            }
            if (TransactionType.HasValue)
            {
                predicates.Add(acc => acc.TransactionType == (byte)TransactionType.Value);
            }
            if (OrderStatusID.HasValue)
            {
                predicates.Add(acc => acc.OrderStatusID == OrderStatusID.Value);
            }

            return predicates.ToArray();
        }
    }
}
