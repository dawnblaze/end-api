﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Filter class for FlatListItems
    /// </summary>
    public class FlatListItemFilter : IIsActiveQueryFilter<FlatListItem>
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// OrderItemStatus.OrderStatusID
        /// </summary>
        public FlatListType FlatListType { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public Expression<Func<FlatListItem, bool>>[] WherePredicates()
        {
            List<Expression<Func<FlatListItem, bool>>> predicates = new List<Expression<Func<FlatListItem, bool>>>();

            predicates.Add(t => !t.IsAdHoc);

            if (IsActive.HasValue)
            {
                predicates.Add(gla => gla.IsActive == IsActive.Value);
            }

            predicates.Add(i => i.FlatListType == FlatListType);

            if(Name != null)
            {
                predicates.Add(i => i.Name.Contains(Name.Trim()));
            }

            return predicates.ToArray();
        }
    }
}
