﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Enums
{
    public enum EnumSecurityRightFormat
    {
        [Description("JSON")]
        JSON,
        [Description("Base64")]
        Base64
    }
}
