﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Simple Destination Filters for HTTP GET Methods
    /// </summary>
    public class SimpleDestinationFilters : IQueryFilters<SimpleDestinationData>
    {
        /// <summary>
        /// Filters simplelist of Destination based on `IsActive` property
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// Query Filter for filters on Destination
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimpleDestinationData, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<SimpleDestinationData, bool>>>();

            if (IsActive.HasValue)
                predicates.Add(simpleItem => simpleItem.IsActive == IsActive.Value);

            return predicates.ToArray();
        }
    }
}
