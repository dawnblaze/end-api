﻿using Endor.Api.Web.Classes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    public class SecurityRightFilter
    {
        /// <summary>
        /// Choose from JSON = 0 and Base64 = 1
        /// </summary>
        public EnumSecurityRightFormat? Format { get; set; }
    }
}
