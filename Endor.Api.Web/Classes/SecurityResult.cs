﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web
{
    /// <summary>
    /// result for the security endpoint
    /// that is for Auth to API communication
    /// </summary>
    public class SecurityResult
    {
        /// <summary>
        /// user's access type
        /// </summary>
        public string accessType;
        /// <summary>
        /// a special Base64 string of rights
        /// </summary>
        public string rights;
        /// <summary>
        /// the employee ID if any for the user
        /// </summary>
        public short? employeeID;
        /// <summary>
        /// the contact ID if any for the user
        /// </summary>
        public int? contactID;
        /// <summary>
        /// the userlink ID if any for the user
        /// </summary>
        public short? userlinkID;
    }
}
