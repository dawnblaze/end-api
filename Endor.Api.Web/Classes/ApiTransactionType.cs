﻿
namespace Endor.Api.Web
{
    public enum ApiTransactionType
    {
        Estimate = 0,
        Order = 1,
        Invoice = 2
    }
}
