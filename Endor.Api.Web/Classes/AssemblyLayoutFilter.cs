﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// filter object for IsActive on GET routes
    /// </summary>
    public class AssemblyLayoutFilter : IQueryFilters<AssemblyLayout>
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public AssemblyLayoutType[] LayoutTypes { get; set; }

        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public Expression<Func<AssemblyLayout, bool>>[] WherePredicates()
        {
            List<Expression<Func<AssemblyLayout, bool>>> predicates = new List<Expression<Func<AssemblyLayout, bool>>>();

            if (LayoutTypes != null)
            {
                predicates.Add(l => LayoutTypes.Contains(l.LayoutType));
            }

            return predicates.ToArray();
        }
    }
}
