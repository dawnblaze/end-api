﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// A CrmOrigin filter for get endpoints
    /// strongly bound to <see cref="CrmOrigin"/>
    /// </summary>
    public class CrmOriginFilter : IIsActiveQueryFilter<CrmOrigin>
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// Used for filtering on CrmOrigin.Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public Expression<Func<CrmOrigin, bool>>[] WherePredicates()
        {
            List<Expression<Func<CrmOrigin, bool>>> predicates = new List<Expression<Func<CrmOrigin, bool>>>();

            if (IsActive.HasValue)
                predicates.Add(pt => pt.IsActive == IsActive.Value);
            if (!String.IsNullOrWhiteSpace(Name))
                predicates.Add(pt => pt.Name.ToLower().Contains(Name.ToLower()));

            return predicates.ToArray();
        }
    }
    /// <summary>
    /// A CrmOrigin filter for get endpoints
    /// strongly bound to <see cref="CrmOrigin"/>
    /// </summary>
    public class SimpleCrmOriginFilter : IIsActiveQueryFilter<SimpleOriginData>
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimpleOriginData, bool>>[] WherePredicates()
        {
            List<Expression<Func<SimpleOriginData, bool>>> predicates = new List<Expression<Func<SimpleOriginData, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(pt => pt.IsActive == IsActive.Value);
            }

            return predicates.ToArray();
        }
    }
}

