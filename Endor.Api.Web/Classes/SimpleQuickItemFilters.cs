using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters object for simple list endpoint of QuickItem controller
    /// </summary>
    public class SimpleQuickItemFilters : IQueryFilters<QuickItemData>
    {
        /// <summary>
        /// Filters simplelist of QuickItem based on `IsActive` property
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// Optional filter for CompanyID
        /// </summary>
        public int? CompanyID { get; set; }
        /// <summary>
        /// Optional filter for EmployeeID
        /// </summary>
        public int? EmployeeID { get; set; }
        /// <summary>
        /// Optional filter for Name
        /// </summary>
        /// <value></value>
        public string Name {get;set;}

        /// <summary>
        /// query filter for filters on this object
        /// </summary>
        /// <returns></returns>
        public Expression<Func<QuickItemData, bool>>[] WherePredicates()
        {
            List<Expression<Func<QuickItemData, bool>>> predicates = new List<Expression<Func<QuickItemData, bool>>>();

            if (IsActive.HasValue)
                predicates.Add(simpleItem => simpleItem.IsActive == IsActive.Value);

            if (CompanyID.HasValue)
                predicates.Add(simpleItem => simpleItem.CompanyID == CompanyID.Value);

            if (EmployeeID.HasValue)
                predicates.Add(simpleItem => simpleItem.EmployeeID == EmployeeID.Value);

            if (!string.IsNullOrWhiteSpace(this.Name))
                predicates.Add(simpleItem => simpleItem.Name.Trim().Equals(this.Name.Trim()));

            return predicates.ToArray();
        }
    }
}
