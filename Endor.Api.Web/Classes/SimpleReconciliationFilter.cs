﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Simple Reconciliation Filter class
    /// </summary>
    public class SimpleReconciliationFilter : IQueryFilters<Reconciliation>, IQueryFilterMapper<Reconciliation, SimpleReconciliation>
    {
        /// <summary>
        /// Simple Reconciliation LocationID attribute
        /// </summary>
        public byte? LocationID { get; set; }

        public IEnumerable<SimpleReconciliation> Map(IQueryable<Reconciliation> source) => source.Select((Reconciliation model) => new SimpleReconciliation()
        {
            BID = model.BID,
            ClassTypeID = model.ClassTypeID,
            DisplayName = model.Description,
            ID = model.ID,
            IsActive = true,
            IsDefault = false
        });

        public Expression<Func<Reconciliation, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<Reconciliation, bool>>>();
            if (LocationID.HasValue) predicates.Add(a => a.LocationID == LocationID.Value);
            return predicates.ToArray();
        }
    }
}
