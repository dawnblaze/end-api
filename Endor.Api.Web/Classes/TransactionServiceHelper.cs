﻿using Endor.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// TransactionServiceHelper
    /// </summary>
    public static class TransactionServiceHelper
    {
        /// <summary>
        /// Recursively sets the OrderID and OrderItemID of components and child components
        /// </summary>
        /// <param name="components"></param>
        /// <param name="orderID"></param>
        /// <param name="orderItemID"></param>
        public static void SetComponentsIDs(ICollection<OrderItemComponent> components, int orderID, int orderItemID)
        {
            if (components != null)
            {
                foreach (var component in components)
                {
                    component.OrderID = orderID;
                    component.OrderItemID = orderItemID;
                    SetComponentsIDs(component.ChildComponents, orderID, orderItemID);
                }
            }
        }



    }
}
