﻿using Endor.DocumentStorage.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web
{
    /// <summary>
    /// Interface for a document manager
    /// </summary>
    public interface IDocumentManager
    {
        /// <summary>
        /// Upload a Document asynchronously
        /// </summary>
        /// <param name="document"></param>
        /// <param name="form"></param>
        /// <param name="designation"></param>
        /// <param name="uploaderID"></param>
        /// <param name="uploaderCTID"></param>
        /// <returns></returns>
        Task<DMItem> UploadDocumentAsync(string document, IFormCollection form, string designation, int uploaderID,
            int uploaderCTID);
        
        /// <summary>
        /// Get List of document items asynchronously
        /// </summary>
        /// <returns></returns>
        Task<List<DMItem>> GetDocumentsAsync(bool flat = false);
        
        /// <summary>
        /// Renames a document
        /// </summary>
        /// <param name="document"></param>
        /// <param name="destination"></param>
        /// <param name="designation"></param>
        /// <returns></returns>
        Task<int> RenameFileAsync(string document, string destination, string designation);

        /// <summary>
        /// Renames a folder
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="destination"></param>
        /// <param name="designation"></param>
        /// <returns></returns>
        Task<int> RenameFolderAsync(string folder, string destination, string designation);

        /// <summary>
        /// Updates a documents metadata
        /// </summary>
        /// <param name="document"></param>
        /// <param name="designation"></param>
        /// <returns></returns>
        Task<DMItem> UpdateDocumentMetadataAsync(string document, string designation);

        /// <summary>
        /// Replace the contents of a document
        /// </summary>
        /// <param name="document"></param>
        /// <param name="form"></param>
        /// <param name="designation"></param>
        /// <param name="userID"></param>
        /// <param name="userCTID"></param>
        /// <returns></returns>
        Task<DMItem> ReplaceDocumentAsync(string document, IFormCollection form, string designation, int userID,
            int userCTID);

        /// <summary>
        /// Move a file to a new destination
        /// </summary>
        /// <param name="document"></param>
        /// <param name="destination"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="designation"></param>
        /// <returns></returns>
        Task<int> MoveFileAsync(string document, string destination, int? destCTID, int? destID, string designation);
        
        /// <summary>
        /// Move files to a new destination
        /// </summary>
        /// <param name="documents"></param>
        /// <param name="destPath"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="designation"></param>
        /// <returns></returns>
        Task<int> MoveFilesAsync(string[] documents, string destPath, int? destCTID, int? destID, string designation);

        /// <summary>
        /// Move a folder to a new destination
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="destination"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="designation"></param>
        /// <returns></returns>
        Task<int> MoveFolderAsync(string folder, string destination, int? destCTID, int? destID, string designation);

        /// <summary>
        /// Move multiple folders to a new destination
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="destPath"></param>
        /// <param name="destCTID"></param>
        /// <param name="destID"></param>
        /// <param name="designation"></param>
        /// <returns></returns>
        Task<int> MoveFoldersAsync(string[] folder, string destPath, int? destCTID, int? destID, string designation);

        /****  D  *****/
        /// <summary>
        /// Delete a single document
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        Task<int> DeleteDocumentAsync(string document);

        /// <summary>
        /// Delete multiple documents
        /// </summary>
        /// <param name="documents">Documents to Delete</param>
        /// <returns></returns>
        Task<int> DeleteDocumentsAsync(string[] documents);

        /// <summary>
        /// Deletes all documents in all buckets
        /// </summary>
        /// <returns></returns>
        Task<int> DeleteAllDocumentsAsync();

        /****  Actions  *****/
        /// <summary>
        /// Copies documents into a target id/ctid
        /// </summary>
        /// <param name="documents"></param>
        /// <param name="destination"></param>
        /// <param name="destinationBin"></param>
        /// <param name="destinationID"></param>
        /// <param name="destinationCTID"></param>
        /// <param name="destinationTempID"></param>
        /// <returns></returns>
        Task<int> CopyDocumentsAsync(string[] documents, string destination, StorageBin? destinationBin, int? destinationID, int? destinationCTID, Guid? destinationTempID = null);

        /// <summary>
        /// Duplicate Multiple Documents
        /// </summary>
        /// <param name="documents"></param>
        /// <returns></returns>
        Task<int> DuplicateDocumentsAsync(string[] documents);
        //Task<Stream> ZipDocumentsAsync(string[] documents);

        /// <summary>
        /// Compress the list of documents
        /// </summary>
        /// <param name="documents"></param>
        /// <param name="userID"></param>
        /// <param name="userCTID"></param>
        /// <param name="flatExport"></param>
        /// /// <returns></returns>
        Task<string> ZipDocumentsAsync(string[] documents, int userID, int userCTID, bool flatExport);

        /// <summary>
        /// Compress all the documents
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="userCTID"></param>
        /// <returns></returns>
        Task<string> ZipAllDocumentsAsync(int userID, int userCTID);

        /// <summary>
        /// Initialiaze Storage Client
        /// </summary>
        /// <returns></returns>
        Task<int> InitializeAllAsync(); //handles both entity and temp => entity via context

        /// <summary>
        /// Keeps any objects associated with the model
        /// </summary>
        /// <param name="temp"></param>
        /// <param name="destinationID"></param>
        /// <param name="destinationCTID"></param>
        /// <returns></returns>
        Task<int> PersistDocumentsAsync(Guid temp, int destinationID, int destinationCTID);
    }
}
