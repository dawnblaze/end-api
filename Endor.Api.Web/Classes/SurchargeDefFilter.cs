﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// filters for surcharge GET all
    /// </summary>
    public class SurchargeDefFilter : IQueryFilters<SurchargeDef>
    {
        /// <summary>
        /// Query parameters to filter the SurchargeDef
        /// </summary>
        public string Search { get; set; } = "";

        /// <summary>
        /// When true returns active records
        /// When false, returns inactive records
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// When true returns records that are AppliedByDefault
        /// When false, returns only records that are not applied by default
        /// </summary>
        public bool? AppliedByDefault { get; set; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SurchargeDef, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<SurchargeDef, bool>>>();

            if (!String.IsNullOrEmpty(this.Search))
            {
                var lowerCaseSearchString = this.Search.ToLower();
                predicates.Add(df =>
                    df.Name.ToLower().Contains(lowerCaseSearchString) ||
                    df.TaxCode.TaxCode.Contains(lowerCaseSearchString) ||
                    df.TaxCode.Name.ToLower().Contains(lowerCaseSearchString) ||
                    df.IncomeAccount.NumberedName.ToLower().Contains(lowerCaseSearchString)
                );
            }

            if (this.IsActive.HasValue)
                predicates.Add(df => df.IsActive == IsActive.Value);

            if (this.AppliedByDefault.HasValue)
                predicates.Add(df1 => df1.AppliedByDefault == AppliedByDefault.Value);

            return predicates.ToArray();
        }

        /// <summary>
        /// Return true if any filters are present
        /// </summary>
        internal bool HasFilters => WherePredicates().Length > 0;
    }
}