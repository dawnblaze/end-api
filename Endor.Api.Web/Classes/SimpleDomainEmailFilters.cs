﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters object for simple list endpoint of domain email controller
    /// </summary>
    public class SimpleDomainEmailFilters : IQueryFilters<SimpleDomainEmail>
    {
        /// <summary>
        /// Filters simplelist of DomainEmail items base on `IsActive` property
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// query filter for filters on this object
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimpleDomainEmail, bool>>[] WherePredicates()
        {
            List<Expression<Func<SimpleDomainEmail, bool>>> predicates = new List<Expression<Func<SimpleDomainEmail, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(simpleItem => simpleItem.IsActive == IsActive.Value);
            }

            return predicates.ToArray();
        }
    }
}
