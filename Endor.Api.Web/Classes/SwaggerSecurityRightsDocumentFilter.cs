﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Enables Bearer Operation Filter for Authorization in Swagger UI
    /// </summary>
    public class SwaggerSecurityRightsDocumentFilter : IOperationFilter
    { 
        /// <summary>
        /// sets up security to use Bearer auth
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="context"></param>
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            //TODO
            operation.Security = new List<OpenApiSecurityRequirement>()
            {
                new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,

                        },
                        new List<string>()
                    }
                }
            };




//            operation.Security = new List<IDictionary<string, IEnumerable<string>>>
//            {
//                new Dictionary<string, IEnumerable<string>>
//                {
//                    { "Bearer", new string[] {} }
//                }
//            };
        }
    }
}
