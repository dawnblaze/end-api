﻿using Endor.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Endor.Api.Web.Classes
{
    public class ReconciliationPaymentInfo
    {
        /// <summary>
        /// The ID of the Location Object the payment or refund was for.
        /// </summary>
        public short LocationID { get; set; }

        /// <summary>
        ///The Currency Type for the payment
        /// </summary>
        public AccountingCurrencyType CurrencyType { get; set; }

        /// <summary>
        /// The name of the Payment Method Type.
        /// This is the grouping category used to classify the payment in the reconciliation report.
        /// E.g.: Cash, Credit Cards, etc.
        /// </summary>
        public string PaymentMethodType { get; set; }

        /// <summary>
        /// The total of all payments for the Payment Method Type.
        /// This is the group total for the Payment Method Type.
        /// </summary>
        public decimal PaymentMethodTypeAmount { get; set; }

        /// <summary>
        /// The name of the individual PaymentMethod (PaymentType) Object.
        /// E.g.  Cash, Visa, Amex, etc.
        /// </summary>
        public string PaymentMethod { get; set; }

        /// <summary>
        /// The total payments for that Payment Method and Payment Transaction Type.
        /// </summary>
        public decimal PaymentMethodAmount { get; set; }

        /// <summary>
        /// The Type of Payment Transaction, usually "Payment" or "Refund".
        /// </summary>
        public string PaymentTransactionType { get; set; }

        /// <summary>
        /// The name of the GLAccount Object the payment was recorded into.
        /// </summary>
        public string GLAccountName { get; set; }

        /// <summary>
        /// The ID of the Location Object that received at, which may be different than the Location it was applied to.
        /// </summary>
        public int ReceivedLocationID { get; set; }

        /// <summary>
        /// The ID of the PaymentMethod (PaymentType) Object.
        /// </summary>
        public int PaymentMethodID { get; set; }

        /// <summary>
        /// The ID of the Payment Transaction Type. ( 1= Payment, 2 = Refund )
        /// </summary>
        public byte PaymentTransactionTypeID { get; set; }

        /// <summary>
        /// The number of Payments included in this row.
        /// </summary>
        public int PaymentCount { get; set; }

        /// <summary>
        /// The number of distinct Orders paid through payments in this row.
        /// </summary>
        public int OrderCount { get; set; }

        /// <summary>
        /// An array of IDs for all of the Payments in this row.
        /// This is the value returned from the core TVF in the DB. It will be used to build out the PaymentIDs list.
        /// This property should have the JsonIgnore attribute on it.
        /// </summary>
        [JsonIgnore]
        public string PaymentIDsString { get; set; }

        /// <summary>
        /// An array of IDs for all of the Payments in this row.
        /// This is used for the detailed search screen for this payment type.
        /// </summary>
        public List<int> PaymentIDs {
            get
            {
                if (string.IsNullOrWhiteSpace(PaymentIDsString))
                    return null;

                List<int> paymentIDs = new List<int>();
                string[] ids = PaymentIDsString.Split(',');
                foreach (var id in ids)
                {
                    if(int.TryParse(id, out int parsedID))
                        paymentIDs.Add(parsedID);
                }

                if (paymentIDs.Count > 0)
                    return paymentIDs;
                else
                    return null;
            }
        }

        /// <summary>
        /// The sort index.  The 10s digit is the column; the 1s digit is the sort index.
        /// </summary>
        public int SortIndex { get; set; }
    }
}
