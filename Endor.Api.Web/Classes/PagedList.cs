using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// paged list
    /// taken from end-web's paged-generic.ts
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PagedList<T>
    {
        /// <summary>
        /// total count of all results
        /// </summary>
        public int TotalCount {get;set;}
        /// <summary>
        /// collection of this page's results
        /// </summary>
        public ICollection<T> Results {get;set;}
    }
}
