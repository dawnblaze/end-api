﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Query Filters for TaxabilityCode
    /// </summary>
    public class TaxabilityCodeFilters : IQueryFilters<TaxabilityCode>
    {
        /// <summary>
        /// TaxabilityCode.IsActive
        /// </summary>>
        public bool? IsActive { get; set; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<TaxabilityCode, bool>> WherePredicate()
        {
            return x => (x.IsActive == this.IsActive || !this.IsActive.HasValue);
        }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<TaxabilityCode, bool>>[] WherePredicates()
        {
            return new Expression<Func<TaxabilityCode, bool>>[0];
        }

    }
}
