﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters object for simple list endpoint of material category controller
    /// </summary>
    public class SimpleMaterialCategoryFilters : IQueryFilters<SimpleMaterialCategory>
    {
        /// <summary>
        /// Filters simplelist of MaterialCategory items base on `IsActive` property
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// query filter for filters on this object
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimpleMaterialCategory, bool>>[] WherePredicates()
        {
            List<Expression<Func<SimpleMaterialCategory, bool>>> predicates = new List<Expression<Func<SimpleMaterialCategory, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(simpleItem => simpleItem.IsActive == IsActive.Value);
            }

            return predicates.ToArray();
        }
    }
}
