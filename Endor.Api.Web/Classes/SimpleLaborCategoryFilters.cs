using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters object for simple list endpoint of labor category controller
    /// </summary>
    public class SimpleLaborCategoryFilters : IQueryFilters<SimpleLaborCategory>
    {
        /// <summary>
        /// Filters simplelist of LaborCategory items base on `IsActive` property
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// query filter for filters on this object
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimpleLaborCategory, bool>>[] WherePredicates()
        {
            List<Expression<Func<SimpleLaborCategory, bool>>> predicates = new List<Expression<Func<SimpleLaborCategory, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(simpleItem => simpleItem.IsActive == IsActive.Value);
            }

            return predicates.ToArray();
        }
    }
}
