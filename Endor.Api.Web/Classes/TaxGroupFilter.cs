﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters object for simple list endpoint of TaxGroup controller
    /// </summary>
    public class TaxGroupFilter : IQueryFilters<TaxGroup>
    {
        /// <summary>
        /// Filters simplelist of TaxGroup based on `IsActive` property
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// Optional filter for Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Optional filter for Searching TaxGroup
        /// </summary>
        public string Search { get; set; }

        /// <summary>
        /// query filter for filters on this object
        /// </summary>
        /// <returns></returns>
        public Expression<Func<TaxGroup, bool>>[] WherePredicates()
        {
            List<Expression<Func<TaxGroup, bool>>> predicates = new List<Expression<Func<TaxGroup, bool>>>();

            if (IsActive.HasValue)
                predicates.Add(taxGroup => taxGroup.IsActive == IsActive.Value);

            if (!string.IsNullOrWhiteSpace(this.Name))
                predicates.Add(taxGroup => taxGroup.Name.ToLower().Contains(this.Name.Trim().ToLower()));

            if (!string.IsNullOrWhiteSpace(this.Search))
                predicates.Add(taxGroup => taxGroup.Name.ToLower().Contains(this.Search.Trim().ToLower())
                || taxGroup.TaxRate.ToString().ToLower().Contains(this.Search.Trim().ToLower()));

            return predicates.ToArray();
        }
    }
}
