﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    public class CrmIndustryFilter : IQueryFilters<CrmIndustry>
    {
        /// <summary>
        /// If only Active records should be returned.
        /// When not provided, returns both Active and InActive.
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// If only IsTopLevel records should be returned.
        /// When not provided, returns both IsTopLevel and not.
        /// </summary>
        public bool? IsTopLevel { get; set; }
        /// <summary>
        /// If supplied, filters to a matching name.
        /// </summary>
        public string Name { get; set; }
        public Expression<Func<CrmIndustry, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<CrmIndustry, bool>>>();

            if (this.IsActive.HasValue)
                predicates.Add(ci => ci.IsActive == this.IsActive);

            if (this.IsTopLevel.HasValue)
                predicates.Add(ci => ci.IsTopLevel == this.IsTopLevel);

            if (!string.IsNullOrWhiteSpace(this.Name))
                predicates.Add(ci => ci.Name.ToLower() == this.Name.ToLower());

            return predicates.ToArray();
        }

        /// <summary>
        /// Return true if any filters are present
        /// </summary>
        internal bool HasFilters => WherePredicates().Length > 0;
    }
}
