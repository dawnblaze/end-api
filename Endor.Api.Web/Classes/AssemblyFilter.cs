﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// filter object for IsActive on GET routes
    /// </summary>
    public class AssemblyFilter : IIsActiveQueryFilter<AssemblyData>
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// optional filter for AssemblyType
        /// </summary>
        public AssemblyType? AssemblyType { get; set; }

        /// <summary>
        /// When non-null, filters layouts
        /// </summary>
        public AssemblyLayoutType[] LayoutType { get; set; }

        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public Expression<Func<AssemblyData, bool>>[] WherePredicates()
        {
            List<Expression<Func<AssemblyData, bool>>> predicates = new List<Expression<Func<AssemblyData, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(gla => gla.IsActive == IsActive.Value);
            }

            if (AssemblyType != null)
            {
                predicates.Add(a => a.AssemblyType == AssemblyType);
            }

            return predicates.ToArray();
        }
    }
}
