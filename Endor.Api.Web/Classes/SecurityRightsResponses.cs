﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    public class SecurityRightsResponses
    {
        public class JSONRights
        {
            public short BID { get; set; }
            public int UserLinkID { get; set; }
            public int[] Rights { get; set; }
        }
    }
}
