using System.Linq;
using Endor.Models;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/846004309/Message+API+Endpoints
    /// </summary>
    public class MessageIncludes
    {
        /// <summary>
        /// default false
        /// </summary>
        public bool IncludeLinks { get; set; } = false;
        /// <summary>
        /// default false
        /// </summary>
        public bool IncludeDeliveryHistory { get; set; } = false;

        /// <summary>
        /// AssignIncludedProps
        /// </summary>
        /// <param name="query"></param>
        public IQueryable<MessageHeader> AssignIncludedProps(IQueryable<MessageHeader> query){
            if(this.IncludeLinks){
                query = query.Include(e => e.MessageBody).ThenInclude(b => b.MessageObjectLinks);
            }

            if(this.IncludeDeliveryHistory){
                query = query.Include(e => e.MessageBody)
                    .ThenInclude(b => b.MessageParticipantInfos)
                    .ThenInclude(p => p.MessageDeliveryRecords);
            }
            return query;
        }
    }
}