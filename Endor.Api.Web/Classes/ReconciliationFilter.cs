﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Reconciliation Filter class
    /// </summary>
    public class ReconciliationFilter : IQueryFilters<Reconciliation>
    {
        /// <summary>
        /// Reconciliation LocationID attribute
        /// </summary>
        public byte? LocationID { get; set; }

        /// <summary>
        /// Include items on list
        /// </summary>
        public bool? IncludeItems { get; set; }

        public Expression<Func<Reconciliation, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<Reconciliation, bool>>>();
            if (LocationID.HasValue) predicates.Add(a => a.LocationID == LocationID.Value);
            return predicates.ToArray();
        }
    }
}
