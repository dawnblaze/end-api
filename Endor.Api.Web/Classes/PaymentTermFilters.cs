﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// A payment term filter for get endpoints
    /// strongly bound to <see cref="PaymentTerm"/>
    /// </summary>
    public class PaymentTermFilter : IIsActiveQueryFilter<PaymentTerm>
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// Used for filtering on PaymentTerm.Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public Expression<Func<PaymentTerm, bool>>[] WherePredicates()
        {
            List<Expression<Func<PaymentTerm, bool>>> predicates = new List<Expression<Func<PaymentTerm, bool>>>();

            if (IsActive.HasValue)
                predicates.Add(pt => pt.IsActive == IsActive.Value);
            if (!String.IsNullOrWhiteSpace(Name))
                predicates.Add(pt => pt.Name.ToLower().Contains(Name.ToLower()));

            return predicates.ToArray();
        }
    }
    /// <summary>
    /// A payment term filter for get endpoints
    /// strongly bound to <see cref="PaymentTerm"/>
    /// </summary>
    public class SimplePaymentTermFilter : IIsActiveQueryFilter<SimplePaymentTerm>
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimplePaymentTerm, bool>>[] WherePredicates()
        {
            List<Expression<Func<SimplePaymentTerm, bool>>> predicates = new List<Expression<Func<SimplePaymentTerm, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(pt => pt.IsActive == IsActive.Value);
            }

            return predicates.ToArray();
        }
    }
}
