using System;
using System.Linq.Expressions;
using Endor.Models;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Query filters for CustomFieldLayoutDefinition entity
    /// </summary>
    public class CustomFieldLayoutDefinitionFilter : IQueryFilters<CustomFieldLayoutDefinition>
    {
        /// <summary>
        /// IsActive filter
        /// </summary>
        public bool? IsActive {get;set;} = null;
        /// <summary>
        /// AppliesToClassTypeID filter
        /// </summary>
        public int? AppliesToClassTypeID {get;set;} = null;
        /// <summary>
        /// Query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<CustomFieldLayoutDefinition, bool>>[] WherePredicates()
        {
            Expression<Func<CustomFieldLayoutDefinition, bool>> predicate;
            predicate = x => x.IsActive == (this.IsActive ?? x.IsActive) 
                        && x.AppliesToClassTypeID == (this.AppliesToClassTypeID ?? x.AppliesToClassTypeID);
            
            return new Expression<Func<CustomFieldLayoutDefinition, bool>>[]{ predicate };
        }
    }
}