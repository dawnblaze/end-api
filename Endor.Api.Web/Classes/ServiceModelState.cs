﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// 
    /// </summary>
    public class ServiceModelState : IValidationDictionary
    {
        private ModelStateDictionary _modelState;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelState"></param>
        public ServiceModelState(ModelStateDictionary modelState)
        {
            _modelState = modelState;
        }

        /// <summary>
        /// 
        /// </summary>
        public ServiceModelState():this(new ModelStateDictionary())
        {
        }

        #region IValidationDictionary Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="errorMessage"></param>
        public void AddError(string key, string errorMessage)
        {
            _modelState.AddModelError(key, errorMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get { return _modelState.IsValid; }
        }

        #endregion
    }
}
