using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// filters for simple list endpoints on contact endpoint
    /// </summary>
    public class SimpleContactFilters : IQueryFilters<SimpleContactData>
    {
        /// <summary>
        /// optional parent ID filter
        /// </summary>
        public int? ParentID { get; set; }

        /// <summary>
        /// query filter for filters on this object
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimpleContactData, bool>>[] WherePredicates()
        {
            List<Expression<Func<SimpleContactData, bool>>> predicates = new List<Expression<Func<SimpleContactData, bool>>>();

            if (this.ParentID.HasValue)
                predicates.Add(contact => contact.ParentID == this.ParentID);

            return predicates.ToArray();
        }
    }
}
