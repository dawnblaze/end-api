﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Filters for Custom Field Definitions
    /// </summary>
    public class CustomFieldDefinitionFilter : IQueryFilters<CustomFieldDefinition>
    {
        /// <summary>
        /// Filter for only active custom field definitions
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// Disregard .IsActive state
        /// </summary>
        public bool? DisregardActiveState { get; set; }

        /// <summary>
        /// Filter for getting custom field definitions that apply to a type of Class
        /// </summary>
        public int? AppliesToClassTypeID { get; set; }

        /// <summary>
        /// Indicates the custom field definition is associated with a single item
        /// </summary>
        public int? AppliesToID { get; set; }

        /// <summary>
        /// Returns an array of Expressions to pass as where clause parameters
        /// </summary>
        /// <returns></returns>
        public Expression<Func<CustomFieldDefinition, bool>>[] WherePredicates()
        {
            List<Expression<Func<CustomFieldDefinition, bool>>> predicates = new List<Expression<Func<CustomFieldDefinition, bool>>>();

            if (this.DisregardActiveState.GetValueOrDefault(false))
            {
                //noop
            }
            else if (IsActive.HasValue)
            {
                predicates.Add(t => t.IsActive == IsActive.Value);
            }

            if (this.AppliesToClassTypeID.HasValue)
                predicates.Add(t => t.AppliesToClassTypeID == this.AppliesToClassTypeID.Value);
            if (this.AppliesToID.HasValue)
                predicates.Add(t => t.AppliesToID == this.AppliesToID.Value);

            return predicates.ToArray();
        }

        /// <summary>
        /// Return true if any filters are present
        /// </summary>
        internal bool HasFilters => WherePredicates().Length > 0;
    }
}
