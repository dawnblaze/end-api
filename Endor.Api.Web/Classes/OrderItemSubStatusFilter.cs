﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters for order item entity
    /// </summary>
    public class OrderItemSubStatusFilter : IQueryFilters<OrderItemSubStatus>
    {
        /// <summary>
        /// OrderItemSubStatus.OrderStatusID
        /// </summary>
        public byte? OrderItemStatusID { get; set; }

        /// <summary>
        /// OrderItemSubStatus.Name
        /// </summary>
        public string Name {get;set;}

        /// <summary>
        /// predicate builder for this object
        /// </summary>
        /// <returns></returns>
        public Expression<Func<OrderItemSubStatus, bool>> WherePredicate()
        {
            return oiss => (!this.OrderItemStatusID.HasValue ||  ((oiss.OrderItemStatusSubStatusLinks!=null) &&(oiss.OrderItemStatusSubStatusLinks.Where(ois => ois.StatusID == OrderItemStatusID).Any())))
                        && (oiss.Name.Contains((this.Name ?? "")));
        }

        /// <summary>
        /// empty predicate builder for this object
        /// </summary>
        /// <returns></returns>
        public Expression<Func<OrderItemSubStatus, bool>>[] WherePredicates()
        {
            List<Expression<Func<OrderItemSubStatus, bool>>> predicates = new List<Expression<Func<OrderItemSubStatus, bool>>>();
            return predicates.ToArray();
        }

    }
}
