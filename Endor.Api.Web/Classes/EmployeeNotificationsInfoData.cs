﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// notifications data
    /// </summary>
    public class EmployeeNotificationsInfoData
    {
        /// <summary>
        /// the count of unread messages
        /// </summary>
        public int UnreadMessageCount { get; set; }
        /// <summary>
        /// the count of unread alerts
        /// </summary>
        public int UnreadAlertCount { get; set; }
        /// <summary>
        /// the count of unread SystemNoticeCount
        /// </summary>
        public int UnreadSystemNoticeCount { get; set; }

        /// <summary>
        /// the ID of the TimeCard
        /// </summary>
        public int TimeCardID { get; set; }

        /// <summary>
        /// current time clock status
        /// </summary>
        public bool IsLoggedIn { get; set; }

        /// <summary>
        /// The ID of the TimeCard detail
        /// </summary>
        public int TimeCardDetailID { get; set; }

        /// <summary>
        /// the ID of the OrderItemStatus
        /// </summary>
        public int OrderItemStatusID { get; set; }

        /// <summary>
        /// the ID of the TimeClockActivity
        /// </summary>
        public int TimeClockActivityID { get; set; }

        /// <summary>
        /// the ID of the TimeClockBreak
        /// </summary>
        public int TimeClockBreakID { get; set; }

        /// <summary>
        /// The status name of TimeCard
        /// </summary>
        public string StatusName { get; set; }
    }
}
