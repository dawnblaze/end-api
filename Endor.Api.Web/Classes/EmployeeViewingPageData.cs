﻿namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// ID/Initials for use when listing employees on a page
    /// </summary>
    public class EmployeeViewingPageData
    {
        /// <summary>
        /// the ID of the employee
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// the initials of the employee
        /// </summary>
        public string Initials { get; set; }
    }
}