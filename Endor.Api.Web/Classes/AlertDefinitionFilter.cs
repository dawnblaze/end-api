﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters for AlertDefinition entity
    /// </summary>>
    public class AlertDefinitionFilter : IQueryFilters<AlertDefinition>
    {
        /// <summary>
        /// If only Active records should be returned.
        /// When not provided, returns both Active and InActive.
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// If supplied, filters for Alerts with the specified datatype.
        /// </summary>
        public short? DataType { get; set; }

        /// <summary>
        /// If supplied, filters for Alerts with the specified employee.
        /// </summary>
        public int? EmployeeID { get; set; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<AlertDefinition, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<AlertDefinition, bool>>>();

            if (this.IsActive.HasValue)
                predicates.Add(ad => ad.IsActive == this.IsActive);

            if (this.DataType.HasValue)
                predicates.Add(ad => ad.DataType == (DataType)this.DataType);

            if (this.EmployeeID.HasValue)
                predicates.Add(ad => ad.EmployeeID == this.EmployeeID);

            return predicates.ToArray();
        }

        /// <summary>
        /// Return true if any filters are present
        /// </summary>
        internal bool HasFilters => WherePredicates().Length > 0;
    }
}
