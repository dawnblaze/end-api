﻿using Endor.Models;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Assembly Variable Validation Result
    /// </summary>
    public class VariableValidationResult
    {
        /// <summary>
        /// If it valid
        /// </summary>
        public bool Valid { get; set; }
        /// <summary>
        /// Assembly Data Response
        /// </summary>
        public GenericResponse<AssemblyData> Response { get; set; }
    }
}
