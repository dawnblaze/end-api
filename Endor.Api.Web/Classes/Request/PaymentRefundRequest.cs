﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Request
{
    /// <summary>
    /// Payment Request
    /// </summary>
    public class PaymentRefundRequest
    {

        /// <summary>
        /// Payment Transaction Type ID
        /// </summary>
        public PaymentTransactionType PaymentTransactionType;

        /// <summary>
        /// Payment Master ID - if not defined the relevant master payments will be calculated
        /// </summary>
        public int? MasterPaymentID;

        /// <summary>
        /// Company ID - required
        /// </summary>
        public int CompanyID;

        /// <summary>
        /// Total Amount to refund
        /// </summary>
        public decimal Amount;

        /// <summary>
        /// Currency Type ID (optional)
        /// </summary>
        public byte? CurrencyType;

        /// <summary>
        /// Applications
        /// </summary>
        public List<PaymentApplicationRequest> Applications;

        /// <summary>
        /// Display Number
        /// </summary>
        public string DisplayNumber;

        /// <summary>
        /// Processor Info
        /// </summary>
        public PaymentProcessorInfoRequest ProcessorInfo;
    }
}
