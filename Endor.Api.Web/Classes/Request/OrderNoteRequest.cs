﻿using Endor.Models;

namespace Endor.Api.Web
{
    /// <summary>
    /// Order Note Request
    /// </summary>
    public class OrderNoteRequest
    {
        /// <summary>
        /// Note
        /// </summary>
        public string Note { get; set; }
    }
}
