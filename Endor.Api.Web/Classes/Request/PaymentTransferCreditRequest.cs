﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Request
{
    /// <summary>
    /// Payment TransferCreditRequest
    /// </summary>
    public class PaymentTransferCreditRequest
    {
        /// <summary>
        /// Payment Transaction Type ID
        /// </summary>
        public PaymentTransactionType PaymentTransactionType;

        /// <summary>
        /// Payment Method ID - This should be 251
        /// </summary>
        public int PaymentMethodId;

        /// <summary>
        /// Payment Master ID - if not defined the relevent master payment(s) will be calculated
        /// </summary>
        public int? MasterPaymentID;

        /// <summary>
        /// Company ID - required
        /// </summary>
        public int CompanyID;

        /// <summary>
        /// Notes
        /// </summary>
        public string Notes { get; set; }


        /// <summary>
        /// Applications
        /// </summary>
        public List<PaymentApplicationRequest> Applications;
    }
}
