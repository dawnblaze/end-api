﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class PaymentApplicationRequest
    {
        /// <summary>
        /// Order ID
        /// </summary>
        public int? OrderID;

        /// <summary>
        /// Amount applied
        /// </summary>
        public decimal Amount;
    }
}
