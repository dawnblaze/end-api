﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Request
{
    /// <summary>
    /// Payment Request
    /// </summary>
    public class PaymentRequest
    {
        /// <summary>
        /// Payment Transaction Type ID
        /// </summary>
        public int PaymentTransactionType;

        /// <summary>
        /// Location ID
        /// </summary>
        public byte LocationID;

        /// <summary>
        /// Received Location ID (optional)
        /// </summary>
        public byte? ReceivedLocationID;

        /// <summary>
        /// Company ID
        /// </summary>
        public int CompanyID;

        /// <summary>
        /// Contact ID
        /// </summary>
        public int ContactID;

        /// <summary>
        /// Payment Method ID
        /// </summary>
        public int PaymentMethodID;

        /// <summary>
        /// Payment Type ID
        /// </summary>
        public int PaymentType;

        /// <summary>
        /// Total Amount
        /// </summary>
        public decimal Amount;

        /// <summary>
        /// Currency Type ID (optional)
        /// </summary>
        public byte? CurrencyType;

        /// <summary>
        /// Display Number
        /// </summary>
        public string DisplayNumber;

        /// <summary>
        /// Notes
        /// </summary>
        public string Notes;

        /// <summary>
        /// Applications
        /// </summary>
        public List<PaymentApplicationRequest> Applications;

        /// <summary>
        /// Processor Info
        /// </summary>
        public PaymentProcessorInfoRequest ProcessorInfo;
    }
}
