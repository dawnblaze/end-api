﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Request
{
    /// <summary>
    /// request for board move
    /// </summary>
    public class BoardMoveRequest
    {
        /// <summary>
        /// property name board column
        /// </summary>
        public string PropertyName { get; set; }
        /// <summary>
        /// old value 
        /// </summary>
        public string From { get; set; }
        /// <summary>
        /// new value 
        /// </summary>
        public string To { get; set; }
    }
}
