﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Request
{
    /// <summary>
    /// Payment Request
    /// </summary>
    public class PaymentCreditAdjustmentRequest
    {

        /// <summary>
        /// Payment Transaction Type ID
        /// </summary>
        public PaymentTransactionType PaymentTransactionType;

        /// <summary>
        /// Payment Method ID - This should be 251
        /// </summary>
        public int PaymentMethodId;

        /// <summary>
        /// Payment Master ID - if not defined the master payment(s) will be calculated correctly
        /// </summary>
        public int? MasterPaymentID;

        /// <summary>
        /// Company ID - required
        /// </summary>
        public int CompanyID;

        /// <summary>
        /// Total Amount to refund
        /// </summary>
        public decimal Amount;

        /// <summary>
        /// Currency Type ID (optional)
        /// </summary>
        public byte? CurrencyType;

        /// <summary>
        /// Request notes (optional)
        /// </summary>
        public string Notes;

        /// <summary>
        /// Applications
        /// </summary>
        public List<PaymentApplicationRequest> Applications;
    }
}
