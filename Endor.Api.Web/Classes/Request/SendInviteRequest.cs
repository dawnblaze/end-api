﻿using Endor.Models;

namespace Endor.Api.Web
{
    /// <summary>
    /// Elements of the SendInvite Request Body
    /// </summary>
    public class SendInviteRequest
    {
        /// <summary>
        /// Email Address
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Phone Number
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// User Access Type
        /// </summary>
        public UserAccessType UserAccessType { get; set; }
        /// <summary>
        /// Rights Group List ID
        /// </summary>
        public short? RightsGroupListID { get; set; }
    }
}
