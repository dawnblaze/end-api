﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Request
{
    /// <summary>
    /// Credit Gift Request
    /// </summary>
    public class CreditGiftRequest
    {
        /// <summary>
        /// Payment Transaction Type ID
        /// </summary>
        public PaymentTransactionType PaymentTransactionType;

        /// <summary>
        /// Payment Method ID - This should be 251
        /// </summary>
        public int PaymentMethodId;

        /// <summary>
        /// Location ID
        /// </summary>
        public byte LocationID;

        /// <summary>
        /// Received Location ID (optional)
        /// </summary>
        public byte? ReceivedLocationID;

        /// <summary>
        /// Company ID
        /// </summary>
        public int CompanyID;

        /// <summary>
        /// Total Amount
        /// </summary>
        public decimal Amount;

        /// <summary>
        /// Currency Type ID (optional)
        /// </summary>
        public byte? CurrencyType;

        /// <summary>
        /// Notes
        /// </summary>
        public string Notes { get; set; }


        /// <summary>
        /// Applications
        /// </summary>
        public List<PaymentApplicationRequest> Applications;

    }
}
