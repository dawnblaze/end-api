﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes.Request
{
    /// <summary>
    /// Payment Processor Request
    /// </summary>
    public class PaymentProcessorInfoRequest
    {
        /// <summary>
        /// Payment Token
        /// </summary>
        public string token;
        /// <summary>
        /// Payment Vault ID
        /// </summary>
        public int? vaultID;
        /// <summary>
        /// Payment Authorization Number
        /// </summary>
        public string authreferenceNumber;
        /// <summary>
        /// Payment Expiration Date
        /// </summary>
        public string expirationDate;
        /// <summary>
        /// Payment cvv code
        /// </summary>
        public int? cvv;
        /// <summary>
        /// Payment address
        /// </summary>
        public PaymentAddress address;
    }

    /// <summary>
    /// Payment Address
    /// </summary>
    public class PaymentAddress
    {
        /// <summary>
        /// Payment Address State
        /// </summary>
        public string street;
        /// <summary>
        /// Payment Address City
        /// </summary>
        public string city;
        /// <summary>
        /// Payment Address Region
        /// </summary>
        public string region;
        /// <summary>
        /// Payment Postal Code
        /// </summary>
        public string postalCode;
    }
}
