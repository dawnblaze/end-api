﻿using System;

namespace Endor.Api.Web
{
    /// <summary>
    /// Response to auth generate invite 
    /// </summary>
    public class GenerateInviteResponse
    {
        /// <summary>
        /// Is Employee
        /// </summary>
        public bool IsEmployee { get; set; }

        /// <summary>
        /// Business Id
        /// </summary>
        public short BID { get; set; }

        /// <summary>
        /// Suggested Email
        /// </summary>
        public string SuggestedEmail { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        public Guid ID { get; set; }
    }
}
