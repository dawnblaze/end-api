﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web
{
    /// <summary>
    /// Request class for Generate Invite
    /// </summary>
    public class GenerateInviteRequest
    {
        /// <summary>
        /// Is Employee
        /// </summary>
        public bool IsEmployee { get; set; }

        /// <summary>
        /// Business Id
        /// </summary>
        public short BID { get; set; }

        /// <summary>
        /// Suggested Email
        /// </summary>
        public string SuggestedEmail { get; set; }

        /// <summary>
        /// Display Name
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Property Indicating if Employee Location has an image associated with it
        /// </summary>
        public bool HasImage { get; set; }

        /// <summary>
        /// Blob Uri of Image if one exists
        /// </summary>
        public string ImageLocation { get; set; }
    }
}
