﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Returns the both Employee Details and UserDetails
    /// </summary>
    public class UserExtendedDetail
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userLink"></param>
        public UserExtendedDetail(UserLink userLink = null)
        {
            if (userLink?.EmployeeID!=null)
            {
                this.First = userLink.Employee.First;
                this.Last = userLink.Employee.Last;
                this.Suffix = userLink.Employee.Suffix;
                this.NickName = userLink.Employee.NickName;
                this.ShortName = userLink.Employee.ShortName;
                this.LongName = userLink.Employee.LongName;
                this.EmployeeData = userLink.Employee;
            }
            else if (userLink?.ContactID != null)
            {
                this.First = userLink.Contact.First;
                this.Last = userLink.Contact.Last;
                this.Suffix = userLink.Contact.Suffix;
                this.NickName = userLink.Contact.NickName;
                this.ShortName = userLink.Contact.ShortName;
                this.LongName = userLink.Contact.LongName;
                this.ContactData = userLink.Contact;
            }

            this.UserLinkID = userLink.ID;
            this.AuthUserID = userLink.AuthUserID;
            this.UserName = userLink.UserName;
            this.BID = userLink.BID;
            this.UserAccessType = userLink.UserAccessType;
            this.UserAccessTypeEnum = userLink.UserAccessTypeEnum;
            this.RightsGroupListID = userLink.RightsGroupListID;
        }

        /// <summary>
        /// 
        /// </summary>
        public int? AuthUserID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? UserLinkID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        ///
        /// </summary>
        public short BID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public UserAccessType UserAccessType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public EnumUserAccessType UserAccessTypeEnum { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string First { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Last { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Suffix { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ShortName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LongName { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public EmployeeData EmployeeData { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ContactData ContactData { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public short? RightsGroupListID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ICollection<RightsGroup> RightsGroups { get; set; }
    }
}
