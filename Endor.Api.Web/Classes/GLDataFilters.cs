﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    public class GLDataFilters : IQueryFilters<GLData>
    {
        /// <summary>
        /// GLData`s OrderID
        /// </summary>
        public int? OrderID { get; set; }
        /// <summary>
        /// GLData`s CompanyID
        /// </summary>
        public int? CompanyID { get; set; }
        /// <summary>
        /// GLData`s Activity
        /// </summary>
        public int? GLActivityID { get; set; }
        /// <summary>
        /// GLData`s Account
        /// </summary>
        public int? GLAccountID { get; set; }
        /// <summary>
        /// GLData`s Accounting Start Date
        /// </summary>
        public DateTime? StartDT { get; set; }
        /// <summary>
        /// GLData`s Accounting End Date
        /// </summary>
        public DateTime? EndDT { get; set; }

        /// <summary>
        /// GLData`s number of registry to take
        /// </summary>
        public int? Take { get; set; }

        /// <summary>
        /// GLData`s number of registry to skip
        /// </summary>
        public int? Skip { get; set; }

        /// <summary>
        /// Returns an array of Expressions to pass as where clause parameters
        /// </summary>
        /// <returns></returns>
        public Expression<Func<GLData, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<GLData, bool>>>();

            if (OrderID.HasValue) predicates.Add(gld => gld.OrderID == OrderID.Value);
            if (CompanyID.HasValue) predicates.Add(gld => gld.CompanyID == CompanyID.Value);
            if (GLActivityID.HasValue) predicates.Add(gld => gld.ActivityID == GLActivityID.Value);
            if (GLAccountID.HasValue) predicates.Add(gld => gld.GLAccountID == GLAccountID.Value);
            if (StartDT.HasValue) predicates.Add(gld => gld.AccountingDT >= StartDT.Value);
            if (EndDT.HasValue) predicates.Add(gld => gld.AccountingDT <= EndDT.Value);

            return predicates.ToArray();
        }

        /// <summary>
        /// Verify if the requested amount is valid
        /// </summary>
        /// <returns></returns>
        internal bool AreAmountOfRecordsValid()
        {
            if (Take > 5000 || Take <= 0 || Skip < 0)
                return false;
            return true;
        }

        /// <summary>
        /// Validate all filters
        /// </summary>
        /// <returns></returns>
        internal bool AreFiltersValid()
        {
            return OrderID.HasValue || CompanyID.HasValue || GLActivityID.HasValue || GLAccountID.HasValue || StartDT.HasValue || EndDT.HasValue;
        }

        /// <summary>
        /// Only Zero or One of the OrderID, CompanyID, or GLActivityID can be used.
        /// </summary>
        /// <returns></returns>
        internal bool IsOneOrZeroORderCompanyGLActivityFilters()
        {
            var count = 0;

            if (OrderID.HasValue)
                count++;

            if (CompanyID.HasValue)
                count++;

            if (GLActivityID.HasValue)
                count++;

            return count <= 1;
        }
    }
}
