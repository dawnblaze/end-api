﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Pair of entity ID and its SortIndex
    /// </summary>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="S"></typeparam>
    public class IDSortIndexRef<I, S>
    {
        /// <summary>
        /// ID
        /// </summary>
        public I ID {get; set;}
        /// <summary>
        /// SortIndex
        /// </summary>
        public S SortIndex { get; set; }
    }
}
