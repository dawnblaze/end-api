﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters for PaymentApplication entity
    /// </summary>>
    public class PaymentApplicationFilter : IQueryFilters<PaymentApplication>
    {
        /// <summary>
        /// if specified, filters results to the specified Location.
        /// </summary>
        public int[] LocationID { get; set; }

        /// <summary>
        /// if specified, filters results to the specified Company.
        /// </summary>
        public int[] CompanyID { get; set; }

        /// <summary>
        /// if specified, filters results to the specified Order.
        /// </summary>
        public int? OrderID { get; set; }

        /// <summary>
        /// if specified, filters results to the specified amount.
        /// </summary>
        public decimal? Amount { get; set; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<PaymentApplication, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<PaymentApplication, bool>>>();

            if (this.LocationID != null && this.LocationID.Count() > 0)
            {
                predicates.Add(ad => this.LocationID.Contains(ad.LocationID));
            }

            if (this.CompanyID != null && this.CompanyID.Count() > 0)
            {
                predicates.Add(ad => this.CompanyID.Contains(ad.CompanyID.Value));
            }

            if (this.OrderID.HasValue)
            {
                predicates.Add(ad => ad.OrderID == this.OrderID);
            }

            if (this.Amount.HasValue)
            {
                predicates.Add(ad => ad.Amount == this.Amount);
            }

            return predicates.ToArray();
        }

        /// <summary>
        /// Return true if any filters are present
        /// </summary>
        internal bool HasFilters => WherePredicates().Length > 0;
    }
}
