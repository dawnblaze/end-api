﻿using Endor.EF;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// SimpleGLAccount Filters
    /// </summary>
    public class SimpleGLAccountFilters : IQueryFilters<GLAccount>, IQueryFilterAsyncPrepper, IQueryFilterMapper<GLAccount, SimpleGLAccount>
    {
        /// <summary>
        /// If the GLAccount is active
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// If the GLAccount is assest
        /// </summary>
        public bool? IsAsset { get; set; }
        /// <summary>
        /// If the GLAccount is COGS
        /// </summary>
        public bool? IsCOGS { get; set; }
        /// <summary>
        /// If the GLAccount is equity
        /// </summary>
        public bool? IsEquity { get; set; }
        /// <summary>
        /// If the GLAccount is expense
        /// </summary>
        public bool? IsExpense { get; set; }
        /// <summary>
        /// If the GLAccount is income
        /// </summary>
        public bool? IsIncome { get; set; }
        /// <summary>
        /// If the GLAccount is liability
        /// </summary>
        public bool? IsLiability { get; set; }
        /// <summary>
        /// The GLAccount's type
        /// </summary>
        public int[] GLAccountType { get; set; }
        /// <summary>
        /// If the Numbered Name should be used
        /// </summary>
        public bool? UseNumberedName { get; set; }

        /// <summary>
        /// Performs extra logic before performing standard select operation
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="ctx">API Context</param>
        /// <returns></returns>
        public async Task DoBeforeSelectAsync(short bid, ApiContext ctx)
        {
            if (!UseNumberedName.HasValue)
            {
                var result = await ctx.Database.ExecuteSqlRawAsync2("EXEC [Option.GetValue] @BID = @BID, @OptionName = 'Accounting.GLAccount.ShowNumberedNames'", CancellationToken.None, new SqlParameter("@BID", bid));

                using (DbDataReader dr = result.DbDataReader)
                {
                    while (dr.Read())
                    {
                        bool optionBool;
                        if (bool.TryParse(dr.GetString(1), out optionBool))
                            this.UseNumberedName = optionBool;
                    }
                }
            }
        }
        
        /// <summary>
        /// Mapping function from Full to Simple
        /// </summary>
        /// <param name="source">Full GLAccount</param>
        /// <returns></returns>
        public IEnumerable<SimpleGLAccount> Map(IQueryable<GLAccount> source)
        {
            return source.Select((GLAccount model) => new SimpleGLAccount()
            {
                BID = model.BID,
                ID = model.ID,
                ClassTypeID = model.ClassTypeID,
                IsActive = model.IsActive,
                //EF can handle unpacking the bool? it uses COALESCE
                DisplayName = this.UseNumberedName ?? false ? model.NumberedName : model.Name,
                IsDefault = false
            });
        }

        /// <summary>
        /// Returns an array of Expressions to pass as where clause parameters
        /// </summary>
        /// <returns></returns>
        public Expression<Func<GLAccount, bool>>[] WherePredicates()
        {
            List<Expression<Func<GLAccount, bool>>> predicates = new List<Expression<Func<GLAccount, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(acc => acc.IsActive == IsActive.Value);
            }

            if (GLAccountType != null)
            {
                predicates.Add(acc => GLAccountType.Contains(acc.GLAccountType));
            }


            if(IsExpense.HasValue || IsCOGS.HasValue)//allow retrieving via OR
            {
                predicates.Add(
                    acc => (acc.IsExpense == IsExpense && IsExpense.HasValue)
                    || (acc.IsCOGS == IsCOGS && IsCOGS.HasValue)
                );
            }
            
            if (IsAsset.HasValue)
            {
                predicates.Add(acc => acc.IsAsset == IsAsset.Value);
            }

            if (IsEquity.HasValue)
            {
                predicates.Add(acc => acc.IsEquity == IsEquity.Value);
            }

            if (IsIncome.HasValue)
            {
                predicates.Add(acc => acc.IsIncome == IsIncome.Value);
            }

            if (IsLiability.HasValue)
            {
                predicates.Add(acc => acc.IsLiability == IsLiability.Value);
            }

            return predicates.ToArray();
        }
    }
}
