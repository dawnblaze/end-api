﻿namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Interface for a Dictionary displays validity for a model
    /// </summary>
    public interface IValidationDictionary
    {
        /// <summary>
        /// Adds validation error to the dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <param name="errorMessage"></param>
        void AddError(string key, string errorMessage);
        /// <summary>
        /// If it is valid
        /// </summary>
        bool IsValid { get; }
    }
}