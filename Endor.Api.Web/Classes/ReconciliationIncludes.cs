﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Reconcilitions includes
    /// </summary>
    public class ReconciliationIncludes : IExpandIncludes
    {
        /// <summary>
        /// Include Items from Reconciliations
        /// </summary>
        public IncludesLevel ItemsLevel { get; set; } = IncludesLevel.Full;

        /// <summary>
        /// Get the includes of reconciliation
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { nameof(this.ItemsLevel), new IncludeSettings(this.ItemsLevel, nameof(Reconciliation.Items)) }
            };
        }
    }
}
