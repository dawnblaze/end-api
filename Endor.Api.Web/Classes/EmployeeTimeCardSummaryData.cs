﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// notifications data
    /// </summary>
    public class EmployeeTimeCardSummaryData
    {
        /// <summary>
        /// total hours for week
        /// </summary>
        public decimal TotalHoursForWeek { get; set; }

        /// <summary>
        /// paid hours for week
        /// </summary>
        public decimal PaidHoursForWeek { get; set; }

        /// <summary>
        /// unpaid hours for week
        /// </summary>
        public decimal UnpaidHoursForWeek { get; set; }

        /// <summary>
        /// total hours for a day
        /// </summary>
        public decimal TotalHoursForDay { get; set; }

        /// <summary>
        /// paid hours for a day
        /// </summary>
        public decimal PaidHoursForDay { get; set; }

        /// <summary>
        /// unpaid hours for a day
        /// </summary>
        public decimal UnpaidHoursForDay { get; set; }
    }
}
