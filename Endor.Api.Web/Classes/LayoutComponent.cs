﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// This class is an embedded assembly for calculation layout during computation
    /// </summary>
    public class LayoutComponent : AssemblyData
    {
        /// <summary>
        /// Constructs a layout component with the necessary variables
        /// </summary>
        public LayoutComponent()
        {
            Name = "LayoutComponent";
            Description = "Layout Component";
            this.ID = (int)EmbeddedAssemblyType.LayoutComponent;
            this.AssemblyType = AssemblyType.Embedded;
            this.ClassTypeID = ClassType.Assembly.ID();
            this.IsActive = true;
            Variables = new List<AssemblyVariable>()
            {
                GetVariable("Bleed", DataType.Number, AssemblyElementType.Number),
                GetVariable("BleedBottom", DataType.Number, AssemblyElementType.Number),
                GetVariable("BleedLeft", DataType.Number, AssemblyElementType.Number),
                GetVariable("BleedRight", DataType.Number, AssemblyElementType.Number),
                GetVariable("BleedTop", DataType.Number, AssemblyElementType.Number),
                GetVariable("ColorBarLength", DataType.Number, AssemblyElementType.Number),
                GetVariable("ColorBarPosition", DataType.String, AssemblyElementType.SingleLineText),
                GetVariable("ColorBarThickness", DataType.Number, AssemblyElementType.Number),
                GetVariable("ColorsBack", DataType.Number, AssemblyElementType.Number),
                GetVariable("ColorsFront", DataType.Number, AssemblyElementType.Number),
                GetVariable("Gutter", DataType.Number, AssemblyElementType.Number),
                GetVariable("GutterHorizontal", DataType.Number, AssemblyElementType.Number),
                GetVariable("GutterVertical", DataType.Number, AssemblyElementType.Number),
                GetVariable("ImageBackUniquenessRule", DataType.String, AssemblyElementType.SingleLineText),
                GetVariable("ImageItemUniquenessRule", DataType.String, AssemblyElementType.SingleLineText),
                GetVariable("ImageOrderUniquenessRule", DataType.String, AssemblyElementType.SingleLineText),
                GetVariable("ImpositionType", DataType.String, AssemblyElementType.SingleLineText),
                GetVariable("IncludeColorBar", DataType.Boolean, AssemblyElementType.Checkbox),
                GetVariable("IncludeCuts", DataType.Boolean, AssemblyElementType.Checkbox),
                GetVariable("IncludeVisualization", DataType.Boolean, AssemblyElementType.Checkbox),
                GetVariable("Is2Sided", DataType.Boolean, AssemblyElementType.Checkbox),
                GetVariable("ItemHeight", DataType.Number, AssemblyElementType.Number),
                GetVariable("ItemQuantity", DataType.Number, AssemblyElementType.Number),
                GetVariable("ItemWidth", DataType.Number, AssemblyElementType.Number),
                GetVariable("LayoutType", DataType.String, AssemblyElementType.SingleLineText),
                GetVariable("LeaderBottom", DataType.Number, AssemblyElementType.Number),
                GetVariable("LeaderLeft", DataType.Number, AssemblyElementType.Number),
                GetVariable("LeaderRight", DataType.Number, AssemblyElementType.Number),
                GetVariable("LeaderTop", DataType.Number, AssemblyElementType.Number),
                GetVariable("MaterialHeight", DataType.Number, AssemblyElementType.Number),
                GetVariable("MaterialMargin", DataType.Number, AssemblyElementType.Number),
                GetVariable("MaterialMarginBottom", DataType.Number, AssemblyElementType.Number),
                GetVariable("MaterialMarginLeft", DataType.Number, AssemblyElementType.Number),
                GetVariable("MaterialMarginRight", DataType.Number, AssemblyElementType.Number),
                GetVariable("MaterialMarginTop", DataType.Number, AssemblyElementType.Number),
                GetVariable("MaterialRunningWastePercentage", DataType.Number, AssemblyElementType.Number),
                GetVariable("MaterialSetupWasteCount", DataType.Number, AssemblyElementType.Number),
                GetVariable("MaterialWidth", DataType.Number, AssemblyElementType.Number),
                GetVariable("MaxMaterialsPerCut", DataType.Number, AssemblyElementType.Number),
                GetVariable("OrderedQuantity", DataType.Number, AssemblyElementType.Number),
                GetVariable("RotateItem", DataType.Boolean, AssemblyElementType.Checkbox),
                GetVariable("RotateMaterial", DataType.Boolean, AssemblyElementType.Checkbox),
                GetVariable("ShowColorBar", DataType.Boolean, AssemblyElementType.Checkbox),
                GetVariable("ShowCutLines", DataType.Boolean, AssemblyElementType.Checkbox),
                GetVariable("ShowFoldLines", DataType.Boolean, AssemblyElementType.Checkbox),
                GetVariable("ShowRegistrationMarks", DataType.Boolean, AssemblyElementType.Checkbox),
                GetVariable("TotalMaterialWasteCount", DataType.Number, AssemblyElementType.Number),
                GetVariable("Machine", DataType.String, AssemblyElementType.SingleLineText),
                GetVariable("Material", DataType.String, AssemblyElementType.SingleLineText),
                GetVariable("MachineMaxHorizontal", DataType.Number, AssemblyElementType.Number),
                GetVariable("MachineMaxVertical", DataType.Number, AssemblyElementType.Number),
                GetVariable("ShowBlanks", DataType.Boolean, AssemblyElementType.Checkbox),
            };
        }

        private AssemblyVariable GetVariable(string name, DataType dataType, AssemblyElementType elementType)
        {
            return new AssemblyVariable()
            {
                Name = name,
                Label = name,
                DataType = dataType,
                ElementType = elementType,
            };
        }
    }
}
