﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// DashboardWidget Filters
    /// </summary>
    public class DashboardWidgetFilter : IQueryFilters<DashboardWidgetData>
    {
        /// <summary>
        /// ID to Filter on - Required
        /// </summary>
        public int DashboardID { get; set; }

        /// <summary>
        /// Query Filter for filters on Board Definitions
        /// </summary>
        /// <returns></returns>
        public Expression<Func<DashboardWidgetData, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<DashboardWidgetData, bool>>>();

            predicates.Add(dw => dw.DashboardID == this.DashboardID);

            return predicates.ToArray();
        }
    }
}
