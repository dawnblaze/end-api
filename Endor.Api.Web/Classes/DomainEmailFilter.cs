﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters for DomainEmail entity
    /// </summary>>
    public class DomainEmailFilter : IQueryFilters<DomainEmail>
    {
        /// <summary>
        /// If only Active records should be returned.
        /// When not provided, returns both Active and InActive.
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// If supplied, filters the list to domain emails valid at that location.
        /// </summary>
        public int? LocationID { get; set; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<DomainEmail, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<DomainEmail, bool>>>();

            if (this.IsActive.HasValue)
                predicates.Add(de => de.IsActive == this.IsActive);

            if (this.LocationID.HasValue)
                predicates.Add(de => de.IsForAllLocations == true ||
                    de.LocationLinks.FirstOrDefault(x => x.LocationID == this.LocationID) != null);

            return predicates.ToArray();
        }

        /// <summary>
        /// Return true if any filters are present
        /// </summary>
        internal bool HasFilters => WherePredicates().Length > 0;
    }
}
