﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Swagger Ignore Parameters
    /// </summary>
    public class IgnoreHttpRequestParameterOperationFilter : IOperationFilter
    {
        /// <summary>
        /// Apply
        /// </summary>
        /// <param name="operation">Model</param>
        /// <param name="context">Schema Filter Context</param>
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var toRemove = operation.Parameters.SingleOrDefault(p => p.Name == "force");

            if (toRemove != null)
                operation.Parameters.Remove(toRemove);
        }
    }
}
