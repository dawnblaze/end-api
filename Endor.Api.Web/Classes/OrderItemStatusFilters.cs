using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters for order item entity
    /// </summary>
    public class OrderItemStatusFilters: IQueryFilters<OrderItemStatus>
    {
        /// <summary>
        /// OrderItemStatus.IsActive
        /// </summary>>
        public bool? IsActive { get; set; }
        /// <summary>
        /// OrderItemStatus.OrderStatusID
        /// </summary>
        public byte? OrderStatusID { get; set; }
        /// <summary>
        /// OrderItemStatus.TransactionType
        /// </summary>
        public byte? TransactionType { get; set; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<OrderItemStatus, bool>> WherePredicate()
        {
            return ois=> (ois.IsActive == (bool)(this.IsActive ?? ois.IsActive))
                            && (!this.OrderStatusID.HasValue || ois.OrderStatusID == (OrderOrderStatus)this.OrderStatusID.GetValueOrDefault())
                            && (ois.TransactionType == (this.TransactionType ?? ois.TransactionType));
        }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<OrderItemStatus, bool>>[] WherePredicates()
        {
            List<Expression<Func<OrderItemStatus, bool>>> predicates = new List<Expression<Func<OrderItemStatus, bool>>>();
            return predicates.ToArray();
        }
    }
}
