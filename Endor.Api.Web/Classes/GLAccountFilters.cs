﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// GLAccount Filters
    /// </summary>
    public class GLAccountFilters: IQueryFilters<GLAccount>
    {
        /// <summary>
        /// If the GLAccount is active
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// The GLAccount's type
        /// </summary>
        public int[] GLAccountType { get; set; }
        /// <summary>
        /// If the GLAccount is assest
        /// </summary>
        public bool? IsAsset { get; set; }
        /// <summary>
        /// If the GLAccount is liability
        /// </summary>
        public bool? IsLiability { get; set; }
        /// <summary>
        /// If the GLAccount is COGS
        /// </summary>
        public bool? IsCOGS { get; set; }
        /// <summary>
        /// If the GLAccount is expense
        /// </summary>
        public bool? IsExpense { get; set; }
        /// <summary>
        /// If the GLAccount is income
        /// </summary>
        public bool? IsIncome { get; set; }
        /// <summary>
        /// If the GLAccount is top level
        /// </summary>
        public bool? TopLevel { get; set; }
        /// <summary>
        /// The GLAccount's numbered name
        /// </summary>
        public string NumberedName { get; set; }
        /// <summary>
        /// If going to include inactive GLACcount
        /// </summary>
        public bool? IncludeInActive { get; set; }

        /// <summary>
        /// Returns an array of Expressions to pass as where clause parameters
        /// </summary>
        /// <returns></returns>
        public Expression<Func<GLAccount, bool>>[] WherePredicates()
        {
            List<Expression<Func<GLAccount, bool>>> predicates = new List<Expression<Func<GLAccount, bool>>>();
            
            if ((!IncludeInActive.HasValue || (IncludeInActive.HasValue && IncludeInActive.Value == false)) && IsActive.HasValue) predicates.Add(gla => gla.IsActive == IsActive.Value);
            if (IsAsset.HasValue) predicates.Add(gla => gla.IsAsset.Value);
            if (IsLiability.HasValue) predicates.Add(gla => gla.IsLiability.Value);
            if (IsCOGS.HasValue) predicates.Add(gla => gla.IsCOGS.Value);
            if (IsExpense.HasValue) predicates.Add(gla => gla.IsExpense.Value);
            if (IsIncome.HasValue) predicates.Add(gla => gla.IsIncome.Value);

            if (TopLevel.HasValue) predicates.Add(gla => gla.ParentID == null);

            if (GLAccountType != null)
            {
                predicates.Add(gla => GLAccountType.Contains(gla.GLAccountType));
            }

            if (!String.IsNullOrWhiteSpace(NumberedName))
            {
                predicates.Add(gla => 
                    gla.NumberedName.Contains(NumberedName) || 
                    gla.Subaccounts.Any(subgla => //sub account level 1
                        subgla.NumberedName.Contains(NumberedName) ||
                        subgla.Subaccounts.Any(subsubgla => //sub account level 2
                            subsubgla.NumberedName.Contains(NumberedName) ||
                            subsubgla.Subaccounts.Any(subsubsubgla => //sub account level 3
                                subsubsubgla.NumberedName.Contains(NumberedName)
                            )
                        )
                    )
                );
            }

            return predicates.ToArray();
        }
    }

    /// <summary>
    /// Extensions for GLAccountFilters
    /// </summary>
    public static class GLAccountFilterExtensions
    {
        /// <summary>
        /// Checks if the GLAccount has any active filters
        /// </summary>
        /// <param name="filters">GLAccount filters</param>
        /// <returns></returns>
        public static bool HasFilter(this GLAccountFilters filters)
        {
            return filters != null &&
                (filters.IsActive.HasValue ||
                (filters.GLAccountType != null && filters.GLAccountType.Length > 0) ||
                filters.IsAsset.HasValue || 
                filters.IsLiability.HasValue || 
                filters.IsCOGS.HasValue || 
                filters.IsExpense.HasValue || 
                filters.IsIncome.HasValue ||
                !String.IsNullOrWhiteSpace(filters.NumberedName));

        }
    }
}
