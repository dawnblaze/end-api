﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Simple Order Filters for HTTP GET Methods
    /// </summary>
    public class SimpleOrderFilters : IQueryFilters<SimpleOrderData>
    {
        /// <summary>
        /// (Required) The transaction type to pull
        /// </summary>
        public OrderTransactionType TransactionType { get; set; }


        /// <summary>
        /// Query Filter for filters on Orders
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimpleOrderData, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<SimpleOrderData, bool>>>();

            predicates.Add(bdd => bdd.TransactionType == (byte)this.TransactionType);

            return predicates.ToArray();
        }
    }
}
