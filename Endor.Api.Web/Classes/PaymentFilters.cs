﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Payment Method ID
    /// </summary>
    public class PaymentFilters : IQueryFilters<PaymentMaster>
    {
        /// <summary>
        /// Payment Location ID
        /// </summary>
        public int[] LocationID { get; set; }
        /// <summary>
        /// Payment Company ID
        /// </summary>
        public int[] CompanyID { get; set; }

        /// <summary>
        /// Payment Credit
        /// </summary>
        public bool WithCredit { get; set; } = false;

        /// <summary>
        /// Payment Amount
        /// </summary>
        public decimal? Amount { get; set; }

        /// <summary>
        /// Payment Where Method
        /// </summary>
        public Expression<Func<PaymentMaster, bool>>[] WherePredicates()
        {

            List<Expression<Func<PaymentMaster, bool>>> predicates = new List<Expression<Func<PaymentMaster, bool>>>();
            
            if (this.LocationID != null && this.LocationID.Count() > 0)
            {
                predicates.Add(p => this.LocationID.Contains(p.LocationID));
            }

            if (this.CompanyID != null && this.CompanyID.Count() > 0)
            {
                predicates.Add(p => this.CompanyID.Contains(p.CompanyID));
            }

            if (this.WithCredit)
            {
                predicates.Add(p => p.RefBalance > 0);
            }

            if (this.Amount.HasValue)
            {
                predicates.Add(p => p.Amount == this.Amount);
            }
            

            return predicates.ToArray();
        }

        internal bool HasFilters => WherePredicates().Length > 0;
    }
}
