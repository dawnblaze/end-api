﻿using Endor.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Payment Includes
    /// </summary>
    public class PaymentIncludes : IExpandIncludes
    {
        /// <summary>
        /// 
        /// </summary>
        public bool IncludeApplications { get; set; } = false;
        /// <summary>
        /// Include Company
        /// </summary>
        public bool IncludeCompany { get; set; } = false;

        /// <summary>
        /// Payment Includes Method
        /// </summary>
        public IQueryable<PaymentMaster> AssignIncludedProps(IQueryable<PaymentMaster> query)
        {
            if (this.IncludeApplications)
            {

                query = query.Include(e => e.PaymentApplications);
            }
            if (this.IncludeCompany)
            {

                query = query.Include(e => e.Company);
            }

            return query;
        }

        /// <summary>
        /// Returns the items that should be included with a DB call
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>() {
                { nameof(this.IncludeApplications), new IncludeSettings(this.IncludeApplications ? IncludesLevel.Full : IncludesLevel.None) },
                { nameof(this.IncludeCompany), new IncludeSettings(this.IncludeCompany ? IncludesLevel.Full : IncludesLevel.None) }
            };
        }
    }
}
