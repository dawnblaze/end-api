﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// filters for simple list endpoints on the company controller
    /// </summary>
    public class SimpleSurchargeDefFilters : IQueryFilters<SimpleSurchargeDef>
    {
        /// <summary>
        /// option filter for IsActive
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimpleSurchargeDef, bool>>[] WherePredicates()
        {
            List<Expression<Func<SimpleSurchargeDef, bool>>> predicates = new List<Expression<Func<SimpleSurchargeDef, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(surcharge => surcharge.IsActive == IsActive.Value);
            }

            return predicates.ToArray();
        }
    }
}
