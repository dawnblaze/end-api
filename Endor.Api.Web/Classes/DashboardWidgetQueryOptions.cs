﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Dashboard Widget Query Params
    /// </summary>
    public class DashboardWidgetQueryOptions
    {
        /// <summary>
        /// Dashboard Widget Query Params
        /// </summary>
        public DashboardWidgetQueryOptions()
        {
            //StatusIDList = new List<int>(new[] {21,22,23,24,25,26,29});
        }

        /// <summary>
        /// Employee ID to Filter on - Optional
        /// </summary>
        public long? EmployeeID { get; set; }

        /// <summary>
        /// The starting DateTime for the Query - Optional
        /// </summary>
        public DateTime? StartDT { get; set; }

        /// <summary>
        /// The ending DateTime for the Query - Optional
        /// </summary>
        public DateTime? EndDT { get; set; }

        /// <summary>
        /// The StatusID for the Query.  (This may refer to different Statuses for different queries.) - Optional
        /// </summary>
        public int StatusID { get; set; }

        /// <summary>
        /// The list of StatusIds.
        /// </summary>
        public List<int> StatusIDList { get; set; }

        /// <summary>
        /// For widgets that don't support 'Custom Date Range', eg. OrderTroughput Widget
        /// </summary>
        public short? RangeType { get; set; }

        /// <summary>
        /// For widget #2 where it can specify which timezone rather than the location's timezone or the business location's timezone 
        /// </summary>
        public short? Timezone { get; set; }

        /// <summary>
        /// Instantiates a DashboardWidgetQueryOptions from a json string
        /// </summary>
        /// <param name="S"></param>
        /// <returns></returns>
        public static DashboardWidgetQueryOptions Deserialize(string S)
        {
            if (string.IsNullOrWhiteSpace(S))
                return null;

            return JsonConvert.DeserializeObject<DashboardWidgetQueryOptions>(S);
        }
    }
}
