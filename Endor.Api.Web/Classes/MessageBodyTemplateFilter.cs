﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// filter object for IsActive on GET routes
    /// </summary>
    public class MessageBodyTemplateFilter : IQueryFilters<MessageBodyTemplate>
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IsActive { get; set; }
        public int? AppliesToClassTypeID { get; set; }
        public int? TemplateType { get; set; }
        
        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public Expression<Func<MessageBodyTemplate, bool>>[] WherePredicates()
        {
            List<Expression<Func<MessageBodyTemplate, bool>>> predicates = new List<Expression<Func<MessageBodyTemplate, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(gla => gla.IsActive == IsActive.Value);
            }

            if (AppliesToClassTypeID.HasValue)
            {
                predicates.Add(gla => gla.AppliesToClassTypeID == AppliesToClassTypeID.Value);
            }

            if (TemplateType != null)
            {
                if (AppliesToClassTypeID.HasValue)
                    predicates.Add(gla => gla.TemplateType.AppliesToClassTypeID == AppliesToClassTypeID && gla.TemplateType.ID == TemplateType);
                predicates.Add(gla => gla.TemplateType.ID == TemplateType);
            }

            return predicates.ToArray();
        }

        internal bool HasFilters => WherePredicates().Length > 0;
    }
}
