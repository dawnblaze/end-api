﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters for LocationGoal entity
    /// </summary>>
    public class LocationGoalFilter : IQueryFilters<BusinessGoal>
    {
        /// <summary>
        /// Optional. If true, the business totals are also returned
        /// </summary>
        public bool? IncludeBusiness { get; set; }

        /// <summary>
        /// Optional. The ID of the Location Object whose Goals are to be pulled
        /// </summary>
        public List<byte> LocationID { get; set; }

        /// <summary>
        /// Required. The Year of the goals to be pulled
        /// </summary>
        public List<short> Year { get; set; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<BusinessGoal, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<BusinessGoal, bool>>>();

            if (this.IncludeBusiness.HasValue && this.IncludeBusiness.Value) {
                //just including business - not excluding locations
                //predicates.Add(lg => lg.LocationID == null || lg.LocationID != null);
            }
            else
                predicates.Add(lg => lg.LocationID != null);

            if (this.LocationID != null && this.LocationID.Any())
                predicates.Add(lg => lg.LocationID.HasValue && this.LocationID.ToList().Contains(lg.LocationID.GetValueOrDefault()));

            if (this.Year != null && this.Year.Any())
                predicates.Add(lg => this.Year.ToList().Contains(lg.FiscalYear));

            return predicates.ToArray();
        }
    }
}
