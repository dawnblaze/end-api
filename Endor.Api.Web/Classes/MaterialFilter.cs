﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// filter for material entity
    /// </summary>
    public class MaterialFilter : IIsActiveQueryFilter<MaterialData>
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public Expression<Func<MaterialData, bool>>[] WherePredicates()
        {
            List<Expression<Func<MaterialData, bool>>> predicates = new List<Expression<Func<MaterialData, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(gla => gla.IsActive == IsActive.Value);
            }

            return predicates.ToArray();
        }
    }
}
