﻿using System.Collections.Generic;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// DropDownListValues
    /// </summary>
    public class DropDownListValues
    {
        /// <summary>
        /// Categories
        /// </summary>
        public List<NamedQuark> Categories { get; set; }

        /// <summary>
        /// Components
        /// </summary>
        public List<NamedQuark> Components { get; set; }
    }

    /// <summary>
    /// NamedQuark
    /// </summary>
    public class NamedQuark
    {
        /// <summary>
        /// ID
        /// </summary>
        public int? ID { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }
}
