﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// A payment method filter for simple list endpoints
    /// strongly bound to <see cref="SimplePaymentMethod"/>
    /// </summary>
    public class SimplePaymentMethodFilter: IIsActiveQueryFilter<SimplePaymentMethod>
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimplePaymentMethod, bool>>[] WherePredicates()
        {
            List<Expression<Func<SimplePaymentMethod, bool>>> predicates = new List<Expression<Func<SimplePaymentMethod, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(gla => gla.IsActive == IsActive.Value);
            }

            return predicates.ToArray();
        }
    }

    /// <summary>
    /// a payment method filter for list endpoints
    /// strongly bound to <see cref="PaymentMethod"/>
    /// </summary>
    public class PaymentMethodFilter : IIsActiveQueryFilter<PaymentMethod>
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public Expression<Func<PaymentMethod, bool>>[] WherePredicates()
        {
            List<Expression<Func<PaymentMethod, bool>>> predicates = new List<Expression<Func<PaymentMethod, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(gla => gla.IsActive == IsActive.Value);
            }

            return predicates.ToArray();
        }
    }
}
