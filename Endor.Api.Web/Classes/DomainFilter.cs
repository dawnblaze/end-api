﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// query filters for DomainEmail entity
    /// </summary>>
    public class DomainFilter : IQueryFilters<DomainData>
    {
        /// <summary>
        /// When true includes Inactive records
        /// When not provided, returns Active only.
        /// </summary>
        public bool? IncludeInactive { get; set; }

        /// <summary>
        /// If supplied, filters the list to domains specific to a location.
        /// </summary>
        public byte? LocationID { get; set; }

        /// <summary>
        /// If supplied, filters the list to specific domain access type.
        /// </summary>
        public DomainAccessType? ApplicationType { get; set; }

        /// <summary>
        /// If supplied, filters to a matching domain.
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<DomainData, bool>>[] WherePredicates()
        {
            var predicates = new List<Expression<Func<DomainData, bool>>>();

            if (!this.IncludeInactive.GetValueOrDefault(false))
                predicates.Add(df => df.Status != DomainStatus.Inactive);

            if (this.LocationID.HasValue)
                predicates.Add(df1 => df1.LocationID == LocationID);

            if (this.ApplicationType.HasValue)
                predicates.Add(df1 => df1.AccessType == ApplicationType);

            if (!string.IsNullOrWhiteSpace(this.Domain))
                predicates.Add(df1 => df1.Domain.ToLower() == Domain.ToLower());

            return predicates.ToArray();
        }

        /// <summary>
        /// Return true if any filters are present
        /// </summary>
        internal bool HasFilters => WherePredicates().Length > 0;
    }
}
