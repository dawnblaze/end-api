﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{

    /// <summary>
    /// Layout Component Singleton
    /// </summary>
    public sealed class EmbeddedAssemblySingleton
    {
        private static volatile EmbeddedAssemblySingleton _instance;

        private LayoutComponent layoutComponent = null;

        /// <summary>
        /// Private Constructor
        /// </summary>
        private EmbeddedAssemblySingleton() { }

        /// <summary>
        /// Returns an instance of this class
        /// </summary>
        /// <returns></returns>
        public static EmbeddedAssemblySingleton GetInstance()
        {
            if (_instance == null)
            {
                _instance = new EmbeddedAssemblySingleton();
            }

            return _instance;
        }

        /// <summary>
        /// Get Embedded Assemblies by type
        /// </summary>
        public AssemblyData GetEmbeddedAssembly(EmbeddedAssemblyType type)
        {
            switch(type)
            {
                case EmbeddedAssemblyType.LayoutComponent:
                    if (layoutComponent == null)
                        layoutComponent = new LayoutComponent();
                    return layoutComponent;
                default:
                    return null;
            }
        }
    }
}
