﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// filter object for IsActive on GET routes
    /// </summary>
    public class MessageTemplateTypeFilter : IQueryFilters<SystemMessageTemplateType>
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public int? AppliesToClassTypeID { get; set; }
        public MessageChannelType? ChannelType { get; set; }
        
        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SystemMessageTemplateType, bool>>[] WherePredicates()
        {
            List<Expression<Func<SystemMessageTemplateType, bool>>> predicates = new List<Expression<Func<SystemMessageTemplateType, bool>>>();

            if (AppliesToClassTypeID.HasValue)
            {
                predicates.Add(gla => gla.AppliesToClassTypeID == AppliesToClassTypeID.Value);
            }

            if (ChannelType.HasValue)
            {
                predicates.Add(gla => gla.ChannelType == ChannelType.Value);
            }

            return predicates.ToArray();
        }
    }
}
