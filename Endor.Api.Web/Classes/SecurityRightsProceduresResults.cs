﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    public class SecurityRightsProceduresResults
    {
        public class RightsForArray
        {
            public int RightID { get; set; }
        }

        public class RightsForString 
        {
            public byte[] RightString { get; set; }
        }

        public class RightsForSource
        {
            public int RightID { get; set; }
            public string RightName { get; set; }
            public int GroupID { get; set; }
            public string GroupName { get; set; }
            public string Sources { get; set; }
        }
    }
}
