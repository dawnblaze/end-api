﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// 
    /// </summary>
    public class SimpleAlertDefinitionFilter: SimpleListItemIsActiveFilter<SimpleAlertDefinition, short>
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public DataType? DataType { get; set; }

        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public short? EmployeeID { get; set; }

        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public override Expression<Func<SimpleAlertDefinition, bool>>[] WherePredicates()
        {
            List<Expression<Func<SimpleAlertDefinition, bool>>> predicates = new List<Expression<Func<SimpleAlertDefinition, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(gla => gla.IsActive == IsActive.Value);
            }

            if (DataType.HasValue)
            {
                predicates.Add(gla => gla.DataType == DataType.Value);
            }

            if (EmployeeID.HasValue)
            {
                predicates.Add(gla => gla.EmployeeID == EmployeeID.Value);
            }

            return predicates.ToArray();
        }
    }
}
