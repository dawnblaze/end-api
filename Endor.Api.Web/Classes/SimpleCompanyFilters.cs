﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// filters for simple list endpoints on the company controller
    /// </summary>
    public class SimpleCompanyFilters : IQueryFilters<SimpleCompanyData>
    {
        /// <summary>
        /// option filter for IsActive
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// optional filter for isadhoc
        /// </summary>
        public bool? IsAdHoc { get; set; }
        /// <summary>
        /// List of CompanyIDs to be always shown in the result regardless of the filter conditions
        /// </summary>
        public int[] AllowList { get; set; } = { };
        /// <summary>
        /// Optional filter for Name
        /// </summary>
        /// <value></value>
        public string Name { get; set; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <returns></returns>
        public Expression<Func<SimpleCompanyData, bool>>[] WherePredicates()
        {
            List<Expression<Func<SimpleCompanyData, bool>>> predicates = new List<Expression<Func<SimpleCompanyData, bool>>>();

            if (IsAdHoc.HasValue)
            {
                predicates.Add(company => company.IsAdHoc == IsAdHoc.Value || AllowList.Contains(company.ID));
            } else
            {
                predicates.Add(company => company.IsAdHoc == false || AllowList.Contains(company.ID));
            }

            if (IsActive.HasValue)
            {
                predicates.Add(company => company.IsActive == IsActive.Value || AllowList.Contains(company.ID));
            }

            if (!string.IsNullOrWhiteSpace(this.Name))
            {
                predicates.Add(company => company.DisplayName.ToLower().Contains(this.Name.ToLower()));
            }
                
            return predicates.ToArray();
        }
    }
}
