using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Models;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for LaborData
    /// </summary>
    public class LaborDataIncludes : IExpandIncludes
    {
        internal const string LaborCategoriesKey = "!LaborCategoryLinksWithLaborCategory";

        /// <summary>
        /// indicates whether LaborCategories property is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel LaborCategories { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { LaborCategoriesKey, new IncludeSettings(this.LaborCategories) },
            };
        }
    }
}