﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Includes
{
    /// <summary>
    /// Child Includes for TaxGroups
    /// </summary>
    public class TaxGroupIncludes : IExpandIncludes
    {
        internal const string TaxItemsKey = "!TaxGroupItemLinksWithTaxItem";
        internal const string LocationsKey = "!TaxGroupLocationLinksWithLocation";

        /// <summary>
        /// indicates whether TaxItems property is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel TaxItems { get; set; } = IncludesLevel.None;
        /// <summary>
        /// indicates whether Locations property is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel Locations { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { TaxItemsKey, new IncludeSettings(this.TaxItems) },
                { LocationsKey, new IncludeSettings(this.Locations) }
            };
        }
    }
}
