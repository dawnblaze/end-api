using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Models;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for MachineData
    /// </summary>
    public class MachineDataIncludes : IExpandIncludes
    {
        internal const string MachineCategoriesKey = "!MachineCategoryLinksWithMachineCategory";

        /// <summary>
        /// indicates whether MachineCategories property is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel MachineCategories { get; set; } = IncludesLevel.None;

        //public IncludesLevel IncludeProfiles { get; set; } = IncludesLevel.Full;
        //public IncludesLevel IncludeInstances { get; set; } = IncludesLevel.Full;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                //{ nameof(this.IncludeProfiles), new IncludeSettings(this.IncludeProfiles, nameof(MachineData.Profiles)) },
                //{ nameof(this.IncludeInstances), new IncludeSettings(this.IncludeInstances, nameof(MachineData.Instances)) },
                { MachineCategoriesKey, new IncludeSettings(this.MachineCategories) },
            };
        }
    }
}