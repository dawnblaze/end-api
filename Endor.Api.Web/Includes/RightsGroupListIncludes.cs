﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Includes
{
    /// <summary>
    /// Child Includes for Rights Group List
    /// </summary>
    public class RightsGroupListIncludes : IExpandIncludes
    {
        internal const string EmployeesIncludeKey = "!UserLinks.Employee";
        internal const string ContactsIncludeKey = "!UserLinks.Contact";
        internal const string RightsGroupIncludeKey = "!RightsGroupRightLinks.RightsGroup";

        /// <summary>
        /// indicates whether Employees list is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel Employees { get; set; } = IncludesLevel.None;
        /// <summary>
        /// indicates whether Contacts list is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel Contacts { get; set; } = IncludesLevel.None;
        /// <summary>
        /// indicates whether Rights Group is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel RightsGroups { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { EmployeesIncludeKey, new IncludeSettings(this.Employees) }
                ,{ ContactsIncludeKey, new IncludeSettings(this.Contacts) }
                ,{ RightsGroupIncludeKey, new IncludeSettings(this.RightsGroups) }
            };
        }
    }
}
