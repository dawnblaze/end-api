using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Models;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for LaborCategories
    /// </summary>
    public class LaborCategoryIncludes : IExpandIncludes
    {
        internal const string LaborPartsKey = "!LaborCategoryLinksWithLabor";

        /// <summary>
        /// indicates whether LaborParts property is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel LaborParts { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { LaborPartsKey, new IncludeSettings(this.LaborParts) },
            };
        }
    }
}
