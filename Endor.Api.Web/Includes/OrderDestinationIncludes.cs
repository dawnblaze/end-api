﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Models;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for OrderDestinations
    /// </summary>
    public class OrderDestinationIncludes : IExpandIncludes
    {
        /// <summary>
        /// Contact Level Include
        /// </summary>
        public IncludesLevel ContactLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Employee Level Include
        /// </summary>
        public IncludesLevel EmployeeLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Note Level Include
        /// </summary>
        public IncludesLevel NoteLevel { get; set; } = IncludesLevel.None;
        
        /// <summary>
        /// Gets the dictionary of includes for OrderItems
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { nameof(this.ContactLevel), new IncludeSettings(this.ContactLevel, nameof(OrderDestinationData.ContactRoles)) },
                { nameof(this.EmployeeLevel), new IncludeSettings(this.EmployeeLevel, nameof(OrderDestinationData.EmployeeRoles)) },
                { nameof(this.NoteLevel), new IncludeSettings(this.NoteLevel, nameof(OrderDestinationData.Notes)) },
        
            };
        }
    }
}
