﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Models;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for OrderItems
    /// </summary>
    public class OrderItemIncludes : IExpandIncludes
    {
        /// <summary>
        /// Contact Level Include
        /// </summary>
        public IncludesLevel ContactLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Employee Level Include
        /// </summary>
        public IncludesLevel EmployeeLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Note Level Include
        /// </summary>
        public IncludesLevel NoteLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Date level Includes
        /// </summary>
        public IncludesLevel DateLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Component level Includes
        /// </summary>
        public IncludesLevel Components { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Surcharges level Includes
        /// </summary>
        public IncludesLevel Surcharges { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes for OrderItems
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { nameof(this.ContactLevel), new IncludeSettings(this.ContactLevel, nameof(OrderItemData.ContactRoles)) },
                { nameof(this.EmployeeLevel), new IncludeSettings(this.EmployeeLevel, nameof(OrderItemData.EmployeeRoles)) },
                { nameof(this.NoteLevel), new IncludeSettings(this.NoteLevel, nameof(OrderItemData.Notes)) },
                { nameof(this.DateLevel), new IncludeSettings(this.DateLevel, nameof(OrderItemData.Dates)) },
                { nameof(this.Components), new IncludeSettings(this.Components, nameof(OrderItemData.Components)) },
                { nameof(this.Surcharges), new IncludeSettings(this.Surcharges, nameof(OrderItemData.Surcharges)) }

            };
        }
    }
}
