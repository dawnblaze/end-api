﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Includes
{
    /// <summary>
    /// Child Includes for PaymentMethods
    /// </summary>
    public class PaymentMethodIncludes : IExpandIncludes
    {
        /// <summary>
        /// indicates whether DepositGLAccount property is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel DepositGLAccount { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { nameof(this.DepositGLAccount), new IncludeSettings(this.DepositGLAccount) },
            };
        }
    }
}
