﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for Industries
    /// </summary>
    public class IndustryIncludes : IExpandIncludes
    {
        /// <summary>
        /// Companies Include
        /// </summary>
        public IncludesLevel Companies { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Parent Industry Include
        /// </summary>
        public IncludesLevel ParentIndustry { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                {nameof(this.Companies), new IncludeSettings(this.Companies) },
                {nameof(this.ParentIndustry), new IncludeSettings(this.ParentIndustry)},
            };
        }
    }
}
