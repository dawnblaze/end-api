﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Models;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for MachineCategories
    /// </summary>
    public class MachineCategoryIncludes : IExpandIncludes
    {
        internal const string MachinePartsKey = "!MachineCategoryLinksWithMachine";

        /// <summary>
        /// indicates whether MachineParts property is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel MachineParts { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { MachinePartsKey, new IncludeSettings(this.MachineParts) },
            };
        }
    }
}
