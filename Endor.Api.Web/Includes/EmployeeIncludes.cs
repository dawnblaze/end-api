﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Includes
{
    /// <summary>
    /// Employee model includes
    /// </summary>
    public class EmployeeIncludes : IExpandIncludes
    {
        /// <summary>
        /// Location include
        /// </summary>
        public IncludesLevel Location { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes for employee
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>() { { nameof(this.Location), new IncludeSettings(this.Location) } };
        }
    }
}
