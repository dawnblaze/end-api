﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web
{
    public class CompanyContactLinkIncludes : IExpandIncludes
    {
        /// <summary>
        /// Contact Include
        /// </summary>
        public IncludesLevel Contact { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Company Include
        /// </summary>
        public IncludesLevel Company { get; set; } = IncludesLevel.None;
        /// <summary>
        /// ContactRole Include
        /// </summary>
        public IncludesLevel ContactRole { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes for companies
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { nameof(this.Contact), new IncludeSettings(this.Contact) },
                { nameof(this.Company), new IncludeSettings(this.Company) },
                { nameof(this.ContactRole), new IncludeSettings(this.ContactRole) },
            };
        }
    }
}
