﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Models;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for AssemblyCategories
    /// </summary>
    public class AssemblyCategoryIncludes : IExpandIncludes
    {
        internal const string AssemblyPartsKey = "!AssemblyCategoryLinksWithAssembly";

        /// <summary>
        /// indicates whether AssemblyParts property is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel AssemblyParts { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { AssemblyPartsKey, new IncludeSettings(this.AssemblyParts) },
            };
        }
    }
}
