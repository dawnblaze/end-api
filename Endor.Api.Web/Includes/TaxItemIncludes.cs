using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Includes
{
    /// <summary>
    /// Child Includes for TaxItems
    /// </summary>
    public class TaxItemIncludes : IExpandIncludes
    {
        internal const string TaxGroupKey = "!TaxGroupItemLinksWithTaxGroup";

        /// <summary>
        /// indicates whether TaxGroups property is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel TaxGroups { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { TaxGroupKey, new IncludeSettings(this.TaxGroups) }
            };
        }
    }
}
