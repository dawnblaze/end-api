﻿using Endor.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Includes
{
    public class EmailAccountIncludes : IExpandIncludes
    {
        internal const string IncludeTeamsKey = "!EmailAccountTeamLinksWithTeam";
        public IncludesLevel IncludeTeams { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Returns the items that should be included with a DB call
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>() {
                { IncludeTeamsKey, new IncludeSettings(this.IncludeTeams) }
            };
        }
    }
}
