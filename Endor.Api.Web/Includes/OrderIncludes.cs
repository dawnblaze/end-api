﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for Orders
    /// </summary>
    public class OrderIncludes : IExpandIncludes
    {
        /// <summary>
        /// Destination Level Include
        /// </summary>
        public IncludesLevel DestinationLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Item Level Include
        /// </summary>
        public IncludesLevel ItemLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Contact Level Include
        /// </summary>
        public IncludesLevel ContactLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Employee Level Include
        /// </summary>
        public IncludesLevel EmployeeLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Note Level Include
        /// </summary>
        public IncludesLevel NoteLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Order Link Level Include
        /// </summary>
        public IncludesLevel OrderLinkLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Version Log Include
        /// </summary>
        public IncludesLevel VersionLogLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Order Status Level Include
        /// </summary>
        public IncludesLevel OrderStatusLogLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Key Date Level Include
        /// </summary>
        public IncludesLevel KeyDateLevel { get; set; } = IncludesLevel.Full;
        /// <summary>
        /// Company Level Include
        /// </summary>
        public IncludesLevel CompanyLevel { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes for orders
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { nameof(this.DestinationLevel), new IncludeSettings(this.DestinationLevel, nameof(OrderData.Destinations)) },
                { nameof(this.ItemLevel), new IncludeSettings(this.ItemLevel, nameof(OrderData.Items)) },
                { "!ItemsWithComponentsWithChildComponentsWithChildComponentsWithChildComponents", new IncludeSettings(this.ItemLevel) },
                { "!ItemsWithDates", new IncludeSettings(this.ItemLevel) },
                { "!ItemsWithSurcharges", new IncludeSettings(this.ItemLevel) },
                { nameof(this.ContactLevel), new IncludeSettings(this.ContactLevel, nameof(OrderData.ContactRoles)) },
                { nameof(this.EmployeeLevel), new IncludeSettings(this.EmployeeLevel, nameof(OrderData.EmployeeRoles)) },
                { nameof(this.NoteLevel), new IncludeSettings(this.NoteLevel, nameof(OrderData.Notes)) },
                { nameof(this.OrderLinkLevel), new IncludeSettings(this.OrderLinkLevel, nameof(OrderData.Links)) },
                { nameof(this.VersionLogLevel), new IncludeSettings(this.VersionLogLevel) },
                { nameof(this.OrderStatusLogLevel), new IncludeSettings(this.OrderStatusLogLevel) },
                { nameof(this.KeyDateLevel), new IncludeSettings(this.KeyDateLevel, nameof(OrderData.Dates)) },
                { nameof(this.CompanyLevel), new IncludeSettings(this.CompanyLevel, nameof(OrderData.Company)) }
            };
        }
    }
}
