﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Includes
{
    /// <summary>
    /// Child Includes for OrderItemSubStatuses
    /// </summary>
    public class OrderItemSubStatusIncludes : IExpandIncludes
    {
        internal const string OrderItemsStatusKey = "!OrderItemStatusSubStatusLinksWithOrderItemStatus";

        /// <summary>
        /// indicates whether Order Item Statuses property is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel OrderItemStatuses { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { OrderItemsStatusKey, new IncludeSettings(this.OrderItemStatuses) }
                //{ nameof(this.OrderItemStatuses), new IncludeSettings(this.OrderItemStatuses) },

            };
        }
    }
}
