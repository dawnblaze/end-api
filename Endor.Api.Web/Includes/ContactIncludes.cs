﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for Contacts
    /// </summary>
    public class ContactIncludes : IExpandIncludes
    {
        /// <summary>
        /// Opportunities Include
        /// </summary>
        public IncludesLevel Opportunities { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes for contacts
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { nameof(this.Opportunities), new IncludeSettings(this.Opportunities) }
            };
        }
    }
}
