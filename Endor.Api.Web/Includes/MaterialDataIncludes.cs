using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Models;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for MaterialData
    /// </summary>
    public class MaterialDataIncludes : IExpandIncludes
    {
        internal const string MaterialCategoriesKey = "!MaterialCategoryLinksWithMaterialCategory";

        /// <summary>
        /// indicates whether MaterialCategories property is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel MaterialCategories { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { MaterialCategoriesKey, new IncludeSettings(this.MaterialCategories) },
            };
        }
    }
}