﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Models;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for MaterialCategory
    /// </summary>
    public class MaterialCategoryIncludes : IExpandIncludes
    {
        internal const string MaterialPartsKey = "!MaterialCategoryLinksWithMaterial";

        /// <summary>
        /// indicates whether MaterialParts property is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel MaterialParts { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { MaterialPartsKey, new IncludeSettings(this.MaterialParts) },
            };
        }
    }
}
