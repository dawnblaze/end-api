﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Includes
{
    /// <summary>
    /// Child Includes for EmployeeTeams
    /// </summary>
    public class EmployeeTeamIncludes : IExpandIncludes
    {
        /// <summary>
        /// Employees Include
        /// </summary>
        public IncludesLevel EmployeeLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Locations Include
        /// </summary>
        public IncludesLevel LocationLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Companies Include
        /// </summary>
        public IncludesLevel CompaniesLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Campaigns Include
        /// </summary>
        public IncludesLevel CampaignsLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Opportunities Include
        /// </summary>
        public IncludesLevel OpportunitiesLevel { get; set; } = IncludesLevel.None;
        /// <summary>
        /// EmailAccounts Include
        /// </summary>
        public IncludesLevel EmailAccountsLevel { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { nameof(this.EmployeeLevel), new IncludeSettings(this.EmployeeLevel) },
                { nameof(this.LocationLevel), new IncludeSettings(this.LocationLevel) },
                { nameof(this.CompaniesLevel), new IncludeSettings(this.CompaniesLevel) },
                { nameof(this.CampaignsLevel), new IncludeSettings(this.CampaignsLevel) },
                { nameof(this.OpportunitiesLevel), new IncludeSettings(this.OpportunitiesLevel) },
                { "EmailAccountTeamLinks", new IncludeSettings(this.EmailAccountsLevel) }
            };
        }
    }
}
