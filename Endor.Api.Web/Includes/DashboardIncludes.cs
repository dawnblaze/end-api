﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Includes
{
    /// <summary>
    /// Child Includes for Dashboards
    /// </summary>
    public class DashboardIncludes : IExpandIncludes
    {
        /// <summary>
        /// Widgets Include
        /// </summary>
        public IncludesLevel IncludeWidgets { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes for dashboards
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { nameof(this.IncludeWidgets), new IncludeSettings(this.IncludeWidgets, nameof(DashboardData.Widgets)) },
            };
        }
    }
}
