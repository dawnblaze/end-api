﻿namespace Endor.Api.Web
{
    /// <summary>
    /// Class to define include settings
    /// </summary>
    public class IncludeSettings
    {
        /// <summary>
        /// Include Level
        /// </summary>
        public IncludesLevel Level { get; private set; }
        /// <summary>
        /// Alternative Name
        /// </summary>
        public string AlternativeName { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="level">Level</param>
        /// <param name="altName">Alternative Name</param>
        public IncludeSettings(IncludesLevel level, string altName = null)
        {
            Level = level;
            if (!string.IsNullOrWhiteSpace(altName))
                AlternativeName = altName;
        }
    }
}
