﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for OrderContactRole
    /// </summary>
    public class OrderContactRoleIncludes : IExpandIncludes
    {

        /// <summary>
        /// IncludesLevel setting for type OrderContactRoleLocators.
        /// Currently this property is only used to fix swagger.json validation
        /// and does not affect the GetIncludes implementation.
        /// </summary>
        public IncludesLevel IncludeOrderContactRoleLocators { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>();
        }
    }
}
