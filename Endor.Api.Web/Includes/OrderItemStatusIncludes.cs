﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Classes
{
    /// <summary>
    /// Child Includes for OrderItemStatuses
    /// </summary>
    public class OrderItemStatusIncludes: IExpandIncludes
    {
        internal const string OrderItemsSubStatusKey = "!OrderItemStatusSubStatusLinksWithOrderItemSubStatus";

        /// <summary>
        /// indicates whether SubStatuses property is included
        /// </summary>
        /// <returns></returns>
        public IncludesLevel SubStatuses { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { OrderItemsSubStatusKey, new IncludeSettings(this.SubStatuses) }

            };
        }
    }
}
