﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Models;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for Companies
    /// </summary>
    public class CompanyIncludes : IExpandIncludes
    {
        /// <summary>
        /// Company Locators Include
        /// </summary>
        public IncludesLevel CompanyLocators { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Contacts Include
        /// </summary>
        public IncludesLevel Contacts { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Child Company Include
        /// </summary>
        public IncludesLevel ChildCompanies { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Opportunities Include
        /// </summary>
        public IncludesLevel Opportunities { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Crm Industry Include
        /// </summary>
        public IncludesLevel CrmIndustry { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Custom Fields Include
        /// </summary>
        public IncludesLevel CustomFields { get; set; } = IncludesLevel.None;
        /// <summary>
        /// Company Contact Links Include
        /// </summary>
        public IncludesLevel CompanyContactLinks { get; set; } = IncludesLevel.Full;

        /// <summary>
        /// Gets the dictionary of includes for companies
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { nameof(this.CompanyContactLinks), new IncludeSettings(this.CompanyContactLinks) },
                { nameof(this.Contacts), new IncludeSettings(this.Contacts) },
                { nameof(this.CompanyLocators), new IncludeSettings(this.CompanyLocators) },
                { nameof(this.ChildCompanies), new IncludeSettings(this.ChildCompanies) },
                { nameof(this.Opportunities), new IncludeSettings(this.Opportunities) },
                { nameof(this.CrmIndustry), new IncludeSettings(this.CrmIndustry) },
                { nameof(this.CustomFields), new IncludeSettings(this.CustomFields, nameof(CompanyData.CFValues)) }
            };
        }
    }
}
