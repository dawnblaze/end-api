﻿using System.Collections.Generic;

namespace Endor.Api.Web
{
    /// <summary>
    /// Interface for Includes
    /// </summary>
    public interface IExpandIncludes
    {
        /// <summary>
        /// Get Include Levels for Child Entity Types
        /// </summary>
        /// <returns>Dictionary of include keys and include levels</returns>
        IDictionary<string, IncludeSettings> GetIncludes();
    }
}