﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Models;

namespace Endor.Api.Web
{
    /// <summary>
    /// Child Includes for AssemblyData
    /// </summary>
    public class AssemblyDataIncludes : IExpandIncludes
    {
        /// <summary>
        /// When Full, expand child layouts
        /// </summary>
        public IncludesLevel IncludeLayouts { get; set; } = IncludesLevel.Full;
        /// <summary>
        /// When Full, expand child Variables
        /// </summary>
        public IncludesLevel IncludeVariables { get; set; } = IncludesLevel.Full;

        /// <summary>
        /// When Full, expand child Linked Parts
        /// </summary>
        public IncludesLevel IncludeLinkedParts { get; set; } = IncludesLevel.None;

        /// <summary>
        /// When Full, expand child inked Assemblies 
        /// </summary>
        public IncludesLevel IncludeLinkedAssemblies { get; set; } = IncludesLevel.None;

        /// <summary>
        /// When Full, expand categories
        /// </summary>
        public IncludesLevel IncludeCategories { get; set; } = IncludesLevel.Simple;

        /// <summary>
        /// When Full, expand AssemblyTable 
        /// </summary>
        public IncludesLevel IncludeTables { get; set; } = IncludesLevel.None;

        /// <summary>
        /// Gets the dictionary of includes
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IncludeSettings> GetIncludes()
        {
            return new Dictionary<string, IncludeSettings>()
            {
                { nameof(this.IncludeLayouts), new IncludeSettings(this.IncludeLayouts, "!LayoutsWithElements") },
                { nameof(this.IncludeVariables), new IncludeSettings(this.IncludeVariables, nameof(AssemblyData.Variables)) },
                { "!AssemblyCategoryLinksWithAssemblyCategory", new IncludeSettings(this.IncludeCategories) },
                { nameof(this.IncludeTables), new IncludeSettings(this.IncludeTables, nameof(AssemblyData.Tables)) },
                { "!AssemblyVariablesWithFormulas", new IncludeSettings(this.IncludeVariables, "!VariablesWithFormulas") },
                //{ nameof(this.IncludeLinkedParts), new IncludeSettings(this.IncludeLinkedParts) },
                //{ nameof(this.IncludeLinkedAssemblies), new IncludeSettings(this.IncludeLinkedAssemblies) },
            };
        }
    }
}