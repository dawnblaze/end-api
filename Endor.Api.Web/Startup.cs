﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using Endor.Api.Web.Classes;
using Endor.Models;
using Microsoft.AspNetCore.Http;
using Endor.Logging.Client;
using System.Reflection;
using Endor.RTM;
using Endor.Tasks;
using Endor.EF;
using Endor.Tenant;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Endor.Pricing;
using System.Security.Claims;

using Endor.DNSManagement;
using Endor.Api.Web.Classes.PeerHub;
using Endor.ExternalAuthenticator.Authenticator.Providers;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;
using Endor.CBEL.Common;
using Endor.Api.Web.Services;
using Endor.Api.Web.Providers;
using Endor.CBEL.Autocomplete;
using Endor.Security;
using Endor.Security.Repositories.Interfaces;
using Endor.Security.Repositories;
using Endor.Security.Interfaces;

namespace Endor.Api.Web
{
    /// <summary>
    /// Base Startup Class
    /// </summary>
    public abstract class BaseStartup
    {
        /// <summary>
        /// Updates that impact swagger should be reflected by a change in version
        /// </summary>
        private const string swaggerVersion = "v1.3.0";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env"></param>
        public BaseStartup(IWebHostEnvironment env)
        {
            IConfigurationBuilder builder = GetConfigurationBuilder(env);

            if (env.IsDevelopment())
            {
                Task.Run(AzureStorage.EntityStorageClient.ConfigureLocalDevelopmentCORS).ConfigureAwait(false);
                builder.AddUserSecrets<Startup>();
            }

            Configuration = builder.Build();
        }

        /// <summary>
        /// Returns a configuration builder
        /// </summary>
        /// <param name="env"></param>
        /// <returns></returns>
        public virtual IConfigurationBuilder GetConfigurationBuilder(IWebHostEnvironment env)
        {
            return new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
        }

        /// <summary>
        /// Configuration
        /// </summary>
        public IConfigurationRoot Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public async void ConfigureServices(IServiceCollection services)
        {
            // NOTE: added code for net core 3
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddConfiguration(Configuration.GetSection("Logging"));
                loggingBuilder.AddConsole();
                loggingBuilder.AddDebug();
            });

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, NullValueHandling = NullValueHandling.Ignore };

            // Add framework services.
            services.Configure<EndorOptions>(Configuration.GetSection("Endor"));
            services.AddMemoryCache();
            EndorOptions endorOptions = Configuration.GetSection("Endor").Get<EndorOptions>();
            services.AddSingleton<Tenant.IEnvironmentOptions>(endorOptions);
            services.AddSingleton<EndorOptions>(endorOptions);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<EtagCache>();
            services.AddSingleton(EmbeddedAssemblySingleton.GetInstance());

            EmailProvider emailProvider = new EmailProvider();
            emailProvider.Providers.Add(new Office365Provider(Configuration));
            emailProvider.Providers.Add(new GmailProvider(Configuration));
            services.AddSingleton<IEmailProvider>(emailProvider);
            services.AddMvc(o => o.InputFormatters.Insert(0, new RawRequestFormatterProvider()));
            ConfigureExternalEndorServices(services);

            this.AddAuthentication(services, Configuration);

            //previously we used (options => options.UseSqlServer(Configuration.GetConnectionString("ApiContext")));
            //now ApiContext uses the ITenantDataCache (above) to get the connection string per-request
            services.AddDbContext<ApiContext>();

            services.AddCors();
            services
            .AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            })
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
            })
            .AddApplicationPart(typeof(LogLevelController).GetTypeInfo().Assembly)
            .AddApplicationPart(Assembly.Load("Endor.Api.Web"))
            .AddControllersAsServices();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(swaggerVersion, new OpenApiInfo
                {
                    Title = "Endor Api",
                    Version = swaggerVersion
                });
#pragma warning disable CS0618
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteActions();
                c.CustomSchemaIds(x => x.FullName);
                ApplyIgnoreRelationships(c);
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });
                c.OperationFilter<SwaggerSecurityRightsDocumentFilter>();
                string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                string commentsFileName = Assembly.GetExecutingAssembly().GetName().Name + ".XML";
                string commentsFile = System.IO.Path.Combine(baseDirectory, commentsFileName);

                if (System.IO.File.Exists(commentsFile))
                {
                    c.IncludeXmlComments(commentsFile);
                }
                c.SchemaFilter<SwaggerAddMissingEnums>();
            });

            PeerHubClient peerHubClient = new PeerHubClient(endorOptions);
            await peerHubClient.ConnectAsync();
            await peerHubClient.JoinGroupByName($"bid.ct{ClassType.EmployeeRole.ID()}");
            await peerHubClient.JoinGroupByName($"bid.ct{ClassType.CustomFieldDefinition.ID()}");
            await peerHubClient.JoinGroupByName($"bid.ct{ClassType.CustomFieldLayoutDefinition.ID()}");
            services.AddSingleton<PeerHubClient>(peerHubClient);
        }

        /// <summary>
        /// Configures External Endor Services
        /// </summary>
        /// <param name="services"></param>
        public virtual void ConfigureExternalEndorServices(IServiceCollection services)
        {
            services.AddSingleton<ITenantDataCache, NetworkTenantDataCache>();
            services.AddSingleton<ITaskQueuer>(new HttpTaskQueuer(Configuration["Endor:TasksAPIURL"], Configuration["Endor:TenantSecret"]));
            services.AddSingleton<RemoteLogger>(x => {
                ITenantDataCache cache = x.GetService<ITenantDataCache>();
                RemoteLogger logger = new RemoteLogger(cache);
                logger.TenantSecret = Configuration["Endor:TenantSecret"];
                return logger;
            });
            services.AddSingleton<IServerConfigurations>((x) => new SecurityServerConfiguration(
                x.GetService<ITenantDataCache>(), Configuration["Endor:MessagingServerURL"], x.GetService<IHttpContextAccessor>()));
            services.AddSingleton<IRightsCache, RightsCache>();
            services.AddTransient<IRTMPushClient>((x) => new RealtimeMessagingPushClient(Configuration["Endor:MessagingServerURL"]));
            services.AddSingleton<IMigrationHelper, MigrationHelper>();
            services.AddScoped<IPricingEngine>((x) =>
            {
                ApiContext context = x.GetService<ApiContext>();
                IHttpContextAccessor httpContext = x.GetService<IHttpContextAccessor>();
                ClaimsIdentity identity = httpContext.HttpContext.User.Identity as ClaimsIdentity;
                short BID;
                short.TryParse(identity.FindFirst(ClaimNameConstants.BID).Value, out BID);
                ITenantDataCache cache = x.GetService<ITenantDataCache>();
                RemoteLogger logger = new RemoteLogger(cache);
                return new PricingEngine(BID, context, cache, logger);
            });
            services.AddScoped<IAutocompleteEngine>((x) =>
            {
                ApiContext context = x.GetService<ApiContext>();
                IHttpContextAccessor httpContext = x.GetService<IHttpContextAccessor>();
                ClaimsIdentity identity = httpContext.HttpContext.User.Identity as ClaimsIdentity;
                short BID;
                short.TryParse(identity.FindFirst(ClaimNameConstants.BID).Value, out BID);
                ITenantDataCache cache = x.GetService<ITenantDataCache>();
                RemoteLogger logger = new RemoteLogger(cache);
                return new AutocompleteEngine(BID, context, cache, logger);
            });
            services.AddSingleton<IDNSManager>(new DNSManager(Configuration["Endor:DNSManager:tenantid"],
                                                              Configuration["Endor:DNSManager:client_id"],
                                                              Configuration["Endor:DNSManager:client_secret"],
                                                              Configuration["Endor:DNSManager:subscription_id"],
                                                              Configuration["Endor:DNSManager:resourceGroup"],
                                                              Configuration["Endor:DNSManager:appName"],
                                                              Configuration["Endor:DNSManager:hostNameDNSTarget"],
                                                              Configuration["Endor:DNSManager:resourceLocation"]
                                                                ));

            var _gmailProvider = new GmailProvider(Configuration);
            var _office365Provider = new Office365Provider(Configuration);

            services.AddSingleton<GmailProvider>(_gmailProvider);
            services.AddSingleton<Office365Provider>(_office365Provider);

            bool.TryParse(Configuration["Assemblies:SaveCopyToFile"], out bool saveCopyToFile);
            CBELAssemblyHelper.SaveCopyToFile = saveCopyToFile;

            bool.TryParse(Configuration["Assemblies:SaveCopyToUniqueFileName"], out bool saveCopyToUniqueFileName);
            CBELAssemblyHelper.SaveCopyToUniqueFileName = saveCopyToUniqueFileName;

            CBELAssemblyHelper.SaveToDirectory = Configuration["Assemblies:SaveToDirectory"];
        }

        private static void ApplyIgnoreRelationships(Swashbuckle.AspNetCore.SwaggerGen.SwaggerGenOptions c)
        {
            c.SchemaFilter<ApplyIgnoreRelationshipsInModelsNamespace>("Endor.Models");
        }

        /// <summary>
        /// This method configures the authentication service
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public abstract void AddAuthentication(IServiceCollection services, IConfigurationRoot configuration);

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline. 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="appLifetime"></param>
        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime appLifetime)
        {
            RemoteLoggingExtensions.ConfigureSystemRemoteLogging(Configuration, loggerFactory, appLifetime);
            app.UseAuthentication();
            app.UseSwagger()
            .UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/{swaggerVersion}/swagger.json", $"Endor Api {swaggerVersion}");
                c.DocExpansion(DocExpansion.None);
                c.RoutePrefix = "swagger";
            })
            .UseCors(t => t.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod())
            .UseMvc()
            .UseDeveloperExceptionPage();
        }
    }

    /// <summary>
    /// Startup Class
    /// </summary>
    public class Startup : BaseStartup
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env"></param>
        public Startup(IWebHostEnvironment env) : base(env)
        {
        }

        /// <summary>
        /// This method configures the authentication service
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public override void AddAuthentication(IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(option =>
            {
                option.Audience = Configuration["Auth:ValidAudience"];
                option.TokenValidationParameters = new TokenValidationParameters()
                {
                    NameClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier",
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidIssuer = Configuration["Auth:ValidIssuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(WebEncoders.Base64UrlDecode(Configuration["Auth:SymmetricKey"])),
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromMilliseconds(2)
                };
            });
            services.AddAuthorization();
        }
    }
}
