﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Web.Extensions
{
    public static class ServiceProviderExtensions
    {
        public static TServiceImplementation GetService<TServiceImplementation>(this IServiceProvider provider) => 
            (TServiceImplementation)provider.GetService(typeof(TServiceImplementation));
    }
}
