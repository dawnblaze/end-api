BEGIN TRANSACTION
ALTER TABLE dbo.[Company.Data] ADD
    HasImage bit NOT NULL CONSTRAINT [DF_Company.Data_HasImage] DEFAULT 1

ALTER TABLE dbo.[Contact.Data] ADD
    HasImage bit NOT NULL CONSTRAINT [DF_Contact.Data_HasImage] DEFAULT 1

ALTER TABLE dbo.[Business.Data] ADD
    HasImage bit NOT NULL CONSTRAINT [DF_Business.Data_HasImage] DEFAULT 1

ALTER TABLE dbo.[Employee.Data] ADD
    HasImage bit NOT NULL CONSTRAINT [DF_Employee.Data_HasImage] DEFAULT 1

ALTER TABLE dbo.[Lead.Data] ADD
    HasImage bit NOT NULL CONSTRAINT [DF_Lead.Data_HasImage] DEFAULT 1

ALTER TABLE dbo.[Lead.Locator] ADD
    HasImage bit NOT NULL CONSTRAINT [DF_Lead.Locator_HasImage] DEFAULT 1

ALTER TABLE dbo.[Company.Locator] ADD
    HasImage bit NOT NULL CONSTRAINT [DF_Company.Locator_HasImage] DEFAULT 1

ALTER TABLE dbo.[Business.Locator] ADD
    HasImage bit NOT NULL CONSTRAINT [DF_Business.Locator_HasImage] DEFAULT 1

ALTER TABLE dbo.[Employee.Locator] ADD
    HasImage bit NOT NULL CONSTRAINT [DF_Employee.Locator_HasImage] DEFAULT 1

ALTER TABLE dbo.[Contact.Locator] ADD
    HasImage bit NOT NULL CONSTRAINT [DF_Contact.Locator_HasImage] DEFAULT 1

COMMIT