CREATE TABLE [dbo].[Util.NextID](
	BID smallint NOT NULL,
	ClassTypeID int NOT NULL,
	NextID int NOT NULL,
 CONSTRAINT PK_NextID PRIMARY KEY CLUSTERED (BID, ClassTypeID)
);

IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Util.ID.GetID')
  DROP PROCEDURE [Util.ID.GetID];
GO
-- ========================================================
-- 
-- Name: Util.ID.GetID( @BID smallint, @ClassTypeID int, @Count int = 1 )
--
-- Description: This Function Gets a New ID for a ClassType (Table).
--  If requesting multiple IDs, the first ID in the series is returned.
--
-- Sample Use:   
--      declare @NewID int;
--      exec @NewID = dbo.[Util.ID.GetID] @BID=100, @ClassTypeID=3500
--
-- ========================================================
CREATE PROCEDURE [Util.ID.GetID] 
        @BID smallint
      , @ClassTypeID int

      -- Optional fields
      , @Count      int = 1   -- how many IDs are requested.  The first ID in a sequential block is returned.
AS
BEGIN
    DECLARE @NextID INT;
    IF (@BID IS NULL) SET @BID = -1; -- Use -1 for System

    -- Assume the record exists ... 
    UPDATE [Util.NextID]
    SET @NextID = NextID = COALESCE(NextID, 999) + @Count
    WHERE BID = @BID AND ClassTypeID = @ClassTypeID
    ;

    -- Check if no rows found, in which case use an add
    IF (@@RowCount=0)
    BEGIN
        SET @NextID = 1000 + @Count;
        INSERT INTO [Util.NextID] (BID, ClassTypeID, NextID) VALUES(@BID, @ClassTypeID, @NextID);
    END;

    RETURN (@NextID-@Count);
END;


IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Util.ID.ValidateTableID')
  DROP PROCEDURE [Util.ID.ValidateTableID];
GO
-- ========================================================
-- 
-- Name: Util.ID.ValidateTableID( @BID smallint, @ClassTypeID int, @TableName varchar(155) )
--
-- Description: This Function Gets a New ID for a ClassType (Table).
--  If requesting multiple IDs, the first ID in the series is returned.
--
-- Sample Use:   
--      exec dbo.[Util.ID.ValidateTableID] @BID=100, @ClassTypeID=3500, @TableName='Employee.Data'
--
-- ========================================================
CREATE PROCEDURE [Util.ID.ValidateTableID] 
        @BID smallint
      , @ClassTypeID int
      , @TableName  varchar(155) = '' -- The corresponding tablename.  Only used when @ValidateTable=1
AS
BEGIN
    DECLARE @NextID INT =  (SELECT NextID 
                            FROM [Util.NextID]
                            WHERE BID = @BID AND ClassTypeID = @ClassTypeID);

    DECLARE @TableMaxID int
            , @cmd Nvarchar(max) = 'SELECT @outvar = MAX(ID) FROM ['+@TableName+']';

    EXEC SP_ExecuteSQL @Query = @cmd
                    , @Params = N'@outvar INT OUTPUT'
                    , @outvar = @TableMaxID OUTPUT
    ;

    IF (@NextID IS NULL)
    BEGIN
        INSERT INTO [Util.NextID] (BID, ClassTypeID, NextID) VALUES(@BID, @ClassTypeID, CASE WHEN @TableMaxID > 1000 THEN @TableMaxID + 1 ELSE 1000 END)
        ;
        SELECT @TableName + ' NexyID Updated to ' + CONVERT(Varchar(12), CASE WHEN @TableMaxID > 1000 THEN @TableMaxID + 1 ELSE 1000 END )
        ;
    END

    ELSE IF (@TableMaxID >= @NextID)
    BEGIN
        UPDATE [Util.NextID]
        SET NextID = @TableMaxID + 1
        WHERE BID = @BID AND ClassTypeID = @ClassTypeID
        ;
        SELECT @TableName + ' NexyID Updated to ' + CONVERT(Varchar(12), @TableMaxID + 1 )
        ;
    END
    ELSE
        SELECT @TableName + ' Ok ';
END;